TEMPLATE = subdirs


CONFIG += ordered

SUBDIRS += \
    src \
    examples

!ios{

    # update our .qmake.conf file which is used by qdoc for version numbering
    system(echo load\\(qt_build_config\\)\\\\n\\\\nMODULE_VERSION=$$QT_VERSION > $$_PRO_FILE_PWD_/.qmake.conf)

    SUBDIRS += tests \
        doc
}

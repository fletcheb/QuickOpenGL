/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksampler.h"
#include <QQuickWindow>
#include <QThread>
#include <QSGTextureProvider>
#include <QSGTexture>
#include <QOpenGLTexture>
#include <QOpenGLFunctions_3_3_Core>
#include "support.h"
#include "quicksgsamplertextureprovider.h"

/*!
 * \qmltype Sampler
 * \brief The Sampler QML type represents an OpenGL sampler object.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickSamplerObject
 * \ingroup QuickOpenGL QML Types
 * 
 *
 * Sampler allows OpenGL sampler objects to be used directly in QML. A sampler
 * object stores sampling parameters for a texture.
 *
 * Example usage:
 *
 * \qml
 * Image {
 *     id: image
 *     source: 'image.jpg'
 *     visible: false
 * }
 *
 * Sampler {
 *     id: sampler
 *     source: image
 *     wrapS: Sampler.WrapMirroredRepeat
 *     wrapT: Sampler.WrapClampToBorder
 *     borderColor: '#ff0000'
 * }
 *
 * GraphicsProgram {
 *     anchors.fill: parent
 *     property var image: sampler
 *
 *     fragmentShader: "
 *         #version 330
 *
 *         in vec2 coord;
 *         out vec4 fragColor;
 *         uniform sampler2D image;
 *
 *         void main()
 *         {
 *           fragColor = texture(image, vec2(-0.25)+coord*1.5);
 *         }"
 * }
 *
 * \endqml
 *
 * \sa https://www.khronos.org/opengl/wiki/Sampler_Object
 */


class QuickSamplerObjectPrivate : public QObject
{
    Q_OBJECT

public:
    QuickSamplerObjectPrivate(QuickSamplerObject *q);

    QuickSamplerObject::Filter magFilter;
    QuickSamplerObject::Filter minFilter;
    double lodBias;
    double maxLod;
    double minLod;
    QuickSamplerObject::Wrap wrapS;
    QuickSamplerObject::Wrap wrapT;
    QuickSamplerObject::Wrap wrapR;
    QColor borderColor;

    QQuickItem * source;

    QuickSGSamplerTextureProvider *textureProvider;

public slots:

    void onSceneGraphInvalidated();

protected:

    QuickSamplerObject *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickSamplerObject)


};

QuickSamplerObjectPrivate::QuickSamplerObjectPrivate(QuickSamplerObject *q):
    magFilter(QuickSamplerObject::FilterLinear),
    minFilter(QuickSamplerObject::FilterNearestMipMapLinear),
    lodBias(0.0),
    maxLod(1000.0),
    minLod(-1000.0),
    wrapS(QuickSamplerObject::WrapRepeat),
    wrapT(QuickSamplerObject::WrapRepeat),
    wrapR(QuickSamplerObject::WrapRepeat),
    source(nullptr),
    textureProvider(nullptr),
    q_ptr(q)
{

}

void QuickSamplerObjectPrivate::onSceneGraphInvalidated()
{
    delete textureProvider;
    textureProvider = nullptr;
}

QuickSamplerObject::QuickSamplerObject(QQuickItem *parent) :
    QQuickItem(parent),
    d_ptr(new QuickSamplerObjectPrivate(this))
{
    setFlag(QQuickItem::ItemHasContents);
}

QuickSamplerObject::~QuickSamplerObject()
{
    Q_D(QuickSamplerObject);

    // we should have cleaned up by now
    Q_ASSERT(d->textureProvider==nullptr);
}

bool QuickSamplerObject::isTextureProvider() const
{
    Q_D(const QuickSamplerObject);

    if(d->source)
    {
        return d->source->isTextureProvider();
    }

    return true;
}

QSGTextureProvider *QuickSamplerObject::textureProvider() const
{
    Q_D(const QuickSamplerObject);

    if(window()&&window()->openglContext()&&window()->openglContext()->thread()&&(window()->openglContext()->thread()==QThread::currentThread()))
    {

        if(d->source&&d->source->textureProvider())
        {
            // cast away the constedness
            QuickSamplerObjectPrivate *dd = const_cast<QuickSamplerObjectPrivate *>(d);

            if(!dd->textureProvider)
            {
                // connect to scene graph invalidated so we can clean up
                connect(window(), &QQuickWindow::sceneGraphInvalidated, d, &QuickSamplerObjectPrivate::onSceneGraphInvalidated, Qt::DirectConnection);

                // create a new sampler texture provider
                dd->textureProvider = new QuickSGSamplerTextureProvider;

                // set the sampler texture provider properties
                dd->textureProvider->setSourceProvider(d->source->textureProvider());
                dd->textureProvider->setMagFilter(d->magFilter);
                dd->textureProvider->setMinFilter(d->minFilter);
                dd->textureProvider->setLodBias(d->lodBias);
                dd->textureProvider->setMaxLod(d->maxLod);
                dd->textureProvider->setMinLod(d->minLod);
                dd->textureProvider->setWrapS(d->wrapS);
                dd->textureProvider->setWrapT(d->wrapT);
                dd->textureProvider->setWrapR(d->wrapR);
                dd->textureProvider->setBorderColor(d->borderColor);

            }

            return  d->textureProvider;
        }
    }
    else
    {
        qWarning("QuickSamplerObject::textureProvider: can only be queried on the rendering thread of an exposed window");
    }

    return nullptr;

}

/*!
 * \qmlproperty enumeration Sampler::magFilter
 *
 * This property holds the sampler's magnification filter type. The default value
 * is FilterLinear.
 *
 * magFilter takes the following values:
 *
 * \value FilterNearest
 *   GL_NEAREST
 * \value FilterLinear
 *   GL_LINEAR
 *
 * \sa minFilter
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
QuickSamplerObject::Filter QuickSamplerObject::magFilter() const
{
    Q_D(const QuickSamplerObject);

    return d->magFilter;
}

void QuickSamplerObject::setMagFilter(QuickSamplerObject::Filter magFilter)
{
    Q_D(QuickSamplerObject);

    if(d->magFilter == magFilter)
        return;

    d->magFilter = magFilter;
    emit magFilterChanged(magFilter);
    update();
}

/*!
 * \qmlproperty enumeration Sampler::minFilter
 *
 * This property holds the sampler's minification filter type. The default value
 * is FilterNearestMipMapLinear.
 *
 * minFilter takes the following values:
 *
 * \value FilterNearest
 *   GL_NEAREST
 * \value FilterLinear
 *   GL_LINEAR
 * \value FilterNearestMipMapNearest
 *   GL_NEAREST_MIPMAP_NEAREST
 * \value FilterNearestMipMapLinear
 *   GL_NEAREST_MIPMAP_LINEAR
 * \value FilterLinearMipMapNearest
 *   GL_LINEAR_MIPMAP_NEAREST
 * \value FilterLinearMipMapLinear
 *   GL_LINEAR_MIPMAP_LINEAR
 *
 * \sa magFilter
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
QuickSamplerObject::Filter QuickSamplerObject::minFilter() const
{
    Q_D(const QuickSamplerObject);

    return d->minFilter;
}

void QuickSamplerObject::setMinFilter(QuickSamplerObject::Filter minFilter)
{
    Q_D(QuickSamplerObject);

    if(d->minFilter == minFilter)
        return;

    d->minFilter = minFilter;
    emit minFilterChanged(minFilter);
    update();
}

/*!
 * \qmlproperty real Sampler::lodBias
 *
 * This property holds the sampler's level of detail bias. It specifies a fixed
 * bias value that is to be added to the level-of-detail parameter for the
 * texture before texture sampling.  The default value is 0.
 *
 * lodBias takes values in the range [-OpenGLParameters::glMaxTextureLodBias,
 * OpenGLParameters::glMaxTextureLodBias]
 *
 * \sa minLod
 * \sa maxLod
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
double QuickSamplerObject::lodBias() const
{
    Q_D(const QuickSamplerObject);

    return d->lodBias;
}

void QuickSamplerObject::setLodBias(double lodBias)
{
    Q_D(QuickSamplerObject);

    if(d->lodBias == lodBias)
        return;

    d->lodBias = lodBias;
    emit lodBiasChanged(lodBias);
    update();
}

/*!
 * \qmlproperty real Sampler::maxLod
 *
 * This property holds the sampler's maximum level-of-detail parameter. It
 * specifies the lowest resolution mipmap (highest mipmap level) available for
 * selection. The default value is 1000.
 *
 * \sa minLod
 * \sa lodBias
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
double QuickSamplerObject::maxLod() const
{
    Q_D(const QuickSamplerObject);

    return d->maxLod;
}

void QuickSamplerObject::setMaxLod(double maxLod)
{
    Q_D(QuickSamplerObject);

    if (d->maxLod == maxLod)
        return;

    d->maxLod = maxLod;
    emit maxLodChanged(d->maxLod);
    update();
}

double QuickSamplerObject::minLod() const
{
    Q_D(const QuickSamplerObject);

    return d->minLod;
}

/*!
 * \qmlproperty real Sampler::minLod
 *
 * This property holds the sampler's minimum level-of-detail parameter. It
 * specifies the highest resolution mipmap (lowest mipmap level) available for
 * selection. The default value is -1000.
 *
 * \sa maxLod
 * \sa lodBias
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
void QuickSamplerObject::setMinLod(double minLod)
{
    Q_D(QuickSamplerObject);

    if (d->minLod == minLod)
        return;

    d->minLod = minLod;
    emit minLodChanged(d->minLod);
    update();

}

/*!
 * \qmlproperty enumeration Sampler::wrapS
 *
 * This property holds the sampler's s coordinate wrap parameter. The default
 * value is WrapRepeat.
 *
 * wrapS takes the following values:
 *
 * \value WrapRepeat
 *   GL_REPEAT
 * \value WrapMirroredRepeat
 *   GL_MIRRORED_REPEAT
 * \value WrapClampToEdge
 *   GL_CLAMP_TO_EDGE
 * \value WrapClampToBorder
 *   GL_CLAMP_TO_BORDER
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
QuickSamplerObject::Wrap QuickSamplerObject::wrapS() const
{
    Q_D(const QuickSamplerObject);

    return d->wrapS;
}

void QuickSamplerObject::setwrapS(QuickSamplerObject::Wrap wrapS)
{
    Q_D(QuickSamplerObject);

    if(d->wrapS == wrapS)
        return;

    d->wrapS = wrapS;
    emit wrapSChanged(wrapS);
    update();
}

/*!
 * \qmlproperty enumeration Sampler::wrapT
 *
 * This property holds the sampler's t coordinate wrap parameter. The default
 * value is WrapRepeat.
 *
 * wrapT takes the following values:
 *
 * \value WrapRepeat
 *   GL_REPEAT
 * \value WrapMirroredRepeat
 *   GL_MIRRORED_REPEAT
 * \value WrapClampToEdge
 *   GL_CLAMP_TO_EDGE
 * \value WrapClampToBorder
 *   GL_CLAMP_TO_BORDER
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
QuickSamplerObject::Wrap QuickSamplerObject::wrapT() const
{
    Q_D(const QuickSamplerObject);

    return d->wrapT;
}

void QuickSamplerObject::setwrapT(QuickSamplerObject::Wrap wrapT)
{
    Q_D(QuickSamplerObject);

    if(d->wrapT == wrapT)
        return;

    d->wrapT = wrapT;
    emit wrapTChanged(wrapT);
    update();
}

/*!
 * \qmlproperty enumeration Sampler::wrapR
 *
 * This property holds the sampler's r coordinate wrap parameter. The default
 * value is WrapRepeat.
 *
 * wrapR takes the following values:
 *
 * \value WrapRepeat
 *   GL_REPEAT
 * \value WrapMirroredRepeat
 *   GL_MIRRORED_REPEAT
 * \value WrapClampToEdge
 *   GL_CLAMP_TO_EDGE
 * \value WrapClampToBorder
 *   GL_CLAMP_TO_BORDER
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
QuickSamplerObject::Wrap QuickSamplerObject::wrapR() const
{
    Q_D(const QuickSamplerObject);

    return d->wrapR;
}

void QuickSamplerObject::setwrapR(QuickSamplerObject::Wrap wrapR)
{
    Q_D(QuickSamplerObject);

    if(d->wrapR == wrapR)
        return;

    d->wrapR = wrapR;
    emit wrapRChanged(wrapR);
    update();
}

/*!
 * \qmlproperty color Sampler::borderColor
 *
 * This property holds the sampler's border color.  The default value is #000000.
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glSamplerParameter
 */
QColor QuickSamplerObject::borderColor() const
{
    Q_D(const QuickSamplerObject);

    return d->borderColor;
}

void QuickSamplerObject::setBorderColor(QColor borderColor)
{
    Q_D(QuickSamplerObject);

    if(d->borderColor == borderColor)
        return;

    d->borderColor = borderColor;
    emit borderColorChanged(borderColor);
    update();

}

/*!
 * \qmlproperty object Sampler::source
 *
 * This property holds the sampler's source item.  It must be a texture provider.
 */
QQuickItem *QuickSamplerObject::source() const
{
    Q_D(const QuickSamplerObject);

    return d->source;
}

void QuickSamplerObject::setSource(QQuickItem *source)
{
    Q_D(QuickSamplerObject);

    if(d->source == source)
        return;

    d->source = source;
    emit sourceChanged(source);

    if(d->source&&(!d->source->isTextureProvider()))
    {
        qWarning() << "QuickOpenGL:" << this << "source set to" << source << "which is not a texture provider";
    }

    update();
}


void QuickSamplerObject::releaseResources()
{
    Q_D(QuickSamplerObject);

    if(d->textureProvider)
    {
        window()->scheduleRenderJob(cleanup(d->textureProvider), QQuickWindow::AfterSynchronizingStage);
        d->textureProvider=nullptr;
    }

}

QSGNode *QuickSamplerObject::updatePaintNode(QSGNode *, UpdatePaintNodeData *)
{

    Q_D(QuickSamplerObject);

    if(d->source&&d->source->textureProvider()&&d->textureProvider)
    {
        // synchronise the sampler texture provider properties
        d->textureProvider->setSourceProvider(d->source->textureProvider());
        d->textureProvider->setMagFilter(d->magFilter);
        d->textureProvider->setMinFilter(d->minFilter);
        d->textureProvider->setLodBias(d->lodBias);
        d->textureProvider->setMaxLod(d->maxLod);
        d->textureProvider->setMinLod(d->minLod);
        d->textureProvider->setWrapS(d->wrapS);
        d->textureProvider->setWrapT(d->wrapT);
        d->textureProvider->setWrapR(d->wrapR);
        d->textureProvider->setBorderColor(d->borderColor);

    }

    return nullptr;
}

#include "quicksampler.moc"




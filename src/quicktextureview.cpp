/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicktextureview.h"
#include <QQuickWindow>
#include <QThread>
#include <QSGTextureProvider>
#include <QSGTexture>
#include <QOpenGLTexture>
#include <QOpenGLFunctions_4_3_Core>
#include "support.h"
#include "quicksgtextureviewtextureprovider.h"



/*!
 * \qmltype TextureView
 * \brief The TextureView QML type represents an OpenGL texture view.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickTexture
 * \ingroup QuickOpenGL QML Types
 * 
 *
 * TextureView represents an OpenGL texture view object. It manages a \l
 * QSGTextureProvider in the \l{Qt Quick Scene Graph}.
 *
 * TextureView allows specification of an OpenGL texture view object directly
 * in QML. A texture view object shares all or some of the source texture's
 * data store.
 *
 * \sa Texture
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTextureView
 */

class QuickTextureViewPrivate: public QObject
{
    Q_OBJECT

public:

    QuickTextureViewPrivate(QuickTextureView *q);

    QQuickItem * source;
    QuickTextureView::Target target;
    QuickTextureView::Format format;
    int minimumLevel;
    int levels;
    int minimumLayer;
    int layers;

    QuickSGTextureViewTextureProvider *textureProvider;

public slots:

    void onSceneGraphInvalidated();

protected:

    QuickTextureView *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickTextureView)

};

QuickTextureViewPrivate::QuickTextureViewPrivate(QuickTextureView *q) :
    source(nullptr),
    target(QuickTextureView::Target2D),
    format(QuickTextureView::FormatRGBA32F),
    minimumLevel(0),
    levels(1),
    minimumLayer(0),
    layers(1),
    textureProvider(nullptr),
    q_ptr(q)
{

}

void QuickTextureViewPrivate::onSceneGraphInvalidated()
{
    if(textureProvider)
    {
        delete textureProvider;
        textureProvider=nullptr;
    }
}

QuickTextureView::QuickTextureView(QQuickItem *parent):
    QQuickItem(parent),
    d_ptr(new QuickTextureViewPrivate(this))
{
    setFlag(QQuickItem::ItemHasContents);
}

QuickTextureView::~QuickTextureView()
{

}

/*!
 * \qmlproperty object TextureView::source
 *
 * This property holds the texture view's source item. It must be a texture
 * provider.
 */
QQuickItem *QuickTextureView::source() const
{
    Q_D(const QuickTextureView);
    return d->source;
}

void QuickTextureView::setSource(QQuickItem *source)
{
    Q_D(QuickTextureView);

    if (d->source == source)
        return;

    d->source = source;
    emit sourceChanged(source);
    update();
}

/*!
 * \qmlproperty enumeration TextureView::target
 *
 * This property holds the texture view's target.  It must be compatible with
 * with the source texture's target.
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTextureView
 */
QuickTextureView::Target QuickTextureView::target() const
{
    Q_D(const QuickTextureView);
    return d->target;
}

void QuickTextureView::setTarget(QuickTextureView::Target target)
{
    Q_D(QuickTextureView);

    if (d->target == target)
        return;

    d->target = target;
    emit targetChanged(target);
    update();
}

/*!
 * \qmlproperty int TextureView::minimumLevel
 *
 *  This property holds the texture view's lowest level of detail.
 */
int QuickTextureView::minimumLevel() const
{
    Q_D(const QuickTextureView);
    return d->minimumLevel;
}

/*!
 * \qmlproperty int TextureView::levels
 *
 *  This property hodls the texture view's number of levels of detail.
 */
int QuickTextureView::levels() const
{
    Q_D(const QuickTextureView);
    return d->levels;
}

/*!
 * \qmlproperty int TextureView::minimumLayer
 *
 * This property holds the texture view's first included layer.
 */
int QuickTextureView::minimumLayer() const
{
    Q_D(const QuickTextureView);
    return d->minimumLayer;
}

/*!
 * \qmlproperty int TextureView::layers
 *
 * This property holds the texture view's number of included layers
 */
int QuickTextureView::layers() const
{
    Q_D(const QuickTextureView);
    return d->layers;
}

/*!
 * \qmlproperty enumeration TextureView::format
 *
 * This property holds the texture view's internal format.  It must be
 * compatible with the source texture's internal format class.
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTextureView
 */
QuickTextureView::Format QuickTextureView::format() const
{
    Q_D(const QuickTextureView);
    return d->format;
}

bool QuickTextureView::isTextureProvider() const
{
    Q_D(const QuickTextureView);

    if(d->source)
    {
        return d->source->isTextureProvider();
    }

    return true;
}

QSGTextureProvider *QuickTextureView::textureProvider() const
{
    Q_D(const QuickTextureView);

    // if we are in the thread that owns the OpenGL context
    if(window()&&window()->openglContext()&&window()->openglContext()->thread()&&(window()->openglContext()->thread()==QThread::currentThread()))
    {

        // if we have a valid source texture
        if(d->source&&d->source->isTextureProvider()&&d->source->textureProvider()->texture())
        {

            // if we don't already have a texture provider
            if(!d->textureProvider)
            {
                // cast away the const
                QuickTextureViewPrivate *dd = const_cast<QuickTextureViewPrivate *>(d);

                // create a new texture provider
                dd->textureProvider = new QuickSGTextureViewTextureProvider;

                // enable cleanup when scene graph is invalidated
                connect(window(), &QQuickWindow::sceneGraphInvalidated, d, &QuickTextureViewPrivate::onSceneGraphInvalidated, Qt::DirectConnection);

                // Get the texture
                QuickSGTextureViewTexture *texture = static_cast<QuickSGTextureViewTexture *>(dd->textureProvider->texture());

                // Apply properties to the texture
                texture->setTarget(d->target);
                texture->setFormat(d->format);
                texture->setMinLevel(d->minimumLevel);
                texture->setNumLevels(d->levels);
                texture->setMinLayer(d->minimumLayer);
                texture->setNumLayers(d->layers);

                // set the source provider
                dd->textureProvider->setSourceProvider(d->source->textureProvider());

                // Add a record of the owner QQuickItem to the texture for debug message context
                texture->setProperty("QuickTextureView", QVariant::fromValue(const_cast<QObject *>(qobject_cast<const QObject *>(this))));

            }

            return  d->textureProvider;
        }
    }
    else
    {
        qWarning("QuickTextureView::textureProvider: can only be queried on the rendering thread of an exposed window");
    }

    return nullptr;
}

void QuickTextureView::setMinimumLevel(int minimumLevel)
{
    Q_D(QuickTextureView);

    if (d->minimumLevel == minimumLevel)
        return;

    d->minimumLevel = minimumLevel;
    emit minimumLevelChanged(minimumLevel);
    update();
}

void QuickTextureView::setLevels(int levels)
{
    Q_D(QuickTextureView);

    if (d->levels == levels)
        return;

    d->levels = levels;
    emit levelsChanged(levels);
    update();
}

void QuickTextureView::setMinimumLayer(int minimumLayer)
{
    Q_D(QuickTextureView);

    if (d->minimumLayer == minimumLayer)
        return;

    d->minimumLayer = minimumLayer;
    emit minimumLayerChanged(minimumLayer);
    update();
}

void QuickTextureView::setLayers(int layers)
{
    Q_D(QuickTextureView);

    if (d->layers == layers)
        return;

    d->layers = layers;
    emit layersChanged(layers);
    update();
}

void QuickTextureView::setFormat(QuickTextureView::Format format)
{
    Q_D(QuickTextureView);

    if (d->format == format)
        return;

    d->format = format;
    emit formatChanged(format);
    update();
}

QSGNode *QuickTextureView::updatePaintNode(QSGNode *, QQuickItem::UpdatePaintNodeData *)
{
    Q_D(QuickTextureView);

    if(d->textureProvider)
    {
        // Get the texture
        QuickSGTextureViewTexture *texture = static_cast<QuickSGTextureViewTexture *>(d->textureProvider->texture());

        // Apply properties to the texture
        texture->setTarget(d->target);
        texture->setFormat(d->format);
        texture->setMinLevel(d->minimumLevel);
        texture->setNumLevels(d->levels);
        texture->setMinLayer(d->minimumLayer);
        texture->setNumLayers(d->layers);

        // set the source provider
        d->textureProvider->setSourceProvider(d->source->textureProvider());

    }

    return nullptr;
}

void QuickTextureView::releaseResources()
{
}

#include "quicktextureview.moc"

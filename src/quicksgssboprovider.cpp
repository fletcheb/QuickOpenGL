/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgssboprovider.h"
#include "quicksgssbo.h"
#include <QDebug>
#include <QMetaMethod>
#include <QJSValue>
#include <QVector4D>
#include <QFutureWatcher>
#include "quickopenglthreadpool.h"

class QuickSGSSBOProviderPrivate : public QObject
{
    Q_OBJECT

public:

    QuickSGSSBOProviderPrivate(QuickSGSSBOProvider *q);

    QuickSGSSBO ssbo;
    QMap<uint,  Introspection::ShaderStorageBlock> shaderStorageBlocks;
    QByteArray data;
    int size;
    bool updated;
    bool beingDeleted;
    bool syncFromOpenGL;
    QuickShaderStorageBuffer::SyncWrite syncToOpenGL;
    QuickShaderStorageBuffer::UsagePattern  usagePattern;

public slots:

    void futureFinished();

protected:

    QuickSGSSBOProvider *q_ptr;

    QList <QFutureWatcher<QByteArray> *> watchers;
private:

    Q_DECLARE_PUBLIC(QuickSGSSBOProvider)

};

QuickSGSSBOProviderPrivate::QuickSGSSBOProviderPrivate(QuickSGSSBOProvider *q) :
    size(-1),
    updated(true),
    beingDeleted(false),
    syncFromOpenGL(false),
    syncToOpenGL(QuickShaderStorageBuffer::SyncWriteNever),
    usagePattern(QuickShaderStorageBuffer::UsagePattern::DynamicRead),
    q_ptr(q)
{

}

/*!
 * \internal
 * \brief Runs on  ssbo read future finished
 *
 * Runs in QSGRenderThread.
 */
void QuickSGSSBOProviderPrivate::futureFinished()
{

    Q_Q(QuickSGSSBOProvider);
    if(static_cast<QFutureWatcher<QByteArray> *>(sender())->future().resultCount())
    {
        if(!beingDeleted)
        {
            QByteArray data = static_cast<QFutureWatcher<QByteArray> *>(sender())->result();
            QVariantMap result = Introspection::getShaderStorageBlockVariables(shaderStorageBlocks.first(), data);
            emit q->ssboData(data);
            emit q->ssboVariables(result);
        }

        delete sender();

        // assume that the watchers are processed in sequential order
        watchers.removeFirst();
    }

}

QuickSGSSBOProvider::QuickSGSSBOProvider() :
    d_ptr(new QuickSGSSBOProviderPrivate(this))
{

}

QuickSGSSBOProvider::~QuickSGSSBOProvider()
{

    Q_D(QuickSGSSBOProvider);

    d->beingDeleted = true;

    // burn down the list of watchers before we delete ourselves
    while(d->watchers.count())
    {
        d->watchers.first()->waitForFinished();
        QCoreApplication::processEvents();
    }
}

void QuickSGSSBOProvider::setData(const QByteArray &data)
{
    Q_D(QuickSGSSBOProvider);
    d->data = data;
}

QByteArray QuickSGSSBOProvider::data() const
{
    Q_D(const QuickSGSSBOProvider);
    return d->data;

}

QuickSGSSBO *QuickSGSSBOProvider::ssbo()
{
    Q_D(QuickSGSSBOProvider);

    return &d->ssbo;
}


bool QuickSGSSBOProvider::updated() const
{
    Q_D(const QuickSGSSBOProvider);

    return d->updated;
}

void QuickSGSSBOProvider::setSyncToOpenGL(QuickShaderStorageBuffer::SyncWrite syncToOpenGL)
{
    Q_D(QuickSGSSBOProvider);
    d->syncToOpenGL = syncToOpenGL;
}

QuickShaderStorageBuffer::SyncWrite QuickSGSSBOProvider::syncToOpenGL() const
{
    Q_D(const QuickSGSSBOProvider);
    return d->syncToOpenGL;
}

void QuickSGSSBOProvider::setSyncFromOpenGL(bool syncFromOpenGL)
{
    Q_D(QuickSGSSBOProvider);
    d->syncFromOpenGL = syncFromOpenGL;

}

bool QuickSGSSBOProvider::syncFromOpenGL() const
{
    Q_D(const QuickSGSSBOProvider);
    return d->syncFromOpenGL;
}

void QuickSGSSBOProvider::setSize(int size)
{
    Q_D(QuickSGSSBOProvider);
    d->size = size;
}

int QuickSGSSBOProvider::size() const
{
    Q_D(const QuickSGSSBOProvider);
    return d->size;
}

void QuickSGSSBOProvider::setUsagePattern(QuickShaderStorageBuffer::UsagePattern usagePattern)
{
    Q_D(QuickSGSSBOProvider);
    d->usagePattern = usagePattern;

}

QuickShaderStorageBuffer::UsagePattern QuickSGSSBOProvider::usagePattern() const
{
    Q_D(const QuickSGSSBOProvider);
    return d->usagePattern;
}

void QuickSGSSBOProvider::setUpdated(bool updated)
{
    Q_D(QuickSGSSBOProvider);

    if(updated&&(d->updated!=updated))
    {
        emit ssboChanged();
    }
    d->updated = updated;
}

bool QuickSGSSBOProvider::setShaderStorageBlock(const Introspection::ShaderStorageBlock &shaderStorageBlock)
{
    Q_D(QuickSGSSBOProvider);

    bool result = true;

    // add the shader storage block for this program to the map table
    d->shaderStorageBlocks[shaderStorageBlock.programId] = shaderStorageBlock;

    // compare against the first added uniform block
    for(int i=1;(i<d->shaderStorageBlocks.count())&&result;++i)
    {
        result &= d->shaderStorageBlocks.values().at(0)==d->shaderStorageBlocks.values().at(i);
    }

    return result;
}

Introspection::ShaderStorageBlock QuickSGSSBOProvider::shaderStorageBlock() const
{
    Q_D(const QuickSGSSBOProvider);

    return d->shaderStorageBlocks.count()?d->shaderStorageBlocks.first():Introspection::ShaderStorageBlock();
}


/*!
 * \internal
 * \brief Called by QQuickWindow::afterRendering
 *
 * Runs in QSGRenderThread.  Reads asynchronously from ssbo.
 */
void QuickSGSSBOProvider::onAfterRendering()
{
    Q_D(QuickSGSSBOProvider);

    d->ssbo.setSynced(false);

    if(d->syncFromOpenGL&&d->ssbo.size())
    {
        QFutureWatcher<QByteArray> *watcher = new QFutureWatcher<QByteArray>;
        connect(watcher, &QFutureWatcherBase::finished, d, &QuickSGSSBOProviderPrivate::futureFinished, Qt::DirectConnection);
        watcher->setFuture(d->ssbo.getBufferData());
        d->watchers.append(watcher);
    }
}


#include "quicksgssboprovider.moc"


/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKSHADERSTORAGEBUFFEROBJECT_H
#define QUICKSHADERSTORAGEBUFFEROBJECT_H

#include <QQuickItem>
#include <QScopedPointer>

class QuickSGSSBOProvider;
class QuickShaderStorageBufferPrivate;

class QuickShaderStorageBuffer : public QQuickItem
{
    Q_OBJECT


    Q_PROPERTY(UsagePattern usagePattern READ usagePattern WRITE setUsagePattern NOTIFY usagePatternChanged)
    Q_PROPERTY(SyncWrite syncToOpenGL READ syncToOpenGL WRITE setSyncToOpenGL NOTIFY syncToOpenGLChanged)
    Q_PROPERTY(bool syncFromOpenGL READ syncFromOpenGL WRITE setSyncFromOpenGL NOTIFY syncFromOpenGLChanged)
    Q_PROPERTY(int bufferObject READ bufferObject NOTIFY bufferObjectChanged)
    Q_PROPERTY(int size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(QVariant data READ data WRITE setData NOTIFY dataChanged)

public:

    enum SyncWrite
    {
      SyncWriteNever,
      SyncWriteOnce,
      SyncWriteOnUpdated,
      SyncWriteAlways,
      SyncWriteZeroAlways
    };
    Q_ENUM(SyncWrite)

    /*!
     * \brief OpenGL buffer usage patterns
     */
    enum UsagePattern
    {
        StreamDraw          = 0x88E0, // GL_STREAM_DRAW
        StreamRead          = 0x88E1, // GL_STREAM_READ
        StreamCopy          = 0x88E2, // GL_STREAM_COPY
        StaticDraw          = 0x88E4, // GL_STATIC_DRAW
        StaticRead          = 0x88E5, // GL_STATIC_READ
        StaticCopy          = 0x88E6, // GL_STATIC_COPY
        DynamicDraw         = 0x88E8, // GL_DYNAMIC_DRAW
        DynamicRead         = 0x88E9, // GL_DYNAMIC_READ
        DynamicCopy         = 0x88EA  // GL_DYNAMIC_COPY
    };
    Q_ENUM(UsagePattern)


    QuickShaderStorageBuffer(QQuickItem *parent = 0);

    ~QuickShaderStorageBuffer();
    UsagePattern usagePattern() const;

    SyncWrite syncToOpenGL() const;

    bool syncFromOpenGL() const;

    int size() const;

    QuickSGSSBOProvider *ssboProvider();

    virtual void componentComplete() override;

    int bufferObject() const;

    QVariant data() const;

signals:

    void usagePatternChanged(UsagePattern usagePattern);

    void syncToOpenGLChanged(SyncWrite syncToOpenGL);

    void syncFromOpenGLChanged(bool syncFromOpenGL);

    void sizeChanged(int size);

    void bufferObjectChanged(int bufferObject);

    void dataChanged(QVariant data);

public slots:

    void setUsagePattern(UsagePattern usagePattern);

    void setSyncToOpenGL(SyncWrite syncToOpenGL);

    void setSyncFromOpenGL(bool syncFromOpenGL);

    void setSize(int size);

    void setData(QVariant data);

protected:

    QScopedPointer<QuickShaderStorageBufferPrivate> d_ptr;

    void releaseResources() override;

private:

    Q_DECLARE_PRIVATE(QuickShaderStorageBuffer)
};

#endif // SHADERSTORAGEBUFFEROBJECT_H

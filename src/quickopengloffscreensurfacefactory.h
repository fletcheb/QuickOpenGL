#ifndef QUICKOPENGLOFFSCREENSURFACEFACTORY_H
#define QUICKOPENGLOFFSCREENSURFACEFACTORY_H

#include <QObject>
#include <QOpenGLContext>
#include <QOffscreenSurface>
#include <QGuiApplication>
#include <QDebug>
#include <QMutex>

class QuickOpenGLOffscreenSurfaceFactory: public QObject
{
    Q_OBJECT

public:

    static QuickOpenGLOffscreenSurfaceFactory *instance();

    QuickOpenGLOffscreenSurfaceFactory(QObject *parent = nullptr);

    ~QuickOpenGLOffscreenSurfaceFactory();

    QOffscreenSurface *getSurface();

signals:

    void createSurface();

protected slots:

    void onCreateSurface();

protected:

    QList<QOffscreenSurface *> surfaces;

    QMutex mutex;

};

#endif // QUICKOPENGLOFFSCREENSURFACEFACTORY_H

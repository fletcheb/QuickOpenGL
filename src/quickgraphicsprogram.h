/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKGRAPHICSPROGRAM_H
#define QUICKGRAPHICSPROGRAM_H

#include <QQuickItem>
#include <QScopedPointer>
#include <QOpenGLContext>

class QuickGraphicsProgramPrivate;

class QuickGraphicsProgram : public QQuickItem
{
    Q_OBJECT

    // Shaders
    Q_PROPERTY(QByteArray vertexShader READ vertexShader WRITE setVertexShader NOTIFY vertexShaderChanged)
    Q_PROPERTY(QByteArray tessellationControlShader READ tessellationControlShader WRITE setTessellationControlShader NOTIFY tessellationControlShaderChanged)
    Q_PROPERTY(QByteArray tessellationEvaluationShader READ tessellationEvaluationShader WRITE setTessellationEvaluationShader NOTIFY tessellationEvaluationShaderChanged)
    Q_PROPERTY(QByteArray geometryShader READ geometryShader WRITE setGeometryShader NOTIFY geometryShaderChanged)
    Q_PROPERTY(QByteArray fragmentShader READ fragmentShader WRITE setFragmentShader NOTIFY fragmentShaderChanged)

    // Introspected values
    Q_PROPERTY(QVariantMap attributes READ attributes NOTIFY attributesChanged)
    Q_PROPERTY(QVariantMap uniforms READ uniforms NOTIFY uniformsChanged)
    Q_PROPERTY(QVariantMap subroutineUniforms READ subroutineUniforms NOTIFY subroutineUniformsChanged)
    Q_PROPERTY(QVariantMap uniformBlocks READ uniformBlocks NOTIFY uniformBlocksChanged)
    Q_PROPERTY(QVariantMap shaderStorageBlocks READ shaderStorageBlocks NOTIFY shaderStorageBlocksChanged)

    // Geometry
    Q_PROPERTY(QVariant vertices READ vertices WRITE setVertices NOTIFY verticesChanged)
    Q_PROPERTY(UsagePattern vertexUsagePattern READ vertexUsagePattern WRITE setVertexUsagePattern NOTIFY vertexUsagePatternChanged)
    Q_PROPERTY(DrawingMode drawingMode READ drawingMode WRITE setDrawingMode NOTIFY drawingModeChanged)

    // Points
    Q_PROPERTY(double pointSize READ pointSize WRITE setPointSize NOTIFY pointSizeChanged)
    Q_PROPERTY(bool programPointSize READ programPointSize WRITE setProgramPointSize NOTIFY programPointSizeChanged)
    Q_PROPERTY(CoordOrigin pointSpriteCoordOrigin READ pointSpriteCoordOrigin WRITE setPointSpriteCoordOrigin NOTIFY pointSpriteCoordOriginChanged)
    Q_PROPERTY(double pointFadeThresholdSize READ pointFadeThresholdSize WRITE setPointFadeThresholdSize NOTIFY pointFadeThresholdSizeChanged)

    // Lines
    Q_PROPERTY(double lineWidth READ lineWidth WRITE setLineWidth NOTIFY lineWidthChanged)
    Q_PROPERTY(bool lineSmooth READ lineSmooth WRITE setLineSmooth NOTIFY lineSmoothChanged)

    // Polygons
    Q_PROPERTY(CullFace cullFace READ cullFace WRITE setCullFace NOTIFY cullFaceChanged)
    Q_PROPERTY(FrontFace frontFace READ frontFace WRITE setFrontFace NOTIFY frontFaceChanged)
    Q_PROPERTY(PolygonMode polygonMode READ polygonMode WRITE setPolygonMode NOTIFY polygonModeChanged)

    // Patches
    Q_PROPERTY(int patchVertices READ patchVertices WRITE setPatchVertices NOTIFY patchVerticesChanged)
    Q_PROPERTY(QList<double> patchDefaultOuterLevel READ patchDefaultOuterLevel WRITE setPatchDefaultOuterLevel NOTIFY patchDefaultOuterLevelChanged)
    Q_PROPERTY(QList<double> patchDefaultInnerLevel READ patchDefaultInnerLevel WRITE setPatchDefaultInnerLevel NOTIFY patchDefaultInnerLevelChanged)

    // Blending
    Q_PROPERTY(bool blend READ blend WRITE setBlend NOTIFY blendChanged)
    Q_PROPERTY(BlendFunction blendSrcRgb READ blendSrcRgb WRITE setBlendSrcRgb NOTIFY blendSrcRgbChanged)
    Q_PROPERTY(BlendFunction blendDstRgb READ blendDstRgb WRITE setBlendDstRgb NOTIFY blendDstRgbChanged)
    Q_PROPERTY(BlendFunction blendSrcAlpha READ blendSrcAlpha WRITE setBlendSrcAlpha NOTIFY blendSrcAlphaChanged)
    Q_PROPERTY(BlendFunction blendDstAlpha READ blendDstAlpha WRITE setBlendDstAlpha NOTIFY blendDstAlphaChanged)

    // Color logic
    Q_PROPERTY(LogicOp logicOp READ logicOp WRITE setLogicOp NOTIFY logicOpChanged)

    // Memory barrier
    Q_PROPERTY(Barrier memoryBarrier READ memoryBarrier WRITE setMemoryBarrier NOTIFY memoryBarrierChanged)





public:

    //
    // Emumerations
    //

    enum class UsagePattern
    {
        UsagePatternAlwaysUpload = 0,
        UsagePatternStream = 1,
        UsagePatternDynamic = 2,
        UsagePatternStatic = 3
    };
    Q_ENUM(UsagePattern)


    enum class DrawingMode : GLenum
    {
        DrawingModePoints = 0x0000, // GL_POINTS
        DrawingModeLines = 0x0001, // GL_LINES
        DrawingModeLineLoop = 0x0002, // GL_LINE_LOOP
        DrawingModeLineStrip = 0x0003, // GL_LINE_STRIP
        DrawingModeTriangles = 0x0004, // GL_TRIANGLES
        DrawingModeTriangleStrip = 0x0005, // GL_TRIANGLE_STRIP
        DrawingModeTriangleFan = 0x0006, // GL_TRIANGLE_FAN
        DrawingModeQuads = 0x0007, // GL_QUADS
        DrawingModeQuadStrip = 0x0008, // GL_QUAD_STRIP
        DrawingModeLinesAdjacency = 0x000A, //GL_LINES_ADJACENCY
        DrawingModeLineStripAdjacency = 0x000B, //GL_LINE_STRIP_ADJACENCY
        DrawingModeTrianglesAdjacency = 0x000C, //GL_TRIANGLES_ADJACENCY
        DrawingModeTriangleStripAdjacency = 0x000D, //GL_TRIANGLE_STRIP_ADJACENCY
        DrawingModePolygon = 0x0009, // GL_POLYGON
        DrawingModePatches = 0x000E, //GL_PATCHES
    };
    Q_ENUM(DrawingMode)

    enum class CoordOrigin : GLenum
    {
        CoordOriginLowerLeft = 0x8CA1, // GL_LOWER_LEFT
        CoordOriginUpperLeft = 0x8CA2 // GL_UPPER_LEFT
    };
    Q_ENUM(CoordOrigin)

    enum class CullFace : GLenum
    {
        CullFaceNone = 0,
        CullFaceBack = 0x0405, // GL_BACK
        CullFaceFront = 0x0404, // GL_FRONT
        CullFaceFrontAndBack = 0x0408 // GL_FRONT_AND_BACK
    };
    Q_ENUM(CullFace)

    enum class FrontFace : GLenum
    {
        FrontFaceCW = 0x0900, // GL_CW
        FrontFaceCCW = 0x0901 // GL_CCW
    };
    Q_ENUM(FrontFace)

    enum class PolygonMode : GLenum
    {
        PolygonModePoint = 0x1B00, // GL_POINT
        PolygonModeLine = 0x1B01, // GL_LINE
        PolygonModeFill = 0x1B02, // GL_FILL
    };
    Q_ENUM(PolygonMode)

    enum class BlendFunction : GLenum
    {
        BlendFunctionZero = 0, // GL_ZERO
        BlendFunctionOne = 1, // GL_ONE
        BlendFunctionSrcColor = 0x0300, // GL_SRC_COLOR,
        BlendFunctionMinusSrcColor = 0x0301, // GL_ONE_MINUS_SRC_COLOR
        BlendFunctionDstColor = 0x0306, // GL_DST_COLOR
        BlendFunctionOneMinusDstColor = 0x0307, // GL_ONE_MINUS_DST_COLOR,
        BlendFunctionSrcAlpha = 0x0302, // GL_SRC_ALPHA,
        BlendFunctionOneMinusSrcAlpha = 0x0303, // GL_ONE_MINUS_SRC_ALPHA,
        BlendFunctionDstAlpha = 0x0304, // GL_DST_ALPHA,
        BlendFunctionOneMinusDstAlpha = 0x0305, // GL_ONE_MINUS_DST_ALPHA,
        BlendFunctionConstandColor = 0x8001, // GL_CONSTANT_COLOR,
        BlendFunctionOneMinusConstantColor = 0x8002, // GL_ONE_MINUS_CONSTANT_COLOR,
        BlendFunctionConstantAlpha = 0x8003, // GL_CONSTANT_ALPHA,
        BlendFunctionOneMinusConstantAlpha = 0x8004, // GL_ONE_MINUS_CONSTANT_ALPHA,
        BlendFunctionSrcAlphaSaturate = 0x0308, // GL_SRC_ALPHA_SATURATE,
        BlendFunctionSrc1Color = 0x88F9, // GL_SRC1_COLOR,
        BlendFunctionOneMinusSrc1Color = 0x88FA, // GL_ONE_MINUS_SRC1_COLOR,
        BlendFunctionSrc1Alpha = 0x8589, // GL_SRC1_ALPHA,
        BlendFunctionOneMinusSrc1Alpha = 0x88FB, // GL_ONE_MINUS_SRC1_ALPHA
    };
    Q_ENUM(BlendFunction)

    enum class LogicOp : GLenum
    {
        LogicOpNone,
        LogicOpClear = 0x1500, //GL_CLEAR
        LogicOpSet = 0x150F, //GL_SET
        LogicOpCopy = 0x1503, //GL_COPY
        LogicOpCopyInverted = 0x150C, // GL_COPY_INVERTED
        LogicOpNoOp = 0x1505, // GL_NOOP
        LogicOpInvert = 0x150A, // GL_INVERT
        LogicOpAnd = 0x1501, // GL_AND
        LogicOpNand = 0x150E, // GL_NAND
        LogicOpOr = 0x1507, // GL_OR
        LogicOpNor = 0x1508, // GL_NOR
        LogicOpXor = 0x1506, // GL_XOR
        LogicOpEquiv = 0x1509, // GL_EQUIV
        LogicOpAndReverse = 0x1502, // GL_AND_REVERSE
        LogicOpAndInverted = 0x1504, // GL_AND_INVERTED
        LogicOpOrReverse = 0x150B, // GL_OR_REVERSE
        LogicOpOrInverted = 0x150D // GL_OR_INVERTED
    };
    Q_ENUM(LogicOp)

    enum Barrier
    {
        BarrierNone = 0,
        BarrierVertexAttribArray = 0x00000001, // GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT
        BarrierElementArray = 0x00000002, // GL_ELEMENT_ARRAY_BARRIER_BIT
        BarrierUniform = 0x00000004, // GL_UNIFORM_BARRIER_BIT
        BarrierTextureFetch = 0x00000008, // GL_TEXTURE_FETCH_BARRIER_BIT
        BarrierShaderImageAccess = 0x00000020, // GL_SHADER_IMAGE_ACCESS_BARRIER_BIT
        BarrierCommand = 0x00000040, // GL_COMMAND_BARRIER_BIT,
        BarrierPixelBuffer = 0x00000080, // GL_PIXEL_BUFFER_BARRIER_BIT
        BarrierTextureUpdate = 0x00000100, // GL_TEXTURE_UPDATE_BARRIER_BIT
        BarrierBufferUpdate = 0x00000200, // GL_BUFFER_UPDATE_BARRIER_BIT
        BarrierFramebuffer = 0x00000400, // GL_FRAMEBUFFER_BARRIER_BIT
        BarrierTransformFeedback = 0x00000800, // GL_TRANSFORM_FEEDBACK_BARRIER_BIT
        BarrierAtomicCounter = 0x00001000, // GL_ATOMIC_COUNTER_BARRIER_BIT
        BarrierShaderStorage = 0x00002000, // GL_SHADER_STORAGE_BARRIER_BIT
        BarrierClientMappedBuffer = 0x00004000, // GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT
        BarrierQueryBuffer = 0x00008000, // GL_QUERY_BUFFER_BARRIER_BIT
        BarrierAll = 0xFFFFFFFF // GL_ALL_BARRIER_BITS
    };
    Q_ENUM(Barrier)
    Q_DECLARE_FLAGS(Barriers, Barrier)
    Q_FLAG(Barriers)

    QuickGraphicsProgram(QQuickItem *parent = 0);
    ~QuickGraphicsProgram();

    // Shaders
    QByteArray vertexShader() const;
    QByteArray tessellationControlShader() const;
    QByteArray tessellationEvaluationShader() const;
    QByteArray geometryShader() const;
    QByteArray fragmentShader() const;

    // Geometry
    QVariant vertices() const;
    UsagePattern vertexUsagePattern() const;
    DrawingMode drawingMode() const;

    // Points
    double pointSize() const;
    bool programPointSize() const;
    CoordOrigin pointSpriteCoordOrigin() const;
    double pointFadeThresholdSize() const;

    //Lines
    double lineWidth() const;
    bool lineSmooth() const;

    // Polygons
    CullFace cullFace() const;
    FrontFace frontFace() const;
    PolygonMode polygonMode() const;

    // Patches
    int patchVertices() const;
    QList<double> patchDefaultOuterLevel() const;
    QList<double> patchDefaultInnerLevel() const;

    // Blending
    bool blend() const;
    BlendFunction blendSrcRgb() const;
    BlendFunction blendDstRgb() const;
    BlendFunction blendSrcAlpha() const;
    BlendFunction blendDstAlpha() const;

    // Color logic
    LogicOp logicOp() const;

    // Memory barrier
    Barrier memoryBarrier() const;

    // Introspected values
    QVariantMap attributes() const;
    QVariantMap uniforms() const;
    QVariantMap subroutineUniforms() const;
    QVariantMap uniformBlocks() const;
    QVariantMap shaderStorageBlocks() const;   

public slots:

    // Shaders
    void setVertexShader(QByteArray vertexShader);
    void setTessellationControlShader(QByteArray tessellationControlShader);
    void setTessellationEvaluationShader(QByteArray tessellationEvaluationShader);
    void setGeometryShader(QByteArray geometryShader);
    void setFragmentShader(QByteArray fragmentShader);

    // Geometry
    void setVertices(QVariant vertices);
    void setVertexUsagePattern(UsagePattern vertexUsagePattern);
    void setDrawingMode(DrawingMode drawingMode);

    // Points
    void setPointSize(double pointSize);
    void setProgramPointSize(bool programPointSize);
    void setPointSpriteCoordOrigin(CoordOrigin pointSpriteCoordOrigin);
    void setPointFadeThresholdSize(double pointFadeThresholdSize);

    // Lines
    void setLineWidth(double lineWidth);
    void setLineSmooth(bool lineSmooth);

    // Polygons
    void setCullFace(CullFace cullFace);
    void setFrontFace(FrontFace frontFace);
    void setPolygonMode(PolygonMode polygonMode);

    // Patches
    void setPatchVertices(int patchVertices);
    void setPatchDefaultOuterLevel(QList<double> patchDefaultOuterLevel);
    void setPatchDefaultInnerLevel(QList<double> patchDefaultInnerLevel);

    // Blending
    void setBlend(bool blend);
    void setBlendSrcRgb(BlendFunction blendSrcRgb);
    void setBlendDstRgb(BlendFunction blendDstRgb);
    void setBlendSrcAlpha(BlendFunction blendSrcAlpha);
    void setBlendDstAlpha(BlendFunction blendDstAlpha);

    // Color logic
    void setLogicOp(LogicOp logicOp);

    // Memory barrier
    void setMemoryBarrier(Barrier memoryBarrier);

signals:

    // Shaders
    void vertexShaderChanged(QByteArray vertexShader);
    void tessellationControlShaderChanged(QByteArray tessellationControlShader);
    void tessellationEvaluationShaderChanged(QByteArray tessellationEvaluationShader);
    void geometryShaderChanged(QByteArray geometryShader);
    void fragmentShaderChanged(QByteArray fragmentShader);

    // Geometry
    void verticesChanged(QVariant vertices);
    void vertexUsagePatternChanged(UsagePattern vertexUsagePattern);
    void drawingModeChanged(DrawingMode drawingMode);

    // Points
    void pointSizeChanged(double pointSize);
    void programPointSizeChanged(bool programPointSize);
    void pointSpriteCoordOriginChanged(CoordOrigin pointSpriteCoordOrigin);
    void pointFadeThresholdSizeChanged(double pointFadeThresholdSize);

    // Lines
    void lineWidthChanged(double lineWidth);
    void lineSmoothChanged(bool lineSmooth);

    // Polygons
    void cullFaceChanged(CullFace cullFace);
    void frontFaceChanged(FrontFace frontFace);
    void polygonModeChanged(PolygonMode polygonMode);

    // Patches
    void patchVerticesChanged(int patchVertices);
    void patchDefaultOuterLevelChanged(QList<double> patchDefaultOuterLevel);
    void patchDefaultInnerLevelChanged(QList<double> patchDefaultInnerLevel);

    // Blending
    void blendChanged(bool blend);
    void blendSrcRgbChanged(BlendFunction blendSrcRgb);
    void blendDstRgbChanged(BlendFunction blendDstRgb);
    void blendSrcAlphaChanged(BlendFunction blendSrcAlpha);
    void blendDstAlphaChanged(BlendFunction blendDstAlpha);

    // Color logic
    void logicOpChanged(LogicOp logicOp);

    // Memory barrier
    void memoryBarrierChanged(Barrier memoryBarrier);

    // Introspected values
    void attributesChanged(QVariantMap attributes);
    void uniformsChanged(QVariantMap uniforms);
    void subroutineUniformsChanged(QVariantMap subroutineUniforms);
    void uniformBlocksChanged(QVariantMap uniformBlocks);
    void shaderStorageBlocksChanged(QVariantMap shaderStorageBlocks);

protected:

    QScopedPointer<QuickGraphicsProgramPrivate> d_ptr;

    // overrides
    QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;


private:

    Q_DECLARE_PRIVATE(QuickGraphicsProgram)    
};


#endif // QUICKGRAPHICSPROGRAM_H

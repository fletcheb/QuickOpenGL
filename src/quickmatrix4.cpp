#include "quickmatrix4.h"

/*!
 * \qmlbasictype mat4
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The mat4 basic type represents a GLSL mat4 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty float mat4::m00
 *
 * This property holds the matrix row 0 column 0 value
 */

/*!
 * \qmlproperty float mat4::m01
 *
 * This property holds the matrix row 0 column 1 value
 */

/*!
 * \qmlproperty float mat4::m02
 *
 * This property holds the matrix row 0 column 2 value
 */

/*!
 * \qmlproperty float mat4::m03
 *
 * This property holds the matrix row 0 column 3 value
 */

/*!
 * \qmlproperty float mat4::m10
 *
 * This property holds the matrix row 1 column 0 value
 */

/*!
 * \qmlproperty float mat4::m11
 *
 * This property holds the matrix row 1 column 1 value
 */

/*!
 * \qmlproperty float mat4::m12
 *
 * This property holds the matrix row 1 column 2 value
 */

/*!
 * \qmlproperty float mat4::m13
 *
 * This property holds the matrix row 1 column 3 value
 */

/*!
 * \qmlproperty float mat4::m20
 *
 * This property holds the matrix row 2 column 0 value
 */

/*!
 * \qmlproperty float mat4::m21
 *
 * This property holds the matrix row 2 column 1 value
 */

/*!
 * \qmlproperty float mat4::m22
 *
 * This property holds the matrix row 2 column 2 value
 */

/*!
 * \qmlproperty float mat4::m23
 *
 * This property holds the matrix row 2 column 3 value
 */

/*!
 * \qmlproperty float mat4::m30
 *
 * This property holds the matrix row 3 column 0 value
 */

/*!
 * \qmlproperty float mat4::m31
 *
 * This property holds the matrix row 3 column 1 value
 */

/*!
 * \qmlproperty float mat4::m32
 *
 * This property holds the matrix row 3 column 2 value
 */

/*!
 * \qmlproperty float mat4::m33
 *
 * This property holds the matrix row 3 column 3 value
 */

/*!
 * \qmlproperty vec4 mat4::r0
 *
 * This property holds the matrix row 0 vector
 */

/*!
 * \qmlproperty vec4 mat4::r1
 *
 * This property holds the matrix row 1 vector
 */

/*!
 * \qmlproperty vec4 mat4::r2
 *
 * This property holds the matrix row 2 vector
 */

/*!
 * \qmlproperty vec4 mat4::r3
 *
 * This property holds the matrix row 3 vector
 */

/*!
 * \qmlproperty vec4 mat4::c0
 *
 * This property holds the matrix column 0 vector
 */

/*!
 * \qmlproperty vec4 mat4::c1
 *
 * This property holds the matrix column 1 vector
 */

/*!
 * \qmlproperty vec4 mat4::c2
 *
 * This property holds the matrix column 2 vector
 */

/*!
 * \qmlproperty vec4 mat4::c3
 *
 * This property holds the matrix column 3 vector
 */

/*!
 * \qmlmethod mat4 mat4::times(mat4 other)
 * \param other the other matrix
 * \return The result of multiplying this matrix with the other matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} times method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod vec4 mat4::times(vec4 vector)
 * \param vector the vector
 * \return the vec4 result of transforming the vector with this matrix applied pre-vector.
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} times method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod vec3 mat4::times(vec3 vector)
 * \param vector the vector
 * \return the vec3 result of transforming the vector with this matrix applied pre-vector
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} times method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod mat4 mat4::times(float factor)
 * \param factor the factor
 * \return the result of multiplying this matrix with the scalar factor
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} times method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod mat4 mat4::plus(mat4 other)
 * \param other the other matrix
 * \return the result of the addition of this matrix with the other matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} plus method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod mat4 mat4::minus(mat4 &other)
 * \param other the other matrix
 * \return the result of the subtraction of the other matrix from this matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} minus method.
 * \sa matrix4x4
 */

/*!
 * \qmlmethod float mat4::determinant()
 * \return the determinant of the matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} determinant method.
 * \sa matrix4x4
 */

/*!
 * \qmlmethod mat4 mat4::invert()
 * \return the inverse of this matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} inverted method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod mat4 mat4::transpose()
 * \return the transpose of this matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} transposed method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod mat4 mat4::frustum(float left, float right, float bottom, float top, float nearPlane, float farPlane)
 * \param left left edge of window
 * \param right right edge of window
 * \param bottom bottom edge of window
 * \param top top edge of window
 * \param nearPlane near clippling plane
 * \param farplane far clipping plane
 * \return this matrix multipled by another that applies a perspective frustum projection
 *
 * returns this matrix multipled by another that applies a perspective frustum projection for a
 * window with lower-left corner (left, bottom), upper-right corner (right, top), and the specified
 * nearPlane and farPlane clipping planes.
 *
 * \sa QMatrix4x4::frustum
 */

/*!
 * \qmlmethod mat4 mat4::rotate(float angle, vec3 vector)
 * \param angle the angle to rotate (degrees)
 * \param vector the vector to rotate about
 * \return this matrix multiplied by another that rotates coordinates through angle degrees about vector
 *
 * \sa QMatrix4x4::rotate
 */

/*!
 * \qmlmethod mat4 mat4::translate(vec3 vector)
 * \param vector the vector to translate by
 * \return this matrix multiplied by another that translates coordinates by the components of vector
 *
 * \sa QMatrix4x4::translate
 */

/*!
 * \qmlmethod mat4 mat4::lookAt(vec3 &eye, vec3 center, vec3 up)
 * \param eye the eye location
 * \param center the center of the view that the eye is looking at
 * \param up the up direction with respect to the eye
 * \return this matrix multiplied by a viewing matrix
 *
 * Multiplies this matrix by a viewing matrix derived from an eye point. The center value indicates
 * the center of the view that the eye is looking at. The up value indicates which direction should
 * be considered up with respect to the eye.
 *
 * \sa QMatrix4x4::lookAt
 */

/*!
 * \qmlmethod mat4 mat4::scale(vec3 vector)
 * \param vector the vector to scale by
 * \return this matrix multiplied by another that scales coordinates by the components of vector
 *
 * \sa QMatrix4x4::scale
 */



/*!
 * \qmlbasictype dmat4
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The dmat4 basic type represents a GLSL dmat4 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty double dmat4::m00
 *
 * This property holds the matrix row 0 column 0 value
 */

/*!
 * \qmlproperty double dmat4::m01
 *
 * This property holds the matrix row 0 column 1 value
 */

/*!
 * \qmlproperty double dmat4::m02
 *
 * This property holds the matrix row 0 column 2 value
 */

/*!
 * \qmlproperty double dmat4::m03
 *
 * This property holds the matrix row 0 column 3 value
 */

/*!
 * \qmlproperty double dmat4::m10
 *
 * This property holds the matrix row 1 column 0 value
 */

/*!
 * \qmlproperty double dmat4::m11
 *
 * This property holds the matrix row 1 column 1 value
 */

/*!
 * \qmlproperty double dmat4::m12
 *
 * This property holds the matrix row 1 column 2 value
 */

/*!
 * \qmlproperty double dmat4::m13
 *
 * This property holds the matrix row 1 column 3 value
 */

/*!
 * \qmlproperty double dmat4::m20
 *
 * This property holds the matrix row 2 column 0 value
 */

/*!
 * \qmlproperty double dmat4::m21
 *
 * This property holds the matrix row 2 column 1 value
 */

/*!
 * \qmlproperty double dmat4::m22
 *
 * This property holds the matrix row 2 column 2 value
 */

/*!
 * \qmlproperty double dmat4::m23
 *
 * This property holds the matrix row 2 column 3 value
 */

/*!
 * \qmlproperty double dmat4::m30
 *
 * This property holds the matrix row 3 column 0 value
 */

/*!
 * \qmlproperty double dmat4::m31
 *
 * This property holds the matrix row 3 column 1 value
 */

/*!
 * \qmlproperty double dmat4::m32
 *
 * This property holds the matrix row 3 column 2 value
 */

/*!
 * \qmlproperty double dmat4::m33
 *
 * This property holds the matrix row 3 column 3 value
 */

/*!
 * \qmlproperty dvec4 dmat4::r0
 *
 * This property holds the matrix row 0 vector
 */

/*!
 * \qmlproperty dvec4 dmat4::r1
 *
 * This property holds the matrix row 1 vector
 */

/*!
 * \qmlproperty dvec4 dmat4::r2
 *
 * This property holds the matrix row 2 vector
 */

/*!
 * \qmlproperty dvec4 dmat4::r3
 *
 * This property holds the matrix row 3 vector
 */

/*!
 * \qmlproperty dvec4 dmat4::c0
 *
 * This property holds the matrix column 0 vector
 */

/*!
 * \qmlproperty dvec4 dmat4::c1
 *
 * This property holds the matrix column 1 vector
 */

/*!
 * \qmlproperty dvec4 dmat4::c2
 *
 * This property holds the matrix column 2 vector
 */

/*!
 * \qmlproperty dvec4 dmat4::c3
 *
 * This property holds the matrix column 3 vector
 */

/*!
 * \qmlmethod dmat4 dmat4::times(dmat4 other)
 * \param other the other matrix
 * \return The result of multiplying this matrix with the other matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} times method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod dvec4 dmat4::times(dvec4 vector)
 * \param vector the vector
 * \return the dvec4 result of transforming the vector with this matrix applied pre-vector.
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} times method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod dvec3 dmat4::times(dvec3 vector)
 * \param vector the vector
 * \return the dvec3 result of transforming the vector with this matrix applied pre-vector
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} times method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod dmat4 dmat4::times(double factor)
 * \param factor the factor
 * \return the result of multiplying this matrix with the scalar factor
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} times method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod dmat4 dmat4::plus(dmat4 other)
 * \param other the other matrix
 * \return the result of the addition of this matrix with the other matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} plus method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod dmat4 dmat4::minus(dmat4 &other)
 * \param other the other matrix
 * \return the result of the subtraction of the other matrix from this matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} minus method.
 * \sa matrix4x4
 */

/*!
 * \qmlmethod double dmat4::determinant()
 * \return the determinant of the matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} determinant method.
 * \sa matrix4x4
 */

/*!
 * \qmlmethod dmat4 dmat4::invert()
 * \return the inverse of this matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} inverted method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod dmat4 dmat4::transpose()
 * \return the transpose of this matrix
 *
 * This method performs the same function as the equivalent QML \l{matrix4x4} transposed method.
 *
 * \sa matrix4x4
 */

/*!
 * \qmlmethod dmat4 dmat4::frustum(double left, double right, double bottom, double top, double nearPlane, double farPlane)
 * \param left left edge of window
 * \param right right edge of window
 * \param bottom bottom edge of window
 * \param top top edge of window
 * \param nearPlane near clippling plane
 * \param farplane far clipping plane
 * \return this matrix multipled by another that applies a perspective frustum projection
 *
 * returns this matrix multipled by another that applies a perspective frustum projection for a
 * window with lower-left corner (left, bottom), upper-right corner (right, top), and the specified
 * nearPlane and farPlane clipping planes.
 *
 * \sa QMatrix4x4::frustum
 */

/*!
 * \qmlmethod dmat4 dmat4::rotate(double angle, dvec3 vector)
 * \param angle the angle to rotate (degrees)
 * \param vector the vector to rotate about
 * \return this matrix multiplied by another that rotates coordinates through angle degrees about vector
 *
 * \sa QMatrix4x4::rotate
 */

/*!
 * \qmlmethod dmat4 dmat4::translate(dvec3 vector)
 * \param vector the vector to translate by
 * \return this matrix multiplied by another that translates coordinates by the components of vector
 *
 * \sa QMatrix4x4::translate
 */

/*!
 * \qmlmethod dmat4 dmat4::lookAt(dvec3 &eye, dvec3 center, dvec3 up)
 * \param eye the eye location
 * \param center the center of the view that the eye is looking at
 * \param up the up direction with respect to the eye
 * \return this matrix multiplied by a viewing matrix
 *
 * Multiplies this matrix by a viewing matrix derived from an eye point. The center value indicates
 * the center of the view that the eye is looking at. The up value indicates which direction should
 * be considered up with respect to the eye.
 *
 * \sa QMatrix4x4::lookAt
 */

/*!
 * \qmlmethod dmat4 dmat4::scale(dvec3 vector)
 * \param vector the vector to scale by
 * \return this matrix multiplied by another that scales coordinates by the components of vector
 *
 * \sa QMatrix4x4::scale
 */


static inline double matrixDet2(const qdmat4 &m, int col0, int col1, int row0, int row1)
{
    return m(col0,row0) * m(col1,row1) - m(col0,row1) * m(col1,row0);
}

static inline double matrixDet3(const qdmat4 &m, int col0, int col1, int col2, int row0, int row1, int row2)
{
    return m(col0,row0) * matrixDet2(m, col1, col2, row1, row2)
            - m(col1,row0) * matrixDet2(m, col0, col2, row1, row2)
            + m(col2,row0) * matrixDet2(m, col0, col1, row1, row2);
}

static inline double matrixDet4(const qdmat4 &m)
{
    double det;
    det  = m(0,0) * matrixDet3(m, 1, 2, 3, 1, 2, 3);
    det -= m(1,0) * matrixDet3(m, 0, 2, 3, 1, 2, 3);
    det += m(2,0) * matrixDet3(m, 0, 1, 3, 1, 2, 3);
    det -= m(3,0) * matrixDet3(m, 0, 1, 2, 1, 2, 3);
    return det;
}


//
// End from Qt qmatrix4x4.cpp
//

qdmat4::operator qmat4() const
{
    qmat4 result;
    for(int i=0;i<16;++i)
    {
        result.data()[i] = float(constData()[i]);
    }

    return result;
}

qdmat4 qdmat4::times(const qdmat4 &other) const
{
    return *this*other;
}

qdmat4 qdmat4::transpose() const
{
    return QGenericMatrix<4,4,double>::transposed();
}

qdmat4 qdmat4::frustum(double left, double right, double bottom, double top, double nearPlane, double farPlane) const
{
    return matrix4<double, qdvec4>::frustum(left, right, bottom, top, nearPlane, farPlane);
}

qdmat4 qdmat4::rotate(double angle, const qdvec3 &vector) const
{
    return matrix4<double, qdvec4>::rotate(angle, vector);
}

qdmat4 qdmat4::translate(const qdvec3 &vector) const
{
    return matrix4<double, qdvec4>::translate(vector);
}

qdmat4 qdmat4::lookAt(const qdvec3 &eye, const qdvec3 &center, const qdvec3 &up) const
{
    return matrix4<double, qdvec4>::lookAt(eye, center, up);
}

qdmat4 qdmat4::scale(const qdvec3 &vector) const
{
    return matrix4<double, qdvec4>::scale(vector);
}

qdvec4 qdmat4::times(const qdvec4 &other) const
{
    return *this*other;
}

qdvec3 qdmat4::times(const qdvec3 &vector) const
{
    return *this*vector;
}

qdmat4 qdmat4::times(double factor) const
{
    return *this*factor;
}

qdmat4 qdmat4::plus(const qdmat4 &other) const
{
    return *this + other;
}

qdmat4 qdmat4::minus(const qdmat4 &other) const
{
    return *this - other;
}

double qdmat4::determinant() const
{
    return matrixDet4(*this);
}

qdmat4 qdmat4::invert() const
{
    double invdet = 1.0/matrixDet4(*this);
    qdmat4 inv;
    inv(0, 0) =  matrixDet3(*this, 1, 2, 3, 1, 2, 3) * invdet;
    inv(0, 1) = -matrixDet3(*this, 0, 2, 3, 1, 2, 3) * invdet;
    inv(0, 2) =  matrixDet3(*this, 0, 1, 3, 1, 2, 3) * invdet;
    inv(0, 3) = -matrixDet3(*this, 0, 1, 2, 1, 2, 3) * invdet;
    inv(1, 0) = -matrixDet3(*this, 1, 2, 3, 0, 2, 3) * invdet;
    inv(1, 1) =  matrixDet3(*this, 0, 2, 3, 0, 2, 3) * invdet;
    inv(1, 2) = -matrixDet3(*this, 0, 1, 3, 0, 2, 3) * invdet;
    inv(1, 3) =  matrixDet3(*this, 0, 1, 2, 0, 2, 3) * invdet;
    inv(2, 0) =  matrixDet3(*this, 1, 2, 3, 0, 1, 3) * invdet;
    inv(2, 1) = -matrixDet3(*this, 0, 2, 3, 0, 1, 3) * invdet;
    inv(2, 2) =  matrixDet3(*this, 0, 1, 3, 0, 1, 3) * invdet;
    inv(2, 3) = -matrixDet3(*this, 0, 1, 2, 0, 1, 3) * invdet;
    inv(3, 0) = -matrixDet3(*this, 1, 2, 3, 0, 1, 2) * invdet;
    inv(3, 1) =  matrixDet3(*this, 0, 2, 3, 0, 1, 2) * invdet;
    inv(3, 2) = -matrixDet3(*this, 0, 1, 3, 0, 1, 2) * invdet;
    inv(3, 3) =  matrixDet3(*this, 0, 1, 2, 0, 1, 2) * invdet;
    return inv;
}

qmat4::operator qdmat4() const
{
    qdmat4 result;
    for(int i=0;i<16;++i)
    {
        result.data()[i] = double(constData()[i]);
    }

    return result;
}

qmat4 qmat4::times(const qmat4 &other) const
{
    return *this*other;
}

qvec4 qmat4::times(const qvec4 &vector) const
{
    return *this*vector;
}

qvec3 qmat4::times(const qvec3 &vector) const
{
    return *this*vector;
}

qmat4 qmat4::times(float factor) const
{
    return *this*factor;
}

qmat4 qmat4::plus(const qmat4 &other) const
{
    return *this+other;
}

qmat4 qmat4::minus(const qmat4 &other) const
{
    return *this-other;
}

float qmat4::determinant() const
{
    qdmat4 m;
    for(int i=0;i<16;++i)
    {
        m.data()[i] = double(constData()[i]);
    }

    return float(matrixDet4(m));
}

qmat4 qmat4::invert() const
{
    qdmat4 m;
    for(int i=0;i<16;++i)
    {
        m.data()[i] = double(constData()[i]);
    }

    m = m.invert();

    qmat4 result;
    for(int i=0;i<16;++i)
    {
        result.data()[i] = float(m.constData()[i]);
    }

    return result;

}

qmat4 qmat4::transpose() const
{
    return QGenericMatrix<4,4,float>::transposed();
}

qmat4 qmat4::frustum(float left, float right, float bottom, float top, float nearPlane, float farPlane) const
{
    return matrix4<float, qvec4>::frustum(left, right, bottom, top, nearPlane, farPlane);
}

qmat4 qmat4::rotate(float angle, const qvec3 &vector) const
{
    return matrix4<float, qvec4>::rotate(angle, vector);
}

qmat4 qmat4::translate(const qvec3 &vector) const
{
    return matrix4<float, qvec4>::translate(vector);
}

qmat4 qmat4::lookAt(const qvec3 &eye, const qvec3 &center, const qvec3 &up) const
{
    return matrix4<float, qvec4>::lookAt(eye, center, up);
}

qmat4 qmat4::scale(const qvec3 &vector) const
{
    return matrix4<float, qvec4>::scale(vector);
}


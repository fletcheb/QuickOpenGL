/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgtextureprovider.h"
#include "quickbuffer.h"

class QuickSGTextureProviderPrivate : public QObject
{
    Q_OBJECT

public:

    QuickSGTextureProviderPrivate(QuickSGTextureProvider *q);

    QuickSGTexture texture;
    bool syncFromOpenGL;
    bool textureChanged;
    QList<QFutureWatcher<QList<QByteArray>>*> watchers;
    bool beingDeleted;

public slots:

    void onTextureChanged();
    void onFutureFinished();

protected:

    QuickSGTextureProvider *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickSGTextureProvider)
};


QuickSGTextureProviderPrivate::QuickSGTextureProviderPrivate(QuickSGTextureProvider *q):
    syncFromOpenGL(false),
    textureChanged(false),
    beingDeleted(false),
    q_ptr(q)
{

}

QuickSGTextureProvider::QuickSGTextureProvider() :
    d_ptr(new QuickSGTextureProviderPrivate(this))
{
    Q_D(QuickSGTextureProvider);

    connect(&d->texture, &QuickSGTexture::targetChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::formatChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::widthChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::heightChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::depthChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::layersChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::imageLayerChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::imageAccessChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::mipLevelsChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::wrapSChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::wrapTChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::wrapRChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::magFilterChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::minFilterChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::lodBiasChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::maxLodChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::minLodChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::borderColorChanged, this, &QuickSGTextureProvider::textureChanged);
    connect(&d->texture, &QuickSGTexture::imageLevelChanged, this, &QuickSGTextureProvider::textureChanged);

    connect(this, &QuickSGTextureProvider::textureChanged, d, &QuickSGTextureProviderPrivate::onTextureChanged);
}

QuickSGTextureProvider::~QuickSGTextureProvider()
{
    Q_D(QuickSGTextureProvider);

    d->beingDeleted = true;

    // burn down the list of watchers before we delete ourselves
    while(d->watchers.count())
    {
        d->watchers.first()->waitForFinished();
        QCoreApplication::processEvents();
    }
}

QSGTexture *QuickSGTextureProvider::texture() const
{
    Q_D(const QuickSGTextureProvider);

    return const_cast<QuickSGTexture *>(&d->texture);
}

bool QuickSGTextureProvider::syncFromOpenGL() const
{
    Q_D(const QuickSGTextureProvider);

    return d->syncFromOpenGL;
}

void QuickSGTextureProviderPrivate::onTextureChanged()
{
    textureChanged = true;
}

void QuickSGTextureProvider::onAfterRendering()
{
    Q_D(QuickSGTextureProvider);

    if(d->syncFromOpenGL&&d->textureChanged)
    {
        QFutureWatcher<QList<QByteArray>> *watcher = new QFutureWatcher<QList<QByteArray>>;

        connect(watcher, &QFutureWatcherBase::finished, d, &QuickSGTextureProviderPrivate::onFutureFinished, Qt::DirectConnection);
        {
            watcher->setFuture(d->texture.getTextureData());
            d->watchers.append(watcher);
        }

        d->textureChanged=false;
    }

}

void QuickSGTextureProviderPrivate::onFutureFinished()
{

    Q_Q(QuickSGTextureProvider);

    if(static_cast<QFutureWatcher<QList<QByteArray>> *>(sender())->future().resultCount())
    {
        if(!beingDeleted)
        {
            QList<QByteArray> data = static_cast<QFutureWatcher<QList<QByteArray>> *>(sender())->result();

            QVariantList result;
            for(const QByteArray &buffer: data)
            {
                result.append(QVariant::fromValue(QuickBuffer(buffer)));
            }

            emit q->textureDataChanged(QVariant::fromValue(result));
        }

        delete sender();

        // assume that watchers are processed in sequential order
        watchers.removeFirst();

    }

}

void QuickSGTextureProvider::setSyncFromOpenGL(bool syncFromOpenGL)
{
    Q_D(QuickSGTextureProvider);

    if (d->syncFromOpenGL == syncFromOpenGL)
        return;

    d->syncFromOpenGL = syncFromOpenGL;
    emit syncFromOpenGLChanged(d->syncFromOpenGL);
}



#include "quicksgtextureprovider.moc"

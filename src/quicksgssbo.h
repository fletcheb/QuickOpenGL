/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKSHADERSTORAGEBUFFEROBJECTBUFFER_H
#define QUICKSHADERSTORAGEBUFFEROBJECTBUFFER_H

#include <QObject>
#include <QFuture>
#include "support.h"

class QuickSGSSBOPrivate;

class QuickSGSSBO : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(int bufferId READ bufferId NOTIFY bufferIdChanged)
    Q_PROPERTY(int usagePattern READ usagePattern WRITE setUsagePattern NOTIFY usagePatternChanged)
    Q_PROPERTY(bool synced READ synced WRITE setSynced)

public:

    QuickSGSSBO(QObject *parent = nullptr);
    ~QuickSGSSBO();
    uint bufferId() const;
    void bind();
    void release();
    void bindBufferBase(uint bindingIndex);
    void bufferData(const QByteArray &data);
    void bufferData(int size);
    QFuture<QByteArray> getBufferData();
    int size();
    int usagePattern() const;

    bool synced() const;

public slots:

    void setSize(int size);
    void setUsagePattern(GLenum usagePattern);

    void setSynced(bool synced);

signals:

    void bufferIdChanged(uint bufferId);
    void sizeChanged(int size);
    void usagePatternChanged(int usagePattern);

protected:

    QScopedPointer<QuickSGSSBOPrivate> d_ptr;

private:

    Q_DECLARE_PRIVATE(QuickSGSSBO)   
};

#endif // QUICKSHADERSTORAGEBUFFEROBJECTBUFFER_H

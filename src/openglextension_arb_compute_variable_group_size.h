/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#ifndef OPENGLEXTENSION_ARB_COMPUTE_VARIABLE_GROUP_SIZE_H
#define OPENGLEXTENSION_ARB_COMPUTE_VARIABLE_GROUP_SIZE_H

#include <QtOpenGLExtensions/QOpenGLExtensions>

class QOpenGLExtension_ARB_compute_variable_group_sizePrivate;

class QOpenGLExtension_ARB_compute_variable_group_size : public QAbstractOpenGLExtension
{
public:
    QOpenGLExtension_ARB_compute_variable_group_size();

    bool initializeOpenGLFunctions() Q_DECL_FINAL;

    void glDispatchComputeGroupSizeARB(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z, GLuint group_size_x, GLuint group_size_y, GLuint group_size_z);

protected:

    Q_DECLARE_PRIVATE(QOpenGLExtension_ARB_compute_variable_group_size)
};

#endif // OPENGLEXTENSION_ARB_COMPUTE_VARIABLE_GROUP_SIZE_H

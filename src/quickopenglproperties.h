/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKOPENGLPROPERTIES_H
#define QUICKOPENGLPROPERTIES_H

#include <QQuickItem>
#include <QtQml>
#include <QScopedPointer>
#include "quickvector3.h"

class QuickOpenGLPropertiesPrivate;


class QuickOpenGLProperties : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool isOpenGLES READ isOpenGLES CONSTANT FINAL)
    Q_PROPERTY(QStringList extensions READ extensions NOTIFY extensionsChanged)
    Q_PROPERTY(QString vendor READ vendor NOTIFY vendorChanged)
    Q_PROPERTY(QVector<float> glLineWidthRange READ glLineWidthRange NOTIFY glLineWidthRangeChanged)
    Q_PROPERTY(int glMaxComputeShaderStorageBlocks READ glMaxComputeShaderStorageBlocks NOTIFY glMaxComputeShaderStorageBlocksChanged)
    Q_PROPERTY(int glMaxCombinedShaderStorageBlocks READ glMaxCombinedShaderStorageBlocks NOTIFY glMaxCombinedShaderStorageBlocksChanged)
    Q_PROPERTY(int glMaxComputeUniformBlocks READ glMaxComputeUniformBlocks NOTIFY glMaxComputeUniformBlocksChanged)
    Q_PROPERTY(int glMaxComputeTextureImageUnits READ glMaxComputeTextureImageUnits NOTIFY glMaxComputeTextureImageUnitsChanged)
    Q_PROPERTY(int glMaxComputeUniformComponents READ glMaxComputeUniformComponents NOTIFY glMaxComputeUniformComponentsChanged)
    Q_PROPERTY(int glMaxComputeAtomicCounters READ glMaxComputeAtomicCounters NOTIFY glMaxComputeAtomicCountersChanged)
    Q_PROPERTY(int glMaxComputeAtomicCounterBuffers READ glMaxComputeAtomicCounterBuffers NOTIFY glMaxComputeAtomicCounterBuffersChanged)
    Q_PROPERTY(int glMaxCombinedComputeUniformComponents READ glMaxCombinedComputeUniformComponents NOTIFY glMaxCombinedComputeUniformComponentsChanged)
    Q_PROPERTY(int glMaxComputeWorkGroupInvocations READ glMaxComputeWorkGroupInvocations NOTIFY glMaxComputeWorkGroupInvocationsChanged)
#ifdef Q_QDOC
    Q_PROPERTY(ivec3 glMaxComputeWorkGroupCount READ glMaxComputeWorkGroupCount NOTIFY glMaxComputeWorkGroupCountChanged)
    Q_PROPERTY(ivec3 glMaxComputeWorkGroupSize READ glMaxComputeWorkGroupSize NOTIFY glMaxComputeWorkGroupSizeChanged)
#else
    Q_PROPERTY(qivec3 glMaxComputeWorkGroupCount READ glMaxComputeWorkGroupCount NOTIFY glMaxComputeWorkGroupCountChanged)
    Q_PROPERTY(qivec3 glMaxComputeWorkGroupSize READ glMaxComputeWorkGroupSize NOTIFY glMaxComputeWorkGroupSizeChanged)
#endif
    Q_PROPERTY(int glMaxDebugGroupStackDepth READ glMaxDebugGroupStackDepth NOTIFY glMaxDebugGroupStackDepthChanged)
    Q_PROPERTY(int glMajorVersion READ glMajorVersion NOTIFY glMajorVersionChanged)
    Q_PROPERTY(int glMax3DTextureSize READ glMax3DTextureSize NOTIFY glMax3DTextureSizeChanged)
    Q_PROPERTY(int glMaxArrayTextureLayers READ glMaxArrayTextureLayers NOTIFY glMaxArrayTextureLayersChanged)
    Q_PROPERTY(int glMaxClipDistances READ glMaxClipDistances NOTIFY glMaxClipDistancesChanged)
    Q_PROPERTY(int glMaxColorTextureSamples READ glMaxColorTextureSamples NOTIFY glMaxColorTextureSamplesChanged)
    Q_PROPERTY(int glMaxCombinedAtomicCounters READ glMaxCombinedAtomicCounters NOTIFY glMaxCombinedAtomicCountersChanged)
    Q_PROPERTY(int glMaxCombinedFragmentUniformComponents READ glMaxCombinedFragmentUniformComponents NOTIFY glMaxCombinedFragmentUniformComponentsChanged)
    Q_PROPERTY(int glMaxCombinedGeometryUniformComponents READ glMaxCombinedGeometryUniformComponents NOTIFY glMaxCombinedGeometryUniformComponentsChanged)
    Q_PROPERTY(int glMaxCombinedTextureImageUnits READ glMaxCombinedTextureImageUnits NOTIFY glMaxCombinedTextureImageUnitsChanged)
    Q_PROPERTY(int glMaxCombinedUniformBlocks READ glMaxCombinedUniformBlocks NOTIFY glMaxCombinedUniformBlocksChanged)
    Q_PROPERTY(int glMaxCombinedVeretexUniformComponents READ glMaxCombinedVeretexUniformComponents NOTIFY glMaxCombinedVeretexUniformComponentsChanged)
    Q_PROPERTY(int glMaxCubeMapTextureSize READ glMaxCubeMapTextureSize NOTIFY glMaxCubeMapTextureSizeChanged)
    Q_PROPERTY(int glMaxDepthTextureSamples READ glMaxDepthTextureSamples NOTIFY glMaxDepthTextureSamplesChanged)
    Q_PROPERTY(int glMaxDrawBuffers READ glMaxDrawBuffers NOTIFY glMaxDrawBuffersChanged)
    Q_PROPERTY(int glMaxDualSourceDrawBuffers READ glMaxDualSourceDrawBuffers NOTIFY glMaxDualSourceDrawBuffersChanged)
    Q_PROPERTY(int glMaxElementsIndices READ glMaxElementsIndices NOTIFY glMaxElementsIndicesChanged)
    Q_PROPERTY(int glMaxElementsVertices READ glMaxElementsVertices NOTIFY glMaxElementsVerticesChanged)
    Q_PROPERTY(int glMaxFragmentAtomicCounters READ glMaxFragmentAtomicCounters NOTIFY glMaxFragmentAtomicCountersChanged)
    Q_PROPERTY(int glMaxFragmentShaderStorageBlocks READ glMaxFragmentShaderStorageBlocks NOTIFY glMaxFragmentShaderStorageBlocksChanged)
    Q_PROPERTY(int glMaxFragmentInputComponents READ glMaxFragmentInputComponents NOTIFY glMaxFragmentInputComponentsChanged)
    Q_PROPERTY(int glMaxFragmentUniformComponents READ glMaxFragmentUniformComponents NOTIFY glMaxFragmentUniformComponentsChanged)
    Q_PROPERTY(int glMaxFragmentUniformVectors READ glMaxFragmentUniformVectors NOTIFY glMaxFragmentUniformVectorsChanged)
    Q_PROPERTY(int glMaxFragmentUniformBlocks READ glMaxFragmentUniformBlocks NOTIFY glMaxFragmentUniformBlocksChanged)
    Q_PROPERTY(int glMaxFramebufferWidth READ glMaxFramebufferWidth NOTIFY glMaxFramebufferWidthChanged)
    Q_PROPERTY(int glMaxFramebufferHeight READ glMaxFramebufferHeight NOTIFY glMaxFramebufferHeightChanged)
    Q_PROPERTY(int glMaxFramebufferLayers READ glMaxFramebufferLayers NOTIFY glMaxFramebufferLayersChanged)
    Q_PROPERTY(int glMaxFramebufferSamples READ glMaxFramebufferSamples NOTIFY glMaxFramebufferSamplesChanged)
    Q_PROPERTY(int glMaxGeometryAtomicCounters READ glMaxGeometryAtomicCounters NOTIFY glMaxGeometryAtomicCountersChanged)
    Q_PROPERTY(int glMaxGeometryShaderStorageBlocks READ glMaxGeometryShaderStorageBlocks NOTIFY glMaxGeometryShaderStorageBlocksChanged)
    Q_PROPERTY(int glMaxGeometryInputComponents READ glMaxGeometryInputComponents NOTIFY glMaxGeometryInputComponentsChanged)
    Q_PROPERTY(int glMaxGeometryOutputComponents READ glMaxGeometryOutputComponents NOTIFY glMaxGeometryOutputComponentsChanged)
    Q_PROPERTY(int glMaxGeometryTextureImageUnits READ glMaxGeometryTextureImageUnits NOTIFY glMaxGeometryTextureImageUnitsChanged)
    Q_PROPERTY(int glMaxGeometryUniformBlocks READ glMaxGeometryUniformBlocks  NOTIFY glMaxGeometryUniformBlocksChanged)
    Q_PROPERTY(int glMaxGeometryUniformComponents READ glMaxGeometryUniformComponents NOTIFY glMaxGeometryUniformComponentsChanged)
    Q_PROPERTY(int glMaxIntegerSamples READ glMaxIntegerSamples  NOTIFY glMaxIntegerSamplesChanged)
    Q_PROPERTY(int glMaxLabelLength READ glMaxLabelLength  NOTIFY glMaxLabelLengthChanged)
    Q_PROPERTY(int glMaxProgramTexelOffset READ glMaxProgramTexelOffset  NOTIFY glMaxProgramTexelOffsetChanged)
    Q_PROPERTY(int glMinProgramTexelOffset READ glMinProgramTexelOffset NOTIFY glMinProgramTexelOffsetChanged)
    Q_PROPERTY(int glMaxRectangleTextureSize READ glMaxRectangleTextureSize NOTIFY glMaxRectangleTextureSizeChanged)
    Q_PROPERTY(int glMaxRenderbufferSize READ glMaxRenderbufferSize NOTIFY glMaxRenderbufferSizeChanged)
    Q_PROPERTY(int glMaxSampleMaskWords READ glMaxSampleMaskWords NOTIFY glMaxSampleMaskWordsChanged)
    Q_PROPERTY(int glMaxServerWaitTimeout READ glMaxServerWaitTimeout  NOTIFY glMaxServerWaitTimeoutChanged)
    Q_PROPERTY(int glMaxShaderStorageBufferBindings READ glMaxShaderStorageBufferBindings NOTIFY glMaxShaderStorageBufferBindingsChanged)
    Q_PROPERTY(int glMaxTessControlAtomicCounters READ glMaxTessControlAtomicCounters NOTIFY glMaxTessControlAtomicCountersChanged)
    Q_PROPERTY(int glMaxTessEvaluationAtomicCounters READ glMaxTessEvaluationAtomicCounters NOTIFY glMaxTessEvaluationAtomicCountersChanged)
    Q_PROPERTY(int glMaxTessControlShaderStorageBlocks READ glMaxTessControlShaderStorageBlocks NOTIFY glMaxTessControlShaderStorageBlocksChanged)
    Q_PROPERTY(int glMaxTessEvaluationShaderStorageBlocks READ glMaxTessEvaluationShaderStorageBlocks NOTIFY glMaxTessEvaluationShaderStorageBlocksChanged)
    Q_PROPERTY(int glMaxTextureBufferSize READ glMaxTextureBufferSize NOTIFY glMaxTextureBufferSizeChanged)
    Q_PROPERTY(int glMaxTextureImageUnits READ glMaxTextureImageUnits NOTIFY glMaxTextureImageUnitsChanged)
    Q_PROPERTY(float glMaxTextureLodBias READ glMaxTextureLodBias NOTIFY glMaxTextureLodBiasChanged)
    Q_PROPERTY(int glMaxTextureSize READ glMaxTextureSize NOTIFY glMaxTextureSizeChanged)
    Q_PROPERTY(int glMaxUniformBufferBindings READ glMaxUniformBufferBindings NOTIFY glMaxUniformBufferBindingsChanged)
    Q_PROPERTY(int glMaxUniformBlockSize READ glMaxUniformBlockSize NOTIFY glMaxUniformBlockSizeChanged)
    Q_PROPERTY(int glMaxUniformLocations READ glMaxUniformLocations NOTIFY glMaxUniformLocationsChanged)
    Q_PROPERTY(int glMaxVaryingComponents READ glMaxVaryingComponents NOTIFY glMaxVaryingComponentsChanged)
    Q_PROPERTY(int glMaxVaryingVectors READ glMaxVaryingVectors NOTIFY glMaxVaryingVectorsChanged)
    Q_PROPERTY(int glMaxVaryingFloats READ glMaxVaryingFloats NOTIFY glMaxVaryingFloatsChanged)
    Q_PROPERTY(int glMaxVertexAtomicCounters READ glMaxVertexAtomicCounters NOTIFY glMaxVertexAtomicCountersChanged)
    Q_PROPERTY(int glMaxVertexAttribs READ glMaxVertexAttribs NOTIFY glMaxVertexAttribsChanged)
    Q_PROPERTY(int glMaxVertexShaderStorageBlocks READ glMaxVertexShaderStorageBlocks NOTIFY glMaxVertexShaderStorageBlocksChanged)
    Q_PROPERTY(int glMaxVertexTextureImageUnits READ glMaxVertexTextureImageUnits NOTIFY glMaxVertexTextureImageUnitsChanged)
    Q_PROPERTY(int glMaxVertexUniformComponents READ glMaxVertexUniformComponents NOTIFY glMaxVertexUniformComponentsChanged)
    Q_PROPERTY(int glMaxVertexUniformVectors READ glMaxVertexUniformVectors NOTIFY glMaxVertexUniformVectorsChanged)
    Q_PROPERTY(int glMaxVertexOutputComponents READ glMaxVertexOutputComponents NOTIFY glMaxVertexOutputComponentsChanged)
    Q_PROPERTY(int glMaxVertexUniformBlocks READ glMaxVertexUniformBlocks NOTIFY glMaxVertexUniformBlocksChanged)
    Q_PROPERTY(QSize glMaxViewportDims READ glMaxViewportDims NOTIFY glMaxViewportDimsChanged)
    Q_PROPERTY(int glMaxViewports READ glMaxViewportsGlMaxViewports NOTIFY glMaxViewportsChanged)
    Q_PROPERTY(int glMinorVersion READ glMinorVersion NOTIFY glMinorVersionChanged)
    Q_PROPERTY(float glPointSizeGranularity READ glPointSizeGranularity NOTIFY glPointSizeGranularityChanged)
    Q_PROPERTY(QVector<float> glSmoothPointSizeRange READ glSmoothPointSizeRange NOTIFY glSmoothPointSizeRangeChanged)
    Q_PROPERTY(QVector<float> glAliasedPointSizeRange READ glAliasedPointSizeRange NOTIFY glAliasedPointSizeRangeChanged)
    Q_PROPERTY(QVector<float> glPointSizeRange READ glPointSizeRange NOTIFY glPointSizeRangeChanged)
    Q_PROPERTY(QVector<float> glSmoothLineWidthRange READ glSmoothLineWidthRange NOTIFY glSmoothLineWidthRangeChanged)
    Q_PROPERTY(float glSmoothLineWidthGranularity READ glSmoothLineWidthGranularity NOTIFY glSmoothLineWidthGranularityChanged)
    Q_PROPERTY(QVector<float> glAliasedLineWidthRange READ glAliasedLineWidthRange NOTIFY glAliasedLineWidthRangeChanged)
    Q_PROPERTY(bool glStereo READ glStereo NOTIFY glStereoChanged)
    Q_PROPERTY(int glSubPixelBits READ glSubPixelBits NOTIFY glSubPixelBitsChanged)
    Q_PROPERTY(int glMaxVertexAttribRelativeOffset READ glMaxVertexAttribRelativeOffset NOTIFY glMaxVertexAttribRelativeOffsetChanged)
    Q_PROPERTY(int glMaxVertexAttribBindings READ glMaxVertexAttribBindings NOTIFY glMaxVertexAttribBindingsChanged)
    Q_PROPERTY(QVector<int> glViewportBoundsRange READ glViewportBoundsRange NOTIFY glViewportBoundsRangeChanged)
    Q_PROPERTY(int glViewportSubpixelBits READ glViewportSubpixelBits NOTIFY glViewportSubpixelBitsChanged)
    Q_PROPERTY(int glMaxElementIndex READ glMaxElementIndex NOTIFY glMaxElementIndexChanged)
    Q_PROPERTY(int glMaxTessGenLevel READ glMaxTessGenLevel NOTIFY glMaxTessGenLevelChanged)
    Q_PROPERTY(int glMaxPatchVertices READ glMaxPatchVertices NOTIFY glMaxPatchVerticesChanged)
#ifdef Q_QDOC
    Q_PROPERTY(ivec3 glMaxComputeVariableGroupSizeArb READ glMaxComputeVariableGroupSizeArb NOTIFY glMaxComputeVariableGroupSizeArbChanged)
#else
    Q_PROPERTY(qivec3 glMaxComputeVariableGroupSizeArb READ glMaxComputeVariableGroupSizeArb NOTIFY glMaxComputeVariableGroupSizeArbChanged)
#endif
    Q_PROPERTY(int glMaxComputeVariableGroupInvocationsArb READ glMaxComputeVariableGroupInvocationsArb NOTIFY glMaxComputeVariableGroupInvocationsArbChanged)

public:

    QuickOpenGLProperties(QQuickItem *parent = nullptr);
    ~QuickOpenGLProperties();

    static QuickOpenGLProperties *qmlAttachedProperties(QObject *object);

    QVector<float> glLineWidthRange() const;
    int glMaxComputeShaderStorageBlocks() const;
    int glMaxCombinedShaderStorageBlocks() const;
    int glMaxComputeUniformBlocks() const;
    int glMaxComputeTextureImageUnits() const;
    int glMaxComputeUniformComponents() const;
    int glMaxComputeAtomicCounters() const;
    int glMaxComputeAtomicCounterBuffers() const;
    int glMaxCombinedComputeUniformComponents() const;
    int glMaxComputeWorkGroupInvocations() const;
    qivec3 glMaxComputeWorkGroupCount() const;
    qivec3 glMaxComputeWorkGroupSize() const;
    int glMaxDebugGroupStackDepth() const;
    int glMajorVersion() const;
    int glMax3DTextureSize() const;
    int glMaxArrayTextureLayers() const;
    int glMaxClipDistances() const;
    int glMaxColorTextureSamples() const;
    int glMaxCombinedAtomicCounters() const;
    int glMaxCombinedFragmentUniformComponents() const;
    int glMaxCombinedGeometryUniformComponents() const;
    int glMaxCombinedTextureImageUnits() const;
    int glMaxCombinedUniformBlocks() const;
    int glMaxCombinedVeretexUniformComponents() const;
    int glMaxCubeMapTextureSize() const;
    int glMaxDepthTextureSamples() const;
    int glMaxDrawBuffers() const;
    int glMaxDualSourceDrawBuffers() const;
    int glMaxElementsIndices() const;
    int glMaxElementsVertices() const;
    int glMaxFragmentAtomicCounters() const;
    int glMaxFragmentShaderStorageBlocks() const;
    int glMaxFragmentInputComponents() const;
    int glMaxFragmentUniformComponents() const;
    int glMaxFragmentUniformVectors() const;
    int glMaxFragmentUniformBlocks() const;
    int glMaxFramebufferWidth() const;
    int glMaxFramebufferHeight() const;
    int glMaxFramebufferLayers() const;
    int glMaxFramebufferSamples() const;
    int glMaxGeometryAtomicCounters() const;
    int glMaxGeometryShaderStorageBlocks() const;
    int glMaxGeometryInputComponents() const;
    int glMaxGeometryOutputComponents() const;
    int glMaxGeometryTextureImageUnits() const;
    int glMaxGeometryUniformBlocks() const;
    int glMaxGeometryUniformComponents() const;
    int glMaxIntegerSamples() const;
    int glMaxLabelLength() const;
    int glMaxProgramTexelOffset() const;
    int glMinProgramTexelOffset() const;
    int glMaxRectangleTextureSize() const;
    int glMaxRenderbufferSize() const;
    int glMaxSampleMaskWords() const;
    int glMaxServerWaitTimeout() const;
    int glMaxShaderStorageBufferBindings() const;
    int glMaxTessControlAtomicCounters() const;
    int glMaxTessEvaluationAtomicCounters() const;
    int glMaxTessControlShaderStorageBlocks() const;
    int glMaxTessEvaluationShaderStorageBlocks() const;
    int glMaxTextureBufferSize() const;
    int glMaxTextureImageUnits() const;
    float glMaxTextureLodBias() const;
    int glMaxTextureSize() const;
    int glMaxUniformBufferBindings() const;
    int glMaxUniformBlockSize() const;
    int glMaxUniformLocations() const;
    int glMaxVaryingComponents() const;
    int glMaxVaryingVectors() const;
    int glMaxVaryingFloats() const;
    int glMaxVertexAtomicCounters() const;
    int glMaxVertexAttribs() const;
    int glMaxVertexShaderStorageBlocks() const;
    int glMaxVertexTextureImageUnits() const;
    int glMaxVertexUniformComponents() const;
    int glMaxVertexUniformVectors() const;
    int glMaxVertexOutputComponents() const;
    int glMaxVertexUniformBlocks() const;
    QSize glMaxViewportDims() const;
    int glMaxViewportsGlMaxViewports() const;
    int glMinorVersion() const;
    float glPointSizeGranularity() const;
    QVector<float> glSmoothLineWidthRange() const;
    float glSmoothLineWidthGranularity() const;
    bool glStereo() const;
    int glSubPixelBits() const;
    int glMaxVertexAttribRelativeOffset() const;
    int glMaxVertexAttribBindings() const;
    QVector<int> glViewportBoundsRange() const;
    int glViewportSubpixelBits() const;
    int glMaxElementIndex() const;
    QVector<float> glPointSizeRange() const;
    int glMaxTessGenLevel() const;
    int glMaxPatchVertices() const;
    QVector<float> glAliasedLineWidthRange() const;
    QVector<float> glSmoothPointSizeRange() const;
    QVector<float> glAliasedPointSizeRange() const;
    qivec3 glMaxComputeVariableGroupSizeArb() const;
    int glMaxComputeVariableGroupInvocationsArb() const;
    QString vendor() const;
    QStringList extensions() const;

    bool isOpenGLES() const;

signals:

    void glLineWidthRangeChanged(QVector<float> glLineWidthRange);
    void glMaxElementsIndicesChanged(int glMaxElementsIndices);
    void glMaxComputeShaderStorageBlocksChanged(int glMaxComputeShaderStorageBlocks);
    void glMaxCombinedShaderStorageBlocksChanged(int glMaxCombinedShaderStorageBlocks);
    void glMaxComputeUniformBlocksChanged(int glMaxComputeUniformBlocks);
    void glMaxComputeTextureImageUnitsChanged(int glMaxComputeTextureImageUnits);
    void glMaxComputeUniformComponentsChanged(int glMaxComputeUniformComponents);
    void glMaxComputeAtomicCountersChanged(int glMaxComputeAtomicCounters);
    void glMaxComputeAtomicCounterBuffersChanged(int glMaxComputeAtomicCounterBuffers);
    void glMaxCombinedComputeUniformComponentsChanged(int glMaxCombinedComputeUniformComponents);
    void glMaxComputeWorkGroupInvocationsChanged(int glMaxComputeWorkGroupInvocations);
    void glMaxComputeWorkGroupCountChanged(qivec3 glMaxComputeWorkGroupCount);
    void glMaxComputeWorkGroupSizeChanged(qivec3 glMaxComputeWorkGroupSize);
    void glMaxDebugGroupStackDepthChanged(int glMaxDebugGroupStackDepth);
    void glMajorVersionChanged(int glMajorVersion);
    void glMax3DTextureSizeChanged(int glMax3DTextureSize);
    void glMaxArrayTextureLayersChanged(int glMaxArrayTextureLayers);
    void glMaxClipDistancesChanged(int glMaxClipDistances);
    void glMaxColorTextureSamplesChanged(int glMaxColorTextureSamples);
    void glMaxCombinedAtomicCountersChanged(int glMaxCombinedAtomicCounters);
    void glMaxCombinedFragmentUniformComponentsChanged(int glMaxCombinedFragmentUniformComponents);
    void glMaxCombinedGeometryUniformComponentsChanged(int glMaxCombinedGeometryUniformComponents);
    void glMaxCombinedTextureImageUnitsChanged(int glMaxCombinedTextureImageUnits);
    void glMaxCombinedUniformBlocksChanged(int glMaxCombinedUniformBlocks);
    void glMaxCombinedVeretexUniformComponentsChanged(int glMaxCombinedVeretexUniformComponents);
    void glMaxCubeMapTextureSizeChanged(int glMaxCubeMapTextureSize);
    void glMaxDepthTextureSamplesChanged(int glMaxDepthTextureSamples);
    void glMaxDrawBuffersChanged(int glMaxDrawBuffers);
    void glMaxDualSourceDrawBuffersChanged(int glMaxDualSourceDrawBuffers);
    void glMaxElementsVerticesChanged(int glMaxElementsVertices);
    void glMaxFragmentAtomicCountersChanged(int glMaxFragmentAtomicCounters);
    void glMaxFragmentShaderStorageBlocksChanged(int glMaxFragmentShaderStorageBlocks);
    void glMaxFragmentInputComponentsChanged(int glMaxFragmentInputComponents);
    void glMaxFragmentUniformComponentsChanged(int glMaxFragmentUniformComponents);
    void glMaxFragmentUniformVectorsChanged(int glMaxFragmentUniformVectors);
    void glMaxFragmentUniformBlocksChanged(int glMaxFragmentUniformBlocks);
    void glMaxFramebufferWidthChanged(int glMaxFramebufferWidth);
    void glMaxFramebufferHeightChanged(int glMaxFramebufferHeight);
    void glMaxFramebufferLayersChanged(int glMaxFramebufferLayers);
    void glMaxFramebufferSamplesChanged(int glMaxFramebufferSamples);
    void glMaxGeometryAtomicCountersChanged(int glMaxGeometryAtomicCounters);
    void glMaxGeometryShaderStorageBlocksChanged(int glMaxGeometryShaderStorageBlocks);
    void glMaxGeometryInputComponentsChanged(int glMaxGeometryInputComponents);
    void glMaxGeometryOutputComponentsChanged(int glMaxGeometryOutputComponents);
    void glMaxGeometryTextureImageUnitsChanged(int glMaxGeometryTextureImageUnits);
    void glMaxGeometryUniformBlocksChanged(int glMaxGeometryUniformBlocks);
    void glMaxGeometryUniformComponentsChanged(int glMaxGeometryUniformComponents);
    void glMaxIntegerSamplesChanged(int glMaxIntegerSamples);
    void glMaxLabelLengthChanged(int glMaxLabelLength);
    void glMaxProgramTexelOffsetChanged(int glMaxProgramTexelOffset);
    void glMinProgramTexelOffsetChanged(int glMinProgramTexelOffset);
    void glMaxRectangleTextureSizeChanged(int glMaxRectangleTextureSize);
    void glMaxRenderbufferSizeChanged(int glMaxRenderbufferSize);
    void glMaxSampleMaskWordsChanged(int glMaxSampleMaskWords);
    void glMaxServerWaitTimeoutChanged(int glMaxServerWaitTimeout);
    void glMaxShaderStorageBufferBindingsChanged(int glMaxShaderStorageBufferBindings);
    void glMaxTessControlAtomicCountersChanged(int glMaxTessControlAtomicCounters);
    void glMaxTessEvaluationAtomicCountersChanged(int glMaxTessEvaluationAtomicCounters);
    void glMaxTessControlShaderStorageBlocksChanged(int glMaxTessControlShaderStorageBlocks);
    void glMaxTessEvaluationShaderStorageBlocksChanged(int glMaxTessEvaluationShaderStorageBlocks);
    void glMaxTextureBufferSizeChanged(int glMaxTextureBufferSize);
    void glMaxTextureImageUnitsChanged(int glMaxTextureImageUnits);
    void glMaxTextureLodBiasChanged(float glMaxTextureLodBias);
    void glMaxTextureSizeChanged(int glMaxTextureSize);
    void glMaxUniformBufferBindingsChanged(int glMaxUniformBufferBindings);
    void glMaxUniformBlockSizeChanged(int glMaxUniformBlockSize);
    void glMaxUniformLocationsChanged(int glMaxUniformLocations);
    void glMaxVaryingComponentsChanged(int glMaxVaryingComponents);
    void glMaxVaryingVectorsChanged(int glMaxVaryingVectors);
    void glMaxVaryingFloatsChanged(int glMaxVaryingFloats);
    void glMaxVertexAtomicCountersChanged(int glMaxVertexAtomicCounters);
    void glMaxVertexAttribsChanged(int glMaxVertexAttribs);
    void glMaxVertexShaderStorageBlocksChanged(int glMaxVertexShaderStorageBlocks);
    void glMaxVertexTextureImageUnitsChanged(int glMaxVertexTextureImageUnits);
    void glMaxVertexUniformComponentsChanged(int glMaxVertexUniformComponents);
    void glMaxVertexUniformVectorsChanged(int glMaxVertexUniformVectors);
    void glMaxVertexOutputComponentsChanged(int glMaxVertexOutputComponents);
    void glMaxVertexUniformBlocksChanged(int glMaxVertexUniformBlocks);
    void glMaxViewportDimsChanged(QSize glMaxViewportDims);
    void glMaxViewportsChanged(int glMaxViewports);
    void glMinorVersionChanged(int glMinorVersion);
    void glPointSizeGranularityChanged(float glPointSizeGranularity);
    void glSmoothLineWidthRangeChanged(QVector<float> glSmoothLineWidthRange);
    void glSmoothLineWidthGranularityChanged(float glSmoothLineWidthGranularity);
    void glStereoChanged(bool glStereo);
    void glSubPixelBitsChanged(int glSubPixelBits);
    void glMaxVertexAttribRelativeOffsetChanged(int glMaxVertexAttribRelativeOffset);
    void glMaxVertexAttribBindingsChanged(int glMaxVertexAttribBindings);
    void glViewportBoundsRangeChanged(QVector<int> glViewportBoundsRange);
    void glViewportSubpixelBitsChanged(int glViewportSubpixelBits);
    void glMaxElementIndexChanged(int glMaxElementIndex);
    void glPointSizeRangeChanged(QVector<float> glPointSizeRange);
    void glMaxTessGenLevelChanged(int glMaxTessGenLevel);
    void glMaxPatchVerticesChanged(int glMaxPatchVertices);
    void glAliasedLineWidthRangeChanged(QVector<float> glAliasedLineWidthRange);
    void glSmoothPointSizeRangeChanged(QVector<float> glSmoothPointSizeRange);
    void glAliasedPointSizeRangeChanged(QVector<float> glAliasedPointSizeRange);
    void glMaxComputeVariableGroupSizeArbChanged(qivec3 glMaxComputeVariableGroupSizeArb);
    void glMaxComputeVariableGroupInvocationsArbChanged(int glMaxComputeVariableGroupInvocationsArb);    
    void vendorChanged(QString vendor);    
    void extensionsChanged(QStringList extensions);

protected:

    QScopedPointer<QuickOpenGLPropertiesPrivate> d_ptr;

private:

    Q_DECLARE_PRIVATE(QuickOpenGLProperties)

};

QML_DECLARE_TYPE(QuickOpenGLProperties)
QML_DECLARE_TYPEINFO(QuickOpenGLProperties, QML_HAS_ATTACHED_PROPERTIES)

#endif // QUICKOPENGLPROPERTIES_H

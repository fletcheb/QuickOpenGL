/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quickopengldebugmessage.h"

/*!
 * \qmltype OpenGLDebugMessage
 * \brief The OpenGLDebugMessage QML type represents an OpenGL debug mesage.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickOpenGLDebugMessage
 * \ingroup QuickOpenGL QML Types
 * 
 *
 * OpenGLDebugMessage represents an OpenGL
 * \l{https://www.khronos.org/registry/OpenGL/extensions/KHR/KHR_debug.txt}
 * {GL_KHR_debug} log message. It is based on \l QOpenGLDebugMessage.
 *
 * \sa OpenGLDebugLogger
 * \sa QOpenGLDebugMessage
 */


/*!
 * \qmlproperty int OpenGLDebugMessage::id
 *
 * This property holds the id of the debug message. Ids are generally vendor-specific
 *
 * \sa QOpenGLDebugMessage::id
 */

 /*!
 * \qmlproperty string OpenGLDebugMessage::message
 *
 * This property holds a textual description of the debug message.
 *
 * \sa QOpenGLDebugMessage::message
 */


QuickOpenGLDebugMessage::QuickOpenGLDebugMessage() :
    QOpenGLDebugMessage()
{
}

QuickOpenGLDebugMessage::QuickOpenGLDebugMessage(const QOpenGLDebugMessage &other) :
    QOpenGLDebugMessage(other)
{
}

/*!
 * \qmlproperty enumeration OpenGLDebugMessage::source
 *
 * This property holds the debug message's source.
 *
 * source takes the following values:
 *
 * \value SourceInvalid
 * \value SourceAPI
 * \value SourceWindowSystem
 * \value SourceShaderCompiler
 * \value SourceThirdParty
 * \value SourceApplication
 * \value SourceOther
 * \value SourceLast
 * \value SourceAny
 *
 * \sa QOpenGLDebugMessage::source
 */
QuickOpenGLDebugMessage::Source QuickOpenGLDebugMessage::source() const
{
    return QuickOpenGLDebugMessage::Source(static_cast<const QOpenGLDebugMessage *>(this)->source());
}

/*!
 * \qmlproperty enumeration OpenGLDebugMessage::type
 *
 * This property holds the debug message's type.
 *
 * source takes the following values:
 *
 * \value TypeInvalid
 * \value TypeError
 * \value TypeDeprecatedBehavior
 * \value TypeUndefinedBehavior
 * \value TypePortability
 * \value TypePerformance
 * \value TypeOther
 * \value TypeMarker
 * \value TypeGroupPush
 * \value TypeGroupPop
 * \value TypeLast
 * \value TypeAny
 *
 * \sa QOpenGLDebugMessage::type
 */
QuickOpenGLDebugMessage::Type QuickOpenGLDebugMessage::type() const
{
    return QuickOpenGLDebugMessage::Type(static_cast<const QOpenGLDebugMessage *>(this)->type());
}


/*!
 * \qmlproperty enumeration OpenGLDebugMessage::severity
 *
 * This property holds the debug message's severity.
 *
 * severity takes the following values:
 *
 * \value SeverityInvalid
 * \value SeverityHigh
 * \value SeverityMedium
 * \value SeverityLow
 * \value SeverityNotification
 * \value SeverityLast
 * \value SeverityAny
 *
 * \sa QOpenGLDebugMessage::severity
 */
QuickOpenGLDebugMessage::Severity QuickOpenGLDebugMessage::severity() const
{
    return QuickOpenGLDebugMessage::Severity(static_cast<const QOpenGLDebugMessage *>(this)->severity());
}

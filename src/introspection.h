/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKOPENGL_INTROSPECTION_H
#define QUICKOPENGL_INTROSPECTION_H

#include <QDebug>
#include <QHash>
#include <QOpenGLContext>
#include <QQuickItem>
#include <QRegularExpression>
#include "support.h"
#include "quicksgtexturehandleprovider.h"
#include "quicksgimagehandleprovider.h"
#include <QSGTextureProvider>
#include <QSGTexture>
#include "quickopengl_global.h"

/*!
 * \brief OpenGL Introspection Class
 *
 * Collection of classes and functions for performing OpenGL introspection
 */
class QUICKOPENGL_EXPORT Introspection
{
public:

    /*!
     * \brief OpenGL Shader Uniform Information class
     */
    class QUICKOPENGL_EXPORT Uniform
    {
    public:

        /*!
         * \brief The uniform name
         */
        QByteArray name;

        /*!
         * \brief The uniform type
         */
        GLenum type;

        /*!
         * \brief The uniform location
         */
        GLint location;

        /*!
         * \brief The uniform size
         */
        GLint arraySize;

        /*!
         * \brief The block index
         */
        GLint blockIndex;

        /*!
         * \brief The byte offset
         */
        GLint offset;

        /*!
         * \brief The array stride in bytes
         */
        GLint arrayStride;

        /*!
         * \brief The matrix stride in bytes
         */
        GLint matrixStride;

        /*!
         * \brief Row major flag
         */
        GLboolean isRowMajor;

        GLint atomicCounterBufferIndex;

        bool operator ==(const Uniform& other) const;
    };

    class QUICKOPENGL_EXPORT UniformBlock
    {
    public:
        UniformBlock() :
            index(0),
            binding(0),
            size(0),
            programId(0)
        {}

        bool operator ==(const UniformBlock &other) const;
        bool operator !=(const UniformBlock& other) const;

        bool isValid() const;

        QString name;
        int index;
        int binding;
        int size;
        uint programId;

        QHash<QString, Uniform> uniforms;
    };

    /*!
     * \brief OpenGL Vertex Attribute Information class
     */
    class QUICKOPENGL_EXPORT Attribute
    {
    public:

        /*!
         * \brief The attribute name
         */
        QByteArray name;

        /*!
         * \brief The attribute type
         */
        GLenum type;

        /*!
         * \brief The attribute location
         */
        GLint location;

        /*!
         * \brief The attribute size
         */
        GLint arraySize;
    };


    class QUICKOPENGL_EXPORT ShaderStorageBlock
    {
    public:

        ShaderStorageBlock() :
            programId(0),
            index(0),
            binding(0),
            size(0)
        {}

        bool operator ==(const ShaderStorageBlock& other) const;
        bool operator !=(const ShaderStorageBlock& other) const;

        bool isValid() const;
        uint programId;

        QString name;

        GLuint index;

        GLuint binding;

        GLsizei size;

        class Variable
        {
        public:
            bool operator ==(const Variable &other) const;
            QByteArray name;
            GLenum type;
            GLint arraySize;
            GLint offset;
            GLint blockIndex;
            GLint arrayStride;
            GLint matrixStride;
            GLint isRowMajor;
            GLint topLevelArraySize;
            GLint topLevelArrayStride;
        };

        QHash<QString, Variable> variables;

    };

    static QList<UniformBlock> activeUniformBlocks(GLuint program);

    static QList<Uniform> activeUniforms(GLuint program);

    static QList<Attribute> activeAttributes(GLuint program);

    static QList<ShaderStorageBlock> activeShaderStorageBlocks(GLuint program);

    static void setUniforms(const QList<Uniform> &activeUniforms, const QVariantHash &properties);

    static void setShaderStorageBlocks(const QList<ShaderStorageBlock> &shaderStorageBlocks, const QVariantHash &properties);

    static QVariantMap getShaderStorageBlockVariables(const ShaderStorageBlock &shaderStorageBlock, const QByteArray &data);

    static QVariant conditionVariant(const QVariant &variant, QObject *notifyItem = nullptr, bool getProviders = true, QQuickWindow *window = nullptr);

    class SubroutineUniform
    {
    public:

        QByteArray name;
        int location;
        QHash<QByteArray, int> compatibleSubroutines;
    };

    static QVariantList conditionProperty(QVariant property, const QString &introspectedName, GLenum introspectedType, int introspectedArraySize, const QString &propertyType, const QQuickItem *qmlItem, QString name = QString());


    static QList<Introspection::SubroutineUniform> activeSubroutineUniforms(GLuint program, GLuint shader);

    static void setSubroutineUniforms(GLuint shader, const QList<SubroutineUniform> &activeSubroutineUniforms, const QVariantHash &properties);

    static void setUniformBlocks(const QList<Introspection::UniformBlock> &activeUniformBlocks, const QVariantHash &properties, GLuint program);    

protected:

    Introspection();
};



QDebug operator << (QDebug debug, const Introspection::Uniform &uniform);
QDebug operator << (QDebug debug, const Introspection::UniformBlock &uniformBlock);
QDebug operator << (QDebug debug, const Introspection::Attribute &attribute);
QDebug operator << (QDebug debug, const Introspection::ShaderStorageBlock::Variable &variable);
QDebug operator << (QDebug debug, const Introspection::ShaderStorageBlock &buffer);
QDebug operator << (QDebug debug, const Introspection::SubroutineUniform &subroutine);

Q_DECLARE_METATYPE(Introspection::ShaderStorageBlock)

#endif // QUICKOPENGL_INTROSPECTION_H

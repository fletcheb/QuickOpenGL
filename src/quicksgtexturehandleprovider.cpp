/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgtexturehandleprovider.h"
#include <QOpenGLFunctions_3_3_Core>
#include "openglextension_arb_bindless_texture.h"
#include "support.h"

class QuickSGTextureHandleProviderPrivate
{
public:

    QuickSGTextureHandleProviderPrivate();

    QOpenGLExtension_ARB_bindless_texture *glArbBindless;
    QSGTextureProvider *sourceProvider;
    quint64 handle;
};

QuickSGTextureHandleProviderPrivate::QuickSGTextureHandleProviderPrivate() :
    glArbBindless(nullptr),
    sourceProvider(nullptr),
    handle(0)
{
}

QuickSGTextureHandleProvider::QuickSGTextureHandleProvider() :
    d_ptr(new QuickSGTextureHandleProviderPrivate)
{
    Q_D(QuickSGTextureHandleProvider);

    d->glArbBindless = new QOpenGLExtension_ARB_bindless_texture;
    Q_CHECK_PTR(d->glArbBindless);

    if(d->glArbBindless)
    {
        bool ok = d->glArbBindless->initializeOpenGLFunctions();
        Q_ASSERT(ok);
    }
    else
    {
        qWarning() << "QuickOpenGL: OpenGL ARB Bindless texture extension not supported";
    }
}

QuickSGTextureHandleProvider::~QuickSGTextureHandleProvider()
{
    Q_D(QuickSGTextureHandleProvider);

    if(d->glArbBindless)
    {
        makeNonResident();
        delete d->glArbBindless;
    }
}

quint64 QuickSGTextureHandleProvider::handle() const
{
    Q_D(const QuickSGTextureHandleProvider);

    return d->handle;
}

void QuickSGTextureHandleProvider::makeResident() const
{
    Q_D(const QuickSGTextureHandleProvider);

    if(d->glArbBindless&&d->handle&&!d->glArbBindless->glIsTextureHandleResidentARB(d->handle))
    {
        d->glArbBindless->glMakeTextureHandleResidentARB(d->handle);debugGL;
    }
}

void QuickSGTextureHandleProvider::makeNonResident() const
{
    Q_D(const QuickSGTextureHandleProvider);

    Q_ASSERT(QOpenGLContext::currentContext());

    if(d->glArbBindless&&d->handle&&d->glArbBindless->glIsTextureHandleResidentARB(d->handle))
    {
        d->glArbBindless->glMakeTextureHandleNonResidentARB(d->handle);debugGL;
    }
}

void QuickSGTextureHandleProvider::setSourceProvider(QSGTextureProvider *sourceProvider)
{

    Q_D(QuickSGTextureHandleProvider);

    // if the source provider is not changing or our hardware doesnt support ARB Bindless extension, bail out
    if((sourceProvider==d->sourceProvider)||(!d->glArbBindless))
    {
        return;
    }

    // if we already have a source provider, disconnect connected signals
    if(d->sourceProvider)
    {
        disconnect(d->sourceProvider, &QSGTextureProvider::textureChanged, this, &QuickSGTextureHandleProvider::textureChanged);
        disconnect(d->sourceProvider, &QSGTextureProvider::destroyed, this, &QuickSGTextureHandleProvider::onSourceProviderDestroyed);
    }

    // take a copy of the source provider
    d->sourceProvider = sourceProvider;

    // the texture handle
    GLuint64 handle = 0;

    // the texture removed from any texture atlas
    QSGTexture *texture = sourceProvider->texture()->isAtlasTexture()?sourceProvider->texture()->removedFromAtlas():sourceProvider->texture();

    // some QSGTextures are not created until bind is called, so call it, then unbind it
    texture->bind();
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();

    GLuint target = sourceProvider->texture()->property("target").isValid()?sourceProvider->texture()->property("target").toUInt():GL_TEXTURE_2D;
    gl->glBindTexture(target, 0);

    // if the source provider has a QuickOpenGL sampler object attached, use the appropriate call to get the teture handle
    if(sourceProvider->property("SamplerObject").isValid())
    {
        handle = d->glArbBindless->glGetTextureSamplerHandleARB(texture->textureId(), sourceProvider->property("SamplerObject").toUInt());debugGL;
    }
    else
    {
        handle = d->glArbBindless->glGetTextureHandleARB(texture->textureId());debugGL;
    }

    // connect the source provider textureChanged signal to our textureChanged signal
    connect(d->sourceProvider, &QSGTextureProvider::textureChanged, this, &QuickSGTextureHandleProvider::textureChanged);
    connect(d->sourceProvider, &QSGTextureProvider::destroyed, this, &QuickSGTextureHandleProvider::onSourceProviderDestroyed);

    // if the handle has changed, let the world know
    if(handle!=d->handle)
    {
        d->handle = handle;
        emit textureChanged();
    }

}

QSGTextureProvider *QuickSGTextureHandleProvider::sourceProvider() const
{
    Q_D(const QuickSGTextureHandleProvider);

    return d->sourceProvider;
}

void QuickSGTextureHandleProvider::onSourceProviderDestroyed()
{
    Q_D(QuickSGTextureHandleProvider);
    makeNonResident();
    d->sourceProvider = nullptr;
}

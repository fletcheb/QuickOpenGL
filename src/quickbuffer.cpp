#include "quickbuffer.h"
#include <QByteArray>

/*!
 * \qmlbasictype buffer
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 * \brief The buffer basic type represents a data buffer containing OpenGL basic types.
 *
 * A buffer may be used as a value for \l{GraphicsProgram::vertices} or \l{Texture::data}.
 *
 * Example usage:
 *
 * \qml
 *
 * function makeVertices()
 * {
 *     var buffer = GL.buffer()
 *
 *     buffer.addVec2(GL.vec2(1,2))
 *     buffer.addIvec3(GL.Ivec3(3,4,5))
 *
 *     return buffer
 * }
 * \endqml
 *
 * buffer is able to be converted to \l{QByteArray} via the \l{QMetaType} system.
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 * \sa QByteArray
 */

template<typename TYPE> QVariant typeAtOffset(const QByteArray &data, unsigned int offset)
{
    QVariant result;
    if(offset<quint32(data.size()))
    {
        result = QVariant::fromValue(reinterpret_cast<const TYPE &>(data.constData()[offset]));
    }

    return result;

}

template<typename TYPE> void setTypeAtOffset(QByteArray &data, const TYPE &value, unsigned int offset)
{
    if((offset+sizeof(TYPE))>quint32(data.size()))
    {
        data.resize(offset+sizeof(TYPE));
    }

    reinterpret_cast<TYPE &>(data.data()[offset]) = value;
}

template<typename TYPE> QVariant typeAt(const QByteArray &data, unsigned int index)
{
    return typeAtOffset<TYPE>(data, index*sizeof(TYPE));
}

template<typename TYPE> void setTypeAt(QByteArray &data, const TYPE &value, unsigned int index)
{
    setTypeAtOffset<TYPE>(data, value, index*sizeof(TYPE));
}


class QuickBufferData : public QSharedData
{
public:

    QByteArray buffer;

};


QuickBuffer::QuickBuffer(int reserveSize) : data(new QuickBufferData)
{
    if(reserveSize!=0)
    {
        data->buffer.reserve(reserveSize);
    }
}

QuickBuffer::QuickBuffer(const QuickBuffer &rhs) : data(rhs.data)
{

}

QuickBuffer::QuickBuffer(const QByteArray &byteArray) : data(new QuickBufferData)
{
    data->buffer = byteArray;
}

QuickBuffer &QuickBuffer::operator=(const QuickBuffer &rhs)
{
    if (this != &rhs)
        data.operator=(rhs.data);
    return *this;
}

QuickBuffer::~QuickBuffer()
{

}

/*!
 * \qmlmethod buffer::addFloat(float value)
 *
 * Append a 32 bit floating point value to the buffer
 */
void QuickBuffer::addFloat(float value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addVec2(vec2 value)
 *
 * Append a vec2 value to the buffer
 */
void QuickBuffer::addVec2(const qvec2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addVec3(vec3 value)
 *
 * Append a vec3 value to the buffer
 */
void QuickBuffer::addVec3(const qvec3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addVec4(vec4 value)
 *
 * Append a vec4 value to the buffer
 */
void QuickBuffer::addVec4(const qvec4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addInt(int value)
 *
 * Append a 32bit int value to the buffer
 */
void QuickBuffer::addInt(int value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addIvec2(ivec2 value)
 *
 * Append an ivec2 value to the buffer
 */
void QuickBuffer::addIvec2(const qivec2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addIvec3(ivec3 value)
 *
 * Append an ivec3 value to the buffer
 */
void QuickBuffer::addIvec3(const qivec3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addIvec4(ivec4 value)
 *
 * Append an ivec4 value to the buffer
 */
void QuickBuffer::addIvec4(const qivec4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addUint(unsigned int value)
 *
 * Append a 32bit unsigned int value to the buffer
 */
void QuickBuffer::addUint(unsigned int value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}


/*!
 * \qmlmethod buffer::addUvec2(uvec2 value)
 *
 * Append a uvec2 value to the buffer
 */
void QuickBuffer::addUvec2(const quvec2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addUvec3(uvec3 value)
 *
 * Append a uvec3 value to the buffer
 */
void QuickBuffer::addUvec3(const quvec3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addUvec4(uvec4 value)
 *
 * Append a uvec4 value to the buffer
 */
void QuickBuffer::addUvec4(const quvec4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDouble(double value)
 *
 * Append a 64bit double precision floating point value to the buffer
 */
void QuickBuffer::addDouble(double value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDvec2(dvec2 value)
 *
 * Append a dvec2 value to the buffer
 */
void QuickBuffer::addDvec2(const qdvec2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDvec3(dvec3 value)
 *
 * Append a dvec3 value to the buffer
 */
void QuickBuffer::addDvec3(const qdvec3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDvec4(dvec4 value)
 *
 * Append a dvec4 value to the buffer
 */
void QuickBuffer::addDvec4(const qdvec4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat2(mat2 value)
 *
 * Append a mat2 value to the buffer
 */
void QuickBuffer::addMat2(const qmat2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat2x3(mat2x3 value)
 *
 * Append a mat2x3 value to the buffer
 */
void QuickBuffer::addMat2x3(const qmat2x3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat2x4(mat2x4 value)
 *
 * Append a mat2x4 value to the buffer
 */
void QuickBuffer::addMat2x4(const qmat2x4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat3x2(mat3x2 value)
 *
 * Append a mat3x2 value to the buffer
 */
void QuickBuffer::addMat3x2(const qmat3x2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat3(mat3 value)
 *
 * Append a mat3 value to the buffer
 */
void QuickBuffer::addMat3(const qmat3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat3x4(mat3x4 value)
 *
 * Append a mat3x4 value to the buffer
 */
void QuickBuffer::addMat3x4(const qmat3x4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat4x2(mat4x2 value)
 *
 * Append a mat4x2 value to the buffer
 */
void QuickBuffer::addMat4x2(const qmat4x2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat4x3(mat4x3 value)
 *
 * Append a mat4x3 value to the buffer
 */
void QuickBuffer::addMat4x3(const qmat4x3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addMat4(mat4 value)
 *
 * Append a mat4 value to the buffer
 */
void QuickBuffer::addMat4(const qmat4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat2(dmat2 value)
 *
 * Append a dmat2 value to the buffer
 */
void QuickBuffer::addDmat2(const qdmat2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat2x3(dmat2x3 value)
 *
 * Append a dmat2x3 value to the buffer
 */
void QuickBuffer::addDmat2x3(const qdmat2x3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat2x4(dmat2x4 value)
 *
 * Append a dmat2x4 value to the buffer
 */
void QuickBuffer::addDmat2x4(const qdmat2x4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat3x2(dmat3x2 value)
 *
 * Append a dmat3x2 value to the buffer
 */
void QuickBuffer::addDmat3x2(const qdmat3x2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat3(dmat3 value)
 *
 * Append a dmat3 value to the buffer
 */
void QuickBuffer::addDmat3(const qdmat3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat3x4(dmat3x4 value)
 *
 * Append a dmat3x4 value to the buffer
 */
void QuickBuffer::addDmat3x4(const qdmat3x4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat2x4(dmat4x2 value)
 *
 * Append a dmat4x2 value to the buffer
 */
void QuickBuffer::addDmat4x2(const qdmat4x2 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat4x3(dmat4x3 value)
 *
 * Append a dmat4x3 value to the buffer
 */
void QuickBuffer::addDmat4x3(const qdmat4x3 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addDmat4(dmat4 value)
 *
 * Append a dmat4 value to the buffer
 */
void QuickBuffer::addDmat4(const qdmat4 &value)
{
    data->buffer.append(reinterpret_cast<const char *>(&value), sizeof(value));
}

/*!
 * \qmlmethod buffer::addBuffer(buffer value)
 *
 * Append a buffer to the buffer
 */
void QuickBuffer::addBuffer(const QuickBuffer &value)
{
    data->buffer.append(value.operator QByteArray());
}

/*!
 * \qmlmethod int buffer::intAt(unsigned int index)
 *
 * Cast the buffer to an array of int and return the value at index
 */
QVariant QuickBuffer::intAt(unsigned int index) const
{
    return typeAt<int>(data->buffer, index);
}

/*!
 * \qmlmethod ivec2 buffer::ivec2At(unsigned int index)
 *
 * Cast the buffer to an array of ivec2 and return the value at index
 */
QVariant QuickBuffer::ivec2At(unsigned int index) const
{
    return typeAt<qivec2>(data->buffer, index);
}

/*!
 * \qmlmethod ivec3 buffer::ivec3At(unsigned int index)
 *
 * Cast the buffer to an array of ivec3 and return the value at index
 */
QVariant QuickBuffer::ivec3At(unsigned int index) const
{
    return typeAt<qivec3>(data->buffer, index);
}

/*!
 * \qmlmethod ivec4 buffer::ivec4At(unsigned int index)
 *
 * Cast the buffer to an array of ivec4 and return the value at index
 */
QVariant QuickBuffer::ivec4At(unsigned int index) const
{
    return typeAt<qivec4>(data->buffer, index);
}

/*!
 * \qmlmethod uint buffer::uintAt(unsigned int index)
 *
 * Cast the buffer to an array of uint and return the int value at index
 */
QVariant QuickBuffer::uintAt(unsigned int index) const
{
    return typeAt<uint>(data->buffer, index);
}

/*!
 * \qmlmethod uvec2 buffer::uvec2At(unsigned int index)
 *
 * Cast the buffer to an array of uvec2 and return the value at index
 */
QVariant QuickBuffer::uvec2At(unsigned int index) const
{
    return typeAt<quvec2>(data->buffer, index);
}

/*!
 * \qmlmethod uvec3 buffer::uvec3At(unsigned int index)
 *
 * Cast the buffer to an array of uvec3 and return the value at index
 */
QVariant QuickBuffer::uvec3At(unsigned int index) const
{
    return typeAt<quvec3>(data->buffer, index);
}

/*!
 * \qmlmethod uvec4 buffer::uvec4At(unsigned int index)
 *
 * Cast the buffer to an array of uvec4 and return the value at index
 */
QVariant QuickBuffer::uvec4At(unsigned int index) const
{
    return typeAt<quvec4>(data->buffer, index);
}

/*!
 * \qmlmethod float buffer::floatAt(unsigned int index)
 *
 * Cast the buffer to an array of float and return the value at index
 */
QVariant QuickBuffer::floatAt(unsigned int index) const
{
    return typeAt<float>(data->buffer, index);
}

/*!
 * \qmlmethod vec2 buffer::vec2At(unsigned int index)
 *
 * Cast the buffer to an array of vec2 and return the value at index
 */
QVariant QuickBuffer::vec2At(unsigned int index) const
{
    return typeAt<qvec2>(data->buffer, index);
}

/*!
 * \qmlmethod vec3 buffer::vec3At(unsigned int index)
 *
 * Cast the buffer to an array of vec3 and return the value at index
 */
QVariant QuickBuffer::vec3At(unsigned int index) const
{
    return typeAt<qvec3>(data->buffer, index);
}

/*!
 * \qmlmethod vec4 buffer::vec4At(unsigned int index)
 *
 * Cast the buffer to an array of vec4 and return the value at index
 */
QVariant QuickBuffer::vec4At(unsigned int index) const
{
    return typeAt<qvec4>(data->buffer, index);
}

/*!
 * \qmlmethod double buffer::doubleAt(unsigned int index)
 *
 * Cast the buffer to an array of double and return the value at index
 */
QVariant QuickBuffer::doubleAt(unsigned int index) const
{
    return typeAt<double>(data->buffer, index);
}

/*!
 * \qmlmethod dvec2 buffer::dvec2At(unsigned int index)
 *
 * Cast the buffer to an array of dvec2 and return the value at index
 */
QVariant QuickBuffer::dvec2At(unsigned int index) const
{
    return typeAt<qdvec2>(data->buffer, index);
}

/*!
 * \qmlmethod dvec3 buffer::dvec3At(unsigned int index)
 *
 * Cast the buffer to an array of dvec3 and return the value at index
 */
QVariant QuickBuffer::dvec3At(unsigned int index) const
{
    return typeAt<qvec3>(data->buffer, index);
}

/*!
 * \qmlmethod dvec4 buffer::dvec4At(unsigned int index)
 *
 * Cast the buffer to an array of dvec4 and return the value at index
 */
QVariant QuickBuffer::dvec4At(unsigned int index) const
{
    return typeAt<qvec4>(data->buffer, index);
}

/*!
 * \qmlmethod int buffer::intAtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as int
 */
QVariant QuickBuffer::intAtOffset(unsigned int offset) const
{
    return typeAtOffset<int>(data->buffer, offset);
}

/*!
 * \qmlmethod ivec2 buffer::ivec2AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as ivec2
 */
QVariant QuickBuffer::ivec2AtOffset(unsigned int offset) const
{
    return typeAtOffset<qivec2>(data->buffer, offset);
}

/*!
 * \qmlmethod ivec3 buffer::ivec3AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as ivec3
 */
QVariant QuickBuffer::ivec3AtOffset(unsigned int offset) const
{
    return typeAtOffset<qivec3>(data->buffer, offset);
}

/*!
 * \qmlmethod ivec4 buffer::ivec4AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as ivec4
 */
QVariant QuickBuffer::ivec4AtOffset(unsigned int offset) const
{
    return typeAtOffset<qivec4>(data->buffer, offset);
}

/*!
 * \qmlmethod uint buffer::uintAtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as uint
 */
QVariant QuickBuffer::uintAtOffset(unsigned int offset) const
{
    return typeAtOffset<uint>(data->buffer, offset);
}

/*!
 * \qmlmethod uvec2 buffer::uvec2AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as uvec2
 */
QVariant QuickBuffer::uvec2AtOffset(unsigned int offset) const
{
    return typeAtOffset<quvec2>(data->buffer, offset);
}

/*!
 * \qmlmethod uvec3 buffer::uvec3AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as uvec3
 */
QVariant QuickBuffer::uvec3AtOffset(unsigned int offset) const
{
    return typeAtOffset<quvec3>(data->buffer, offset);
}

/*!
 * \qmlmethod uvec4 buffer::uvec4AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as uvec4
 */
QVariant QuickBuffer::uvec4AtOffset(unsigned int offset) const
{
    return typeAtOffset<quvec4>(data->buffer, offset);
}

/*!
 * \qmlmethod float buffer::floatAtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as float
 */
QVariant QuickBuffer::floatAtOffset(unsigned int offset) const
{
    return typeAtOffset<float>(data->buffer, offset);
}

/*!
 * \qmlmethod vec2 buffer::vec2AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as vec2
 */
QVariant QuickBuffer::vec2AtOffset(unsigned int offset) const
{
    return typeAtOffset<qvec2>(data->buffer, offset);
}

/*!
 * \qmlmethod vec3 buffer::vec3AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as vec3
 */
QVariant QuickBuffer::vec3AtOffset(unsigned int offset) const
{
    return typeAtOffset<qvec3>(data->buffer, offset);
}

/*!
 * \qmlmethod vec4 buffer::vec4AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as vec4
 */
QVariant QuickBuffer::vec4AtOffset(unsigned int offset) const
{
    return typeAtOffset<qvec4>(data->buffer, offset);
}

/*!
 * \qmlmethod double buffer::doubleAtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as double
 */
QVariant QuickBuffer::doubleAtOffset(unsigned int offset) const
{
    return typeAtOffset<double>(data->buffer, offset);
}

/*!
 * \qmlmethod dvec2 buffer::dvec2AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as dvec2
 */
QVariant QuickBuffer::dvec2AtOffset(unsigned int offset) const
{
    return typeAtOffset<qdvec2>(data->buffer, offset);
}

/*!
 * \qmlmethod dvec3 buffer::dvec3AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as dvec3
 */
QVariant QuickBuffer::dvec3AtOffset(unsigned int offset) const
{
    return typeAtOffset<qdvec3>(data->buffer, offset);
}

/*!
 * \qmlmethod dvec4 buffer::dvec4AtOffset(unsigned int offset)
 *
 * Return the data at byte offset into the buffer interpreted as dvec4
 */
QVariant QuickBuffer::dvec4AtOffset(unsigned int offset) const
{
    return typeAtOffset<qdvec4>(data->buffer, offset);
}

/*!
 * \qmlmethod void buffer::setIntAtOffset(int value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setIntAtOffset(const int &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setIvec2AtOffset(ivec2 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setIvec2AtOffset(const qivec2 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setIvec3AtOffset(ivec3 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setIvec3AtOffset(const qivec3 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setIvec4AtOffset(ivec4 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setIvec4AtOffset(const qivec4 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setUintAtOffset(uint value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setUintAtOffset(const uint &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setUvec2AtOffset(uvec2 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setUivec2AtOffset(const quvec2 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setUvec3AtOffset(uvec3 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setUivec3AtOffset(const quvec3 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setUvec4AtOffset(uvec4 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setUivec4AtOffset(const quvec4 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setFloatAtOffset(float value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */

void QuickBuffer::setFloatAtOffset(const float &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setVec2AtOffset(vec2 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setVec2AtOffset(const qvec2 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setVec3AtOffset(vec3 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setVec3AtOffset(const qvec3 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setVec4AtOffset(vec4 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */

void QuickBuffer::setVec4AtOffset(const qvec4 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setDoubleAtOffset(double value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setDoubleAtOffset(const double &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setDvec2AtOffset(dvec2 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setDvec2AtOffset(const qdvec2 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setDvec3AtOffset(dvec3 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setDvec3AtOffset(const qdvec3 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setDvec4AtOffset(dvec4 value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */

void QuickBuffer::setDvec4AtOffset(const qdvec4 &value, unsigned int offset)
{
    setTypeAtOffset(data->buffer, value, offset);
}

/*!
 * \qmlmethod void buffer::setIntAt(int value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setIntAt(const int &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setIvec2At(ivec2 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setIvec2At(const qivec2 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setIvec3At(ivec3 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setIvec3At(const qivec3 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setIvec4At(ivec4 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setIvec4At(const qivec4 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setUintAt(uint value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setUintAt(const uint &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setUvec2At(uvec2 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setUvec2At(const quvec2 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setUvec4At(uvec3 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setUvec3At(const quvec3 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setUvec4At(uvec4 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setUvec4At(const quvec4 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setFloatAt(float value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setFloatAt(const float &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setVec2At(vec2 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setVec2At(const qvec2 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setVec3At(vec3 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setVec3At(const qvec3 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setVec4At(vec4 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setVec4At(const qvec4 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setDoubleAt(double value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setDoubleAt(const double &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setDvec2At(dvec2 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setDvec2At(const qdvec2 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setDvec3At(dvec3 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setDvec3At(const qdvec3 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setDvec4At(dvec4 value, unsigned int index)
 *
 * Set the data at array index to the specified value
 */
void QuickBuffer::setDvec4At(const qdvec4 &value, unsigned int index)
{
    setTypeAt(data->buffer, value, index);
}

/*!
 * \qmlmethod void buffer::setBufferAtOffset(buffer value, unsigned int offset)
 *
 * Set the data at byte offset to the specified value
 */
void QuickBuffer::setBufferAtOffset(const QuickBuffer &value, unsigned int offset)
{
    if((offset+quint32(value.size()))>quint32(data->buffer.size()))
    {
        data->buffer.resize(offset+value.size());
    }

    memcpy(&data->buffer.data()[offset], QByteArray(value).constData(), value.size());
}

QuickBuffer::operator QByteArray() const
{
    return data->buffer;
}

/*!
 * \qmlproperty int buffer::size
 *
 * This property holds the buffer size in bytes
 */
int QuickBuffer::size() const
{
    return data->buffer.size();
}

void QuickBuffer::setSize(int size)
{
    data->buffer.resize(size);
}

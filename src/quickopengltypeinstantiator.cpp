/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quickopengltypeinstantiator.h"

/*!
 * \qmltype GL
 * \brief The GL QML singleton provides functions for working with OpenGL.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickOpenGLTypeInstantiator
 * \ingroup QuickOpenGL QML Singleton Types
 *
 * OpenGL Shader Language (GLSL) data type instantiators
 *
 * Example usage:
 *
 * \qml
 * import QuickOpenGL 1.0
 *
 * Item {
 *   property var myVec4: GL.vec4(0,1,2,3,4)
 * }
 * \endqml
 *
 */

QuickOpenGLTypeInstantiator::QuickOpenGLTypeInstantiator(QObject *parent) :
    QObject(parent)
{

}

/*!
 * \qmlmethod buffer GL::buffer()
 *
 * Constructs an empty buffer.
 */


/*!
 * \qmlmethod buffer GL::buffer(int reserveSize)
 *
 * Constructs an empty buffer with a reserved size of reserveSize.
 *
 * \sa QByteArray::reserve
 */

QuickBuffer QuickOpenGLTypeInstantiator::buffer(int reserveSize) const
{
    return QuickBuffer(reserveSize);
}


template <typename VEC2, typename TYPE> VEC2 instantiateVec2(const QVariant &a)
{
    if(a.canConvert<VEC2>())
    {
        return a.value<VEC2>();
    }

    return VEC2(a.value<TYPE>(), a.value<TYPE>());
}

template <typename VEC2, typename TYPE> VEC2 instantiateVec2(const QVariant &a, const QVariant &b)
{
    return VEC2(a.value<TYPE>(), b.value<TYPE>());
}

template <typename VEC3, typename TYPE> VEC3 instantiateVec3(const QVariant &a)
{
    if(a.canConvert<VEC3>())
    {
        return a.value<VEC3>();
    }

    return VEC3(a.value<TYPE>(), a.value<TYPE>(), a.value<TYPE>());
}

template <typename VEC2, typename VEC3, typename TYPE> VEC3 instantiateVec3(const QVariant &a, const QVariant &b)
{
    if(a.canConvert<VEC2>()&&b.canConvert<TYPE>())
    {
        return VEC3(a.value<VEC2>().x(), a.value<VEC2>().y(), b.value<TYPE>());
    }
    else if(a.canConvert<TYPE>()&&b.canConvert<VEC2>())
    {
        return VEC3(a.value<TYPE>(), b.value<VEC2>().x(), b.value<VEC2>().y());
    }

    return VEC3(std::numeric_limits<TYPE>::quiet_NaN(),std::numeric_limits<TYPE>::quiet_NaN(),std::numeric_limits<TYPE>::quiet_NaN());
}

template <typename VEC3, typename TYPE> VEC3 instantiateVec3(const QVariant &a, const QVariant &b, const QVariant &c)
{
    return VEC3(a.value<TYPE>(), b.value<TYPE>(), c.value<TYPE>());
}

template <typename VEC4, typename TYPE> VEC4 instantiateVec4(const QVariant &a)
{
    if(a.canConvert<VEC4>())
    {
        return a.value<VEC4>();
    }

    return VEC4(a.value<TYPE>(), a.value<TYPE>(), a.value<TYPE>(), a.value<TYPE>());
}

template <typename VEC2, typename VEC3, typename VEC4, typename TYPE> VEC4 instantiateVec4(const QVariant &a, const QVariant &b)
{
    if(a.canConvert<VEC2>()&&b.canConvert<VEC2>())
    {
        return VEC4(a.value<VEC2>().x(), a.value<VEC2>().y(), b.value<VEC2>().x(), a.value<VEC2>().y());
    }
    else if (a.canConvert<VEC3>()&&b.canConvert<TYPE>())
    {
        return VEC4(a.value<VEC3>().x(), a.value<VEC3>().y(), a.value<VEC3>().z(), b.value<TYPE>());
    }
    else if (a.canConvert<TYPE>()&&b.canConvert<VEC3>())
    {
        return VEC4(a.value<TYPE>(), b.value<VEC3>().x(), b.value<VEC3>().y(), b.value<VEC3>().z());
    }

    return VEC4(std::numeric_limits<TYPE>::quiet_NaN(),std::numeric_limits<TYPE>::quiet_NaN(),std::numeric_limits<TYPE>::quiet_NaN(),std::numeric_limits<TYPE>::quiet_NaN());
}

template <typename VEC2, typename VEC4, typename TYPE> VEC4 instantiateVec4(const QVariant &a, const QVariant &b, const QVariant &c)
{
    if(a.canConvert<TYPE>()&&b.canConvert<TYPE>()&&c.canConvert<VEC2>())
    {
        return VEC4(a.value<TYPE>(), b.value<TYPE>(), c.value<VEC2>().x(), c.value<VEC2>().y());
    }
    else if(a.canConvert<TYPE>()&&b.canConvert<VEC2>()&&c.canConvert<TYPE>())
    {
        return VEC4(a.value<TYPE>(), b.value<VEC2>().x(), b.value<VEC2>().y(), c.value<TYPE>());
    }
    else if(a.canConvert<VEC2>()&&b.canConvert<TYPE>()&&c.canConvert<TYPE>())
    {
        return VEC4(a.value<VEC2>().x(), a.value<VEC2>().y(), b.value<TYPE>(), c.value<TYPE>());
    }

    return VEC4(std::numeric_limits<TYPE>::quiet_NaN(),std::numeric_limits<TYPE>::quiet_NaN(),std::numeric_limits<TYPE>::quiet_NaN(),std::numeric_limits<TYPE>::quiet_NaN());
}

template <typename VEC4, typename TYPE> VEC4 instantiateVec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d)
{
    return VEC4(a.value<TYPE>(), b.value<TYPE>(), c.value<TYPE>(), d.value<TYPE>());
}

/*!
 * \qmlmethod vec2 GL::vec2()
 *
 * Constructs a vec2(0,0)
 */

/*!
 * \qmlmethod vec2 GL::vec2(var a)
 *
 * Constructs a vec2 from the specified value.
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li vec2
 *  \li vec2(a.x,a.y)
 * \row
 *  \li float
 *  \li vec2(a,a)
 * \endtable
 */

/*!
 * \qmlmethod vec2 GL::vec2(var x, var y)
 *
 * Constructs a vec2 with the specified x and y values.
 */

/*!
 * \qmlmethod ivec2 GL::ivec2()
 *
 * Constructs a ivec2(0,0)
 */

/*!
 * \qmlmethod ivec2 GL::ivec2(var a)
 *
 * Constructs a ivec2 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li ivec2
 *  \li ivec2(a.x,a.y)
 * \row
 *  \li int
 *  \li ivec2(a,a)
 * \endtable
 */

/*!
 * \qmlmethod ivec2 GL::ivec2(var x, var y)
 *
 * Constructs a ivec2 with the specified x and y values.
 */

/*!
 * \qmlmethod uvec2 GL::uvec2()
 *
 * Constructs a uvec2(0,0)
 */

/*!
 * \qmlmethod uvec2 GL::uvec2(var a)
 *
 * Constructs a uvec2 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li uvec2
 *  \li uvec2(a.x,a.y)
 * \row
 *  \li uint
 *  \li uvec2(a,a)
 * \endtable
 */

/*!
 * \qmlmethod uvec2 GL::uvec2(var x, var y)
 *
 * Constructs a uvec2 with the specified x and y values.
 */

/*!
 * \qmlmethod bvec2 GL::bvec2()
 *
 * Constructs a bvec2(0,0)
 */

/*!
 * \qmlmethod bvec2 GL::bvec2(var a)
 *
 * Constructs a bvec2 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li bvec2
 *  \li bvec2(a.x,a.y)
 * \row
 *  \li bool
 *  \li bvec2(a,a)
 * \endtable
 */

/*!
 * \qmlmethod bvec2 GL::bvec2(var x, var y)
 *
 * Constructs a bvec2 with the specified x and y values.
 */

/*!
 * \qmlmethod dvec2 GL::dvec2()
 *
 * Constructs a dvec2(0,0)
 */

/*!
 * \qmlmethod dvec2 GL::dvec2(var a)
 *
 * Constructs a dvec2 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li dvec2
 *  \li dvec2(a.x,a.y)
 * \row
 *  \li double
 *  \li dvec2(a,a)
 * \endtable
 */

/*!
 * \qmlmethod dvec2 GL::dvec2(var x, var y)
 *
 * Constructs a dvec2 with the specified x and y values.
 */
qvec2 QuickOpenGLTypeInstantiator::vec2() const
{
    return qvec2(0,0);
}

qvec2 QuickOpenGLTypeInstantiator::vec2(const QVariant &a) const
{
    return instantiateVec2<qvec2, float>(a);
}

qvec2 QuickOpenGLTypeInstantiator::vec2(const QVariant &a, const QVariant &b) const
{
    return instantiateVec2<qvec2, float>(a, b);
}

qivec2 QuickOpenGLTypeInstantiator::ivec2() const
{
    return qivec2(0,0);
}

qivec2 QuickOpenGLTypeInstantiator::ivec2(const QVariant &a) const
{
    return instantiateVec2<qivec2, qint32>(a);
}

qivec2 QuickOpenGLTypeInstantiator::ivec2(const QVariant &a, const QVariant &b) const
{
    return instantiateVec2<qivec2, qint32>(a, b);
}

quvec2 QuickOpenGLTypeInstantiator::uvec2() const
{
    return uvec2(0,0);
}

quvec2 QuickOpenGLTypeInstantiator::uvec2(const QVariant &a) const
{
    return instantiateVec2<quvec2, quint32>(a);
}

quvec2 QuickOpenGLTypeInstantiator::uvec2(const QVariant &a, const QVariant &b) const
{
    return instantiateVec2<quvec2, quint32>(a, b);
}

qbvec2 QuickOpenGLTypeInstantiator::bvec2() const
{
    return qbvec2(false,false);
}

qbvec2 QuickOpenGLTypeInstantiator::bvec2(const QVariant &a) const
{
    return instantiateVec2<qbvec2, bool>(a);
}

qbvec2 QuickOpenGLTypeInstantiator::bvec2(const QVariant &a, const QVariant &b) const
{
    return instantiateVec2<qbvec2, bool>(a, b);
}

qdvec2 QuickOpenGLTypeInstantiator::dvec2() const
{
    return qdvec2(0,0);
}

qdvec2 QuickOpenGLTypeInstantiator::dvec2(const QVariant &a) const
{
    return instantiateVec2<qdvec2, double>(a);
}

qdvec2 QuickOpenGLTypeInstantiator::dvec2(const QVariant &a, const QVariant &b) const
{
    return instantiateVec2<qdvec2, double>(a, b);
}

qvec3 QuickOpenGLTypeInstantiator::vec3() const
{
    return vec3(0,0,0);
}



/*!
 * \qmlmethod vec3 GL::vec3()
 *
 * Constructs a vec3(0, 0, 0)
 */

/*!
 * \qmlmethod vec3 GL::vec3(var a)
 *
 * Constructs a vec3 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li vec3
 *  \li vec3(a.x, a.y, a.z)
 * \row
 *  \li float
 *  \li vec3(a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod vec3 GL::vec3(var a, var b)
 *
 * Constructs a vec3 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li vec2
 *  \li float
 *  \li vec3(a.x, a.y, b)
 * \row
 *  \li float
 *  \li vec2
 *  \li vec3(a, b.x, b.y)
 * \endtable
 */

/*!
 * \qmlmethod vec3 GL::vec3(var x, var y, var z)
 *
 * Constructs a vec3 from the x and y and z variants
 */

/*!
 * \qmlmethod ivec3 GL::ivec3()
 *
 * Constructs a ivec3(0, 0, 0)
 */

/*!
 * \qmlmethod ivec3 GL::ivec3(var a)
 *
 * Constructs a ivec3 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li ivec3
 *  \li ivec3(a.x, a.y, a.z)
 * \row
 *  \li int
 *  \li ivec3(a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod ivec3 GL::ivec3(var a, var b)
 *
 * Constructs a ivec3 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li ivec2
 *  \li int
 *  \li ivec3(a.x, a.y, b)
 * \row
 *  \li int
 *  \li ivec2
 *  \li ivec3(a, b.x, b.y)
 * \endtable
 */

/*!
 * \qmlmethod ivec3 GL::ivec3(var x, var y, var z)
 *
 * Constructs a ivec3 from the x and y and z variants
 */


/*!
 * \qmlmethod uvec3 GL::uvec3()
 *
 * Constructs a uvec3(0, 0, 0)
 */

/*!
 * \qmlmethod uvec3 GL::uvec3(var a)
 *
 * Constructs a uvec3 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li uvec3
 *  \li uvec3(a.x, a.y, a.z)
 * \row
 *  \li uint
 *  \li uvec3(a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod uvec3 GL::uvec3(var a, var b)
 *
 * Constructs a uvec3 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li uvec2
 *  \li uint
 *  \li uvec3(a.x, a.y, b)
 * \row
 *  \li uint
 *  \li uvec2
 *  \li uvec3(a, b.x, b.y)
 * \endtable
 */

/*!
 * \qmlmethod uvec3 GL::uvec3(var x, var y, var z)
 *
 * Constructs a uvec3 from the x and y and z variants
 */

/*!
 * \qmlmethod bvec3 GL::bvec3()
 *
 * Constructs a bvec3(false, false, false)
 */

/*!
 * \qmlmethod bvec3 GL::bvec3(var a)
 *
 * Constructs a bvec3 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li bvec3
 *  \li bvec3(a.x, a.y, a.z)
 * \row
 *  \li bool
 *  \li bvec3(a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod bvec3 GL::bvec3(var a, var b)
 *
 * Constructs a bvec3 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li bvec2
 *  \li bool
 *  \li bvec3(a.x, a.y, b)
 * \row
 *  \li bool
 *  \li bvec2
 *  \li bvec3(a, b.x, b.y)
 * \endtable
 */

/*!
 * \qmlmethod bvec3 GL::bvec3(var x, var y, var z)
 *
 * Constructs a bvec3 from the x and y and z variants
 */


/*!
 * \qmlmethod dvec3 GL::dvec3()
 *
 * Constructs a dvec3(0, 0, 0)
 */

/*!
 * \qmlmethod dvec3 GL::dvec3(var a)
 *
 * Constructs a dvec3 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li dvec3
 *  \li dvec3(a.x, a.y, a.z)
 * \row
 *  \li double
 *  \li dvec3(a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod dvec3 GL::dvec3(var a, var b)
 *
 * Constructs a dvec3 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li dvec2
 *  \li double
 *  \li dvec3(a.x, a.y, b)
 * \row
 *  \li double
 *  \li dvec2
 *  \li dvec3(a, b.x, b.y)
 * \endtable
 */

/*!
 * \qmlmethod dvec3 GL::dvec3(var x, var y, var z)
 *
 * Constructs a dvec3 from the x and y and z variants
 */
qvec3 QuickOpenGLTypeInstantiator::vec3(const QVariant &a) const
{
    return instantiateVec3<qvec3, float>(a);
}

qvec3 QuickOpenGLTypeInstantiator::vec3(const QVariant &a, const QVariant &b) const
{
    return instantiateVec3<qvec2, qvec3, float>(a, b);
}

qvec3 QuickOpenGLTypeInstantiator::vec3(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec3<qvec3, float>(a, b, c);
}

qivec3 QuickOpenGLTypeInstantiator::ivec3() const
{
    return qivec3(0,0,0);
}

qivec3 QuickOpenGLTypeInstantiator::ivec3(const QVariant &a) const
{
    return instantiateVec3<qivec3, qint32>(a);
}

qivec3 QuickOpenGLTypeInstantiator::ivec3(const QVariant &a, const QVariant &b) const
{
    return instantiateVec3<qivec2, qivec3, qint32>(a, b);
}

qivec3 QuickOpenGLTypeInstantiator::ivec3(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec3<qivec3, qint32>(a, b, c);
}

quvec3 QuickOpenGLTypeInstantiator::uvec3() const
{
    return quvec3(0,0,0);
}

quvec3 QuickOpenGLTypeInstantiator::uvec3(const QVariant &a) const
{
    return instantiateVec3<quvec3, quint32>(a);
}

quvec3 QuickOpenGLTypeInstantiator::uvec3(const QVariant &a, const QVariant &b) const
{
    return instantiateVec3<quvec2, quvec3, quint32>(a, b);
}

quvec3 QuickOpenGLTypeInstantiator::uvec3(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec3<quvec3, quint32>(a, b, c);
}

qbvec3 QuickOpenGLTypeInstantiator::bvec3() const
{
    return qbvec3(false,false,false);
}

qbvec3 QuickOpenGLTypeInstantiator::bvec3(const QVariant &a) const
{
    return instantiateVec3<qbvec3, bool>(a);
}

qbvec3 QuickOpenGLTypeInstantiator::bvec3(const QVariant &a, const QVariant &b) const
{
    return instantiateVec3<qbvec2, qbvec3, bool>(a, b);
}

qbvec3 QuickOpenGLTypeInstantiator::bvec3(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec3<qbvec3, bool>(a, b, c);
}

qdvec3 QuickOpenGLTypeInstantiator::dvec3() const
{
    return qdvec3(0,0,0);
}

qdvec3 QuickOpenGLTypeInstantiator::dvec3(const QVariant &a) const
{
    return instantiateVec3<qdvec3, double>(a);
}

qdvec3 QuickOpenGLTypeInstantiator::dvec3(const QVariant &a, const QVariant &b) const
{
    return instantiateVec3<qdvec2, qdvec3, double>(a, b);
}

qdvec3 QuickOpenGLTypeInstantiator::dvec3(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec3<qdvec3, double>(a, b, c);
}



/*!
 * \qmlmethod vec4 GL::vec4()
 *
 * Constructs a vec4(0, 0, 0, 0)
 */

/*!
 * \qmlmethod vec4 GL::vec4(var a)
 *
 * Constructs a vec4 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li vec4
 *  \li vec4(a.x, a.y, a.z, a.w)
 * \row
 *  \li float
 *  \li vec4(a, a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod vec4 GL::vec4(var a, var b)
 *
 * Constructs a vec4 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li vec2
 *  \li vec2
 *  \li vec4(a.x, a.y, b.x, b.y)
 * \row
 *  \li float
 *  \li vec3
 *  \li vec4(a, b.x, b.y, b.z)
 * \row
 *  \li vec3
 *  \li float
 *  \li vec4(a.x, a.y, a.z, b)
 * \endtable
 */

/*!
 * \qmlmethod vec4 GL::vec4(var a, var b, var c)
 *
 * Constructs a vec4 from the a, b and c variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li c.canConvert
 *  \li Result
 *  \row
 *  \li vec2
 *  \li float
 *  \li float
 *  \li vec4(a.x, a.y, b, c)
 *  \row
 *  \li float
 *  \li vec2
 *  \li float
 *  \li vec4(a, b.x, b.y, c)
 *  \row
 *  \li float
 *  \li float
 *  \li vec2
 *  \li vec4(a, b, c.x, c.y)
 * \endtable
 */

/*!
 * \qmlmethod vec4 GL::vec4(var x, var y, var z, var w)
 *
 * Constructs a vec4(x, y, z, w).
 *
 */
qvec4 QuickOpenGLTypeInstantiator::vec4() const
{
    return qvec4(0,0,0,0);
}

qvec4 QuickOpenGLTypeInstantiator::vec4(const QVariant &a) const
{
    return instantiateVec4<qvec4, float>(a);
}

qvec4 QuickOpenGLTypeInstantiator::vec4(const QVariant &a, const QVariant &b) const
{
    return instantiateVec4<qvec2, qvec3, qvec4, float>(a, b);
}

qvec4 QuickOpenGLTypeInstantiator::vec4(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec4<qvec2, qvec4, float>(a, b, c);
}

qvec4 QuickOpenGLTypeInstantiator::vec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    return instantiateVec4<qvec4, float>(a, b, c, d);
}


/*!
 * \qmlmethod ivec4 GL::ivec4()
 *
 * Constructs a ivec4(0, 0, 0, 0)
 */

/*!
 * \qmlmethod ivec4 GL::ivec4(var a)
 *
 * Constructs a ivec4 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li ivec4
 *  \li ivec4(a.x, a.y, a.z, a.w)
 * \row
 *  \li int
 *  \li ivec4(a, a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod ivec4 GL::ivec4(var a, var b)
 *
 * Constructs a ivec4 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li ivec2
 *  \li ivec2
 *  \li ivec4(a.x, a.y, b.x, b.y)
 * \row
 *  \li int
 *  \li ivec3
 *  \li ivec4(a, b.x, b.y, b.z)
 * \row
 *  \li ivec3
 *  \li int
 *  \li ivec4(a.x, a.y, a.z, b)
 * \endtable
 */

/*!
 * \qmlmethod ivec4 GL::ivec4(var a, var b, var c)
 *
 * Constructs a ivec4 from the a, b and c variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li c.canConvert
 *  \li Result
 *  \row
 *  \li ivec2
 *  \li int
 *  \li int
 *  \li ivec4(a.x, a.y, b, c)
 *  \row
 *  \li int
 *  \li ivec2
 *  \li int
 *  \li ivec4(a, b.x, b.y, c)
 *  \row
 *  \li int
 *  \li int
 *  \li ivec2
 *  \li ivec4(a, b, c.x, c.y)
 * \endtable
 */

/*!
 * \qmlmethod ivec4 GL::ivec4(var x, var y, var z, var w)
 *
 * Constructs a ivec4(x, y, z, w).
 *
 */
qivec4 QuickOpenGLTypeInstantiator::ivec4() const
{
    return qivec4(0,0,0,0);
}

qivec4 QuickOpenGLTypeInstantiator::ivec4(const QVariant &a) const
{
    return instantiateVec4<qivec4, qint32>(a);
}

qivec4 QuickOpenGLTypeInstantiator::ivec4(const QVariant &a, const QVariant &b) const
{
    return instantiateVec4<qivec2, qivec3, qivec4, qint32>(a, b);
}

qivec4 QuickOpenGLTypeInstantiator::ivec4(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec4<qivec2, qivec4, qint32>(a, b, c);
}

qivec4 QuickOpenGLTypeInstantiator::ivec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    return instantiateVec4<qivec4, qint32>(a, b, c, d);
}

/*!
 * \qmlmethod uvec4 GL::uvec4()
 *
 * Constructs a uvec4(0, 0, 0, 0)
 */

/*!
 * \qmlmethod uvec4 GL::uvec4(var a)
 *
 * Constructs a uvec4 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li uvec4
 *  \li uvec4(a.x, a.y, a.z, a.w)
 * \row
 *  \li uint
 *  \li uvec4(a, a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod uvec4 GL::uvec4(var a, var b)
 *
 * Constructs a uvec4 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li uvec2
 *  \li uvec2
 *  \li uvec4(a.x, a.y, b.x, b.y)
 * \row
 *  \li uint
 *  \li uvec3
 *  \li uvec4(a, b.x, b.y, b.z)
 * \row
 *  \li uvec3
 *  \li uint
 *  \li uvec4(a.x, a.y, a.z, b)
 * \endtable
 */

/*!
 * \qmlmethod uvec4 GL::uvec4(var a, var b, var c)
 *
 * Constructs a uvec4 from the a, b and c variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li c.canConvert
 *  \li Result
 *  \row
 *  \li uvec2
 *  \li uint
 *  \li uint
 *  \li uvec4(a.x, a.y, b, c)
 *  \row
 *  \li uint
 *  \li uvec2
 *  \li uint
 *  \li uvec4(a, b.x, b.y, c)
 *  \row
 *  \li uint
 *  \li uint
 *  \li uvec2
 *  \li uvec4(a, b, c.x, c.y)
 * \endtable
 */

/*!
 * \qmlmethod uvec4 GL::uvec4(var x, var y, var z, var w)
 *
 * Constructs a uvec4(x, y, z, w).
 *
 */

quvec4 QuickOpenGLTypeInstantiator::uvec4() const
{
    return quvec4(0,0,0,0);
}

quvec4 QuickOpenGLTypeInstantiator::uvec4(const QVariant &a) const
{
    return instantiateVec4<quvec4, quint32>(a);
}

quvec4 QuickOpenGLTypeInstantiator::uvec4(const QVariant &a, const QVariant &b) const
{
    return instantiateVec4<quvec2, quvec3, quvec4, quint32>(a, b);
}

quvec4 QuickOpenGLTypeInstantiator::uvec4(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec4<quvec2, quvec4, quint32>(a,b,c);
}

quvec4 QuickOpenGLTypeInstantiator::uvec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    return instantiateVec4<quvec4, quint32>(a,b,c,d);
}

/*!
 * \qmlmethod bvec4 GL::bvec4()
 *
 * Constructs a bvec4(0, 0, 0, 0)
 */

/*!
 * \qmlmethod bvec4 GL::bvec4(var a)
 *
 * Constructs a bvec4 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li bvec4
 *  \li bvec4(a.x, a.y, a.z, a.w)
 * \row
 *  \li bool
 *  \li bvec4(a, a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod bvec4 GL::bvec4(var a, var b)
 *
 * Constructs a bvec4 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li bvec2
 *  \li bvec2
 *  \li bvec4(a.x, a.y, b.x, b.y)
 * \row
 *  \li bool
 *  \li bvec3
 *  \li bvec4(a, b.x, b.y, b.z)
 * \row
 *  \li bvec3
 *  \li bool
 *  \li bvec4(a.x, a.y, a.z, b)
 * \endtable
 */

/*!
 * \qmlmethod bvec4 GL::bvec4(var a, var b, var c)
 *
 * Constructs a bvec4 from the a, b and c variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li c.canConvert
 *  \li Result
 *  \row
 *  \li bvec2
 *  \li bool
 *  \li bool
 *  \li bvec4(a.x, a.y, b, c)
 *  \row
 *  \li bool
 *  \li bvec2
 *  \li bool
 *  \li bvec4(a, b.x, b.y, c)
 *  \row
 *  \li bool
 *  \li bool
 *  \li bvec2
 *  \li bvec4(a, b, c.x, c.y)
 * \endtable
 */

/*!
 * \qmlmethod bvec4 GL::bvec4(var x, var y, var z, var w)
 *
 * Constructs a bvec4(x, y, z, w).
 *
 */
qbvec4 QuickOpenGLTypeInstantiator::bvec4() const
{
    return(qbvec4(false,false,false,false));
}

qbvec4 QuickOpenGLTypeInstantiator::bvec4(const QVariant &a) const
{
    return instantiateVec4<qbvec4, bool>(a);
}

qbvec4 QuickOpenGLTypeInstantiator::bvec4(const QVariant &a, const QVariant &b) const
{
    return instantiateVec4<qbvec2, qbvec3, qbvec4, bool>(a, b);
}

qbvec4 QuickOpenGLTypeInstantiator::bvec4(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec4<qbvec2, qbvec4, bool>(a, b, c);
}

qbvec4 QuickOpenGLTypeInstantiator::bvec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    return instantiateVec4<qbvec4, bool>(a, b, c, d);
}

/*!
 * \qmlmethod dvec4 GL::dvec4()
 *
 * Constructs a dvec4(0, 0, 0, 0)
 */

/*!
 * \qmlmethod dvec4 GL::dvec4(var a)
 *
 * Constructs a dvec4 from the specified value
 *
 * \table
 * \header
 *  \li  a.canConvert
 *  \li Result
 *  \row
 *  \li dvec4
 *  \li dvec4(a.x, a.y, a.z, a.w)
 * \row
 *  \li double
 *  \li dvec4(a, a, a, a)
 * \endtable
 */

/*!
 * \qmlmethod dvec4 GL::dvec4(var a, var b)
 *
 * Constructs a dvec4 from the a and b variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li Result
 *  \row
 *  \li dvec2
 *  \li dvec2
 *  \li dvec4(a.x, a.y, b.x, b.y)
 * \row
 *  \li double
 *  \li dvec3
 *  \li dvec4(a, b.x, b.y, b.z)
 * \row
 *  \li dvec3
 *  \li double
 *  \li dvec4(a.x, a.y, a.z, b)
 * \endtable
 */

/*!
 * \qmlmethod dvec4 GL::dvec4(var a, var b, var c)
 *
 * Constructs a dvec4 from the a, b and c variants.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li b.canConvert
 *  \li c.canConvert
 *  \li Result
 *  \row
 *  \li dvec2
 *  \li double
 *  \li double
 *  \li dvec4(a.x, a.y, b, c)
 *  \row
 *  \li double
 *  \li dvec2
 *  \li double
 *  \li dvec4(a, b.x, b.y, c)
 *  \row
 *  \li double
 *  \li double
 *  \li dvec2
 *  \li dvec4(a, b, c.x, c.y)
 * \endtable
 */

/*!
 * \qmlmethod dvec4 GL::dvec4(var x, var y, var z, var w)
 *
 * Constructs a dvec4(x, y, z, w).
 *
 */
qdvec4 QuickOpenGLTypeInstantiator::dvec4() const
{
    return dvec4(0,0,0,0);
}

qdvec4 QuickOpenGLTypeInstantiator::dvec4(const QVariant &a) const
{
    return instantiateVec4<qdvec4, double>(a);
}

qdvec4 QuickOpenGLTypeInstantiator::dvec4(const QVariant &a, const QVariant &b) const
{
    return instantiateVec4<qdvec2, qdvec3, qdvec4, double>(a, b);
}

qdvec4 QuickOpenGLTypeInstantiator::dvec4(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    return instantiateVec4<qdvec2, qdvec4, double>(a, b, c);
}

qdvec4 QuickOpenGLTypeInstantiator::dvec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    return instantiateVec4<qdvec4, double>(a, b, c, d);
}

qmat3x2 QuickOpenGLTypeInstantiator::mat3x2() const
{
    return qmat3x2();
}

qmat3x2 QuickOpenGLTypeInstantiator::mat3x2(const QVariant &a) const
{
    if(a.canConvert<qmat3x2>())
    {
        return a.value<qmat3x2>();
    }

    return qmat3x2(a.value<float>());
}

qmat3x2 QuickOpenGLTypeInstantiator::mat3x2(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    if(a.canConvert<qvec2>()&&b.canConvert<qvec2>()&&c.canConvert<qvec2>())
    {
        return qmat3x2(a.value<qvec2>(), b.value<qvec2>(), c.value<qvec2>());
    }

    return qmat3x2(std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                   std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                   std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN());
}

qmat3x2 QuickOpenGLTypeInstantiator::mat3x2(float a, float b, float c, float d, float e, float f)
{
    return qmat3x2(a,b,c,d,e,f);
}

qdmat3x2 QuickOpenGLTypeInstantiator::dmat3x2() const
{
    return qdmat3x2();
}

qdmat3x2 QuickOpenGLTypeInstantiator::dmat3x2(const QVariant &a) const
{
    if(a.canConvert<qdmat3x2>())
    {
        return a.value<qdmat3x2>();
    }

    return qdmat3x2(a.value<double>());
}

qdmat3x2 QuickOpenGLTypeInstantiator::dmat3x2(const QVariant &a, const QVariant &b, const QVariant &c) const
{
    if(a.canConvert<qdvec2>()&&b.canConvert<qdvec2>()&&c.canConvert<qdvec2>())
    {
        return qdmat3x2(a.value<qdvec2>(), b.value<qdvec2>(), c.value<qdvec2>());
    }

    return qdmat3x2(std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN());
}

qdmat3x2 QuickOpenGLTypeInstantiator::dmat3x2(double a, double b, double c, double d, double e, double f)
{
    return qdmat3x2(a,b,c,d,e,f);
}

qmat4x2 QuickOpenGLTypeInstantiator::mat4x2() const
{
    return qmat4x2();
}

qmat4x2 QuickOpenGLTypeInstantiator::mat4x2(const QVariant &a) const
{
    if(a.canConvert<qmat4x2>())
    {
        return a.value<qmat4x2>();
    }

    return qmat4x2(a.value<float>());
}

qmat4x2 QuickOpenGLTypeInstantiator::mat4x2(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    if(a.canConvert<qvec2>()&&b.canConvert<qvec2>()&&c.canConvert<qvec2>()&&d.canConvert<qvec2>())
    {
        return qmat4x2(a.value<qvec2>(), b.value<qvec2>(), c.value<qvec2>(), d.value<qvec2>());
    }

    return qmat4x2(std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                   std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                   std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                   std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN());

}

qmat4x2 QuickOpenGLTypeInstantiator::mat4x2(float a, float b, float c, float d, float e, float f, float g, float h)
{
    return qmat4x2(a,b,c,d,e,f,g,h);
}

/*!
 * \qmlmethod dmat4x2 GL::mat4x2()
 *
 * Constructs a dmat4x2 identity matrix.
 */

qdmat4x2 QuickOpenGLTypeInstantiator::dmat4x2() const
{
    return qdmat4x2();
}

/*!
 * \qmlmethod dmat4x2 GL::mat4x2(var a)
 *
 * Constructs a mat4x2 with the specified column values, as per GLSL mat4x2.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li Result
 *  \row
 *  \li mat4x2
 *  \li mat4x2(a.m00, a.m10, a.m01, a.m11, a.m02, a.m12, a.m03, a.m13)
 *  \row
 *  \li float
 *  \li mat4(a, 0, 0, a, 0, 0, 0, 0)
 * \endtable
 */
qdmat4x2 QuickOpenGLTypeInstantiator::dmat4x2(const QVariant &a) const
{
    if(a.canConvert<qdmat4x2>())
    {
        return a.value<qdmat4x2>();
    }

    return qdmat4x2(a.value<double>());
}

qdmat4x2 QuickOpenGLTypeInstantiator::dmat4x2(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    if(a.canConvert<qdvec2>()&&b.canConvert<qdvec2>()&&c.canConvert<qdvec2>()&&d.canConvert<qdvec2>())
    {
        return qdmat4x2(a.value<qdvec2>(), b.value<qdvec2>(), c.value<qdvec2>(), d.value<qdvec2>());
    }

    return qdmat4x2(std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN());

}

qdmat4x2 QuickOpenGLTypeInstantiator::dmat4x2(double a, double b, double c, double d, double e, double f, double g, double h)
{
    return qdmat4x2(a,b,c,d,e,f,g,h);
}

qmat4x3 QuickOpenGLTypeInstantiator::mat4x3() const
{
    return qmat4x3();
}

qmat4x3 QuickOpenGLTypeInstantiator::mat4x3(const QVariant &a) const
{
    if(a.canConvert<qmat4x3>())
    {
        return a.value<qmat4x3>();
    }

    return qmat4x3(a.value<float>());
}

qmat4x3 QuickOpenGLTypeInstantiator::mat4x3(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    if(a.canConvert<qvec3>()&&b.canConvert<qvec3>()&&c.canConvert<qvec3>()&&d.canConvert<qvec3>())
    {
        return qmat4x3(a.value<qvec3>(), b.value<qvec3>(), c.value<qvec3>(), d.value<qvec3>());
    }

    return qmat4x3(std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN());

}

qmat4x3 QuickOpenGLTypeInstantiator::mat4x3(float a, float b, float c, float d, float e, float f, float g, float h, float i, float j, float k, float l)
{
    return qmat4x3(a,b,c,d,e,f,g,h,i,j,k,l);
}


qdmat4x3 QuickOpenGLTypeInstantiator::dmat4x3() const
{
    return qdmat4x3();
}

qdmat4x3 QuickOpenGLTypeInstantiator::dmat4x3(const QVariant &a) const
{
    if(a.canConvert<qdmat4x3>())
    {
        return a.value<qdmat4x3>();
    }

    return qdmat4x3(a.value<double>());
}

qdmat4x3 QuickOpenGLTypeInstantiator::dmat4x3(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    if(a.canConvert<qdvec3>()&&b.canConvert<qdvec3>()&&c.canConvert<qdvec3>()&&d.canConvert<qdvec3>())
    {
        return qdmat4x3(a.value<qdvec3>(), b.value<qdvec3>(), c.value<qdvec3>(), d.value<qdvec3>());
    }

    return qdmat4x3(std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                   std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN());

}

qdmat4x3 QuickOpenGLTypeInstantiator::dmat4x3(double a, double b, double c, double d, double e, double f, double g, double h, double i, double j, double k, double l)
{
    return qdmat4x3(a,b,c,d,e,f,g,h,i,j,k,l);
}

qmat2 QuickOpenGLTypeInstantiator::mat2() const
{
    return qmat2();
}

qmat2 QuickOpenGLTypeInstantiator::mat2(const QVariant &a) const
{
    if(a.canConvert<qmat2>())
    {
        return a.value<qmat2>();
    }

    return qmat2(a.value<float>());
}

qmat2 QuickOpenGLTypeInstantiator::mat2(const QVariant &a, const QVariant &b) const
{
    if(a.canConvert<qvec2>()&&b.canConvert<qvec2>())
    {
        return qmat2(a.value<qvec2>(), b.value<qvec2>());
    }

    return qmat2(std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                 std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN());

}

qmat2 QuickOpenGLTypeInstantiator::mat2(float a, float b, float c, float d) const
{
    return qmat2(a,b,c,d);
}

/*!
 * \qmlmethod mat4 GL::mat4()
 *
 * Constructs a mat4 identity matrix.
 */
qmat4 QuickOpenGLTypeInstantiator::mat4() const
{
    return qmat4();
}

/*!
 * \qmlmethod mat4 GL::mat4(var a)
 *
 * Constructs a mat4 with the specified column values, as per GLSL mat4.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li Result
 *  \row
 *  \li mat4
 *  \li mat4(a.m00, a.m10, a.m20, a.m30, a.m01, a.m11, a.m21, a.m31, a.m02, a.m12, a.m22, a.m32, a.m03, a.m13, a.m23, a.m33)
 *  \row
 *  \li float
 *  \li mat4(a, 0, 0, 0, 0, a, 0, 0, 0, 0, a, 0, 0, 0, 0, a)
 * \endtable
 */
qmat4 QuickOpenGLTypeInstantiator::mat4(const QVariant &a) const
{
    if(a.canConvert<qmat4>())
    {
        return a.value<qmat4>();
    }

    return qmat4(a.value<float>());
}

/*!
 * \qmlmethod mat4 GL::mat4(vec4 a, vec4 b, vec4 c, vec4 d)
 *
 * Constructs a mat4 with the specified column values, as per GLSL mat4.
 */
qmat4 QuickOpenGLTypeInstantiator::mat4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    if(a.canConvert<qvec4>()&&b.canConvert<qvec4>()&&c.canConvert<qvec4>()&&d.canConvert<qvec4>())
    {
        return qmat4(a.value<qvec4>(), b.value<qvec4>(), c.value<qvec4>(), d.value<qvec4>());
    }

    return qmat4(std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                 std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                 std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),
                 std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN());
}


/*!
 * \qmlmethod mat4 GL::mat4(float a, float b, float c, float d, float e, float f, float g, float h, float i, float j, float k, float l, float m, float n, float o, float p)
 *
 * Constructs a mat4 with the specified values.  The values are filled column wise as per GLSL mat4.
 */
qmat4 QuickOpenGLTypeInstantiator::mat4(float a, float b, float c, float d, float e, float f, float g, float h, float i, float j, float k, float l, float m, float n, float o, float p) const
{
    return qmat4(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p);
}

/*!
 * \qmlmethod dmat4 GL::dmat4()
 *
 * Constructs a dmat4 identity matrix.
 */
qdmat4 QuickOpenGLTypeInstantiator::dmat4() const
{
    return qdmat4();
}

/*!
 * \qmlmethod dmat4 GL::dmat4(var a)
 *
 * Constructs a dmat4 with the specified column values, as per GLSL dmat4.
 *
 * \table
 * \header
 *  \li a.canConvert
 *  \li Result
 *  \row
 *  \li dmat4
 *  \li dmat4(a.m00, a.m10, a.m20, a.m30, a.m01, a.m11, a.m21, a.m31, a.m02, a.m12, a.m22, a.m32, a.m03, a.m13, a.m23, a.m33)
 *  \row
 *  \li double
 *  \li dmat4(a, 0, 0, 0, 0, a, 0, 0, 0, 0, a, 0, 0, 0, 0, a)
 * \endtable
 */
qdmat4 QuickOpenGLTypeInstantiator::dmat4(const QVariant &a) const
{
    if(a.canConvert<qdmat4>())
    {
        return a.value<qdmat4>();
    }

    return qdmat4(a.value<double>());

}

/*!
 * \qmlmethod dmat4 GL::dmat4(dvec4 a, dvec4 b, dvec4 c, dvec4 d)
 *
 * Constructs a dmat4 with the specified column values, as per GLSL dmat4.
 */
qdmat4 QuickOpenGLTypeInstantiator::dmat4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const
{
    if(a.canConvert<qdvec4>()&&b.canConvert<qdvec4>()&&c.canConvert<qdvec4>()&&d.canConvert<qdvec4>())
    {
        return qdmat4(a.value<qdvec4>(), b.value<qdvec4>(), c.value<qdvec4>(), d.value<qdvec4>());
    }

    return qdmat4(std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                  std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                  std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),
                  std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN(),std::numeric_limits<double>::quiet_NaN());

}

/*!
 * \qmlmethod dmat4 GL::dmat4(double a, double b, double c, double d, double e, double f, double g, double h, double i, double j, double k, double l, double m, double n, double o, double p)
 *
 * Constructs a dmat4 with the specified values.  The values are filled column wise as per GLSL dmat4.
 */
qdmat4 QuickOpenGLTypeInstantiator::dmat4(double a, double b, double c, double d, double e, double f, double g, double h, double i, double j, double k, double l, double m, double n, double o, double p) const
{
    return qdmat4(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p);
}




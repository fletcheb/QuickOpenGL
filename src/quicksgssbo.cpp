/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgssbo.h"
#include <QOpenGLBuffer>
#include <QOpenGLContext>
#include <QOpenGLFunctions_4_3_Core>
#include <QtConcurrent>
#include "support.h"
#include "quickshaderstoragebuffer.h"
#include "quickopenglthreadpool.h"
#include <QElapsedTimer>

class QuickSGSSBOPrivate : public QObject
{
    Q_OBJECT

public:

    QuickSGSSBOPrivate(QuickSGSSBO *q);

    QOpenGLBuffer buffer;
    GLenum usagePattern;
    int size;
    bool synced;

    static QByteArray readSsbo(GLsync fenceSync, GLuint buffer, QObject *ssbo, GLenum usagePattern);

protected:

    QuickSGSSBO *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickSGSSBO)
};

QuickSGSSBOPrivate::QuickSGSSBOPrivate(QuickSGSSBO *q) :
    buffer(QOpenGLBuffer::Type(GL_SHADER_STORAGE_BUFFER)),
    usagePattern(GL_DYNAMIC_DRAW),
    size(0),
    synced(false),
    q_ptr(q)
{
}

QByteArray QuickSGSSBOPrivate::readSsbo(GLsync fenceSync, GLuint buffer, QObject *ssbo, GLenum usagePattern)
{
    Q_UNUSED(ssbo);
    Q_UNUSED(usagePattern);
#ifndef QT_OPENGL_ES

    // get the OpenGL functions object
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    bool ok = gl43->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);


#ifdef QT_DEBUG
    QScopedPointer<QElapsedTimer> timer;
    qint64 syncTime =0;

    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        timer.reset(new QElapsedTimer);
        timer->start();
    }
#endif

    Q_ASSERT(gl43->glIsSync(fenceSync));

    // wait for up to 1s for the fence sync
    GLenum result = gl43->glClientWaitSync(fenceSync, 0, 1000000000);debugGL;

    Q_UNUSED(result);

    gl43->glDeleteSync(fenceSync);debugGL;
#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        syncTime = timer->nsecsElapsed();
    }
#endif

    Q_ASSERT(gl43->glIsBuffer(buffer));

    // bind the buffer
    gl43->glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer);debugGL;

    // get the buffer size
    GLint size=0;
    gl43->glGetBufferParameteriv(GL_SHADER_STORAGE_BUFFER, GL_BUFFER_SIZE, &size);debugGL;

    Q_ASSERT(size>0);

    // create a byte array to hold the result
    QByteArray data(size, 0);

    // read the buffer data
    if(size>0)
    {
        gl43->glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, size, data.data());debugGL;
    }

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        qint64 elapsed = timer->nsecsElapsed();
        qDebug().nospace() << "quickopengl.scenegraph.time.shaderstoragebuffer.read: bufferId " << buffer << " " << ssbo << " usage pattern " << QuickOpenGL::GLenum(usagePattern) << " size " << size << " bytes took " << elapsed*1e-6 << "ms (wait sync: " << syncTime*1e-6 << "ms)";
    }
#endif

    // unbind the buffer
    gl43->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);debugGL;

    // return the result
    return data;
#else
    return QByteArray();
#endif

}

QuickSGSSBO::QuickSGSSBO(QObject *parent) :
    QObject(parent),
    d_ptr(new QuickSGSSBOPrivate(this))
{
}

QuickSGSSBO::~QuickSGSSBO()
{
}

uint QuickSGSSBO::bufferId() const
{
    Q_D(const QuickSGSSBO);

    return d->buffer.bufferId();
}

void QuickSGSSBO::bind()
{

   Q_D(QuickSGSSBO);

    if(!d->buffer.isCreated())
    {
        d->buffer.create();
        emit bufferIdChanged(d->buffer.bufferId());
    }

    d->buffer.bind();debugGL;

}

void QuickSGSSBO::release()
{
    Q_D(QuickSGSSBO);

    if(d->buffer.isCreated())
    {
        d->buffer.release();debugGL;
    }

}

void QuickSGSSBO::bindBufferBase(uint bindingIndex)
{
#ifndef QT_OPENGL_ES
    Q_D(QuickSGSSBO);

    // get the OpenGL functions object
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    bool ok = gl43->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl43->glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingIndex, d->buffer.bufferId());debugGL;
#endif
}

void QuickSGSSBO::bufferData(const QByteArray &data)
{
#ifndef QT_OPENGL_ES
    Q_D(QuickSGSSBO);

    if(!d->buffer.isCreated())
    {
        d->buffer.create();
        emit bufferIdChanged(d->buffer.bufferId());
    }

    // bind the buffer
    bind();

    // get the OpenGL functions object
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    bool ok = gl43->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl43->glBufferData(GL_SHADER_STORAGE_BUFFER, data.count(), data.constData(), d->usagePattern);debugGL;
#endif
}

void QuickSGSSBO::bufferData(int size)
{
    QByteArray data(size,0);
    bufferData(data);
}

QFuture<QByteArray> QuickSGSSBO::getBufferData()
{
    #ifndef QT_OPENGL_ES

    Q_D(QuickSGSSBO);

    // get the OpenGL functions object
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    bool ok = gl43->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    // place a fence in the GL command stream
    GLsync fenceSync = gl43->glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);debugGL;
    Q_ASSERT(fenceSync);

    Q_ASSERT(gl43->glIsBuffer(bufferId()));

    // read from the buffer in the QuickOpenGLThreadPool thread
    return QtConcurrent::run(QuickOpenGLThreadPool::globalInstance(), &QuickSGSSBOPrivate::readSsbo, fenceSync, bufferId(), this->property("QmlItem").value<QObject *>(), d->usagePattern);

#else

    return QFuture<QByteArray>();

#endif
}

int QuickSGSSBO::size()
{
    #ifndef QT_OPENGL_ES
    bind();

    // get the OpenGL functions object
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    bool ok = gl43->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    GLint size=0;
    gl43->glGetBufferParameteriv(GL_SHADER_STORAGE_BUFFER, GL_BUFFER_SIZE, &size);debugGL;

    return size;
#else

    return 0;
#endif


}

int QuickSGSSBO::usagePattern() const
{
    Q_D(const QuickSGSSBO);

    return d->usagePattern;
}

bool QuickSGSSBO::synced() const
{
    Q_D(const QuickSGSSBO);

    return d->synced;
}

void QuickSGSSBO::setSize(int size)
{
    Q_D(QuickSGSSBO);

    if (d->size == size)
        return;

    d->size = size;
    emit sizeChanged(size);
}

void QuickSGSSBO::setUsagePattern(GLenum usage)
{
    Q_D(QuickSGSSBO);

    if (d->usagePattern == usage)
        return;

    d->usagePattern = usage;
    emit usagePatternChanged(usage);
}

void QuickSGSSBO::setSynced(bool synced)
{
    Q_D(QuickSGSSBO);

    d->synced = synced;
}

#include "quicksgssbo.moc"

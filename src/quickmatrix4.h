#ifndef QUICKMATRIX4_H
#define QUICKMATRIX4_H


#include <QMatrix4x4>
#include <QDebug>
#include "quickvector2.h"
#include "quickvector3.h"
#include "quickvector4.h"
#include <cmath>
#include <QtMath>


template <typename Type, typename RowVector, typename ColumnVector> class matrix4x2 : public QGenericMatrix<4,2,Type>
{
public:
    matrix4x2(){}

    matrix4x2(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix4x2(const ColumnVector &a, const ColumnVector &b, const ColumnVector &c, const ColumnVector &d)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();

        this->operator()(0,2) = c.x();
        this->operator()(1,2) = c.y();

        this->operator()(0,3) = d.x();
        this->operator()(1,3) = d.y();
    }

    // column wise intitialization as per GLSL
    matrix4x2(Type a, Type b, // first column
            Type c, Type d, // second column
            Type e, Type f, // third column
            Type g, Type h) // fourth column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;

        this->operator()(0,1) = c;
        this->operator()(1,1) = d;

        this->operator()(0,2) = e;
        this->operator()(1,2) = f;

        this->operator()(0,3) = g;
        this->operator()(1,3) = h;
    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m02() const {return this->operator()(0, 2);}
    Type m03() const {return this->operator()(0, 3);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}
    Type m12() const {return this->operator()(1, 2);}
    Type m13() const {return this->operator()(1, 3);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM02(Type value) {this->operator()(0, 2) = value;}
    void setM03(Type value) {this->operator()(0, 3) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}
    void setM12(Type value) {this->operator()(1, 2) = value;}
    void setM13(Type value) {this->operator()(1, 3) = value;}

    RowVector r0() const {return RowVector(this->operator()(0, 0), this->operator()(0, 1), this->operator()(0, 2), this->operator()(0, 3));}
    RowVector r1() const {return RowVector(this->operator()(1, 0), this->operator()(1, 1), this->operator()(1, 2), this->operator()(1, 3));}

    void setR0(const RowVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y(); this->operator()(0, 2) = value.z(); this->operator()(0, 3)=value.w();}
    void setR1(const RowVector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(1, 2) = value.z(); this->operator()(1, 3)=value.w();}

    ColumnVector c0() const {return ColumnVector(this->operator()(0, 0), this->operator()(1, 0));}
    ColumnVector c1() const {return ColumnVector(this->operator()(0, 1), this->operator()(1, 1));}
    ColumnVector c2() const {return ColumnVector(this->operator()(0, 2), this->operator()(1, 2));}
    ColumnVector c3() const {return ColumnVector(this->operator()(0, 3), this->operator()(1, 3));}

    void setC0(const ColumnVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y(); }
    void setC1(const ColumnVector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y(); }
    void setC2(const ColumnVector & value) {this->operator()(0, 2) =  value.x(); this->operator()(1, 2) = value.y(); }
    void setC3(const ColumnVector & value) {this->operator()(0, 3) =  value.x(); this->operator()(1, 3) = value.y(); }

};

class qmat4x2 : public matrix4x2<float, qvec4, qvec2>
{
    Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m02 READ m02 WRITE setM02)
    Q_PROPERTY(float m03 READ m03 WRITE setM03)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)
    Q_PROPERTY(float m12 READ m12 WRITE setM12)
    Q_PROPERTY(float m13 READ m13 WRITE setM13)

    Q_PROPERTY(qvec4 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec4 r1 READ r1 WRITE setR1)

    Q_PROPERTY(qvec2 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec2 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qvec2 c2 READ c2 WRITE setC2)
    Q_PROPERTY(qvec2 c3 READ c3 WRITE setC3)

public:

    using matrix4x2<float, qvec4, qvec2>::matrix4x2;
    using QGenericMatrix<4,2,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

};

class qdmat4x2 : public matrix4x2<double, qdvec4, qdvec2>
{
    Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m02 READ m02 WRITE setM02)
    Q_PROPERTY(double m03 READ m03 WRITE setM03)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)
    Q_PROPERTY(double m12 READ m12 WRITE setM12)
    Q_PROPERTY(double m13 READ m13 WRITE setM13)

    Q_PROPERTY(qdvec4 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec4 r1 READ r1 WRITE setR1)

    Q_PROPERTY(qdvec2 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec2 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qdvec2 c2 READ c2 WRITE setC2)
    Q_PROPERTY(qdvec2 c3 READ c3 WRITE setC3)

public:

    using matrix4x2<double, qdvec4, qdvec2>::matrix4x2;
    using QGenericMatrix<4,2,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

};

template <typename Type, typename RowVector, typename ColumnVector> class matrix4x3 : public QGenericMatrix<4,3,Type>
{
public:

    matrix4x3(){}

    matrix4x3(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix4x3(const ColumnVector &a, const ColumnVector &b, const ColumnVector &c, const ColumnVector &d)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();
        this->operator()(2,0) = a.z();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();
        this->operator()(2,1) = b.z();

        this->operator()(0,2) = c.x();
        this->operator()(1,2) = c.y();
        this->operator()(2,2) = c.z();

        this->operator()(0,3) = d.x();
        this->operator()(1,3) = d.y();
        this->operator()(2,3) = d.z();

    }

    // column wise intitialization as per GLSL
    matrix4x3(Type a, Type b, Type c, // first column
            Type d, Type e, Type f, // second column
            Type g, Type h, Type i, // third column
            Type j, Type k, Type l) // fourth column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;
        this->operator()(2,0) = c;

        this->operator()(0,1) = d;
        this->operator()(1,1) = e;
        this->operator()(2,1) = f;

        this->operator()(0,2) = g;
        this->operator()(1,2) = h;
        this->operator()(2,2) = i;

        this->operator()(0,3) = j;
        this->operator()(1,3) = k;
        this->operator()(2,3) = l;

    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m02() const {return this->operator()(0, 2);}
    Type m03() const {return this->operator()(0, 3);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}
    Type m12() const {return this->operator()(1, 2);}
    Type m13() const {return this->operator()(1, 3);}
    Type m20() const {return this->operator()(2, 0);}
    Type m21() const {return this->operator()(2, 1);}
    Type m22() const {return this->operator()(2, 2);}
    Type m23() const {return this->operator()(2, 3);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM02(Type value) {this->operator()(0, 2) = value;}
    void setM03(Type value) {this->operator()(0, 3) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}
    void setM12(Type value) {this->operator()(1, 2) = value;}
    void setM13(Type value) {this->operator()(1, 3) = value;}
    void setM20(Type value) {this->operator()(2, 0) = value;}
    void setM21(Type value) {this->operator()(2, 1) = value;}
    void setM22(Type value) {this->operator()(2, 2) = value;}
    void setM23(Type value) {this->operator()(2, 3) = value;}

    RowVector r0() const {return RowVector(this->operator()(0, 0), this->operator()(0, 1), this->operator()(0, 2), this->operator()(0, 3));}
    RowVector r1() const {return RowVector(this->operator()(1, 0), this->operator()(1, 1), this->operator()(1, 2), this->operator()(1, 3));}
    RowVector r2() const {return RowVector(this->operator()(2, 0), this->operator()(2, 1), this->operator()(2, 2), this->operator()(2, 3));}

    void setR0(const RowVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y(); this->operator()(0, 2) = value.z(); this->operator()(0, 3)=value.w();}
    void setR1(const RowVector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(1, 2) = value.z(); this->operator()(1, 3)=value.w();}
    void setR2(const RowVector & value) {this->operator()(2, 0) =  value.x(); this->operator()(2, 1) = value.y(); this->operator()(2, 2) = value.z(); this->operator()(2, 3)=value.w();}

    ColumnVector c0() const {return ColumnVector(this->operator()(0, 0), this->operator()(1, 0), this->operator()(2, 0));}
    ColumnVector c1() const {return ColumnVector(this->operator()(0, 1), this->operator()(1, 1), this->operator()(2, 1));}
    ColumnVector c2() const {return ColumnVector(this->operator()(0, 2), this->operator()(1, 2), this->operator()(2, 2));}
    ColumnVector c3() const {return ColumnVector(this->operator()(0, 3), this->operator()(1, 3), this->operator()(2, 3));}

    void setC0(const ColumnVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y(); this->operator()(2, 0) = value.z();}
    void setC1(const ColumnVector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(2, 1) = value.z();}
    void setC2(const ColumnVector & value) {this->operator()(0, 2) =  value.x(); this->operator()(1, 2) = value.y(); this->operator()(2, 2) = value.z();}
    void setC3(const ColumnVector & value) {this->operator()(0, 3) =  value.x(); this->operator()(1, 3) = value.y(); this->operator()(2, 3) = value.z();}

};

class qmat4x3 : public matrix4x3<float, qvec4, qvec3>
{
    Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m02 READ m02 WRITE setM02)
    Q_PROPERTY(float m03 READ m03 WRITE setM03)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)
    Q_PROPERTY(float m12 READ m12 WRITE setM12)
    Q_PROPERTY(float m13 READ m13 WRITE setM13)
    Q_PROPERTY(float m20 READ m20 WRITE setM20)
    Q_PROPERTY(float m21 READ m21 WRITE setM21)
    Q_PROPERTY(float m22 READ m22 WRITE setM22)
    Q_PROPERTY(float m23 READ m23 WRITE setM23)

    Q_PROPERTY(qvec4 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec4 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qvec4 r2 READ r2 WRITE setR2)

    Q_PROPERTY(qvec3 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec3 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qvec3 c2 READ c2 WRITE setC2)
    Q_PROPERTY(qvec3 c3 READ c3 WRITE setC3)

public:

    using matrix4x3<float, qvec4, qvec3>::matrix4x3;
    using QGenericMatrix<4,3,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

};

class qdmat4x3 : public matrix4x3<double, qdvec4, qdvec3>
{
    Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m02 READ m02 WRITE setM02)
    Q_PROPERTY(double m03 READ m03 WRITE setM03)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)
    Q_PROPERTY(double m12 READ m12 WRITE setM12)
    Q_PROPERTY(double m13 READ m13 WRITE setM13)
    Q_PROPERTY(double m20 READ m20 WRITE setM20)
    Q_PROPERTY(double m21 READ m21 WRITE setM21)
    Q_PROPERTY(double m22 READ m22 WRITE setM22)
    Q_PROPERTY(double m23 READ m23 WRITE setM23)

    Q_PROPERTY(qdvec4 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec4 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qdvec4 r2 READ r2 WRITE setR2)

    Q_PROPERTY(qdvec3 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec3 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qdvec3 c2 READ c2 WRITE setC2)
    Q_PROPERTY(qdvec3 c3 READ c3 WRITE setC3)

public:

    using matrix4x3<double, qdvec4, qdvec3>::matrix4x3;
    using QGenericMatrix<4,3,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

};

template <typename Type, typename Vector> class matrix4 : public QGenericMatrix<4,4,Type>
{
public:

    matrix4(){}

    matrix4(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix4(const Vector &a, const Vector &b, const Vector &c, const Vector &d)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();
        this->operator()(2,0) = a.z();
        this->operator()(3,0) = a.w();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();
        this->operator()(2,1) = b.z();
        this->operator()(3,1) = b.w();

        this->operator()(0,2) = c.x();
        this->operator()(1,2) = c.y();
        this->operator()(2,2) = c.z();
        this->operator()(3,2) = c.w();

        this->operator()(0,3) = d.x();
        this->operator()(1,3) = d.y();
        this->operator()(2,3) = d.z();
        this->operator()(3,3) = d.w();

    }

    // column wise intitialization as per GLSL
    matrix4(Type a, Type b, Type c, Type d, // first column
            Type e, Type f, Type g, Type h, // second column
            Type i, Type j, Type k, Type l, // third column
            Type m, Type n, Type o, Type p) // fourth column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;
        this->operator()(2,0) = c;
        this->operator()(3,0) = d;

        this->operator()(0,1) = e;
        this->operator()(1,1) = f;
        this->operator()(2,1) = g;
        this->operator()(3,1) = h;

        this->operator()(0,2) = i;
        this->operator()(1,2) = j;
        this->operator()(2,2) = k;
        this->operator()(3,2) = l;

        this->operator()(0,3) = m;
        this->operator()(1,3) = n;
        this->operator()(2,3) = o;
        this->operator()(3,3) = p;

    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m02() const {return this->operator()(0, 2);}
    Type m03() const {return this->operator()(0, 3);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}
    Type m12() const {return this->operator()(1, 2);}
    Type m13() const {return this->operator()(1, 3);}
    Type m20() const {return this->operator()(2, 0);}
    Type m21() const {return this->operator()(2, 1);}
    Type m22() const {return this->operator()(2, 2);}
    Type m23() const {return this->operator()(2, 3);}
    Type m30() const {return this->operator()(3, 0);}
    Type m31() const {return this->operator()(3, 1);}
    Type m32() const {return this->operator()(3, 2);}
    Type m33() const {return this->operator()(3, 3);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM02(Type value) {this->operator()(0, 2) = value;}
    void setM03(Type value) {this->operator()(0, 3) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}
    void setM12(Type value) {this->operator()(1, 2) = value;}
    void setM13(Type value) {this->operator()(1, 3) = value;}
    void setM20(Type value) {this->operator()(2, 0) = value;}
    void setM21(Type value) {this->operator()(2, 1) = value;}
    void setM22(Type value) {this->operator()(2, 2) = value;}
    void setM23(Type value) {this->operator()(2, 3) = value;}
    void setM30(Type value) {this->operator()(3, 0) = value;}
    void setM31(Type value) {this->operator()(3, 1) = value;}
    void setM32(Type value) {this->operator()(3, 2) = value;}
    void setM33(Type value) {this->operator()(3, 3) = value;}


    Vector r0() const {return Vector(this->operator()(0, 0), this->operator()(0, 1), this->operator()(0, 2), this->operator()(0, 3));}
    Vector r1() const {return Vector(this->operator()(1, 0), this->operator()(1, 1), this->operator()(1, 2), this->operator()(1, 3));}
    Vector r2() const {return Vector(this->operator()(2, 0), this->operator()(2, 1), this->operator()(2, 2), this->operator()(2, 3));}
    Vector r3() const {return Vector(this->operator()(3, 0), this->operator()(3, 1), this->operator()(3, 2), this->operator()(3, 3));}

    void setR0(const Vector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y(); this->operator()(0, 2) = value.z(); this->operator()(0, 3)=value.w();}
    void setR1(const Vector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(1, 2) = value.z(); this->operator()(1, 3)=value.w();}
    void setR2(const Vector & value) {this->operator()(2, 0) =  value.x(); this->operator()(2, 1) = value.y(); this->operator()(2, 2) = value.z(); this->operator()(2, 3)=value.w();}
    void setR3(const Vector & value) {this->operator()(3, 0) =  value.x(); this->operator()(3, 1) = value.y(); this->operator()(3, 2) = value.z(); this->operator()(3, 3)=value.w();}

    Vector c0() const {return Vector(this->operator()(0, 0), this->operator()(1, 0), this->operator()(2, 0), this->operator()(3, 0));}
    Vector c1() const {return Vector(this->operator()(0, 1), this->operator()(1, 1), this->operator()(2, 1), this->operator()(3, 1));}
    Vector c2() const {return Vector(this->operator()(0, 2), this->operator()(1, 2), this->operator()(2, 2), this->operator()(3, 2));}
    Vector c3() const {return Vector(this->operator()(0, 3), this->operator()(1, 3), this->operator()(2, 3), this->operator()(3, 3));}

    void setC0(const Vector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y(); this->operator()(2, 0) = value.z(); this->operator()(3, 0)=value.w();}
    void setC1(const Vector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(2, 1) = value.z(); this->operator()(3, 1)=value.w();}
    void setC2(const Vector & value) {this->operator()(0, 2) =  value.x(); this->operator()(1, 2) = value.y(); this->operator()(2, 2) = value.z(); this->operator()(3, 2)=value.w();}
    void setC3(const Vector & value) {this->operator()(0, 3) =  value.x(); this->operator()(1, 3) = value.y(); this->operator()(2, 3) = value.z(); this->operator()(3, 3)=value.w();}


    matrix4(const QGenericMatrix<4,4,Type> &matrix) :
      QGenericMatrix<4,4,Type>()
    {
        memcpy(this->data(), matrix.constData(), sizeof(Type)*16);
    }

    matrix4(const QMatrix4x4 &matrix)
    {
        for(int i=0;i<16;++i)
        {
            this->data()[i] = Type(matrix.data()[i]);
        }
    }

    operator QMatrix4x4() const
    {
        QMatrix4x4 matrix;
        for(int i=0;i<16;++i)
        {
            matrix.data()[i] = float(this->constData()[i]);
        }
        return matrix;
    }


    matrix4<Type, Vector> frustum(Type left, Type right, Type bottom, Type top, Type nearPlane, Type farPlane) const;
    matrix4<Type,Vector> rotate(Type angle, const QGenericMatrix<1,3,Type> &vector) const;
    matrix4<Type,Vector> translate(const QGenericMatrix<1,3,Type> &vector) const;
    matrix4<Type,Vector> scale(const QGenericMatrix<1,3,Type> &vector) const;
    matrix4<Type,Vector> lookAt(const QGenericMatrix<1,3,Type> &eye, const QGenericMatrix<1,3,Type> &center, const QGenericMatrix<1,3,Type> &up) const;

};

template<typename Type, typename Vector>
matrix4<Type, Vector> matrix4<Type, Vector>::frustum(Type left, Type right, Type bottom, Type top, Type nearPlane, Type farPlane) const
{

    // Construct the projection.
    matrix4<Type,Vector> m;
    Type width = right - left;
    Type invheight = top - bottom;
    Type clip = farPlane - nearPlane;
    m(0,0) = 2.0 * nearPlane / width;
    m(0,1) = 0.0;
    m(0,2) = (left + right) / width;
    m(0,3) = 0.0;
    m(1,0) = 0.0;
    m(1,1) = 2.0 * nearPlane / invheight;
    m(1,2) = (top + bottom) / invheight;
    m(1,3) = 0.0;
    m(2,0) = 0.0;
    m(2,1) = 0.0;
    m(2,2) = -(nearPlane + farPlane) / clip;
    m(2,3) = -2.0 * nearPlane * farPlane / clip;
    m(3,0) = 0.0;
    m(3,1) = 0.0;
    m(3,2) = -1.0;
    m(3,3) = 0.0;

    return *this*m;
}

template<typename Type, typename Vector>
matrix4<Type, Vector> matrix4<Type, Vector>::rotate(Type angle, const QGenericMatrix<1,3, Type> &vector) const
{
    Type a = qDegreesToRadians(angle);
    Type c = std::cos(a);
    Type s = std::sin(a);
    Type ic = 1.0f - c;

    Type length = std::sqrt(vector(0,0)*vector(0,0)+vector(1,0)*vector(1,0)+vector(2,0)*vector(2,0));
    Type x = vector(0,0)/length;
    Type y = vector(1,0)/length;
    Type z = vector(2,0)/length;

    matrix4<Type,Vector> m;
    m(0,0) = x*x*ic + c;
    m(0,1) = x*y*ic - z*s;
    m(0,2) = x*z*ic + y*s;
    m(0,3) = 0.0f;
    m(1,0) = x*y*ic + z*s;
    m(1,1) = y*y*ic + c;
    m(1,2) = y*z*ic - x*s;
    m(1,3) = 0.0f;
    m(2,0) = x*z*ic - y*s;
    m(2,1) = y*z*ic + x*s;
    m(2,2) = z*z*ic + c;
    m(2,3) = 0.0f;
    m(3,0) = 0.0f;
    m(3,1) = 0.0f;
    m(3,2) = 0.0f;
    m(3,3) = 1.0f;

    return *this*m;
}

template<typename Type, typename Vector>
matrix4<Type, Vector> matrix4<Type, Vector>::translate(const QGenericMatrix<1, 3, Type> &vector) const
{
    Type x = vector(0,0);
    Type y = vector(1,0);
    Type z = vector(2,0);

    matrix4<Type,Vector> m;
    m(0,3) = x;
    m(1,3) = y;
    m(2,3) = z;

    return *this*m;
}

template<typename Type>
const QGenericMatrix<1, 3, Type> crossProduct(const QGenericMatrix<1, 3, Type> &a, const QGenericMatrix<1, 3, Type> &b)
{
    QGenericMatrix<1, 3, Type> v;
    v(0,0) = a(1,0)*b(2,0) - a(2,0)*b(1,0);
    v(1,0) = a(2,0)*b(0,0) - a(0,0)*b(2,0);
    v(2,0) = a(0,0)*b(1,0) - a(1,0)*b(0,0);
    return v;
}

template<typename Type>
const QGenericMatrix<1, 3, Type> normalize(const QGenericMatrix<1, 3, Type> &a)
{
    Type length = std::sqrt((a.transposed()*a)(0,0));
    return a*(Type(1.0)/length);
}

template<typename Type, typename Vector>
matrix4<Type, Vector> matrix4<Type, Vector>::scale(const QGenericMatrix<1, 3, Type> &vector) const
{
    matrix4<Type, Vector> m;
    m(0,0)=vector(0,0);
    m(1,1)=vector(1,0);
    m(2,2)=vector(2,0);

    return *this*m;
}


template<typename Type, typename Vector>
matrix4<Type, Vector> matrix4<Type, Vector>::lookAt(const QGenericMatrix<1, 3, Type> &eye, const QGenericMatrix<1, 3, Type> &center, const QGenericMatrix<1, 3, Type> &up) const
{
    QGenericMatrix<1, 3, Type> forward = normalize(center-eye);
    QGenericMatrix<1, 3, Type> side = normalize(crossProduct(forward, up));
    QGenericMatrix<1, 3, Type> upVector = crossProduct(side, forward);

    matrix4<Type, Vector> m;
    m(0,0) = side(0,0);
    m(0,1) = side(1,0);
    m(0,2) = side(2,0);
    m(0,3) = 0.0;
    m(1,0) = upVector(0,0);
    m(1,1) = upVector(1,0);
    m(1,2) = upVector(2,0);
    m(1,3) = 0.0;
    m(2,0) = -forward(0,0);
    m(2,1) = -forward(1,0);
    m(2,2) = -forward(2,0);
    m(2,3) = 0.0;
    m(3,0) = 0.0;
    m(3,1) = 0.0;
    m(3,2) = 0.0;
    m(3,3) = 1.0;

    m = m.translate(-eye);

    return *this*m;
}

class qdmat4;

class QUICKOPENGL_EXPORT qmat4 : public matrix4<float, qvec4>
{
    Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m02 READ m02 WRITE setM02)
    Q_PROPERTY(float m03 READ m03 WRITE setM03)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)
    Q_PROPERTY(float m12 READ m12 WRITE setM12)
    Q_PROPERTY(float m13 READ m13 WRITE setM13)
    Q_PROPERTY(float m20 READ m20 WRITE setM20)
    Q_PROPERTY(float m21 READ m21 WRITE setM21)
    Q_PROPERTY(float m22 READ m22 WRITE setM22)
    Q_PROPERTY(float m23 READ m23 WRITE setM23)
    Q_PROPERTY(float m30 READ m30 WRITE setM30)
    Q_PROPERTY(float m31 READ m31 WRITE setM31)
    Q_PROPERTY(float m32 READ m32 WRITE setM32)
    Q_PROPERTY(float m33 READ m33 WRITE setM33)

    Q_PROPERTY(qvec4 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec4 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qvec4 r2 READ r2 WRITE setR2)
    Q_PROPERTY(qvec4 r3 READ r3 WRITE setR3)

    Q_PROPERTY(qvec4 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec4 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qvec4 c2 READ c2 WRITE setC2)
    Q_PROPERTY(qvec4 c3 READ c3 WRITE setC3)

public:
    using matrix4<float, qvec4>::matrix4;
    using QGenericMatrix<4,4,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

    operator qdmat4() const;

    Q_INVOKABLE qmat4 times(const qmat4 &other) const;
    Q_INVOKABLE qvec4 times(const qvec4 &vector) const;
    Q_INVOKABLE qvec3 times(const qvec3 &vector) const;
    Q_INVOKABLE qmat4 times(float factor) const;
    Q_INVOKABLE qmat4 plus(const qmat4 &other) const;
    Q_INVOKABLE qmat4 minus(const qmat4 &other) const;
    Q_INVOKABLE float determinant() const;
    Q_INVOKABLE qmat4 invert() const;
    Q_INVOKABLE qmat4 transpose() const;
    Q_INVOKABLE qmat4 frustum(float left, float right, float bottom, float top, float nearPlane, float farPlane) const;
    Q_INVOKABLE qmat4 rotate(float angle, const qvec3 &vector) const;
    Q_INVOKABLE qmat4 translate(const qvec3 &vector) const;
    Q_INVOKABLE qmat4 lookAt(const qvec3 &eye, const qvec3 &center, const qvec3 &up) const;
    Q_INVOKABLE qmat4 scale(const qvec3 &vector) const;

};

class QUICKOPENGL_EXPORT qdmat4 : public matrix4<double, qdvec4>
{
    Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m02 READ m02 WRITE setM02)
    Q_PROPERTY(double m03 READ m03 WRITE setM03)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)
    Q_PROPERTY(double m12 READ m12 WRITE setM12)
    Q_PROPERTY(double m13 READ m13 WRITE setM13)
    Q_PROPERTY(double m20 READ m20 WRITE setM20)
    Q_PROPERTY(double m21 READ m21 WRITE setM21)
    Q_PROPERTY(double m22 READ m22 WRITE setM22)
    Q_PROPERTY(double m23 READ m23 WRITE setM23)
    Q_PROPERTY(double m30 READ m30 WRITE setM30)
    Q_PROPERTY(double m31 READ m31 WRITE setM31)
    Q_PROPERTY(double m32 READ m32 WRITE setM32)
    Q_PROPERTY(double m33 READ m33 WRITE setM33)

    Q_PROPERTY(qdvec4 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec4 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qdvec4 r2 READ r2 WRITE setR2)
    Q_PROPERTY(qdvec4 r3 READ r3 WRITE setR3)

    Q_PROPERTY(qdvec4 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec4 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qdvec4 c2 READ c2 WRITE setC2)
    Q_PROPERTY(qdvec4 c3 READ c3 WRITE setC3)

public:

    using matrix4<double, qdvec4>::matrix4;
    using QGenericMatrix<4,4,double>::operator==;

    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

    operator qmat4() const;

    Q_INVOKABLE qdmat4 times(const qdmat4 &other) const;
    Q_INVOKABLE qdvec4 times(const qdvec4 &vector) const;
    Q_INVOKABLE qdvec3 times(const qdvec3 &vector) const;
    Q_INVOKABLE qdmat4 times(double factor) const;
    Q_INVOKABLE qdmat4 plus(const qdmat4 &other) const;
    Q_INVOKABLE qdmat4 minus(const qdmat4 &other) const;
    Q_INVOKABLE double determinant() const;
    Q_INVOKABLE qdmat4 invert() const;
    Q_INVOKABLE qdmat4 transpose() const;
    Q_INVOKABLE qdmat4 frustum(double left, double right, double bottom, double top, double nearPlane, double farPlane) const;
    Q_INVOKABLE qdmat4 rotate(double angle, const qdvec3 &vector) const;
    Q_INVOKABLE qdmat4 translate(const qdvec3 &vector) const;
    Q_INVOKABLE qdmat4 lookAt(const qdvec3 &eye, const qdvec3 &center, const qdvec3 &up) const;
    Q_INVOKABLE qdmat4 scale(const qdvec3 &vector) const;
};


inline QDebug operator << (QDebug debug, const qmat4 &value)
{
    QDebugStateSaver saver(debug);

    debug.nospace() << "mat4(" << Qt::endl << qSetFieldWidth(10);
    for (int row = 0; row < 4; ++row) {
        for (int col = 0; col < 4; ++col)
        {
            debug << value(row, col);
        }
        debug << Qt::endl;
    }
    debug << qSetFieldWidth(0) << ')';
    return debug;
}

inline QDebug operator << (QDebug debug, const qdmat4 &value)
{
    QDebugStateSaver saver(debug);

    debug.nospace() << "dmat4(" << Qt::endl << qSetFieldWidth(10);
    for (int row = 0; row < 4; ++row) {
        for (int col = 0; col < 4; ++col)
        {
            debug << value(row, col);
        }
        debug << Qt::endl;
    }
    debug << qSetFieldWidth(0) << ')';
    return debug;
}

Q_DECLARE_TYPEINFO(qmat4, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat4, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qmat4x2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat4x2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qmat4x3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat4x3, Q_MOVABLE_TYPE);

Q_DECLARE_METATYPE(qmat4)
Q_DECLARE_METATYPE(qdmat4)
Q_DECLARE_METATYPE(qmat4x2)
Q_DECLARE_METATYPE(qdmat4x2)
Q_DECLARE_METATYPE(qmat4x3)
Q_DECLARE_METATYPE(qdmat4x3)

#endif // QUICKMATRIX4_H

/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKVECTOR3_H
#define QUICKVECTOR3_H

#include <QGenericMatrix>
#include <QVariant>
#include <QDebug>
#include <QGeoCoordinate>
#include <QVector3D>
#include <QColor>
#include <cmath>
#include "quickopengl_global.h"
#include "quickvector2.h"

class qdmat4;
class qmat4;

template <typename Type> class vector3 : public QGenericMatrix<1,3,Type>
{
public:

    vector3(const QGenericMatrix<1,3,Type> &other)
    {
        memcpy(reinterpret_cast<void *>(this), &other, sizeof(vector3<Type>));
    }

    vector3(Type x = Type(0), Type y = Type(0), Type z = Type(0))
    {
        setX(x);
        setY(y);
        setZ(z);
    }

    vector3(const vector2<Type> &xy, Type z)
    {
        setX(xy.x());
        setY(xy.y());
        setZ(z);
    }

    vector3(Type x, const vector2<Type> &yz)
    {
        setX(x);
        setY(yz.x());
        setZ(yz.y());
    }

    vector3(const QGeoCoordinate &coordinate)
    {
        setX(Type(coordinate.longitude()));
        setY(Type(coordinate.latitude()));
        setZ(Type(coordinate.altitude()));
    }

    vector3(const QVector3D &vector3d)
    {
        setX(Type(vector3d.x()));
        setY(Type(vector3d.y()));
        setZ(Type(vector3d.z()));
    }

    vector3(const QColor &color)
    {
        setR(Type(color.red()));
        setG(Type(color.green()));
        setB(Type(color.blue()));
    }

    vector3(const QString &string) : vector3(QColor(string))
    {
    }

    operator QGeoCoordinate() const
    {
        return QGeoCoordinate(double(y()), double(x()), double(z()));
    }

    operator QVector3D() const
    {
        return QVector3D(float(x()), float(y()), float(z()));
    }

    operator QColor() const
    {
        return QColor(int(r()), int(g()), int(b()));
    }

    operator QString() const
    {
        // if this value could be a color, encode as #rrggbb
        if(std::is_same<Type, float>::value&&
                (float(r())>=0.0f&&float(r())<=1.0f)&&
                (float(g())>=0.0f&&float(g())<=1.0f)&&
                (float(b())>=0.0f&&float(b())<=1.0f))
        {
            return this->operator QColor().name(QColor::HexRgb);
        }
        else if(std::is_same<Type, int>::value||std::is_same<Type, uint>::value)
        {
            return this->operator QColor().name(QColor::HexRgb);
        }

        QString result;
        QTextStream stream(&result);
        stream << "(" << x() << ", " << y() << ", " << z() << ")";
        return result;
    }

    vector2<Type> xx() const {return vector2<Type>(x(),x());}
    vector2<Type> xy() const {return vector2<Type>(x(),y());}
    vector2<Type> xz() const {return vector2<Type>(x(),z());}
    vector2<Type> yx() const {return vector2<Type>(y(),x());}
    vector2<Type> yy() const {return vector2<Type>(y(),y());}
    vector2<Type> yz() const {return vector2<Type>(y(),z());}
    vector2<Type> zx() const {return vector2<Type>(z(),x());}
    vector2<Type> zy() const {return vector2<Type>(z(),y());}
    vector2<Type> zz() const {return vector2<Type>(z(),z());}


    vector3<Type> xxx() const {return vector3<Type>(x(),x(),x());}
    vector3<Type> xxy() const {return vector3<Type>(x(),x(),y());}
    vector3<Type> xxz() const {return vector3<Type>(x(),x(),z());}
    vector3<Type> xyx() const {return vector3<Type>(x(),y(),x());}
    vector3<Type> xyy() const {return vector3<Type>(x(),y(),y());}
    vector3<Type> xyz() const {return vector3<Type>(x(),y(),z());}
    vector3<Type> xzx() const {return vector3<Type>(x(),z(),x());}
    vector3<Type> xzy() const {return vector3<Type>(x(),z(),y());}
    vector3<Type> xzz() const {return vector3<Type>(x(),z(),z());}
    vector3<Type> yxx() const {return vector3<Type>(y(),x(),x());}
    vector3<Type> yxy() const {return vector3<Type>(y(),x(),y());}
    vector3<Type> yxz() const {return vector3<Type>(y(),x(),z());}
    vector3<Type> yyx() const {return vector3<Type>(y(),y(),x());}
    vector3<Type> yyy() const {return vector3<Type>(y(),y(),y());}
    vector3<Type> yyz() const {return vector3<Type>(y(),y(),z());}
    vector3<Type> yzx() const {return vector3<Type>(y(),z(),x());}
    vector3<Type> yzy() const {return vector3<Type>(y(),z(),y());}
    vector3<Type> yzz() const {return vector3<Type>(y(),z(),z());}
    vector3<Type> zxx() const {return vector3<Type>(z(),x(),x());}
    vector3<Type> zxy() const {return vector3<Type>(z(),x(),y());}
    vector3<Type> zxz() const {return vector3<Type>(z(),x(),z());}
    vector3<Type> zyx() const {return vector3<Type>(z(),y(),x());}
    vector3<Type> zyy() const {return vector3<Type>(z(),y(),y());}
    vector3<Type> zyz() const {return vector3<Type>(z(),y(),z());}
    vector3<Type> zzx() const {return vector3<Type>(z(),z(),x());}
    vector3<Type> zzy() const {return vector3<Type>(z(),z(),y());}
    vector3<Type> zzz() const {return vector3<Type>(z(),z(),z());}

    Type x() const
    {
        return this->operator()(0,0);
    }
    Type y() const
    {
        return this->operator()(1,0);
    }
    Type z() const
    {
        return this->operator()(2,0);
    }
    Type r() const
    {
        return this->operator()(0,0);
    }
    Type g() const
    {
        return this->operator()(1,0);
    }
    Type b() const
    {
        return this->operator()(2,0);
    }

    void setX(Type x)
    {
        this->operator()(0,0) = x;
    }

    void setY(Type y)
    {
        this->operator()(1,0) = y;
    }

    void setZ(Type z)
    {
        this->operator()(2,0) = z;
    }

    void setR(Type r)
    {
        this->operator()(0,0) = r;
    }

    void setG(Type g)
    {
        this->operator()(1,0) = g;
    }

    void setB(Type b)
    {
        this->operator()(2,0) = b;
    }   

};


template <> inline vector3<float>::vector3(const QColor &color)
{
    setR(float(color.redF()));
    setG(float(color.greenF()));
    setB(float(color.blueF()));
}

template <> inline vector3<double>::vector3(const QColor &color)
{
    setR(double(color.redF()));
    setG(double(color.greenF()));
    setB(double(color.blueF()));
}

template <> inline vector3<float>::operator QColor() const
{
    QColor color;
    color.setRedF(qreal(r()));
    color.setGreenF(qreal(g()));
    color.setBlueF(qreal(b()));
    return color;
}

template <> inline vector3<double>::operator QColor() const
{
    QColor color;
    color.setRedF(qreal(r()));
    color.setGreenF(qreal(g()));
    color.setBlueF(qreal(b()));
    return color;
}

class QUICKOPENGL_EXPORT qvec3 : public vector3<float>
{
    Q_GADGET

    Q_PROPERTY(float x READ x WRITE setX)
    Q_PROPERTY(float y READ y WRITE setY)
    Q_PROPERTY(float z READ z WRITE setZ)
    Q_PROPERTY(float r READ r WRITE setR)
    Q_PROPERTY(float g READ g WRITE setG)
    Q_PROPERTY(float b READ b WRITE setB)

    Q_PROPERTY(qvec2 xx READ xx STORED false)
    Q_PROPERTY(qvec2 xy READ xy STORED false)
    Q_PROPERTY(qvec2 xz READ xz STORED false)
    Q_PROPERTY(qvec2 yx READ yx STORED false)
    Q_PROPERTY(qvec2 yy READ yy STORED false)
    Q_PROPERTY(qvec2 yz READ yz STORED false)
    Q_PROPERTY(qvec2 zx READ zx STORED false)
    Q_PROPERTY(qvec2 zy READ zy STORED false)
    Q_PROPERTY(qvec2 zz READ zz STORED false)

    Q_PROPERTY(qvec3 xxx READ xxx STORED false)
    Q_PROPERTY(qvec3 xxy READ xxy STORED false)
    Q_PROPERTY(qvec3 xxz READ xxz STORED false)
    Q_PROPERTY(qvec3 xyx READ xyx STORED false)
    Q_PROPERTY(qvec3 xyy READ xyy STORED false)
    Q_PROPERTY(qvec3 xyz READ xyz STORED false)
    Q_PROPERTY(qvec3 xzx READ xzx STORED false)
    Q_PROPERTY(qvec3 xzy READ xzy STORED false)
    Q_PROPERTY(qvec3 xzz READ xzz STORED false)
    Q_PROPERTY(qvec3 yxx READ yxx STORED false)
    Q_PROPERTY(qvec3 yxy READ yxy STORED false)
    Q_PROPERTY(qvec3 yxz READ yxz STORED false)
    Q_PROPERTY(qvec3 yyx READ yyx STORED false)
    Q_PROPERTY(qvec3 yyy READ yyy STORED false)
    Q_PROPERTY(qvec3 yyz READ yyz STORED false)
    Q_PROPERTY(qvec3 yzx READ yzx STORED false)
    Q_PROPERTY(qvec3 yzy READ yzy STORED false)
    Q_PROPERTY(qvec3 yzz READ yzz STORED false)
    Q_PROPERTY(qvec3 zxx READ zxx STORED false)
    Q_PROPERTY(qvec3 zxy READ zxy STORED false)
    Q_PROPERTY(qvec3 zxz READ zxz STORED false)
    Q_PROPERTY(qvec3 zyx READ zyx STORED false)
    Q_PROPERTY(qvec3 zyy READ zyy STORED false)
    Q_PROPERTY(qvec3 zyz READ zyz STORED false)
    Q_PROPERTY(qvec3 zzx READ zzx STORED false)
    Q_PROPERTY(qvec3 zzy READ zzy STORED false)
    Q_PROPERTY(qvec3 zzz READ zzz STORED false)

public:

    Q_INVOKABLE qvec3 crossProduct(const qvec3 &other) const;
    Q_INVOKABLE float dotProduct(const qvec3 &other) const;
    Q_INVOKABLE qvec3 times(const qmat4 &other) const;
    Q_INVOKABLE qvec3 times(const qvec3 &other) const;
    Q_INVOKABLE qvec3 times(float factor) const;
    Q_INVOKABLE qvec3 plus(const qvec3 &other) const;
    Q_INVOKABLE qvec3 minus(const qvec3 &other) const;
    Q_INVOKABLE qvec3 normalized() const;
    Q_INVOKABLE float length() const;

    using vector3<float>::vector3;
    using QGenericMatrix<1,3,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};



class QUICKOPENGL_EXPORT qivec3 : public vector3<int>
{
    Q_GADGET
    Q_PROPERTY(int x READ x WRITE setX)
    Q_PROPERTY(int y READ y WRITE setY)
    Q_PROPERTY(int z READ z WRITE setZ)
    Q_PROPERTY(int r READ r WRITE setR)
    Q_PROPERTY(int g READ g WRITE setG)
    Q_PROPERTY(int b READ b WRITE setB)

    Q_PROPERTY(qivec2 xx READ xx STORED false)
    Q_PROPERTY(qivec2 xy READ xy STORED false)
    Q_PROPERTY(qivec2 xz READ xz STORED false)
    Q_PROPERTY(qivec2 yx READ yx STORED false)
    Q_PROPERTY(qivec2 yy READ yy STORED false)
    Q_PROPERTY(qivec2 yz READ yz STORED false)
    Q_PROPERTY(qivec2 zx READ zx STORED false)
    Q_PROPERTY(qivec2 zy READ zy STORED false)
    Q_PROPERTY(qivec2 zz READ zz STORED false)

    Q_PROPERTY(qivec3 xxx READ xxx STORED false)
    Q_PROPERTY(qivec3 xxy READ xxy STORED false)
    Q_PROPERTY(qivec3 xxz READ xxz STORED false)
    Q_PROPERTY(qivec3 xyx READ xyx STORED false)
    Q_PROPERTY(qivec3 xyy READ xyy STORED false)
    Q_PROPERTY(qivec3 xyz READ xyz STORED false)
    Q_PROPERTY(qivec3 xzx READ xzx STORED false)
    Q_PROPERTY(qivec3 xzy READ xzy STORED false)
    Q_PROPERTY(qivec3 xzz READ xzz STORED false)
    Q_PROPERTY(qivec3 yxx READ yxx STORED false)
    Q_PROPERTY(qivec3 yxy READ yxy STORED false)
    Q_PROPERTY(qivec3 yxz READ yxz STORED false)
    Q_PROPERTY(qivec3 yyx READ yyx STORED false)
    Q_PROPERTY(qivec3 yyy READ yyy STORED false)
    Q_PROPERTY(qivec3 yyz READ yyz STORED false)
    Q_PROPERTY(qivec3 yzx READ yzx STORED false)
    Q_PROPERTY(qivec3 yzy READ yzy STORED false)
    Q_PROPERTY(qivec3 yzz READ yzz STORED false)
    Q_PROPERTY(qivec3 zxx READ zxx STORED false)
    Q_PROPERTY(qivec3 zxy READ zxy STORED false)
    Q_PROPERTY(qivec3 zxz READ zxz STORED false)
    Q_PROPERTY(qivec3 zyx READ zyx STORED false)
    Q_PROPERTY(qivec3 zyy READ zyy STORED false)
    Q_PROPERTY(qivec3 zyz READ zyz STORED false)
    Q_PROPERTY(qivec3 zzx READ zzx STORED false)
    Q_PROPERTY(qivec3 zzy READ zzy STORED false)
    Q_PROPERTY(qivec3 zzz READ zzz STORED false)

public:

    using vector3<int>::vector3;
    using QGenericMatrix<1,3,int>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class QUICKOPENGL_EXPORT quvec3 : public vector3<unsigned int>
{
    Q_GADGET
    Q_PROPERTY(unsigned int x READ x WRITE setX)
    Q_PROPERTY(unsigned int y READ y WRITE setY)
    Q_PROPERTY(unsigned int z READ z WRITE setZ)
    Q_PROPERTY(unsigned int r READ r WRITE setR)
    Q_PROPERTY(unsigned int g READ g WRITE setG)
    Q_PROPERTY(unsigned int b READ b WRITE setB)

    Q_PROPERTY(quvec2 xx READ xx STORED false)
    Q_PROPERTY(quvec2 xy READ xy STORED false)
    Q_PROPERTY(quvec2 xz READ xz STORED false)
    Q_PROPERTY(quvec2 yx READ yx STORED false)
    Q_PROPERTY(quvec2 yy READ yy STORED false)
    Q_PROPERTY(quvec2 yz READ yz STORED false)
    Q_PROPERTY(quvec2 zx READ zx STORED false)
    Q_PROPERTY(quvec2 zy READ zy STORED false)
    Q_PROPERTY(quvec2 zz READ zz STORED false)

    Q_PROPERTY(quvec3 xxx READ xxx STORED false)
    Q_PROPERTY(quvec3 xxy READ xxy STORED false)
    Q_PROPERTY(quvec3 xxz READ xxz STORED false)
    Q_PROPERTY(quvec3 xyx READ xyx STORED false)
    Q_PROPERTY(quvec3 xyy READ xyy STORED false)
    Q_PROPERTY(quvec3 xyz READ xyz STORED false)
    Q_PROPERTY(quvec3 xzx READ xzx STORED false)
    Q_PROPERTY(quvec3 xzy READ xzy STORED false)
    Q_PROPERTY(quvec3 xzz READ xzz STORED false)
    Q_PROPERTY(quvec3 yxx READ yxx STORED false)
    Q_PROPERTY(quvec3 yxy READ yxy STORED false)
    Q_PROPERTY(quvec3 yxz READ yxz STORED false)
    Q_PROPERTY(quvec3 yyx READ yyx STORED false)
    Q_PROPERTY(quvec3 yyy READ yyy STORED false)
    Q_PROPERTY(quvec3 yyz READ yyz STORED false)
    Q_PROPERTY(quvec3 yzx READ yzx STORED false)
    Q_PROPERTY(quvec3 yzy READ yzy STORED false)
    Q_PROPERTY(quvec3 yzz READ yzz STORED false)
    Q_PROPERTY(quvec3 zxx READ zxx STORED false)
    Q_PROPERTY(quvec3 zxy READ zxy STORED false)
    Q_PROPERTY(quvec3 zxz READ zxz STORED false)
    Q_PROPERTY(quvec3 zyx READ zyx STORED false)
    Q_PROPERTY(quvec3 zyy READ zyy STORED false)
    Q_PROPERTY(quvec3 zyz READ zyz STORED false)
    Q_PROPERTY(quvec3 zzx READ zzx STORED false)
    Q_PROPERTY(quvec3 zzy READ zzy STORED false)
    Q_PROPERTY(quvec3 zzz READ zzz STORED false)

public:

    using vector3<unsigned int>::vector3;
    using QGenericMatrix<1,3,unsigned int>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class QUICKOPENGL_EXPORT qdvec3 : public vector3<double>
{
    Q_GADGET
    Q_PROPERTY(double x READ x WRITE setX)
    Q_PROPERTY(double y READ y WRITE setY)
    Q_PROPERTY(double z READ z WRITE setZ)
    Q_PROPERTY(double r READ r WRITE setR)
    Q_PROPERTY(double g READ g WRITE setG)
    Q_PROPERTY(double b READ b WRITE setB)

    Q_PROPERTY(qdvec2 xx READ xx STORED false)
    Q_PROPERTY(qdvec2 xy READ xy STORED false)
    Q_PROPERTY(qdvec2 xz READ xz STORED false)
    Q_PROPERTY(qdvec2 yx READ yx STORED false)
    Q_PROPERTY(qdvec2 yy READ yy STORED false)
    Q_PROPERTY(qdvec2 yz READ yz STORED false)
    Q_PROPERTY(qdvec2 zx READ zx STORED false)
    Q_PROPERTY(qdvec2 zy READ zy STORED false)
    Q_PROPERTY(qdvec2 zz READ zz STORED false)

    Q_PROPERTY(qdvec3 xxx READ xxx STORED false)
    Q_PROPERTY(qdvec3 xxy READ xxy STORED false)
    Q_PROPERTY(qdvec3 xxz READ xxz STORED false)
    Q_PROPERTY(qdvec3 xyx READ xyx STORED false)
    Q_PROPERTY(qdvec3 xyy READ xyy STORED false)
    Q_PROPERTY(qdvec3 xyz READ xyz STORED false)
    Q_PROPERTY(qdvec3 xzx READ xzx STORED false)
    Q_PROPERTY(qdvec3 xzy READ xzy STORED false)
    Q_PROPERTY(qdvec3 xzz READ xzz STORED false)
    Q_PROPERTY(qdvec3 yxx READ yxx STORED false)
    Q_PROPERTY(qdvec3 yxy READ yxy STORED false)
    Q_PROPERTY(qdvec3 yxz READ yxz STORED false)
    Q_PROPERTY(qdvec3 yyx READ yyx STORED false)
    Q_PROPERTY(qdvec3 yyy READ yyy STORED false)
    Q_PROPERTY(qdvec3 yyz READ yyz STORED false)
    Q_PROPERTY(qdvec3 yzx READ yzx STORED false)
    Q_PROPERTY(qdvec3 yzy READ yzy STORED false)
    Q_PROPERTY(qdvec3 yzz READ yzz STORED false)
    Q_PROPERTY(qdvec3 zxx READ zxx STORED false)
    Q_PROPERTY(qdvec3 zxy READ zxy STORED false)
    Q_PROPERTY(qdvec3 zxz READ zxz STORED false)
    Q_PROPERTY(qdvec3 zyx READ zyx STORED false)
    Q_PROPERTY(qdvec3 zyy READ zyy STORED false)
    Q_PROPERTY(qdvec3 zyz READ zyz STORED false)
    Q_PROPERTY(qdvec3 zzx READ zzx STORED false)
    Q_PROPERTY(qdvec3 zzy READ zzy STORED false)
    Q_PROPERTY(qdvec3 zzz READ zzz STORED false)

public:

    Q_INVOKABLE qdvec3 crossProduct(const qdvec3 &other) const;
    Q_INVOKABLE double dotProduct(const qdvec3 &other) const;
    Q_INVOKABLE qdvec3 times(const qdmat4 &other) const;
    Q_INVOKABLE qdvec3 times(const qdvec3 &other) const;
    Q_INVOKABLE qdvec3 times(double factor) const;
    Q_INVOKABLE qdvec3 plus(const qdvec3 &other) const;
    Q_INVOKABLE qdvec3 minus(const qdvec3 &other) const;
    Q_INVOKABLE qdvec3 normalized() const;
    Q_INVOKABLE double length() const;

    using vector3<double>::vector3;
    using QGenericMatrix<1,3,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

};

class QUICKOPENGL_EXPORT qbvec3 : public vector3<bool>
{
    Q_GADGET
    Q_PROPERTY(bool x READ x WRITE setX)
    Q_PROPERTY(bool y READ y WRITE setY)
    Q_PROPERTY(bool z READ z WRITE setZ)
    Q_PROPERTY(bool r READ r WRITE setR)
    Q_PROPERTY(bool g READ g WRITE setG)
    Q_PROPERTY(bool b READ b WRITE setB)

    Q_PROPERTY(qbvec2 xx READ xx STORED false)
    Q_PROPERTY(qbvec2 xy READ xy STORED false)
    Q_PROPERTY(qbvec2 xz READ xz STORED false)
    Q_PROPERTY(qbvec2 yx READ yx STORED false)
    Q_PROPERTY(qbvec2 yy READ yy STORED false)
    Q_PROPERTY(qbvec2 yz READ yz STORED false)
    Q_PROPERTY(qbvec2 zx READ zx STORED false)
    Q_PROPERTY(qbvec2 zy READ zy STORED false)
    Q_PROPERTY(qbvec2 zz READ zz STORED false)


    Q_PROPERTY(qbvec3 xxx READ xxx STORED false)
    Q_PROPERTY(qbvec3 xxy READ xxy STORED false)
    Q_PROPERTY(qbvec3 xxz READ xxz STORED false)
    Q_PROPERTY(qbvec3 xyx READ xyx STORED false)
    Q_PROPERTY(qbvec3 xyy READ xyy STORED false)
    Q_PROPERTY(qbvec3 xyz READ xyz STORED false)
    Q_PROPERTY(qbvec3 xzx READ xzx STORED false)
    Q_PROPERTY(qbvec3 xzy READ xzy STORED false)
    Q_PROPERTY(qbvec3 xzz READ xzz STORED false)
    Q_PROPERTY(qbvec3 yxx READ yxx STORED false)
    Q_PROPERTY(qbvec3 yxy READ yxy STORED false)
    Q_PROPERTY(qbvec3 yxz READ yxz STORED false)
    Q_PROPERTY(qbvec3 yyx READ yyx STORED false)
    Q_PROPERTY(qbvec3 yyy READ yyy STORED false)
    Q_PROPERTY(qbvec3 yyz READ yyz STORED false)
    Q_PROPERTY(qbvec3 yzx READ yzx STORED false)
    Q_PROPERTY(qbvec3 yzy READ yzy STORED false)
    Q_PROPERTY(qbvec3 yzz READ yzz STORED false)
    Q_PROPERTY(qbvec3 zxx READ zxx STORED false)
    Q_PROPERTY(qbvec3 zxy READ zxy STORED false)
    Q_PROPERTY(qbvec3 zxz READ zxz STORED false)
    Q_PROPERTY(qbvec3 zyx READ zyx STORED false)
    Q_PROPERTY(qbvec3 zyy READ zyy STORED false)
    Q_PROPERTY(qbvec3 zyz READ zyz STORED false)
    Q_PROPERTY(qbvec3 zzx READ zzx STORED false)
    Q_PROPERTY(qbvec3 zzy READ zzy STORED false)
    Q_PROPERTY(qbvec3 zzz READ zzz STORED false)

public:

    using vector3<bool>::vector3;
    using QGenericMatrix<1,3,bool>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

inline QDebug operator << (QDebug debug, const qvec3 &value)
{
    debug.nospace() << "vec3(" << value.x() << ", " << value.y() << ", " << value.z() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qivec3 &value)
{
    debug.nospace() << "ivec3(" << value.x() << ", " << value.y() << ", " << value.z() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const quvec3 &value)
{
    debug.nospace() << "uvec3(" << value.x() << ", " << value.y() << ", " << value.z() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qdvec3 &value)
{
    debug.nospace() << "dvec3(" << value.x() << ", " << value.y() << ", " << value.z() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qbvec3 &value)
{
    debug.nospace() << "bvec3(" << value.x() << ", " << value.y() << ", " << value.z() << ")";
    return debug.space();
}

Q_DECLARE_TYPEINFO(qvec3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qivec3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(quvec3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdvec3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qbvec3, Q_MOVABLE_TYPE);

Q_DECLARE_METATYPE(qvec3)
Q_DECLARE_METATYPE(qivec3)
Q_DECLARE_METATYPE(quvec3)
Q_DECLARE_METATYPE(qdvec3)
Q_DECLARE_METATYPE(qbvec3)

#endif // QUICKVECTOR3_H

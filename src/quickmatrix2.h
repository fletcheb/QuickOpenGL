/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty sof MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#ifndef QUICKMATRIX_H
#define QUICKMATRIX_H

#include "quickvector2.h"
#include "quickvector3.h"
#include "quickvector4.h"

template <typename Type, typename Vector> class matrix2 : public QGenericMatrix<2,2,Type>
{
public:
    matrix2(){}

    matrix2(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix2(const Vector &a, const Vector &b)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();
    }

    // column wise intitialization as per GLSL
    matrix2(Type a, Type b, // first column
            Type c, Type d) // second column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;

        this->operator()(0,1) = c;
        this->operator()(1,1) = d;
    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}

    Vector r0() const {return Vector(this->operator()(0, 0), this->operator()(0, 1));}
    Vector r1() const {return Vector(this->operator()(1, 0), this->operator()(1, 1));}

    void setR0(const Vector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y(); }
    void setR1(const Vector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y(); }

    Vector c0() const {return Vector(this->operator()(0, 0), this->operator()(1, 0)); }
    Vector c1() const {return Vector(this->operator()(0, 1), this->operator()(1, 1)); }

    void setC0(const Vector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y();}
    void setC1(const Vector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y();}
};

template <typename Type, typename RowVector, typename ColumnVector> class matrix2x3 : public QGenericMatrix<2,3,Type>
{
public:
    matrix2x3(){}

    matrix2x3(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix2x3(const ColumnVector &a, const ColumnVector &b)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();
        this->operator()(2,0) = a.z();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();
        this->operator()(2,1) = b.z();

    }

    // column wise intitialization as per GLSL
    matrix2x3(Type a, Type b, Type c, // first column
              Type d, Type e, Type f) // second column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;
        this->operator()(2,0) = c;

        this->operator()(0,1) = d;
        this->operator()(1,1) = e;
        this->operator()(2,1) = f;
    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}
    Type m20() const {return this->operator()(2, 0);}
    Type m21() const {return this->operator()(2, 1);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}
    void setM20(Type value) {this->operator()(2, 0) = value;}
    void setM21(Type value) {this->operator()(2, 1) = value;}

    RowVector r0() const {return RowVector(this->operator()(0, 0), this->operator()(0, 1));}
    RowVector r1() const {return RowVector(this->operator()(1, 0), this->operator()(1, 1));}
    RowVector r2() const {return RowVector(this->operator()(2, 0), this->operator()(2, 1));}

    void setR0(const RowVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y();}
    void setR1(const RowVector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y();}
    void setR2(const RowVector & value) {this->operator()(2, 0) =  value.x(); this->operator()(2, 1) = value.y();}

    ColumnVector c0() const {return ColumnVector(this->operator()(0, 0), this->operator()(1, 0), this->operator()(2, 0));}
    ColumnVector c1() const {return ColumnVector(this->operator()(0, 1), this->operator()(1, 1), this->operator()(2, 1));}

    void setC0(const ColumnVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y(); this->operator()(2, 0) = value.z();}
    void setC1(const ColumnVector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(2, 1) = value.z();}

};

template <typename Type, typename RowVector, typename ColumnVector> class matrix2x4 : public QGenericMatrix<2,4,Type>
{
public:
    matrix2x4(){}

    matrix2x4(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix2x4(const ColumnVector &a, const ColumnVector &b)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();
        this->operator()(2,0) = a.z();
        this->operator()(3,0) = a.w();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();
        this->operator()(2,1) = b.z();
        this->operator()(3,1) = b.w();

    }

    // column wise intitialization as per GLSL
    matrix2x4(Type a, Type b, Type c, Type d, // first column
              Type e, Type f, Type g, Type h) // second column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;
        this->operator()(2,0) = c;
        this->operator()(3,0) = d;

        this->operator()(0,1) = e;
        this->operator()(1,1) = f;
        this->operator()(2,1) = g;
        this->operator()(3,1) = h;

    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}
    Type m20() const {return this->operator()(2, 0);}
    Type m21() const {return this->operator()(2, 1);}
    Type m30() const {return this->operator()(3, 0);}
    Type m31() const {return this->operator()(3, 1);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}
    void setM20(Type value) {this->operator()(2, 0) = value;}
    void setM21(Type value) {this->operator()(2, 1) = value;}
    void setM30(Type value) {this->operator()(3, 0) = value;}
    void setM31(Type value) {this->operator()(3, 1) = value;}

    RowVector r0() const {return RowVector(this->operator()(0, 0), this->operator()(0, 1));}
    RowVector r1() const {return RowVector(this->operator()(1, 0), this->operator()(1, 1));}
    RowVector r2() const {return RowVector(this->operator()(2, 0), this->operator()(2, 1));}
    RowVector r3() const {return RowVector(this->operator()(3, 0), this->operator()(3, 1));}

    void setR0(const RowVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y(); }
    void setR1(const RowVector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y(); }
    void setR2(const RowVector & value) {this->operator()(2, 0) =  value.x(); this->operator()(2, 1) = value.y(); }
    void setR3(const RowVector & value) {this->operator()(3, 0) =  value.x(); this->operator()(3, 1) = value.y(); }

    ColumnVector c0() const {return ColumnVector(this->operator()(0, 0), this->operator()(1, 0), this->operator()(2, 0)); }
    ColumnVector c1() const {return ColumnVector(this->operator()(0, 1), this->operator()(1, 1), this->operator()(2, 1)); }
    ColumnVector c2() const {return ColumnVector(this->operator()(0, 2), this->operator()(1, 2), this->operator()(2, 2)); }

    void setC0(const ColumnVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y(); this->operator()(2, 0) = value.z();}
    void setC1(const ColumnVector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(2, 1) = value.z();}
    void setC2(const ColumnVector & value) {this->operator()(0, 2) =  value.x(); this->operator()(1, 2) = value.y(); this->operator()(2, 2) = value.z();}

};


class qmat2 : public matrix2<float, qvec2>
{
    Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)

    Q_PROPERTY(qvec2 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec2 r1 READ r1 WRITE setR1)

    Q_PROPERTY(qvec2 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec2 c1 READ c1 WRITE setC1)

public:
    using matrix2<float, qvec2>::matrix2;
    using QGenericMatrix<2,2,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};


class qmat2x3 : public matrix2x3<float, qvec2, qvec3>
{
  Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)
    Q_PROPERTY(float m20 READ m20 WRITE setM20)
    Q_PROPERTY(float m21 READ m21 WRITE setM21)

    Q_PROPERTY(qvec2 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec2 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qvec2 r2 READ r2 WRITE setR2)

    Q_PROPERTY(qvec3 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec3 c1 READ c1 WRITE setC1)

public:
    using matrix2x3<float, qvec2, qvec3>::matrix2x3;
    using QGenericMatrix<2,3,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class qmat2x4 : public matrix2x4<float, qvec2, qvec4>
{
  Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)
    Q_PROPERTY(float m20 READ m20 WRITE setM20)
    Q_PROPERTY(float m21 READ m21 WRITE setM21)
    Q_PROPERTY(float m30 READ m30 WRITE setM30)
    Q_PROPERTY(float m31 READ m31 WRITE setM31)

    Q_PROPERTY(qvec2 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec2 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qvec2 r2 READ r2 WRITE setR2)
    Q_PROPERTY(qvec2 r3 READ r3 WRITE setR3)

    Q_PROPERTY(qvec4 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec4 c1 READ c1 WRITE setC1)

public:
    using matrix2x4<float, qvec2, qvec4>::matrix2x4;
    using QGenericMatrix<2,4,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class qdmat2 : public matrix2<double, qdvec2>
{
    Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)

    Q_PROPERTY(qdvec2 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec2 r1 READ r1 WRITE setR1)

    Q_PROPERTY(qdvec2 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec2 c1 READ c1 WRITE setC1)

public:
    using matrix2<double, qdvec2>::matrix2;
    using QGenericMatrix<2,2,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};


class qdmat2x3 : public matrix2x3<double, qdvec2, qdvec3>
{
  Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)
    Q_PROPERTY(double m20 READ m20 WRITE setM20)
    Q_PROPERTY(double m21 READ m21 WRITE setM21)

    Q_PROPERTY(qdvec2 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec2 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qdvec2 r2 READ r2 WRITE setR2)

    Q_PROPERTY(qdvec3 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec3 c1 READ c1 WRITE setC1)

public:
    using matrix2x3<double, qdvec2, qdvec3>::matrix2x3;
    using QGenericMatrix<2,3,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class qdmat2x4 : public matrix2x4<double, qdvec2, qdvec4>
{
  Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)
    Q_PROPERTY(double m20 READ m20 WRITE setM20)
    Q_PROPERTY(double m21 READ m21 WRITE setM21)
    Q_PROPERTY(double m30 READ m30 WRITE setM30)
    Q_PROPERTY(double m31 READ m31 WRITE setM31)

    Q_PROPERTY(qdvec2 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec2 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qdvec2 r2 READ r2 WRITE setR2)
    Q_PROPERTY(qdvec2 r3 READ r3 WRITE setR3)

    Q_PROPERTY(qdvec4 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec4 c1 READ c1 WRITE setC1)

public:
    using matrix2x4<double, qdvec2, qdvec4>::matrix2x4;
    using QGenericMatrix<2,4,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};


Q_DECLARE_TYPEINFO(qmat2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qmat2x3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qmat2x4, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat2x3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat2x4, Q_MOVABLE_TYPE);

Q_DECLARE_METATYPE(qmat2)
Q_DECLARE_METATYPE(qmat2x3)
Q_DECLARE_METATYPE(qmat2x4)
Q_DECLARE_METATYPE(qdmat2)
Q_DECLARE_METATYPE(qdmat2x3)
Q_DECLARE_METATYPE(qdmat2x4)


#endif // QUICKMATRIX_H

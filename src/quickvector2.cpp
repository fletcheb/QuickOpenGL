#include "quickvector2.h"
#include <cmath>

/*!
 * \qmlbasictype vec2
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 * \brief The vec2 basic type represents a GLSL vec2 data type.
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty float vec2::x
 *
 * This property holds the x component of the vec2.
 */

/*!
 * \qmlproperty float vec2::y
 *
 * This property holds the y component of the vec2.
 */

/*!
 * \qmlproperty vec2 vec2::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty vec2 vec2::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty vec2 vec2::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty vec2 vec2::yy
 *
 * This property holds the swizzled yy value
 */


/*!
 * \qmlbasictype ivec2
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 * \brief The ivec2 basic type represents a GLSL ivec2 data type.
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty float ivec2::x
 *
 * This property holds the x component of the ivec2.
 */

/*!
 * \qmlproperty float ivec2::y
 *
 * This property holds the y component of the ivec2.
 */

/*!
 * \qmlproperty ivec2 ivec2::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty ivec2 ivec2::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty ivec2 ivec2::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty ivec2 ivec2::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlbasictype uvec2
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 * \brief The uvec2 basic type represents a GLSL uvec2 data type.
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty float uvec2::x
 *
 * This property holds the x component of the uvec2.
 */

/*!
 * \qmlproperty float uvec2::y
 *
 * This property holds the y component of the uvec2.
 */

/*!
 * \qmlproperty uvec2 uvec2::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty uvec2 uvec2::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty uvec2 uvec2::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty uvec2 uvec2::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlbasictype bvec2
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 * \brief The bvec2 basic type represents a GLSL bvec2 data type.
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty float bvec2::x
 *
 * This property holds the x component of the bvec2.
 */

/*!
 * \qmlproperty float bvec2::y
 *
 * This property holds the y component of the bvec2.
 */

/*!
 * \qmlproperty bvec2 bvec2::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty bvec2 bvec2::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty bvec2 bvec2::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty bvec2 bvec2::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlbasictype dvec2
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 * \brief The dvec2 basic type represents a GLSL dvec2 data type.
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty float dvec2::x
 *
 * This property holds the x component of the dvec2.
 */

/*!
 * \qmlproperty float dvec2::y
 *
 * This property holds the y component of the dvec2.
 */

/*!
 * \qmlproperty dvec2 dvec2::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty dvec2 dvec2::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty dvec2 dvec2::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty dvec2 dvec2::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlmethod float vec2::dotProduct(vec2 other)
 * \param other the other vector
 * \return the dot product of this vector and the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} dotProduct method.
 * \sa vector2d
 */
float qvec2::dotProduct(const qvec2 &other)
{
    return x()*other.x() + y()*other.y();
}

/*!
 * \qmlmethod vec2 vec2::times(vec2 other)
 * \param other the other vector
 * \return the result of multiplying this vector by the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} times method.
 * \sa vector2d
 */
qvec2 qvec2::times(const qvec2 &other)
{
    return *this*other;
}

/*!
 * \qmlmethod vec2 vec2::times(float factor)
 * \param factor the scalar factor
 * \return the result of scaling the vector by the scalar factor
 *
 * This method performs the same function as the equivalent QML \l{vector2d} times method.
 * \sa vector2d
 */
qvec2 qvec2::times(float factor)
{
    return *this*factor;
}

/*!
 * \qmlmethod vec2 vec2::plus(vec2 other)
 * \param other the other vector
 * \return the result of adding the other vector to this vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} plus method.
 * \sa vector2d
 */
qvec2 qvec2::plus(const qvec2 &other)
{
    return *this+other;
}

/*!
 * \qmlmethod vec2 vec2::minus(vec2 other)
 * \param other the other vector
 * \return the result of subtracting the other vector from this vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} minus method.
 * \sa vector2d
 */
qvec2 qvec2::minus(const qvec2 &other)
{
    return *this-other;
}

qvec2 qvec2::divide(float factor)
{
    return *this/factor;
}

qvec2 qvec2::divide(const qvec2 &other)
{
    return *this/other;
}


/*!
 * \qmlmethod vec2 vec2::normalized()
 * \return the normalized vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} normalized method.
 * \sa vector2d
 */
qvec2 qvec2::normalized()
{
    return (*this)*(1.0f/length());
}

/*!
 * \qmlmethod float vec2::length()
 * \return the length of the vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} length method.
 * \sa vector2d
 */
float qvec2::length()
{
    return std::sqrt(x()*x()+y()*y());
}

/*!
 * \qmlmethod double dvec2::dotProduct(dvec2 other)
 * \param other the other vector
 * \return the dot product of this vector and the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} dotProduct method.
 * \sa vector2d
 */
double qdvec2::dotProduct(const qdvec2 &other)
{
    return x()*other.x() + y()*other.y();
}

/*!
 * \qmlmethod dvec2 dvec2::times(dvec2 other)
 * \param other the other vector
 * \return the result of multiplying this vector by the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} times method.
 * \sa vector2d
 */
qdvec2 qdvec2::times(const qdvec2 &other)
{
    return *this*other;
}

/*!
 * \qmlmethod dvec2 dvec2::times(double factor)
 * \param factor the scalar factor
 * \return the result of scaling the vector by the scalar factor
 *
 * This method performs the same function as the equivalent QML \l{vector2d} times method.
 * \sa vector2d
 */
qdvec2 qdvec2::times(double factor)
{
    return *this*factor;
}

/*!
 * \qmlmethod dvec2 dvec2::plus(dvec2 other)
 * \param other the other vector
 * \return the result of adding the other vector to this vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} plus method.
 * \sa vector2d
 */
qdvec2 qdvec2::plus(const qdvec2 &other)
{
    return *this+other;
}

/*!
 * \qmlmethod dvec2 dvec2::minus(dvec2 other)
 * \param other the other vector
 * \return the result of subtracting the other vector from this vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} minus method.
 * \sa vector2d
 */
qdvec2 qdvec2::minus(const qdvec2 &other)
{
    return *this-other;
}

/*!
 * \qmlmethod dvec2 dvec2::normalized()
 * \return the normalized vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} normalized method.
 * \sa vector2d
 */
qdvec2 qdvec2::normalized()
{
    return*this*(1.0/length());
}

/*!
 * \qmlmethod double dvec2::length()
 * \return the length of the vector
 *
 * This method performs the same function as the equivalent QML \l{vector2d} length method.
 * \sa vector2d
 */
double qdvec2::length()
{
    return std::sqrt(x()*x()+y()*y());
}

qdvec2::operator qvec2() const
{
    qvec2 result;
    for(int i=0;i<2;++i)
    {
        result.data()[i] = float(constData()[i]);
    }

    return result;
}

qvec2::operator qdvec2() const
{
    qdvec2 result;
    for(int i=0;i<2;++i)
    {
        result.data()[i] = double(constData()[i]);
    }

    return result;

}



/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgtextureviewtextureprovider.h"

QuickSGTextureViewTextureProvider::QuickSGTextureViewTextureProvider() :
    m_sourceProvider(nullptr)
{
    connect(&m_texture, &QuickSGTextureViewTexture::textureChanged, this, &QuickSGTextureViewTextureProvider::textureChanged);
}

QSGTexture *QuickSGTextureViewTextureProvider::texture() const
{
    return &const_cast<QuickSGTextureViewTextureProvider *>(this)->m_texture;
}

QSGTextureProvider *QuickSGTextureViewTextureProvider::sourceProvider() const
{
    return m_sourceProvider;
}

void QuickSGTextureViewTextureProvider::setSourceProvider(QSGTextureProvider *sourceProvider)
{
    if (m_sourceProvider == sourceProvider)
        return;

    if(m_sourceProvider)
    {
        disconnect(m_sourceProvider, &QSGTextureProvider::textureChanged, this, &QSGTextureProvider::textureChanged);
        disconnect(m_sourceProvider, &QSGTextureProvider::destroyed, this, &QuickSGTextureViewTextureProvider::onSourceProviderDestroyed);
    }

    m_sourceProvider = sourceProvider;

    if(m_sourceProvider)
    {
        connect(m_sourceProvider, &QSGTextureProvider::textureChanged, this, &QSGTextureProvider::textureChanged, Qt::DirectConnection);
        connect(m_sourceProvider, &QSGTextureProvider::destroyed, this, &QuickSGTextureViewTextureProvider::onSourceProviderDestroyed);
        m_texture.setSourceTexture(m_sourceProvider->texture());
    }

    emit sourceProviderChanged(m_sourceProvider);
}

void QuickSGTextureViewTextureProvider::onSourceProviderDestroyed()
{
    m_sourceProvider = nullptr;
    m_texture.setSourceTexture(nullptr);
}

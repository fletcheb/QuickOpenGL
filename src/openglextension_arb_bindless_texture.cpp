/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "openglextension_arb_bindless_texture.h"
#include <QOpenGLContext>

class QOpenGLExtension_ARB_bindless_texturePrivate : public QAbstractOpenGLExtensionPrivate
{
public:
    GLuint64 (QOPENGLF_APIENTRYP GetTextureHandleARB)(GLuint);
    GLuint64 (QOPENGLF_APIENTRYP GetTextureSamplerHandleARB)(GLuint, GLuint);

    void (QOPENGLF_APIENTRYP MakeTextureHandleResidentARB)(GLuint64);
    void (QOPENGLF_APIENTRYP MakeTextureHandleNonResidentARB)(GLuint64);

    GLuint64 (QOPENGLF_APIENTRYP GetImageHandleARB)(GLuint, GLint, GLboolean, GLint, GLenum);

    void (QOPENGLF_APIENTRYP MakeImageHandleResidentARB)(GLuint64, GLenum);
    void (QOPENGLF_APIENTRYP MakeImageHandleNonResidentARB)(GLuint64);

    void (QOPENGLF_APIENTRYP UniformHandleui64ARB)(GLint, GLuint64);
    void (QOPENGLF_APIENTRYP UniformHandleui64vARB)(GLint, GLsizei, const GLuint64 *);
    void (QOPENGLF_APIENTRYP ProgramUniformHandleui64ARB)(GLuint, GLint, GLuint64);
    void (QOPENGLF_APIENTRYP ProgramUniformHandleui64vARB)(GLuint, GLint, GLsizei, const GLuint64 *);

    GLboolean (QOPENGLF_APIENTRYP IsTextureHandleResidentARB)(GLuint64);
    GLboolean (QOPENGLF_APIENTRYP IsImageHandleResidentARB)(GLuint64);

    void (QOPENGLF_APIENTRYP VertexAttribL1ui64ARB)(GLuint, GLuint64);
    void (QOPENGLF_APIENTRYP VertexAttribL1ui64vARB)(GLuint, const GLuint64 *);
    void (QOPENGLF_APIENTRYP GetVertexAttribLui64vARB)(GLuint, GLenum, GLuint64 *);
};

QOpenGLExtension_ARB_bindless_texture::QOpenGLExtension_ARB_bindless_texture() :
    QAbstractOpenGLExtension(*(new QOpenGLExtension_ARB_bindless_texturePrivate))
{

}

bool QOpenGLExtension_ARB_bindless_texture::initializeOpenGLFunctions()
{
    if (isInitialized())
        return true;

    QOpenGLContext *context = QOpenGLContext::currentContext();
    if (!context) {
        qWarning("A current OpenGL context is required to resolve OpenGL extension functions");
        return false;
    }

    // Resolve the functions
    Q_D(QOpenGLExtension_ARB_bindless_texture);

    d->GetTextureHandleARB = reinterpret_cast<GLuint64 (QOPENGLF_APIENTRYP)(GLuint)>(context->getProcAddress("glGetTextureHandleARB"));
    Q_CHECK_PTR(d->GetTextureHandleARB);
    d->GetTextureSamplerHandleARB = reinterpret_cast<GLuint64 (QOPENGLF_APIENTRYP)(GLuint, GLuint)>(context->getProcAddress("glGetTextureSamplerHandleARB"));
    Q_CHECK_PTR(d->GetTextureSamplerHandleARB);
    d->MakeTextureHandleResidentARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint64)>(context->getProcAddress("glMakeTextureHandleResidentARB"));
    Q_CHECK_PTR(d->MakeTextureHandleResidentARB);
    d->MakeTextureHandleNonResidentARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint64)>(context->getProcAddress("glMakeTextureHandleNonResidentARB"));
    Q_CHECK_PTR(d->MakeTextureHandleNonResidentARB);
    d->GetImageHandleARB = reinterpret_cast<GLuint64 (QOPENGLF_APIENTRYP)(GLuint, GLint, GLboolean, GLint, GLenum)>(context->getProcAddress("glGetImageHandleARB"));
    Q_CHECK_PTR(d->GetImageHandleARB);
    d->MakeImageHandleResidentARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint64, GLenum)>(context->getProcAddress("glMakeImageHandleResidentARB"));
    Q_CHECK_PTR(d->MakeImageHandleResidentARB);
    d->MakeImageHandleNonResidentARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint64)>(context->getProcAddress("glMakeImageHandleNonResidentARB"));
    Q_CHECK_PTR(d->MakeImageHandleNonResidentARB);
    d->UniformHandleui64ARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLint, GLuint64)>(context->getProcAddress("glUniformHandleui64ARB"));
    Q_CHECK_PTR(d->UniformHandleui64ARB);
    d->UniformHandleui64vARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLint, GLsizei, const GLuint64 *)>(context->getProcAddress("glUniformHandleui64vARB"));
    Q_CHECK_PTR(d->UniformHandleui64vARB);
    d->ProgramUniformHandleui64ARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint, GLint, GLuint64)>(context->getProcAddress("glProgramUniformHandleui64ARB"));
    Q_CHECK_PTR(d->ProgramUniformHandleui64ARB);
    d->ProgramUniformHandleui64vARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint, GLint, GLsizei, const GLuint64 *)>(context->getProcAddress("glProgramUniformHandleui64vARB"));
    Q_CHECK_PTR(d->ProgramUniformHandleui64vARB);
    d->IsTextureHandleResidentARB = reinterpret_cast<GLboolean (QOPENGLF_APIENTRYP)(GLuint64)>(context->getProcAddress("glIsTextureHandleResidentARB"));
    Q_CHECK_PTR(d->IsTextureHandleResidentARB);
    d->IsImageHandleResidentARB = reinterpret_cast<GLboolean (QOPENGLF_APIENTRYP)(GLuint64)>(context->getProcAddress("glIsImageHandleResidentARB"));
    Q_CHECK_PTR(d->IsImageHandleResidentARB);
    d->VertexAttribL1ui64ARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint, GLuint64)>(context->getProcAddress("glVertexAttribL1ui64ARB"));
    Q_CHECK_PTR(d->VertexAttribL1ui64ARB);
    d->VertexAttribL1ui64vARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint, const GLuint64 *)>(context->getProcAddress("glVertexAttribL1ui64vARB"));
    Q_CHECK_PTR(d->VertexAttribL1ui64vARB);
    d->GetVertexAttribLui64vARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint, GLenum, GLuint64 *)>(context->getProcAddress("glGetVertexAttribLui64vARB"));
    Q_CHECK_PTR(d->GetVertexAttribLui64vARB);

    QAbstractOpenGLExtension::initializeOpenGLFunctions();

    return d->GetTextureHandleARB!=nullptr;

}

GLuint64 QOpenGLExtension_ARB_bindless_texture::glGetTextureHandleARB(GLuint texture)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    return d->GetTextureHandleARB(texture);
}

GLuint64 QOpenGLExtension_ARB_bindless_texture::glGetTextureSamplerHandleARB(GLuint texture, GLuint sampler)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    return d->GetTextureSamplerHandleARB(texture, sampler);

}

void QOpenGLExtension_ARB_bindless_texture::glMakeTextureHandleResidentARB(GLuint64 handle)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    d->MakeTextureHandleResidentARB(handle);

}

void QOpenGLExtension_ARB_bindless_texture::glMakeTextureHandleNonResidentARB(GLuint64 handle)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    d->MakeTextureHandleNonResidentARB(handle);
}

GLuint64 QOpenGLExtension_ARB_bindless_texture::glGetImageHandleARB(GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    return d->GetImageHandleARB(texture, level, layered, layer, format);

}

void QOpenGLExtension_ARB_bindless_texture::glMakeImageHandleResidentARB(GLuint64 handle, GLenum access)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    d->MakeImageHandleResidentARB(handle, access);

}

void QOpenGLExtension_ARB_bindless_texture::glMakeImageHandleNonResidentARB(GLuint64 handle)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    d->MakeImageHandleNonResidentARB(handle);

}

void QOpenGLExtension_ARB_bindless_texture::glUniformHandleui64ARB(GLint location, GLuint64 value)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    d->UniformHandleui64ARB(location, value);
}

void QOpenGLExtension_ARB_bindless_texture::glUniformHandleui64vARB(GLint location, GLsizei count, const GLuint64 *value)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    d->UniformHandleui64vARB(location, count, value);
}

void QOpenGLExtension_ARB_bindless_texture::glProgramUniformHandleui64ARB(GLuint program, GLint location, GLuint64 value)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    d->ProgramUniformHandleui64ARB(program, location, value);
}

void QOpenGLExtension_ARB_bindless_texture::glProgramUniformHandleui64vARB(GLuint program, GLint location, GLsizei count, const GLuint64 *values)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    d->ProgramUniformHandleui64vARB(program, location, count, values);

}

GLboolean QOpenGLExtension_ARB_bindless_texture::glIsTextureHandleResidentARB(GLuint64 handle)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    return d->IsTextureHandleResidentARB(handle);

}

GLboolean QOpenGLExtension_ARB_bindless_texture::glIsImageHandleResidentARB(GLuint64 handle)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);
    return d->IsImageHandleResidentARB(handle);

}

void QOpenGLExtension_ARB_bindless_texture::glVertexAttribL1ui64ARB(GLuint index, GLuint64 x)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);

    d->VertexAttribL1ui64ARB(index, x);
}

void QOpenGLExtension_ARB_bindless_texture::glVertexAttribL1ui64vARB(GLuint index, const GLuint64 *v)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);

    d->VertexAttribL1ui64vARB(index, v);

}

void QOpenGLExtension_ARB_bindless_texture::glGetVertexAttribLui64vARB(GLuint index, GLenum pname, GLuint64 *params)
{
    Q_D(QOpenGLExtension_ARB_bindless_texture);

    d->GetVertexAttribLui64vARB(index, pname, params);

}


/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#ifndef QUICKCOMPUTEPROGRAM_H
#define QUICKCOMPUTEPROGRAM_H

#include <QQuickItem>
#include <QScopedPointer>
#include <QOpenGLContext>


class QuickComputeProgramPrivate;

class QuickComputeProgram : public QQuickItem
{

    Q_INTERFACES(QQmlParserStatus)

    Q_OBJECT

    Q_PROPERTY(QByteArray computeShader READ computeShader WRITE setComputeShader NOTIFY computeShaderChanged)

    Q_PROPERTY(bool hasComputeVariableGroupSizeExtension READ hasComputeVariableGroupSizeExtension NOTIFY hasComputeVariableGroupSizeExtensionChanged)
    Q_PROPERTY(int xGroups READ xGroups WRITE setXGroups NOTIFY xGroupsChanged)
    Q_PROPERTY(int yGroups READ yGroups WRITE setYGroups NOTIFY yGroupsChanged)
    Q_PROPERTY(int zGroups READ zGroups WRITE setZGroups NOTIFY zGroupsChanged)

    Q_PROPERTY(int xGroupSize READ xGroupSize WRITE setXGroupSize NOTIFY xGroupSizeChanged)
    Q_PROPERTY(int yGroupSize READ yGroupSize WRITE setYGroupSize NOTIFY yGroupSizeChanged)
    Q_PROPERTY(int zGroupSize READ zGroupSize WRITE setZGroupSize NOTIFY zGroupSizeChanged)

    Q_PROPERTY(Barriers memoryBarrier READ memoryBarrier WRITE setMemoryBarrier NOTIFY memoryBarrierChanged)

    Q_PROPERTY(bool dispatchOnUniformPropertyChanges READ dispatchOnUniformPropertyChanges WRITE setDispatchOnUniformPropertyChanges NOTIFY dispatchOnUniformPropertyChangesChanged)
    Q_PROPERTY(bool dispatchOnGroupsAndGroupSizeChanges READ dispatchOnGroupsAndGroupSizeChanges WRITE setDispatchOnGroupsAndGroupSizeChanges NOTIFY dispatchOnGroupsAndGroupSizeChangesChanged)

    // Introspected values
    Q_PROPERTY(QVariantMap uniforms READ uniforms NOTIFY uniformsChanged)
    Q_PROPERTY(QVariantMap subroutineUniforms READ subroutineUniforms NOTIFY subroutineUniformsChanged)
    Q_PROPERTY(QVariantMap uniformBlocks READ uniformBlocks NOTIFY uniformBlocksChanged)
    Q_PROPERTY(QVariantMap shaderStorageBlocks READ shaderStorageBlocks NOTIFY shaderStorageBlocksChanged)



public:

    enum Barrier
    {
        BarrierNone = 0,
        BarrierVertexAttribArray = 0x00000001, // GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT
        BarrierElementArray = 0x00000002, // GL_ELEMENT_ARRAY_BARRIER_BIT
        BarrierUniform = 0x00000004, // GL_UNIFORM_BARRIER_BIT
        BarrierTextureFetch = 0x00000008, // GL_TEXTURE_FETCH_BARRIER_BIT
        BarrierShaderImageAccess = 0x00000020, // GL_SHADER_IMAGE_ACCESS_BARRIER_BIT
        BarrierCommand = 0x00000040, // GL_COMMAND_BARRIER_BIT,
        BarrierPixelBuffer = 0x00000080, // GL_PIXEL_BUFFER_BARRIER_BIT
        BarrierTextureUpdate = 0x00000100, // GL_TEXTURE_UPDATE_BARRIER_BIT
        BarrierBufferUpdate = 0x00000200, // GL_BUFFER_UPDATE_BARRIER_BIT
        BarrierFramebuffer = 0x00000400, // GL_FRAMEBUFFER_BARRIER_BIT
        BarrierTransformFeedback = 0x00000800, // GL_TRANSFORM_FEEDBACK_BARRIER_BIT
        BarrierAtomicCounter = 0x00001000, // GL_ATOMIC_COUNTER_BARRIER_BIT
        BarrierShaderStorage = 0x00002000, // GL_SHADER_STORAGE_BARRIER_BIT
        BarrierClientMappedBuffer = 0x00004000, // GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT
        BarrierQueryBuffer = 0x00008000, // GL_QUERY_BUFFER_BARRIER_BIT
        BarrierAll = 0xFFFFFFFF // GL_ALL_BARRIER_BITS
    };
    Q_ENUM(Barrier)
    Q_DECLARE_FLAGS(Barriers, Barrier)
    Q_FLAG(Barriers)

    QuickComputeProgram(QQuickItem *parent = 0);
    ~QuickComputeProgram();

    QByteArray computeShader() const;
    int xGroups() const;
    int yGroups() const;
    int zGroups() const;
    int xGroupSize() const;
    int yGroupSize() const;
    int zGroupSize() const;
    Barriers memoryBarrier() const;
    bool dispatchOnUniformPropertyChanges() const;
    bool dispatchOnGroupsAndGroupSizeChanges() const;
    bool hasComputeVariableGroupSizeExtension() const;

    QVariantMap uniforms() const;

    QVariantMap subroutineUniforms() const;

    QVariantMap uniformBlocks() const;

    QVariantMap shaderStorageBlocks() const;

public slots:

    void setComputeShader(QByteArray computeShader);
    void setXGroups(int xGroups);
    void setYGroups(int yGroups);
    void setZGroups(int zGroups);
    void setXGroupSize(int xGroupSize);
    void setYGroupSize(int yGroupSize);
    void setZGroupSize(int zGroupSize);
    void setMemoryBarrier(Barriers memoryBarrier);
    void setDispatchOnUniformPropertyChanges(bool dispatchOnUniformPropertyChanges);
    void setDispatchOnGroupsAndGroupSizeChanges(bool dispatchOnGroupsAndGroupSizeChanges);
    void dispatch();
    void invalidateSceneGraph();

signals:

    void computeShaderChanged(QByteArray computeShader);
    void xGroupsChanged(int xGroups);
    void yGroupsChanged(int yGroups);
    void zGroupsChanged(int zGroups);
    void xGroupSizeChanged(int xGroupSize);
    void yGroupSizeChanged(int yGroupSize);
    void zGroupSizeChanged(int zGroupSize);
    void memoryBarrierChanged(Barriers memoryBarrier);
    void dispatchOnUniformPropertyChangesChanged(bool dispatchOnUniformPropertyChanges);
    void dispatchOnGroupsAndGroupSizeChangesChanged(bool dispatchOnGroupsAndGroupSizeChanges);
    void hasComputeVariableGroupSizeExtensionChanged(bool hasComputeVariableGroupSizeExtension);
    void dispatched(const QVariantMap &state);

    void uniformsChanged(QVariantMap uniforms);

    void subroutineUniformsChanged(QVariantMap subroutineUniforms);

    void uniformBlocksChanged(QVariantMap uniformBlocks);

    void shaderStorageBlocksChanged(QVariantMap shaderStorageBlocks);

protected:

    QScopedPointer<QuickComputeProgramPrivate> d_ptr;

    QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override;

    void releaseResources() override;

private:

    Q_DECLARE_PRIVATE(QuickComputeProgram)
};

#endif // QUICKCOMPUTEPROGRAM_H


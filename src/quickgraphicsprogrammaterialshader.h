/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKGRAPHICSPROGRAMMATERIALSHADER_H
#define QUICKGRAPHICSPROGRAMMATERIALSHADER_H

#include <QSGMaterialShader>
#include <QScopedPointer>
#include <QQuickWindow>

#include "introspection.h"

class QuickGraphicsProgramMaterialShaderPrivate;

class QuickGraphicsProgramMaterialShader : public QSGMaterialShader
{
public:
    QuickGraphicsProgramMaterialShader();
    ~QuickGraphicsProgramMaterialShader();

    // Set the material shader source code
    void setVertexShader(const QByteArray &vertexShader);
    void setFragmentShader(const QByteArray &fragmentShader);
    void setGeometryShader(const QByteArray &geometryShader);
    void setTessellationControlShader(const QByteArray &tessellationControlShader);
    void setTessellationEvaluationShader(const QByteArray &tessellationEvaluationShader);

    // this item is referenced in debug messages to give context    
    void setQmlItem(const QObject *qmlItem);
    QString qmlItemName() const;

    void setQQuickWindow(QQuickWindow *window);

    // sets the material attributes
    void setAttributeNames(const QList<Introspection::Attribute> &attributes);

    void activate() override;
    void deactivate() override;
    void updateState(const RenderState &state, QSGMaterial *newMaterial, QSGMaterial *oldMaterial) override;
    const char * const *attributeNames() const override;

protected:

    void compile() override;
    void initialize() override;
    const char *vertexShader() const override;
    const char *fragmentShader() const override;

    QScopedPointer<QuickGraphicsProgramMaterialShaderPrivate> d_ptr;

private:

    Q_DECLARE_PRIVATE(QuickGraphicsProgramMaterialShader)
};

#endif // QUICKGRAPHICSPROGRAMMATERIALSHADER_H

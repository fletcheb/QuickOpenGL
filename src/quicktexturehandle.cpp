/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include <QOpenGLFunctions_3_3_Core>
#include <QQuickWindow>
#include <QSGTextureProvider>
#include <QThread>
#include "quicktexturehandle.h"
#include "quicksgtexturehandleprovider.h"
#include "openglextension_arb_bindless_texture.h"
#include "support.h"

/*!
 * \qmltype TextureHandle
 * \brief The TextureHandle QML type represents an OpenGL texture handle.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickTextureHandle
 * \ingroup QuickOpenGL QML Types
 * 
 *
 * TextureHandle allows OpenGL ARB bindless textures to be used directly in
 * QML. It takes a texture provided by a source QML object, and generates an
 * OpenGL ARB bindless texture handle. It is intended to be used as a \l QML
 * property within a GraphicsProgram or \l ComputeProgram matched to a
 * corresponding OpenGL shader uint64_t uniform.
 *
 * Example usage:
 *
 * \qml
 * Image {
 *   id: image
 *   source: 'image.jpg'
 *   visible: false
 * }
 *
 * TextureHandle {
 *   id: textureHandle
 *   source: image
 * }
 *
 * GraphicsProgram {
 *   anchors.fill: parent
 *   property var handle: textureHandle
 *
 *   fragmentShader: "
 *     #version 430
 *     #extension GL_ARB_gpu_shader_int64 : enable
 *     #extension GL_ARB_bindless_texture : require
 *
 *     in vec2 coord;
 *     out vec4 fragColor;
 *     uniform uint64_t handle;
 *
 *     void main()
 *     {
 *       fragColor = texture(sampler2D(handle), coord);
 *     }"
 * }
 * \endqml
 *
 * \sa Texture
 * \sa ImageHandle
 * \sa GraphicsProgram
 * \sa ComputeProgram
 * \sa https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_bindless_texture.txt
 */


class QuickTextureHandlePrivate : public QObject
{
    Q_OBJECT

public:

    QuickTextureHandlePrivate(QuickTextureHandle *q);

    QQuickItem * source;
    QuickSGTextureHandleProvider *textureHandleProvider;

public slots:

    void onSceneGraphInvalidated();

protected:

    QuickTextureHandle *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickTextureHandle)
};

QuickTextureHandlePrivate::QuickTextureHandlePrivate(QuickTextureHandle *q) :
    source(nullptr),
    textureHandleProvider(nullptr),
    q_ptr(q)
{

}

void QuickTextureHandlePrivate::onSceneGraphInvalidated()
{
    if(textureHandleProvider)
    {
        delete textureHandleProvider;
        textureHandleProvider = nullptr;
    }
}

QuickTextureHandle::QuickTextureHandle(QQuickItem *parent) :
    QQuickItem(parent),
    d_ptr(new QuickTextureHandlePrivate(this))
{

    setFlag(QQuickItem::ItemHasContents);
}

QuickTextureHandle::~QuickTextureHandle()
{

}

QuickSGTextureHandleProvider *QuickTextureHandle::textureHandleProvider()
{
    Q_D(QuickTextureHandle);

    // make sure we are in the render thread
    if(window()&&window()->openglContext()&&window()->openglContext()->thread()&&(window()->openglContext()->thread()==QThread::currentThread()))
    {
        if(!d->textureHandleProvider)
        {
            d->textureHandleProvider = new QuickSGTextureHandleProvider();
            connect(window(), &QQuickWindow::sceneGraphInvalidated, d, &QuickTextureHandlePrivate::onSceneGraphInvalidated, Qt::DirectConnection);
        }

        if(d->source&&d->source->isTextureProvider()&&d->source->textureProvider())
        {
            d->textureHandleProvider->setSourceProvider(d->source->textureProvider());

            return d->textureHandleProvider;
        }
    }

    return nullptr;
}


/*!
 * \qmlproperty object TextureHandle::source
 *
 * This property holds the source texture provider object. It provides the
 * texture for which a texture handle is generated. The default value is null
 */
QQuickItem *QuickTextureHandle::source() const
{
    Q_D(const QuickTextureHandle);

    return d->source;
}

void QuickTextureHandle::setSource(QQuickItem *source)
{
    Q_D(QuickTextureHandle);

    if (d->source == source)
        return;

    d->source = source;
    emit sourceChanged(source);

    if(d->source&&(!d->source->isTextureProvider()))
    {
        qWarning() << "QuickOpenGL:" << this << "source set to" << source << "which is not a texture provider";
    }

    update();
}

QSGNode *QuickTextureHandle::updatePaintNode(QSGNode *, QQuickItem::UpdatePaintNodeData *)
{
    Q_D(QuickTextureHandle);

    if(d->textureHandleProvider&&d->source&&d->source->textureProvider())
    {
        // synchronise
        d->textureHandleProvider->setSourceProvider(d->source->textureProvider());
    }
    return nullptr;
}

void QuickTextureHandle::releaseResources()
{
    Q_D(QuickTextureHandle);

    if(d->textureHandleProvider)
    {
        window()->scheduleRenderJob(cleanup(d->textureHandleProvider), QQuickWindow::NoStage);
        d->textureHandleProvider = nullptr;
    }
}

#include "quicktexturehandle.moc"

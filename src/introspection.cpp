/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/



#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLFunctions_4_3_Core>
#include <QOpenGLFunctions_4_0_Core>
#include <QOpenGLExtraFunctions>
#include <QSGTextureProvider>
#include <QSGTexture>
#include <QElapsedTimer>

#include "introspection.h"
#include "quickshaderstoragebuffer.h"
#include "quicksgssboprovider.h"
#include "quicksgssbo.h"
#include "quickuniformbuffer.h"
#include "quicksguniformbufferprovider.h"
#include "quicksguniformbuffer.h"
#include "support.h"
#include "openglextension_arb_bindless_texture.h"
#include "quickimagehandle.h"
#include "quicksgimagehandleprovider.h"
#include "quicktexturehandle.h"
#include "quicksgtexturehandleprovider.h"

Q_DECLARE_OPAQUE_POINTER(QSGTextureProvider)
Q_DECLARE_METATYPE(QMetaObject::Connection)


/*!
 * \internal
 * \brief Bind an image texture or array of image textures
 * \param property the QML property value
 * \param uniform the introspected GLSL uniform properties
 * \param imageUnit the current image unit to bind to
 * \param qmlItem the QML item which owns the uniform
 * \return true on success
 *
 * This function binds the appropriate QML property values to the GLSL image
 * uniform.
 *
 */
bool bindImageTexture(const QVariant &property, const Introspection::Uniform &uniform, int &imageUnit)
{
    static QOpenGLExtension_ARB_bindless_texture glArbBindless;

    // convert property to a list
    QVariantList list = property.canConvert<QVariantList>()?property.value<QVariantList>():QVariantList({property});

    bool result = list.count()>0;

    // bind each image uniform in the list
    for(int uniformIndex=0;uniformIndex<qMin(uniform.arraySize, list.count());++uniformIndex)
    {
        // if the variant is a texture provider
        if(list.at(uniformIndex).canConvert<QSGTextureProvider *>()&&list.at(uniformIndex).value<QSGTextureProvider *>()->texture())
        {

            QSGTexture *texture = list.at(uniformIndex).value<QSGTextureProvider *>()->texture();

            //
            // Get properties implemented by QuickOpenGL's QuickSGTexture
            //
            int imageLayer = -1;
            if(texture->property("imageLayer").isValid())
            {
                imageLayer = texture->property("imageLayer").toInt();
            }

            int imageLevel = 0;
            if(texture->property("imageLevel").isValid())
            {
                imageLevel = texture->property("imageLevel").toInt();
            }

            uint imageAccess = GL_READ_WRITE;
            if(texture->property("imageAccess").isValid())
            {
                imageAccess = texture->property("imageAccess").toUInt();
            }

            uint format = GL_RGBA32F;
            if(texture->property("format").isValid())
            {
                format = texture->property("format").toUInt();
            }

            // Get the current context and OpenGL functions
            Q_ASSERT(QOpenGLContext::currentContext());

            QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
            Q_ASSERT(glExtra);
            glExtra->initializeOpenGLFunctions();

            // set the uniform location to the current image unit
            glExtra->glUniform1i(uniform.location + uniformIndex, imageUnit);debugGL;

            // bind the image texture
            glExtra->glBindImageTexture(imageUnit, texture->textureId(), imageLevel, imageLayer==-1?GL_TRUE:GL_FALSE, imageLayer>0?imageLayer:0, imageAccess, format);debugGL;

            // increment the image unit
            imageUnit++;

        }

        // if the variant is an image handle provider
        else if(list.at(uniformIndex).canConvert<QuickSGImageHandleProvider *>())
        {
            QuickSGImageHandleProvider *image = list.at(uniformIndex).value<QuickSGImageHandleProvider *>();

            // if the image has a source set
            if(image->sourceProvider())
            {
                quint64 handle = image->handle();
                image->makeResident();

                // bind the image
                bool ok = glArbBindless.initializeOpenGLFunctions();
                Q_ASSERT(ok);
                glArbBindless.glUniformHandleui64ARB(uniform.location+uniformIndex, handle);debugGL;

            }
        }

        // the variant is something we don't know how to handle
        else
        {
            result = false;
        }
    }

    return result;

}

/*!
 * \internal
 * \brief Bind a texture or array of textures
 * \param property the QML property value
 * \param uniform the introspected GLSL uniform properties
 * \param textureUnit the current texture unit
 * \param qmlItem the QML item which owns the uniform
 * \return true on success
 *
 * This function binds the appropriate QML property values to the GLSL sampler
 * uniform.
 */
bool bindTexture(const QVariant &property, const Introspection::Uniform &uniform, int &textureUnit)
{
    static QOpenGLExtension_ARB_bindless_texture glArbBindless;

    QVariantList list = property.canConvert<QVariantList>()?property.toList():QVariantList({property});

    bool result = list.count()>0;

    // bind each texture uniform in the list
    for(int uniformIndex=0;uniformIndex<qMin(uniform.arraySize, list.count());++uniformIndex)
    {
        // if the variant is a texture provider
        if(list.at(uniformIndex).canConvert<QSGTextureProvider *>())
        {
            QSGTextureProvider *provider = list.at(uniformIndex).value<QSGTextureProvider *>();

            // if the provider has a valid texture
            if(provider&&provider->texture())
            {
                QSGTexture *texture = provider->texture();

                //
                // Get properties implemented by QuickSGTexture and QuickSGSamplerTextureProvider
                //
                GLuint samplerObject = 0;
                if(provider->property("SamplerObject").isValid())
                {
                    samplerObject = provider->property("SamplerObject").toUInt();
                }

                // if the texture is an atlas, remove it
                if(texture->isAtlasTexture())
                {
                    texture = texture->removedFromAtlas();
                }

                // Get the current context and OpenGL functions
                QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
                Q_ASSERT(glExtra);
                glExtra->initializeOpenGLFunctions();

                // set the uniform location to the current texture unit
                glExtra->glUniform1i(uniform.location + uniformIndex, textureUnit);debugGL;

                // select the texture unit
                glExtra->glActiveTexture(GL_TEXTURE0 + GLenum(textureUnit));debugGL;

                // Bind sampler object to this texture unit
                glExtra->glBindSampler(GLenum(textureUnit), samplerObject);debugGL;

                // bind the texture
                texture->bind();debugGL;

                // increment the texture unit
                textureUnit++;
            }
        }

        // if the variant is a texture handle
        else if (list.at(uniformIndex).canConvert<QuickSGTextureHandleProvider *>())
        {
            QuickSGTextureHandleProvider *texture = list.at(uniformIndex).value<QuickSGTextureHandleProvider *>();

            // if the texture has a source set
            if(texture->sourceProvider())
            {
                quint64 handle = texture->handle();
                texture->makeResident();

                // bind the texture
                bool ok = glArbBindless.initializeOpenGLFunctions();
                Q_ASSERT(ok);
                glArbBindless.glUniformHandleui64ARB(uniform.location+uniformIndex, handle);debugGL;

            }
        }

        // if the variant is something we don't know how to handle
        else
        {
            result = false;
        }
    }

    return result;
}

template <typename Type> QVariant getBufferData(const QByteArray &buffer, const Introspection::ShaderStorageBlock::Variable &variable, int topLevel)
{

    int arraySize = variable.arraySize==0?(buffer.count()-variable.offset)/variable.arrayStride:variable.arraySize;
    int offset = variable.offset + topLevel*variable.topLevelArrayStride;

    QVariantList list;

    for(int i=0;i<arraySize;++i)
    {
        list.append(QVariant::fromValue(reinterpret_cast<const Type &>(buffer.constData()[offset +i*variable.arrayStride])));
    }

    if(list.count()==1)
    {
        return list.at(0);
    }

    return list;

}

template <typename Type> bool setBufferData(QVariant property, QByteArray &buffer, const Introspection::Uniform uniform, const Introspection::UniformBlock &block, const QQuickItem *qmlItem)
{

    QVariantList list = Introspection::conditionProperty(property, uniform.name, uniform.type, uniform.arraySize, "uniform", qmlItem);

    bool result = list.count()>0;

    if(uniform.arraySize!=list.count())
    {
        qWarning() << "QuickOpenGL:" << qmlItem << "Uniform buffer variable array size mismatch:" << QString(block.name + "." + uniform.name) << "OpenGL shader storage block contains" << uniform.arraySize << "element(s), QML property contains" << list.count() << "element(s)";
    }


    for(int i=0;i<qMin(uniform.arraySize, list.count());++i)
    {
        result &= list.at(i).canConvert<Type>();

        if(list.at(i).canConvert<Type>())
        {
            reinterpret_cast<Type &>(buffer.data()[uniform.offset +i*uniform.arrayStride]) = list.at(i).value<Type>();
        }
    }

    return result;
}

QList<Introspection::UniformBlock> Introspection::activeUniformBlocks(GLuint program)
{
    QList<Introspection::UniformBlock> result;

    // Get the current context and OpenGL functions
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();

    Q_ASSERT(glExtra);
    if(!glExtra)
    {
        return result;
    }

    // Get the number of active uniforms
    GLint activeUniformBlocks = 0;
    glExtra->glGetProgramiv(program, GL_ACTIVE_UNIFORM_BLOCKS, &activeUniformBlocks);debugGL;

    // Get the active uniform name length
    GLint activeUniformMaxLength = 0;
    glExtra->glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &activeUniformMaxLength);debugGL;

    // Get the active uniform block name length - fails on intel windows with GL_INVALID_ENUM
    // as a work around - we proceed with the activeUniformMaxLength - seems to work.
    GLint activeUniformBlockMaxNameLength = activeUniformMaxLength;
    glExtra->glGetProgramiv(program, GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH, &activeUniformBlockMaxNameLength);debugGL;

    for(GLuint index =0; index<GLuint(activeUniformBlocks); index++)
    {
        UniformBlock block;

        QByteArray name(activeUniformBlockMaxNameLength, 0);
        GLint length = 0;
        glExtra->glGetActiveUniformBlockName(program, index, name.count(), &length, name.data());debugGL;
        name.resize(length);

        GLint binding = 0;
        glExtra->glGetActiveUniformBlockiv(program, index, GL_UNIFORM_BLOCK_BINDING, &binding);debugGL;

        GLint size = 0;
        glExtra->glGetActiveUniformBlockiv(program, index, GL_UNIFORM_BLOCK_DATA_SIZE, &size);debugGL;

        GLint activeUniforms = 0;
        glExtra->glGetActiveUniformBlockiv(program, index, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &activeUniforms);debugGL;

        QVector<GLuint> uniformIndices(activeUniforms, 0);
        glExtra->glGetActiveUniformBlockiv(program, index, GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, (GLint *)uniformIndices.data());debugGL;

        block.name = name;
        block.binding = binding;
        block.size = size;
        block.index = index;
        block.programId = program;

        for(const GLuint index: uniformIndices)
        {

            QByteArray name(activeUniformMaxLength, 0);
            GLint length = 0;

            GLint size = 0;
            GLenum type = GL_INVALID_ENUM;
            glExtra->glGetActiveUniform(program, index, name.count(), &length, &size, &type, name.data());debugGL;
            name.resize(length);

            GLint blockIndex = -1;
            glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_BLOCK_INDEX, &blockIndex);debugGL;

            GLint offset = -1;
            glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_OFFSET, &offset);debugGL;

            GLint arrayStride = -1;
            glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_ARRAY_STRIDE, &arrayStride);debugGL;

            GLint matrixStride = -1;
            glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_MATRIX_STRIDE, &matrixStride);debugGL;

            GLint isRowMajor = 0;
            glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_IS_ROW_MAJOR, &isRowMajor);debugGL;

            GLint atomicCounterBufferIndex = -1;
            if((QOpenGLContext::currentContext()->format().majorVersion()*100+QOpenGLContext::currentContext()->format().minorVersion()*10)>=420)
            {
                glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX, &atomicCounterBufferIndex);debugGL;
            }

            GLint location = glExtra->glGetUniformLocation(program, name.constData());debugGL;

            Uniform uniform;
            uniform.location = location;
            uniform.type = type;
            uniform.arraySize = size;
            uniform.name = name;
            uniform.blockIndex = blockIndex;
            uniform.offset = offset;
            uniform.arrayStride = arrayStride;
            uniform.matrixStride = matrixStride;
            uniform.isRowMajor = isRowMajor;
            uniform.atomicCounterBufferIndex = atomicCounterBufferIndex;

            block.uniforms[uniform.name] = uniform;
        }

        result.append(block);

    }

    return result;
}

QList<Introspection::Uniform> Introspection::activeUniforms(GLuint program)
{
    QList<Introspection::Uniform> result;

    // Get the current context and OpenGL functions
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();

    Q_ASSERT(glExtra);
    if(!glExtra)
    {
        return result;
    }

    // Make sure the program is valid
    Q_ASSERT(glExtra->glIsProgram(program));
    if(!glExtra->glIsProgram(program))
    {
        return result;
    }

    // Get the number of active uniforms
    GLint activeUniforms = 0;
    glExtra->glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &activeUniforms);debugGL;

    // Get the active uniform name length
    GLint activeUniformMaxLength = 0;
    glExtra->glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &activeUniformMaxLength);debugGL;

    // Introspect information for each active uniform
    for(GLuint index=0; index<GLuint(activeUniforms); index++)
    {
        QByteArray name(activeUniformMaxLength, 0);
        GLint length = 0;
        GLint size = 0;
        GLenum type = GL_INVALID_ENUM;
        glExtra->glGetActiveUniform(program, index, name.count(), &length, &size, &type, name.data());debugGL;
        name.resize(length);

        GLint blockIndex = -1;
        glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_BLOCK_INDEX, &blockIndex);debugGL;

        GLint offset = -1;
        glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_OFFSET, &offset);debugGL;

        GLint arrayStride = -1;
        glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_ARRAY_STRIDE, &arrayStride);debugGL;

        GLint matrixStride = -1;
        glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_MATRIX_STRIDE, &matrixStride);debugGL;

        GLint isRowMajor = 0;
        glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_IS_ROW_MAJOR, &isRowMajor);debugGL;

        GLint atomicCounterBufferIndex = -1;
        if((QOpenGLContext::currentContext()->format().majorVersion()*100+QOpenGLContext::currentContext()->format().minorVersion()*10)>=420)
        {
            glExtra->glGetActiveUniformsiv(program, 1, &index, GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX, &atomicCounterBufferIndex);debugGL;
        }

        GLint location = glExtra->glGetUniformLocation(program, name.constData());debugGL;

        Uniform uniform;
        uniform.location = location;
        uniform.type = type;
        uniform.arraySize = size;
        uniform.name = name;
        uniform.blockIndex = blockIndex;
        uniform.offset = offset;
        uniform.arrayStride = arrayStride;
        uniform.matrixStride = matrixStride;
        uniform.isRowMajor = isRowMajor;
        uniform.atomicCounterBufferIndex = atomicCounterBufferIndex;

        if(uniform.location!=-1)
        {
            result.append(uniform);
        }

    }

    return result;


}

QList<Introspection::Attribute> Introspection::activeAttributes(GLuint program)
{
    QList<Attribute> result;

    debugGL;

    Q_ASSERT(QOpenGLContext::currentContext());

    if(!QOpenGLContext::currentContext())
    {
        return result;
    }

    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;

    Q_ASSERT(gl);
    if(!gl)
    {
        return result;
    }

    Q_ASSERT(gl->glIsProgram(program));debugGL;
    if(!gl->glIsProgram(program))
    {
        return result;
    }

    GLint activeAttributes = 0;
    gl->glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &activeAttributes);debugGL;

    GLint activeAttributeMaxLength = 0;
    gl->glGetProgramiv(program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &activeAttributeMaxLength);debugGL;

    for(GLuint index=0; index< GLuint(activeAttributes); index++)
    {
        GLenum type=GL_INVALID_ENUM;
        GLint size=0;
        GLint length=0;
        QByteArray name(activeAttributeMaxLength, 0);
        gl->glGetActiveAttrib(program, index, name.count(), &length, &size, &type, name.data());debugGL;
        name.resize(length);

        Attribute attribute;
        attribute.name = name;
        attribute.type = type;
        attribute.arraySize = size;
        attribute.location = gl->glGetAttribLocation(program, name.constData());debugGL;

        // do not add built in gl_* attributes
        if(attribute.name.left(3)!=QByteArrayLiteral("gl_"))
        {
            result.append(attribute);
        }

    }

    // sort in ascending order of attribute location
    std::sort(result.begin(), result.end(), [](const Attribute &a, const Attribute &b){return a.location<b.location;});

    return result;
}

QList<Introspection::ShaderStorageBlock> Introspection::activeShaderStorageBlocks(GLuint program)
{
    QList<Introspection::ShaderStorageBlock> result;
    Q_ASSERT(QOpenGLContext::currentContext());

    if(!QOpenGLContext::currentContext())
    {
        return result;
    }

    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;

    if(!glExtra)
    {
        return result;
    }
    glExtra->initializeOpenGLFunctions();debugGL;

    Q_ASSERT(glExtra->glIsProgram(program));debugGL;
    if(!glExtra->glIsProgram(program))
    {
        return result;
    }

    // Get the number of active buffers
    GLuint activeBuffers = 0;
    glExtra->glGetProgramInterfaceiv(program, GL_SHADER_STORAGE_BLOCK, GL_ACTIVE_RESOURCES, reinterpret_cast<GLint *>(&activeBuffers));debugGL;

    // Get the maximum name length of the active storage blocks
    GLint maxNameLength = 0;
    glExtra->glGetProgramInterfaceiv(program, GL_SHADER_STORAGE_BLOCK, GL_MAX_NAME_LENGTH, &maxNameLength);debugGL;

    QHash<GLuint, QString> bindings;

    for(GLuint index = 0; index < activeBuffers; ++index)
    {
        ShaderStorageBlock block;

        block.programId = program;

        block.index = index;

        // get the shader storage block name
        GLsizei length = 0;
        QByteArray shaderStorageBlockName(maxNameLength, 0);
        glExtra->glGetProgramResourceName(program, GL_SHADER_STORAGE_BLOCK, index, shaderStorageBlockName.count(), &length, shaderStorageBlockName.data());debugGL;
        Q_ASSERT(length);
        block.name = shaderStorageBlockName;

        // get the program properties
        QVector<GLenum> properties({GL_BUFFER_BINDING, GL_BUFFER_DATA_SIZE, GL_NUM_ACTIVE_VARIABLES});
        QVector<GLuint> results(properties.count(),0);
        glExtra->glGetProgramResourceiv(program, GL_SHADER_STORAGE_BLOCK, index, properties.count(), properties.constData(), results.size(), &length, reinterpret_cast<GLint *>(results.data()));debugGL;
        Q_ASSERT(length==results.size());
#ifndef QT_OPENGL_ES
        block.binding = results.at(0);
        if(bindings.contains(block.binding))
        {

            if(block.binding!=0)
            {
                qWarning() << "QuickOpenGL: Attempting to bind shader storage block" << block.name << "to binding" << block.binding << "which is already bound to" << bindings[block.binding];
            }

            GLint maxShaderStorageBufferBindings = 0;
            glExtra->glGetIntegerv(GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS, &maxShaderStorageBufferBindings);debugGL;

            QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();debugGL;
            if(!gl43)
            {
                continue;
            }
            bool ok = gl43->initializeOpenGLFunctions();debugGL;
            Q_ASSERT(ok);
            if(!ok)
            {
                continue;
            }

            for(int i=int(maxShaderStorageBufferBindings)-1;i>=0;--i)
            {
                if(!bindings.keys().contains(i))
                {
                    block.binding = GLuint(i);
                    gl43->glShaderStorageBlockBinding(program, index, block.binding);debugGL;
                    break;
                }
            }

        }
#endif

        bindings[block.binding] = block.name;
        block.size = results.at(1);

        int activeVariables = results.at(2);
        properties.resize(1);
        properties[0] = GL_ACTIVE_VARIABLES;
        results.resize(activeVariables);
        glExtra->glGetProgramResourceiv(program, GL_SHADER_STORAGE_BLOCK, index, properties.count(), properties.constData(), results.size(), &length, reinterpret_cast<GLint *>(results.data()));debugGL;
        Q_ASSERT(length==activeVariables);

        // for each variable
        for(const GLint variableIndex : results)
        {
            // get the variable name length
            GLint nameLength = 0;
            glExtra->glGetProgramInterfaceiv(program, GL_BUFFER_VARIABLE, GL_MAX_NAME_LENGTH, &nameLength);debugGL;

            // get the variable name
            QByteArray variableName(nameLength, 0);
            GLint length =0;
            glExtra->glGetProgramResourceName(program, GL_BUFFER_VARIABLE, variableIndex, variableName.count(), &length, variableName.data());debugGL;
            variableName.truncate(length);

            // get the program properties
            QVector<GLenum> properties({GL_TYPE, GL_ARRAY_SIZE, GL_OFFSET, GL_BLOCK_INDEX, GL_ARRAY_STRIDE, GL_MATRIX_STRIDE, GL_IS_ROW_MAJOR, GL_TOP_LEVEL_ARRAY_SIZE, GL_TOP_LEVEL_ARRAY_STRIDE });
            QVector<GLint> results(properties.count(),0);
            glExtra->glGetProgramResourceiv(program, GL_BUFFER_VARIABLE, variableIndex, properties.count(), properties.constData(), results.size(), &length, results.data());debugGL;
            Q_ASSERT(length==results.size());

            // if the variable name is prepended with the block name.  This happens when we give
            // the buffer block a name (mytest) and access members as mytest.variable.  OpenGL
            // returns variable name "test.variable".  Remove the "test." component.
            //
            // buffer test {
            //   int variable
            // } mytest;
            //
            if(variableName.startsWith((block.name+".").toLocal8Bit()))
            {
                variableName.remove(0, block.name.count()+1);
            }

            block.variables[variableName].name = variableName;
            block.variables[variableName].type = results.at(0);
            block.variables[variableName].arraySize = results.at(1);
            block.variables[variableName].offset = results.at(2);
            block.variables[variableName].blockIndex = results.at(3);
            block.variables[variableName].arrayStride = results.at(4);
            block.variables[variableName].matrixStride = results.at(5);
            block.variables[variableName].isRowMajor = results.at(6);
            block.variables[variableName].topLevelArraySize = results.at(7);
            block.variables[variableName].topLevelArrayStride = results.at(8);

            // For top level arrays AMD and NVIDIA have interpreted the specification
            // differently.  We standardize to the NVIDIA interpretation.
            //
            // buffer test {
            //    vec4 variable[10];
            // }
            //
            // Component            NVIDIA          AMD
            // --------------------+----------------+--------------
            // name                |  variable[0]   | variable[0]
            // arraySize           |  10            | 10
            // arrayStride         |  16            | 16
            // topLevelArraySize   |  1             | 10
            // topLevelArrayStride |  0             | 16
            //
            if((block.variables[variableName].name.count('[')==1)
               &&(block.variables[variableName].arraySize==block.variables[variableName].topLevelArraySize)
               &&(block.variables[variableName].arrayStride==block.variables[variableName].topLevelArrayStride))
            {
                block.variables[variableName].topLevelArraySize = 1;
                block.variables[variableName].topLevelArrayStride = 0;
            }

        }

        result.append(block);
    }



    return result;
}

/*!
 * \internal
 * \brief Condition a variant into a state that can be used in the QSGRenderThread context
 * \param variant The input QVariant
 * \param notifyItem The Item to notify when the variant changes
 * \return The conditioned variant
 *
 * This function is run in \l QQuickItem::updatePaintNode overrides. This
 * allows it to run in the QSGRenderContext thread while the main thread is
 * locked, making the calls used safe.
 *
 * \note This function is called recursively
 */
QVariant Introspection::conditionVariant(const QVariant &variant, QObject *item, bool getProviders, QQuickWindow *window)
{

    QVariant result = variant;

    QObject *notifyItem = window?reinterpret_cast<QObject *>(window):item;

    if(getProviders)
    {

        // if the property is a QQuickItem
        if(variant.canConvert<QQuickItem *>()&&variant.value<QQuickItem *>())
        {
            // if the item is a texture provider
            if(variant.value<QQuickItem *>()->isTextureProvider()&&variant.value<QQuickItem *>()->textureProvider())
            {
                // get the texture provider
                result = QVariant::fromValue(variant.value<QQuickItem *>()->textureProvider());

                if(notifyItem)
                {
                    QSGTextureProvider *object = variant.value<QQuickItem *>()->textureProvider();

                    QString name = QString("notify%1").arg(qint64(notifyItem));
                    if(!object->property(name.toLocal8Bit().constData()).isValid())
                    {
                        QMetaMethod slot = notifyItem->metaObject()->method(notifyItem->metaObject()->indexOfSlot("update()"));
                        QMetaMethod signal = object->metaObject()->method(object->metaObject()->indexOfSignal("textureChanged()"));
                        QMetaObject::Connection connection = QObject::connect(object, signal, notifyItem, slot);
                        object->setProperty(name.toLocal8Bit().constData(), QVariant::fromValue(connection));
                        Q_ASSERT(connection);
                    }
                }
            }
            // if the item is a shader storage buffer
            else if(variant.canConvert<QuickShaderStorageBuffer *>())
            {
                // get the ssbo provider
                result = QVariant::fromValue(variant.value<QuickShaderStorageBuffer *>()->ssboProvider());

                if(notifyItem)
                {
                    QuickSGSSBOProvider *object = variant.value<QuickShaderStorageBuffer *>()->ssboProvider();
                    Q_ASSERT(object);

                    QString name = QString("notify%1").arg(qint64(notifyItem));
                    if(object&&(!object->property(name.toLocal8Bit().constData()).isValid()))
                    {
                        QMetaMethod slot = notifyItem->metaObject()->method(notifyItem->metaObject()->indexOfSlot("update()"));
                        QMetaMethod signal = object->metaObject()->method(object->metaObject()->indexOfSignal("ssboChanged()"));
                        QMetaObject::Connection connection = QObject::connect(object, signal, notifyItem, slot);
                        object->setProperty(name.toLocal8Bit().constData(), QVariant::fromValue(connection));
                        Q_ASSERT(connection);
                    }
                }

            }
            // if the item is a uniform buffer
            else if(variant.canConvert<QuickUniformBuffer *>())
            {
                result = QVariant::fromValue(variant.value<QuickUniformBuffer *>()->uniformBufferProvider());

                if(notifyItem)
                {
                    QuickSGUniformBufferProvider *object = variant.value<QuickUniformBuffer *>()->uniformBufferProvider();
                    Q_ASSERT(object);

                    QString name = QString("notify%1").arg(qint64(notifyItem));
                    if(object&&(!object->property(name.toLocal8Bit().constData()).isValid()))
                    {
                        QMetaMethod slot = notifyItem->metaObject()->method(notifyItem->metaObject()->indexOfSlot("update()"));
                        QMetaMethod signal = object->metaObject()->method(object->metaObject()->indexOfSignal("uniformBufferChanged()"));
                        QMetaObject::Connection connection = QObject::connect(object, signal, notifyItem, slot);
                        object->setProperty(name.toLocal8Bit().constData(), QVariant::fromValue(connection));
                        Q_ASSERT(connection);
                    }
                }


            }
            // if the item is a texture handle
            else if (variant.canConvert<QuickTextureHandle *>())
            {
                result = QVariant::fromValue(variant.value<QuickTextureHandle *>()->textureHandleProvider());

                if(notifyItem)
                {
                    QuickSGTextureHandleProvider *object = variant.value<QuickTextureHandle *>()->textureHandleProvider();
                    Q_ASSERT(object);

                    QString name = QString("notify%1").arg(qint64(notifyItem));
                    if(object&&(!object->property(name.toLocal8Bit().constData()).isValid()))
                    {
                        QMetaMethod slot = notifyItem->metaObject()->method(notifyItem->metaObject()->indexOfSlot("update()"));
                        QMetaMethod signal = object->metaObject()->method(object->metaObject()->indexOfSignal("textureChanged()"));
                        QMetaObject::Connection connection = QObject::connect(object, signal, notifyItem, slot);
                        object->setProperty(name.toLocal8Bit().constData(), QVariant::fromValue(connection));
                        Q_ASSERT(connection);
                    }
                }
            }
            // if the item is an image handle
            else if (variant.canConvert<QuickImageHandle *>())
            {
                result = QVariant::fromValue(variant.value<QuickImageHandle *>()->imageHandleProvider());

                if(notifyItem)
                {
                    QuickSGImageHandleProvider *object = variant.value<QuickImageHandle *>()->imageHandleProvider();
                    Q_ASSERT(object);

                    QString name = QString("notify%1").arg(qint64(notifyItem));
                    if(object&&(!object->property(name.toLocal8Bit().constData()).isValid()))
                    {                        
                        QMetaMethod slot = notifyItem->metaObject()->method(notifyItem->metaObject()->indexOfSlot("update()"));
                        QMetaMethod signal = object->metaObject()->method(object->metaObject()->indexOfSignal("imageChanged()"));
                        QMetaObject::Connection connection = QObject::connect(object, signal, notifyItem, slot);
                        object->setProperty(name.toLocal8Bit().constData(), QVariant::fromValue(connection));
                        Q_ASSERT(connection);
                    }
                }
            }
        }
        // if the property is a QObject
        else if(variant.canConvert<QObject *>()&&variant.value<QObject *>())
        {
            QObject *object = variant.value<QObject *>();

            // Convert QML Matrix4x4 to QMatrix4x4
            if(object->property("matrix").isValid())
            {
                result = variant.value<QObject *>()->property("matrix");
                if(item)
                {
                    QString name = QString("notify%1").arg(qint64(item));
                    if(!object->property(name.toLocal8Bit().constData()).isValid())
                    {
                        QMetaMethod slot = item->metaObject()->method(item->metaObject()->indexOfSlot("update()"));
                        QMetaMethod signal = object->metaObject()->property(object->metaObject()->indexOfProperty("matrix")).notifySignal();
                        QVariant connection = QVariant::fromValue(QObject::connect(object, signal, item, slot));
                        object->setProperty(name.toLocal8Bit().constData(), connection);
                    }
                }
            }
        }
    }

    // if the property is a QVariantHash
    if(variant.canConvert<QVariantHash>()&&!variant.value<QVariantHash>().isEmpty())
    {
        QVariantHash hash = variant.value<QVariantHash>();

        for(QVariantHash::iterator it = hash.begin(); it!= hash.end(); ++it)
        {
            it.value() = conditionVariant(it.value(), notifyItem, getProviders, window);
        }
        result = hash;
    }
    // if the property is a QVariantMap
    else if(variant.canConvert<QVariantMap>()&&!variant.value<QVariantMap>().isEmpty())
    {
        QVariantMap map = variant.value<QVariantMap>();

        for(QVariantMap::iterator it = map.begin(); it!= map.end(); ++it)
        {
            it.value() = conditionVariant(it.value(), notifyItem, getProviders, window);
        }
        result = map;
    }
    // if the property is a variant list
    else if(variant.canConvert<QVariantList>()&&!variant.value<QVariantList>().isEmpty())
    {
        QVariantList list = variant.value<QVariantList>();

        for(int i=0;i<list.count();++i)
        {
            list[i] = conditionVariant(list.at(i), notifyItem, getProviders, window);
        }

        result = QVariant::fromValue(list);
    }

    return result;

}


QList<Introspection::SubroutineUniform> Introspection::activeSubroutineUniforms(GLuint program, GLuint shader)
{
    QList<Introspection::SubroutineUniform> result;

#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    if(!gl40)
    {
        qWarning() << "QuickOpenGL: OpenGL 4 required for subroutine support. Current context is OpenGL" << qPrintable(QString("%1.%2").arg(QOpenGLContext::currentContext()->format().version().first).arg(QOpenGLContext::currentContext()->format().version().first));
        return result;
    }
    bool ok = gl40->initializeOpenGLFunctions();
    Q_ASSERT(ok);

    // the number of active subroutine uniform locations
    GLint subroutineUniforms=0;
    gl40->glGetProgramStageiv(program, shader, GL_ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS, &subroutineUniforms);debugGL;

    // Get the maximum name length of the active subroutine uniforms
    GLint activeSubroutineUniformMaxLength=0;
    gl40->glGetProgramStageiv(program, shader, GL_ACTIVE_SUBROUTINE_UNIFORM_MAX_LENGTH, &activeSubroutineUniformMaxLength);debugGL;

    // get the maximum name length of the active subroutines
    GLint activeSubroutineMaxLength=0;
    gl40->glGetProgramStageiv(program, shader, GL_ACTIVE_SUBROUTINE_MAX_LENGTH, &activeSubroutineMaxLength);debugGL;

    for(int index=0; index<subroutineUniforms; ++index)
    {
        SubroutineUniform uniform;

        // get the subroutine name
        int length;
        QByteArray name(activeSubroutineUniformMaxLength, Qt::Uninitialized);
        gl40->glGetActiveSubroutineUniformName(program, shader, index, name.size(), &length, name.data());debugGL;
        name.resize(length);
        uniform.name = name;

        // get the subroutine uniform location
        uniform.location = gl40->glGetSubroutineUniformLocation(program, shader, name);debugGL;

        // get the number of compatible subroutines
        int numCompatibleSubroutines = 0;
        gl40->glGetActiveSubroutineUniformiv(program, shader, index, GL_NUM_COMPATIBLE_SUBROUTINES, &numCompatibleSubroutines);debugGL;

        // get the list of compatible subroutines
        QVector<int> compatibleSubroutines(numCompatibleSubroutines);
        gl40->glGetActiveSubroutineUniformiv(program, shader, index, GL_COMPATIBLE_SUBROUTINES, compatibleSubroutines.data());debugGL;

        for(int subroutineIndex: compatibleSubroutines)
        {
            // get the subroutine name
            QByteArray name(activeSubroutineMaxLength, Qt::Uninitialized);
            gl40->glGetActiveSubroutineName(program, shader, subroutineIndex, name.size(), &length, name.data());debugGL;
            name.resize(length);

            uniform.compatibleSubroutines[name] = subroutineIndex;
        }

        result.append(uniform);
    }
#endif
    return result;
}

void Introspection::setSubroutineUniforms(GLuint shader, const QList<Introspection::SubroutineUniform> &activeSubroutineUniforms, const QVariantHash &properties)
{
#ifndef QT_OPENGL_ES
    QVector<GLuint> subroutineUniform(activeSubroutineUniforms.count(),0);

    for(const Introspection::SubroutineUniform &uniform: activeSubroutineUniforms)
    {
        // load a default compatible value
        if(!uniform.compatibleSubroutines.isEmpty())
        {
            subroutineUniform[uniform.location] = uniform.compatibleSubroutines.values().at(0);
        }

        QString name = uniform.name;

        // if the property is valid
        if(properties.contains(name))
        {
            // get the property
            QVariant property = properties[name];

            if(property.canConvert<QString>()&&uniform.compatibleSubroutines.contains(property.value<QString>().toLocal8Bit()))
            {
                subroutineUniform[uniform.location] = uniform.compatibleSubroutines[property.value<QString>().toLocal8Bit()];
            }
            else
            {
                qWarning() << "QuickOpenGL:" << "subroutine uniform" << uniform.name << "QML property value" << property.toString() << "is not a compatible subroutine" << uniform.compatibleSubroutines.keys();
            }
        }
        else
        {
            qWarning() << "QuickOpenGL:" << "No property found for OpenGL subroutine uniform" << uniform.name << "containing compatible subroutine" << uniform.compatibleSubroutines.keys();
        }
    }

    if(subroutineUniform.count())
    {
        QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
        Q_ASSERT(gl40);
        bool ok = gl40->initializeOpenGLFunctions();
        Q_ASSERT(ok);

        gl40->glUniformSubroutinesuiv(shader, subroutineUniform.count(), subroutineUniform.constData());debugGL;
    }
#endif
}


inline void setUniform(int location, float value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform1f(location, value);debugGL;
}

inline void setUniform(int location, const qvec2 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform2f(location, value(0,0), value(1,0));debugGL;

}

inline void setUniform(int location, const qvec3 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform3f(location, value(0,0), value(1,0), value(2,0));debugGL;

}

inline void setUniform(int location, const qvec4 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform4f(location, value(0,0), value(1,0), value(2,0), value(3,0));debugGL;
}

inline void setUniform(int location, const qmat2 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniformMatrix2fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, const qmat3 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniformMatrix3fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, const qmat4 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniformMatrix4fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, const qmat2x3 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniformMatrix2x3fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, const qmat2x4 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniformMatrix2x4fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, const qmat3x2 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniformMatrix3x2fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, const qmat3x4 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniformMatrix3x4fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, const qmat4x2 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniformMatrix4x2fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, const qmat4x3 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniformMatrix4x3fv(location, 1, GL_FALSE, value.constData());debugGL;
}

inline void setUniform(int location, int value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform1i(location, value);debugGL;
}

inline void setUniform(int location, const qivec2 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform2i(location, value(0,0), value(1,0));debugGL;
}

inline void setUniform(int location, const qivec3 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform3i(location, value(0,0), value(1,0), value(2,0));debugGL;
}

inline void setUniform(int location, const qivec4 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform4i(location, value(0,0), value(1,0), value(2,0), value(3,0));debugGL;
}

inline void setUniform(int location, bool value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform1i(location, value);debugGL;
}

inline void setUniform(int location, const qbvec2 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform2i(location, value(0,0), value(1,0));debugGL;
}

inline void setUniform(int location, const qbvec3 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform3i(location, value(0,0), value(1,0), value(2,0));debugGL;
}

inline void setUniform(int location, const qbvec4 &value)
{
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();debugGL;
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();debugGL;

    gl->glUniform4i(location, value(0,0), value(1,0), value(2,0), value(3,0));debugGL;
}

inline void setUniform(int location, uint value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniform1ui(location, value);debugGL;
}

inline void setUniform(int location, const quvec2 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniform2ui(location, value(0,0), value(1,0));debugGL;
}

inline void setUniform(int location, const quvec3 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniform3ui(location, value(0,0), value(1,0), value(2,0));debugGL;
}

inline void setUniform(int location, const quvec4 &value)
{
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    glExtra->glUniform4ui(location, value(0,0), value(1,0), value(2,0), value(3,0));debugGL;
}

inline void setUniform(int location, double value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniform1d(location, value);debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif

}

inline void setUniform(int location, const qdvec2 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniform2d(location, value(0,0), value(1,0));debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdvec3 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniform3d(location, value(0,0), value(1,0), value(2,0));debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdvec4 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniform4d(location, value(0,0), value(1,0), value(2,0), value(3,0));debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat2 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix2dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat3 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix3dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat4 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix4dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat2x3 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix2x3dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat2x4 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix2x4dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat3x2 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix3x2dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat3x4 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix3x4dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat4x2 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix4x2dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, const qdmat4x3 &value)
{
#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();debugGL;
    Q_ASSERT(gl40);
    bool ok = gl40->initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl40->glUniformMatrix4x3dv(location, 1, GL_FALSE, value.constData());debugGL;
#else
    qWarning() << "QuickOpenGL: double precision floating point not supported by OpenGL ES";
#endif
}

inline void setUniform(int location, quint64 value)
{
    static QOpenGLExtension_ARB_bindless_texture gl;
    bool ok = gl.initializeOpenGLFunctions();debugGL;
    Q_ASSERT(ok);

    gl.glUniformHandleui64ARB(location, value);debugGL;
}

/*
 * \internal
 * \brief Set a uniform
 * \param property the conditioned QML property value for the uniform
 * \param uniform the introspected GLSL uniform properties
 * \return true on success
 *
 * This template function set the appropriate QML property values to the GLSL
 * uniform.
 */
template <typename Type> bool setUniform(const QVariant &property, const Introspection::Uniform &uniform)
{
    QVariantList list = property.canConvert<QVariantList>()?property.toList():QVariantList({property});

    bool result = list.count()>0;

    for(int uniformIndex=0;uniformIndex<qMin(list.count(), uniform.arraySize);++uniformIndex)
    {
        result &= list.at(uniformIndex).canConvert<Type>();
        setUniform(uniform.location + uniformIndex, list.at(uniformIndex).value<Type>());
    }

    return result;
}

void Introspection::setUniformBlocks(const QList<Introspection::UniformBlock> &activeUniformBlocks, const QVariantHash &properties, GLuint program)
{
    // for each introspected uniform block
    for(const UniformBlock &uniformBlock : activeUniformBlocks)
    {
        // if we have a material property which matches this uniform block name
        if(properties.contains(uniformBlock.name))
        {

            // get the matching property
            QVariant property = properties[uniformBlock.name];

            // if the property is a uniform buffer provider
            if(property.isValid()&&property.canConvert<QuickSGUniformBufferProvider *>())
            {

                // the uniform buffer provider
                QuickSGUniformBufferProvider *provider = property.value<QuickSGUniformBufferProvider *>();
                Q_ASSERT(provider);

                // the uniform buffer
                QuickSGUniformBuffer *uniformBuffer = provider->uniformBuffer();
                Q_ASSERT(uniformBuffer);

                // update the uniform buffer if required
                if(provider->updated())
                {
                    uniformBuffer->bufferData(provider->data());
                    provider->setUpdated(false);
                }

                // bind the buffer
                uniformBuffer->bind();

                // bind the buffer base
                uniformBuffer->bindBufferBase(uniformBlock.binding);
                Q_ASSERT(QOpenGLContext::currentContext());
                QOpenGLExtraFunctions*glExtra = QOpenGLContext::currentContext()->extraFunctions();
                Q_ASSERT(glExtra);
                glExtra->initializeOpenGLFunctions();

                glExtra->glUniformBlockBinding(program, uniformBlock.index, uniformBlock.binding);debugGL;

                // release the buffer
                uniformBuffer->release();
            }
        }
    }
}


void Introspection::setUniforms(const QList<Introspection::Uniform> &activeUniforms, const QVariantHash &properties)
{
    // the texture unit to bind to
    int textureUnit = 0;

    // the image unit to bind to
    int imageUnit = 0;

    // iterate through our list of uniforms
    for(const Uniform &uniform: activeUniforms)
    {
        QString name = uniform.name;

        // if the property exists
        if(properties.contains(name))
        {
            QVariant property = properties[name];

            // do the appropriate thing for the uniform type
            switch(uniform.type)
            {
            case GL_SAMPLER_1D:
            case GL_INT_SAMPLER_1D:
            case GL_UNSIGNED_INT_SAMPLER_1D:
            case GL_SAMPLER_2D:
            case GL_INT_SAMPLER_2D:
            case GL_UNSIGNED_INT_SAMPLER_2D:
            case GL_SAMPLER_3D:
            case GL_INT_SAMPLER_3D:
            case GL_UNSIGNED_INT_SAMPLER_3D:
            case GL_SAMPLER_CUBE:
            case GL_INT_SAMPLER_CUBE:
            case GL_UNSIGNED_INT_SAMPLER_CUBE:
            case GL_SAMPLER_1D_ARRAY:
            case GL_INT_SAMPLER_1D_ARRAY:
            case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
            case GL_SAMPLER_2D_ARRAY:
            case GL_INT_SAMPLER_2D_ARRAY:
            case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
            case GL_SAMPLER_BUFFER:
            case GL_INT_SAMPLER_BUFFER:
            case GL_UNSIGNED_INT_SAMPLER_BUFFER:
                bindTexture(property, uniform, textureUnit);
                break;
            case GL_FLOAT:
                setUniform<float>(property, uniform);
                break;
            case GL_FLOAT_VEC2:
                setUniform<qvec2>(property, uniform);
                break;
            case GL_FLOAT_VEC3:
                setUniform<qvec3>(property, uniform);
                break;
            case GL_FLOAT_VEC4:
                setUniform<qvec4>(property, uniform);
                break;
            case GL_DOUBLE:
                setUniform<double>(property, uniform);
                break;
            case GL_DOUBLE_VEC2:
                setUniform<qdvec2>(property, uniform);
                break;
            case GL_DOUBLE_VEC3:
                setUniform<qdvec3>(property, uniform);
                break;
            case GL_DOUBLE_VEC4:
                setUniform<qdvec4>(property, uniform);
                break;
            case GL_INT:
                setUniform<int>(property, uniform);
                break;
            case GL_INT_VEC2:
                setUniform<qivec2>(property, uniform);
                break;
            case GL_INT_VEC3:
                setUniform<qivec3>(property, uniform);
                break;
            case GL_INT_VEC4:
                setUniform<qivec4>(property, uniform);
                break;
            case GL_UNSIGNED_INT:
                setUniform<unsigned int>(property, uniform);
                break;
            case GL_UNSIGNED_INT_VEC2:
                setUniform<quvec2>(property, uniform);
                break;
            case GL_UNSIGNED_INT_VEC3:
                setUniform<quvec3>(property, uniform);
                break;
            case GL_UNSIGNED_INT_VEC4:
                setUniform<quvec4>(property, uniform);
                break;
            case GL_BOOL:
                setUniform<bool>(property, uniform);
                break;
            case GL_BOOL_VEC2:
                setUniform<qbvec2>(property, uniform);
                break;
            case GL_BOOL_VEC3:
                setUniform<qbvec3>(property, uniform);
                break;
            case GL_BOOL_VEC4:
                setUniform<qbvec4>(property, uniform);
                break;
            case GL_FLOAT_MAT2:
                setUniform<qmat2>(property, uniform);
                break;
            case GL_FLOAT_MAT3:
                setUniform<qmat3>(property, uniform);
                break;
            case GL_FLOAT_MAT4:
                setUniform<qmat4>(property, uniform);
                break;
            case GL_FLOAT_MAT2x3:
                setUniform<qmat2x3>(property, uniform);
                break;
            case GL_FLOAT_MAT2x4:
                setUniform<qmat2x4>(property, uniform);
                break;
            case GL_FLOAT_MAT3x2:
                setUniform<qmat3x2>(property, uniform);
                break;
            case GL_FLOAT_MAT3x4:
                setUniform<qmat3x4>(property, uniform);
                break;
            case GL_FLOAT_MAT4x2:
                setUniform<qmat4x2>(property, uniform);
                break;
            case GL_FLOAT_MAT4x3:
                setUniform<qmat4x3>(property, uniform);
                break;
            case GL_DOUBLE_MAT2:
                setUniform<qdmat2>(property, uniform);
                break;
            case GL_DOUBLE_MAT3:
                setUniform<qdmat3>(property, uniform);
                break;
            case GL_DOUBLE_MAT4:
                setUniform<qdmat4>(property, uniform);
                break;
            case GL_DOUBLE_MAT2x3:
                setUniform<qdmat2x3>(property, uniform);
                break;
            case GL_DOUBLE_MAT2x4:
                setUniform<qdmat2x4>(property, uniform);
                break;
            case GL_DOUBLE_MAT3x2:
                setUniform<qdmat3x2>(property, uniform);
                break;
            case GL_DOUBLE_MAT3x4:
                setUniform<qdmat3x4>(property, uniform);
                break;
            case GL_DOUBLE_MAT4x2:
                setUniform<qdmat4x2>(property, uniform);
                break;
            case GL_DOUBLE_MAT4x3:
                setUniform<qdmat4x3>(property, uniform);
                break;
            case GL_UNSIGNED_INT64_ARB:
                setUniform<quint64>(property, uniform);
                break;
            case GL_IMAGE_1D:
            case GL_IMAGE_2D:
            case GL_IMAGE_3D:
            case GL_IMAGE_2D_RECT:
            case GL_IMAGE_CUBE:
            case GL_IMAGE_BUFFER:
            case GL_IMAGE_1D_ARRAY:
            case GL_IMAGE_2D_ARRAY:
            case GL_IMAGE_2D_MULTISAMPLE:
            case GL_IMAGE_2D_MULTISAMPLE_ARRAY:
            case GL_INT_IMAGE_1D:
            case GL_INT_IMAGE_2D:
            case GL_INT_IMAGE_3D:
            case GL_INT_IMAGE_2D_RECT:
            case GL_INT_IMAGE_CUBE:
            case GL_INT_IMAGE_BUFFER:
            case GL_INT_IMAGE_1D_ARRAY:
            case GL_INT_IMAGE_2D_ARRAY:
            case GL_INT_IMAGE_2D_MULTISAMPLE:
            case GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
            case GL_UNSIGNED_INT_IMAGE_1D:
            case GL_UNSIGNED_INT_IMAGE_2D:
            case GL_UNSIGNED_INT_IMAGE_3D:
            case GL_UNSIGNED_INT_IMAGE_2D_RECT:
            case GL_UNSIGNED_INT_IMAGE_CUBE:
            case GL_UNSIGNED_INT_IMAGE_BUFFER:
            case GL_UNSIGNED_INT_IMAGE_1D_ARRAY:
            case GL_UNSIGNED_INT_IMAGE_2D_ARRAY:
            case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE:
            case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
                bindImageTexture(property, uniform, imageUnit);
                break;
            default:
                break;
            }

        }
    }
}

/*!
 * \internal
 * \brief Set shader storage block data
 * \param shaderStorageBlocks list of introspected shader storage blocks
 * \param properties QML properties
 */
void Introspection::setShaderStorageBlocks(const QList<Introspection::ShaderStorageBlock> &shaderStorageBlocks, const QVariantHash &properties)
{    

    for(const Introspection::ShaderStorageBlock &shaderStorageBlock: shaderStorageBlocks)
    {

        QString name = shaderStorageBlock.name;

        // if we have a material property which matches this shader storage block name and is an SSBO provider
        if(properties.contains(name))
        {
            QVariant property = properties[name];

            if(property.isValid()&&property.canConvert<QuickSGSSBOProvider *>())
            {
                // The shader storage bufer object provider
                QuickSGSSBOProvider *provider = property.value<QuickSGSSBOProvider *>();
                Q_ASSERT(provider);

                // The shader storage buffer object
                QuickSGSSBO *ssbo = provider->ssbo();
                ssbo->bindBufferBase(shaderStorageBlock.binding);

            }
        }
    }

}

void traverseVariantStructure(const QVariant &variant, void (*functionPointer)(const QVariant &variant, const QString &name), const QString &name)
{

    if (variant.canConvert<QVariantMap>())
    {
        QVariantMap map = variant.value<QVariantMap>();

        for(QVariantMap::ConstIterator it = map.constBegin(); it!=map.constEnd(); ++it)
        {
            traverseVariantStructure(it.value(), functionPointer, name + (name.isEmpty()?QString():QString(".")) + it.key());
        }
    }
    else if(variant.canConvert<QVariantList>())
    {
        QVariantList list = variant.value<QVariantList>();

        for(int i=0;i<list.count();++i)
        {
            traverseVariantStructure(list[i], functionPointer, name + QString("[%1]").arg(i));
        }
    }
    else
    {
        functionPointer(variant, name);
    }

}

void printVariantInfo(const QVariant &variant, const QString &name)
{
    qDebug().nospace() << qSetFieldWidth(32) << qPrintable(name) << qSetFieldWidth(0) << ": " << variant;
}

void dumpVariantInfo(const QVariant &variant, const QString &name)
{
    traverseVariantStructure(variant, &printVariantInfo, name);
}

/*!
 * \internal
 * \brief Get the variant map for the array index
 * \param name the name containing array indices
 * \param parent the parent list
 * \return the variant map at the array index
 *
 * Traverse a variant list tree to return a variant map value at the required
 * array index. This function is called recursively. If the array structure
 * required to access the index does not exist, an empty variant map is
 * returned.
 */
QVariantMap arrayValue(const QString &name, const QVariantList &parent)
{
    // find the first array index
    QRegularExpressionMatch match = QRegularExpression("\\[([\\d]+)\\]").match(name);
    Q_ASSERT(match.hasMatch());

    // get the first array index
    int index = match.captured(1).toInt();

    // if the array index exists
    if(index < parent.count())
    {
        // if we are dealing with more than one array dimension
        if(name.count('[')>1)
        {
            // get the list for the next dimension
            QVariantList list = parent[index].toList();

            // return the array value for the next dimension
            return arrayValue(name.mid(match.capturedEnd(0)), list);
        }
        else
        {
            // return the value for this dimension
            return parent[index].toMap();
        }
    }

    // if we can't find the index, return an empty variant map
    return QVariantMap();
}

/*!
 * \internal
 * \brief Assemble the array tree for the array name
 * \param name the array name
 * \param value the value to set to the array index
 * \param parent the parent list
 * \return the parent list
 *
 * Traverse a list tree and set a value. If the required list index does not
 * exist, resize the array to include the index. This function is called
 * recursively.
 */
QVariantList &assembleArrayTree(const QString &name, const QVariant &value, QVariantList &parent)
{

    // find the first array index
    QRegularExpressionMatch match = QRegularExpression("\\[([\\d]+)\\]").match(name);
    Q_ASSERT(match.hasMatch());

    // get the first array index
    int index = match.captured(1).toInt();

    // resize the list to accommodate the index
    while((index+1)>parent.count())
    {
        parent.append(QVariant());
    }

    // if we are dealing with more than one array dimension
    if(name.count('[')>1)
    {
        // get the list for the next dimension
        QVariantList list = parent[index].toList();

        // assemble the array tree for the next dimension
        parent[index] = assembleArrayTree(name.mid(match.capturedEnd(0)), value, list);

    }
    // this is the final array dimension
    else
    {
        // set the value into the array index
        parent[index] = value;
    }

    return parent;
}

/*!
 * \internal
 * \brief Assemble the struct tree for the array name
 * \param name the struct name
 * \param value the value to set to the struct member
 * \param parent the parent map
 * \return the parent map
 *
 * Traverse a map tree and set a value.  If the required member does not exist,
 * create it. This function is called recursively.
 *
 */
QVariantMap &assembleStructTree(const QString &name, const QVariant &value, QVariantMap &parent)
{
    // find the top level struct component
    QRegularExpressionMatch match = QRegularExpression("([\\w\\d\\[\\]]+)").match(name);
    Q_ASSERT(match.hasMatch());

    // determine the component name (including array subscripts) that we are dealing with
    QString component = match.captured(1);

    // determine the sub-component name
    QString subComponent = name;
    subComponent = subComponent.remove(0, component.length());
    if(!subComponent.isEmpty()&&name.at(0)==QChar('.'))
    {
        subComponent.remove(0,1);
    }

    // determine the component base name (remove array subscripts)
    QString baseName = component;
    baseName.remove(QRegularExpression("\\[[\\d]+\\]"));

    // if there are no sub-components
    if(subComponent.isEmpty())
    {
        // if this is a multi-dimensional array
        if(component.count('[')>1)
        {
            // get the existing child list
            QVariantList childList = parent[baseName].toList();

            // remove the last array dimension index (should be 0 - we will be setting a list to it);
            QString arrayName = component.left(component.lastIndexOf('['));

            // set the value into the child list
            parent[baseName] = assembleArrayTree(arrayName, value, childList);
        }
        // This is a single dimension array, or scalar
        else
        {
            // set the value
            parent[baseName] = value;
        }
    }
    // there are remaining sub-components
    else
    {
        // if the component is an array
        if(component.count('['))
        {
            // get the existing child list
            QVariantList childList = parent[baseName].value<QVariantList>();

            // get the existing child map
            QVariantMap childMap = arrayValue(component, childList);

            // assemble the struct tree for the child map
            childMap = assembleStructTree(subComponent, value, childMap);

            // insert the child map into the list tree
            parent[baseName] = assembleArrayTree(component, childMap, childList);
        }
        // the component is a scalar
        else
        {
            // get the existing child map
            QVariantMap childMap = parent[baseName].value<QVariantMap>();

            // insert the value into the child map tree
            parent[baseName]=assembleStructTree(subComponent, value, childMap);
        }
    }

    return parent;
}

/*!
 * \internal
 * \brief Assemble introspected variable name - value pairs into an object tree
 * \param hash the variable name - value pairs
 * \return An object tree of QVariantMap and QVariantList
 *
 * The returned object tree consists of variant maps and lists which are able
 * to be converted to JavaScript objects accessible from QML.
 */
QVariantMap assembleObjects(const QVariantHash &hash)
{
    QVariantMap result;

    for(QVariantHash::ConstIterator it = hash.constBegin(); it != hash.constEnd(); ++it)
    {
        result = assembleStructTree(it.key(), it.value(), result);
    }

    return result;
}

/*!
 * \internal
 * \brief Get shader storage block variable values from a SSBO byte array
 * \param shaderStorageBlock the introspected shader storage block properties
 * \param data the byte array
 * \return a QVariantHash of variable name-value pairs
 */
QVariantMap Introspection::getShaderStorageBlockVariables(const Introspection::ShaderStorageBlock &shaderStorageBlock, const QByteArray &data)
{
    QVariantHash result;


    foreach(const Introspection::ShaderStorageBlock::Variable &variable, shaderStorageBlock.variables)
    {

        Q_ASSERT(data.count()>=shaderStorageBlock.size);

        // the top level array size, accounting for the possibility of an unsized last variable
        int topLevelArraySize = variable.topLevelArraySize==0?(data.count() - shaderStorageBlock.size + variable.topLevelArrayStride)/variable.topLevelArrayStride:variable.topLevelArraySize;

        for(int topLevel=0;topLevel<topLevelArraySize;++topLevel)
        {
            QVariant value;

            switch(variable.type)
            {
            case GL_FLOAT:
                value = getBufferData<float>(data, variable, topLevel);
                break;
            case GL_FLOAT_VEC2:
                value = getBufferData<qvec2>(data, variable, topLevel);
                break;
            case GL_FLOAT_VEC3:
                value = getBufferData<qvec3>(data, variable, topLevel);
                break;
            case GL_FLOAT_VEC4:
                value = getBufferData<qvec4>(data, variable, topLevel);
                break;
            case GL_DOUBLE:
                value = getBufferData<double>(data, variable, topLevel);
                break;
            case GL_DOUBLE_VEC2:
                value = getBufferData<qdvec2>(data, variable, topLevel);
                break;
            case GL_DOUBLE_VEC3:
                value = getBufferData<qdvec3>(data, variable, topLevel);
                break;
            case GL_DOUBLE_VEC4:
                value = getBufferData<qdvec4>(data, variable, topLevel);
                break;
            case GL_INT:
                value = getBufferData<int>(data, variable, topLevel);
                break;
            case GL_INT_VEC2:
                value = getBufferData<qivec2>(data, variable, topLevel);
                break;
            case GL_INT_VEC3:
                value = getBufferData<qivec3>(data, variable, topLevel);
                break;
            case GL_INT_VEC4:
                value = getBufferData<qivec4>(data, variable, topLevel);
                break;
            case GL_UNSIGNED_INT:
                value = getBufferData<unsigned int>(data, variable, topLevel);
                break;
            case GL_UNSIGNED_INT_VEC2:
                value = getBufferData<quvec2>(data, variable, topLevel);
                break;
            case GL_UNSIGNED_INT_VEC3:
                value = getBufferData<quvec3>(data, variable, topLevel);
                break;
            case GL_UNSIGNED_INT_VEC4:
                value = getBufferData<quvec4>(data, variable, topLevel);
                break;
            case GL_BOOL:
                value = getBufferData<bool>(data, variable, topLevel);
                break;
            case GL_BOOL_VEC2:
                value = getBufferData<qbvec2>(data, variable, topLevel);
                break;
            case GL_BOOL_VEC3:
                value = getBufferData<qbvec3>(data, variable, topLevel);
                break;
            case GL_BOOL_VEC4:
                value = getBufferData<qbvec4>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT2:
                value = getBufferData<qmat2>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT3:
                value = getBufferData<qmat3>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT4:
                value = getBufferData<qmat4>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT2x3:
                value = getBufferData<qmat2x3>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT2x4:
                value = getBufferData<qmat2x4>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT3x2:
                value = getBufferData<qmat3x2>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT3x4:
                value = getBufferData<qmat3x4>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT4x2:
                value = getBufferData<qmat4x2>(data, variable, topLevel);
                break;
            case GL_FLOAT_MAT4x3:
                value = getBufferData<qmat4x3>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT2:
                value = getBufferData<qdmat2>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT3:
                value = getBufferData<qdmat3>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT4:
                value = getBufferData<qdmat4>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT2x3:
                value = getBufferData<qdmat2x3>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT2x4:
                value = getBufferData<qdmat2x4>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT3x2:
                value = getBufferData<qdmat3x2>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT3x4:
                value = getBufferData<qdmat3x4>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT4x2:
                value = getBufferData<qdmat4x2>(data, variable, topLevel);
                break;
            case GL_DOUBLE_MAT4x3:
                value = getBufferData<qdmat4x3>(data, variable, topLevel);
                break;
            case GL_UNSIGNED_INT64_ARB:
                value = getBufferData<quint64>(data, variable, topLevel);
                break;
            case GL_INT64_ARB:
                value = getBufferData<qint64>(data, variable, topLevel);
                break;
            default:
                qWarning() << "QuickOpenGL: cannot convert" << qPrintable(shaderStorageBlock.name + "." + variable.name) << "of type" << QuickOpenGL::GLenum(variable.type);

            }

            QString name = variable.name;
            QRegularExpressionMatch match = QRegularExpression("\\[([\\d]+)\\]").match(variable.name);
            if(match.hasMatch())
            {
                name.replace(match.capturedStart(1), match.capturedLength(1),QString("%1").arg(topLevel));
            }
            result[name] = value;
        }
    }

    return assembleObjects(result);

}

Introspection::Introspection()
{

}

QDebug operator <<(QDebug debug, const Introspection::Uniform &uniform)
{
    debug.nospace() << "Uniform(name: " << uniform.name << ", location: " << uniform.location << ", type: " << QuickOpenGL::GLenum(uniform.type) << ", size: " << uniform.arraySize << ", blockIndex: "  << uniform.blockIndex << ", offset: " << uniform.offset << ", arrayStride: " << uniform.arrayStride << ", matrixStride: " << uniform.matrixStride << ", isRowMajor: "  << uniform.isRowMajor << ", atomicCounterBufferIndex: " << uniform.atomicCounterBufferIndex << ")";
    return debug.space();
}

QDebug operator <<(QDebug debug, const Introspection::UniformBlock &uniformBlock)
{
    debug.nospace() << "UniformBlock(name: " << uniformBlock.name <<", index: " << uniformBlock.index << ", binding: " << uniformBlock.binding << ", size: " << uniformBlock.size << ", uniforms:" << uniformBlock.uniforms << ")";
    return debug.space();
}

QDebug operator <<(QDebug debug, const Introspection::Attribute &attribute)
{
    debug.nospace() << "Attribute(name: " << attribute.name << ", location: " << attribute.location << ", type: " <<  QuickOpenGL::GLenum(attribute.type) << ", size: " << attribute.arraySize <<  ")";
    return debug.space();
}

QDebug operator << (QDebug debug, const Introspection::ShaderStorageBlock::Variable &variable)
{
    debug.nospace() << "Variable(name:" << qPrintable(variable.name)
                    << ", type:" << QuickOpenGL::GLenum(variable.type)<< ", arraySize: "
                    << variable.arraySize << ", offset: " << variable.offset << ", blockIndex: "<< variable.blockIndex
                    << ", arrayStride: " << variable.arrayStride << ", matrixStride: " <<  variable.matrixStride
                    << ", isRowMajor: " << variable.isRowMajor <<  ", topLevelArraySize: " << variable.topLevelArraySize
                    << ", topLevelArrayStride: " << variable.topLevelArrayStride << ")";

    return debug.space();
}

QDebug operator << (QDebug debug, const Introspection::ShaderStorageBlock &buffer)
{
    debug.nospace() << "Buffer(owner: " << buffer.programId << ", name: " << buffer.name << ", index: " << buffer.index << ", binding: " << buffer.binding << ", size: " << buffer.size << ", variables: " << buffer.variables << ")";

    return debug.space();
}

QDebug operator <<(QDebug debug, const Introspection::SubroutineUniform &subroutine)
{
    debug.nospace() << "SubroutineUniform(name: " << subroutine.name << ", location: " << subroutine.location << ", compatibleSubroutines: " << subroutine.compatibleSubroutines << ")";

    return debug.space();

}

bool Introspection::ShaderStorageBlock::operator ==(const Introspection::ShaderStorageBlock &other) const
{
    return  (size == other.size) &&
            (variables == other.variables);
}

bool Introspection::ShaderStorageBlock::operator !=(const Introspection::ShaderStorageBlock &other) const
{
    return !(*this==other);
}

bool Introspection::ShaderStorageBlock::isValid() const
{
    return !name.isEmpty();
}

bool Introspection::ShaderStorageBlock::Variable::operator ==(const Introspection::ShaderStorageBlock::Variable &other) const
{
    return (name == other.name) &&
            (type == other.type) &&
            (arraySize == other.arraySize) &&
            (offset  == other.offset) &&
            (arrayStride == other.arrayStride) &&
            (matrixStride == other.matrixStride) &&
            (isRowMajor == other.isRowMajor) &&
            (topLevelArraySize == other.topLevelArraySize) &&
            (topLevelArrayStride == other.topLevelArrayStride);
}


bool Introspection::UniformBlock::operator ==(const Introspection::UniformBlock &other) const
{
    return (size==other.size) &&
           (uniforms==other.uniforms);
}

bool Introspection::UniformBlock::operator !=(const UniformBlock &other) const
{
    return !(*this==other);
}

bool Introspection::UniformBlock::isValid() const
{
    return !name.isEmpty();
}

bool Introspection::Uniform::operator ==(const Introspection::Uniform &other) const
{
    return (name==other.name) &&
           (type==other.type) &&
           (arraySize==other.arraySize) &&
            (offset==other.offset) &&
            (arrayStride==other.arrayStride) &&
            (matrixStride==other.matrixStride) &&
            (isRowMajor==other.isRowMajor) &&
            (atomicCounterBufferIndex==other.atomicCounterBufferIndex);
}

/*!
 * \internal
 * \brief Condition a GLSL uniform's QML property
 * \param property the QML property
 * \param introspected the introspected GLSL property type
 * \param name used when calling recursively
 * \return a list of the required properties
 *
 * GLSL 430+ allows multi-dimensional uniform arrays, and structs. OpenGL will
 * return an introspected name containing [] brackets for each array dimension,
 * and xxx.member names for structs. OpenGL will return multiple uniform names
 * for different dimensions of the introspected property. As an example,
 * uniform int test[2][2] will be introspected as test[0][0] and test[1][0].
 *
 * Our QML property value (if specified correctly by the user) will contain all
 * dimensions in the one variant as a QVariantList of QVariantList and so on.
 * We pick the part of the QML variant that matches the GLSL name.
 */
QVariantList Introspection::conditionProperty(QVariant property, const QString &introspectedName, GLenum introspectedType, int introspectedArraySize, const QString &propertyType, const QQuickItem *qmlItem, QString name)
{
    if(!property.isValid())
    {
        return QVariantList();
    }

    if(name.isEmpty())
    {
        name = introspectedName;
    }

    // Get the base name (including array indices) without struct member
    QRegularExpressionMatch match = QRegularExpression(QStringLiteral("([\\w\\d\\[\\]]+)")).match(name);
    Q_ASSERT(match.hasMatch());
    QString baseName = match.captured(1);
    bool isStruct = baseName!=name;


    // Get the introspected array indices
    QList<int> indices;
    QRegularExpressionMatchIterator matchIterator = QRegularExpression(QStringLiteral("\\[([\\d]+)\\]")).globalMatch(baseName);
    while(matchIterator.hasNext())
    {
        indices << matchIterator.next().captured(1).toInt();
    }

    // if we found introspected array indices
    if(indices.count())
    {
        // if we are not dealing with an array of structs
        if(!isStruct)
        {
            // remove the last value (should be 0)
            Q_ASSERT(indices.last()==0);
            indices.removeLast();
        }

        // find property at the required uniform indices
        for(int index: indices)
        {
            if(property.canConvert<QVariantList>()&&property.value<QVariantList>().count()>index)
            {
                property = property.value<QVariantList>()[index];
            }
            else
            {
                qWarning() << "QuickOpenGL:" << static_cast<const QObject *>(qmlItem) << qPrintable(propertyType) << "property" << QRegularExpression(QStringLiteral("([\\w\\d]+)")).match(name).captured(1) << ":" << property << "dimension mismatch";
            }
        }
    }

    QVariantList list;

    // if we are dealing with a structure
    if(isStruct)
    {
        // get the struct member name
        match = QRegularExpression(QStringLiteral("\\.([\\w\\d]+)")).match(name);
        Q_ASSERT(match.hasMatch());
        QString memberName = match.captured(1);

        // if we are expecting a structure and we have a list of 1 element (QML seems to only like directly specfied javascript objects to be in [] brackets)
        if(property.canConvert<QVariantList>()&&property.value<QVariantList>().count()==1)
        {
            property = property.value<QVariantList>().first();
        }

        // if the property is a JavaScript object (can be converted to a QVariantHash)
        if(property.canConvert<QVariantHash>())
        {
            // get the variant hash
            QVariantHash hash = property.value<QVariantHash>();

            // if the JavaScript object contains our uniform member name
            if(hash.contains(memberName))
            {
                // get the member name
                property = hash[memberName];

                // get the full struct member name
                match = QRegularExpression(QStringLiteral("\\.([\\w\\d\\.\\[\\]]+)")).match(name);
                Q_ASSERT(match.hasMatch());

                // condition the uniform and property
                list = conditionProperty(property, introspectedName, introspectedType, introspectedArraySize, propertyType, qmlItem, match.captured(1));
            }
            else
            {
                qWarning() << "QuickOpenGL:" << static_cast<const QObject *>(qmlItem) << qPrintable(propertyType) << "struct mismatch in" << qPrintable(introspectedName) << "at" << qPrintable(name) << property;
            }
        }
        else
        {
            qWarning() << "QuickOpenGL:" << static_cast<const QObject *>(qmlItem) << qPrintable(propertyType) << "struct mismatch in" << qPrintable(introspectedName) << "at" << qPrintable(name) << property;
        }
    }
    else
    {
        // convert property to a list
        list = property.canConvert<QVariantList>()?property.value<QVariantList>():QVariantList({property});

        // check array size
        if(introspectedArraySize&&(introspectedArraySize!=list.count()))
        {
            qWarning() << "QuickOpenGL:" << static_cast<const QObject *>(qmlItem) << qPrintable(propertyType) << "property" << introspectedName << ":" << property << "array size mismatch, OpenGL array contains" << introspectedArraySize << "element(s), QML property contains" << list.count() << "element(s)";
        }

        // check that we can convert from the QML specified values to the required GL type
        for(int index=0;index<list.count();++index)
        {

            bool compatible = false;

            switch(introspectedType)
            {
            case GL_SAMPLER_1D:
            case GL_INT_SAMPLER_1D:
            case GL_UNSIGNED_INT_SAMPLER_1D:
            case GL_SAMPLER_2D:
            case GL_INT_SAMPLER_2D:
            case GL_UNSIGNED_INT_SAMPLER_2D:
            case GL_SAMPLER_3D:
            case GL_INT_SAMPLER_3D:
            case GL_UNSIGNED_INT_SAMPLER_3D:
            case GL_SAMPLER_CUBE:
            case GL_INT_SAMPLER_CUBE:
            case GL_UNSIGNED_INT_SAMPLER_CUBE:
            case GL_SAMPLER_1D_ARRAY:
            case GL_INT_SAMPLER_1D_ARRAY:
            case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
            case GL_SAMPLER_2D_ARRAY:
            case GL_INT_SAMPLER_2D_ARRAY:
            case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
            case GL_SAMPLER_BUFFER:
            case GL_INT_SAMPLER_BUFFER:
            case GL_UNSIGNED_INT_SAMPLER_BUFFER:

                compatible = list.at(index).canConvert<QSGTextureProvider *>()||list.at(index).canConvert<QuickSGTextureHandleProvider *>();

                // Make sure the sampler target is correct
                if(list.at(index).canConvert<QSGTextureProvider *>()&&list.at(index).value<QSGTextureProvider *>()&&list.at(index).value<QSGTextureProvider *>()->texture())
                {
                    QSGTexture *texture =  list.at(index).value<QSGTextureProvider *>()->texture();
                    GLenum target = GL_TEXTURE_2D;
                    if(texture->property("target").isValid())
                    {
                        target = texture->property("target").toUInt();
                    }

                    // check that we are binding the target to a compatible sampler
                    if(target!=QuickOpenGL::samplerToTarget(introspectedType))
                    {
                        QString name = introspectedName;
                        if(name.endsWith("[0]")&&(index>0))
                        {
                            name.truncate(name.length()-2);
                            name += QString("%1]").arg(index);
                        }

                        qWarning() << "QuickOpenGL:" << static_cast<const QObject *>(qmlItem) << qPrintable(propertyType) << "property" << name << ":" <<  QuickOpenGL::GLenum(target) << "cannot be converted to target" << QuickOpenGL::GLenum(QuickOpenGL::samplerToTarget(introspectedType));
                    }
                }

                break;
            case GL_FLOAT:
                compatible = list.at(index).canConvert<float>();
                break;
            case GL_FLOAT_VEC2:
                compatible = list.at(index).canConvert<qvec2>();
                break;
            case GL_FLOAT_VEC3:
                compatible = list.at(index).canConvert<qvec3>();
                break;
            case GL_FLOAT_VEC4:
                compatible = list.at(index).canConvert<qvec4>();
                break;
            case GL_DOUBLE:
                compatible = list.at(index).canConvert<double>();
                break;
            case GL_DOUBLE_VEC2:
                compatible = list.at(index).canConvert<qdvec2>();
                break;
            case GL_DOUBLE_VEC3:
                compatible = list.at(index).canConvert<qdvec3>();
                break;
            case GL_DOUBLE_VEC4:
                compatible = list.at(index).canConvert<qdvec4>();
                break;
            case GL_INT:
                compatible = list.at(index).canConvert<int>();
                break;
            case GL_INT_VEC2:
                compatible = list.at(index).canConvert<qivec2>();
                break;
            case GL_INT_VEC3:
                compatible = list.at(index).canConvert<qivec3>();
                break;
            case GL_INT_VEC4:
                compatible = list.at(index).canConvert<qivec4>();
                break;
            case GL_UNSIGNED_INT:
                compatible = list.at(index).canConvert<unsigned int>();
                break;
            case GL_UNSIGNED_INT_VEC2:
                compatible = list.at(index).canConvert<quvec2>();
                break;
            case GL_UNSIGNED_INT_VEC3:
                compatible = list.at(index).canConvert<quvec3>();
                break;
            case GL_UNSIGNED_INT_VEC4:
                compatible = list.at(index).canConvert<quvec4>();
                break;
            case GL_BOOL:
                compatible = list.at(index).canConvert<bool>();
                break;
            case GL_BOOL_VEC2:
                compatible = list.at(index).canConvert<qbvec2>();
                break;
            case GL_BOOL_VEC3:
                compatible = list.at(index).canConvert<qbvec3>();
                break;
            case GL_BOOL_VEC4:
                compatible = list.at(index).canConvert<qbvec4>();
                break;
            case GL_FLOAT_MAT2:
                compatible = list.at(index).canConvert<qmat2>();
                break;
            case GL_FLOAT_MAT3:
                compatible = list.at(index).canConvert<qmat3>();
                break;
            case GL_FLOAT_MAT4:
                compatible = list.at(index).canConvert<qmat4>();
                break;
            case GL_FLOAT_MAT2x3:
                compatible = list.at(index).canConvert<qmat2x3>();
                break;
            case GL_FLOAT_MAT2x4:
                compatible = list.at(index).canConvert<qmat2x4>();
                break;
            case GL_FLOAT_MAT3x2:
                compatible = list.at(index).canConvert<qmat3x2>();
                break;
            case GL_FLOAT_MAT3x4:
                compatible = list.at(index).canConvert<qmat3x4>();
                break;
            case GL_FLOAT_MAT4x2:
                compatible = list.at(index).canConvert<qmat4x2>();
                break;
            case GL_FLOAT_MAT4x3:
                compatible = list.at(index).canConvert<qmat4x3>();
                break;
            case GL_DOUBLE_MAT2:
                compatible = list.at(index).canConvert<qdmat2>();
                break;
            case GL_DOUBLE_MAT3:
                compatible = list.at(index).canConvert<qdmat3>();
                break;
            case GL_DOUBLE_MAT4:
                compatible = list.at(index).canConvert<qdmat4>();
                break;
            case GL_DOUBLE_MAT2x3:
                compatible = list.at(index).canConvert<qdmat2x3>();
                break;
            case GL_DOUBLE_MAT2x4:
                compatible = list.at(index).canConvert<qdmat2x4>();
                break;
            case GL_DOUBLE_MAT3x2:
                compatible = list.at(index).canConvert<qdmat3x2>();
                break;
            case GL_DOUBLE_MAT3x4:
                compatible = list.at(index).canConvert<qdmat3x4>();
                break;
            case GL_DOUBLE_MAT4x2:
                compatible = list.at(index).canConvert<qdmat4x2>();
                break;
            case GL_DOUBLE_MAT4x3:
                compatible = list.at(index).canConvert<qdmat4x3>();
                break;
            case GL_UNSIGNED_INT64_ARB:
                compatible = list.at(index).canConvert<quint64>();
                break;
            case GL_INT64_ARB:
                compatible = list.at(index).canConvert<qint64>();
                break;
            case GL_IMAGE_1D:
            case GL_IMAGE_2D:
            case GL_IMAGE_3D:
            case GL_IMAGE_2D_RECT:
            case GL_IMAGE_CUBE:
            case GL_IMAGE_BUFFER:
            case GL_IMAGE_1D_ARRAY:
            case GL_IMAGE_2D_ARRAY:
            case GL_IMAGE_2D_MULTISAMPLE:
            case GL_IMAGE_2D_MULTISAMPLE_ARRAY:
            case GL_INT_IMAGE_1D:
            case GL_INT_IMAGE_2D:
            case GL_INT_IMAGE_3D:
            case GL_INT_IMAGE_2D_RECT:
            case GL_INT_IMAGE_CUBE:
            case GL_INT_IMAGE_BUFFER:
            case GL_INT_IMAGE_1D_ARRAY:
            case GL_INT_IMAGE_2D_ARRAY:
            case GL_INT_IMAGE_2D_MULTISAMPLE:
            case GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
            case GL_UNSIGNED_INT_IMAGE_1D:
            case GL_UNSIGNED_INT_IMAGE_2D:
            case GL_UNSIGNED_INT_IMAGE_3D:
            case GL_UNSIGNED_INT_IMAGE_2D_RECT:
            case GL_UNSIGNED_INT_IMAGE_CUBE:
            case GL_UNSIGNED_INT_IMAGE_BUFFER:
            case GL_UNSIGNED_INT_IMAGE_1D_ARRAY:
            case GL_UNSIGNED_INT_IMAGE_2D_ARRAY:
            case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE:
            case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
                compatible = list.at(index).canConvert<QSGTextureProvider *>()||list.at(index).canConvert<QuickSGImageHandleProvider *>();
                break;

            }

            if(!compatible)
            {
                QString name = introspectedName;
                if(introspectedArraySize>1)
                {
                    name += QString("[%1]").arg(index);
                }
                qWarning().nospace() << "QuickOpenGL: " << static_cast<const QObject *>(qmlItem) << " " << qPrintable(propertyType) <<  " property " << name  << " : " << list.at(index) << " cannot be converted to " << QuickOpenGL::GLenum(introspectedType);
            }


        }
    }

    return list;

}

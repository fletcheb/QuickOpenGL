/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKSGTEXTUREPROVIDER_H
#define QUICKSGTEXTUREPROVIDER_H

#include <QSGTextureProvider>
#include <QScopedPointer>
#include "quicksgtexture.h"
#include "quicktexture.h"

class QuickSGTextureProviderPrivate;

class QuickSGTextureProvider : public QSGTextureProvider
{
    Q_OBJECT

    Q_PROPERTY(bool syncFromOpenGL READ syncFromOpenGL WRITE setSyncFromOpenGL NOTIFY syncFromOpenGLChanged)

public:

    QuickSGTextureProvider();

    ~QuickSGTextureProvider() override;

    QSGTexture *texture() const override;

    bool syncFromOpenGL() const;

public slots:

    void onAfterRendering();
    void setSyncFromOpenGL(bool syncFromOpenGL);

signals:

    void syncFromOpenGLChanged(bool syncFromOpenGL);
    void textureDataChanged(QVariant data);

protected:

    QScopedPointer<QuickSGTextureProviderPrivate> d_ptr;

private:

    Q_DECLARE_PRIVATE(QuickSGTextureProvider)
};

#endif // QUICKSGTEXTUREPROVIDER_H

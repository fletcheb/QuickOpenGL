/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKTEXTUREVIEWTEXTURE_H
#define QUICKTEXTUREVIEWTEXTURE_H

#include <QSGTexture>
#include <QOpenGLFunctions_4_3_Core>
#include "support.h"
#include "quicktextureview.h"

class QuickSGTextureViewTexture : public QSGTexture
{
    Q_OBJECT

public:

    QuickSGTextureViewTexture();

    ~QuickSGTextureViewTexture() override;


    virtual int textureId() const override;
    virtual QSize textureSize() const override;
    virtual bool hasAlphaChannel() const override;
    virtual bool hasMipmaps() const override;
    virtual void bind() override;

public slots:

    void setTarget(QuickTextureView::Target target);
    void setFormat(QuickTextureView::Format format);
    void setMinLevel(int minLevel);
    void setNumLevels(int numLevels);
    void setMinLayer(int minLayer);
    void setNumLayers(int numLayers);
    void setSourceTexture(QSGTexture * sourceTexture);

signals:

    void textureChanged();
    void targetChanged(QuickTextureView::Target m_target);
    void formatChanged(QuickTextureView::Format m_format);
    void minLevelChanged(int m_minLevel);
    void numLevelsChanged(int m_numLevels);
    void minLayerChanged(int m_minLayer);
    void numLayersChanged(int m_numLayers);
    void sourceTextureChanged(QSGTexture * sourceTexture);

protected slots:

    void update();

protected:

    void updateTexture();

    GLuint m_texture;
    QuickTextureView::Target m_target;
    QuickTextureView::Format m_format;
    GLint m_source;
    int m_minLevel;
    int m_numLevels;
    int m_minLayer;
    int m_numLayers;
    QSGTexture * m_sourceTexture;
    bool m_update;
};

#endif // QUICKTEXTUREVIEWTEXTURE_H

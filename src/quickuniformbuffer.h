/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKUNIFORMBUFFER_H
#define QUICKUNIFORMBUFFER_H

#include <QQuickItem>

class QuickSGUniformBufferProvider;

class QuickUniformBufferPrivate;

class QuickUniformBuffer : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(int bufferObject READ bufferObject NOTIFY bufferObjectChanged)

public:

    QuickUniformBuffer(QQuickItem *parent = nullptr);

    virtual ~QuickUniformBuffer() override;

    virtual QuickSGUniformBufferProvider *uniformBufferProvider();

    virtual void componentComplete() override;

    int bufferObject() const;

signals:

    void bufferObjectChanged(int bufferObject);

protected:

    QScopedPointer<QuickUniformBufferPrivate> d_ptr;

private:

    Q_DECLARE_PRIVATE(QuickUniformBuffer)

};

#endif // QUICKUNIFORMBUFFER_H

/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgtextureviewtexture.h"


QuickSGTextureViewTexture::QuickSGTextureViewTexture() :
    m_texture(0),
    m_target(QuickTextureView::Target(0)),
    m_format(QuickTextureView::Format(0)),
    m_source(0),
    m_minLevel(-1),
    m_numLevels(-1),
    m_minLayer(-1),
    m_numLayers(-1)
{
#ifndef QT_OPENGL_ES
    // Generate an OpenGL texture name
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    gl43->initializeOpenGLFunctions();
    gl43->glGenTextures(1, &m_texture);debugGL;

    connect(this, &QuickSGTextureViewTexture::targetChanged, this, &QuickSGTextureViewTexture::update);
    connect(this, &QuickSGTextureViewTexture::formatChanged, this, &QuickSGTextureViewTexture::update);
    connect(this, &QuickSGTextureViewTexture::minLevelChanged, this, &QuickSGTextureViewTexture::update);
    connect(this, &QuickSGTextureViewTexture::numLevelsChanged, this, &QuickSGTextureViewTexture::update);
    connect(this, &QuickSGTextureViewTexture::minLayerChanged, this, &QuickSGTextureViewTexture::update);
    connect(this, &QuickSGTextureViewTexture::numLayersChanged, this, &QuickSGTextureViewTexture::update);
    connect(this, &QuickSGTextureViewTexture::sourceTextureChanged, this, &QuickSGTextureViewTexture::update);
#endif
}

QuickSGTextureViewTexture::~QuickSGTextureViewTexture()
{
#ifndef QT_OPENGL_ES
    // Cleanup the OpenGL texture name
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    gl43->initializeOpenGLFunctions();
    gl43->glDeleteTextures(1, &m_texture);debugGL;
#endif
}

void QuickSGTextureViewTexture::updateTexture()
{
#ifndef QT_OPENGL_ES
    static QHash<GLenum, GLenum> bindingMap({{GL_TEXTURE_1D, GL_TEXTURE_BINDING_1D}, {GL_TEXTURE_2D, GL_TEXTURE_BINDING_2D}, {GL_TEXTURE_3D, GL_TEXTURE_BINDING_3D}, {GL_TEXTURE_1D_ARRAY, GL_TEXTURE_BINDING_1D_ARRAY}, {GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BINDING_2D_ARRAY}, {GL_TEXTURE_RECTANGLE, GL_TEXTURE_BINDING_RECTANGLE}, {GL_TEXTURE_BUFFER, GL_TEXTURE_BINDING_BUFFER}, {GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BINDING_CUBE_MAP}, {GL_TEXTURE_BINDING_CUBE_MAP_ARRAY,GL_TEXTURE_CUBE_MAP_ARRAY}, {GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_BINDING_2D_MULTISAMPLE}, {GL_TEXTURE_2D_MULTISAMPLE_ARRAY, GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY}});
    static QHash<GLenum, QList<GLenum>> targetMap({{GL_TEXTURE_1D, {GL_TEXTURE_1D, GL_TEXTURE_1D_ARRAY}},
                                                 {GL_TEXTURE_2D, {GL_TEXTURE_2D, GL_TEXTURE_2D_ARRAY}},
                                                 {GL_TEXTURE_3D, {GL_TEXTURE_3D}},
                                                 {GL_TEXTURE_CUBE_MAP, {GL_TEXTURE_CUBE_MAP, GL_TEXTURE_2D, GL_TEXTURE_2D_ARRAY, GL_TEXTURE_CUBE_MAP_ARRAY}},
                                                 {GL_TEXTURE_RECTANGLE, {GL_TEXTURE_RECTANGLE}},
                                                 {GL_TEXTURE_BUFFER, {}},
                                                 {GL_TEXTURE_1D_ARRAY, {GL_TEXTURE_1D, GL_TEXTURE_1D_ARRAY}},
                                                 {GL_TEXTURE_2D_ARRAY, {GL_TEXTURE_2D, GL_TEXTURE_2D_ARRAY}},
                                                 {GL_TEXTURE_CUBE_MAP_ARRAY, {GL_TEXTURE_CUBE_MAP, GL_TEXTURE_2D, GL_TEXTURE_2D_ARRAY, GL_TEXTURE_CUBE_MAP_ARRAY}},
                                                 {GL_TEXTURE_2D_MULTISAMPLE, {GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_2D_MULTISAMPLE_ARRAY}},
                                                 {GL_TEXTURE_2D_MULTISAMPLE_ARRAY, {GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_2D_MULTISAMPLE_ARRAY}}});



    int sourceFormat = GL_RGBA32F;
    if(m_sourceTexture->property("format").isValid())
    {
        sourceFormat =m_sourceTexture->property("format").toInt();
    }

    Q_ASSERT(QuickOpenGL::formatClass(sourceFormat)!=GL_NONE);
    Q_ASSERT(QuickOpenGL::formatClass(m_format)!=GL_NONE);

    if(QuickOpenGL::formatClass(sourceFormat)!=QuickOpenGL::formatClass(m_format))
    {
        qWarning() << "QuickOpenGL:" << property("QuickTextureView").value<QObject *>() << "attempting to create texture view on original format" << QuickOpenGL::GLenum(sourceFormat) << "with unsuported format" << QuickOpenGL::GLenum(m_format);
    }

    int sourceTarget = GL_TEXTURE_2D;
    if(m_sourceTexture->property("target").isValid())
    {
        sourceTarget =m_sourceTexture->property("target").toInt();
    }

    if(!targetMap.contains(sourceTarget)||!targetMap[sourceTarget].contains(m_target))
    {
        qWarning() << "QuickOpenGL:" << property("QuickTextureView").value<QObject *>() << "attempting to create texture view on original target" << QuickOpenGL::GLenum(sourceTarget) << "with unsuported target" << QuickOpenGL::GLenum(m_target);
    }

    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    gl43->initializeOpenGLFunctions();

    // some texture providers don't create their texture until bind is called, so call it
    GLint id = 0;
    Q_ASSERT(bindingMap.contains(sourceTarget));
    gl43->glGetIntegerv(bindingMap[sourceTarget], &id);debugGL;
    m_sourceTexture->bind();
    GLint textureImmutableFormat = 0;
    gl43->glGetTexParameteriv(sourceTarget, GL_TEXTURE_IMMUTABLE_FORMAT, &textureImmutableFormat);debugGL;
    gl43->glBindTexture(sourceTarget, id);debugGL;

    gl43->glDeleteTextures(1, &m_texture);debugGL;
    gl43->glGenTextures(1, &m_texture);debugGL;

    gl43->glTextureView(m_texture, m_target, m_sourceTexture->textureId(), m_format, m_minLevel, m_numLevels, m_minLayer, m_numLayers);debugGL;

    setProperty("format", QVariant(m_format));
    setProperty("target", QVariant(m_target));

    m_update = false;
    emit textureChanged();
#endif
}

int QuickSGTextureViewTexture::textureId() const
{
    return m_texture;
}

QSize QuickSGTextureViewTexture::textureSize() const
{
    GLint width = 0, height = 0;

#ifndef QT_OPENGL_ES
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    gl43->initializeOpenGLFunctions();

    gl43->glBindTexture(m_target, m_texture);debugGL;
    gl43->glGetTexLevelParameteriv(m_target, 0, GL_TEXTURE_WIDTH, &width);debugGL;
    gl43->glGetTexLevelParameteriv(m_target, 0, GL_TEXTURE_HEIGHT, &height);debugGL;
    gl43->glBindTexture(m_target, 0);debugGL;
#endif

    return QSize(width,height);
}

bool QuickSGTextureViewTexture::hasAlphaChannel() const
{
    bool result = false;
    switch(m_format)
    {
    case QuickTextureView::FormatRGBA8_UNorm:    // GL_RGBA8
    case QuickTextureView::FormatRGBA16_UNorm:    // GL_RGBA16
    case QuickTextureView::FormatRGBA8_SNorm:    // GL_RGBA8_SNORM
    case QuickTextureView::FormatRGBA16_SNorm:    // GL_RGBA16_SNORM
    case QuickTextureView::FormatRGBA8U:    // GL_RGBA8UI
    case QuickTextureView::FormatRGBA16U:    // GL_RGBA16UI
    case QuickTextureView::FormatRGBA32U:    // GL_RGBA32UI
    case QuickTextureView::FormatRGBA8I:    // GL_RGBA8I
    case QuickTextureView::FormatRGBA16I:    // GL_RGBA16I
    case QuickTextureView::FormatRGBA32I:    // GL_RGBA32I
    case QuickTextureView::FormatRGBA16F:    // GL_RGBA16F
    case QuickTextureView::FormatRGBA32F:    // GL_RGBA32F
    case QuickTextureView::FormatRGB5A1:    // GL_RGB5_A1
    case QuickTextureView::FormatRGBA4:    // GL_RGBA4
    case QuickTextureView::FormatRGB10A2:    // GL_RGB10_A2UI
    case QuickTextureView::FormatRGBA_DXT1:    // GL_COMPRESSED_RGBA_S3TC_DXT1_EXT
    case QuickTextureView::FormatRGBA_DXT3:    // GL_COMPRESSED_RGBA_S3TC_DXT3_EXT
    case QuickTextureView::FormatRGBA_DXT5:    // GL_COMPRESSED_RGBA_S3TC_DXT5_EXT
    case QuickTextureView::FormatSRGB8_Alpha8:    // GL_SRGB8_ALPHA8
    case QuickTextureView::FormatSRGB_Alpha_DXT1:    // GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT
    case QuickTextureView::FormatSRGB_Alpha_DXT3:    // GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT
    case QuickTextureView::FormatSRGB_Alpha_DXT5:    // GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT
    case QuickTextureView::FormatAlphaFormat:    // GL_ALPHA
    case QuickTextureView::FormatRGBAFormat:    // GL_RGBA
    case QuickTextureView::FormatLuminanceAlphaFormat:
        result =true;
        break;
    default:
        break;
    }

    return result;
}

bool QuickSGTextureViewTexture::hasMipmaps() const
{
    GLint width=0;

#ifndef QT_OPENGL_ES
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    gl43->initializeOpenGLFunctions();
    gl43->glGetTexLevelParameteriv(m_target, 1, GL_TEXTURE_WIDTH, &width);debugGL;
    gl43->glBindTexture(m_target, 0);debugGL;
#endif

    return width>0;
}

void QuickSGTextureViewTexture::bind()
{

#ifndef QT_OPENGL_ES

    if(m_update)
    {
        updateTexture();
    }

    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
    Q_ASSERT(gl43);
    gl43->initializeOpenGLFunctions();
    gl43->glBindTexture(m_target, m_texture);debugGL;

#endif
}

void QuickSGTextureViewTexture::setTarget(QuickTextureView::Target target)
{
    if (m_target == target)
        return;

    m_target = target;
    emit targetChanged(target);
}

void QuickSGTextureViewTexture::setFormat(QuickTextureView::Format format)
{
    if (m_format == format)
        return;

    m_format = format;
    emit formatChanged(m_format);
}

void QuickSGTextureViewTexture::setMinLevel(int minLevel)
{
    if (m_minLevel == minLevel)
        return;

    m_minLevel = minLevel;
    emit minLevelChanged(m_minLevel);
}

void QuickSGTextureViewTexture::setNumLevels(int numLevels)
{
    if (m_numLevels == numLevels)
        return;

    m_numLevels = numLevels;
    emit numLevelsChanged(m_numLevels);
}

void QuickSGTextureViewTexture::setMinLayer(int minLayer)
{
    if (m_minLayer == minLayer)
        return;

    m_minLayer = minLayer;
    emit minLayerChanged(m_minLayer);
}

void QuickSGTextureViewTexture::setNumLayers(int numLayers)
{
    if (m_numLayers == numLayers)
        return;

    m_numLayers = numLayers;
    emit numLayersChanged(m_numLayers);
}

void QuickSGTextureViewTexture::setSourceTexture(QSGTexture * sourceTexture)
{
    if (m_sourceTexture == sourceTexture)
        return;

    m_sourceTexture = sourceTexture;
    emit sourceTextureChanged(m_sourceTexture);
}

void QuickSGTextureViewTexture::update()
{
    m_update = true;
}

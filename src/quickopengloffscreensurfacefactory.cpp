#include "quickopengloffscreensurfacefactory.h"
#include <QThread>
#include <QGuiApplication>

QuickOpenGLOffscreenSurfaceFactory *QuickOpenGLOffscreenSurfaceFactory::instance()
{
    static QuickOpenGLOffscreenSurfaceFactory *factory = nullptr;

    if(!factory)
    {
        if(QThread::currentThread()!=qGuiApp->thread())
        {
            qDebug() << Q_FUNC_INFO << "factory must be initially created on the GUI thread";
        }
        else
        {
            factory = new QuickOpenGLOffscreenSurfaceFactory(qGuiApp);
        }
    }

    return factory;
}

QuickOpenGLOffscreenSurfaceFactory::QuickOpenGLOffscreenSurfaceFactory(QObject *parent):
    QObject(parent)
{
    connect(this, &QuickOpenGLOffscreenSurfaceFactory::createSurface, this, &QuickOpenGLOffscreenSurfaceFactory::onCreateSurface, Qt::QueuedConnection);

    onCreateSurface();
}

QuickOpenGLOffscreenSurfaceFactory::~QuickOpenGLOffscreenSurfaceFactory()
{
    qDeleteAll(surfaces);
}

QOffscreenSurface *QuickOpenGLOffscreenSurfaceFactory::getSurface()
{

    QMutexLocker locker(&mutex);
    Q_ASSERT(surfaces.count());
    QOffscreenSurface *surface = surfaces.first();
    surfaces.removeFirst();
    emit createSurface();
    return surface;
}

void QuickOpenGLOffscreenSurfaceFactory::onCreateSurface()
{
    QOffscreenSurface *surface = new QOffscreenSurface(QGuiApplication::primaryScreen());
    surface->setFormat(QSurfaceFormat::defaultFormat());
    surface->create();
    Q_ASSERT(surface->isValid());

    QMutexLocker locker(&mutex);
    surfaces.append(surface);
}

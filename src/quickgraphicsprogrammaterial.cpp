/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quickgraphicsprogrammaterial.h"
#include "quickgraphicsprogrammaterialshader.h"
#include <QCryptographicHash>
#include "support.h"

QuickGraphicsProgramMaterial::QuickGraphicsProgramMaterial()
{
}

QuickGraphicsProgramMaterial::~QuickGraphicsProgramMaterial()
{

}

/*!
 * \internal
 * \brief The material type
 * \return unique material type
 *
 * We only require a single QuickGraphicsProgramMaterialShader to service
 * multiple GraphicsPrograms that have the same OpenGL shader source code. The
 * scene graph caters for this by only creating one QSGMaterialShader for each
 * QSGMaterialType. To make this work in our case, we need to define a
 * QSGMaterialType based on the shader source code.  We achieve this through
 * hashing the combined shader stage source code and casting the hash to a
 * QSGMaterialType.
 *
 * The MD5 hash is 128bits long and the QSGMaterialType pointer is 64bits.  We
 * only use the first 8bytes of the MD5 hash.
 *
 */
QSGMaterialType *QuickGraphicsProgramMaterial::type() const
{
    if(identifier.isEmpty())
    {

        Q_ASSERT(properties.contains("vertexShader"));
        Q_ASSERT(properties.contains("fragmentShader"));
        Q_ASSERT(properties.contains("geometryShader"));
        Q_ASSERT(properties.contains("tessellationControlShader"));
        Q_ASSERT(properties.contains("tessellationEvaluationShader"));

        // Add all our shader source into one byte array
        QByteArray sourceCode = properties["vertexShader"].toByteArray() +
                                properties["fragmentShader"].toByteArray() +
                                properties["geometryShader"].toByteArray() +
                                properties["tessellationControlShader"].toByteArray() +
                                properties["tessellationEvaluationShader"].toByteArray();

        // determine the MD5 hash (128 bit value)
        QCryptographicHash hash(QCryptographicHash::Md5);
        hash.addData(sourceCode);

        // store the result
        const_cast<QuickGraphicsProgramMaterial *>(this)->identifier = hash.result();

    }

    // return the unique identifier for our shader source code (64 bits)
    return *(QSGMaterialType **)identifier.constData();
}

QSGMaterialShader *QuickGraphicsProgramMaterial::createShader() const
{

    debugGL;

    // Create our material shader
    QuickGraphicsProgramMaterialShader *shader = new QuickGraphicsProgramMaterialShader;
    Q_CHECK_PTR(shader);

    Q_ASSERT(properties.contains("vertexShader"));
    Q_ASSERT(properties.contains("fragmentShader"));
    Q_ASSERT(properties.contains("geometryShader"));
    Q_ASSERT(properties.contains("tessellationControlShader"));
    Q_ASSERT(properties.contains("tessellationEvaluationShader"));
    Q_ASSERT(properties.contains("QmlItem"));
    Q_ASSERT(properties.contains("QQuickWindow"));

    // set the material shader GLSL source code for each shader stage
    shader->setVertexShader(properties["vertexShader"].toByteArray());
    shader->setFragmentShader(properties["fragmentShader"].toByteArray());
    shader->setGeometryShader(properties["geometryShader"].toByteArray());
    shader->setTessellationControlShader(properties["tessellationControlShader"].toByteArray());
    shader->setTessellationEvaluationShader(properties["tessellationEvaluationShader"].toByteArray());
    shader->setQmlItem(properties["QmlItem"].value<QObject *>());
    shader->setQQuickWindow(qobject_cast<QQuickWindow *>(properties["QQuickWindow"].value<QObject *>()));

    // set the material shader attributes
    shader->setAttributeNames(attributes);

    return shader;
}


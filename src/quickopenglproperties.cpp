/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quickopenglproperties.h"
#include <QOpenGLShader>
#include <QOpenGLExtraFunctions>
#include <QQuickWindow>
#include "quickcomputeprogram.h"
#include "quickgraphicsprogram.h"
#include "support.h"
#include <QOpenGLContext>
#include <QOffscreenSurface>
#include <QGuiApplication>

/*!
 * \qmltype OpenGLProperties
 * \brief The OpenGLProperties QML type reports the implementation's OpenGL properties.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickOpenGLProperties
 * \ingroup QuickOpenGL QML Attached Properties
 * 
 *
 */

class QuickOpenGLPropertiesPrivate : public QObject
{
    Q_OBJECT

public:

    QuickOpenGLPropertiesPrivate(QuickOpenGLProperties *q);

    QStringList extensions;
    QString vendor;
    QVector<float> glLineWidthRange;
    int glMaxComputeShaderStorageBlocks;
    int glMaxCombinedShaderStorageBlocks;
    int glMaxComputeUniformBlocks;
    int glMaxComputeTextureImageUnits;
    int glMaxComputeUniformComponents;
    int glMaxComputeAtomicCounters;
    int glMaxComputeAtomicCounterBuffers;
    int glMaxCombinedComputeUniformComponents;
    int glMaxComputeWorkGroupInvocations;
    qivec3 glMaxComputeWorkGroupCount;
    qivec3 glMaxComputeWorkGroupSize;
    int glMaxDebugGroupStackDepth;
    int glMajorVersion;
    int glMax3DTextureSize;
    int glMaxArrayTextureLayers;
    int glMaxClipDistances;
    int glMaxColorTextureSamples;
    int glMaxCombinedAtomicCounters;
    int glMaxCombinedFragmentUniformComponents;
    int glMaxCombinedGeometryUniformComponents;
    int glMaxCombinedTextureImageUnits;
    int glMaxCombinedUniformBlocks;
    int glMaxCombinedVeretexUniformComponents;
    int glMaxCubeMapTextureSize;
    int glMaxDepthTextureSamples;
    int glMaxDrawBuffers;
    int glMaxDualSourceDrawBuffers;
    int glMaxElementsIndices;
    int glMaxElementsVertices;
    int glMaxFragmentAtomicCounters;
    int glMaxFragmentShaderStorageBlocks;
    int glMaxFragmentInputComponents;
    int glMaxFragmentUniformComponents;
    int glMaxFragmentUniformVectors;
    int glMaxFragmentUniformBlocks;
    int glMaxFramebufferWidth;
    int glMaxFramebufferHeight;
    int glMaxFramebufferLayers;
    int glMaxFramebufferSamples;
    int glMaxGeometryAtomicCounters;
    int glMaxGeometryShaderStorageBlocks;
    int glMaxGeometryInputComponents;
    int glMaxGeometryOutputComponents;
    int glMaxGeometryTextureImageUnits;
    int glMaxGeometryUniformBlocks;
    int glMaxGeometryUniformComponents;
    int glMaxIntegerSamples;
    int glMaxLabelLength;
    int glMaxProgramTexelOffset;
    int glMinProgramTexelOffset;
    int glMaxRectangleTextureSize;
    int glMaxRenderbufferSize;
    int glMaxSampleMaskWords;
    int glMaxServerWaitTimeout;
    int glMaxShaderStorageBufferBindings;
    int glMaxTessControlAtomicCounters;
    int glMaxTessEvaluationAtomicCounters;
    int glMaxTessControlShaderStorageBlocks;
    int glMaxTessEvaluationShaderStorageBlocks;
    int glMaxTextureBufferSize;
    int glMaxTextureImageUnits;
    float glMaxTextureLodBias;
    int glMaxTextureSize;
    int glMaxUniformBufferBindings;
    int glMaxUniformBlockSize;
    int glMaxUniformLocations;
    int glMaxVaryingComponents;
    int glMaxVaryingVectors;
    int glMaxVaryingFloats;
    int glMaxVertexAtomicCounters;
    int glMaxVertexAttribs;
    int glMaxVertexShaderStorageBlocks;
    int glMaxVertexTextureImageUnits;
    int glMaxVertexUniformComponents;
    int glMaxVertexUniformVectors;
    int glMaxVertexOutputComponents;
    int glMaxVertexUniformBlocks;
    QSize glMaxViewportDims;
    int glMaxViewports;
    int glMinorVersion;
    float glPointSizeGranularity;
    QVector<float> glPointSizeRange;
    QVector<float> glSmoothPointSizeRange;
    QVector<float> glAliasedPointSizeRange;
    QVector<float> glSmoothLineWidthRange;
    float glSmoothLineWidthGranularity;
    QVector<float> glAliasedLineWidthRange;
    int glStereo;
    int glSubPixelBits;
    int glMaxVertexAttribRelativeOffset;
    int glMaxVertexAttribBindings;
    QVector<int> glViewportBoundsRange;
    int glViewportSubpixelBits;
    int glMaxElementIndex;
    int glMaxTessGenLevel;
    int glMaxPatchVertices;
    qivec3 glMaxComputeVariableGroupSizeArb;
    int glMaxComputeVariableGroupInvocationsArb;
signals:

    void propertiesChanged();

public slots:

    void updateProperties();

    void notifyPropertiesChanged();

protected:

    QuickOpenGLProperties *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickOpenGLProperties)
};

QuickOpenGLPropertiesPrivate::QuickOpenGLPropertiesPrivate(QuickOpenGLProperties *q) :    
    glLineWidthRange(2, 1.0),
    glMaxComputeShaderStorageBlocks(0),
    glMaxCombinedShaderStorageBlocks(0),
    glMaxComputeUniformBlocks(0),
    glMaxComputeTextureImageUnits(0),
    glMaxComputeUniformComponents(0),
    glMaxComputeAtomicCounters(0),
    glMaxComputeAtomicCounterBuffers(0),
    glMaxCombinedComputeUniformComponents(0),
    glMaxComputeWorkGroupInvocations(0),
    glMaxComputeWorkGroupCount(0),
    glMaxComputeWorkGroupSize(0),
    glMaxDebugGroupStackDepth(0),
    glMajorVersion(0),
    glMax3DTextureSize(64),
    glMaxArrayTextureLayers(256),
    glMaxClipDistances(0),
    glMaxColorTextureSamples(0),
    glMaxCombinedAtomicCounters(0),
    glMaxCombinedFragmentUniformComponents(0),
    glMaxCombinedGeometryUniformComponents(0),
    glMaxCombinedTextureImageUnits(0),
    glMaxCombinedUniformBlocks(0),
    glMaxCombinedVeretexUniformComponents(0),
    glMaxCubeMapTextureSize(0),
    glMaxDepthTextureSamples(0),
    glMaxDrawBuffers(0),
    glMaxDualSourceDrawBuffers(0),
    glMaxElementsIndices(0),
    glMaxElementsVertices(0),
    glMaxFragmentAtomicCounters(0),
    glMaxFragmentShaderStorageBlocks(0),
    glMaxFragmentInputComponents(0),
    glMaxFragmentUniformComponents(0),
    glMaxFragmentUniformVectors(0),
    glMaxFragmentUniformBlocks(0),
    glMaxFramebufferWidth(0),
    glMaxFramebufferHeight(0),
    glMaxFramebufferLayers(0),
    glMaxFramebufferSamples(0),
    glMaxGeometryAtomicCounters(0),
    glMaxGeometryShaderStorageBlocks(0),
    glMaxGeometryInputComponents(0),
    glMaxGeometryOutputComponents(0),
    glMaxGeometryTextureImageUnits(0),
    glMaxGeometryUniformBlocks(0),
    glMaxGeometryUniformComponents(0),
    glMaxIntegerSamples(0),
    glMaxLabelLength(0),
    glMaxProgramTexelOffset(0),
    glMinProgramTexelOffset(0),
    glMaxRectangleTextureSize(0),
    glMaxRenderbufferSize(0),
    glMaxSampleMaskWords(0),
    glMaxServerWaitTimeout(0),
    glMaxShaderStorageBufferBindings(0),
    glMaxTessControlAtomicCounters(0),
    glMaxTessEvaluationAtomicCounters(0),
    glMaxTessControlShaderStorageBlocks(0),
    glMaxTessEvaluationShaderStorageBlocks(0),
    glMaxTextureBufferSize(0),
    glMaxTextureImageUnits(0),
    glMaxTextureLodBias(2.0),
    glMaxTextureSize(0),
    glMaxUniformBufferBindings(0),
    glMaxUniformBlockSize(0),
    glMaxUniformLocations(0),
    glMaxVaryingComponents(0),
    glMaxVaryingVectors(0),
    glMaxVaryingFloats(0),
    glMaxVertexAtomicCounters(0),
    glMaxVertexAttribs(16), // Must be at least 16
    glMaxVertexShaderStorageBlocks(0),
    glMaxVertexTextureImageUnits(0),
    glMaxVertexUniformComponents(0),
    glMaxVertexUniformVectors(0),
    glMaxVertexOutputComponents(0),
    glMaxVertexUniformBlocks(0),
    glMaxViewportDims(0,0),
    glMaxViewports(0),
    glMinorVersion(0),
    glPointSizeGranularity(0),
    glPointSizeRange(2, 0.0),
    glSmoothPointSizeRange(2, 1.0),
    glAliasedPointSizeRange(2, 1.0),
    glSmoothLineWidthRange(2, 1.0),
    glSmoothLineWidthGranularity(0),
    glAliasedLineWidthRange(2, 1.0),
    glStereo(0),
    glSubPixelBits(0),
    glMaxVertexAttribRelativeOffset(0),
    glMaxVertexAttribBindings(0),
    glViewportBoundsRange(2,0),
    glViewportSubpixelBits(0),
    glMaxElementIndex(0),
    glMaxTessGenLevel(0),
    glMaxPatchVertices(0),
    glMaxComputeVariableGroupInvocationsArb(0),
    q_ptr(q)
{

    Q_ASSERT(QThread::currentThread()==qGuiApp->thread());

    // create a context on an offscreen surface
    QOpenGLContext context;
    context.create();
    QOffscreenSurface surface(QGuiApplication::primaryScreen());
    surface.setFormat(QSurfaceFormat::defaultFormat());
    surface.create();
    context.makeCurrent(&surface);

    // get the properties
    updateProperties();

    // notify property changes
    notifyPropertiesChanged();

    context.doneCurrent();
}

QStringList toStringList(const QList<QByteArray> &list)
{
    QStringList result;
    for(const QByteArray &item : list)
    {
        result.append(QString(item));
    }
    return result;
}
void QuickOpenGLPropertiesPrivate::updateProperties()
{

    if(QOpenGLContext::currentContext())
    {
        QOpenGLExtraFunctions *gl33 = QOpenGLContext::currentContext()->extraFunctions();

        if(gl33)
        {
            gl33->initializeOpenGLFunctions();

            extensions = toStringList(QOpenGLContext::currentContext()->extensions().values());
            vendor = QString::fromLocal8Bit(reinterpret_cast<const char *>(gl33->glGetString(GL_VENDOR)));debugGLquiet;
            gl33->glGetFloatv(GL_LINE_WIDTH_RANGE, &glLineWidthRange[0]);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_ELEMENTS_INDICES, &glMaxElementsIndices);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS, &glMaxComputeShaderStorageBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS, &glMaxCombinedShaderStorageBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMPUTE_UNIFORM_BLOCKS, &glMaxComputeUniformBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS, &glMaxComputeTextureImageUnits);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMPUTE_UNIFORM_COMPONENTS, &glMaxComputeUniformComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMPUTE_ATOMIC_COUNTERS, &glMaxComputeAtomicCounters);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS, &glMaxComputeAtomicCounterBuffers);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS, &glMaxCombinedComputeUniformComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &glMaxComputeWorkGroupInvocations);debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &glMaxComputeWorkGroupCount(0,0));debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &glMaxComputeWorkGroupCount(1,0));debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &glMaxComputeWorkGroupCount(2,0));debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &glMaxComputeWorkGroupSize(0,0));debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &glMaxComputeWorkGroupSize(1,0));debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &glMaxComputeWorkGroupSize(2,0));debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_DEBUG_GROUP_STACK_DEPTH, &glMaxDebugGroupStackDepth);debugGLquiet;
            gl33->glGetIntegerv(GL_MAJOR_VERSION, &glMajorVersion);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &glMax3DTextureSize);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &glMaxArrayTextureLayers);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_CLIP_DISTANCES, &glMaxClipDistances);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COLOR_TEXTURE_SAMPLES, &glMaxColorTextureSamples);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMBINED_ATOMIC_COUNTERS, &glMaxCombinedAtomicCounters);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS, &glMaxCombinedFragmentUniformComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS, &glMaxCombinedGeometryUniformComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &glMaxCombinedTextureImageUnits);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMBINED_UNIFORM_BLOCKS, &glMaxCombinedUniformBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS, &glMaxCombinedVeretexUniformComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE, &glMaxCubeMapTextureSize);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_DEPTH_TEXTURE_SAMPLES, &glMaxDepthTextureSamples);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_DRAW_BUFFERS, &glMaxDrawBuffers);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_DUAL_SOURCE_DRAW_BUFFERS, &glMaxDualSourceDrawBuffers);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_ELEMENTS_INDICES, &glMaxElementsIndices);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_ELEMENTS_VERTICES, &glMaxElementsVertices);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAGMENT_ATOMIC_COUNTERS, &glMaxFragmentAtomicCounters);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS, &glMaxFragmentShaderStorageBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAGMENT_INPUT_COMPONENTS, &glMaxFragmentInputComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &glMaxFragmentUniformComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_VECTORS, &glMaxFragmentUniformVectors);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_BLOCKS, &glMaxFragmentUniformBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAMEBUFFER_WIDTH, &glMaxFramebufferWidth);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAMEBUFFER_HEIGHT, &glMaxFramebufferHeight);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAMEBUFFER_LAYERS, &glMaxFramebufferLayers);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_FRAMEBUFFER_SAMPLES, &glMaxFramebufferSamples);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_GEOMETRY_ATOMIC_COUNTERS, &glMaxGeometryAtomicCounters);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS, &glMaxGeometryShaderStorageBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_GEOMETRY_INPUT_COMPONENTS, &glMaxGeometryInputComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_COMPONENTS, &glMaxGeometryOutputComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS, &glMaxGeometryTextureImageUnits);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_BLOCKS, &glMaxGeometryUniformBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_COMPONENTS, &glMaxGeometryUniformComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_INTEGER_SAMPLES, &glMaxIntegerSamples);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_LABEL_LENGTH, &glMaxLabelLength);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_PROGRAM_TEXEL_OFFSET, &glMaxProgramTexelOffset);debugGLquiet;
            gl33->glGetIntegerv(GL_MIN_PROGRAM_TEXEL_OFFSET, &glMinProgramTexelOffset);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE, &glMaxRectangleTextureSize);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &glMaxRenderbufferSize);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_SAMPLE_MASK_WORDS, &glMaxSampleMaskWords);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_SERVER_WAIT_TIMEOUT, &glMaxServerWaitTimeout);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS, &glMaxShaderStorageBufferBindings);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS, &glMaxTessControlAtomicCounters);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS, &glMaxTessEvaluationAtomicCounters);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS, &glMaxTessControlShaderStorageBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS, &glMaxTessEvaluationShaderStorageBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &glMaxTextureBufferSize);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &glMaxTextureImageUnits);debugGLquiet;
            gl33->glGetFloatv(GL_MAX_TEXTURE_LOD_BIAS, &glMaxTextureLodBias);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_TEXTURE_SIZE, &glMaxTextureSize);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &glMaxUniformBufferBindings);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &glMaxUniformBlockSize);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_UNIFORM_LOCATIONS, &glMaxUniformLocations);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VARYING_COMPONENTS, &glMaxVaryingComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VARYING_VECTORS, &glMaxVaryingVectors);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VARYING_FLOATS, &glMaxVaryingFloats);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_ATOMIC_COUNTERS, &glMaxVertexAtomicCounters);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &glMaxVertexAttribs);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS, &glMaxVertexShaderStorageBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &glMaxVertexTextureImageUnits);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &glMaxVertexUniformComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_UNIFORM_VECTORS, &glMaxVertexUniformVectors);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_OUTPUT_COMPONENTS, &glMaxVertexOutputComponents);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_UNIFORM_BLOCKS, &glMaxVertexUniformBlocks);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VIEWPORT_DIMS, reinterpret_cast<int *>(&glMaxViewportDims));debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VIEWPORTS, &glMaxViewports);debugGLquiet;
            gl33->glGetIntegerv(GL_MINOR_VERSION, &glMinorVersion);debugGLquiet;
            gl33->glGetFloatv(GL_POINT_SIZE_GRANULARITY, &glPointSizeGranularity);debugGLquiet;
            gl33->glGetFloatv(GL_POINT_SIZE_RANGE, glPointSizeRange.data());debugGLquiet;
            gl33->glGetFloatv(GL_SMOOTH_POINT_SIZE_RANGE, glSmoothPointSizeRange.data());debugGLquiet;
            gl33->glGetFloatv(GL_ALIASED_POINT_SIZE_RANGE, glAliasedPointSizeRange.data());debugGLquiet;
            gl33->glGetFloatv(GL_SMOOTH_LINE_WIDTH_RANGE, glSmoothLineWidthRange.data());debugGLquiet;
            gl33->glGetFloatv(GL_SMOOTH_LINE_WIDTH_GRANULARITY, &glSmoothLineWidthGranularity);debugGLquiet;
            gl33->glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, glAliasedLineWidthRange.data());debugGLquiet;
            gl33->glGetIntegerv(GL_STEREO, &glStereo);debugGLquiet;
            gl33->glGetIntegerv(GL_SUBPIXEL_BITS, &glSubPixelBits);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET, &glMaxVertexAttribRelativeOffset);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_VERTEX_ATTRIB_BINDINGS, &glMaxVertexAttribBindings);debugGLquiet;
            gl33->glGetIntegerv(GL_VIEWPORT_BOUNDS_RANGE, glViewportBoundsRange.data());debugGLquiet;
            gl33->glGetIntegerv(GL_VIEWPORT_SUBPIXEL_BITS, &glViewportSubpixelBits);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_ELEMENT_INDEX, &glMaxElementIndex);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_TESS_GEN_LEVEL, &glMaxTessGenLevel);debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_PATCH_VERTICES, &glMaxPatchVertices);debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_VARIABLE_GROUP_SIZE_ARB, 0, &glMaxComputeVariableGroupSizeArb(0,0));debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_VARIABLE_GROUP_SIZE_ARB, 1, &glMaxComputeVariableGroupSizeArb(1,0));debugGLquiet;
            gl33->glGetIntegeri_v(GL_MAX_COMPUTE_VARIABLE_GROUP_SIZE_ARB, 2, &glMaxComputeVariableGroupSizeArb(2,0));debugGLquiet;
            gl33->glGetIntegerv(GL_MAX_COMPUTE_VARIABLE_GROUP_INVOCATIONS_ARB, &glMaxComputeVariableGroupInvocationsArb);debugGLquiet;
        }
    }
}


void QuickOpenGLPropertiesPrivate::notifyPropertiesChanged()
{
    Q_Q(QuickOpenGLProperties);

    emit q->extensionsChanged(extensions);
    emit q->vendorChanged(vendor);
    emit q->glLineWidthRangeChanged(glLineWidthRange);
    emit q->glMaxComputeShaderStorageBlocksChanged(glMaxComputeShaderStorageBlocks);
    emit q->glMaxCombinedShaderStorageBlocksChanged(glMaxCombinedShaderStorageBlocks);
    emit q->glMaxComputeUniformBlocksChanged(glMaxComputeUniformBlocks);
    emit q->glMaxComputeTextureImageUnitsChanged(glMaxComputeTextureImageUnits);
    emit q->glMaxComputeUniformComponentsChanged(glMaxComputeUniformComponents);
    emit q->glMaxComputeAtomicCountersChanged(glMaxComputeAtomicCounters);
    emit q->glMaxComputeAtomicCounterBuffersChanged(glMaxComputeAtomicCounterBuffers);
    emit q->glMaxCombinedComputeUniformComponentsChanged(glMaxCombinedComputeUniformComponents);
    emit q->glMaxComputeWorkGroupInvocationsChanged(glMaxComputeWorkGroupInvocations);
    emit q->glMaxComputeWorkGroupCountChanged(glMaxComputeWorkGroupCount);
    emit q->glMaxComputeWorkGroupSizeChanged(glMaxComputeWorkGroupSize);
    emit q->glMaxDebugGroupStackDepthChanged(glMaxDebugGroupStackDepth);
    emit q->glMajorVersionChanged(glMajorVersion);
    emit q->glMax3DTextureSizeChanged(glMax3DTextureSize);
    emit q->glMaxArrayTextureLayersChanged(glMaxArrayTextureLayers);
    emit q->glMaxClipDistancesChanged(glMaxClipDistances);
    emit q->glMaxColorTextureSamplesChanged(glMaxColorTextureSamples);
    emit q->glMaxCombinedAtomicCountersChanged(glMaxCombinedAtomicCounters);
    emit q->glMaxCombinedFragmentUniformComponentsChanged(glMaxCombinedFragmentUniformComponents);
    emit q->glMaxCombinedGeometryUniformComponentsChanged(glMaxCombinedGeometryUniformComponents);
    emit q->glMaxCombinedTextureImageUnitsChanged(glMaxComputeTextureImageUnits);
    emit q->glMaxCombinedUniformBlocksChanged(glMaxCombinedUniformBlocks);
    emit q->glMaxCombinedVeretexUniformComponentsChanged(glMaxCombinedVeretexUniformComponents);
    emit q->glMaxCubeMapTextureSizeChanged(glMaxCubeMapTextureSize);
    emit q->glMaxDepthTextureSamplesChanged(glMaxDepthTextureSamples);
    emit q->glMaxDrawBuffersChanged(glMaxDrawBuffers);
    emit q->glMaxDualSourceDrawBuffersChanged(glMaxDualSourceDrawBuffers);
    emit q->glMaxElementsIndicesChanged(glMaxElementsIndices);
    emit q->glMaxElementsVerticesChanged(glMaxElementsVertices);
    emit q->glMaxFragmentAtomicCountersChanged(glMaxFragmentAtomicCounters);
    emit q->glMaxFragmentShaderStorageBlocksChanged(glMaxFragmentShaderStorageBlocks);
    emit q->glMaxFragmentInputComponentsChanged(glMaxFragmentInputComponents);
    emit q->glMaxFragmentUniformComponentsChanged(glMaxFragmentUniformComponents);
    emit q->glMaxFragmentUniformVectorsChanged(glMaxFragmentUniformVectors);
    emit q->glMaxFragmentUniformBlocksChanged(glMaxFragmentUniformBlocks);
    emit q->glMaxFramebufferWidthChanged(glMaxFramebufferWidth);
    emit q->glMaxFramebufferHeightChanged(glMaxFramebufferHeight);
    emit q->glMaxFramebufferLayersChanged(glMaxFramebufferLayers);
    emit q->glMaxFramebufferSamplesChanged(glMaxFramebufferSamples);
    emit q->glMaxGeometryAtomicCountersChanged(glMaxGeometryAtomicCounters);
    emit q->glMaxGeometryShaderStorageBlocksChanged(glMaxGeometryShaderStorageBlocks);
    emit q->glMaxGeometryInputComponentsChanged(glMaxGeometryInputComponents);
    emit q->glMaxGeometryOutputComponentsChanged(glMaxGeometryOutputComponents);
    emit q->glMaxGeometryTextureImageUnitsChanged(glMaxGeometryTextureImageUnits);
    emit q->glMaxGeometryUniformBlocksChanged(glMaxGeometryUniformBlocks);
    emit q->glMaxGeometryUniformComponentsChanged(glMaxGeometryUniformComponents);
    emit q->glMaxIntegerSamplesChanged(glMaxIntegerSamples);
    emit q->glMaxLabelLengthChanged(glMaxLabelLength);
    emit q->glMaxProgramTexelOffsetChanged(glMaxProgramTexelOffset);
    emit q->glMinProgramTexelOffsetChanged(glMinProgramTexelOffset);
    emit q->glMaxRectangleTextureSizeChanged(glMaxRectangleTextureSize);
    emit q->glMaxRenderbufferSizeChanged(glMaxRenderbufferSize);
    emit q->glMaxSampleMaskWordsChanged(glMaxSampleMaskWords);
    emit q->glMaxServerWaitTimeoutChanged(glMaxServerWaitTimeout);
    emit q->glMaxShaderStorageBufferBindingsChanged(glMaxShaderStorageBufferBindings);
    emit q->glMaxTessControlAtomicCountersChanged(glMaxTessControlAtomicCounters);
    emit q->glMaxTessEvaluationAtomicCountersChanged(glMaxTessEvaluationAtomicCounters);
    emit q->glMaxTessControlShaderStorageBlocksChanged(glMaxTessControlShaderStorageBlocks);
    emit q->glMaxTessEvaluationShaderStorageBlocksChanged(glMaxTessEvaluationShaderStorageBlocks);
    emit q->glMaxTextureBufferSizeChanged(glMaxTextureBufferSize);
    emit q->glMaxTextureImageUnitsChanged(glMaxTextureImageUnits);
    emit q->glMaxTextureLodBiasChanged(glMaxTextureLodBias);
    emit q->glMaxTextureSizeChanged(glMaxTextureSize);
    emit q->glMaxUniformBufferBindingsChanged(glMaxUniformBufferBindings);
    emit q->glMaxUniformBlockSizeChanged(glMaxUniformBlockSize);
    emit q->glMaxUniformLocationsChanged(glMaxUniformLocations);
    emit q->glMaxVaryingComponentsChanged(glMaxVaryingComponents);
    emit q->glMaxVaryingVectorsChanged(glMaxVaryingVectors);
    emit q->glMaxVaryingFloatsChanged(glMaxVaryingFloats);
    emit q->glMaxVertexAtomicCountersChanged(glMaxVertexAtomicCounters);
    emit q->glMaxVertexAttribsChanged(glMaxVertexAttribs);
    emit q->glMaxVertexShaderStorageBlocksChanged(glMaxVertexShaderStorageBlocks);
    emit q->glMaxVertexTextureImageUnitsChanged(glMaxVertexTextureImageUnits);
    emit q->glMaxVertexUniformComponentsChanged(glMaxVertexUniformComponents);
    emit q->glMaxVertexUniformVectorsChanged(glMaxVertexUniformVectors);
    emit q->glMaxVertexOutputComponentsChanged(glMaxVertexOutputComponents);
    emit q->glMaxVertexUniformBlocksChanged(glMaxVertexUniformBlocks);
    emit q->glMaxViewportDimsChanged(glMaxViewportDims);
    emit q->glMaxViewportsChanged(glMaxViewports);
    emit q->glMinorVersionChanged(glMinorVersion);
    emit q->glPointSizeGranularityChanged(glPointSizeGranularity);
    emit q->glSmoothPointSizeRangeChanged(glSmoothPointSizeRange);
    emit q->glPointSizeRangeChanged(glPointSizeRange);
    emit q->glAliasedPointSizeRangeChanged(glAliasedPointSizeRange);
    emit q->glSmoothLineWidthRangeChanged(glSmoothLineWidthRange);
    emit q->glSmoothLineWidthGranularityChanged(glSmoothLineWidthGranularity);
    emit q->glAliasedLineWidthRangeChanged(glAliasedLineWidthRange);
    emit q->glStereoChanged(glStereo);
    emit q->glSubPixelBitsChanged(glSubPixelBits);
    emit q->glMaxVertexAttribRelativeOffsetChanged(glMaxVertexAttribRelativeOffset);
    emit q->glMaxVertexAttribBindingsChanged(glMaxVertexAttribBindings);
    emit q->glViewportBoundsRangeChanged(glViewportBoundsRange);
    emit q->glViewportSubpixelBitsChanged(glViewportSubpixelBits);
    emit q->glMaxElementIndexChanged(glMaxElementIndex);
    emit q->glMaxTessGenLevelChanged(glMaxTessGenLevel);
    emit q->glMaxPatchVerticesChanged(glMaxPatchVertices);
    emit q->glMaxComputeVariableGroupSizeArbChanged(glMaxComputeVariableGroupSizeArb);
    emit q->glMaxComputeVariableGroupInvocationsArbChanged(glMaxComputeVariableGroupInvocationsArb);
}



QuickOpenGLProperties::QuickOpenGLProperties(QQuickItem *parent) :
    QObject(parent),
    d_ptr(new QuickOpenGLPropertiesPrivate(this))
{    
}

QuickOpenGLProperties::~QuickOpenGLProperties()
{
}

QuickOpenGLProperties *QuickOpenGLProperties::qmlAttachedProperties(QObject *object)
{
    if (qobject_cast<QQuickItem *>(object))
    {
        return new QuickOpenGLProperties(qobject_cast<QQuickItem *>(object));
    }
    else if(qobject_cast<QQuickWindow *>(object))
    {
        return new QuickOpenGLProperties(qobject_cast<QQuickWindow*>(object)->contentItem());
    }

    return nullptr;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxElementsIndices
 *
 * This property holds the OpenGL implementation's maximum allowed number of
 * vertex indices.
 */
int QuickOpenGLProperties::glMaxElementsIndices() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxElementsIndices;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeShaderStorageBlocks
 *
 * This property holds the OpenGL implementation's maximum allowed number of
 * shader storage blocks for a compute shader.
 */
int QuickOpenGLProperties::glMaxComputeShaderStorageBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeShaderStorageBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCombinedShaderStorageBlocks
 */
int QuickOpenGLProperties::glMaxCombinedShaderStorageBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCombinedShaderStorageBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeUniformBlocks
 */
int QuickOpenGLProperties::glMaxComputeUniformBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeUniformBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeTextureImageUnits
 */
int QuickOpenGLProperties::glMaxComputeTextureImageUnits() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeTextureImageUnits;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeUniformComponents
 */
int QuickOpenGLProperties::glMaxComputeUniformComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeUniformComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeAtomicCounters
 */
int QuickOpenGLProperties::glMaxComputeAtomicCounters() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeAtomicCounters;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeAtomicCounterBuffers
 */
int QuickOpenGLProperties::glMaxComputeAtomicCounterBuffers() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeAtomicCounterBuffers;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCombinedComputeUniformComponents
 */
int QuickOpenGLProperties::glMaxCombinedComputeUniformComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCombinedComputeUniformComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeWorkGroupInvocations
 */
int QuickOpenGLProperties::glMaxComputeWorkGroupInvocations() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeWorkGroupInvocations;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeWorkGroupCount
 */
qivec3 QuickOpenGLProperties::glMaxComputeWorkGroupCount() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeWorkGroupCount;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeWorkGroupSize
 */
qivec3 QuickOpenGLProperties::glMaxComputeWorkGroupSize() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeWorkGroupSize;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxDebugGroupStackDepth
 */
int QuickOpenGLProperties::glMaxDebugGroupStackDepth() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxDebugGroupStackDepth;
}

/*!
 * \qmlproperty int OpenGLProperties::glMax3DTextureSize
 *
 * This property holds the OpenGL implementation's maximum allowed 3D texture
 * size. It must be at least 64.
 *
 * \sa https://www.khronos.org/opengl/wiki/3D_Texture
 */
int QuickOpenGLProperties::glMax3DTextureSize() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMax3DTextureSize;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxArrayTextureLayers
 *
 * This property holds the OpenGL impementation's maximum allowed number of layers
 * in an array texture.  It must be at least 256.
 *
 * \sa https://www.khronos.org/opengl/wiki/Array_Texture
 */
int QuickOpenGLProperties::glMaxArrayTextureLayers() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxArrayTextureLayers;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxClipDistances
 */
int QuickOpenGLProperties::glMaxClipDistances() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxClipDistances;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxColorTextureSamples
 */
int QuickOpenGLProperties::glMaxColorTextureSamples() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxColorTextureSamples;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCombinedAtomicCounters
 */
int QuickOpenGLProperties::glMaxCombinedAtomicCounters() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCombinedAtomicCounters;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCombinedFragmentUniformComponents
 */
int QuickOpenGLProperties::glMaxCombinedFragmentUniformComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCombinedFragmentUniformComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCombinedGeometryUniformComponents
 */
int QuickOpenGLProperties::glMaxCombinedGeometryUniformComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCombinedGeometryUniformComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCombinedTextureImageUnits
 */
int QuickOpenGLProperties::glMaxCombinedTextureImageUnits() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCombinedTextureImageUnits;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCombinedUniformBlocks
 */
int QuickOpenGLProperties::glMaxCombinedUniformBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCombinedUniformBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCombinedVeretexUniformComponents
 */
int QuickOpenGLProperties::glMaxCombinedVeretexUniformComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCombinedVeretexUniformComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxCubeMapTextureSize
 */
int QuickOpenGLProperties::glMaxCubeMapTextureSize() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxCubeMapTextureSize;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxDepthTextureSamples
 */
int QuickOpenGLProperties::glMaxDepthTextureSamples() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxDepthTextureSamples;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxDrawBuffers
 */
int QuickOpenGLProperties::glMaxDrawBuffers() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxDrawBuffers;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxDualSourceDrawBuffers
 */
int QuickOpenGLProperties::glMaxDualSourceDrawBuffers() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxDualSourceDrawBuffers;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxElementsVertices
 */
int QuickOpenGLProperties::glMaxElementsVertices() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxElementsVertices;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFragmentAtomicCounters
 */
int QuickOpenGLProperties::glMaxFragmentAtomicCounters() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFragmentAtomicCounters;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFragmentShaderStorageBlocks
 */
int QuickOpenGLProperties::glMaxFragmentShaderStorageBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFragmentShaderStorageBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFragmentInputComponents
 */
int QuickOpenGLProperties::glMaxFragmentInputComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFragmentInputComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFragmentUniformComponents
 */
int QuickOpenGLProperties::glMaxFragmentUniformComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFragmentUniformComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFragmentUniformVectors
 */
int QuickOpenGLProperties::glMaxFragmentUniformVectors() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFragmentUniformVectors;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFragmentUniformBlocks
 */
int QuickOpenGLProperties::glMaxFragmentUniformBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFragmentUniformBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFramebufferWidth
 */
int QuickOpenGLProperties::glMaxFramebufferWidth() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFramebufferWidth;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFramebufferHeight
 */
int QuickOpenGLProperties::glMaxFramebufferHeight() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFramebufferHeight;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFramebufferLayers
 */
int QuickOpenGLProperties::glMaxFramebufferLayers() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFramebufferLayers;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxFramebufferSamples
 */
int QuickOpenGLProperties::glMaxFramebufferSamples() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxFramebufferSamples;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxGeometryAtomicCounters
 */
int QuickOpenGLProperties::glMaxGeometryAtomicCounters() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxGeometryAtomicCounters;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxGeometryShaderStorageBlocks
 */
int QuickOpenGLProperties::glMaxGeometryShaderStorageBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxGeometryShaderStorageBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxGeometryInputComponents
 */
int QuickOpenGLProperties::glMaxGeometryInputComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxGeometryInputComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxGeometryOutputComponents
 */
int QuickOpenGLProperties::glMaxGeometryOutputComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxGeometryOutputComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxGeometryTextureImageUnits
 */
int QuickOpenGLProperties::glMaxGeometryTextureImageUnits() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxGeometryTextureImageUnits;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxGeometryUniformBlocks
 */
int QuickOpenGLProperties::glMaxGeometryUniformBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxGeometryUniformBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxGeometryUniformComponents
 */
int QuickOpenGLProperties::glMaxGeometryUniformComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxGeometryUniformComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxIntegerSamples
 */
int QuickOpenGLProperties::glMaxIntegerSamples() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxIntegerSamples;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxLabelLength
 */
int QuickOpenGLProperties::glMaxLabelLength() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxLabelLength;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxProgramTexelOffset
 */
int QuickOpenGLProperties::glMaxProgramTexelOffset() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxProgramTexelOffset;
}

/*!
 * \qmlproperty int OpenGLProperties::glMinProgramTexelOffset
 */
int QuickOpenGLProperties::glMinProgramTexelOffset() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMinProgramTexelOffset;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxRectangleTextureSize
 */
int QuickOpenGLProperties::glMaxRectangleTextureSize() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxRectangleTextureSize;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxRenderbufferSize
 */
int QuickOpenGLProperties::glMaxRenderbufferSize() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxRenderbufferSize;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxSampleMaskWords
 */
int QuickOpenGLProperties::glMaxSampleMaskWords() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxSampleMaskWords;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxServerWaitTimeout
 */
int QuickOpenGLProperties::glMaxServerWaitTimeout() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxServerWaitTimeout;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxShaderStorageBufferBindings
 */
int QuickOpenGLProperties::glMaxShaderStorageBufferBindings() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxShaderStorageBufferBindings;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxTessControlAtomicCounters
 */
int QuickOpenGLProperties::glMaxTessControlAtomicCounters() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTessControlAtomicCounters;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxTessEvaluationAtomicCounters
 */
int QuickOpenGLProperties::glMaxTessEvaluationAtomicCounters() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTessEvaluationAtomicCounters;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxTessControlShaderStorageBlocks
 */
int QuickOpenGLProperties::glMaxTessControlShaderStorageBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTessControlShaderStorageBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxTessEvaluationShaderStorageBlocks
 */
int QuickOpenGLProperties::glMaxTessEvaluationShaderStorageBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTessEvaluationShaderStorageBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxTextureBufferSize
 */
int QuickOpenGLProperties::glMaxTextureBufferSize() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTextureBufferSize;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxTextureImageUnits
 */
int QuickOpenGLProperties::glMaxTextureImageUnits() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTextureImageUnits;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxTextureLodBias
 *
 * This property holds the absolute value of the OpenGL implementation's
 * maximum supported texture level-of-detail bias. The value must be at least
 * 2.0.
 */
float QuickOpenGLProperties::glMaxTextureLodBias() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTextureLodBias;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxTextureSize
 */
int QuickOpenGLProperties::glMaxTextureSize() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTextureSize;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxUniformBufferBindings
 */
int QuickOpenGLProperties::glMaxUniformBufferBindings() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxUniformBufferBindings;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxUniformBlockSize
 */
int QuickOpenGLProperties::glMaxUniformBlockSize() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxUniformBlockSize;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxUniformLocations
 */
int QuickOpenGLProperties::glMaxUniformLocations() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxUniformLocations;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVaryingComponents
 */
int QuickOpenGLProperties::glMaxVaryingComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVaryingComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVaryingVectors
 */
int QuickOpenGLProperties::glMaxVaryingVectors() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVaryingVectors;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVaryingFloats
 */
int QuickOpenGLProperties::glMaxVaryingFloats() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVaryingFloats;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexAtomicCounters
 */
int QuickOpenGLProperties::glMaxVertexAtomicCounters() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexAtomicCounters;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexAttribs
 */
int QuickOpenGLProperties::glMaxVertexAttribs() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexAttribs;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexShaderStorageBlocks
 */
int QuickOpenGLProperties::glMaxVertexShaderStorageBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexShaderStorageBlocks;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexTextureImageUnits
 */
int QuickOpenGLProperties::glMaxVertexTextureImageUnits() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexTextureImageUnits;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexUniformComponents
 */
int QuickOpenGLProperties::glMaxVertexUniformComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexUniformComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexUniformVectors
 */
int QuickOpenGLProperties::glMaxVertexUniformVectors() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexUniformVectors;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexOutputComponents
 */
int QuickOpenGLProperties::glMaxVertexOutputComponents() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexOutputComponents;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexUniformBlocks
 */
int QuickOpenGLProperties::glMaxVertexUniformBlocks() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexUniformBlocks;
}

/*!
 * \qmlproperty QSize OpenGLProperties::glMaxViewportDims
 */
QSize QuickOpenGLProperties::glMaxViewportDims() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxViewportDims;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxViewportsGlMaxViewports
 */
int QuickOpenGLProperties::glMaxViewportsGlMaxViewports() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxViewports;
}

/*!
 * \qmlproperty int OpenGLProperties::glMinorVersion
 */
int QuickOpenGLProperties::glMinorVersion() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMinorVersion;
}

/*!
 * \qmlproperty float OpenGLProperties::glPointSizeGranularity
 */
float QuickOpenGLProperties::glPointSizeGranularity() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glPointSizeGranularity;
}

/*!
 * \qmlproperty list<real> OpenGLProperties::glSmoothLineWidthRange
 *
 * This property holds the OpenGL implementation's supported smooth line width
 * range. It consists of two values, the first representing the minimum line
 * width, and the second representing the maximum line width.
 *
 * \sa glAliasedLineWidthRange
 * \sa glLineWidthRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glLineWidth
 */
QVector<float> QuickOpenGLProperties::glSmoothLineWidthRange() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glSmoothLineWidthRange;
}

/*!
 * \qmlproperty float OpenGLProperties::glSmoothLineWidthGranularity
 */
float QuickOpenGLProperties::glSmoothLineWidthGranularity() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glSmoothLineWidthGranularity;
}

/*!
 * \qmlproperty bool OpenGLProperties::glStereo
 */
bool QuickOpenGLProperties::glStereo() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glStereo;
}

/*!
 * \qmlproperty int OpenGLProperties::glSubPixelBits
 */
int QuickOpenGLProperties::glSubPixelBits() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glSubPixelBits;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexAttribRelativeOffset
 */
int QuickOpenGLProperties::glMaxVertexAttribRelativeOffset() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexAttribRelativeOffset;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxVertexAttribBindings
 */
int QuickOpenGLProperties::glMaxVertexAttribBindings() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxVertexAttribBindings;
}

/*!
 * \qmlproperty list<int> OpenGLProperties::glViewportBoundsRange
 */
QVector<int> QuickOpenGLProperties::glViewportBoundsRange() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glViewportBoundsRange;
}

/*!
 * \qmlproperty int OpenGLProperties::glViewportSubpixelBits
 */
int QuickOpenGLProperties::glViewportSubpixelBits() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glViewportSubpixelBits;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxElementIndex
 */
int QuickOpenGLProperties::glMaxElementIndex() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxElementIndex;
}


/*!
 * \qmlproperty int OpenGLProperties::glMaxTessGenLevel
 */
int QuickOpenGLProperties::glMaxTessGenLevel() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxTessGenLevel;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxPatchVertices
 */
int QuickOpenGLProperties::glMaxPatchVertices() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxPatchVertices;
}

/*!
 * \qmlproperty list<real> OpenGLProperties::glAliasedLineWidthRange
 *
 * This property holds the OpenGL implementation's supported aliased line width
 * range. It consists of two values, the first representing the minimum line
 * width, and the second representing the maximum line width.
 *
 * \sa glSmoothLineWidthRange
 * \sa glLineWidthRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glLineWidth
 */
QVector<float> QuickOpenGLProperties::glAliasedLineWidthRange() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glAliasedLineWidthRange;
}

/*!
 * \qmlproperty list<real> OpenGLProperties::glPointSizeRange
 *
 * This property holds the OpenGL impementation's supported point size range.
 * It consists of two values, the first representing the minimum point size,
 * and the second representing the maximum point size.
 *
 * \sa glSmoothPointSizeRange
 * \sa glAliasedPointSizeRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glPointSize
 */
QVector<float> QuickOpenGLProperties::glPointSizeRange() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glPointSizeRange;
}

/*!
 * \qmlproperty list<real> OpenGLProperties::glSmoothPointSizeRange
 *
 * This property holds the OpenGL impementation's supported smooth point size
 * range. It consists of two values, the first representing the minimum point
 * size, and the second representing the maximum point size.
 *
 * \sa glPointSizeRange
 * \sa glAliasedPointSizeRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glPointSize
 */
QVector<float> QuickOpenGLProperties::glSmoothPointSizeRange() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glSmoothPointSizeRange;
}

/*!
 * \qmlproperty list<real> OpenGLProperties::glAliasedPointSizeRange
 *
 * This property holds the OpenGL impementation's supported aliased point size
 * range. It consists of two values, the first representing the minimum point
 * size, and the second representing the maximum point size.
 *
 * \sa glPointSizeRange
 * \sa glSmoothPointSizeRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glPointSize
 */
QVector<float> QuickOpenGLProperties::glAliasedPointSizeRange() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glAliasedPointSizeRange;
}

/*!
 * \qmlproperty ivec3 OpenGLProperties::glMaxComputeVariableGroupSizeArb
 *
 * This property holds the OpenGL implementation's maximum supported compute
 * shader group size for the ARB_compute_variable_group_size extension.
 *
 * \sa glMaxComputeWorkGroupSize
 * \sa glMaxComputeWorkGroupInvocations
 * \sa glMaxComputeVariableGroupInvocationsArb
 */
qivec3 QuickOpenGLProperties::glMaxComputeVariableGroupSizeArb() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeVariableGroupSizeArb;
}

/*!
 * \qmlproperty int OpenGLProperties::glMaxComputeVariableGroupInvocationsArb
 *
 * This property holds the OpenGL implementation's maximum supported compute
 * shader group invocations for the ARB_compute_variable_group_size extenstion.
 *
 * \sa glMaxComputeWorkGroupSize
 * \sa glMaxComputeWorkGroupInvocations
 * \sa glMaxComputeVariableGroupSizeArb
 */
int QuickOpenGLProperties::glMaxComputeVariableGroupInvocationsArb() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMaxComputeVariableGroupInvocationsArb;
}

/*!
 * \qmlproperty int OpenGLProperties::vendor
 *
 * This property holds the OpenGL implementation's vendor string, as reported by
 * glGetString(GL_VENDOR).
 *
 */
QString QuickOpenGLProperties::vendor() const
{
    Q_D(const QuickOpenGLProperties);
    return d->vendor;
}

/*!
 * \qmlproperty var OpenGLProperties::extensions
 *
 * This property holds the OpenGL implementation's supported extensions as
 * a string list.
 *
 */
QStringList QuickOpenGLProperties::extensions() const
{
    Q_D(const QuickOpenGLProperties);
    return d->extensions;
}
/*!
 * \qmlproperty bool OpenGLProperties::isOpenGLES
 *
 * This property holds the OpenGL render type information.
 *
 * It returns true if the render type is Open GL ES.  It is generated at compile time from the platform configuration.
 */
bool QuickOpenGLProperties::isOpenGLES() const
{
#ifdef QT_OPENGL_ES
    return true;
#else
    return false;
#endif
}

/*!
 * \qmlproperty list<real> OpenGLProperties::glLineWidthRange
 *
 * This property holds the OpenGL implementation's supported line width
 * range. It consists of two values, the first representing the minimum line
 * width, and the second representing the maximum line width.
 *
 * \sa glSmoothLineWidthRange
 * \sa glAliasedLineWidthRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glLineWidth
 */
QVector<float> QuickOpenGLProperties::glLineWidthRange() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glLineWidthRange;
}

/*!
 * \qmlproperty int OpenGLProperties::glMajorVersion
 *
 * This property holds the OpenGL implementation's major version
 */
int QuickOpenGLProperties::glMajorVersion() const
{
    Q_D(const QuickOpenGLProperties);

    return d->glMajorVersion;
}

#include "quickopenglproperties.moc"

/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKTEXTUREVIEW_H
#define QUICKTEXTUREVIEW_H

#include <QQuickItem>
#include <QScopedPointer>

class QuickTextureViewPrivate;

class QuickTextureView : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QQuickItem * source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(Target target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(Format format READ format WRITE setFormat NOTIFY formatChanged)
    Q_PROPERTY(int minimumLevel READ minimumLevel WRITE setMinimumLevel NOTIFY minimumLevelChanged)
    Q_PROPERTY(int levels READ levels WRITE setLevels NOTIFY levelsChanged)
    Q_PROPERTY(int minimumLayer READ minimumLayer WRITE setMinimumLayer NOTIFY minimumLayerChanged)
    Q_PROPERTY(int layers READ layers WRITE setLayers NOTIFY layersChanged)


public:

    enum Target
    {
        Target1D = 0x0DE0, // GL_TEXTURE_1D
        Target1DArray = 0x8C18, // GL_TEXTURE_1D_ARRAY
        Target2D = 0x0DE1, // GL_TEXTURE_2D
        Target2DArray = 0x8C1A, // GL_TEXTURE_2D_ARRAY
        Target3D = 0x806F, // GL_TEXTURE_3D
        TargetCubeMap = 0x8513, // GL_TEXTURE_CUBE_MAP
        TargetCubeMapArray = 0x9009, // GL_TEXTURE_CUBE_MAP_ARRAY
        Target2DMultisample = 0x9100, // GL_TEXTURE_2D_MULTISAMPLE
        Target2DMultisampleArray = 0x9102, // GL_TEXTURE_2D_MULTISAMPLE_ARRAY
        TargetRectangle = 0x84F5, // GL_TEXTURE_RECTANGLE
        TargetBuffer = 0x8C2A // GL_TEXTURE_BUFFER
    };
    Q_ENUM(Target)

    enum Format
    {
        FormatNone = 0, // GL_NONE

        // Unsigned normalized formats
        FormatR8_UNorm = 0x8229, // GL_R8
        FormatRG8_UNorm = 0x822B, // GL_RG8
        FormatRGB8_UNorm = 0x8051, // GL_RGB8
        FormatRGBA8_UNorm = 0x8058, // GL_RGBA8

        FormatR16_UNorm = 0x822A, // GL_R16
        FormatRG16_UNorm = 0x822C, // GL_RG16
        FormatRGB16_UNorm = 0x8054, // GL_RGB16
        FormatRGBA16_UNorm = 0x805B, // GL_RGBA16

        // Signed normalized formats
        FormatR8_SNorm = 0x8F94, // GL_R8_SNORM
        FormatRG8_SNorm = 0x8F95, // GL_RG8_SNORM
        FormatRGB8_SNorm = 0x8F96, // GL_RGB8_SNORM
        FormatRGBA8_SNorm = 0x8F97, // GL_RGBA8_SNORM

        FormatR16_SNorm = 0x8F98, // GL_R16_SNORM
        FormatRG16_SNorm = 0x8F99, // GL_RG16_SNORM
        FormatRGB16_SNorm = 0x8F9A, // GL_RGB16_SNORM
        FormatRGBA16_SNorm = 0x8F9B, // GL_RGBA16_SNORM

        // Unsigned integer formats
        FormatR8U = 0x8232, // GL_R8UI
        FormatRG8U = 0x8238, // GL_RG8UI
        FormatRGB8U = 0x8D7D, // GL_RGB8UI
        FormatRGBA8U = 0x8D7C, // GL_RGBA8UI

        FormatR16U = 0x8234, // GL_R16UI
        FormatRG16U = 0x823A, // GL_RG16UI
        FormatRGB16U = 0x8D77, // GL_RGB16UI
        FormatRGBA16U = 0x8D76, // GL_RGBA16UI

        FormatR32U = 0x8236, // GL_R32UI
        FormatRG32U = 0x823C, // GL_RG32UI
        FormatRGB32U = 0x8D71, // GL_RGB32UI
        FormatRGBA32U = 0x8D70, // GL_RGBA32UI

        // Signed integer formats
        FormatR8I = 0x8231, // GL_R8I
        FormatRG8I = 0x8237, // GL_RG8I
        FormatRGB8I = 0x8D8F, // GL_RGB8I
        FormatRGBA8I = 0x8D8E, // GL_RGBA8I

        FormatR16I = 0x8233, // GL_R16I
        FormatRG16I = 0x8239, // GL_RG16I
        FormatRGB16I = 0x8D89, // GL_RGB16I
        FormatRGBA16I = 0x8D88, // GL_RGBA16I

        FormatR32I = 0x8235, // GL_R32I
        FormatRG32I = 0x823B, // GL_RG32I
        FormatRGB32I = 0x8D83, // GL_RGB32I
        FormatRGBA32I = 0x8D82, // GL_RGBA32I

        // Floating point formats
        FormatR16F = 0x822D, // GL_R16F
        FormatRG16F = 0x822F, // GL_RG16F
        FormatRGB16F = 0x881B, // GL_RGB16F
        FormatRGBA16F = 0x881A, // GL_RGBA16F

        FormatR32F = 0x822E, // GL_R32F
        FormatRG32F = 0x8230, // GL_RG32F
        FormatRGB32F = 0x8815, // GL_RGB32F
        FormatRGBA32F = 0x8814, // GL_RGBA32F

        // Packed formats
        FormatRGB9E5 = 0x8C3D, // GL_RGB9_E5
        FormatRG11B10F = 0x8C3A, // GL_R11F_G11F_B10F
        FormatRG3B2 = 0x2A10, // GL_R3_G3_B2
        FormatR5G6B5 = 0x8D62, // GL_RGB565
        FormatRGB5A1 = 0x8057, // GL_RGB5_A1
        FormatRGBA4 = 0x8056, // GL_RGBA4
        FormatRGB10A2 = 0x906F, // GL_RGB10_A2UI

        // Depth formats
        FormatD16 = 0x81A5, // GL_DEPTH_COMPONENT16
        FormatD24 = 0x81A6, // GL_DEPTH_COMPONENT24
        FormatD24S8 = 0x88F0, // GL_DEPTH24_STENCIL8
        FormatD32 = 0x81A7, // GL_DEPTH_COMPONENT32
        FormatD32F = 0x8CAC, // GL_DEPTH_COMPONENT32F
        FormatD32FS8X24 = 0x8CAD, // GL_DEPTH32F_STENCIL8
        FormatS8 = 0x8D48, // GL_STENCIL_INDEX8

        // Compressed formats
        FormatRGB_DXT1 = 0x83F0, // GL_COMPRESSED_RGB_S3TC_DXT1_EXT
        FormatRGBA_DXT1 = 0x83F1, // GL_COMPRESSED_RGBA_S3TC_DXT1_EXT
        FormatRGBA_DXT3 = 0x83F2, // GL_COMPRESSED_RGBA_S3TC_DXT3_EXT
        FormatRGBA_DXT5 = 0x83F3, // GL_COMPRESSED_RGBA_S3TC_DXT5_EXT
        FormatR_ATI1N_UNorm = 0x8DBB, // GL_COMPRESSED_RED_RGTC1
        FormatR_ATI1N_SNorm = 0x8DBC, // GL_COMPRESSED_SIGNED_RED_RGTC1
        FormatRG_ATI2N_UNorm = 0x8DBD, // GL_COMPRESSED_RG_RGTC2
        FormatRG_ATI2N_SNorm = 0x8DBE, // GL_COMPRESSED_SIGNED_RG_RGTC2
        FormatRGB_BP_UNSIGNED_FLOAT = 0x8E8F, // GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB
        FormatRGB_BP_SIGNED_FLOAT = 0x8E8E, // GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB
        FormatRGB_BP_UNorm = 0x8E8C, // GL_COMPRESSED_RGBA_BPTC_UNORM_ARB
        FormatR11_EAC_UNorm = 0x9270, // GL_COMPRESSED_R11_EAC
        FormatR11_EAC_SNorm = 0x9271, // GL_COMPRESSED_SIGNED_R11_EAC
        FormatRG11_EAC_UNorm = 0x9272, // GL_COMPRESSED_RG11_EAC
        FormatRG11_EAC_SNorm = 0x9273, // GL_COMPRESSED_SIGNED_RG11_EAC
        FormatRGB8_ETC2 = 0x9274, // GL_COMPRESSED_RGB8_ETC2
        FormatSRGB8_ETC2 = 0x9275, // GL_COMPRESSED_SRGB8_ETC2
        FormatRGB8_PunchThrough_Alpha1_ETC2 = 0x9276, // GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2
        FormatSRGB8_PunchThrough_Alpha1_ETC2 = 0x9277, // GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2
        FormatRGBA8_ETC2_EAC = 0x9278, // GL_COMPRESSED_RGBA8_ETC2_EAC
        FormatSRGB8_Alpha8_ETC2_EAC = 0x9279, // GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC

        // sRGB formats
        FormatSRGB8 = 0x8C41, // GL_SRGB8
        FormatSRGB8_Alpha8 = 0x8C43, // GL_SRGB8_ALPHA8
        FormatSRGB_DXT1 = 0x8C4C, // GL_COMPRESSED_SRGB_S3TC_DXT1_EXT
        FormatSRGB_Alpha_DXT1 = 0x8C4D, // GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT
        FormatSRGB_Alpha_DXT3 = 0x8C4E, // GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT
        FormatSRGB_Alpha_DXT5 = 0x8C4F, // GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT
        FormatSRGB_BP_UNorm = 0x8E8D, // GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM_ARB

        // ES 2 formats
        FormatDepthFormat = 0x1902, // GL_DEPTH_COMPONENT
        FormatAlphaFormat = 0x1906, // GL_ALPHA
        FormatRGBFormat = 0x1907, // GL_RGB
        FormatRGBAFormat = 0x1908, // GL_RGBA
        FormatLuminanceFormat = 0x1909, // GL_LUMINANCE
        FormatLuminanceAlphaFormat = 0x190A

    };
    Q_ENUM(Format)

    QuickTextureView(QQuickItem *parent = 0);
    virtual ~QuickTextureView();

    QQuickItem * source() const;
    Target target() const;
    int minimumLevel() const;
    int levels() const;
    int minimumLayer() const;
    int layers() const;
    Format format() const;

    bool isTextureProvider() const override;
    QSGTextureProvider *textureProvider() const override;

signals:

    void sourceChanged(QQuickItem * source);
    void targetChanged(Target target);
    void minimumLevelChanged(int minimumLevel);
    void levelsChanged(int levels);
    void minimumLayerChanged(int minimumLayer);
    void layersChanged(int layers);
    void formatChanged(Format format);

public slots:

    void setSource(QQuickItem * source);
    void setTarget(Target target);
    void setMinimumLevel(int minimumLevel);
    void setLevels(int levels);
    void setMinimumLayer(int minimumLayer);
    void setLayers(int layers);
    void setFormat(Format format);

protected:

    QScopedPointer<QuickTextureViewPrivate> d_ptr;

    QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *) override;
    void releaseResources() override;

private:

    Q_DECLARE_PRIVATE(QuickTextureView)

};

#endif // QUICKTEXTUREVIEW_H

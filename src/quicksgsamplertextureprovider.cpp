/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgsamplertextureprovider.h"

#include <QOpenGLExtraFunctions>
#include "support.h"

QuickSGSamplerTextureProvider::QuickSGSamplerTextureProvider() :
    m_sampler(0),
    m_sourceProvider(nullptr),
    m_magFilter(QuickSamplerObject::Filter(0)),
    m_minFilter(QuickSamplerObject::Filter(0)),
    m_lodBias(0.0),
    m_maxLod(1000.0),
    m_minLod(-1000.0),
    m_wrapS(QuickSamplerObject::Wrap(0)),
    m_wrapT(QuickSamplerObject::Wrap(0)),
    m_wrapR(QuickSamplerObject::Wrap(0))
{
    // generate a sampler object
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();
    glExtra->glGenSamplers(1, &m_sampler);debugGL;

    // set a property with our OpenGL sampler object - used when binding and getting ARB texture handle of source texture
    setProperty("SamplerObject", QVariant::fromValue(m_sampler));

}

QuickSGSamplerTextureProvider::~QuickSGSamplerTextureProvider()
{
    // clean up the sampler object
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();
    glExtra->glDeleteSamplers(1, &m_sampler);debugGL;
}

QSGTexture *QuickSGSamplerTextureProvider::texture() const
{
    Q_ASSERT(m_sourceProvider);

    if(m_sourceProvider)
    {
        return m_sourceProvider->texture();
    }

    return nullptr;
}

void QuickSGSamplerTextureProvider::setSourceProvider(QSGTextureProvider *sourceProvider)
{
    if(sourceProvider != m_sourceProvider)
    {

        if(m_sourceProvider)
        {
            disconnect(m_sourceProvider, &QSGTextureProvider::textureChanged, this, &QSGTextureProvider::textureChanged);
            disconnect(m_sourceProvider, &QSGTextureProvider::destroyed, this, &QuickSGSamplerTextureProvider::onSourceProviderDestroyed);
        }


        m_sourceProvider = sourceProvider;

        connect(m_sourceProvider, &QSGTextureProvider::textureChanged, this, &QSGTextureProvider::textureChanged, Qt::DirectConnection);
        connect(m_sourceProvider, &QSGTextureProvider::destroyed, this, &QuickSGSamplerTextureProvider::onSourceProviderDestroyed);

        emit textureChanged();
    }
}

void QuickSGSamplerTextureProvider::setMagFilter(QuickSamplerObject::Filter magFilter)
{
    if(magFilter!=m_magFilter)
    {
        m_magFilter = magFilter;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        glExtra->glSamplerParameteri(m_sampler, GL_TEXTURE_MAG_FILTER, m_magFilter);debugGL;

        emit textureChanged();
    }
}

void QuickSGSamplerTextureProvider::setMinFilter(QuickSamplerObject::Filter minFilter)
{
    if(minFilter!=m_minFilter)
    {
        m_minFilter = minFilter;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        glExtra->glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, m_minFilter);debugGL;

        emit textureChanged();
    }

}

void QuickSGSamplerTextureProvider::setLodBias(double lodBias)
{
    if(lodBias!=m_lodBias)
    {
        m_lodBias = lodBias;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        glExtra->glSamplerParameterf(m_sampler, GL_TEXTURE_LOD_BIAS, m_lodBias);debugGL;

        emit textureChanged();
    }

}

void QuickSGSamplerTextureProvider::setMaxLod(double maxLod)
{
    if(maxLod!=m_maxLod)
    {
        m_maxLod = maxLod;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        glExtra->glSamplerParameterf(m_sampler, GL_TEXTURE_MAX_LOD, m_maxLod);debugGL;

        emit textureChanged();
    }
}

void QuickSGSamplerTextureProvider::setMinLod(double minLod)
{
    if(minLod!=m_minLod)
    {
        m_minLod = minLod;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        glExtra->glSamplerParameterf(m_sampler, GL_TEXTURE_MIN_LOD, m_minLod);debugGL;

        emit textureChanged();
    }

}

void QuickSGSamplerTextureProvider::setWrapS(QuickSamplerObject::Wrap wrapS)
{
    if(wrapS!=m_wrapS)
    {
        m_wrapS = wrapS;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        glExtra->glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_S, m_wrapS);debugGL;

        emit textureChanged();
    }

}

void QuickSGSamplerTextureProvider::setWrapT(QuickSamplerObject::Wrap wrapT)
{
    if(wrapT!=m_wrapT)
    {
        m_wrapT = wrapT;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        glExtra->glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_T, m_wrapT);debugGL;

        emit textureChanged();
    }

}

void QuickSGSamplerTextureProvider::setWrapR(QuickSamplerObject::Wrap wrapR)
{
    if(wrapR!=m_wrapT)
    {
        m_wrapR = wrapR;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        glExtra->glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_R, m_wrapR);debugGL;

        emit textureChanged();
    }

}

void QuickSGSamplerTextureProvider::setBorderColor(const QColor &borderColor)
{
    if(borderColor!=m_borderColor)
    {
        m_borderColor = borderColor;

        Q_ASSERT(QOpenGLContext::currentContext());
        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        QVector<float> color= QVector<float>({float(m_borderColor.redF()), float(m_borderColor.greenF()), float(m_borderColor.blueF()), float(m_borderColor.alphaF())});
        glExtra->glSamplerParameterfv(m_sampler, GL_TEXTURE_BORDER_COLOR, color.constData());debugGL;

        emit textureChanged();
    }

}

void QuickSGSamplerTextureProvider::onSourceProviderDestroyed()
{
    m_sourceProvider = nullptr;
}

/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quickopenglthreadpool.h"
#include <QOpenGLContext>
#include <QtConcurrent>
#include <QHash>
#include <QThread>
#include <QMutexLocker>
#include <QOffscreenSurface>
#include "support.h"
#include "quickopengloffscreensurfacefactory.h"

QHash<QOpenGLContext *, QThreadPool *> QuickOpenGLThreadPool::threadPool;

QMutex QuickOpenGLThreadPool::threadPoolMutex;

bool createOpenGLContext(QOpenGLContext *rootContext)
{

    // create a new shared context
    QOpenGLContext *context = new QOpenGLContext();
    Q_ASSERT(rootContext->isValid());
    context->setShareContext(rootContext);
    bool ok = context->create();
    Q_ASSERT(ok);
    Q_ASSERT(context->isValid());

#ifdef Q_OS_WIN
    QOffscreenSurface *surface = QuickOpenGLOffscreenSurfaceFactory::instance()->getSurface();
#else
    // create an offscreen surface
    QOffscreenSurface *surface = new QOffscreenSurface(QGuiApplication::primaryScreen());
    surface->setFormat(QSurfaceFormat::defaultFormat());
    surface->create();
#endif
    Q_ASSERT(surface->isValid());

    // make our context current on our offscreen surface
    context->makeCurrent(surface);debugGL;

    // clean up when our thread is finished
    QObject::connect(QThread::currentThread(), &QThread::finished, [surface, context](){context->doneCurrent();surface->deleteLater();delete context;});

    return true;
}


QThreadPool *QuickOpenGLThreadPool::globalInstance()
{
    Q_ASSERT(QOpenGLContext::currentContext());

    QOpenGLContext *context = QOpenGLContext::currentContext();

    QMutexLocker locker(&threadPoolMutex);

    if(!threadPool.contains(context))
    {

        // create a thread pool
        QThreadPool *pool = new QThreadPool;

        // make the thread never expire
        pool->setExpiryTimeout(-1);

        // set the thread pool to have 1 thread
        pool->setMaxThreadCount(1);

        // get the surface that the context is current on
        QSurface *surface = context->surface();

        // some drivers (windows intel) don't like making shared surfaces with one that is current
        context->doneCurrent();

        // run a job to create a shared OpenGL context in the thread
        QFuture<bool> future = QtConcurrent::run(pool, &::createOpenGLContext, context);

        // wait for the job to complete
        future.waitForFinished();

        // make our context current again
        context->makeCurrent(surface);

        // make sure it worked
        Q_ASSERT(future.result());

        // add the pool to our hash table
        threadPool[context] = pool;

        // make sure we clean up when the context is destroyed
        QObject::connect(context, &QOpenGLContext::aboutToBeDestroyed, [context](){QCoreApplication::processEvents(); QMutexLocker locker(&threadPoolMutex); threadPool[context]->waitForDone(-1); QCoreApplication::processEvents(); delete threadPool[context]; threadPool.remove(context);});

    }

    // return the thread pool
    return threadPool[context];
}

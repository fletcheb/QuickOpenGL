/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include <QRegularExpression>
#include <QSGGeometryNode>
#include <QQuickWindow>
#include <QMutex>
#include <QHash>
#include "quicksguniformbufferprovider.h"
#include "quicksgssboprovider.h"

#ifdef QT_DEBUG
#include <QElapsedTimer>
#endif

#include "quickgraphicsprogram.h"
#include "quickgraphicsprogrammaterial.h"
#include "introspection.h"
#include "support.h"

/*!
 * \qmltype GraphicsProgram
 * \brief The GraphicsProgram QML type represents an OpenGL graphics program.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickGraphicsProgram
 * \ingroup QuickOpenGL QML Types
 *
 * GraphicsProgram represents an OpenGL program with graphics shader stages. Conceptually, it may
 * be viewed as an enhanced version of the \l{Qt Quick} \l ShaderEffect. Like \l ShaderEffect, it
 * manages a \l QSGGeometryNode in the \l{Qt Quick Scene Graph}.
 *
 * GraphicsProgram allows custom OpenGL Shader Language (GLSL) vertex, tessellation control,
 * tessellation evaluation, geometry, and fragment shaders to be specified directly in QML. It also
 * allows customisation of scene graph geometry and material through introspection of the OpenGL
 * program, matching GLSL uniforms, uniform blocks, shader storage blocks, and subroutine uniforms
 * with user defined QML properties.
 *
 * \section2 Geometry Specification
 *
 * \note The \l{Qt Quick Scene Graph} is designed to operate on two dimensional data. Specifying
 * geometry with clip space z values other than zero may have unintended consequences.
 *
 * GraphicsProgram allows specification of custom geometry through declaration of OpenGL vertex
 * attributes within a GLSL vertex shader. GraphicsProgram uses OpenGL introspection to determine
 * the declared vertex attributes, then attempts to match them with vertex data provided from QML
 * via the \l vertices property. The introspected vertex attributes are made available via the
 * \l{attributes} property.  The OpenGL drawing mode used to process the vertices may be set
 * via the \l drawingMode property.
 *
 * Simple vertices containing a single vertex attribute may be specified in QML as a list of vertex
 * values. If no vertex attributes are specified, \l vertices may be set to an integer value
 * representing the number of vertices required. The vertex shader may then use the in-built OpenGL
 * vertex language variable
 * \l{https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/gl_VertexID.xhtml}{gl_VertexID} to
 * programatically generate or pull vertices. Complex vertices containing more than one vertex
 * attribute may be specified in QML as a list of key-value pairs, where the key is the GLSL vertex
 * attribute name, and the value is a QML object. In both cases, the value must be able to be
 * converted to the GLSL type via the Qt \l QMetaType system. QuickOpenGL registers QMetaType types
 * and converters for common \l {Basic GLSL Types}{GLSL types}.
 *
 * If no geometry is specified, default geometry consisting of a rectangle covering the item's
 * width and height is used. The default geometry uses the same attribute names as \l ShaderEffect,
 * and is equivalent to the following:
 *
 * \qml
 * vertices: [{qt_Vertex: Qt.vector4d(0, 0, 0, 1), qt_MultiTexCoord0: Qt.vector2d(0, 0)},
 *            {qt_Vertex: Qt.vector4d(0, height, 0, 1), qt_MultiTexCoord0: Qt.vector2d(0, 1)},
 *            {qt_Vertex: Qt.vector4d(width, 0, 0, 1), qt_MultiTexCoord0: Qt.vector2d(1, 0)},
 *            {qt_Vertex: Qt.vector4d(width, height, 0, 1), qt_MultiTexCoord0: Qt.vector2d(1, 1)}]
 * \endqml
 *
 * The following code snippet demonstrates GLSL specification of vertex attributes 'vertexPosition'
 * and 'vertexColor', and QML specification of a list of \l vertices with these attributes.
 *
 * \qml
 * import QuickOpenGL 1.0
 *
 * function vertex(x, y, color)
 * {
 *     return {vertexPosition: Qt.vector2d(x,y), vertexColor: color}
 * }
 *
 * GraphicsProgram
 * {
 *     vertices : [vertex(-1.0, -1.0, '#ff0000'),
 *                 vertex(1.0, -1.0, Qt.rgba(0, 1, 0, 1)),
 *                 vertex(0.0, 1.0, 'blue')]
 *
 *     vertexShader: "
 *         #version 330
 *
 *         // vertex attributes
 *         in vec2 vertexPosition;
 *         in vec3 vertexColor;
 *
 *         out vec3 color;
 *
 *         void main()
 *         {
 *             color = vertexColor;
 *             gl_Position = vec4(vertexPosition, 0.0, 1.0);
 *         }"
 *
 *
 *     fragmentShader: "
 *         #version 330
 *
 *         in vec3 color;
 *         out vec4 fragColor;
 *
 *         void main()
 *         {
 *             fragColor = vec4(color, 1.0);
 *         }"
 * }
 * \endqml
 *
 * Graphics program also supports specification of custom geometry without vertex attributes.
 * Examples where this may be useful include applications where vertices are pulled from an OpenGL
 * buffer object. In this case \l vertices is set to an integer value representing the number of
 * vertices to be pulled. The following code snippet demonstrates this use case.
 *
 * \qml
 * import QuickOpenGL 1.0
 *
 * GraphicsProgram
 * {
 *     vertices : 3
 *
 *     vertexShader: "
 *         #version 330
 *
 *         in int glVertexID;
 *         out vec3 color;
 *
 *         void main()
 *         {
 *             switch(gl_VertexID)
 *             {
 *                 case 0:
 *                     gl_Position = vec4(-1, -1, 0, 1);
 *                     color = vec3(1, 0, 0);
 *                     break;
 *                 case 1:
 *                     gl_Position = vec4(1, -1, 0, 1);
 *                     color = vec3(0, 1, 0);
 *                     break;
 *                 case 2:
 *                     gl_Position = vec4(0, 1, 0, 1);
 *                     color = vec3(0, 0, 1);
 *                     break;
 *             }
 *
 *         }"
 *
 *     fragmentShader: "
 *         #version 330
 *
 *         in vec3 color;
 *         out vec4 fragColor;
 *
 *         void main()
 *         {
 *             fragColor = vec4(color, 1.0);
 *         }"
 * }
 * \endqml
 *
 * GraphicsProgram reports QML to OpenGL vertex conversion errors such as mismatching vertex
 * attribute array sizes, incompatible types and missing attributes. These errors are logged to the
 * console.
 *
 * GraphicsProgram allows the geometry triangle winding order to be set via the \l frontFace
 * property.  The culling mode may also be set via \l cullFace.
 *
 * \section2 Material Specification
 *
 * GraphicsProgram allows definition of custom materials through specification of GLSL shader code
 * for all shader stages in the OpenGL graphics pipeline, and declaration of uniforms, subroutine
 * uniforms, uniform blocks, and shader storage blocks within this shader code. GraphicsProgram
 * uses OpenGL introspection to determine the declared GLSL storage variables, then attempts to
 * match them with properties of the QML GraphicsProgram object. The search space for QML
 * properties extends to all properties of GraphicsProgram, including those inherited from \l
 * QQuickItem, and those defined by the user in QML. This means that properties such as width and
 * height are automatically available as part of the material. Information on the introspected
 * OpenGL variables are available via the \l{uniforms}, \l{subroutineUniforms}, \l{uniformBlocks},
 * and \l{shaderStorageBlocks} properties.  QuickOpenGL supports OpenGL arrays and structs for
 * block definitions and uniforms.
 *
 * When a GraphicsProgram material property is updated in QML, GraphicsProgram's \l
 * QQuickItem::update is called. During the resultant call to \l{QQuickItem::updatePaintNode}, the
 * updated property is transferred to the scene graph material.
 *
 * Like \l ShaderEffect, GraphicsProgram predefines some intrinsic \l{Qt Quick Scene Graph}
 * material properties. These are essential for integration with the scene graph, and are listed
 * below.
 *
 * \table
 * \header
 *   \li GLSL Declaration
 *   \li Description
 * \row
 *   \li uniform mat4 qt_Matrix;
 *   \li The scene graph combined transformation matrix - \l QSGMaterialShader::RenderState::combinedMatrix
 * \row
 *   \li uniform mat4 qt_CombinedMatrix;
 *   \li The scene graph combined transformation matrix - \l QSGMaterialShader::RenderState::combinedMatrix
 * \row
 *   \li uniform mat4 qt_ProjectionMatrix;
 *   \li The scene graph projection matrix - \l QSGMaterialShader::RenderState::projectionMatrix
 * \row
 *   \li uniform mat4 qt_ModelViewMatrix;
 *   \li The scene graph model view matrix - \l QSGMaterialShader::RenderState::modelViewMatrix
 * \row
 *    \li uniform float qt_Opacity;
 *    \li The scene graph combined opacity  - \l QSGMaterialShader::RenderState::opacity
 * \row
 *    \li uniform mat4 qt_ViewPort;
 *    \li The OpenGL viewport of being rendered to - \l QSGMaterialShader::RenderState::viewportRect
 * \row
 *    \li uniform int qt_FrameID;
 *    \li The scene graph renderer frame counter
 * \endtable
 *
 * The following code snippet demonstrates GraphicsProgram material specification.
 *
 * \qml
 * import QuickOpenGL 1.0
 *
 * GraphicsProgram
 * {
 *     anchors.fill: parent
 *
 *     vertices : [{vertexPosition: Qt.vector2d(0, height)},
 *                 {vertexPosition: Qt.vector2d(width, height)},
 *                 {vertexPosition: Qt.vector2d(width/2, 0)}]
 *
 *     property var vertexColor: ['#ff0000', '#00ff00', '#0000ff']
 *     property real scale: 0.0
 *     NumberAnimation on scale {to: 1.0; duration: 10000}
 *     opacity: 0.5
 *     blending: true
 *
 *     vertexShader: "
 *         #version 330
 *
 *         in vec2 vertexPosition;
 *         in int gl_VertexID;
 *
 *         out vec3 color;
 *
 *         uniform mat4 qt_Matrix;
 *         uniform vec3 vertexColor[3];
 *         uniform float scale;
 *
 *         void main()
 *         {
 *             color = vertexColor[gl_VertexID];
 *             gl_Position = qt_Matrix*vec4(scale*vertexPosition, 0.0, 1.0);
 *         }"
 *
 *     fragmentShader: "
 *         #version 330
 *
 *         in vec3 color;
 *
 *         out vec4 fragColor;
 *
 *         uniform float qt_Opacity;
 *
 *         void main()
 *         {
 *             fragColor = vec4(color, 1.0)*qt_Opacity;
 *         }"
 * }
 * \endqml
 */


class QuickGraphicsProgramPrivate : public QObject
{
    Q_OBJECT

public:

    QuickGraphicsProgramPrivate(QuickGraphicsProgram *q);

    // Shader GLSL source code
    QByteArray vertexShader;
    QByteArray tessellationControlShader;
    QByteArray tessellationEvaluationShader;
    QByteArray geometryShader;
    QByteArray fragmentShader;

    // Geometry
    QVariant vertices;
    QuickGraphicsProgram::UsagePattern vertexUsagePattern;
    QuickGraphicsProgram::DrawingMode drawingMode;

    // Points
    double pointSize;
    bool programPointSize;
    QuickGraphicsProgram::CoordOrigin pointSpriteCoordOrigin;
    double pointFadeThresholdSize;

    // Lines
    double lineWidth;
    bool lineSmooth;

    // Polygons
    QuickGraphicsProgram::CullFace cullFace;
    QuickGraphicsProgram::FrontFace frontFace;
    QuickGraphicsProgram::PolygonMode polygonMode;

    // Patches
    int patchVertices;
    QList<double> patchDefaultOuterLevel;
    QList<double> patchDefaultInnerLevel;

    // Blending
    bool blend;
    QuickGraphicsProgram::BlendFunction blendSrcRgb;
    QuickGraphicsProgram::BlendFunction blendDstRgb;
    QuickGraphicsProgram::BlendFunction blendSrcAlpha;
    QuickGraphicsProgram::BlendFunction blendDstAlpha;

    // Color logic
    QuickGraphicsProgram::LogicOp logicOp;

    // Memory barrier
    QuickGraphicsProgram::Barrier memoryBarrier;

    // Introspected program properties
    QList<Introspection::Attribute> attributes;
    QHash<int, QList<Introspection::SubroutineUniform>> subroutineUniforms;
    QHash<QString, Introspection::ShaderStorageBlock> shaderStorageBlocks;
    QList<Introspection::Uniform> uniforms;
    QHash<QString, Introspection::UniformBlock> uniformBlocks;
    QHash<QString, QList<Introspection::Uniform>> baseUniforms;

    // Introspected program properties to report back to QML
    QVariantMap qmlAttributes;
    QVariantMap qmlUniforms;
    QVariantMap qmlSubroutineUniforms;
    QVariantMap qmlUniformBlocks;
    QVariantMap qmlShaderStorageBlocks;

    // Flags
    bool defaultVertices;
    bool verticesDirty;
    bool initialized;

    QSGGeometry::AttributeSet attributeSet;

    QList<int> updatedPropertyIndices;

    void introspect();
    void initializeAttributes();
    void connectMaterialProperties();
    QSGGeometry *updateGeometry() const;

    template <typename Type> static bool setVertexData(const QVariant &variant, char *data, const Introspection::Attribute &attribute, const QQuickItem *qmlItem);
    static bool setVertexData(const QVariant &variant, char *data, const Introspection::Attribute &attribute, const QQuickItem *q);

    static QMutex mutex;
    static QHash<QOpenGLContext *, bool> contextInitialized;

    void vendorSpecificInitialization(const QString &vendor);

    void initializeOpenGLContext();

public slots:

    void update();
    void onWindowChanged(QQuickWindow * window);
    void initialize();


protected:

    QuickGraphicsProgram *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickGraphicsProgram)

};

QMutex QuickGraphicsProgramPrivate::mutex;
QHash<QOpenGLContext *, bool> QuickGraphicsProgramPrivate::contextInitialized;

QuickGraphicsProgramPrivate::QuickGraphicsProgramPrivate(QuickGraphicsProgram *q) :
    vertexUsagePattern(QuickGraphicsProgram::UsagePattern::UsagePatternDynamic),
    drawingMode(QuickGraphicsProgram::DrawingMode::DrawingModeTriangleStrip),
    pointSize(1.0),
    programPointSize(false),
    pointSpriteCoordOrigin(QuickGraphicsProgram::CoordOrigin::CoordOriginUpperLeft),
    pointFadeThresholdSize(1.0),
    lineWidth(1.0),
    lineSmooth(false),
    cullFace(QuickGraphicsProgram::CullFace::CullFaceBack),
    frontFace(QuickGraphicsProgram::FrontFace::FrontFaceCCW),
    polygonMode(QuickGraphicsProgram::PolygonMode::PolygonModeFill),
    patchVertices(0),
    patchDefaultOuterLevel({1.0, 1.0, 1.0, 1.0}),
    patchDefaultInnerLevel({1.0, 1.0}),
    blend(false),
    blendSrcRgb(QuickGraphicsProgram::BlendFunction::BlendFunctionOne),
    blendDstRgb(QuickGraphicsProgram::BlendFunction::BlendFunctionOneMinusSrcAlpha),
    blendSrcAlpha(QuickGraphicsProgram::BlendFunction::BlendFunctionOne),
    blendDstAlpha(QuickGraphicsProgram::BlendFunction::BlendFunctionOneMinusSrcAlpha),
    logicOp(QuickGraphicsProgram::LogicOp::LogicOpNone),
    memoryBarrier(QuickGraphicsProgram::BarrierNone),
    defaultVertices(true),
    verticesDirty(true),
    initialized(false),
    q_ptr(q)
{

#ifdef QT_OPENGL_ES
    // The default vertex shader - from ShaderEffect
    vertexShader = " \
                   uniform highp mat4 qt_Matrix;\n \
                   attribute highp vec4 qt_Vertex;\n \
                   attribute highp vec2 qt_MultiTexCoord0;\n \
                   varying highp vec2 coord;\n \
                   void main() { \n \
                       coord = qt_MultiTexCoord0;\n \
                       gl_Position = qt_Matrix * qt_Vertex; \n \
                   }\n";
#else
    // The default vertex shader - from ShaderEffect
    vertexShader = "\
      #version 330 core \n\
      uniform highp mat4 qt_Matrix; \n\
      in highp vec4 qt_Vertex; \n\
      in highp vec2 qt_MultiTexCoord0; \n\
      out highp vec2 coord; \n\
      void main() \n\
      { \n\
          coord = qt_MultiTexCoord0; \n\
          gl_Position = qt_Matrix * qt_Vertex; \n\
      }\n";
#endif

    vertexShader = vertexShader.trimmed();

#ifdef QT_OPENGL_ES
    // The default fragement shader - from ShaderEffect
    fragmentShader = " \
                     varying highp vec2 coord;\n \
                     uniform lowp float qt_Opacity;\n \
                     void main() {\n \
                        gl_FragColor = vec4(coord, 0.0, qt_Opacity);\n \
                     }\n";

#else
    // The default fragement shader - from ShaderEffect
    fragmentShader = "\
    #version 330 core \n\
    in highp vec2 coord; \n\
    out vec4 qt_FragColor; \n\
    uniform lowp float qt_Opacity; \n\
    void main() \n\
    { \n\
         qt_FragColor = vec4(coord, 0.0, qt_Opacity); \n\
    }\n";
#endif
    fragmentShader = fragmentShader.trimmed();

    // initialize the attribute set
    attributeSet.attributes = nullptr;
    attributeSet.count = 0;
    attributeSet.stride = 0;

    // default vertices
    QVariantList vertexList;
    vertexList.append(QVariantMap{std::pair<QString, QVector4D>(QStringLiteral("qt_Vertex"), QVector4D(0, 0, 0, 1)), std::pair<QString, QVector2D>(QStringLiteral("qt_MultiTexCoord0"), QVector2D(0,0))});
    vertexList.append(QVariantMap{std::pair<QString, QVector4D>(QStringLiteral("qt_Vertex"), QVector4D(0, float(q->height()), 0, 1)), std::pair<QString, QVector2D>(QStringLiteral("qt_MultiTexCoord0"), QVector2D(0,1))});
    vertexList.append(QVariantMap{std::pair<QString, QVector4D>(QStringLiteral("qt_Vertex"), QVector4D(float(q->width()), 0, 0, 1)), std::pair<QString, QVector2D>(QStringLiteral("qt_MultiTexCoord0"), QVector2D(1,0))});
    vertexList.append(QVariantMap{std::pair<QString, QVector4D>(QStringLiteral("qt_Vertex"), QVector4D(float(q->width()), float(q->height()), 0, 1)), std::pair<QString, QVector2D>(QStringLiteral("qt_MultiTexCoord0"), QVector2D(1,1))});
    vertices = vertexList;

}

/*!
 * \internal
 * \brief Compiles the OpenGL program and introspects its properties
 *
 * This function must only be called from QSGRenderThread with a current OpenGL
 * context. It creates, compiles and links an OpenGL program using the
 * specified shader code, then introspects its properties. It should be noted
 * that this is a temporary QOpenGLShaderProgram, and is not the program used
 * in the scene graph.
 */
void QuickGraphicsProgramPrivate::introspect()
{
    Q_Q(QuickGraphicsProgram);

    // Compile and link our OpenGL graphics program
    QOpenGLShaderProgram program;
    program.addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShader);
    program.addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShader);
    if(!geometryShader.isEmpty())
    {
        program.addShaderFromSourceCode(QOpenGLShader::Geometry, geometryShader);
    }
    if(!tessellationControlShader.isEmpty())
    {
        program.addShaderFromSourceCode(QOpenGLShader::TessellationControl, tessellationControlShader);
    }
    if(!tessellationEvaluationShader.isEmpty())
    {
        program.addShaderFromSourceCode(QOpenGLShader::TessellationEvaluation, tessellationEvaluationShader);
    }
    program.link();

    int version = QOpenGLContext::currentContext()->format().majorVersion()*100+QOpenGLContext::currentContext()->format().minorVersion()*10;

    // Introspect the OpenGL graphics program properties
    attributes = Introspection::activeAttributes(program.programId());

    if(version>=430)
    {
        shaderStorageBlocks.clear();
        for(const Introspection::ShaderStorageBlock &shaderStorageBlock : Introspection::activeShaderStorageBlocks(program.programId()))
        {
            shaderStorageBlocks[shaderStorageBlock.name] = shaderStorageBlock;
        }
    }

    if(version>=400)
    {
        subroutineUniforms[GL_VERTEX_SHADER] = Introspection::activeSubroutineUniforms(program.programId(), GL_VERTEX_SHADER);
        subroutineUniforms[GL_TESS_CONTROL_SHADER] = Introspection::activeSubroutineUniforms(program.programId(), GL_TESS_CONTROL_SHADER);
        subroutineUniforms[GL_TESS_EVALUATION_SHADER] = Introspection::activeSubroutineUniforms(program.programId(), GL_TESS_EVALUATION_SHADER);
        subroutineUniforms[GL_GEOMETRY_SHADER] = Introspection::activeSubroutineUniforms(program.programId(), GL_GEOMETRY_SHADER);
        subroutineUniforms[GL_FRAGMENT_SHADER] = Introspection::activeSubroutineUniforms(program.programId(), GL_FRAGMENT_SHADER);
    }

    uniformBlocks.clear();
    for(const Introspection::UniformBlock &uniformBlock : Introspection::activeUniformBlocks(program.programId()))
    {
        uniformBlocks[uniformBlock.name] = uniformBlock;
    }

    uniforms = Introspection::activeUniforms(program.programId());

    baseUniforms.clear();
    for(const Introspection::Uniform &uniform: uniforms)
    {
        // Get the base name
        QRegularExpressionMatch match = QRegularExpression(QStringLiteral("([\\w\\d]+)")).match(uniform.name);
        Q_ASSERT(match.hasMatch());
        baseUniforms[match.captured(1)].append(uniform);
    }


    qmlAttributes.clear();
    for(Introspection::Attribute &attribute: attributes)
    {
        QVariantMap object;
        object["name"] = QString(attribute.name);
        object["type"] = QuickOpenGL::toString(attribute.type);
        object["location"] = attribute.location;
        object["arraySize"] = attribute.arraySize;
        object["size"] = QuickOpenGL::primitiveSize(QuickOpenGL::primitiveType(attribute.type))*QuickOpenGL::tupleSize(attribute.type)*QuickOpenGL::tupleCount(attribute.type)*attribute.arraySize;
        qmlAttributes[attribute.name] = object;
    }
    emit q->attributesChanged(qmlAttributes);

    qmlShaderStorageBlocks.clear();
    for(const Introspection::ShaderStorageBlock &block: shaderStorageBlocks)
    {
        QVariantMap object;
        object["name"] = block.name;
        object["index"] = block.index;
        object["binding"] = block.binding;
        object["size"] = block.size;

        QVariantMap variables;
        for(const Introspection::ShaderStorageBlock::Variable &variable: block.variables)
        {
            QVariantMap object;
            object["name"] = QString(variable.name);
            object["type"] = QuickOpenGL::toString(variable.type);
            object["offset"] = variable.offset;
            object["blockIndex"] = variable.blockIndex;
            object["arrayStride"] = variable.arrayStride;
            object["arraySize"] = variable.arraySize;
            object["matrixStride"] = variable.matrixStride;
            object["isRowMajor"] = variable.isRowMajor;
            object["topLevelArraySize"] = variable.topLevelArraySize;
            object["topLevelArrayStride"] = variable.topLevelArrayStride;
            variables[variable.name] = object;
        }

        object["variables"] = variables;
        qmlShaderStorageBlocks[block.name] = object;
    }
    emit q->shaderStorageBlocksChanged(qmlShaderStorageBlocks);

    qmlUniforms.clear();
    for(const Introspection::Uniform &uniform: uniforms)
    {
        QVariantMap object;
        object["name"] = QString(uniform.name);
        object["type"] = QuickOpenGL::toString(uniform.type);
        object["location"] = uniform.location;
        object["arraySize"] = uniform.arraySize;
        object["blockIndex"] = uniform.blockIndex;
        object["offset"] = uniform.offset;
        object["arrayStride"] = uniform.arrayStride;
        object["matrixStride"] = uniform.matrixStride;
        object["isRowMajor"] = uniform.isRowMajor;
        object["atomicCounterBufferIndex"] = uniform.atomicCounterBufferIndex;
        qmlUniforms[uniform.name] = object;
    }
    emit q->uniformsChanged(qmlUniforms);

    qmlSubroutineUniforms.clear();
    QList<int> shaderStages({GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER,  GL_FRAGMENT_SHADER});
    for(const int &stage: shaderStages)
    {
        QVariantMap shaderStage;

        for(const Introspection::SubroutineUniform &uniform: subroutineUniforms[stage])
        {
            QVariantMap object;
            object["name"] = QString(uniform.name);
            object["location"] = uniform.location;

            QVariantMap routines;
            for(QHash<QByteArray, int>::const_iterator it = uniform.compatibleSubroutines.constBegin();it!=uniform.compatibleSubroutines.constEnd();++it)
            {
                QVariantMap object;
                object["name"] = QString(it.key());
                object["index"] = it.value();
                routines[it.key()] = object;
            }
            object["compatibleSubroutines"] = routines;
            shaderStage[uniform.name] = object;
        }

        qmlSubroutineUniforms[QuickOpenGL::toString(stage)] = shaderStage;
    }
    emit q->subroutineUniformsChanged(qmlSubroutineUniforms);

    qmlUniformBlocks.clear();
    for(const Introspection::UniformBlock &block: uniformBlocks)
    {
        QVariantMap object;
        object["name"] = block.name;
        object["index"] = block.index;
        object["binding"] = block.binding;
        object["size"] = block.size;

        QVariantMap uniforms;
        for(const Introspection::Uniform &uniform: block.uniforms)
        {
            QVariantMap object;
            object["name"] = QString(uniform.name);
            object["type"] = QuickOpenGL::toString(uniform.type);
            object["location"] = uniform.location;
            object["arraySize"] = uniform.arraySize;
            object["blockIndex"] = uniform.blockIndex;
            object["offset"] = uniform.offset;
            object["arrayStride"] = uniform.arrayStride;
            object["matrixStride"] = uniform.matrixStride;
            object["isRowMajor"] = uniform.isRowMajor;
            object["atomicCounterBufferIndex"] = uniform.atomicCounterBufferIndex;
            uniforms[uniform.name] = object;
        }
        object["uniforms"] = uniforms;
        qmlUniformBlocks[block.name] = object;
    }
    emit q->uniformBlocksChanged(qmlUniformBlocks);
}

/*!
 * \internal
 * \brief Discovers matching OpenGL / QMetaObject properties and connects notify signals
 *
 * This function should only be called after OpenGL properties have been
 * introspected. It determines a list of OpenGL properties, and matches them
 * with our QMetaObject properties. If the QMetaObject property has a notify
 * signal it is connected to QuickGraphicsProgramPrivate::update.
 *
 * If matching properties are found, the QMetaObject property index is added to
 * the updatedPropertyIndices list. This causes the QMetaObject property to be
 * copied to the scene graph material in QuickGraphicsProgram::updatePaintNode.
 */
void QuickGraphicsProgramPrivate::connectMaterialProperties()
{
    Q_Q(QuickGraphicsProgram);

    // list of material property names
    QStringList names;
    QHash<QString, QString> type;

    // add properties we want to pass to the material
    names << "vertexShader" << "tessellationControlShader" << "tessellationEvaluationShader" << "geometryShader"
          << "fragmentShader" << "lineSmooth" << "blend" << "blendSrcRgb" << "blendDstRgb" << "blendSrcAlpha"
          << "blendDstAlpha" << "logicOp" << "polygonMode" << "programPointSize" << "pointFadeThresholdSize"
          << "pointSpriteCoordOrigin" << "cullFace" << "frontFace"  << "memoryBarrier" << "patchVertices"
          << "patchDefaultOuterLevel" << "patchDefaultInnerLevel";

    // add the uniform names
    for(const Introspection::Uniform &uniform: uniforms)
    {
        names << uniform.name;
        type[uniform.name] = "uniform";
    }

    // add the uniform block names
    for(const Introspection::UniformBlock &uniformBlock: uniformBlocks)
    {
        names << uniformBlock.name;
        type[uniformBlock.name] = "uniform block";
    }

    // add the shader storage block names
    for(const Introspection::ShaderStorageBlock &shaderStorageBlock: shaderStorageBlocks)
    {
        names << shaderStorageBlock.name;
        type[shaderStorageBlock.name] = "shader storage block";
    }

    // add the subroutine uniform names
    for(const QList<Introspection::SubroutineUniform> &shaderSubroutineUniforms : subroutineUniforms)
    {
        for(const Introspection::SubroutineUniform &subroutineUniform: shaderSubroutineUniforms)
        {
            names << subroutineUniform.name;
            type[subroutineUniform.name] = "subroutine uniform";
        }
    }

    QMetaMethod slot = metaObject()->method(metaObject()->indexOfMethod("update()"));
    Q_ASSERT(slot.isValid());

    // process the names
    for(const QString &name: names)
    {
        // remove any array indexing and struct information
        Q_ASSERT(QRegularExpression("([\\w\\d]+)").match(name).hasMatch());
        QString propertyName = QRegularExpression("([\\w\\d]+)").match(name).captured(1);

        int index = q->metaObject()->indexOfProperty(propertyName.toLocal8Bit());

        if(!updatedPropertyIndices.contains(index))
        {
            if((index!=-1))
            {
                QMetaProperty property = q->metaObject()->property(index);

                if(property.hasNotifySignal())
                {
                    connect(q, property.notifySignal(), this, slot);
                }

                updatedPropertyIndices.append(index);
            }
            else if(!propertyName.startsWith("qt_"))
            {
                qWarning() << "QuickOpenGL:" << reinterpret_cast<QObject *>(q) << "no QML property found for OpenGL" << qPrintable(type[name]) << propertyName;
            }
        }
    }
}

/*!
 * \internal
 * \brief Initialize the QSGGeometry attribute set
 *
 * This function should only be called after OpenGL properties have been
 * introspected. It initializes the QSGGeometry attribute set to reflect the
 * attributes specified in the vertex shader.
 */
void QuickGraphicsProgramPrivate::initializeAttributes()
{

    if(attributes.count())
    {
        int tuples = 0;
        for(int i=0;i<attributes.count();++i)
        {
            tuples += QuickOpenGL::tupleCount(attributes.at(i).type)*attributes.at(i).arraySize;
        }

        // Set the Quick Scene Graph Geometry attributes
        attributeSet.count = tuples;
        attributeSet.attributes = new QSGGeometry::Attribute[tuples];
        memset(const_cast<QSGGeometry::Attribute *>(attributeSet.attributes), 0, sizeof(QSGGeometry::Attribute)*tuples);
        attributeSet.stride = 0;

        int tuple = 0;
        for(int i=0;i<attributes.count();++i)
        {
            for(int j=0;j<QuickOpenGL::tupleCount(attributes.at(i).type)*attributes.at(i).arraySize;++j)
            {

                const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[tuple].position = attributes.at(i).location + j;
                //
                // The scene graph calls glVertexAttribPointer with normalise set to true on integer types.  This alters
                // the vertex values, which is not the behaviour we desire.  For the moment, we just tell the scene graph
                // that the vertex attribute is a float or double to effectively pass it through unmolested.
                //

                //
                // Note: As of Qt5.12, the scene graph does not handle passing GL_DOUBLE primitive based vertex attributes
                // properly.  For OpenGL 4.1 and later, GL_DOUBLE pointers must be set using glVertexAttribLPointer.  The
                // scene graph uses glVertexAttribPointer.
                //

                // determine the attribute primitive type and tuple size
                GLenum primitiveType = QuickOpenGL::primitiveType(attributes.at(i).type);
                int tupleSize = QuickOpenGL::tupleSize(attributes.at(i).type);

                const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[tuple].type = (primitiveType==GL_DOUBLE)?GL_DOUBLE:GL_FLOAT;
                const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[tuple].tupleSize = tupleSize;

                // We don't want the scene graph to determine our bounds as we might be doing strange transforms in our shaders
                const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[tuple].isVertexCoordinate = false;
                const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[tuple].attributeType = QSGGeometry::UnknownAttribute;

                // update the stride
                attributeSet.stride += QuickOpenGL::primitiveSize(primitiveType)*tupleSize;

                ++tuple;
            }
        }

    }
    else
    {
        // Set a dummy attribute
        attributeSet.count = 1;
        attributeSet.attributes = new QSGGeometry::Attribute[1];
        attributeSet.stride = 0;
        const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[0].position = 0;
        const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[0].type = GL_INT;
        const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[0].tupleSize = 1;
        // We don't want the scene graph to determine our bounds as we might be doing strange transforms in our shaders
        const_cast<QSGGeometry::Attribute *>(attributeSet.attributes)[0].isVertexCoordinate = false;
        attributeSet.stride += 4;

    }
}

/*
 * \fn bool QuickGraphicsProgramPrivate::setVertexData(const QVariant &variant, char *data, const Introspection::Attribute &attribute, const QQuickItem *qmlItem)
 * \internal
 * \brief Convert and write a vertex attribute value for a particular type to memory
 * \param variant The vertex attribute value to be converted and written to memory
 * \param data A pointer into the QSGGeometry::vertexData buffer where the attribute value is to be written
 * \param attribute The introspected vertex attribute information
 * \param qmlItem The QML item which owns this data (used for debug messages)
 *
 */
template <typename Type> bool QuickGraphicsProgramPrivate::setVertexData(const QVariant &variant, char *data, const Introspection::Attribute &attribute, const QQuickItem *qmlItem)
{

    QVariantList list = Introspection::conditionProperty(variant, attribute.name, attribute.type, attribute.arraySize, "vertex attribute", qmlItem);

    // make sure our list of values is equal to the OpenGL attribute size
    if(list.count() && (list.count()!=attribute.arraySize))
    {
        qWarning() << "QuickOpenGL:" << qmlItem << "vertex attribute array size mismatch:" << attribute.name << "OpenGL vertex attribute array contains" << attribute.arraySize << "element(s), QML vertex attribute contains" << list.count() << "element(s)";
    }

    // select the size that we can deal with
    int size = qMin(list.count(), attribute.arraySize);

    // convert and write to the output
    bool result = size>0;
    for(int i=0;i<size&&result;++i)
    {
        if(list.at(i).canConvert<Type>())
        {
            reinterpret_cast<Type &>(data[i*int(sizeof(Type))]) = list.at(i).value<Type>();
        }

        result &= list.at(i).canConvert<Type>();
    }

    return result;

}

bool QuickGraphicsProgramPrivate::setVertexData(const QVariant &variant, char *data, const Introspection::Attribute &attribute, const QQuickItem *q)
{
    bool matched = false;

    switch(attribute.type)
    {
    case GL_FLOAT:
        matched = setVertexData<float>(variant, data, attribute, q);
        break;
    case GL_FLOAT_VEC2:
        matched = setVertexData<qvec2>(variant, data, attribute, q);
        break;
    case GL_FLOAT_VEC3:
        matched = setVertexData<qvec3>(variant, data, attribute, q);
        break;
    case GL_FLOAT_VEC4:
        matched = setVertexData<qvec4>(variant, data, attribute, q);
        break;
    case GL_DOUBLE:
        matched = setVertexData<double>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_VEC2:
        matched = setVertexData<qdvec2>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_VEC3:
        matched = setVertexData<qdvec3>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_VEC4:
        matched = setVertexData<qdvec4>(variant, data, attribute, q);
        break;
    case GL_INT:
        matched = setVertexData<int>(variant, data, attribute, q);
        break;
    case GL_INT_VEC2:
        matched = setVertexData<qivec2>(variant, data, attribute, q);
        break;
    case GL_INT_VEC3:
        matched = setVertexData<qivec3>(variant, data, attribute, q);
        break;
    case GL_INT_VEC4:
        matched = setVertexData<qivec4>(variant, data, attribute, q);
        break;
    case GL_UNSIGNED_INT:
        matched = setVertexData<unsigned int>(variant, data, attribute, q);
        break;
    case GL_UNSIGNED_INT_VEC2:
        matched = setVertexData<quvec2>(variant, data, attribute, q);
        break;
    case GL_UNSIGNED_INT_VEC3:
        matched = setVertexData<quvec3>(variant, data, attribute, q);
        break;
    case GL_UNSIGNED_INT_VEC4:
        matched = setVertexData<quvec4>(variant, data, attribute, q);
        break;
    case GL_BOOL:
        matched = setVertexData<bool>(variant, data, attribute, q);
        break;
    case GL_BOOL_VEC2:
        matched = setVertexData<qbvec2>(variant, data, attribute, q);
        break;
    case GL_BOOL_VEC3:
        matched = setVertexData<qbvec3>(variant, data, attribute, q);
        break;
    case GL_BOOL_VEC4:
        matched = setVertexData<qbvec4>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT2:
        matched = setVertexData<qmat2>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT3:
        matched = setVertexData<qmat3>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT4:
        matched = setVertexData<qmat4>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT2x3:
        matched = setVertexData<qmat2x3>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT2x4:
        matched = setVertexData<qmat2x4>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT3x2:
        matched = setVertexData<qmat3x2>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT3x4:
        matched = setVertexData<qmat3x4>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT4x2:
        matched = setVertexData<qmat4x2>(variant, data, attribute, q);
        break;
    case GL_FLOAT_MAT4x3:
        matched = setVertexData<qmat4x3>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT2:
        matched = setVertexData<qdmat2>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT3:
        matched = setVertexData<qdmat3>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT4:
        matched = setVertexData<qdmat4>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT2x3:
        matched = setVertexData<qdmat2x3>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT2x4:
        matched = setVertexData<qdmat2x4>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT3x2:
        matched = setVertexData<qdmat3x2>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT3x4:
        matched = setVertexData<qdmat3x4>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT4x2:
        matched = setVertexData<qdmat4x2>(variant, data, attribute, q);
        break;
    case GL_DOUBLE_MAT4x3:
        matched = setVertexData<qdmat4x3>(variant, data, attribute, q);
        break;
    default:
        qWarning() << "QuickOpenGL:" << q << "unsupported vertex attribute type" << QuickOpenGL::GLenum(attribute.type);
        break;
    }

    return matched;
}

/*!
 * \internal
 * \brief Update the geometry
 * \return A QSGGeometry oject containing the updated geometry
 *
 * Tried changing this to a static function and running via QtConcurrent::run
 * for large vertex arrays.  Caused random segfaults, probably because the vertices
 * are still owned and acccessed by the QML thread.
 */
QSGGeometry *QuickGraphicsProgramPrivate::updateGeometry() const
{
    Q_Q(const QuickGraphicsProgram);

    QSGGeometry *geometry = nullptr;

    // If we are dealing with a list of vertices with attributes
    if(vertices.canConvert<QVariantList>())
    {

        // create new vertices with our introspected attribute set
        QVariantList vertices = this->vertices.toList();
        geometry = new QSGGeometry(attributeSet, vertices.count());

        char *data = static_cast<char *>(geometry->vertexData());
        int offset = 0;

        // for each vertex
        for(const QVariant &vertex: vertices)
        {

            Q_ASSERT(offset<vertices.count()*attributeSet.stride);

            // if we only have one attribute and vertices = QVariantList of attribute type
            if((attributes.count()==1)&&!vertex.canConvert<QVariantMap>())
            {
                Introspection::Attribute attribute = attributes.first();

                bool matched = setVertexData(vertex, &data[offset], attribute, q);

                if(!matched)
                {
                    qWarning() << "QuickOpenGL:" << q << "vertex attribute" << attribute.name << "unable to be converted from" << vertex << "to" << QuickOpenGL::GLenum(attribute.type);
                }

                // update the byte offset
                offset += QuickOpenGL::tupleCount(attribute.type)*QuickOpenGL::primitiveSize(QuickOpenGL::primitiveType(attribute.type))*QuickOpenGL::tupleSize(attribute.type)*attribute.arraySize;

            }
            else if(vertex.canConvert<QVariantMap>())
            {

                QVariantMap map = vertex.value<QVariantMap>();

                // for each OpenGL vertex attribute
                for(const Introspection::Attribute &attribute: attributes)
                {
                    // if this is a built in OpenGL gl_* attribute (location is less than zero - on nVidia at least!)
                    if(attribute.location<0)
                    {
                        // bail out
                        continue;
                    }

                    // remove array and struct subscripts from the attribute name
                    QRegularExpressionMatch match = QRegularExpression("([\\w\\d\\[\\]]+)").match(attribute.name);
                    Q_ASSERT(match.hasMatch());
                    QString name = match.captured(1);

                    // make sure the Javascript object contains the attribute name as a property
                    if(map.contains(name))
                    {
                        QVariant variant = map[name];

                        bool matched = setVertexData(variant, &data[offset], attribute, q);

                        if(!matched)
                        {
                            qWarning() << "QuickOpenGL:" << q << "vertex attribute" << attribute.name << "unable to be converted from" << variant << "to" << QuickOpenGL::GLenum(attribute.type);
                        }

                    }
                    else
                    {
                        qWarning() << "QuickOpenGL:" << q << "vertex attribute" << attribute.name << "not found in vertex:" << map;
                    }

                    // update the byte offset
                    offset += QuickOpenGL::tupleCount(attribute.type)*QuickOpenGL::primitiveSize(QuickOpenGL::primitiveType(attribute.type))*QuickOpenGL::tupleSize(attribute.type)*attribute.arraySize;
                }

            }
            else if(vertex.canConvert<QObject *>())
            {
                QObject *object = vertex.value<QObject *>();

                // for each OpenGL vertex attribute
                for(const Introspection::Attribute &attribute: attributes)
                {
                    // if this is a built in OpenGL gl_* attribute (location is less than zero - on nVidia at least!)
                    if(attribute.location<0)
                    {
                        // bail out
                        continue;
                    }

                    // remove array and struct subscripts from the attribute name
                    QRegularExpressionMatch match = QRegularExpression("([\\w\\d\\[\\]]+)").match(attribute.name);
                    Q_ASSERT(match.hasMatch());
                    QString name = match.captured(1);

                     QVariant variant = object->property(name.toLocal8Bit().constData());

                    // make sure the object contains the attribute name as a property
                    if(variant.isValid())
                    {

                        bool matched = setVertexData(variant, &data[offset], attribute, q);

                        if(!matched)
                        {
                            qWarning() << "QuickOpenGL:" << q << "vertex attribute" << attribute.name << "unable to be converted from" << variant << "to" << QuickOpenGL::GLenum(attribute.type);
                        }

                    }
                    else
                    {
                        qWarning() << "QuickOpenGL:" << q << "vertex attribute" << attribute.name << "not found in vertex:" << object;
                    }

                    // update the byte offset
                    offset += QuickOpenGL::tupleCount(attribute.type)*QuickOpenGL::primitiveSize(QuickOpenGL::primitiveType(attribute.type))*QuickOpenGL::tupleSize(attribute.type)*attribute.arraySize;
                }

            }
            else
            {
                qDebug() << "QuickOpenGL:" << q << "cannot convert vertex" << vertex << "to QVariantMap or QObject*";
            }

        }
    }
    // if we are dealing with a buffer containing vertex data
    else if(vertices.canConvert<QByteArray>()&&(vertices.value<QByteArray>()!=QByteArray::number(vertices.value<int>()))&&(vertices.value<QByteArray>()!=QByteArrayLiteral("nan")))
    {
        QByteArray data = vertices.value<QByteArray>();
        if(data.size()%attributeSet.stride)
        {
            qWarning() << "QuickOpenGL:" << q << "supplied vertex data buffer size" << data.size() << "not an integer multiple of vertex size" << attributeSet.stride << attributes;
        }
        geometry = new QSGGeometry(attributeSet, data.size()/attributeSet.stride);

        memcpy(geometry->vertexData(), data.constData(), geometry->vertexCount()*geometry->sizeOfVertex());
    }
    // if we are dealing with a number of vertices with no custom attributes
    else if(vertices.canConvert<int>())
    {
        int count = std::max(0,vertices.toInt());
        geometry = new QSGGeometry(attributeSet, count);
    }
    else
    {
        qWarning() << "QuickOpenGL:" << q << "could not handle geometry" << vertices;
        geometry = new QSGGeometry(attributeSet, 0);
    }


    return geometry;
}

void QuickGraphicsProgramPrivate::vendorSpecificInitialization(const QString &vendor)
{
    Q_ASSERT(QOpenGLContext::currentContext());

    QOpenGLContext *context = QOpenGLContext::currentContext();

    if(vendor.contains("NVIDIA", Qt::CaseInsensitive))
    {
        // GL_POINT_SPRITE is supposedly enabled by default in modern OpenGL, but NVIDIA needs this.
        // This causes the driver to complain that GL_POINT_SPRITE is deprecated etc.
        context->functions()->glEnable(GL_POINT_SPRITE);debugGLquiet;
    }
}

/*!
 * \internal
 * \brief Update slot called when a QuickGraphicsProgram meta property is changed
 *
 * This function identifies the QuickGraphicsProgram property which has changed,
 * and adds it to the list of properties to be copied to the material next time
 * updatePaintNode is called.  It then triggers a scene graph update by calling
 * QuickGraphicsProgram:update.
 */
void QuickGraphicsProgramPrivate::update()
{
    Q_Q(QuickGraphicsProgram);

    // Get the name of the notify signal we have received
    QByteArray signalName = sender()->metaObject()->method(senderSignalIndex()).name();
    Q_ASSERT(signalName.endsWith(QByteArrayLiteral("Changed")));

    // Get the index of the property
    int propertyIndex = sender()->metaObject()->indexOfProperty(signalName.left(signalName.count()-7));
    Q_ASSERT(propertyIndex!=-1);

    // Add the property index to the list of updated properties to be copied to the material
    if(!updatedPropertyIndices.contains(propertyIndex))
    {
        updatedPropertyIndices.append(propertyIndex);
    }

    // Trigger an update on our QQuickItem
    q->update();
}

void QuickGraphicsProgramPrivate::onWindowChanged(QQuickWindow *window)
{
    if(window)
    {
        connect(window, &QQuickWindow::sceneGraphInitialized, this, &QuickGraphicsProgramPrivate::initialize, Qt::DirectConnection);
    }
}

void QuickGraphicsProgramPrivate::initialize()
{
    // perform initialization to work around some vendor specific OpenGL driver issues
    initializeOpenGLContext();

    // introspect our OpenGL program
    introspect();

    // connect our material properties
    connectMaterialProperties();

    // Initialize our OpenGL vertex attributes
    initializeAttributes();

    initialized = true;
}

// runs in QSGRender thread
void QuickGraphicsProgramPrivate::initializeOpenGLContext()
{
    Q_ASSERT(QOpenGLContext::currentContext());

    QOpenGLContext *context = QOpenGLContext::currentContext();

    mutex.lock();
    if(!contextInitialized.contains(context)||!contextInitialized[context])
    {
        QString vendor = QString(reinterpret_cast<const char *>(context->functions()->glGetString(GL_VENDOR)));debugGL;
        vendorSpecificInitialization(vendor);
        contextInitialized[context]=true;
    }
    mutex.unlock();
}

QuickGraphicsProgram::QuickGraphicsProgram(QQuickItem *parent) :
    QQuickItem(parent),
    d_ptr(new QuickGraphicsProgramPrivate(this))
{
    Q_D(QuickGraphicsProgram);

    setFlag(QQuickItem::ItemHasContents, true);

    connect(this, &QQuickItem::windowChanged, d, &QuickGraphicsProgramPrivate::onWindowChanged, Qt::DirectConnection);
}

QuickGraphicsProgram::~QuickGraphicsProgram()
{
    Q_D(QuickGraphicsProgram);

    if(d->attributeSet.attributes)
    {
        delete[] d->attributeSet.attributes;
    }
}

/*!
 * \qmlproperty string GraphicsProgram::vertexShader
 *
 * This property holds the material's vertex shader GLSL source code, or the
 * name of a file containing the code.
 *
 * A qrc resource file may be specified as follows:
 *
 * \code
 * vertexShader: ':/myshader.vert'
 * \endcode
 *
 * Inline shader code may be specified in the following code block. This code
 * corresponds to the default vertexShader value.
 *
 * \code
 * vertexShader: '
 *      #version 330 core
 *
 *      uniform highp mat4 qt_Matrix;
 *
 *      in highp vec4 qt_Vertex;
 *      in highp vec2 qt_MultiTexCoord0;
 *
 *      out highp vec2 coord;
 *
 *      void main()
 *      {
 *          coord = qt_MultiTexCoord0;
 *          gl_Position = qt_Matrix * qt_Vertex;
 *      };'
 * \endcode
 *
 * \note To aid debugging, when inlining shader source code in QML, add a
 * '#line xxx' statement on the line following the #version statement. xxx
 * should equal the .qml file line number at which the #line statement
 * resides plus one. The GLSL compiler will then report errors using the .qml
 * file line numbers.
 *
 *  \sa https://www.khronos.org/opengl/wiki/Vertex_Shader
 */
QByteArray QuickGraphicsProgram::vertexShader() const
{
    Q_D(const QuickGraphicsProgram);
    return d->vertexShader;
}

void QuickGraphicsProgram::setVertexShader(QByteArray vertexShader)
{
    Q_D(QuickGraphicsProgram);

    if(d->initialized)
    {
        qWarning() << "QuickOpenGL" << this << "cannot change vertex shader after scenegraph node initialized";
        return;
    }

    if (d->vertexShader == vertexShader.trimmed())
        return;

    // if the shader does not contain "void main()"
    if(!QString(vertexShader).contains(QRegExp("void \\s*main\\s*\\([\\svoid]*\\)")))
    {
        QString filename = QString(vertexShader);
        if(filename.startsWith("qrc"))
        {
            filename.remove(0,3);
        }

        QFile file(filename);

        if(file.exists()&&file.open(QIODevice::ReadOnly))
        {
            vertexShader = file.readAll();
        }
        else
        {
            qWarning() << "QuickOpenGL" << this << "could not open vertex shader source file" << vertexShader;
        }
    }

    d->vertexShader = vertexShader.trimmed();

    emit vertexShaderChanged(vertexShader);
}

/*!
 * \qmlproperty string GraphicsProgram::tessellationControlShader
 *
 * This property holds the material's tessellation control shader GLSL source
 * code, or the name of a file containing the code.
 *
 * The default value is empty.
 *
 * \sa vertexShader
 * \sa https://www.khronos.org/opengl/wiki/Tessellation_Control_Shader
 */
QByteArray QuickGraphicsProgram::tessellationControlShader() const
{
    Q_D(const QuickGraphicsProgram);
    return d->tessellationControlShader;
}

void QuickGraphicsProgram::setTessellationControlShader(QByteArray tessellationControlShader)
{
    Q_D(QuickGraphicsProgram);

    if(d->initialized)
    {
        qWarning() << "QuickOpenGL" << this << "cannot change tessellation control shader after scenegraph node initialized";
        return;
    }

    if (d->tessellationControlShader == tessellationControlShader.trimmed())
        return;

    // if the shader does not contain "void main()"
    if(!QString(tessellationControlShader).contains(QRegExp("void \\s*main\\s*\\([\\svoid]*\\)")))
    {
        QString filename = QString(tessellationControlShader);
        if(filename.startsWith("qrc"))
        {
            filename.remove(0,3);
        }

        QFile file(filename);

        if(file.exists()&&file.open(QIODevice::ReadOnly))
        {
            tessellationControlShader = file.readAll();
        }
        else
        {
            qWarning() << "QuickOpenGL" << this << "could not open tessellation control shader shader source file" << tessellationControlShader;
        }
    }

    d->tessellationControlShader = tessellationControlShader.trimmed();
    emit tessellationControlShaderChanged(tessellationControlShader);
}

/*!
 * \qmlproperty string GraphicsProgram::tessellationEvaluationShader
 *
 * This property holds the material's tessellation evaluation shader GLSL
 * source code, or the name of a file containing the code.
 *
 *  The default value is empty.
 *
 * \sa vertexShader
 * \sa https://www.khronos.org/opengl/wiki/Tessellation_Evaluation_Shader
 */
QByteArray QuickGraphicsProgram::tessellationEvaluationShader() const
{
    Q_D(const QuickGraphicsProgram);
    return d->tessellationEvaluationShader;
}

void QuickGraphicsProgram::setTessellationEvaluationShader(QByteArray tessellationEvaluationShader)
{
    Q_D(QuickGraphicsProgram);

    if(d->initialized)
    {
        qWarning() << "QuickOpenGL" << this << "cannot change tessellation evaluation shader after scenegraph node initialized";
        return;
    }


    if (d->tessellationEvaluationShader == tessellationEvaluationShader.trimmed())
        return;

    // if the shader does not contain "void main()"
    if(!QString(tessellationEvaluationShader).contains(QRegExp("void \\s*main\\s*\\([\\svoid]*\\)")))
    {

        QString filename = QString(tessellationEvaluationShader);
        if(filename.startsWith("qrc"))
        {
            filename.remove(0,3);
        }

        QFile file(filename);

        if(file.exists()&&file.open(QIODevice::ReadOnly))
        {
            tessellationEvaluationShader = file.readAll();
        }
        else
        {
            qWarning() << "QuickOpenGL" << this << "could not open tessellation evaluation shader source file" << tessellationEvaluationShader;
        }
    }

    d->tessellationEvaluationShader = tessellationEvaluationShader.trimmed();
    emit tessellationEvaluationShaderChanged(tessellationEvaluationShader);
}

/*!
 * \qmlproperty string GraphicsProgram::geometryShader
 *
 * This property holds the material's geometry shader GLSL source code, or the
 * name of a file containing the code.
 *
 * The default value is empty.
 *
 * \sa vertexShader
 * \sa https://www.khronos.org/opengl/wiki/Geometry_Shader
 */
QByteArray QuickGraphicsProgram::geometryShader() const
{
    Q_D(const QuickGraphicsProgram);
    return d->geometryShader;
}

void QuickGraphicsProgram::setGeometryShader(QByteArray geometryShader)
{
    Q_D(QuickGraphicsProgram);

    if(d->initialized)
    {
        qWarning() << "QuickOpenGL" << this << "cannot change geometry shader after scenegraph node initialized";
        return;
    }

    if (d->geometryShader == geometryShader)
        return;

    // if the shader does not contain "void main()"
    if(!QString(geometryShader).contains(QRegExp("void \\s*main\\s*\\([\\svoid]*\\)")))
    {
        QString filename = QString(geometryShader);
        if(filename.startsWith("qrc"))
        {
            filename.remove(0,3);
        }

        QFile file(filename);
        if(file.exists()&&file.open(QIODevice::ReadOnly))
        {
            geometryShader = file.readAll();
        }
        else
        {
            qWarning() << "QuickOpenGL" << this << "could not open geometry shader source file" << geometryShader;
        }
    }

    d->geometryShader = geometryShader.trimmed();
    emit geometryShaderChanged(geometryShader);
}

/*!
 * \qmlproperty string GraphicsProgram::fragmentShader
 *
 * This property holds the material's fragment shader GLSL source code, or the
 * name of a file containing the code.
 *
 * The default value is:
 *
 * \code
 *  #version 330 core
 *
 *  in highp vec2 coord;
 *
 *  out vec4 qt_FragColor;
 *
 *  uniform lowp float qt_Opacity;
 *
 *  void main()
 *  {
 *       qt_FragColor = vec4(coord, 0.0, qt_Opacity);
 *  }
 * \endcode
 *
 *
 * \sa vertexShader
 * \sa https://www.khronos.org/opengl/wiki/Fragment_Shader
 */
QByteArray QuickGraphicsProgram::fragmentShader() const
{
    Q_D(const QuickGraphicsProgram);

    return d->fragmentShader;
}

void QuickGraphicsProgram::setFragmentShader(QByteArray fragmentShader)
{
    Q_D(QuickGraphicsProgram);

    if(d->initialized)
    {
        qWarning() << "QuickOpenGL" << this << "cannot change fragment shader after scenegraph node initialized";
        return;
    }

    if (d->fragmentShader == fragmentShader)
        return;

    // if the shader does not contain "void main()"
    if(!QString(fragmentShader).contains(QRegExp("void \\s*main\\s*\\([\\svoid]*\\)")))
    {
        QString filename = QString(fragmentShader);
        if(filename.startsWith("qrc"))
        {
            filename.remove(0,3);
        }

        QFile file(filename);
        if(file.exists()&&file.open(QIODevice::ReadOnly))
        {
            fragmentShader = file.readAll();
        }
        else
        {
            qWarning() << "QuickOpenGL" << this << "could not open fragment shader source file" << fragmentShader;
        }
    }

    d->fragmentShader = fragmentShader.trimmed();
    emit fragmentShaderChanged(fragmentShader);
}

/*!
 * \qmlproperty object GraphicsProgram::vertices
 *
 * This property holds the geometry. It can be a list of key-value pairs, an
 * integer, a list of variants, a list of QObjects, or a data buffer.
 *
 * If specified as a list of key-value pairs, the keys must be the GLSL vertex
 * attribute names, and the values must be able to be converted to the
 * attribute's GLSL type via the Qt \l QMetaType system.
 *
 * If specified as an integer, the geometry has no attributes and the vertex shader must pull
 * vertex values from elsewhere. The vertex invocation may be determined from the OpenGL in-built
 * variable
 * \l{https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/gl_VertexID.xhtml}{gl_VertexID}.
 *
 * If specified as a list of variants, the vertex shader must contain only one
 * attribute, and the variants must be able to be converted to the attribute's
 * GLSL type via the Qt \l QMetaType system.
 *
 * If specified as a data \l{buffer}, or a custom type able to be converted to
 * \l{QByteArray} via the \l{QMetaType} system, the vertices are set to the
 * buffer data, with no checking against introspected attribute information
 * performed.
 *
 * The default value consits of a rectangle covering the item's width and
 * height and is equivalent to the following:
 *
 * \qml
 * vertices: [{qt_Vertex: Qt.vector4d(0, 0, 0, 1), qt_MultiTexCoord0: Qt.vector2d(0, 0)},
 *            {qt_Vertex: Qt.vector4d(0, height, 0, 1), qt_MultiTexCoord0: Qt.vector2d(0, 1)},
 *            {qt_Vertex: Qt.vector4d(width, 0, 0, 1), qt_MultiTexCoord0: Qt.vector2d(1, 0)},
 *            {qt_Vertex: Qt.vector4d(width, height, 0, 1), qt_MultiTexCoord0: Qt.vector2d(1, 1)}]
 * \endqml
 *
 * \sa {Geometry Specification}
 * \sa {https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/gl_VertexID.xhtml}{gl_VertexID}
 * \sa drawingMode
 * \sa vertexUsagePattern
 */
QVariant QuickGraphicsProgram::vertices() const
{
    Q_D(const QuickGraphicsProgram);

    return d->vertices;
}

void QuickGraphicsProgram::setVertices(QVariant vertices)
{
    Q_D(QuickGraphicsProgram);

    if (d->vertices == vertices)
        return;

    d->vertices = vertices;
    d->verticesDirty = true;
    d->defaultVertices = false;
    emit verticesChanged(vertices);
    update();
}

/*!
 * \qmlproperty enumeration GraphicsProgram::vertexUsagePattern
 *
 * This property holds the geometry's vertex usage pattern. It is passed to \l
 * QSGGeometry::setVertexDataPattern.
 *
 * The default value is UsagePatternDynamic
 *
 * vertexUsagePattern takes the following values:
 *
 * \value UsagePatternAlwaysUpload
 *   The data is always uploaded
 * \value UsagePatternStream
 *   The data is modified for almost every time it is drawn.
 * \value UsagePatternDynamic
 *   The data is modified repeatedly and drawn many times.
 * \value UsagePatternStatic
 *   The data is modified once and drawn many times.
 */
QuickGraphicsProgram::UsagePattern QuickGraphicsProgram::vertexUsagePattern() const
{
    Q_D(const QuickGraphicsProgram);


    return d->vertexUsagePattern;
}


void QuickGraphicsProgram::setVertexUsagePattern(QuickGraphicsProgram::UsagePattern vertexUsagePattern)
{
    Q_D(QuickGraphicsProgram);

    if (d->vertexUsagePattern == vertexUsagePattern)
        return;

    d->vertexUsagePattern = vertexUsagePattern;
    emit vertexUsagePatternChanged(vertexUsagePattern);
    update();
}

/*!
 * \qmlproperty enumeration GraphicsProgram::drawingMode
 *
 * This property holds the geometry's drawing mode.  It specifies how OpenGL
 * should draw the geometry.
 *
 * The default value is DrawingModeTriangleStrip
 *
 * drawingMode takes the following values:
 *
 * \value DrawingModePoints
 *   GL_POINTS
 * \value DrawingModeLines
 *   GL_LINES
 * \value DrawingModeLineLoop
 *   GL_LINE_LOOP
 * \value DrawingModeLineStrip
 *   GL_LINE_STRIP
 * \value DrawingModeTriangles
 *   GL_TRIANGLES
 * \value DrawingModeTriangleStrip
 *   GL_TRIANGLE_STRIP
 * \value DrawingModeTriangleFan
 *   GL_TRIANGLE_FAN
 * \value DrawingModeQuads
 *   GL_QUADS
 * \value DrawingModeQuadStrip
 *   GL_QUAD_STRIP
 * \value DrawingModePolygon
 *   GL_POLYGON
 * \value DrawingModePatches
 *   GL_PATCHES
 *
 * \sa https://www.khronos.org/opengl/wiki/Primitive
 */
QuickGraphicsProgram::DrawingMode QuickGraphicsProgram::drawingMode() const
{
    Q_D(const QuickGraphicsProgram);
    return d->drawingMode;
}

void QuickGraphicsProgram::setDrawingMode(QuickGraphicsProgram::DrawingMode drawingMode)
{
    Q_D(QuickGraphicsProgram);

    if (d->drawingMode == drawingMode)
        return;

    d->drawingMode = drawingMode;
    emit drawingModeChanged(drawingMode);
    update();
}


/*!
 * \qmlproperty enumeration GraphicsProgram::cullFace
 *
 * This property holds the material's cull face. It specifies whether front or
 * back facing facets can be culled. The default value is CullFaceBack.
 *
 * cullFace takes the follwing values:
 * \value CullFaceNone
 *   No face culling, culling is disabled.
 * \value CullFaceBack
 *   Back face culling (GL_BACK).
 * \value CullFaceFront
 *   Front face culling (GL_FRONT).
 * \value CullFaceFrontAndBack
 *   Front and back face culling (GL_FRONT_AND_BACK)
 *
 * \sa frontFace
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glCullFace
 */
QuickGraphicsProgram::CullFace QuickGraphicsProgram::cullFace() const
{
    Q_D(const QuickGraphicsProgram);

    return d->cullFace;
}

void QuickGraphicsProgram::setCullFace(QuickGraphicsProgram::CullFace cullFace)
{
    Q_D(QuickGraphicsProgram);

    if (d->cullFace == cullFace)
        return;

    d->cullFace = cullFace;
    emit cullFaceChanged(cullFace);
}

/*!
 * \qmlproperty enumeration GraphicsProgram::frontFace
 *
 * This property holds the material's front face.  It specifies the orientation
 * of front facing polygons. The default value is FrontFaceCCW.
 *
 * The projection of a polygon to window coordinates is said to have clockwise
 * winding if an imaginary object following the path from its first vertex, its
 * second vertex, and so on, to its last vertex, and finally back to its first
 * vertex, moves in a clockwise direction about the interior of the polygon.
 * The polygon's winding is said to be counterclockwise if the imaginary object
 * following the same path moves in a counterclockwise direction about the
 * interior of the polygon.
 *
 * frontFace takes the following values:
 * \value FrontFaceCW
 *   Selects clockwise polygons as front facing (GL_CW)
 * \value FrontFaceCCW
 *   Selects counterclockwise polygons as front facing (GL_CCW)
 *
 * \sa cullFace
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glFrontFace
 */
QuickGraphicsProgram::FrontFace QuickGraphicsProgram::frontFace() const
{
    Q_D(const QuickGraphicsProgram);

    return d->frontFace;
}

void QuickGraphicsProgram::setFrontFace(QuickGraphicsProgram::FrontFace frontFace)
{
    Q_D(QuickGraphicsProgram);
    if (d->frontFace == frontFace)
        return;

    d->frontFace = frontFace;
    emit frontFaceChanged(frontFace);
}

/*!
 * \qmlproperty enumeration GraphicsProgram::polygonMode
 *
 * This property holds the material's polygon mode.  It specifies the OpenGL mode
 * for polygon rasterization.  The default value is PolygonModeFill.
 *
 * polygonMode takes the following values:
 *
 * \value PolygonModePoint
 *   GL_POINT - Polygon vertices that are marked as the start of a boundary edge are drawn as points.
 * \value PolygonModeLine
 *   GL_LINE - Boundary edges of the polygon are drawn as line segments.
 * \value PolygonModeFill = 0x1B02, // GL_FILL
 *   GL_FILL - The interior of the polygon is filled.
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glPolygonMode
 */
QuickGraphicsProgram::PolygonMode QuickGraphicsProgram::polygonMode() const
{
    Q_D(const QuickGraphicsProgram);
    return d->polygonMode;
}

void QuickGraphicsProgram::setPolygonMode(PolygonMode polygonMode)
{
    Q_D(QuickGraphicsProgram);

    if (d->polygonMode == polygonMode)
        return;

    d->polygonMode = polygonMode;
    emit polygonModeChanged(polygonMode);
}


/*!
 * \qmlproperty bool GraphicsProgram::blend
 *
 * This property holds the material's blend state. Blending enables pixels to
 * be drawn using a function that blends the incoming source values with the
 * existing destination values already in the frame buffer. The default value
 * is false.
 *
 * \sa blendSrcRgb
 * \sa blendDstRgb
 * \sa blendSrcAlpha
 * \sa blendDstAlpha
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glBlendFuncSeparate
 */
bool QuickGraphicsProgram::blend() const
{
    Q_D(const QuickGraphicsProgram);

    return d->blend;
}

void QuickGraphicsProgram::setBlend(bool blend)
{
    Q_D(QuickGraphicsProgram);

    if (d->blend == blend)
        return;

    d->blend = blend;
    emit blendChanged(blend);
    update();
}

/*!
 * \qmlproperty enumeration GraphicsProgram::blendSrcRgb
 *
 * This property holds the material's source RGB blending values. It specifies
 * how the red, green, and blue blending factors are computed. Blending enables
 * pixels to be drawn using a function that blends the incoming source values
 * with the existing destination values already in the frame buffer. The
 * default value is BlendFunctionOne.
 *
 * blendSrcRgb takes the following values:
 *
 * \value BlendFunctionZero
 *   GL_ZERO
 * \value BlendFunctionOne
 *   GL_ONE
 * \value BlendFunctionSrcColor
 *   GL_SRC_COLOR
 * \value BlendFunctionMinusSrcColor
 *   GL_ONE_MINUS_SRC_COLOR
 * \value BlendFunctionDstColor
 *   GL_DST_COLOR
 * \value BlendFunctionOneMinusDstColor
 *   GL_ONE_MINUS_DST_COLOR
 * \value BlendFunctionSrcAlpha
 *   GL_SRC_ALPHA
 * \value BlendFunctionOneMinusSrcAlpha
 *   GL_ONE_MINUS_SRC_ALPHA
 * \value BlendFunctionDstAlpha
 *   GL_DST_ALPHA
 * \value BlendFunctionOneMinusDstAlpha
 *   GL_ONE_MINUS_DST_ALPHA
 * \value BlendFunctionConstandColor
 *   GL_CONSTANT_COLOR
 * \value BlendFunctionOneMinusConstantColor
 *   GL_ONE_MINUS_CONSTANT_COLOR
 * \value BlendFunctionConstantAlpha
 *   GL_CONSTANT_ALPHA
 * \value BlendFunctionOneMinusConstantAlpha
 *   GL_ONE_MINUS_CONSTANT_ALPHA
 * \value BlendFunctionSrcAlphaSaturate
 *   GL_SRC_ALPHA_SATURATE
 * \value BlendFunctionSrc1Color
 *   GL_SRC1_COLOR
 * \value BlendFunctionOneMinusSrc1Color
 *   GL_ONE_MINUS_SRC1_COLOR
 * \value BlendFunctionSrc1Alpha
 *   GL_SRC1_ALPHA
 * \value BlendFunctionOneMinusSrc1Alpha
 *   GL_ONE_MINUS_SRC1_ALPHA
 *
 * \sa blend
 * \sa blendDstRgb
 * \sa blendSrcAlpha
 * \sa blendDstAlpha
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glBlendFuncSeparate
 */
QuickGraphicsProgram::BlendFunction QuickGraphicsProgram::blendSrcRgb() const
{
    Q_D(const QuickGraphicsProgram);

    return d->blendSrcRgb;
}

void QuickGraphicsProgram::setBlendSrcRgb(QuickGraphicsProgram::BlendFunction blendSrcRgb)
{
    Q_D(QuickGraphicsProgram);

    if (d->blendSrcRgb == blendSrcRgb)
        return;

    d->blendSrcRgb = blendSrcRgb;
    emit blendSrcRgbChanged(blendSrcRgb);
}


/*!
 * \qmlproperty enumeration GraphicsProgram::blendDstRgb
 *
 * This property holds the material's destination RGB blending values. It
 * specifies how the red, green, and blue blending factors are computed.
 * Blending enables pixels to be drawn using a function that blends the
 * incoming source values with the existing destination values already in the
 * frame buffer. The default value is BlendFunctionOneMinusSrcAlpha.
 *
 * blendDstRgb takes the following values:
 *
 * \value BlendFunctionZero
 *   GL_ZERO
 * \value BlendFunctionOne
 *   GL_ONE
 * \value BlendFunctionSrcColor
 *   GL_SRC_COLOR
 * \value BlendFunctionMinusSrcColor
 *   GL_ONE_MINUS_SRC_COLOR
 * \value BlendFunctionDstColor
 *   GL_DST_COLOR
 * \value BlendFunctionOneMinusDstColor
 *   GL_ONE_MINUS_DST_COLOR
 * \value BlendFunctionSrcAlpha
 *   GL_SRC_ALPHA,
 * \value BlendFunctionOneMinusSrcAlpha
 *   GL_ONE_MINUS_SRC_ALPHA
 * \value BlendFunctionDstAlpha
 *   GL_DST_ALPHA,
 * \value BlendFunctionOneMinusDstAlpha
 *   GL_ONE_MINUS_DST_ALPHA
 * \value BlendFunctionConstandColor
 *   GL_CONSTANT_COLOR
 * \value BlendFunctionOneMinusConstantColor
 *   GL_ONE_MINUS_CONSTANT_COLOR
 * \value BlendFunctionConstantAlpha
 *   GL_CONSTANT_ALPHA
 * \value BlendFunctionOneMinusConstantAlpha
 *   GL_ONE_MINUS_CONSTANT_ALPHA
 * \value BlendFunctionSrcAlphaSaturate
 *   GL_SRC_ALPHA_SATURATE
 * \value BlendFunctionSrc1Color
 *   GL_SRC1_COLOR
 * \value BlendFunctionOneMinusSrc1Color
 *   GL_ONE_MINUS_SRC1_COLOR
 * \value BlendFunctionSrc1Alpha
 *   GL_SRC1_ALPHA
 * \value BlendFunctionOneMinusSrc1Alpha
 *   GL_ONE_MINUS_SRC1_ALPHA
 *
 * \sa blend
 * \sa blendSrcRgb
 * \sa blendSrcAlpha
 * \sa blendDstAlpha
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glBlendFuncSeparate
 */
QuickGraphicsProgram::BlendFunction QuickGraphicsProgram::blendDstRgb() const
{
    Q_D(const QuickGraphicsProgram);

    return d->blendDstRgb;
}

void QuickGraphicsProgram::setBlendDstRgb(QuickGraphicsProgram::BlendFunction blendDstRgb)
{
    Q_D(QuickGraphicsProgram);

    if (d->blendDstRgb == blendDstRgb)
        return;

    d->blendDstRgb = blendDstRgb;
    emit blendDstRgbChanged(blendDstRgb);
}

/*!
 * \qmlproperty enumeration GraphicsProgram::blendSrcAlpha
 *
 * This property holds the material's source alpha blending values. It
 * specifies how the red, green, and blue blending factors are computed.
 * Blending enables pixels to be drawn using a function that blends the
 * incoming source values with the existing destination values already in the
 * frame buffer. The default value is BlendFunctionOne.
 *
 * blendSrcAlpha takes the following values:
 *
 * \value BlendFunctionZero
 *   GL_ZERO
 * \value BlendFunctionOne
 *   GL_ONE
 * \value BlendFunctionSrcColor
 *   GL_SRC_COLOR
 * \value BlendFunctionMinusSrcColor
 *   GL_ONE_MINUS_SRC_COLOR
 * \value BlendFunctionDstColor
 *   GL_DST_COLOR
 * \value BlendFunctionOneMinusDstColor
 *   GL_ONE_MINUS_DST_COLOR
 * \value BlendFunctionSrcAlpha
 *   GL_SRC_ALPHA
 * \value BlendFunctionOneMinusSrcAlpha
 *   GL_ONE_MINUS_SRC_ALPHA
 * \value BlendFunctionDstAlpha
 *   GL_DST_ALPHA
 * \value BlendFunctionOneMinusDstAlpha
 *   GL_ONE_MINUS_DST_ALPHA
 * \value BlendFunctionConstandColor
 *   GL_CONSTANT_COLOR
 * \value BlendFunctionOneMinusConstantColor
 *   GL_ONE_MINUS_CONSTANT_COLOR
 * \value BlendFunctionConstantAlpha
 *   GL_CONSTANT_ALPHA
 * \value BlendFunctionOneMinusConstantAlpha
 *   GL_ONE_MINUS_CONSTANT_ALPHA
 * \value BlendFunctionSrcAlphaSaturate
 *   GL_SRC_ALPHA_SATURATE
 * \value BlendFunctionSrc1Color
 *   GL_SRC1_COLOR
 * \value BlendFunctionOneMinusSrc1Color
 *   GL_ONE_MINUS_SRC1_COLOR
 * \value BlendFunctionSrc1Alpha
 *   GL_SRC1_ALPHA
 * \value BlendFunctionOneMinusSrc1Alpha
 *   GL_ONE_MINUS_SRC1_ALPHA
 *
 * \sa blend
 * \sa blendSrcRgb
 * \sa blendDstRgb
 * \sa blendDstAlpha
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glBlendFuncSeparate
 */
QuickGraphicsProgram::BlendFunction QuickGraphicsProgram::blendSrcAlpha() const
{
    Q_D(const QuickGraphicsProgram);

    return d->blendSrcAlpha;
}

void QuickGraphicsProgram::setBlendSrcAlpha(QuickGraphicsProgram::BlendFunction blendSrcAlpha)
{
    Q_D(QuickGraphicsProgram);

    if (d->blendSrcAlpha == blendSrcAlpha)
        return;

    d->blendSrcAlpha = blendSrcAlpha;
    emit blendSrcAlphaChanged(blendSrcAlpha);
}

/*!
 * \qmlproperty enumeration GraphicsProgram::blendDstAlpha
 *
 * This property holds the material's destination alpha blending values. It
 * specifies how the red, green, and blue blending factors are computed.
 * Blending enables pixels to be drawn using a function that blends the
 * incoming source values with the existing destination values already in the
 * frame buffer. The default value is BlendFunctionOneMinusSrcAlpha.
 *
 * blendDstAlpha takes the following values:
 *
 * \value BlendFunctionZero
 *   GL_ZERO
 * \value BlendFunctionOne
 *   GL_ONE
 * \value BlendFunctionSrcColor
 *   GL_SRC_COLOR
 * \value BlendFunctionMinusSrcColor
 *   GL_ONE_MINUS_SRC_COLOR
 * \value BlendFunctionDstColor
 *   GL_DST_COLOR
 * \value BlendFunctionOneMinusDstColor
 *   GL_ONE_MINUS_DST_COLOR
 * \value BlendFunctionSrcAlpha
 *   GL_SRC_ALPHA
 * \value BlendFunctionOneMinusSrcAlpha
 *   GL_ONE_MINUS_SRC_ALPHA
 * \value BlendFunctionDstAlpha
 *   GL_DST_ALPHA
 * \value BlendFunctionOneMinusDstAlpha
 *   GL_ONE_MINUS_DST_ALPHA
 * \value BlendFunctionConstandColor
 *   GL_CONSTANT_COLOR
 * \value BlendFunctionOneMinusConstantColor
 *   GL_ONE_MINUS_CONSTANT_COLOR
 * \value BlendFunctionConstantAlpha
 *   GL_CONSTANT_ALPHA
 * \value BlendFunctionOneMinusConstantAlpha
 *   GL_ONE_MINUS_CONSTANT_ALPHA
 * \value BlendFunctionSrcAlphaSaturate
 *   GL_SRC_ALPHA_SATURATE
 * \value BlendFunctionSrc1Color
 *   GL_SRC1_COLOR
 * \value BlendFunctionOneMinusSrc1Color
 *   GL_ONE_MINUS_SRC1_COLOR
 * \value BlendFunctionSrc1Alpha
 *   GL_SRC1_ALPHA
 * \value BlendFunctionOneMinusSrc1Alpha
 *   GL_ONE_MINUS_SRC1_ALPHA
 *
 * \sa blend
 * \sa blendSrcRgb
 * \sa blendDstRgb
 * \sa blendDstAlpha
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glBlendFuncSeparate
 */
QuickGraphicsProgram::BlendFunction QuickGraphicsProgram::blendDstAlpha() const
{
    Q_D(const QuickGraphicsProgram);

    return d->blendDstAlpha;
}

void QuickGraphicsProgram::setBlendDstAlpha(QuickGraphicsProgram::BlendFunction blendDstAlpha)
{
    Q_D(QuickGraphicsProgram);

    if (d->blendDstAlpha == blendDstAlpha)
        return;

    d->blendDstAlpha = blendDstAlpha;
    emit blendDstAlphaChanged(blendDstAlpha);
}

/*!
 * \qmlproperty enumeration GraphicsProgram::logicOp
 *
 * This property holds the material's OpenGL logical operation mode. It
 * specifies a logical operation that, when enabled, is applied between the
 * incoming RGBA color and the RGBA color at the corresponding location in the
 * frame buffer.  The default value is LogicOpNone.
 *
 * logicOp takes the following values:
 *
 * \value LogicOpNone
 *   Disabled.
 * \value LogicOpClear
 *   GL_CLEAR
 * \value LogicOpSet
 *   GL_SET
 * \value LogicOpCopy
 *   GL_COPY
 * \value LogicOpCopyInverted
 *   GL_COPY_INVERTED
 * \value LogicOpNoOp
 *   GL_NOOP
 * \value LogicOpInvert
 *   GL_INVERT
 * \value LogicOpAnd
 *   GL_AND
 * \value LogicOpNand
 *   GL_NAND
 * \value LogicOpOr
 *   GL_OR
 * \value LogicOpNor
 *   GL_NOR
 * \value LogicOpXor
 *   GL_XOR
 * \value LogicOpEquiv
 *   GL_EQUIV
 * \value LogicOpAndReverse
 *   GL_AND_REVERSE
 * \value LogicOpAndInverted
 *   GL_AND_INVERTED
 * \value LogicOpOrReverse
 *   GL_OR_REVERSE
 * \value LogicOpOrInverted
 *   GL_OR_INVERTED
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glLogicOp
 */
QuickGraphicsProgram::LogicOp QuickGraphicsProgram::logicOp() const
{
    Q_D(const QuickGraphicsProgram);

    return d->logicOp;
}

void QuickGraphicsProgram::setLogicOp(QuickGraphicsProgram::LogicOp logicOp)
{
    Q_D(QuickGraphicsProgram);

    if (d->logicOp == logicOp)
        return;

    d->logicOp = logicOp;
    emit logicOpChanged(logicOp);
}

/*!
 * \qmlproperty enumeration GraphicsProgram::memoryBarrier
 *
 * This property holds the material's OpenGL memory barrier state. It defines a
 * barrier ordering the memory transactions issued prior to the command
 * relative to those issued after the barrier. For the purposes of this
 * ordering, memory transactions performed by shaders are considered to be
 * issued by the rendering command that triggered the execution of the shader.
 * memoryBarrier is a bitfield indicating the set of operations that are
 * synchronized with shader stores.
 *
 * memoryBarrier represents how the data written from an image load/store
 * shader operation is intended to be used. If an image is written to in a
 * rendering command, then read from as a texture, BarrierTextureFetch should
 * be used. If instead the image is attached to an FBO and blending is used to
 * do a read/modify/write operation, then BarrierFramebuffer should be used.
 *
 * The memory barrier is inserted prior to insertion of GraphicsProgram's
 * render command. The default value is BarrierNone.
 *
 * memoryBarrier is a bitwise combination of:
 *
 * \value BarrierNone
 *   No memory barrier.
 * \value BarrierVertexAttribArray
 *   GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT
 * \value BarrierElementArray
 *   GL_ELEMENT_ARRAY_BARRIER_BIT
 * \value BarrierUniform
 *   GL_UNIFORM_BARRIER_BIT
 * \value BarrierTextureFetch
 *   GL_TEXTURE_FETCH_BARRIER_BIT
 * \value BarrierShaderImageAccess
 *   GL_SHADER_IMAGE_ACCESS_BARRIER_BIT
 * \value BarrierCommand
 *   GL_COMMAND_BARRIER_BIT,
 * \value BarrierPixelBuffer
 *   GL_PIXEL_BUFFER_BARRIER_BIT
 * \value BarrierTextureUpdate
 *   GL_TEXTURE_UPDATE_BARRIER_BIT
 * \value BarrierBufferUpdate
 *   GL_BUFFER_UPDATE_BARRIER_BIT
 * \value BarrierFramebuffer
 *   GL_FRAMEBUFFER_BARRIER_BIT
 * \value BarrierTransformFeedback
 *   GL_TRANSFORM_FEEDBACK_BARRIER_BIT
 * \value BarrierAtomicCounter
 *   GL_ATOMIC_COUNTER_BARRIER_BIT
 * \value BarrierShaderStorage
 *   GL_SHADER_STORAGE_BARRIER_BIT
 * \value BarrierClientMappedBuffer
 *   GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT
 * \value BarrierQueryBuffer
 *   GL_QUERY_BUFFER_BARRIER_BIT
 * \value BarrierAll
 *   GL_ALL_BARRIER_BITS
 *
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glMemoryBarrier
 */
QuickGraphicsProgram::Barrier QuickGraphicsProgram::memoryBarrier() const
{
    Q_D(const QuickGraphicsProgram);

    return d->memoryBarrier;
}

/*!
 * \qmlproperty object GraphicsProgram::attributes
 *
 * This property holds introspected vertex attribute information.
 *
 * It is structured as an object tree containing the following properties
 * for each attribute:
 *
 * \value arraySize
 *   Number of items in array
 * \value location
 *   Attribute binding location
 * \value name
 *   Attribute name
 * \value size
 *   Attribute size in bytes
 * \value type
 *   Attribute type
 *
 * The attribute information for a GLSL attribute named myAttributeName may be
 * accessed as:
 *
 * \qml
 * var arraySize = attributes.myAttributeName.arraySize
 * var location = attributes.myAttributeName.location
 * \endqml
 *
 * The attributes may be printed as a string via:
 *
 * \qml
 * console.log(JSON.stringify(attributes))
 * \endqml
 */
QVariantMap QuickGraphicsProgram::attributes() const
{
    Q_D(const QuickGraphicsProgram);
    return d->qmlAttributes;
}

/*!
 * \qmlproperty object GraphicsProgram::uniforms
 *
 * This property holds introspected uniform information.
 *
 * It is structured as an object tree containing the following properties for each uniform:
 *
 * \value arraySize
 *   Number of items in array
 * \value arrayStride
 *   Stride in bytes between array elements
 * \value atomicCounterBufferIndex
 *   Atomic counter buffer index
 * \value isRowMajor
 *   Row major format flag
 * \value location
 *   Uniform location
 * \value matrixStride
 *   Matrix stride
 * \value name
 *  Name
 *  \value offset
 *   Offset
 * \value type
 *   Type
 *
 *
 * The uniform information for a GLSL uniform named myUniformName may be
 * accessed as:
 *
 * \qml
 * var arraySize = uniforms.myUniformName.arraySize
 * var location = uniforms.myUniformName.location
 * \endqml
 *
 * The uniforms may be printed as a string via:
 *
 * \qml
 * console.log(JSON.stringify(uniforms))
 * \endqml
 *
 */
QVariantMap QuickGraphicsProgram::uniforms() const
{
    Q_D(const QuickGraphicsProgram);
    return d->qmlUniforms;
}

/*!
 * \qmlproperty object GraphicsProgram::subroutineUniforms
 *
 * This property holds introspected subroutine uniform information.
 *
 * It is structured as an object tree containing each shader stage:
 *
 * \value GL_FRAGMENT_SHADER
 * Fragment shader
 * \value GL_GEOMETRY_SHADER
 * Geometry shader
 * \value GL_TESS_CONTROL_SHADER
 * Tessellation control shader
 * \value GL_TESS_EVALUATION_SHADER
 * Tessellation evaluation shader
 * \value GL_VERTEX_SHADER
 * Vertex shader
 *
 * The object tree under each shader stage contains the following properties for each subroutine
 * uniform:
 *
 * \value compatibleSubroutines
 *  An object tree of compatible subroutines
 * \value location
 *  The subroutine location
 * \value name
 *  The subroutine name
 *
 * The object tree under each compatible subroutine contains the following properties for each
 * subroutine:
 *
 * \value index
 *  The subroutine index
 * \value name
 *  The subroutine name
 *
 * The subroutine uniform information for a GLSL vertex shader subroutine named mySubroutineName
 * may be accessed as:
 *
 * \qml
 * var compatibleSubs = subroutineUniforms.GL_VERTEX_SHADER.mySubroutineName.compatibleSubroutines
 * \endqml
 *
 * The subroutine uniforms may be printed as a string via:
 *
 * \qml
 * console.log(JSON.stringify(subroutineUniforms))
 * \endqml
 */
QVariantMap QuickGraphicsProgram::subroutineUniforms() const
{
    Q_D(const QuickGraphicsProgram);
    return d->qmlSubroutineUniforms;
}

/*!
 * \qmlproperty object GraphicsProgram::uniformBlocks
 *
 * This property holds introspected uniform block information.
 *
 * It is structured as an object tree containing the following properties for each uniform block:
 *
 * \value binding
 * The binding
 * \value index
 * The index
 * \value name
 * The block name
 * \value size
 * The block size in bytes
 * \value uniforms
 * An object tree of the uniforms within the block
 *
 * The uniforms object is structured as an object tree containing the following properties for each
 * uniform within the block:
 *
 * \value arraySize
 * The number of elements in the array
 * \value arrayStride
 * The stride between array elements in bytes
 * \value atomicCounterBufferIndex
 * The atomic counter bufffer index
 * \value blockIndex
 * The block index
 * \value isRowMajor
 * The row major ordering flag
 * \value location
 * The location
 * \value matrixStride
 * The matrix stride
 * \value name
 * The uniform name
 * \value offset
 * The uniform offset in bytes
 * \value type
 * The uniform type
 *
 * The uniform block information for a GLSL uniform block named myUniformBlock with uniform
 * myUniform[0] may be accessed as:
 *
 * \qml
 * var size = uniformBlocks.myUniformBlock.size
 * var arraySize = uniformBlocks.myUniformBlock.uniforms["myUniform[0]"].arraySize
 * \endqml
 *
 * The uniform blocks may be printed as a string via:
 *
 * \qml
 * console.log(JSON.stringify(uniformBlocks))
 * \endqml
 */
QVariantMap QuickGraphicsProgram::uniformBlocks() const
{
    Q_D(const QuickGraphicsProgram);
    return d->qmlUniformBlocks;
}

/*!
 * \qmlproperty object GraphicsProgram::shaderStorageBlocks
 *
 * This property holds introspected shader storage block information.
 *
 * It is structured as an object tree containing the following properties for each shader storage
 * block:
 *
 * \value binding
 * The binding
 * \value index
 * The index
 * \value name
 * The block name
 * \value size
 * The base block size
 * \value variables
 * An object tree of the variables within the block
 *
 * The variables object is structured as an object tree containing the following properties for
 * each variable within the block:
 *
 * \value arraySize
 * The number of elements in the array
 * \value arrayStride
 * The stride in bytes between array elements
 * \value blockIndex
 * The block index
 * \value isRowMajor
 * The row major ordering flag
 * \value matrixStride
 * The matrix stride
 * \value name
 * The variable name
 * \value offset
 * The variable offset in bytes
 * \value topLevelArraySize
 * The number of top level array elements
 * \value topLevelArrayStride
 * The stride in bytes between top level array elements
 * \value type
 * The variable type
 *
 * The shader storage block information for a GLSL shader storage block named myShaderStorageBlock
 * with variable myVariable[0] may be accessed as:
 *
 * \qml
 * var size = shaderStorageBlocks.myShaderStorageBlock.size
 * var arraySize = shaderStorageBlocks.myShaderStorageBlock.variables["myVariable[0]"].arraySize
 * \endqml
 *
 * The shader storage blocks may be printed as a string via:
 *
 * \qml
 * console.log(JSON.stringify(shaderStorageBlocks)
 * \endqml
 */
QVariantMap QuickGraphicsProgram::shaderStorageBlocks() const
{
    Q_D(const QuickGraphicsProgram);
    return d->qmlShaderStorageBlocks;
}

void QuickGraphicsProgram::setMemoryBarrier(QuickGraphicsProgram::Barrier memoryBarrier)
{
    Q_D(QuickGraphicsProgram);

    if (d->memoryBarrier == memoryBarrier)
        return;

    d->memoryBarrier = memoryBarrier;
    emit memoryBarrierChanged(memoryBarrier);
}

/*!
 * \qmlproperty bool GraphicsProgram::lineSmooth
 *
 * This property holds the material's OpenGL line smoothing state. If enabled,
 * lines are drawn with correct filtering. The default value is false.
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glLineWidth
 */
bool QuickGraphicsProgram::lineSmooth() const
{
    Q_D(const QuickGraphicsProgram);

    return d->lineSmooth;
}

void QuickGraphicsProgram::setLineSmooth(bool lineSmoothing)
{
    Q_D(QuickGraphicsProgram);

    if (d->lineSmooth == lineSmoothing)
        return;

    d->lineSmooth = lineSmoothing;
    emit lineSmoothChanged(lineSmoothing);
}

/*!
 * \qmlproperty real GraphicsProgram::lineWidth
 *
 * This property holds the geometry's line width.  The default value is 1.0.
 *
 * The OpenGL implementation's line width range may be determined via the
 * attached property \l OpenGLProperties.
 *
 * \sa OpenGLProperties::glLineWidthRange
 * \sa OpenGLProperties::glAliasedLineWidthRange
 * \sa OpenGLProperties::glSmoothLineWidthRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glLineWidth
 */
double QuickGraphicsProgram::lineWidth() const
{
    Q_D(const QuickGraphicsProgram);

    return d->lineWidth;
}

void QuickGraphicsProgram::setLineWidth(double lineWidth)
{
    Q_D(QuickGraphicsProgram);

    if (d->lineWidth == lineWidth)
        return;

    d->lineWidth = lineWidth;
    emit lineWidthChanged(lineWidth);
    update();
}

/*!
 * \qmlproperty bool GraphicsProgram::programPointSize
 *
 * This property holds the geometry's OpenGL program point size state. If
 * enabled and a vertex or geometry shader is active, then the derived point
 * size is taken from the (potentially clipped) shader builtin gl_PointSize and
 * clamped to the implementation-dependent point size range. The default value
 * is false.
 *
 * \sa OpenGLProperties::glPointSizeRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glPointSize
 */
bool QuickGraphicsProgram::programPointSize() const
{
    Q_D(const QuickGraphicsProgram);

    return d->programPointSize;
}


void QuickGraphicsProgram::setProgramPointSize(bool programPointSize)
{
    Q_D(QuickGraphicsProgram);

    if (d->programPointSize == programPointSize)
        return;

    d->programPointSize = programPointSize;
    emit programPointSizeChanged(programPointSize);
}

/*!
 * \qmlproperty enumeration GraphicsProgram::pointSpriteCoordOrigin
 *
 * This property holds the material's OpenGL point sprite coordinate origin.
 * Within a fragment shader, the OpenGL built-in input vec2 gl_PointCoord
 * provides coordinates within the point primitive on the range [0, 1]. The
 * location of (0, 0) depends on pointSpriteCoordOrigin. The default is
 * CoordOriginLowerLeft.
 *
 * pointSpriteCoordOrigin takes the following values:
 *
 * \value CoordOriginLowerLeft
 *   GL_LOWER_LEFT, (0, 0) is the bottom-left coordinate.
 * \value CoordOriginUpperLeft
 *   GL_UPPER_LEFT, (0, 0) is the top-left coordinate.
 *
 * \sa https://www.khronos.org/opengl/wiki/Primitive#Point_primitives
 */
QuickGraphicsProgram::CoordOrigin QuickGraphicsProgram::pointSpriteCoordOrigin() const
{
    Q_D(const QuickGraphicsProgram);

    return d->pointSpriteCoordOrigin;
}

void QuickGraphicsProgram::setPointSpriteCoordOrigin(QuickGraphicsProgram::CoordOrigin pointSpriteCoordOrigin)
{
    Q_D(QuickGraphicsProgram);

    if (d->pointSpriteCoordOrigin == pointSpriteCoordOrigin)
        return;

    d->pointSpriteCoordOrigin = pointSpriteCoordOrigin;
    emit pointSpriteCoordOriginChanged(pointSpriteCoordOrigin);
}

/*!
 * \qmlproperty real GraphicsProgram::pointFadeThresholdSize
 *
 * This property holds the material's OpenGL point fade threshold size. It
 * specifies the threshold value to which point sizes are clamped if they
 * exceed the specified value. The default value is 1.0.
 *
 * \sa OpenGLProperties::glPointSizeRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glPointParameter
 */
double QuickGraphicsProgram::pointFadeThresholdSize() const
{
    Q_D(const QuickGraphicsProgram);

    return d->pointFadeThresholdSize;
}

void QuickGraphicsProgram::setPointFadeThresholdSize(double pointFadeThresholdSize)
{
    Q_D(QuickGraphicsProgram);

    if (d->pointFadeThresholdSize == pointFadeThresholdSize)
        return;

    d->pointFadeThresholdSize = pointFadeThresholdSize;
    emit pointFadeThresholdSizeChanged(pointFadeThresholdSize);
}

/*!
 * \qmlproperty real GraphicsProgram::pointSize
 *
 * This property holds the material's OpenGL point size. It specifies the
 * rasterized diameter of points. If \l programPointSize is enabled, this
 * value will be ignored. The default value is 1.0.
 *
 * pointSize must fall on the OpenGL implementation dependent range specified in
 * \l{OpenGLProperties::glPointSizeRange}.
 *
 * \sa OpenGLProperties::glPointSizeRange
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glPointSize
 */
double QuickGraphicsProgram::pointSize() const
{
    Q_D(const QuickGraphicsProgram);

    return d->pointSize;
}

void QuickGraphicsProgram::setPointSize(double pointSize)
{
    Q_D(QuickGraphicsProgram);

    if (d->pointSize == pointSize)
        return;

    d->pointSize = pointSize;
    emit pointSizeChanged(pointSize);
    update();
}

/*!
 * \qmlproperty int GraphicsProgram::patchVertices
 *
 * This property holds the geometry's patch vertices parameter. It specifies
 * the number of vertices which will be used to make up a single patch
 * primitive. Patch primitives are consumed by the tessellation control shader
 * (if present) and subsequently used for tessellation. When \l drawingMode is
 * DrawingModePatches each patch will be made up of patchVertices vertices
 * taken from the\l vertices property.
 *
 * patchVertices must be in the range (0,
 * \l{OpenGLProperties::glMaxPatchVertices}].
 *
 * \sa OpenGLProperties::glMaxPatchVertices
 * \sa https://www.khronos.org/opengl/wiki/Tessellation
 *
 */
int QuickGraphicsProgram::patchVertices() const
{
    Q_D(const QuickGraphicsProgram);

    return d->patchVertices;
}

void QuickGraphicsProgram::setPatchVertices(int patchVertices)
{
    Q_D(QuickGraphicsProgram);

    if (d->patchVertices == patchVertices)
            return;

    d->patchVertices = patchVertices;
    emit patchVerticesChanged(patchVertices);
}

/*!
 * \qmlproperty list<real> GraphicsProgram::patchDefaultOuterLevel
 *
 * This property holds the geometry's patch default outer levels. It specifies
 * the default four outer tessellation levels, and is used if no tessellation
 * control shader is present.
 *
 * patchDefaultOuterLevel must contain four values in the range (0,
 * \l{OpenGLProperties::glMaxTessGenLevel}].
 *
 * \sa https://www.khronos.org/opengl/wiki/Tessellation
 */
QList<double> QuickGraphicsProgram::patchDefaultOuterLevel() const
{
    Q_D(const QuickGraphicsProgram);

    return d->patchDefaultOuterLevel;
}

void QuickGraphicsProgram::setPatchDefaultOuterLevel(QList<double> patchDefaultOuterLevel)
{
    Q_D(QuickGraphicsProgram);

    if (d->patchDefaultOuterLevel == patchDefaultOuterLevel)
            return;

        d->patchDefaultOuterLevel = patchDefaultOuterLevel;
        emit patchDefaultOuterLevelChanged(patchDefaultOuterLevel);
}

/*!
 * \qmlproperty list<real> GraphicsProgram::patchDefaultInnerLevel
 *
 * This property holds the geometry's patch default inner levels.  It specifies
 * the default two inner tessellation levels, and is used if no tessellation
 * control shader is present.
 *
 * patchDefaultInnerLevel must contain four values in the range (0,
 * \l{OpenGLProperties::glMaxTessGenLevel}].
 *
 * \sa https://www.khronos.org/opengl/wiki/Tessellation
 */
QList<double> QuickGraphicsProgram::patchDefaultInnerLevel() const
{
    Q_D(const QuickGraphicsProgram);

    return d->patchDefaultInnerLevel;
}

void QuickGraphicsProgram::setPatchDefaultInnerLevel(QList<double> patchDefaultInnerLevel)
{
    Q_D(QuickGraphicsProgram);

    if (d->patchDefaultInnerLevel == patchDefaultInnerLevel)
            return;

        d->patchDefaultInnerLevel = patchDefaultInnerLevel;
        emit patchDefaultInnerLevelChanged(patchDefaultInnerLevel);
}

QSGNode *QuickGraphicsProgram::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
{
    Q_D(QuickGraphicsProgram);

    if(!d->initialized)
    {
        d->initialize();
    }

#ifdef QT_DEBUG
    QScopedPointer<QElapsedTimer> elapsedTimer;
    QList<QPair<QString, qint64>> debugEvents;

    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        elapsedTimer.reset(new QElapsedTimer);
        elapsedTimer->start();
    }
#endif

    // Create a new node if necessary
    QSGGeometryNode *node = static_cast<QSGGeometryNode *>(oldNode);
    if(!node)
    {
        node = new QSGGeometryNode;
        node->setFlag(QSGNode::OwnsGeometry);
        node->setFlag(QSGNode::OwnsMaterial);

        QuickGraphicsProgramMaterial *material = new QuickGraphicsProgramMaterial;

        // add ourselves as the QmlItem property to our property table - used to provide context to debug messages
        material->properties["QmlItem"] = QVariant::fromValue(this);
        material->properties["QQuickWindow"] = QVariant::fromValue(window());

        // set the material attributes
        material->attributes = d->attributes;

        node->setMaterial(material);

#ifdef QT_DEBUG
        if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
            debugEvents.append(qMakePair(QString("Node Creation"), elapsedTimer->nsecsElapsed()));
#endif

    }

    // Get the geometry and material
    QSGGeometry *geometry = node->geometry();
    QuickGraphicsProgramMaterial *material = static_cast<QuickGraphicsProgramMaterial *>(node->material());


    // If the geometry requires updating
    if(!geometry||d->verticesDirty)
    {
        // update the geometry
        geometry = d->updateGeometry();

        // if the updated geometry exists
        if(geometry)
        {
            geometry->setDrawingMode(GLenum(d->drawingMode));
            node->setGeometry(geometry);
            node->markDirty(QSGNode::DirtyGeometry);
        }
        else
        {
            qWarning() << Q_FUNC_INFO << this << "cannot handle geometry";
        }
        d->verticesDirty = false;

#ifdef QT_DEBUG
        if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
            debugEvents.append(qMakePair(QString("Geometry Creation"), elapsedTimer->nsecsElapsed()));
#endif
    }


    if(geometry)
    {
        // update the vertex usage pattern if necessary
        if(geometry->vertexDataPattern()!=QSGGeometry::DataPattern(d->vertexUsagePattern))
        {
            geometry->setVertexDataPattern(QSGGeometry::DataPattern(d->vertexUsagePattern));
            node->markDirty(QSGNode::DirtyGeometry);
        }
        // set the scene graph point/line width
        float lineWidth = d->drawingMode==DrawingMode::DrawingModePoints?float(d->pointSize):float(d->lineWidth);
        if(geometry->lineWidth()!=lineWidth)
        {
            geometry->setLineWidth(lineWidth);
            node->markDirty(QSGNode::DirtyGeometry);
        }
    }


    if(material)
    {

        // set the blending flag if necessary
        if (d->blend!=bool(material->flags()&QSGMaterial::Blending))
        {
            material->setFlag(QSGMaterial::Blending, d->blend);
        }


        // add our updated QMetaObject properties to our property list
        for(int index: d->updatedPropertyIndices)
        {
            QMetaProperty property = metaObject()->property(index);
            QVariant variant = Introspection::conditionVariant(property.read(this), this, true, window());
            material->properties[property.name()] = variant;
            if(d->baseUniforms.contains(property.name()))
            {
                for(const Introspection::Uniform &uniform: d->baseUniforms[property.name()])
                {
                    material->properties[uniform.name] = Introspection::conditionProperty(variant, uniform.name, uniform.type, uniform.arraySize, "uniform", this);
                }
            }
            else if(d->uniformBlocks.contains(property.name()))
            {
                if(variant.canConvert<QuickSGUniformBufferProvider *>())
                {
                    QuickSGUniformBufferProvider *provider = variant.value<QuickSGUniformBufferProvider *>();
                    Q_ASSERT(provider);
                    bool ok = provider->setUniformBlock(d->uniformBlocks[property.name()]);

                    if(!ok)
                    {
                        qWarning()<< "QuickOpenGL:" << static_cast<const QObject *>(this) << "uniform block property" << QString(property.name())  << ":" << variant << "has inconsistent OpenGL uniform block definitions";
                    }
                }
                else
                {
                    qWarning()<< "QuickOpenGL:" << static_cast<const QObject *>(this) << "uniform block property" << QString(property.name())  << ":" << variant << "cannot be converted to \"QuickSGUniformBufferProvider *\"";
                }
            }
            else if(d->shaderStorageBlocks.contains(property.name()))
            {
                if(variant.canConvert<QuickSGSSBOProvider *>())
                {
                    QuickSGSSBOProvider *provider = variant.value<QuickSGSSBOProvider *>();
                    bool ok = provider->setShaderStorageBlock(d->shaderStorageBlocks[property.name()]);

                    if(!ok)
                    {
                        qWarning()<< "QuickOpenGL:" << static_cast<const QObject *>(this) << "shader storage block property" << QString(property.name())  << ":" << variant << "has inconsistent OpenGL shader storage block definitions";
                    }

                }
                else
                {
                    qWarning()<< "QuickOpenGL:" << static_cast<const QObject *>(this) << "shader storage block property" << QString(property.name())  << ":" << variant << "cannot be converted to \"QuickSGSSBOProvider *\"";
                }

            }
        }

#ifdef QT_DEBUG
        if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
        {
            QString string("Material Update [");
            QTextStream stream(&string);

            for(int i=0;i<d->updatedPropertyIndices.count();++i)
            {
                stream << metaObject()->property(d->updatedPropertyIndices.at(i)).name() << (i==d->updatedPropertyIndices.count()-1?"":", ");
            }
            stream << "]";
            debugEvents.append(qMakePair(string, elapsedTimer->nsecsElapsed()));
        }
#endif
        d->updatedPropertyIndices.clear();

        // update has been called for a reason - mark the material as dirty
        node->markDirty(QSGNode::DirtyMaterial);
    }

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QString string;
        QTextStream stream(&string);

        for(int i=0;i<debugEvents.count();++i)
        {
            stream << debugEvents[i].first << ": ";
            stream << double(i?debugEvents[i].second-debugEvents[i-1].second:debugEvents[i].second)*1e-6 << "ms" << (i==debugEvents.count()-1?"":", ");
        }
        qDebug().nospace() << "quickopengl.scenegraph.time.graphicsprogram.updatepaintnode: " << reinterpret_cast<QObject *>(this) << " took " << double(elapsedTimer->nsecsElapsed())*1e-6 << "ms (" << qPrintable(string) << ")";
    }
#endif

    return node;

}

void QuickGraphicsProgram::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    Q_D(QuickGraphicsProgram);

    Q_UNUSED(oldGeometry);

    // if we are using the default vertices
    if(d->defaultVertices)
    {
        QVariantList vertices;

        // update the default vertices to be a rectangle covering our display space
        vertices.append(QVariantMap{std::pair<QString, QVector4D>(QStringLiteral("qt_Vertex"), QVector4D(0, 0, 0, 1)), std::pair<QString, QVector2D>(QStringLiteral("qt_MultiTexCoord0"), QVector2D(0,0))});
        vertices.append(QVariantMap{std::pair<QString, QVector4D>(QStringLiteral("qt_Vertex"), QVector4D(0, float(newGeometry.height()), 0, 1)), std::pair<QString, QVector2D>(QStringLiteral("qt_MultiTexCoord0"), QVector2D(0,1))});
        vertices.append(QVariantMap{std::pair<QString, QVector4D>(QStringLiteral("qt_Vertex"), QVector4D(float(newGeometry.width()), 0, 0, 1)), std::pair<QString, QVector2D>(QStringLiteral("qt_MultiTexCoord0"), QVector2D(1,0))});
        vertices.append(QVariantMap{std::pair<QString, QVector4D>(QStringLiteral("qt_Vertex"), QVector4D(float(newGeometry.width()), float(newGeometry.height()), 0, 1)), std::pair<QString, QVector2D>(QStringLiteral("qt_MultiTexCoord0"), QVector2D(1,1))});
        d->vertices=vertices;
        emit verticesChanged(d->vertices);
        d->verticesDirty = true;
        update();
    }

    QQuickItem::geometryChanged(newGeometry, oldGeometry);
}


#include "quickgraphicsprogram.moc"

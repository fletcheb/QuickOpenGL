/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKOPENGLDEBUG_H
#define QUICKOPENGLDEBUG_H

#include <QObject>
#include "support.h"

class QuickOpenGLDebug : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Flags flags READ flags WRITE setFlags)

public:

    enum Flag
    {
        FlagTiming = DEBUG_TIMING,
        // Not used
//        FlagTextures = DEBUG_TEXTURES,
//        FlagShaderStorageBuffers = DEBUG_SSBOS,
    };

    Q_DECLARE_FLAGS(Flags, Flag)
    Q_FLAG(Flags)
    Q_ENUM(Flag)


    QuickOpenGLDebug(QObject *parent = nullptr);

    Flags flags() const;

public slots:

    void setFlags(Flags flags);
};

#endif // QUICKOPENGLDEBUG_H

#ifndef QUICKMATRIX3_H
#define QUICKMATRIX3_H

#include "quickvector2.h"
#include "quickvector3.h"
#include "quickvector4.h"

template <typename Type, typename RowVector, typename ColumnVector> class matrix3x2 : public QGenericMatrix<3,2,Type>
{
public:
    matrix3x2(){}

    matrix3x2(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix3x2(const ColumnVector &a, const ColumnVector &b, const ColumnVector &c)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();

        this->operator()(0,2) = c.x();
        this->operator()(1,2) = c.y();

    }

    // column wise intitialization as per GLSL
    matrix3x2(Type a, Type b, // first column
            Type c, Type d, // second column
            Type e, Type f) // third column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;

        this->operator()(0,1) = c;
        this->operator()(1,1) = d;

        this->operator()(0,2) = e;
        this->operator()(1,2) = f;
    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m02() const {return this->operator()(0, 2);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}
    Type m12() const {return this->operator()(1, 2);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM02(Type value) {this->operator()(0, 2) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}
    void setM12(Type value) {this->operator()(1, 2) = value;}

    RowVector r0() const {return RowVector(this->operator()(0, 0), this->operator()(0, 1), this->operator()(0, 2));}
    RowVector r1() const {return RowVector(this->operator()(1, 0), this->operator()(1, 1), this->operator()(1, 2));}

    void setR0(const RowVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y(); this->operator()(0, 2) = value.z(); }
    void setR1(const RowVector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(1, 2) = value.z(); }

    ColumnVector c0() const {return ColumnVector(this->operator()(0, 0), this->operator()(1, 0));}
    ColumnVector c1() const {return ColumnVector(this->operator()(0, 1), this->operator()(1, 1));}
    ColumnVector c2() const {return ColumnVector(this->operator()(0, 2), this->operator()(1, 2));}

    void setC0(const ColumnVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y();}
    void setC1(const ColumnVector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y();}
    void setC2(const ColumnVector & value) {this->operator()(0, 2) =  value.x(); this->operator()(1, 2) = value.y();}

};


template <typename Type, typename Vector> class matrix3 : public QGenericMatrix<3,3,Type>
{
public:
    matrix3(){}

    matrix3(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix3(const Vector &a, const Vector &b, const Vector &c)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();
        this->operator()(2,0) = a.z();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();
        this->operator()(2,1) = b.z();

        this->operator()(0,2) = c.x();
        this->operator()(1,2) = c.y();
        this->operator()(2,2) = c.z();

    }

    // column wise intitialization as per GLSL
    matrix3(Type a, Type b, Type c, // first column
            Type d, Type e, Type f, // second column
            Type g, Type h, Type i) // third column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;
        this->operator()(2,0) = c;

        this->operator()(0,1) = d;
        this->operator()(1,1) = e;
        this->operator()(2,1) = f;

        this->operator()(0,2) = g;
        this->operator()(1,2) = h;
        this->operator()(2,2) = i;
    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m02() const {return this->operator()(0, 2);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}
    Type m12() const {return this->operator()(1, 2);}
    Type m20() const {return this->operator()(2, 0);}
    Type m21() const {return this->operator()(2, 1);}
    Type m22() const {return this->operator()(2, 2);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM02(Type value) {this->operator()(0, 2) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}
    void setM12(Type value) {this->operator()(1, 2) = value;}
    void setM20(Type value) {this->operator()(2, 0) = value;}
    void setM21(Type value) {this->operator()(2, 1) = value;}
    void setM22(Type value) {this->operator()(2, 2) = value;}

    Vector r0() const {return Vector(this->operator()(0, 0), this->operator()(0, 1), this->operator()(0, 2));}
    Vector r1() const {return Vector(this->operator()(1, 0), this->operator()(1, 1), this->operator()(1, 2));}
    Vector r2() const {return Vector(this->operator()(2, 0), this->operator()(2, 1), this->operator()(2, 2));}

    void setR0(const Vector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y(); this->operator()(0, 2) = value.z(); }
    void setR1(const Vector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(1, 2) = value.z(); }
    void setR2(const Vector & value) {this->operator()(2, 0) =  value.x(); this->operator()(2, 1) = value.y(); this->operator()(2, 2) = value.z(); }

    Vector c0() const {return Vector(this->operator()(0, 0), this->operator()(1, 0), this->operator()(2, 0)); }
    Vector c1() const {return Vector(this->operator()(0, 1), this->operator()(1, 1), this->operator()(2, 1)); }
    Vector c2() const {return Vector(this->operator()(0, 2), this->operator()(1, 2), this->operator()(2, 2)); }

    void setC0(const Vector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y(); this->operator()(2, 0) = value.z();}
    void setC1(const Vector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(2, 1) = value.z();}
    void setC2(const Vector & value) {this->operator()(0, 2) =  value.x(); this->operator()(1, 2) = value.y(); this->operator()(2, 2) = value.z();}

};


template <typename Type, typename RowVector, typename ColumnVector> class matrix3x4 : public QGenericMatrix<3,4,Type>
{
public:
    matrix3x4(){}

    matrix3x4(Type a)
    {
        this->operator*=(a);
    }

    // column wise vector initialization as per GLSL
    matrix3x4(const ColumnVector &a, const ColumnVector &b, const ColumnVector &c)
    {
        this->operator()(0,0) = a.x();
        this->operator()(1,0) = a.y();
        this->operator()(2,0) = a.z();
        this->operator()(3,0) = a.w();

        this->operator()(0,1) = b.x();
        this->operator()(1,1) = b.y();
        this->operator()(2,1) = b.z();
        this->operator()(3,1) = b.w();

        this->operator()(0,2) = c.x();
        this->operator()(1,2) = c.y();
        this->operator()(2,2) = c.z();
        this->operator()(3,2) = c.w();

    }

    // column wise intitialization as per GLSL
    matrix3x4(Type a, Type b, Type c, Type d, // first column
              Type e, Type f, Type g, Type h, // second column
              Type i, Type j, Type k, Type l) // third column
    {
        this->operator()(0,0) = a;
        this->operator()(1,0) = b;
        this->operator()(2,0) = c;
        this->operator()(3,0) = d;

        this->operator()(0,1) = e;
        this->operator()(1,1) = f;
        this->operator()(2,1) = g;
        this->operator()(3,1) = h;

        this->operator()(0,2) = i;
        this->operator()(1,2) = j;
        this->operator()(2,2) = k;
        this->operator()(3,2) = l;
    }

    Type m00() const {return this->operator()(0, 0);}
    Type m01() const {return this->operator()(0, 1);}
    Type m02() const {return this->operator()(0, 2);}
    Type m10() const {return this->operator()(1, 0);}
    Type m11() const {return this->operator()(1, 1);}
    Type m12() const {return this->operator()(1, 2);}
    Type m20() const {return this->operator()(2, 0);}
    Type m21() const {return this->operator()(2, 1);}
    Type m22() const {return this->operator()(2, 2);}
    Type m30() const {return this->operator()(3, 0);}
    Type m31() const {return this->operator()(3, 1);}
    Type m32() const {return this->operator()(3, 2);}

    void setM00(Type value) {this->operator()(0, 0) = value;}
    void setM01(Type value) {this->operator()(0, 1) = value;}
    void setM02(Type value) {this->operator()(0, 2) = value;}
    void setM10(Type value) {this->operator()(1, 0) = value;}
    void setM11(Type value) {this->operator()(1, 1) = value;}
    void setM12(Type value) {this->operator()(1, 2) = value;}
    void setM20(Type value) {this->operator()(2, 0) = value;}
    void setM21(Type value) {this->operator()(2, 1) = value;}
    void setM22(Type value) {this->operator()(2, 2) = value;}
    void setM30(Type value) {this->operator()(3, 0) = value;}
    void setM31(Type value) {this->operator()(3, 1) = value;}
    void setM32(Type value) {this->operator()(3, 2) = value;}

    RowVector r0() const {return RowVector(this->operator()(0, 0), this->operator()(0, 1), this->operator()(0, 2));}
    RowVector r1() const {return RowVector(this->operator()(1, 0), this->operator()(1, 1), this->operator()(1, 2));}
    RowVector r2() const {return RowVector(this->operator()(2, 0), this->operator()(2, 1), this->operator()(2, 2));}
    RowVector r3() const {return RowVector(this->operator()(3, 0), this->operator()(3, 1), this->operator()(3, 2));}

    void setR0(const RowVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(0, 1) = value.y(); this->operator()(0, 2) = value.z(); }
    void setR1(const RowVector & value) {this->operator()(1, 0) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(1, 2) = value.z(); }
    void setR2(const RowVector & value) {this->operator()(2, 0) =  value.x(); this->operator()(2, 1) = value.y(); this->operator()(2, 2) = value.z(); }
    void setR3(const RowVector & value) {this->operator()(3, 0) =  value.x(); this->operator()(3, 1) = value.y(); this->operator()(3, 2) = value.z(); }

    ColumnVector c0() const {return ColumnVector(this->operator()(0, 0), this->operator()(1, 0), this->operator()(2, 0), this->operator()(3, 0)); }
    ColumnVector c1() const {return ColumnVector(this->operator()(0, 1), this->operator()(1, 1), this->operator()(2, 1), this->operator()(3, 1)); }
    ColumnVector c2() const {return ColumnVector(this->operator()(0, 2), this->operator()(1, 2), this->operator()(2, 2), this->operator()(3, 2)); }

    void setC0(const ColumnVector & value) {this->operator()(0, 0) =  value.x(); this->operator()(1, 0) = value.y(); this->operator()(2, 0) = value.z(); this->operator()(3, 0) = value.w();}
    void setC1(const ColumnVector & value) {this->operator()(0, 1) =  value.x(); this->operator()(1, 1) = value.y(); this->operator()(2, 1) = value.z(); this->operator()(3, 1) = value.w();}
    void setC2(const ColumnVector & value) {this->operator()(0, 2) =  value.x(); this->operator()(1, 2) = value.y(); this->operator()(2, 2) = value.z(); this->operator()(3, 2) = value.w();}

};


class qmat3x2 : public matrix3x2<float, qvec3, qvec2>
{
  Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m02 READ m02 WRITE setM02)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)
    Q_PROPERTY(float m12 READ m12 WRITE setM12)

    Q_PROPERTY(qvec3 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec3 r1 READ r1 WRITE setR1)

    Q_PROPERTY(qvec2 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec2 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qvec2 c2 READ c2 WRITE setC2)

public:
    using matrix3x2<float, qvec3, qvec2>::matrix3x2;
    using QGenericMatrix<3,2,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

};

class qmat3 : public matrix3<float, qvec3>
{
  Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m02 READ m02 WRITE setM02)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)
    Q_PROPERTY(float m12 READ m12 WRITE setM12)
    Q_PROPERTY(float m20 READ m20 WRITE setM20)
    Q_PROPERTY(float m21 READ m21 WRITE setM21)
    Q_PROPERTY(float m22 READ m22 WRITE setM22)

    Q_PROPERTY(qvec3 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec3 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qvec3 r2 READ r2 WRITE setR2)

    Q_PROPERTY(qvec3 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec3 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qvec3 c2 READ c2 WRITE setC2)

public:
    using matrix3<float, qvec3>::matrix3;
    using QGenericMatrix<3,3,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class qmat3x4 : public matrix3x4<float, qvec3, qvec4>
{
  Q_GADGET

    Q_PROPERTY(float m00 READ m00 WRITE setM00)
    Q_PROPERTY(float m01 READ m01 WRITE setM01)
    Q_PROPERTY(float m02 READ m02 WRITE setM02)
    Q_PROPERTY(float m10 READ m10 WRITE setM10)
    Q_PROPERTY(float m11 READ m11 WRITE setM11)
    Q_PROPERTY(float m12 READ m12 WRITE setM12)
    Q_PROPERTY(float m20 READ m20 WRITE setM20)
    Q_PROPERTY(float m21 READ m21 WRITE setM21)
    Q_PROPERTY(float m22 READ m22 WRITE setM22)
    Q_PROPERTY(float m30 READ m30 WRITE setM30)
    Q_PROPERTY(float m31 READ m31 WRITE setM31)
    Q_PROPERTY(float m32 READ m32 WRITE setM32)

    Q_PROPERTY(qvec3 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qvec3 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qvec3 r2 READ r2 WRITE setR2)
    Q_PROPERTY(qvec3 r3 READ r3 WRITE setR3)

    Q_PROPERTY(qvec4 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qvec4 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qvec4 c2 READ c2 WRITE setC2)

public:
    using matrix3x4<float, qvec3, qvec4>::matrix3x4;
    using QGenericMatrix<3,4,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};







class qdmat3x2 : public matrix3x2<double, qdvec3, qdvec2>
{
  Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m02 READ m02 WRITE setM02)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)
    Q_PROPERTY(double m12 READ m12 WRITE setM12)

    Q_PROPERTY(qdvec3 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec3 r1 READ r1 WRITE setR1)

    Q_PROPERTY(qdvec2 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec2 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qdvec2 c2 READ c2 WRITE setC2)

public:
    using matrix3x2<double, qdvec3, qdvec2>::matrix3x2;
    using QGenericMatrix<3,2,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

};

class qdmat3 : public matrix3<double, qdvec3>
{
  Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m02 READ m02 WRITE setM02)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)
    Q_PROPERTY(double m12 READ m12 WRITE setM12)
    Q_PROPERTY(double m20 READ m20 WRITE setM20)
    Q_PROPERTY(double m21 READ m21 WRITE setM21)
    Q_PROPERTY(double m22 READ m22 WRITE setM22)

    Q_PROPERTY(qdvec3 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec3 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qdvec3 r2 READ r2 WRITE setR2)

    Q_PROPERTY(qdvec3 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec3 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qdvec3 c2 READ c2 WRITE setC2)

public:
    using matrix3<double, qdvec3>::matrix3;
    using QGenericMatrix<3,3,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class qdmat3x4 : public matrix3x4<double, qdvec3, qdvec4>
{
  Q_GADGET

    Q_PROPERTY(double m00 READ m00 WRITE setM00)
    Q_PROPERTY(double m01 READ m01 WRITE setM01)
    Q_PROPERTY(double m02 READ m02 WRITE setM02)
    Q_PROPERTY(double m10 READ m10 WRITE setM10)
    Q_PROPERTY(double m11 READ m11 WRITE setM11)
    Q_PROPERTY(double m12 READ m12 WRITE setM12)
    Q_PROPERTY(double m20 READ m20 WRITE setM20)
    Q_PROPERTY(double m21 READ m21 WRITE setM21)
    Q_PROPERTY(double m22 READ m22 WRITE setM22)
    Q_PROPERTY(double m30 READ m30 WRITE setM30)
    Q_PROPERTY(double m31 READ m31 WRITE setM31)
    Q_PROPERTY(double m32 READ m32 WRITE setM32)

    Q_PROPERTY(qdvec3 r0 READ r0 WRITE setR0)
    Q_PROPERTY(qdvec3 r1 READ r1 WRITE setR1)
    Q_PROPERTY(qdvec3 r2 READ r2 WRITE setR2)
    Q_PROPERTY(qdvec3 r3 READ r3 WRITE setR3)

    Q_PROPERTY(qdvec4 c0 READ c0 WRITE setC0)
    Q_PROPERTY(qdvec4 c1 READ c1 WRITE setC1)
    Q_PROPERTY(qdvec4 c2 READ c2 WRITE setC2)

public:
    using matrix3x4<double, qdvec3, qdvec4>::matrix3x4;
    using QGenericMatrix<3,4,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};


Q_DECLARE_TYPEINFO(qmat3x2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qmat3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qmat3x4, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat3x2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat3, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdmat3x4, Q_MOVABLE_TYPE);

Q_DECLARE_METATYPE(qmat3x2)
Q_DECLARE_METATYPE(qmat3)
Q_DECLARE_METATYPE(qmat3x4)
Q_DECLARE_METATYPE(qdmat3x2)
Q_DECLARE_METATYPE(qdmat3)
Q_DECLARE_METATYPE(qdmat3x4)

#endif // QUICKMATRIX3_H

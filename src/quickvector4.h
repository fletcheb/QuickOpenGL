/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKVECTOR4_H
#define QUICKVECTOR4_H

#include <QTextStream>
#include <QGenericMatrix>
#include <QVariant>
#include <QDebug>
#include <QVector4D>
#include <QColor>
#include <QQuaternion>
#include <QRect>
#include <QRectF>
#include "quickopengl_global.h"
#include "quickvector2.h"
#include "quickvector3.h"

class qdmat4;

template <typename Type> class vector4: public QGenericMatrix<1,4,Type>
{
public:

    vector4(const QGenericMatrix<1,4,Type> &other)
    {
        memcpy(reinterpret_cast<void *>(this), &other, sizeof(vector4<Type>));
    }

    vector4(Type x = Type(0), Type y = Type(0), Type z = Type(0), Type w = Type(0))
    {
        setX(x);
        setY(y);
        setZ(z);
        setW(w);
    }

    vector4(Type x, const vector3<Type> &yzw)
    {
        setX(x);
        setY(yzw.x());
        setZ(yzw.y());
        setW(yzw.z());
    }

    vector4(const vector3<Type> &xyz, double w)
    {
        setX(xyz.x());
        setY(xyz.y());
        setZ(xyz.z());
        setW(w);
    }

    vector4(const vector2<Type> &xy, const vector2<Type> &zw)
    {
        setX(xy.x());
        setY(xy.y());
        setZ(zw.x());
        setW(zw.y());
    }

    vector4(double x, double y, const vector2<Type> &zw)
    {
        setX(x);
        setY(y);
        setZ(zw.x());
        setW(zw.y());
    }

    vector4(double x, const vector2<Type> &yz, double w )
    {
        setX(x);
        setY(yz.x());
        setZ(yz.y());
        setW(w);
    }

    vector4(const vector2<Type> &xy, double z, double w )
    {
        setX(xy.x());
        setY(xy.y());
        setZ(z);
        setW(w);
    }

    vector4(const QVector4D &vector4d)
    {
        setX(Type(vector4d.x()));
        setY(Type(vector4d.y()));
        setZ(Type(vector4d.z()));
        setW(Type(vector4d.w()));
    }

    vector4(const QColor &color)
    {
        setR(Type(color.red()));
        setG(Type(color.green()));
        setB(Type(color.blue()));
        setA(Type(color.alpha()));
    }

    vector4(const QString &string) : vector4(QColor(string))
    {
    }

    vector4(const QQuaternion &quaternion)
    {
        setX(Type(quaternion.x()));
        setY(Type(quaternion.y()));
        setZ(Type(quaternion.z()));
        setW(Type(quaternion.scalar()));
    }

    vector4(const QRect &rect)
    {
        setX(Type(rect.x()));
        setY(Type(rect.y()));
        setZ(Type(rect.width()));
        setW(Type(rect.height()));
    }

    vector4(const QRectF &rectF)
    {
        setX(Type(rectF.x()));
        setY(Type(rectF.y()));
        setZ(Type(rectF.width()));
        setW(Type(rectF.height()));
    }

    operator QVector4D() const
    {
        return QVector4D(float(x()), float(y()), float(z()), float(w()));
    }

    operator QColor() const
    {
        return QColor(int(r()), int(g()), int(b()), int(a()));
    }

    operator QString() const
    {        
        // if this value could be a color, encode as #aarrggbb
//        if(std::is_same<Type, float>::value&&
//                (float(r())>=0.0f&&float(r())<=1.0f)&&
//                (float(g())>=0.0f&&float(g())<=1.0f)&&
//                (float(b())>=0.0f&&float(b())<=1.0f)&&
//                (float(a())>=0.0f&&float(a())<=1.0f))
//        {
//            return this->operator QColor().name(QColor::HexArgb);
//        }
//        else if(std::is_same<Type, int>::value||std::is_same<Type, uint>::value)
//        {
//            return this->operator QColor().name(QColor::HexArgb);
//        }

        QString result;
        QTextStream stream(&result);
        stream << "(" << x() << ", " << y() << ", " << z() << ", " << w() << ")";
        return result;
    }

    operator QQuaternion() const
    {
        return QQuaternion(float(w()), float(x()), float(y()), float(z()));
    }

    operator QRect() const
    {
        return QRect(int(x()), int(y()), int(z()), int(w()));
    }

    operator QRectF() const
    {
        return QRectF(qreal(x()), qreal(y()), qreal(z()), qreal(w()));
    }

    vector2<Type> xx() const {return vector2<Type>(x(),x());}
    vector2<Type> xy() const {return vector2<Type>(x(),y());}
    vector2<Type> xz() const {return vector2<Type>(x(),z());}
    vector2<Type> xw() const {return vector2<Type>(x(),w());}
    vector2<Type> yx() const {return vector2<Type>(y(),x());}
    vector2<Type> yy() const {return vector2<Type>(y(),y());}
    vector2<Type> yz() const {return vector2<Type>(y(),z());}
    vector2<Type> yw() const {return vector2<Type>(y(),w());}
    vector2<Type> zx() const {return vector2<Type>(z(),x());}
    vector2<Type> zy() const {return vector2<Type>(z(),y());}
    vector2<Type> zz() const {return vector2<Type>(z(),z());}
    vector2<Type> zw() const {return vector2<Type>(z(),w());}
    vector2<Type> wx() const {return vector2<Type>(w(),x());}
    vector2<Type> wy() const {return vector2<Type>(w(),y());}
    vector2<Type> wz() const {return vector2<Type>(w(),z());}
    vector2<Type> ww() const {return vector2<Type>(w(),w());}

    vector3<Type> xxx() const {return vector3<Type>(x(),x(),x());}
    vector3<Type> xxy() const {return vector3<Type>(x(),x(),y());}
    vector3<Type> xxz() const {return vector3<Type>(x(),x(),z());}
    vector3<Type> xxw() const {return vector3<Type>(x(),x(),w());}
    vector3<Type> xyx() const {return vector3<Type>(x(),y(),x());}
    vector3<Type> xyy() const {return vector3<Type>(x(),y(),y());}
    vector3<Type> xyz() const {return vector3<Type>(x(),y(),z());}
    vector3<Type> xyw() const {return vector3<Type>(x(),y(),w());}
    vector3<Type> xzx() const {return vector3<Type>(x(),z(),x());}
    vector3<Type> xzy() const {return vector3<Type>(x(),z(),y());}
    vector3<Type> xzz() const {return vector3<Type>(x(),z(),z());}
    vector3<Type> xzw() const {return vector3<Type>(x(),z(),w());}
    vector3<Type> xwx() const {return vector3<Type>(x(),w(),x());}
    vector3<Type> xwy() const {return vector3<Type>(x(),w(),y());}
    vector3<Type> xwz() const {return vector3<Type>(x(),w(),z());}
    vector3<Type> xww() const {return vector3<Type>(x(),w(),w());}
    vector3<Type> yxx() const {return vector3<Type>(y(),x(),x());}
    vector3<Type> yxy() const {return vector3<Type>(y(),x(),y());}
    vector3<Type> yxz() const {return vector3<Type>(y(),x(),z());}
    vector3<Type> yxw() const {return vector3<Type>(y(),x(),w());}
    vector3<Type> yyx() const {return vector3<Type>(y(),y(),x());}
    vector3<Type> yyy() const {return vector3<Type>(y(),y(),y());}
    vector3<Type> yyz() const {return vector3<Type>(y(),y(),z());}
    vector3<Type> yyw() const {return vector3<Type>(y(),y(),w());}
    vector3<Type> yzx() const {return vector3<Type>(y(),z(),x());}
    vector3<Type> yzy() const {return vector3<Type>(y(),z(),y());}
    vector3<Type> yzz() const {return vector3<Type>(y(),z(),z());}
    vector3<Type> yzw() const {return vector3<Type>(y(),z(),w());}
    vector3<Type> ywx() const {return vector3<Type>(y(),w(),x());}
    vector3<Type> ywy() const {return vector3<Type>(y(),w(),y());}
    vector3<Type> ywz() const {return vector3<Type>(y(),w(),z());}
    vector3<Type> yww() const {return vector3<Type>(y(),w(),w());}
    vector3<Type> zxx() const {return vector3<Type>(z(),x(),x());}
    vector3<Type> zxy() const {return vector3<Type>(z(),x(),y());}
    vector3<Type> zxz() const {return vector3<Type>(z(),x(),z());}
    vector3<Type> zxw() const {return vector3<Type>(z(),x(),w());}
    vector3<Type> zyx() const {return vector3<Type>(z(),y(),x());}
    vector3<Type> zyy() const {return vector3<Type>(z(),y(),y());}
    vector3<Type> zyz() const {return vector3<Type>(z(),y(),z());}
    vector3<Type> zyw() const {return vector3<Type>(z(),y(),w());}
    vector3<Type> zzx() const {return vector3<Type>(z(),z(),x());}
    vector3<Type> zzy() const {return vector3<Type>(z(),z(),y());}
    vector3<Type> zzz() const {return vector3<Type>(z(),z(),z());}
    vector3<Type> zzw() const {return vector3<Type>(z(),z(),w());}
    vector3<Type> zwx() const {return vector3<Type>(z(),w(),x());}
    vector3<Type> zwy() const {return vector3<Type>(z(),w(),y());}
    vector3<Type> zwz() const {return vector3<Type>(z(),w(),z());}
    vector3<Type> zww() const {return vector3<Type>(z(),w(),w());}
    vector3<Type> wxx() const {return vector3<Type>(w(),x(),x());}
    vector3<Type> wxy() const {return vector3<Type>(w(),x(),y());}
    vector3<Type> wxz() const {return vector3<Type>(w(),x(),z());}
    vector3<Type> wxw() const {return vector3<Type>(w(),x(),w());}
    vector3<Type> wyx() const {return vector3<Type>(w(),y(),x());}
    vector3<Type> wyy() const {return vector3<Type>(w(),y(),y());}
    vector3<Type> wyz() const {return vector3<Type>(w(),y(),z());}
    vector3<Type> wyw() const {return vector3<Type>(w(),y(),w());}
    vector3<Type> wzx() const {return vector3<Type>(w(),z(),x());}
    vector3<Type> wzy() const {return vector3<Type>(w(),z(),y());}
    vector3<Type> wzz() const {return vector3<Type>(w(),z(),z());}
    vector3<Type> wzw() const {return vector3<Type>(w(),z(),w());}
    vector3<Type> wwx() const {return vector3<Type>(w(),w(),x());}
    vector3<Type> wwy() const {return vector3<Type>(w(),w(),y());}
    vector3<Type> wwz() const {return vector3<Type>(w(),w(),z());}
    vector3<Type> www() const {return vector3<Type>(w(),w(),w());}

    vector4<Type> xxxx() const {return vector4<Type>(x(),x(),x(),x());}
    vector4<Type> xxxy() const {return vector4<Type>(x(),x(),x(),y());}
    vector4<Type> xxxz() const {return vector4<Type>(x(),x(),x(),z());}
    vector4<Type> xxxw() const {return vector4<Type>(x(),x(),x(),w());}
    vector4<Type> xxyx() const {return vector4<Type>(x(),x(),y(),x());}
    vector4<Type> xxyy() const {return vector4<Type>(x(),x(),y(),y());}
    vector4<Type> xxyz() const {return vector4<Type>(x(),x(),y(),z());}
    vector4<Type> xxyw() const {return vector4<Type>(x(),x(),y(),w());}
    vector4<Type> xxzx() const {return vector4<Type>(x(),x(),z(),x());}
    vector4<Type> xxzy() const {return vector4<Type>(x(),x(),z(),y());}
    vector4<Type> xxzz() const {return vector4<Type>(x(),x(),z(),z());}
    vector4<Type> xxzw() const {return vector4<Type>(x(),x(),z(),w());}
    vector4<Type> xxwx() const {return vector4<Type>(x(),x(),w(),x());}
    vector4<Type> xxwy() const {return vector4<Type>(x(),x(),w(),y());}
    vector4<Type> xxwz() const {return vector4<Type>(x(),x(),w(),z());}
    vector4<Type> xxww() const {return vector4<Type>(x(),x(),w(),w());}
    vector4<Type> xyxx() const {return vector4<Type>(x(),y(),x(),x());}
    vector4<Type> xyxy() const {return vector4<Type>(x(),y(),x(),y());}
    vector4<Type> xyxz() const {return vector4<Type>(x(),y(),x(),z());}
    vector4<Type> xyxw() const {return vector4<Type>(x(),y(),x(),w());}
    vector4<Type> xyyx() const {return vector4<Type>(x(),y(),y(),x());}
    vector4<Type> xyyy() const {return vector4<Type>(x(),y(),y(),y());}
    vector4<Type> xyyz() const {return vector4<Type>(x(),y(),y(),z());}
    vector4<Type> xyyw() const {return vector4<Type>(x(),y(),y(),w());}
    vector4<Type> xyzx() const {return vector4<Type>(x(),y(),z(),x());}
    vector4<Type> xyzy() const {return vector4<Type>(x(),y(),z(),y());}
    vector4<Type> xyzz() const {return vector4<Type>(x(),y(),z(),z());}
    vector4<Type> xyzw() const {return vector4<Type>(x(),y(),z(),w());}
    vector4<Type> xywx() const {return vector4<Type>(x(),y(),w(),x());}
    vector4<Type> xywy() const {return vector4<Type>(x(),y(),w(),y());}
    vector4<Type> xywz() const {return vector4<Type>(x(),y(),w(),z());}
    vector4<Type> xyww() const {return vector4<Type>(x(),y(),w(),w());}
    vector4<Type> xzxx() const {return vector4<Type>(x(),z(),x(),x());}
    vector4<Type> xzxy() const {return vector4<Type>(x(),z(),x(),y());}
    vector4<Type> xzxz() const {return vector4<Type>(x(),z(),x(),z());}
    vector4<Type> xzxw() const {return vector4<Type>(x(),z(),x(),w());}
    vector4<Type> xzyx() const {return vector4<Type>(x(),z(),y(),x());}
    vector4<Type> xzyy() const {return vector4<Type>(x(),z(),y(),y());}
    vector4<Type> xzyz() const {return vector4<Type>(x(),z(),y(),z());}
    vector4<Type> xzyw() const {return vector4<Type>(x(),z(),y(),w());}
    vector4<Type> xzzx() const {return vector4<Type>(x(),z(),z(),x());}
    vector4<Type> xzzy() const {return vector4<Type>(x(),z(),z(),y());}
    vector4<Type> xzzz() const {return vector4<Type>(x(),z(),z(),z());}
    vector4<Type> xzzw() const {return vector4<Type>(x(),z(),z(),w());}
    vector4<Type> xzwx() const {return vector4<Type>(x(),z(),w(),x());}
    vector4<Type> xzwy() const {return vector4<Type>(x(),z(),w(),y());}
    vector4<Type> xzwz() const {return vector4<Type>(x(),z(),w(),z());}
    vector4<Type> xzww() const {return vector4<Type>(x(),z(),w(),w());}
    vector4<Type> xwxx() const {return vector4<Type>(x(),w(),x(),x());}
    vector4<Type> xwxy() const {return vector4<Type>(x(),w(),x(),y());}
    vector4<Type> xwxz() const {return vector4<Type>(x(),w(),x(),z());}
    vector4<Type> xwxw() const {return vector4<Type>(x(),w(),x(),w());}
    vector4<Type> xwyx() const {return vector4<Type>(x(),w(),y(),x());}
    vector4<Type> xwyy() const {return vector4<Type>(x(),w(),y(),y());}
    vector4<Type> xwyz() const {return vector4<Type>(x(),w(),y(),z());}
    vector4<Type> xwyw() const {return vector4<Type>(x(),w(),y(),w());}
    vector4<Type> xwzx() const {return vector4<Type>(x(),w(),z(),x());}
    vector4<Type> xwzy() const {return vector4<Type>(x(),w(),z(),y());}
    vector4<Type> xwzz() const {return vector4<Type>(x(),w(),z(),z());}
    vector4<Type> xwzw() const {return vector4<Type>(x(),w(),z(),w());}
    vector4<Type> xwwx() const {return vector4<Type>(x(),w(),w(),x());}
    vector4<Type> xwwy() const {return vector4<Type>(x(),w(),w(),y());}
    vector4<Type> xwwz() const {return vector4<Type>(x(),w(),w(),z());}
    vector4<Type> xwww() const {return vector4<Type>(x(),w(),w(),w());}
    vector4<Type> yxxx() const {return vector4<Type>(y(),x(),x(),x());}
    vector4<Type> yxxy() const {return vector4<Type>(y(),x(),x(),y());}
    vector4<Type> yxxz() const {return vector4<Type>(y(),x(),x(),z());}
    vector4<Type> yxxw() const {return vector4<Type>(y(),x(),x(),w());}
    vector4<Type> yxyx() const {return vector4<Type>(y(),x(),y(),x());}
    vector4<Type> yxyy() const {return vector4<Type>(y(),x(),y(),y());}
    vector4<Type> yxyz() const {return vector4<Type>(y(),x(),y(),z());}
    vector4<Type> yxyw() const {return vector4<Type>(y(),x(),y(),w());}
    vector4<Type> yxzx() const {return vector4<Type>(y(),x(),z(),x());}
    vector4<Type> yxzy() const {return vector4<Type>(y(),x(),z(),y());}
    vector4<Type> yxzz() const {return vector4<Type>(y(),x(),z(),z());}
    vector4<Type> yxzw() const {return vector4<Type>(y(),x(),z(),w());}
    vector4<Type> yxwx() const {return vector4<Type>(y(),x(),w(),x());}
    vector4<Type> yxwy() const {return vector4<Type>(y(),x(),w(),y());}
    vector4<Type> yxwz() const {return vector4<Type>(y(),x(),w(),z());}
    vector4<Type> yxww() const {return vector4<Type>(y(),x(),w(),w());}
    vector4<Type> yyxx() const {return vector4<Type>(y(),y(),x(),x());}
    vector4<Type> yyxy() const {return vector4<Type>(y(),y(),x(),y());}
    vector4<Type> yyxz() const {return vector4<Type>(y(),y(),x(),z());}
    vector4<Type> yyxw() const {return vector4<Type>(y(),y(),x(),w());}
    vector4<Type> yyyx() const {return vector4<Type>(y(),y(),y(),x());}
    vector4<Type> yyyy() const {return vector4<Type>(y(),y(),y(),y());}
    vector4<Type> yyyz() const {return vector4<Type>(y(),y(),y(),z());}
    vector4<Type> yyyw() const {return vector4<Type>(y(),y(),y(),w());}
    vector4<Type> yyzx() const {return vector4<Type>(y(),y(),z(),x());}
    vector4<Type> yyzy() const {return vector4<Type>(y(),y(),z(),y());}
    vector4<Type> yyzz() const {return vector4<Type>(y(),y(),z(),z());}
    vector4<Type> yyzw() const {return vector4<Type>(y(),y(),z(),w());}
    vector4<Type> yywx() const {return vector4<Type>(y(),y(),w(),x());}
    vector4<Type> yywy() const {return vector4<Type>(y(),y(),w(),y());}
    vector4<Type> yywz() const {return vector4<Type>(y(),y(),w(),z());}
    vector4<Type> yyww() const {return vector4<Type>(y(),y(),w(),w());}
    vector4<Type> yzxx() const {return vector4<Type>(y(),z(),x(),x());}
    vector4<Type> yzxy() const {return vector4<Type>(y(),z(),x(),y());}
    vector4<Type> yzxz() const {return vector4<Type>(y(),z(),x(),z());}
    vector4<Type> yzxw() const {return vector4<Type>(y(),z(),x(),w());}
    vector4<Type> yzyx() const {return vector4<Type>(y(),z(),y(),x());}
    vector4<Type> yzyy() const {return vector4<Type>(y(),z(),y(),y());}
    vector4<Type> yzyz() const {return vector4<Type>(y(),z(),y(),z());}
    vector4<Type> yzyw() const {return vector4<Type>(y(),z(),y(),w());}
    vector4<Type> yzzx() const {return vector4<Type>(y(),z(),z(),x());}
    vector4<Type> yzzy() const {return vector4<Type>(y(),z(),z(),y());}
    vector4<Type> yzzz() const {return vector4<Type>(y(),z(),z(),z());}
    vector4<Type> yzzw() const {return vector4<Type>(y(),z(),z(),w());}
    vector4<Type> yzwx() const {return vector4<Type>(y(),z(),w(),x());}
    vector4<Type> yzwy() const {return vector4<Type>(y(),z(),w(),y());}
    vector4<Type> yzwz() const {return vector4<Type>(y(),z(),w(),z());}
    vector4<Type> yzww() const {return vector4<Type>(y(),z(),w(),w());}
    vector4<Type> ywxx() const {return vector4<Type>(y(),w(),x(),x());}
    vector4<Type> ywxy() const {return vector4<Type>(y(),w(),x(),y());}
    vector4<Type> ywxz() const {return vector4<Type>(y(),w(),x(),z());}
    vector4<Type> ywxw() const {return vector4<Type>(y(),w(),x(),w());}
    vector4<Type> ywyx() const {return vector4<Type>(y(),w(),y(),x());}
    vector4<Type> ywyy() const {return vector4<Type>(y(),w(),y(),y());}
    vector4<Type> ywyz() const {return vector4<Type>(y(),w(),y(),z());}
    vector4<Type> ywyw() const {return vector4<Type>(y(),w(),y(),w());}
    vector4<Type> ywzx() const {return vector4<Type>(y(),w(),z(),x());}
    vector4<Type> ywzy() const {return vector4<Type>(y(),w(),z(),y());}
    vector4<Type> ywzz() const {return vector4<Type>(y(),w(),z(),z());}
    vector4<Type> ywzw() const {return vector4<Type>(y(),w(),z(),w());}
    vector4<Type> ywwx() const {return vector4<Type>(y(),w(),w(),x());}
    vector4<Type> ywwy() const {return vector4<Type>(y(),w(),w(),y());}
    vector4<Type> ywwz() const {return vector4<Type>(y(),w(),w(),z());}
    vector4<Type> ywww() const {return vector4<Type>(y(),w(),w(),w());}
    vector4<Type> zxxx() const {return vector4<Type>(z(),x(),x(),x());}
    vector4<Type> zxxy() const {return vector4<Type>(z(),x(),x(),y());}
    vector4<Type> zxxz() const {return vector4<Type>(z(),x(),x(),z());}
    vector4<Type> zxxw() const {return vector4<Type>(z(),x(),x(),w());}
    vector4<Type> zxyx() const {return vector4<Type>(z(),x(),y(),x());}
    vector4<Type> zxyy() const {return vector4<Type>(z(),x(),y(),y());}
    vector4<Type> zxyz() const {return vector4<Type>(z(),x(),y(),z());}
    vector4<Type> zxyw() const {return vector4<Type>(z(),x(),y(),w());}
    vector4<Type> zxzx() const {return vector4<Type>(z(),x(),z(),x());}
    vector4<Type> zxzy() const {return vector4<Type>(z(),x(),z(),y());}
    vector4<Type> zxzz() const {return vector4<Type>(z(),x(),z(),z());}
    vector4<Type> zxzw() const {return vector4<Type>(z(),x(),z(),w());}
    vector4<Type> zxwx() const {return vector4<Type>(z(),x(),w(),x());}
    vector4<Type> zxwy() const {return vector4<Type>(z(),x(),w(),y());}
    vector4<Type> zxwz() const {return vector4<Type>(z(),x(),w(),z());}
    vector4<Type> zxww() const {return vector4<Type>(z(),x(),w(),w());}
    vector4<Type> zyxx() const {return vector4<Type>(z(),y(),x(),x());}
    vector4<Type> zyxy() const {return vector4<Type>(z(),y(),x(),y());}
    vector4<Type> zyxz() const {return vector4<Type>(z(),y(),x(),z());}
    vector4<Type> zyxw() const {return vector4<Type>(z(),y(),x(),w());}
    vector4<Type> zyyx() const {return vector4<Type>(z(),y(),y(),x());}
    vector4<Type> zyyy() const {return vector4<Type>(z(),y(),y(),y());}
    vector4<Type> zyyz() const {return vector4<Type>(z(),y(),y(),z());}
    vector4<Type> zyyw() const {return vector4<Type>(z(),y(),y(),w());}
    vector4<Type> zyzx() const {return vector4<Type>(z(),y(),z(),x());}
    vector4<Type> zyzy() const {return vector4<Type>(z(),y(),z(),y());}
    vector4<Type> zyzz() const {return vector4<Type>(z(),y(),z(),z());}
    vector4<Type> zyzw() const {return vector4<Type>(z(),y(),z(),w());}
    vector4<Type> zywx() const {return vector4<Type>(z(),y(),w(),x());}
    vector4<Type> zywy() const {return vector4<Type>(z(),y(),w(),y());}
    vector4<Type> zywz() const {return vector4<Type>(z(),y(),w(),z());}
    vector4<Type> zyww() const {return vector4<Type>(z(),y(),w(),w());}
    vector4<Type> zzxx() const {return vector4<Type>(z(),z(),x(),x());}
    vector4<Type> zzxy() const {return vector4<Type>(z(),z(),x(),y());}
    vector4<Type> zzxz() const {return vector4<Type>(z(),z(),x(),z());}
    vector4<Type> zzxw() const {return vector4<Type>(z(),z(),x(),w());}
    vector4<Type> zzyx() const {return vector4<Type>(z(),z(),y(),x());}
    vector4<Type> zzyy() const {return vector4<Type>(z(),z(),y(),y());}
    vector4<Type> zzyz() const {return vector4<Type>(z(),z(),y(),z());}
    vector4<Type> zzyw() const {return vector4<Type>(z(),z(),y(),w());}
    vector4<Type> zzzx() const {return vector4<Type>(z(),z(),z(),x());}
    vector4<Type> zzzy() const {return vector4<Type>(z(),z(),z(),y());}
    vector4<Type> zzzz() const {return vector4<Type>(z(),z(),z(),z());}
    vector4<Type> zzzw() const {return vector4<Type>(z(),z(),z(),w());}
    vector4<Type> zzwx() const {return vector4<Type>(z(),z(),w(),x());}
    vector4<Type> zzwy() const {return vector4<Type>(z(),z(),w(),y());}
    vector4<Type> zzwz() const {return vector4<Type>(z(),z(),w(),z());}
    vector4<Type> zzww() const {return vector4<Type>(z(),z(),w(),w());}
    vector4<Type> zwxx() const {return vector4<Type>(z(),w(),x(),x());}
    vector4<Type> zwxy() const {return vector4<Type>(z(),w(),x(),y());}
    vector4<Type> zwxz() const {return vector4<Type>(z(),w(),x(),z());}
    vector4<Type> zwxw() const {return vector4<Type>(z(),w(),x(),w());}
    vector4<Type> zwyx() const {return vector4<Type>(z(),w(),y(),x());}
    vector4<Type> zwyy() const {return vector4<Type>(z(),w(),y(),y());}
    vector4<Type> zwyz() const {return vector4<Type>(z(),w(),y(),z());}
    vector4<Type> zwyw() const {return vector4<Type>(z(),w(),y(),w());}
    vector4<Type> zwzx() const {return vector4<Type>(z(),w(),z(),x());}
    vector4<Type> zwzy() const {return vector4<Type>(z(),w(),z(),y());}
    vector4<Type> zwzz() const {return vector4<Type>(z(),w(),z(),z());}
    vector4<Type> zwzw() const {return vector4<Type>(z(),w(),z(),w());}
    vector4<Type> zwwx() const {return vector4<Type>(z(),w(),w(),x());}
    vector4<Type> zwwy() const {return vector4<Type>(z(),w(),w(),y());}
    vector4<Type> zwwz() const {return vector4<Type>(z(),w(),w(),z());}
    vector4<Type> zwww() const {return vector4<Type>(z(),w(),w(),w());}
    vector4<Type> wxxx() const {return vector4<Type>(w(),x(),x(),x());}
    vector4<Type> wxxy() const {return vector4<Type>(w(),x(),x(),y());}
    vector4<Type> wxxz() const {return vector4<Type>(w(),x(),x(),z());}
    vector4<Type> wxxw() const {return vector4<Type>(w(),x(),x(),w());}
    vector4<Type> wxyx() const {return vector4<Type>(w(),x(),y(),x());}
    vector4<Type> wxyy() const {return vector4<Type>(w(),x(),y(),y());}
    vector4<Type> wxyz() const {return vector4<Type>(w(),x(),y(),z());}
    vector4<Type> wxyw() const {return vector4<Type>(w(),x(),y(),w());}
    vector4<Type> wxzx() const {return vector4<Type>(w(),x(),z(),x());}
    vector4<Type> wxzy() const {return vector4<Type>(w(),x(),z(),y());}
    vector4<Type> wxzz() const {return vector4<Type>(w(),x(),z(),z());}
    vector4<Type> wxzw() const {return vector4<Type>(w(),x(),z(),w());}
    vector4<Type> wxwx() const {return vector4<Type>(w(),x(),w(),x());}
    vector4<Type> wxwy() const {return vector4<Type>(w(),x(),w(),y());}
    vector4<Type> wxwz() const {return vector4<Type>(w(),x(),w(),z());}
    vector4<Type> wxww() const {return vector4<Type>(w(),x(),w(),w());}
    vector4<Type> wyxx() const {return vector4<Type>(w(),y(),x(),x());}
    vector4<Type> wyxy() const {return vector4<Type>(w(),y(),x(),y());}
    vector4<Type> wyxz() const {return vector4<Type>(w(),y(),x(),z());}
    vector4<Type> wyxw() const {return vector4<Type>(w(),y(),x(),w());}
    vector4<Type> wyyx() const {return vector4<Type>(w(),y(),y(),x());}
    vector4<Type> wyyy() const {return vector4<Type>(w(),y(),y(),y());}
    vector4<Type> wyyz() const {return vector4<Type>(w(),y(),y(),z());}
    vector4<Type> wyyw() const {return vector4<Type>(w(),y(),y(),w());}
    vector4<Type> wyzx() const {return vector4<Type>(w(),y(),z(),x());}
    vector4<Type> wyzy() const {return vector4<Type>(w(),y(),z(),y());}
    vector4<Type> wyzz() const {return vector4<Type>(w(),y(),z(),z());}
    vector4<Type> wyzw() const {return vector4<Type>(w(),y(),z(),w());}
    vector4<Type> wywx() const {return vector4<Type>(w(),y(),w(),x());}
    vector4<Type> wywy() const {return vector4<Type>(w(),y(),w(),y());}
    vector4<Type> wywz() const {return vector4<Type>(w(),y(),w(),z());}
    vector4<Type> wyww() const {return vector4<Type>(w(),y(),w(),w());}
    vector4<Type> wzxx() const {return vector4<Type>(w(),z(),x(),x());}
    vector4<Type> wzxy() const {return vector4<Type>(w(),z(),x(),y());}
    vector4<Type> wzxz() const {return vector4<Type>(w(),z(),x(),z());}
    vector4<Type> wzxw() const {return vector4<Type>(w(),z(),x(),w());}
    vector4<Type> wzyx() const {return vector4<Type>(w(),z(),y(),x());}
    vector4<Type> wzyy() const {return vector4<Type>(w(),z(),y(),y());}
    vector4<Type> wzyz() const {return vector4<Type>(w(),z(),y(),z());}
    vector4<Type> wzyw() const {return vector4<Type>(w(),z(),y(),w());}
    vector4<Type> wzzx() const {return vector4<Type>(w(),z(),z(),x());}
    vector4<Type> wzzy() const {return vector4<Type>(w(),z(),z(),y());}
    vector4<Type> wzzz() const {return vector4<Type>(w(),z(),z(),z());}
    vector4<Type> wzzw() const {return vector4<Type>(w(),z(),z(),w());}
    vector4<Type> wzwx() const {return vector4<Type>(w(),z(),w(),x());}
    vector4<Type> wzwy() const {return vector4<Type>(w(),z(),w(),y());}
    vector4<Type> wzwz() const {return vector4<Type>(w(),z(),w(),z());}
    vector4<Type> wzww() const {return vector4<Type>(w(),z(),w(),w());}
    vector4<Type> wwxx() const {return vector4<Type>(w(),w(),x(),x());}
    vector4<Type> wwxy() const {return vector4<Type>(w(),w(),x(),y());}
    vector4<Type> wwxz() const {return vector4<Type>(w(),w(),x(),z());}
    vector4<Type> wwxw() const {return vector4<Type>(w(),w(),x(),w());}
    vector4<Type> wwyx() const {return vector4<Type>(w(),w(),y(),x());}
    vector4<Type> wwyy() const {return vector4<Type>(w(),w(),y(),y());}
    vector4<Type> wwyz() const {return vector4<Type>(w(),w(),y(),z());}
    vector4<Type> wwyw() const {return vector4<Type>(w(),w(),y(),w());}
    vector4<Type> wwzx() const {return vector4<Type>(w(),w(),z(),x());}
    vector4<Type> wwzy() const {return vector4<Type>(w(),w(),z(),y());}
    vector4<Type> wwzz() const {return vector4<Type>(w(),w(),z(),z());}
    vector4<Type> wwzw() const {return vector4<Type>(w(),w(),z(),w());}
    vector4<Type> wwwx() const {return vector4<Type>(w(),w(),w(),x());}
    vector4<Type> wwwy() const {return vector4<Type>(w(),w(),w(),y());}
    vector4<Type> wwwz() const {return vector4<Type>(w(),w(),w(),z());}
    vector4<Type> wwww() const {return vector4<Type>(w(),w(),w(),w());}

    Type x() const
    {
        return this->operator()(0,0);
    }
    Type y() const
    {
        return this->operator()(1,0);
    }
    Type z() const
    {
        return this->operator()(2,0);
    }
    Type w() const
    {
        return this->operator()(3,0);
    }
    Type r() const
    {
        return this->operator()(0,0);
    }
    Type g() const
    {
        return this->operator()(1,0);
    }
    Type b() const
    {
        return this->operator()(2,0);
    }
    Type a() const
    {
        return this->operator()(3,0);
    }

    void setX(Type x)
    {
        this->operator()(0,0) = x;
    }

    void setY(Type y)
    {
        this->operator()(1,0) = y;
    }

    void setZ(Type z)
    {
        this->operator()(2,0) = z;
    }
    void setW(Type w)
    {
        this->operator()(3,0) = w;
    }
    void setR(Type r)
    {
        this->operator()(0,0) = r;
    }

    void setG(Type g)
    {
        this->operator()(1,0) = g;
    }

    void setB(Type b)
    {
        this->operator()(2,0) = b;
    }
    void setA(Type a)
    {
        this->operator()(3,0) = a;
    }
};

template <> inline vector4<float>::vector4(const QColor &color)
{
    setR(float(color.redF()));
    setG(float(color.greenF()));
    setB(float(color.blueF()));
    setA(float(color.alphaF()));
}

template <> inline vector4<double>::vector4(const QColor &color)
{
    setR(double(color.redF()));
    setG(double(color.greenF()));
    setB(double(color.blueF()));
    setA(double(color.alphaF()));
}

template <> inline vector4<float>::operator QColor() const
{
    QColor color;
    color.setRedF(qreal(r()));
    color.setGreenF(qreal(g()));
    color.setBlueF(qreal(b()));
    color.setAlphaF(qreal(a()));
    return color;
}

template <> inline vector4<double>::operator QColor() const
{
    QColor color;
    color.setRedF(qreal(r()));
    color.setGreenF(qreal(g()));
    color.setBlueF(qreal(b()));
    color.setAlphaF(qreal(a()));
    return color;
}

class QUICKOPENGL_EXPORT qvec4 : public vector4<float>
{
    Q_GADGET
    Q_PROPERTY(float x READ x WRITE setX)
    Q_PROPERTY(float y READ y WRITE setY)
    Q_PROPERTY(float z READ z WRITE setZ)
    Q_PROPERTY(float w READ w WRITE setW)
    Q_PROPERTY(float r READ r WRITE setR)
    Q_PROPERTY(float g READ g WRITE setG)
    Q_PROPERTY(float b READ b WRITE setB)
    Q_PROPERTY(float a READ a WRITE setA)

    Q_PROPERTY(qvec2 xx READ xx STORED false)
    Q_PROPERTY(qvec2 xy READ xy STORED false)
    Q_PROPERTY(qvec2 xz READ xz STORED false)
    Q_PROPERTY(qvec2 xw READ xw STORED false)
    Q_PROPERTY(qvec2 yx READ yx STORED false)
    Q_PROPERTY(qvec2 yy READ yy STORED false)
    Q_PROPERTY(qvec2 yz READ yz STORED false)
    Q_PROPERTY(qvec2 yw READ yw STORED false)
    Q_PROPERTY(qvec2 zx READ zx STORED false)
    Q_PROPERTY(qvec2 zy READ zy STORED false)
    Q_PROPERTY(qvec2 zz READ zz STORED false)
    Q_PROPERTY(qvec2 zw READ zw STORED false)
    Q_PROPERTY(qvec2 wx READ wx STORED false)
    Q_PROPERTY(qvec2 wy READ wy STORED false)
    Q_PROPERTY(qvec2 wz READ wz STORED false)
    Q_PROPERTY(qvec2 ww READ ww STORED false)

    Q_PROPERTY(qvec3 xxx READ xxx STORED false)
    Q_PROPERTY(qvec3 xxy READ xxy STORED false)
    Q_PROPERTY(qvec3 xxz READ xxz STORED false)
    Q_PROPERTY(qvec3 xxw READ xxw STORED false)
    Q_PROPERTY(qvec3 xyx READ xyx STORED false)
    Q_PROPERTY(qvec3 xyy READ xyy STORED false)
    Q_PROPERTY(qvec3 xyz READ xyz STORED false)
    Q_PROPERTY(qvec3 xyw READ xyw STORED false)
    Q_PROPERTY(qvec3 xzx READ xzx STORED false)
    Q_PROPERTY(qvec3 xzy READ xzy STORED false)
    Q_PROPERTY(qvec3 xzz READ xzz STORED false)
    Q_PROPERTY(qvec3 xzw READ xzw STORED false)
    Q_PROPERTY(qvec3 xwx READ xwx STORED false)
    Q_PROPERTY(qvec3 xwy READ xwy STORED false)
    Q_PROPERTY(qvec3 xwz READ xwz STORED false)
    Q_PROPERTY(qvec3 xww READ xww STORED false)
    Q_PROPERTY(qvec3 yxx READ yxx STORED false)
    Q_PROPERTY(qvec3 yxy READ yxy STORED false)
    Q_PROPERTY(qvec3 yxz READ yxz STORED false)
    Q_PROPERTY(qvec3 yxw READ yxw STORED false)
    Q_PROPERTY(qvec3 yyx READ yyx STORED false)
    Q_PROPERTY(qvec3 yyy READ yyy STORED false)
    Q_PROPERTY(qvec3 yyz READ yyz STORED false)
    Q_PROPERTY(qvec3 yyw READ yyw STORED false)
    Q_PROPERTY(qvec3 yzx READ yzx STORED false)
    Q_PROPERTY(qvec3 yzy READ yzy STORED false)
    Q_PROPERTY(qvec3 yzz READ yzz STORED false)
    Q_PROPERTY(qvec3 yzw READ yzw STORED false)
    Q_PROPERTY(qvec3 ywx READ ywx STORED false)
    Q_PROPERTY(qvec3 ywy READ ywy STORED false)
    Q_PROPERTY(qvec3 ywz READ ywz STORED false)
    Q_PROPERTY(qvec3 yww READ yww STORED false)
    Q_PROPERTY(qvec3 zxx READ zxx STORED false)
    Q_PROPERTY(qvec3 zxy READ zxy STORED false)
    Q_PROPERTY(qvec3 zxz READ zxz STORED false)
    Q_PROPERTY(qvec3 zxw READ zxw STORED false)
    Q_PROPERTY(qvec3 zyx READ zyx STORED false)
    Q_PROPERTY(qvec3 zyy READ zyy STORED false)
    Q_PROPERTY(qvec3 zyz READ zyz STORED false)
    Q_PROPERTY(qvec3 zyw READ zyw STORED false)
    Q_PROPERTY(qvec3 zzx READ zzx STORED false)
    Q_PROPERTY(qvec3 zzy READ zzy STORED false)
    Q_PROPERTY(qvec3 zzz READ zzz STORED false)
    Q_PROPERTY(qvec3 zzw READ zzw STORED false)
    Q_PROPERTY(qvec3 zwx READ zwx STORED false)
    Q_PROPERTY(qvec3 zwy READ zwy STORED false)
    Q_PROPERTY(qvec3 zwz READ zwz STORED false)
    Q_PROPERTY(qvec3 zww READ zww STORED false)
    Q_PROPERTY(qvec3 wxx READ wxx STORED false)
    Q_PROPERTY(qvec3 wxy READ wxy STORED false)
    Q_PROPERTY(qvec3 wxz READ wxz STORED false)
    Q_PROPERTY(qvec3 wxw READ wxw STORED false)
    Q_PROPERTY(qvec3 wyx READ wyx STORED false)
    Q_PROPERTY(qvec3 wyy READ wyy STORED false)
    Q_PROPERTY(qvec3 wyz READ wyz STORED false)
    Q_PROPERTY(qvec3 wyw READ wyw STORED false)
    Q_PROPERTY(qvec3 wzx READ wzx STORED false)
    Q_PROPERTY(qvec3 wzy READ wzy STORED false)
    Q_PROPERTY(qvec3 wzz READ wzz STORED false)
    Q_PROPERTY(qvec3 wzw READ wzw STORED false)
    Q_PROPERTY(qvec3 wwx READ wwx STORED false)
    Q_PROPERTY(qvec3 wwy READ wwy STORED false)
    Q_PROPERTY(qvec3 wwz READ wwz STORED false)
    Q_PROPERTY(qvec3 www READ www STORED false)


    Q_PROPERTY(qvec4 xxxx READ xxxx STORED false)
    Q_PROPERTY(qvec4 xxxy READ xxxy STORED false)
    Q_PROPERTY(qvec4 xxxz READ xxxz STORED false)
    Q_PROPERTY(qvec4 xxxw READ xxxw STORED false)
    Q_PROPERTY(qvec4 xxyx READ xxyx STORED false)
    Q_PROPERTY(qvec4 xxyy READ xxyy STORED false)
    Q_PROPERTY(qvec4 xxyz READ xxyz STORED false)
    Q_PROPERTY(qvec4 xxyw READ xxyw STORED false)
    Q_PROPERTY(qvec4 xxzx READ xxzx STORED false)
    Q_PROPERTY(qvec4 xxzy READ xxzy STORED false)
    Q_PROPERTY(qvec4 xxzz READ xxzz STORED false)
    Q_PROPERTY(qvec4 xxzw READ xxzw STORED false)
    Q_PROPERTY(qvec4 xxwx READ xxwx STORED false)
    Q_PROPERTY(qvec4 xxwy READ xxwy STORED false)
    Q_PROPERTY(qvec4 xxwz READ xxwz STORED false)
    Q_PROPERTY(qvec4 xxww READ xxww STORED false)
    Q_PROPERTY(qvec4 xyxx READ xyxx STORED false)
    Q_PROPERTY(qvec4 xyxy READ xyxy STORED false)
    Q_PROPERTY(qvec4 xyxz READ xyxz STORED false)
    Q_PROPERTY(qvec4 xyxw READ xyxw STORED false)
    Q_PROPERTY(qvec4 xyyx READ xyyx STORED false)
    Q_PROPERTY(qvec4 xyyy READ xyyy STORED false)
    Q_PROPERTY(qvec4 xyyz READ xyyz STORED false)
    Q_PROPERTY(qvec4 xyyw READ xyyw STORED false)
    Q_PROPERTY(qvec4 xyzx READ xyzx STORED false)
    Q_PROPERTY(qvec4 xyzy READ xyzy STORED false)
    Q_PROPERTY(qvec4 xyzz READ xyzz STORED false)
    Q_PROPERTY(qvec4 xyzw READ xyzw STORED false)
    Q_PROPERTY(qvec4 xywx READ xywx STORED false)
    Q_PROPERTY(qvec4 xywy READ xywy STORED false)
    Q_PROPERTY(qvec4 xywz READ xywz STORED false)
    Q_PROPERTY(qvec4 xyww READ xyww STORED false)
    Q_PROPERTY(qvec4 xzxx READ xzxx STORED false)
    Q_PROPERTY(qvec4 xzxy READ xzxy STORED false)
    Q_PROPERTY(qvec4 xzxz READ xzxz STORED false)
    Q_PROPERTY(qvec4 xzxw READ xzxw STORED false)
    Q_PROPERTY(qvec4 xzyx READ xzyx STORED false)
    Q_PROPERTY(qvec4 xzyy READ xzyy STORED false)
    Q_PROPERTY(qvec4 xzyz READ xzyz STORED false)
    Q_PROPERTY(qvec4 xzyw READ xzyw STORED false)
    Q_PROPERTY(qvec4 xzzx READ xzzx STORED false)
    Q_PROPERTY(qvec4 xzzy READ xzzy STORED false)
    Q_PROPERTY(qvec4 xzzz READ xzzz STORED false)
    Q_PROPERTY(qvec4 xzzw READ xzzw STORED false)
    Q_PROPERTY(qvec4 xzwx READ xzwx STORED false)
    Q_PROPERTY(qvec4 xzwy READ xzwy STORED false)
    Q_PROPERTY(qvec4 xzwz READ xzwz STORED false)
    Q_PROPERTY(qvec4 xzww READ xzww STORED false)
    Q_PROPERTY(qvec4 xwxx READ xwxx STORED false)
    Q_PROPERTY(qvec4 xwxy READ xwxy STORED false)
    Q_PROPERTY(qvec4 xwxz READ xwxz STORED false)
    Q_PROPERTY(qvec4 xwxw READ xwxw STORED false)
    Q_PROPERTY(qvec4 xwyx READ xwyx STORED false)
    Q_PROPERTY(qvec4 xwyy READ xwyy STORED false)
    Q_PROPERTY(qvec4 xwyz READ xwyz STORED false)
    Q_PROPERTY(qvec4 xwyw READ xwyw STORED false)
    Q_PROPERTY(qvec4 xwzx READ xwzx STORED false)
    Q_PROPERTY(qvec4 xwzy READ xwzy STORED false)
    Q_PROPERTY(qvec4 xwzz READ xwzz STORED false)
    Q_PROPERTY(qvec4 xwzw READ xwzw STORED false)
    Q_PROPERTY(qvec4 xwwx READ xwwx STORED false)
    Q_PROPERTY(qvec4 xwwy READ xwwy STORED false)
    Q_PROPERTY(qvec4 xwwz READ xwwz STORED false)
    Q_PROPERTY(qvec4 xwww READ xwww STORED false)
    Q_PROPERTY(qvec4 yxxx READ yxxx STORED false)
    Q_PROPERTY(qvec4 yxxy READ yxxy STORED false)
    Q_PROPERTY(qvec4 yxxz READ yxxz STORED false)
    Q_PROPERTY(qvec4 yxxw READ yxxw STORED false)
    Q_PROPERTY(qvec4 yxyx READ yxyx STORED false)
    Q_PROPERTY(qvec4 yxyy READ yxyy STORED false)
    Q_PROPERTY(qvec4 yxyz READ yxyz STORED false)
    Q_PROPERTY(qvec4 yxyw READ yxyw STORED false)
    Q_PROPERTY(qvec4 yxzx READ yxzx STORED false)
    Q_PROPERTY(qvec4 yxzy READ yxzy STORED false)
    Q_PROPERTY(qvec4 yxzz READ yxzz STORED false)
    Q_PROPERTY(qvec4 yxzw READ yxzw STORED false)
    Q_PROPERTY(qvec4 yxwx READ yxwx STORED false)
    Q_PROPERTY(qvec4 yxwy READ yxwy STORED false)
    Q_PROPERTY(qvec4 yxwz READ yxwz STORED false)
    Q_PROPERTY(qvec4 yxww READ yxww STORED false)
    Q_PROPERTY(qvec4 yyxx READ yyxx STORED false)
    Q_PROPERTY(qvec4 yyxy READ yyxy STORED false)
    Q_PROPERTY(qvec4 yyxz READ yyxz STORED false)
    Q_PROPERTY(qvec4 yyxw READ yyxw STORED false)
    Q_PROPERTY(qvec4 yyyx READ yyyx STORED false)
    Q_PROPERTY(qvec4 yyyy READ yyyy STORED false)
    Q_PROPERTY(qvec4 yyyz READ yyyz STORED false)
    Q_PROPERTY(qvec4 yyyw READ yyyw STORED false)
    Q_PROPERTY(qvec4 yyzx READ yyzx STORED false)
    Q_PROPERTY(qvec4 yyzy READ yyzy STORED false)
    Q_PROPERTY(qvec4 yyzz READ yyzz STORED false)
    Q_PROPERTY(qvec4 yyzw READ yyzw STORED false)
    Q_PROPERTY(qvec4 yywx READ yywx STORED false)
    Q_PROPERTY(qvec4 yywy READ yywy STORED false)
    Q_PROPERTY(qvec4 yywz READ yywz STORED false)
    Q_PROPERTY(qvec4 yyww READ yyww STORED false)
    Q_PROPERTY(qvec4 yzxx READ yzxx STORED false)
    Q_PROPERTY(qvec4 yzxy READ yzxy STORED false)
    Q_PROPERTY(qvec4 yzxz READ yzxz STORED false)
    Q_PROPERTY(qvec4 yzxw READ yzxw STORED false)
    Q_PROPERTY(qvec4 yzyx READ yzyx STORED false)
    Q_PROPERTY(qvec4 yzyy READ yzyy STORED false)
    Q_PROPERTY(qvec4 yzyz READ yzyz STORED false)
    Q_PROPERTY(qvec4 yzyw READ yzyw STORED false)
    Q_PROPERTY(qvec4 yzzx READ yzzx STORED false)
    Q_PROPERTY(qvec4 yzzy READ yzzy STORED false)
    Q_PROPERTY(qvec4 yzzz READ yzzz STORED false)
    Q_PROPERTY(qvec4 yzzw READ yzzw STORED false)
    Q_PROPERTY(qvec4 yzwx READ yzwx STORED false)
    Q_PROPERTY(qvec4 yzwy READ yzwy STORED false)
    Q_PROPERTY(qvec4 yzwz READ yzwz STORED false)
    Q_PROPERTY(qvec4 yzww READ yzww STORED false)
    Q_PROPERTY(qvec4 ywxx READ ywxx STORED false)
    Q_PROPERTY(qvec4 ywxy READ ywxy STORED false)
    Q_PROPERTY(qvec4 ywxz READ ywxz STORED false)
    Q_PROPERTY(qvec4 ywxw READ ywxw STORED false)
    Q_PROPERTY(qvec4 ywyx READ ywyx STORED false)
    Q_PROPERTY(qvec4 ywyy READ ywyy STORED false)
    Q_PROPERTY(qvec4 ywyz READ ywyz STORED false)
    Q_PROPERTY(qvec4 ywyw READ ywyw STORED false)
    Q_PROPERTY(qvec4 ywzx READ ywzx STORED false)
    Q_PROPERTY(qvec4 ywzy READ ywzy STORED false)
    Q_PROPERTY(qvec4 ywzz READ ywzz STORED false)
    Q_PROPERTY(qvec4 ywzw READ ywzw STORED false)
    Q_PROPERTY(qvec4 ywwx READ ywwx STORED false)
    Q_PROPERTY(qvec4 ywwy READ ywwy STORED false)
    Q_PROPERTY(qvec4 ywwz READ ywwz STORED false)
    Q_PROPERTY(qvec4 ywww READ ywww STORED false)
    Q_PROPERTY(qvec4 zxxx READ zxxx STORED false)
    Q_PROPERTY(qvec4 zxxy READ zxxy STORED false)
    Q_PROPERTY(qvec4 zxxz READ zxxz STORED false)
    Q_PROPERTY(qvec4 zxxw READ zxxw STORED false)
    Q_PROPERTY(qvec4 zxyx READ zxyx STORED false)
    Q_PROPERTY(qvec4 zxyy READ zxyy STORED false)
    Q_PROPERTY(qvec4 zxyz READ zxyz STORED false)
    Q_PROPERTY(qvec4 zxyw READ zxyw STORED false)
    Q_PROPERTY(qvec4 zxzx READ zxzx STORED false)
    Q_PROPERTY(qvec4 zxzy READ zxzy STORED false)
    Q_PROPERTY(qvec4 zxzz READ zxzz STORED false)
    Q_PROPERTY(qvec4 zxzw READ zxzw STORED false)
    Q_PROPERTY(qvec4 zxwx READ zxwx STORED false)
    Q_PROPERTY(qvec4 zxwy READ zxwy STORED false)
    Q_PROPERTY(qvec4 zxwz READ zxwz STORED false)
    Q_PROPERTY(qvec4 zxww READ zxww STORED false)
    Q_PROPERTY(qvec4 zyxx READ zyxx STORED false)
    Q_PROPERTY(qvec4 zyxy READ zyxy STORED false)
    Q_PROPERTY(qvec4 zyxz READ zyxz STORED false)
    Q_PROPERTY(qvec4 zyxw READ zyxw STORED false)
    Q_PROPERTY(qvec4 zyyx READ zyyx STORED false)
    Q_PROPERTY(qvec4 zyyy READ zyyy STORED false)
    Q_PROPERTY(qvec4 zyyz READ zyyz STORED false)
    Q_PROPERTY(qvec4 zyyw READ zyyw STORED false)
    Q_PROPERTY(qvec4 zyzx READ zyzx STORED false)
    Q_PROPERTY(qvec4 zyzy READ zyzy STORED false)
    Q_PROPERTY(qvec4 zyzz READ zyzz STORED false)
    Q_PROPERTY(qvec4 zyzw READ zyzw STORED false)
    Q_PROPERTY(qvec4 zywx READ zywx STORED false)
    Q_PROPERTY(qvec4 zywy READ zywy STORED false)
    Q_PROPERTY(qvec4 zywz READ zywz STORED false)
    Q_PROPERTY(qvec4 zyww READ zyww STORED false)
    Q_PROPERTY(qvec4 zzxx READ zzxx STORED false)
    Q_PROPERTY(qvec4 zzxy READ zzxy STORED false)
    Q_PROPERTY(qvec4 zzxz READ zzxz STORED false)
    Q_PROPERTY(qvec4 zzxw READ zzxw STORED false)
    Q_PROPERTY(qvec4 zzyx READ zzyx STORED false)
    Q_PROPERTY(qvec4 zzyy READ zzyy STORED false)
    Q_PROPERTY(qvec4 zzyz READ zzyz STORED false)
    Q_PROPERTY(qvec4 zzyw READ zzyw STORED false)
    Q_PROPERTY(qvec4 zzzx READ zzzx STORED false)
    Q_PROPERTY(qvec4 zzzy READ zzzy STORED false)
    Q_PROPERTY(qvec4 zzzz READ zzzz STORED false)
    Q_PROPERTY(qvec4 zzzw READ zzzw STORED false)
    Q_PROPERTY(qvec4 zzwx READ zzwx STORED false)
    Q_PROPERTY(qvec4 zzwy READ zzwy STORED false)
    Q_PROPERTY(qvec4 zzwz READ zzwz STORED false)
    Q_PROPERTY(qvec4 zzww READ zzww STORED false)
    Q_PROPERTY(qvec4 zwxx READ zwxx STORED false)
    Q_PROPERTY(qvec4 zwxy READ zwxy STORED false)
    Q_PROPERTY(qvec4 zwxz READ zwxz STORED false)
    Q_PROPERTY(qvec4 zwxw READ zwxw STORED false)
    Q_PROPERTY(qvec4 zwyx READ zwyx STORED false)
    Q_PROPERTY(qvec4 zwyy READ zwyy STORED false)
    Q_PROPERTY(qvec4 zwyz READ zwyz STORED false)
    Q_PROPERTY(qvec4 zwyw READ zwyw STORED false)
    Q_PROPERTY(qvec4 zwzx READ zwzx STORED false)
    Q_PROPERTY(qvec4 zwzy READ zwzy STORED false)
    Q_PROPERTY(qvec4 zwzz READ zwzz STORED false)
    Q_PROPERTY(qvec4 zwzw READ zwzw STORED false)
    Q_PROPERTY(qvec4 zwwx READ zwwx STORED false)
    Q_PROPERTY(qvec4 zwwy READ zwwy STORED false)
    Q_PROPERTY(qvec4 zwwz READ zwwz STORED false)
    Q_PROPERTY(qvec4 zwww READ zwww STORED false)
    Q_PROPERTY(qvec4 wxxx READ wxxx STORED false)
    Q_PROPERTY(qvec4 wxxy READ wxxy STORED false)
    Q_PROPERTY(qvec4 wxxz READ wxxz STORED false)
    Q_PROPERTY(qvec4 wxxw READ wxxw STORED false)
    Q_PROPERTY(qvec4 wxyx READ wxyx STORED false)
    Q_PROPERTY(qvec4 wxyy READ wxyy STORED false)
    Q_PROPERTY(qvec4 wxyz READ wxyz STORED false)
    Q_PROPERTY(qvec4 wxyw READ wxyw STORED false)
    Q_PROPERTY(qvec4 wxzx READ wxzx STORED false)
    Q_PROPERTY(qvec4 wxzy READ wxzy STORED false)
    Q_PROPERTY(qvec4 wxzz READ wxzz STORED false)
    Q_PROPERTY(qvec4 wxzw READ wxzw STORED false)
    Q_PROPERTY(qvec4 wxwx READ wxwx STORED false)
    Q_PROPERTY(qvec4 wxwy READ wxwy STORED false)
    Q_PROPERTY(qvec4 wxwz READ wxwz STORED false)
    Q_PROPERTY(qvec4 wxww READ wxww STORED false)
    Q_PROPERTY(qvec4 wyxx READ wyxx STORED false)
    Q_PROPERTY(qvec4 wyxy READ wyxy STORED false)
    Q_PROPERTY(qvec4 wyxz READ wyxz STORED false)
    Q_PROPERTY(qvec4 wyxw READ wyxw STORED false)
    Q_PROPERTY(qvec4 wyyx READ wyyx STORED false)
    Q_PROPERTY(qvec4 wyyy READ wyyy STORED false)
    Q_PROPERTY(qvec4 wyyz READ wyyz STORED false)
    Q_PROPERTY(qvec4 wyyw READ wyyw STORED false)
    Q_PROPERTY(qvec4 wyzx READ wyzx STORED false)
    Q_PROPERTY(qvec4 wyzy READ wyzy STORED false)
    Q_PROPERTY(qvec4 wyzz READ wyzz STORED false)
    Q_PROPERTY(qvec4 wyzw READ wyzw STORED false)
    Q_PROPERTY(qvec4 wywx READ wywx STORED false)
    Q_PROPERTY(qvec4 wywy READ wywy STORED false)
    Q_PROPERTY(qvec4 wywz READ wywz STORED false)
    Q_PROPERTY(qvec4 wyww READ wyww STORED false)
    Q_PROPERTY(qvec4 wzxx READ wzxx STORED false)
    Q_PROPERTY(qvec4 wzxy READ wzxy STORED false)
    Q_PROPERTY(qvec4 wzxz READ wzxz STORED false)
    Q_PROPERTY(qvec4 wzxw READ wzxw STORED false)
    Q_PROPERTY(qvec4 wzyx READ wzyx STORED false)
    Q_PROPERTY(qvec4 wzyy READ wzyy STORED false)
    Q_PROPERTY(qvec4 wzyz READ wzyz STORED false)
    Q_PROPERTY(qvec4 wzyw READ wzyw STORED false)
    Q_PROPERTY(qvec4 wzzx READ wzzx STORED false)
    Q_PROPERTY(qvec4 wzzy READ wzzy STORED false)
    Q_PROPERTY(qvec4 wzzz READ wzzz STORED false)
    Q_PROPERTY(qvec4 wzzw READ wzzw STORED false)
    Q_PROPERTY(qvec4 wzwx READ wzwx STORED false)
    Q_PROPERTY(qvec4 wzwy READ wzwy STORED false)
    Q_PROPERTY(qvec4 wzwz READ wzwz STORED false)
    Q_PROPERTY(qvec4 wzww READ wzww STORED false)
    Q_PROPERTY(qvec4 wwxx READ wwxx STORED false)
    Q_PROPERTY(qvec4 wwxy READ wwxy STORED false)
    Q_PROPERTY(qvec4 wwxz READ wwxz STORED false)
    Q_PROPERTY(qvec4 wwxw READ wwxw STORED false)
    Q_PROPERTY(qvec4 wwyx READ wwyx STORED false)
    Q_PROPERTY(qvec4 wwyy READ wwyy STORED false)
    Q_PROPERTY(qvec4 wwyz READ wwyz STORED false)
    Q_PROPERTY(qvec4 wwyw READ wwyw STORED false)
    Q_PROPERTY(qvec4 wwzx READ wwzx STORED false)
    Q_PROPERTY(qvec4 wwzy READ wwzy STORED false)
    Q_PROPERTY(qvec4 wwzz READ wwzz STORED false)
    Q_PROPERTY(qvec4 wwzw READ wwzw STORED false)
    Q_PROPERTY(qvec4 wwwx READ wwwx STORED false)
    Q_PROPERTY(qvec4 wwwy READ wwwy STORED false)
    Q_PROPERTY(qvec4 wwwz READ wwwz STORED false)
    Q_PROPERTY(qvec4 wwww READ wwww STORED false)

public:

    Q_INVOKABLE float dotProduct(const qvec4 &other) const;
    Q_INVOKABLE qvec4 times(const qmat4 &matrix) const;
    Q_INVOKABLE qvec4 times(const qvec4 &other) const;
    Q_INVOKABLE qvec4 times(float factor) const;
    Q_INVOKABLE qvec4 plus(const qvec4 &other) const;
    Q_INVOKABLE qvec4 minus(const qvec4 &other) const;
    Q_INVOKABLE qvec4 normalized() const;
    Q_INVOKABLE float length() const;

    using vector4<float>::vector4;
    using QGenericMatrix<1,4,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};



class QUICKOPENGL_EXPORT qivec4 : public vector4<int>
{
    Q_GADGET
    Q_PROPERTY(int x READ x WRITE setX)
    Q_PROPERTY(int y READ y WRITE setY)
    Q_PROPERTY(int z READ z WRITE setZ)
    Q_PROPERTY(int w READ w WRITE setW)
    Q_PROPERTY(int r READ r WRITE setR)
    Q_PROPERTY(int g READ g WRITE setG)
    Q_PROPERTY(int b READ b WRITE setB)
    Q_PROPERTY(int a READ a WRITE setA)

    Q_PROPERTY(qivec2 xx READ xx STORED false)
    Q_PROPERTY(qivec2 xy READ xy STORED false)
    Q_PROPERTY(qivec2 xz READ xz STORED false)
    Q_PROPERTY(qivec2 xw READ xw STORED false)
    Q_PROPERTY(qivec2 yx READ yx STORED false)
    Q_PROPERTY(qivec2 yy READ yy STORED false)
    Q_PROPERTY(qivec2 yz READ yz STORED false)
    Q_PROPERTY(qivec2 yw READ yw STORED false)
    Q_PROPERTY(qivec2 zx READ zx STORED false)
    Q_PROPERTY(qivec2 zy READ zy STORED false)
    Q_PROPERTY(qivec2 zz READ zz STORED false)
    Q_PROPERTY(qivec2 zw READ zw STORED false)
    Q_PROPERTY(qivec2 wx READ wx STORED false)
    Q_PROPERTY(qivec2 wy READ wy STORED false)
    Q_PROPERTY(qivec2 wz READ wz STORED false)
    Q_PROPERTY(qivec2 ww READ ww STORED false)


    Q_PROPERTY(qivec3 xxx READ xxx STORED false)
    Q_PROPERTY(qivec3 xxy READ xxy STORED false)
    Q_PROPERTY(qivec3 xxz READ xxz STORED false)
    Q_PROPERTY(qivec3 xxw READ xxw STORED false)
    Q_PROPERTY(qivec3 xyx READ xyx STORED false)
    Q_PROPERTY(qivec3 xyy READ xyy STORED false)
    Q_PROPERTY(qivec3 xyz READ xyz STORED false)
    Q_PROPERTY(qivec3 xyw READ xyw STORED false)
    Q_PROPERTY(qivec3 xzx READ xzx STORED false)
    Q_PROPERTY(qivec3 xzy READ xzy STORED false)
    Q_PROPERTY(qivec3 xzz READ xzz STORED false)
    Q_PROPERTY(qivec3 xzw READ xzw STORED false)
    Q_PROPERTY(qivec3 xwx READ xwx STORED false)
    Q_PROPERTY(qivec3 xwy READ xwy STORED false)
    Q_PROPERTY(qivec3 xwz READ xwz STORED false)
    Q_PROPERTY(qivec3 xww READ xww STORED false)
    Q_PROPERTY(qivec3 yxx READ yxx STORED false)
    Q_PROPERTY(qivec3 yxy READ yxy STORED false)
    Q_PROPERTY(qivec3 yxz READ yxz STORED false)
    Q_PROPERTY(qivec3 yxw READ yxw STORED false)
    Q_PROPERTY(qivec3 yyx READ yyx STORED false)
    Q_PROPERTY(qivec3 yyy READ yyy STORED false)
    Q_PROPERTY(qivec3 yyz READ yyz STORED false)
    Q_PROPERTY(qivec3 yyw READ yyw STORED false)
    Q_PROPERTY(qivec3 yzx READ yzx STORED false)
    Q_PROPERTY(qivec3 yzy READ yzy STORED false)
    Q_PROPERTY(qivec3 yzz READ yzz STORED false)
    Q_PROPERTY(qivec3 yzw READ yzw STORED false)
    Q_PROPERTY(qivec3 ywx READ ywx STORED false)
    Q_PROPERTY(qivec3 ywy READ ywy STORED false)
    Q_PROPERTY(qivec3 ywz READ ywz STORED false)
    Q_PROPERTY(qivec3 yww READ yww STORED false)
    Q_PROPERTY(qivec3 zxx READ zxx STORED false)
    Q_PROPERTY(qivec3 zxy READ zxy STORED false)
    Q_PROPERTY(qivec3 zxz READ zxz STORED false)
    Q_PROPERTY(qivec3 zxw READ zxw STORED false)
    Q_PROPERTY(qivec3 zyx READ zyx STORED false)
    Q_PROPERTY(qivec3 zyy READ zyy STORED false)
    Q_PROPERTY(qivec3 zyz READ zyz STORED false)
    Q_PROPERTY(qivec3 zyw READ zyw STORED false)
    Q_PROPERTY(qivec3 zzx READ zzx STORED false)
    Q_PROPERTY(qivec3 zzy READ zzy STORED false)
    Q_PROPERTY(qivec3 zzz READ zzz STORED false)
    Q_PROPERTY(qivec3 zzw READ zzw STORED false)
    Q_PROPERTY(qivec3 zwx READ zwx STORED false)
    Q_PROPERTY(qivec3 zwy READ zwy STORED false)
    Q_PROPERTY(qivec3 zwz READ zwz STORED false)
    Q_PROPERTY(qivec3 zww READ zww STORED false)
    Q_PROPERTY(qivec3 wxx READ wxx STORED false)
    Q_PROPERTY(qivec3 wxy READ wxy STORED false)
    Q_PROPERTY(qivec3 wxz READ wxz STORED false)
    Q_PROPERTY(qivec3 wxw READ wxw STORED false)
    Q_PROPERTY(qivec3 wyx READ wyx STORED false)
    Q_PROPERTY(qivec3 wyy READ wyy STORED false)
    Q_PROPERTY(qivec3 wyz READ wyz STORED false)
    Q_PROPERTY(qivec3 wyw READ wyw STORED false)
    Q_PROPERTY(qivec3 wzx READ wzx STORED false)
    Q_PROPERTY(qivec3 wzy READ wzy STORED false)
    Q_PROPERTY(qivec3 wzz READ wzz STORED false)
    Q_PROPERTY(qivec3 wzw READ wzw STORED false)
    Q_PROPERTY(qivec3 wwx READ wwx STORED false)
    Q_PROPERTY(qivec3 wwy READ wwy STORED false)
    Q_PROPERTY(qivec3 wwz READ wwz STORED false)
    Q_PROPERTY(qivec3 www READ www STORED false)


    Q_PROPERTY(qivec4 xxxx READ xxxx STORED false)
    Q_PROPERTY(qivec4 xxxy READ xxxy STORED false)
    Q_PROPERTY(qivec4 xxxz READ xxxz STORED false)
    Q_PROPERTY(qivec4 xxxw READ xxxw STORED false)
    Q_PROPERTY(qivec4 xxyx READ xxyx STORED false)
    Q_PROPERTY(qivec4 xxyy READ xxyy STORED false)
    Q_PROPERTY(qivec4 xxyz READ xxyz STORED false)
    Q_PROPERTY(qivec4 xxyw READ xxyw STORED false)
    Q_PROPERTY(qivec4 xxzx READ xxzx STORED false)
    Q_PROPERTY(qivec4 xxzy READ xxzy STORED false)
    Q_PROPERTY(qivec4 xxzz READ xxzz STORED false)
    Q_PROPERTY(qivec4 xxzw READ xxzw STORED false)
    Q_PROPERTY(qivec4 xxwx READ xxwx STORED false)
    Q_PROPERTY(qivec4 xxwy READ xxwy STORED false)
    Q_PROPERTY(qivec4 xxwz READ xxwz STORED false)
    Q_PROPERTY(qivec4 xxww READ xxww STORED false)
    Q_PROPERTY(qivec4 xyxx READ xyxx STORED false)
    Q_PROPERTY(qivec4 xyxy READ xyxy STORED false)
    Q_PROPERTY(qivec4 xyxz READ xyxz STORED false)
    Q_PROPERTY(qivec4 xyxw READ xyxw STORED false)
    Q_PROPERTY(qivec4 xyyx READ xyyx STORED false)
    Q_PROPERTY(qivec4 xyyy READ xyyy STORED false)
    Q_PROPERTY(qivec4 xyyz READ xyyz STORED false)
    Q_PROPERTY(qivec4 xyyw READ xyyw STORED false)
    Q_PROPERTY(qivec4 xyzx READ xyzx STORED false)
    Q_PROPERTY(qivec4 xyzy READ xyzy STORED false)
    Q_PROPERTY(qivec4 xyzz READ xyzz STORED false)
    Q_PROPERTY(qivec4 xyzw READ xyzw STORED false)
    Q_PROPERTY(qivec4 xywx READ xywx STORED false)
    Q_PROPERTY(qivec4 xywy READ xywy STORED false)
    Q_PROPERTY(qivec4 xywz READ xywz STORED false)
    Q_PROPERTY(qivec4 xyww READ xyww STORED false)
    Q_PROPERTY(qivec4 xzxx READ xzxx STORED false)
    Q_PROPERTY(qivec4 xzxy READ xzxy STORED false)
    Q_PROPERTY(qivec4 xzxz READ xzxz STORED false)
    Q_PROPERTY(qivec4 xzxw READ xzxw STORED false)
    Q_PROPERTY(qivec4 xzyx READ xzyx STORED false)
    Q_PROPERTY(qivec4 xzyy READ xzyy STORED false)
    Q_PROPERTY(qivec4 xzyz READ xzyz STORED false)
    Q_PROPERTY(qivec4 xzyw READ xzyw STORED false)
    Q_PROPERTY(qivec4 xzzx READ xzzx STORED false)
    Q_PROPERTY(qivec4 xzzy READ xzzy STORED false)
    Q_PROPERTY(qivec4 xzzz READ xzzz STORED false)
    Q_PROPERTY(qivec4 xzzw READ xzzw STORED false)
    Q_PROPERTY(qivec4 xzwx READ xzwx STORED false)
    Q_PROPERTY(qivec4 xzwy READ xzwy STORED false)
    Q_PROPERTY(qivec4 xzwz READ xzwz STORED false)
    Q_PROPERTY(qivec4 xzww READ xzww STORED false)
    Q_PROPERTY(qivec4 xwxx READ xwxx STORED false)
    Q_PROPERTY(qivec4 xwxy READ xwxy STORED false)
    Q_PROPERTY(qivec4 xwxz READ xwxz STORED false)
    Q_PROPERTY(qivec4 xwxw READ xwxw STORED false)
    Q_PROPERTY(qivec4 xwyx READ xwyx STORED false)
    Q_PROPERTY(qivec4 xwyy READ xwyy STORED false)
    Q_PROPERTY(qivec4 xwyz READ xwyz STORED false)
    Q_PROPERTY(qivec4 xwyw READ xwyw STORED false)
    Q_PROPERTY(qivec4 xwzx READ xwzx STORED false)
    Q_PROPERTY(qivec4 xwzy READ xwzy STORED false)
    Q_PROPERTY(qivec4 xwzz READ xwzz STORED false)
    Q_PROPERTY(qivec4 xwzw READ xwzw STORED false)
    Q_PROPERTY(qivec4 xwwx READ xwwx STORED false)
    Q_PROPERTY(qivec4 xwwy READ xwwy STORED false)
    Q_PROPERTY(qivec4 xwwz READ xwwz STORED false)
    Q_PROPERTY(qivec4 xwww READ xwww STORED false)
    Q_PROPERTY(qivec4 yxxx READ yxxx STORED false)
    Q_PROPERTY(qivec4 yxxy READ yxxy STORED false)
    Q_PROPERTY(qivec4 yxxz READ yxxz STORED false)
    Q_PROPERTY(qivec4 yxxw READ yxxw STORED false)
    Q_PROPERTY(qivec4 yxyx READ yxyx STORED false)
    Q_PROPERTY(qivec4 yxyy READ yxyy STORED false)
    Q_PROPERTY(qivec4 yxyz READ yxyz STORED false)
    Q_PROPERTY(qivec4 yxyw READ yxyw STORED false)
    Q_PROPERTY(qivec4 yxzx READ yxzx STORED false)
    Q_PROPERTY(qivec4 yxzy READ yxzy STORED false)
    Q_PROPERTY(qivec4 yxzz READ yxzz STORED false)
    Q_PROPERTY(qivec4 yxzw READ yxzw STORED false)
    Q_PROPERTY(qivec4 yxwx READ yxwx STORED false)
    Q_PROPERTY(qivec4 yxwy READ yxwy STORED false)
    Q_PROPERTY(qivec4 yxwz READ yxwz STORED false)
    Q_PROPERTY(qivec4 yxww READ yxww STORED false)
    Q_PROPERTY(qivec4 yyxx READ yyxx STORED false)
    Q_PROPERTY(qivec4 yyxy READ yyxy STORED false)
    Q_PROPERTY(qivec4 yyxz READ yyxz STORED false)
    Q_PROPERTY(qivec4 yyxw READ yyxw STORED false)
    Q_PROPERTY(qivec4 yyyx READ yyyx STORED false)
    Q_PROPERTY(qivec4 yyyy READ yyyy STORED false)
    Q_PROPERTY(qivec4 yyyz READ yyyz STORED false)
    Q_PROPERTY(qivec4 yyyw READ yyyw STORED false)
    Q_PROPERTY(qivec4 yyzx READ yyzx STORED false)
    Q_PROPERTY(qivec4 yyzy READ yyzy STORED false)
    Q_PROPERTY(qivec4 yyzz READ yyzz STORED false)
    Q_PROPERTY(qivec4 yyzw READ yyzw STORED false)
    Q_PROPERTY(qivec4 yywx READ yywx STORED false)
    Q_PROPERTY(qivec4 yywy READ yywy STORED false)
    Q_PROPERTY(qivec4 yywz READ yywz STORED false)
    Q_PROPERTY(qivec4 yyww READ yyww STORED false)
    Q_PROPERTY(qivec4 yzxx READ yzxx STORED false)
    Q_PROPERTY(qivec4 yzxy READ yzxy STORED false)
    Q_PROPERTY(qivec4 yzxz READ yzxz STORED false)
    Q_PROPERTY(qivec4 yzxw READ yzxw STORED false)
    Q_PROPERTY(qivec4 yzyx READ yzyx STORED false)
    Q_PROPERTY(qivec4 yzyy READ yzyy STORED false)
    Q_PROPERTY(qivec4 yzyz READ yzyz STORED false)
    Q_PROPERTY(qivec4 yzyw READ yzyw STORED false)
    Q_PROPERTY(qivec4 yzzx READ yzzx STORED false)
    Q_PROPERTY(qivec4 yzzy READ yzzy STORED false)
    Q_PROPERTY(qivec4 yzzz READ yzzz STORED false)
    Q_PROPERTY(qivec4 yzzw READ yzzw STORED false)
    Q_PROPERTY(qivec4 yzwx READ yzwx STORED false)
    Q_PROPERTY(qivec4 yzwy READ yzwy STORED false)
    Q_PROPERTY(qivec4 yzwz READ yzwz STORED false)
    Q_PROPERTY(qivec4 yzww READ yzww STORED false)
    Q_PROPERTY(qivec4 ywxx READ ywxx STORED false)
    Q_PROPERTY(qivec4 ywxy READ ywxy STORED false)
    Q_PROPERTY(qivec4 ywxz READ ywxz STORED false)
    Q_PROPERTY(qivec4 ywxw READ ywxw STORED false)
    Q_PROPERTY(qivec4 ywyx READ ywyx STORED false)
    Q_PROPERTY(qivec4 ywyy READ ywyy STORED false)
    Q_PROPERTY(qivec4 ywyz READ ywyz STORED false)
    Q_PROPERTY(qivec4 ywyw READ ywyw STORED false)
    Q_PROPERTY(qivec4 ywzx READ ywzx STORED false)
    Q_PROPERTY(qivec4 ywzy READ ywzy STORED false)
    Q_PROPERTY(qivec4 ywzz READ ywzz STORED false)
    Q_PROPERTY(qivec4 ywzw READ ywzw STORED false)
    Q_PROPERTY(qivec4 ywwx READ ywwx STORED false)
    Q_PROPERTY(qivec4 ywwy READ ywwy STORED false)
    Q_PROPERTY(qivec4 ywwz READ ywwz STORED false)
    Q_PROPERTY(qivec4 ywww READ ywww STORED false)
    Q_PROPERTY(qivec4 zxxx READ zxxx STORED false)
    Q_PROPERTY(qivec4 zxxy READ zxxy STORED false)
    Q_PROPERTY(qivec4 zxxz READ zxxz STORED false)
    Q_PROPERTY(qivec4 zxxw READ zxxw STORED false)
    Q_PROPERTY(qivec4 zxyx READ zxyx STORED false)
    Q_PROPERTY(qivec4 zxyy READ zxyy STORED false)
    Q_PROPERTY(qivec4 zxyz READ zxyz STORED false)
    Q_PROPERTY(qivec4 zxyw READ zxyw STORED false)
    Q_PROPERTY(qivec4 zxzx READ zxzx STORED false)
    Q_PROPERTY(qivec4 zxzy READ zxzy STORED false)
    Q_PROPERTY(qivec4 zxzz READ zxzz STORED false)
    Q_PROPERTY(qivec4 zxzw READ zxzw STORED false)
    Q_PROPERTY(qivec4 zxwx READ zxwx STORED false)
    Q_PROPERTY(qivec4 zxwy READ zxwy STORED false)
    Q_PROPERTY(qivec4 zxwz READ zxwz STORED false)
    Q_PROPERTY(qivec4 zxww READ zxww STORED false)
    Q_PROPERTY(qivec4 zyxx READ zyxx STORED false)
    Q_PROPERTY(qivec4 zyxy READ zyxy STORED false)
    Q_PROPERTY(qivec4 zyxz READ zyxz STORED false)
    Q_PROPERTY(qivec4 zyxw READ zyxw STORED false)
    Q_PROPERTY(qivec4 zyyx READ zyyx STORED false)
    Q_PROPERTY(qivec4 zyyy READ zyyy STORED false)
    Q_PROPERTY(qivec4 zyyz READ zyyz STORED false)
    Q_PROPERTY(qivec4 zyyw READ zyyw STORED false)
    Q_PROPERTY(qivec4 zyzx READ zyzx STORED false)
    Q_PROPERTY(qivec4 zyzy READ zyzy STORED false)
    Q_PROPERTY(qivec4 zyzz READ zyzz STORED false)
    Q_PROPERTY(qivec4 zyzw READ zyzw STORED false)
    Q_PROPERTY(qivec4 zywx READ zywx STORED false)
    Q_PROPERTY(qivec4 zywy READ zywy STORED false)
    Q_PROPERTY(qivec4 zywz READ zywz STORED false)
    Q_PROPERTY(qivec4 zyww READ zyww STORED false)
    Q_PROPERTY(qivec4 zzxx READ zzxx STORED false)
    Q_PROPERTY(qivec4 zzxy READ zzxy STORED false)
    Q_PROPERTY(qivec4 zzxz READ zzxz STORED false)
    Q_PROPERTY(qivec4 zzxw READ zzxw STORED false)
    Q_PROPERTY(qivec4 zzyx READ zzyx STORED false)
    Q_PROPERTY(qivec4 zzyy READ zzyy STORED false)
    Q_PROPERTY(qivec4 zzyz READ zzyz STORED false)
    Q_PROPERTY(qivec4 zzyw READ zzyw STORED false)
    Q_PROPERTY(qivec4 zzzx READ zzzx STORED false)
    Q_PROPERTY(qivec4 zzzy READ zzzy STORED false)
    Q_PROPERTY(qivec4 zzzz READ zzzz STORED false)
    Q_PROPERTY(qivec4 zzzw READ zzzw STORED false)
    Q_PROPERTY(qivec4 zzwx READ zzwx STORED false)
    Q_PROPERTY(qivec4 zzwy READ zzwy STORED false)
    Q_PROPERTY(qivec4 zzwz READ zzwz STORED false)
    Q_PROPERTY(qivec4 zzww READ zzww STORED false)
    Q_PROPERTY(qivec4 zwxx READ zwxx STORED false)
    Q_PROPERTY(qivec4 zwxy READ zwxy STORED false)
    Q_PROPERTY(qivec4 zwxz READ zwxz STORED false)
    Q_PROPERTY(qivec4 zwxw READ zwxw STORED false)
    Q_PROPERTY(qivec4 zwyx READ zwyx STORED false)
    Q_PROPERTY(qivec4 zwyy READ zwyy STORED false)
    Q_PROPERTY(qivec4 zwyz READ zwyz STORED false)
    Q_PROPERTY(qivec4 zwyw READ zwyw STORED false)
    Q_PROPERTY(qivec4 zwzx READ zwzx STORED false)
    Q_PROPERTY(qivec4 zwzy READ zwzy STORED false)
    Q_PROPERTY(qivec4 zwzz READ zwzz STORED false)
    Q_PROPERTY(qivec4 zwzw READ zwzw STORED false)
    Q_PROPERTY(qivec4 zwwx READ zwwx STORED false)
    Q_PROPERTY(qivec4 zwwy READ zwwy STORED false)
    Q_PROPERTY(qivec4 zwwz READ zwwz STORED false)
    Q_PROPERTY(qivec4 zwww READ zwww STORED false)
    Q_PROPERTY(qivec4 wxxx READ wxxx STORED false)
    Q_PROPERTY(qivec4 wxxy READ wxxy STORED false)
    Q_PROPERTY(qivec4 wxxz READ wxxz STORED false)
    Q_PROPERTY(qivec4 wxxw READ wxxw STORED false)
    Q_PROPERTY(qivec4 wxyx READ wxyx STORED false)
    Q_PROPERTY(qivec4 wxyy READ wxyy STORED false)
    Q_PROPERTY(qivec4 wxyz READ wxyz STORED false)
    Q_PROPERTY(qivec4 wxyw READ wxyw STORED false)
    Q_PROPERTY(qivec4 wxzx READ wxzx STORED false)
    Q_PROPERTY(qivec4 wxzy READ wxzy STORED false)
    Q_PROPERTY(qivec4 wxzz READ wxzz STORED false)
    Q_PROPERTY(qivec4 wxzw READ wxzw STORED false)
    Q_PROPERTY(qivec4 wxwx READ wxwx STORED false)
    Q_PROPERTY(qivec4 wxwy READ wxwy STORED false)
    Q_PROPERTY(qivec4 wxwz READ wxwz STORED false)
    Q_PROPERTY(qivec4 wxww READ wxww STORED false)
    Q_PROPERTY(qivec4 wyxx READ wyxx STORED false)
    Q_PROPERTY(qivec4 wyxy READ wyxy STORED false)
    Q_PROPERTY(qivec4 wyxz READ wyxz STORED false)
    Q_PROPERTY(qivec4 wyxw READ wyxw STORED false)
    Q_PROPERTY(qivec4 wyyx READ wyyx STORED false)
    Q_PROPERTY(qivec4 wyyy READ wyyy STORED false)
    Q_PROPERTY(qivec4 wyyz READ wyyz STORED false)
    Q_PROPERTY(qivec4 wyyw READ wyyw STORED false)
    Q_PROPERTY(qivec4 wyzx READ wyzx STORED false)
    Q_PROPERTY(qivec4 wyzy READ wyzy STORED false)
    Q_PROPERTY(qivec4 wyzz READ wyzz STORED false)
    Q_PROPERTY(qivec4 wyzw READ wyzw STORED false)
    Q_PROPERTY(qivec4 wywx READ wywx STORED false)
    Q_PROPERTY(qivec4 wywy READ wywy STORED false)
    Q_PROPERTY(qivec4 wywz READ wywz STORED false)
    Q_PROPERTY(qivec4 wyww READ wyww STORED false)
    Q_PROPERTY(qivec4 wzxx READ wzxx STORED false)
    Q_PROPERTY(qivec4 wzxy READ wzxy STORED false)
    Q_PROPERTY(qivec4 wzxz READ wzxz STORED false)
    Q_PROPERTY(qivec4 wzxw READ wzxw STORED false)
    Q_PROPERTY(qivec4 wzyx READ wzyx STORED false)
    Q_PROPERTY(qivec4 wzyy READ wzyy STORED false)
    Q_PROPERTY(qivec4 wzyz READ wzyz STORED false)
    Q_PROPERTY(qivec4 wzyw READ wzyw STORED false)
    Q_PROPERTY(qivec4 wzzx READ wzzx STORED false)
    Q_PROPERTY(qivec4 wzzy READ wzzy STORED false)
    Q_PROPERTY(qivec4 wzzz READ wzzz STORED false)
    Q_PROPERTY(qivec4 wzzw READ wzzw STORED false)
    Q_PROPERTY(qivec4 wzwx READ wzwx STORED false)
    Q_PROPERTY(qivec4 wzwy READ wzwy STORED false)
    Q_PROPERTY(qivec4 wzwz READ wzwz STORED false)
    Q_PROPERTY(qivec4 wzww READ wzww STORED false)
    Q_PROPERTY(qivec4 wwxx READ wwxx STORED false)
    Q_PROPERTY(qivec4 wwxy READ wwxy STORED false)
    Q_PROPERTY(qivec4 wwxz READ wwxz STORED false)
    Q_PROPERTY(qivec4 wwxw READ wwxw STORED false)
    Q_PROPERTY(qivec4 wwyx READ wwyx STORED false)
    Q_PROPERTY(qivec4 wwyy READ wwyy STORED false)
    Q_PROPERTY(qivec4 wwyz READ wwyz STORED false)
    Q_PROPERTY(qivec4 wwyw READ wwyw STORED false)
    Q_PROPERTY(qivec4 wwzx READ wwzx STORED false)
    Q_PROPERTY(qivec4 wwzy READ wwzy STORED false)
    Q_PROPERTY(qivec4 wwzz READ wwzz STORED false)
    Q_PROPERTY(qivec4 wwzw READ wwzw STORED false)
    Q_PROPERTY(qivec4 wwwx READ wwwx STORED false)
    Q_PROPERTY(qivec4 wwwy READ wwwy STORED false)
    Q_PROPERTY(qivec4 wwwz READ wwwz STORED false)
    Q_PROPERTY(qivec4 wwww READ wwww STORED false)

public:

    using vector4<int>::vector4;
    using QGenericMatrix<1,4,int>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class QUICKOPENGL_EXPORT quvec4 : public vector4<unsigned int>
{
    Q_GADGET
    Q_PROPERTY(unsigned int x READ x WRITE setX)
    Q_PROPERTY(unsigned int y READ y WRITE setY)
    Q_PROPERTY(unsigned int z READ z WRITE setZ)
    Q_PROPERTY(unsigned int w READ w WRITE setW)
    Q_PROPERTY(unsigned int r READ r WRITE setR)
    Q_PROPERTY(unsigned int g READ g WRITE setG)
    Q_PROPERTY(unsigned int b READ b WRITE setB)
    Q_PROPERTY(unsigned int a READ a WRITE setA)


    Q_PROPERTY(quvec2 xx READ xx STORED false)
    Q_PROPERTY(quvec2 xy READ xy STORED false)
    Q_PROPERTY(quvec2 xz READ xz STORED false)
    Q_PROPERTY(quvec2 xw READ xw STORED false)
    Q_PROPERTY(quvec2 yx READ yx STORED false)
    Q_PROPERTY(quvec2 yy READ yy STORED false)
    Q_PROPERTY(quvec2 yz READ yz STORED false)
    Q_PROPERTY(quvec2 yw READ yw STORED false)
    Q_PROPERTY(quvec2 zx READ zx STORED false)
    Q_PROPERTY(quvec2 zy READ zy STORED false)
    Q_PROPERTY(quvec2 zz READ zz STORED false)
    Q_PROPERTY(quvec2 zw READ zw STORED false)
    Q_PROPERTY(quvec2 wx READ wx STORED false)
    Q_PROPERTY(quvec2 wy READ wy STORED false)
    Q_PROPERTY(quvec2 wz READ wz STORED false)
    Q_PROPERTY(quvec2 ww READ ww STORED false)

    Q_PROPERTY(quvec3 xxx READ xxx STORED false)
    Q_PROPERTY(quvec3 xxy READ xxy STORED false)
    Q_PROPERTY(quvec3 xxz READ xxz STORED false)
    Q_PROPERTY(quvec3 xxw READ xxw STORED false)
    Q_PROPERTY(quvec3 xyx READ xyx STORED false)
    Q_PROPERTY(quvec3 xyy READ xyy STORED false)
    Q_PROPERTY(quvec3 xyz READ xyz STORED false)
    Q_PROPERTY(quvec3 xyw READ xyw STORED false)
    Q_PROPERTY(quvec3 xzx READ xzx STORED false)
    Q_PROPERTY(quvec3 xzy READ xzy STORED false)
    Q_PROPERTY(quvec3 xzz READ xzz STORED false)
    Q_PROPERTY(quvec3 xzw READ xzw STORED false)
    Q_PROPERTY(quvec3 xwx READ xwx STORED false)
    Q_PROPERTY(quvec3 xwy READ xwy STORED false)
    Q_PROPERTY(quvec3 xwz READ xwz STORED false)
    Q_PROPERTY(quvec3 xww READ xww STORED false)
    Q_PROPERTY(quvec3 yxx READ yxx STORED false)
    Q_PROPERTY(quvec3 yxy READ yxy STORED false)
    Q_PROPERTY(quvec3 yxz READ yxz STORED false)
    Q_PROPERTY(quvec3 yxw READ yxw STORED false)
    Q_PROPERTY(quvec3 yyx READ yyx STORED false)
    Q_PROPERTY(quvec3 yyy READ yyy STORED false)
    Q_PROPERTY(quvec3 yyz READ yyz STORED false)
    Q_PROPERTY(quvec3 yyw READ yyw STORED false)
    Q_PROPERTY(quvec3 yzx READ yzx STORED false)
    Q_PROPERTY(quvec3 yzy READ yzy STORED false)
    Q_PROPERTY(quvec3 yzz READ yzz STORED false)
    Q_PROPERTY(quvec3 yzw READ yzw STORED false)
    Q_PROPERTY(quvec3 ywx READ ywx STORED false)
    Q_PROPERTY(quvec3 ywy READ ywy STORED false)
    Q_PROPERTY(quvec3 ywz READ ywz STORED false)
    Q_PROPERTY(quvec3 yww READ yww STORED false)
    Q_PROPERTY(quvec3 zxx READ zxx STORED false)
    Q_PROPERTY(quvec3 zxy READ zxy STORED false)
    Q_PROPERTY(quvec3 zxz READ zxz STORED false)
    Q_PROPERTY(quvec3 zxw READ zxw STORED false)
    Q_PROPERTY(quvec3 zyx READ zyx STORED false)
    Q_PROPERTY(quvec3 zyy READ zyy STORED false)
    Q_PROPERTY(quvec3 zyz READ zyz STORED false)
    Q_PROPERTY(quvec3 zyw READ zyw STORED false)
    Q_PROPERTY(quvec3 zzx READ zzx STORED false)
    Q_PROPERTY(quvec3 zzy READ zzy STORED false)
    Q_PROPERTY(quvec3 zzz READ zzz STORED false)
    Q_PROPERTY(quvec3 zzw READ zzw STORED false)
    Q_PROPERTY(quvec3 zwx READ zwx STORED false)
    Q_PROPERTY(quvec3 zwy READ zwy STORED false)
    Q_PROPERTY(quvec3 zwz READ zwz STORED false)
    Q_PROPERTY(quvec3 zww READ zww STORED false)
    Q_PROPERTY(quvec3 wxx READ wxx STORED false)
    Q_PROPERTY(quvec3 wxy READ wxy STORED false)
    Q_PROPERTY(quvec3 wxz READ wxz STORED false)
    Q_PROPERTY(quvec3 wxw READ wxw STORED false)
    Q_PROPERTY(quvec3 wyx READ wyx STORED false)
    Q_PROPERTY(quvec3 wyy READ wyy STORED false)
    Q_PROPERTY(quvec3 wyz READ wyz STORED false)
    Q_PROPERTY(quvec3 wyw READ wyw STORED false)
    Q_PROPERTY(quvec3 wzx READ wzx STORED false)
    Q_PROPERTY(quvec3 wzy READ wzy STORED false)
    Q_PROPERTY(quvec3 wzz READ wzz STORED false)
    Q_PROPERTY(quvec3 wzw READ wzw STORED false)
    Q_PROPERTY(quvec3 wwx READ wwx STORED false)
    Q_PROPERTY(quvec3 wwy READ wwy STORED false)
    Q_PROPERTY(quvec3 wwz READ wwz STORED false)
    Q_PROPERTY(quvec3 www READ www STORED false)


    Q_PROPERTY(quvec4 xxxx READ xxxx STORED false)
    Q_PROPERTY(quvec4 xxxy READ xxxy STORED false)
    Q_PROPERTY(quvec4 xxxz READ xxxz STORED false)
    Q_PROPERTY(quvec4 xxxw READ xxxw STORED false)
    Q_PROPERTY(quvec4 xxyx READ xxyx STORED false)
    Q_PROPERTY(quvec4 xxyy READ xxyy STORED false)
    Q_PROPERTY(quvec4 xxyz READ xxyz STORED false)
    Q_PROPERTY(quvec4 xxyw READ xxyw STORED false)
    Q_PROPERTY(quvec4 xxzx READ xxzx STORED false)
    Q_PROPERTY(quvec4 xxzy READ xxzy STORED false)
    Q_PROPERTY(quvec4 xxzz READ xxzz STORED false)
    Q_PROPERTY(quvec4 xxzw READ xxzw STORED false)
    Q_PROPERTY(quvec4 xxwx READ xxwx STORED false)
    Q_PROPERTY(quvec4 xxwy READ xxwy STORED false)
    Q_PROPERTY(quvec4 xxwz READ xxwz STORED false)
    Q_PROPERTY(quvec4 xxww READ xxww STORED false)
    Q_PROPERTY(quvec4 xyxx READ xyxx STORED false)
    Q_PROPERTY(quvec4 xyxy READ xyxy STORED false)
    Q_PROPERTY(quvec4 xyxz READ xyxz STORED false)
    Q_PROPERTY(quvec4 xyxw READ xyxw STORED false)
    Q_PROPERTY(quvec4 xyyx READ xyyx STORED false)
    Q_PROPERTY(quvec4 xyyy READ xyyy STORED false)
    Q_PROPERTY(quvec4 xyyz READ xyyz STORED false)
    Q_PROPERTY(quvec4 xyyw READ xyyw STORED false)
    Q_PROPERTY(quvec4 xyzx READ xyzx STORED false)
    Q_PROPERTY(quvec4 xyzy READ xyzy STORED false)
    Q_PROPERTY(quvec4 xyzz READ xyzz STORED false)
    Q_PROPERTY(quvec4 xyzw READ xyzw STORED false)
    Q_PROPERTY(quvec4 xywx READ xywx STORED false)
    Q_PROPERTY(quvec4 xywy READ xywy STORED false)
    Q_PROPERTY(quvec4 xywz READ xywz STORED false)
    Q_PROPERTY(quvec4 xyww READ xyww STORED false)
    Q_PROPERTY(quvec4 xzxx READ xzxx STORED false)
    Q_PROPERTY(quvec4 xzxy READ xzxy STORED false)
    Q_PROPERTY(quvec4 xzxz READ xzxz STORED false)
    Q_PROPERTY(quvec4 xzxw READ xzxw STORED false)
    Q_PROPERTY(quvec4 xzyx READ xzyx STORED false)
    Q_PROPERTY(quvec4 xzyy READ xzyy STORED false)
    Q_PROPERTY(quvec4 xzyz READ xzyz STORED false)
    Q_PROPERTY(quvec4 xzyw READ xzyw STORED false)
    Q_PROPERTY(quvec4 xzzx READ xzzx STORED false)
    Q_PROPERTY(quvec4 xzzy READ xzzy STORED false)
    Q_PROPERTY(quvec4 xzzz READ xzzz STORED false)
    Q_PROPERTY(quvec4 xzzw READ xzzw STORED false)
    Q_PROPERTY(quvec4 xzwx READ xzwx STORED false)
    Q_PROPERTY(quvec4 xzwy READ xzwy STORED false)
    Q_PROPERTY(quvec4 xzwz READ xzwz STORED false)
    Q_PROPERTY(quvec4 xzww READ xzww STORED false)
    Q_PROPERTY(quvec4 xwxx READ xwxx STORED false)
    Q_PROPERTY(quvec4 xwxy READ xwxy STORED false)
    Q_PROPERTY(quvec4 xwxz READ xwxz STORED false)
    Q_PROPERTY(quvec4 xwxw READ xwxw STORED false)
    Q_PROPERTY(quvec4 xwyx READ xwyx STORED false)
    Q_PROPERTY(quvec4 xwyy READ xwyy STORED false)
    Q_PROPERTY(quvec4 xwyz READ xwyz STORED false)
    Q_PROPERTY(quvec4 xwyw READ xwyw STORED false)
    Q_PROPERTY(quvec4 xwzx READ xwzx STORED false)
    Q_PROPERTY(quvec4 xwzy READ xwzy STORED false)
    Q_PROPERTY(quvec4 xwzz READ xwzz STORED false)
    Q_PROPERTY(quvec4 xwzw READ xwzw STORED false)
    Q_PROPERTY(quvec4 xwwx READ xwwx STORED false)
    Q_PROPERTY(quvec4 xwwy READ xwwy STORED false)
    Q_PROPERTY(quvec4 xwwz READ xwwz STORED false)
    Q_PROPERTY(quvec4 xwww READ xwww STORED false)
    Q_PROPERTY(quvec4 yxxx READ yxxx STORED false)
    Q_PROPERTY(quvec4 yxxy READ yxxy STORED false)
    Q_PROPERTY(quvec4 yxxz READ yxxz STORED false)
    Q_PROPERTY(quvec4 yxxw READ yxxw STORED false)
    Q_PROPERTY(quvec4 yxyx READ yxyx STORED false)
    Q_PROPERTY(quvec4 yxyy READ yxyy STORED false)
    Q_PROPERTY(quvec4 yxyz READ yxyz STORED false)
    Q_PROPERTY(quvec4 yxyw READ yxyw STORED false)
    Q_PROPERTY(quvec4 yxzx READ yxzx STORED false)
    Q_PROPERTY(quvec4 yxzy READ yxzy STORED false)
    Q_PROPERTY(quvec4 yxzz READ yxzz STORED false)
    Q_PROPERTY(quvec4 yxzw READ yxzw STORED false)
    Q_PROPERTY(quvec4 yxwx READ yxwx STORED false)
    Q_PROPERTY(quvec4 yxwy READ yxwy STORED false)
    Q_PROPERTY(quvec4 yxwz READ yxwz STORED false)
    Q_PROPERTY(quvec4 yxww READ yxww STORED false)
    Q_PROPERTY(quvec4 yyxx READ yyxx STORED false)
    Q_PROPERTY(quvec4 yyxy READ yyxy STORED false)
    Q_PROPERTY(quvec4 yyxz READ yyxz STORED false)
    Q_PROPERTY(quvec4 yyxw READ yyxw STORED false)
    Q_PROPERTY(quvec4 yyyx READ yyyx STORED false)
    Q_PROPERTY(quvec4 yyyy READ yyyy STORED false)
    Q_PROPERTY(quvec4 yyyz READ yyyz STORED false)
    Q_PROPERTY(quvec4 yyyw READ yyyw STORED false)
    Q_PROPERTY(quvec4 yyzx READ yyzx STORED false)
    Q_PROPERTY(quvec4 yyzy READ yyzy STORED false)
    Q_PROPERTY(quvec4 yyzz READ yyzz STORED false)
    Q_PROPERTY(quvec4 yyzw READ yyzw STORED false)
    Q_PROPERTY(quvec4 yywx READ yywx STORED false)
    Q_PROPERTY(quvec4 yywy READ yywy STORED false)
    Q_PROPERTY(quvec4 yywz READ yywz STORED false)
    Q_PROPERTY(quvec4 yyww READ yyww STORED false)
    Q_PROPERTY(quvec4 yzxx READ yzxx STORED false)
    Q_PROPERTY(quvec4 yzxy READ yzxy STORED false)
    Q_PROPERTY(quvec4 yzxz READ yzxz STORED false)
    Q_PROPERTY(quvec4 yzxw READ yzxw STORED false)
    Q_PROPERTY(quvec4 yzyx READ yzyx STORED false)
    Q_PROPERTY(quvec4 yzyy READ yzyy STORED false)
    Q_PROPERTY(quvec4 yzyz READ yzyz STORED false)
    Q_PROPERTY(quvec4 yzyw READ yzyw STORED false)
    Q_PROPERTY(quvec4 yzzx READ yzzx STORED false)
    Q_PROPERTY(quvec4 yzzy READ yzzy STORED false)
    Q_PROPERTY(quvec4 yzzz READ yzzz STORED false)
    Q_PROPERTY(quvec4 yzzw READ yzzw STORED false)
    Q_PROPERTY(quvec4 yzwx READ yzwx STORED false)
    Q_PROPERTY(quvec4 yzwy READ yzwy STORED false)
    Q_PROPERTY(quvec4 yzwz READ yzwz STORED false)
    Q_PROPERTY(quvec4 yzww READ yzww STORED false)
    Q_PROPERTY(quvec4 ywxx READ ywxx STORED false)
    Q_PROPERTY(quvec4 ywxy READ ywxy STORED false)
    Q_PROPERTY(quvec4 ywxz READ ywxz STORED false)
    Q_PROPERTY(quvec4 ywxw READ ywxw STORED false)
    Q_PROPERTY(quvec4 ywyx READ ywyx STORED false)
    Q_PROPERTY(quvec4 ywyy READ ywyy STORED false)
    Q_PROPERTY(quvec4 ywyz READ ywyz STORED false)
    Q_PROPERTY(quvec4 ywyw READ ywyw STORED false)
    Q_PROPERTY(quvec4 ywzx READ ywzx STORED false)
    Q_PROPERTY(quvec4 ywzy READ ywzy STORED false)
    Q_PROPERTY(quvec4 ywzz READ ywzz STORED false)
    Q_PROPERTY(quvec4 ywzw READ ywzw STORED false)
    Q_PROPERTY(quvec4 ywwx READ ywwx STORED false)
    Q_PROPERTY(quvec4 ywwy READ ywwy STORED false)
    Q_PROPERTY(quvec4 ywwz READ ywwz STORED false)
    Q_PROPERTY(quvec4 ywww READ ywww STORED false)
    Q_PROPERTY(quvec4 zxxx READ zxxx STORED false)
    Q_PROPERTY(quvec4 zxxy READ zxxy STORED false)
    Q_PROPERTY(quvec4 zxxz READ zxxz STORED false)
    Q_PROPERTY(quvec4 zxxw READ zxxw STORED false)
    Q_PROPERTY(quvec4 zxyx READ zxyx STORED false)
    Q_PROPERTY(quvec4 zxyy READ zxyy STORED false)
    Q_PROPERTY(quvec4 zxyz READ zxyz STORED false)
    Q_PROPERTY(quvec4 zxyw READ zxyw STORED false)
    Q_PROPERTY(quvec4 zxzx READ zxzx STORED false)
    Q_PROPERTY(quvec4 zxzy READ zxzy STORED false)
    Q_PROPERTY(quvec4 zxzz READ zxzz STORED false)
    Q_PROPERTY(quvec4 zxzw READ zxzw STORED false)
    Q_PROPERTY(quvec4 zxwx READ zxwx STORED false)
    Q_PROPERTY(quvec4 zxwy READ zxwy STORED false)
    Q_PROPERTY(quvec4 zxwz READ zxwz STORED false)
    Q_PROPERTY(quvec4 zxww READ zxww STORED false)
    Q_PROPERTY(quvec4 zyxx READ zyxx STORED false)
    Q_PROPERTY(quvec4 zyxy READ zyxy STORED false)
    Q_PROPERTY(quvec4 zyxz READ zyxz STORED false)
    Q_PROPERTY(quvec4 zyxw READ zyxw STORED false)
    Q_PROPERTY(quvec4 zyyx READ zyyx STORED false)
    Q_PROPERTY(quvec4 zyyy READ zyyy STORED false)
    Q_PROPERTY(quvec4 zyyz READ zyyz STORED false)
    Q_PROPERTY(quvec4 zyyw READ zyyw STORED false)
    Q_PROPERTY(quvec4 zyzx READ zyzx STORED false)
    Q_PROPERTY(quvec4 zyzy READ zyzy STORED false)
    Q_PROPERTY(quvec4 zyzz READ zyzz STORED false)
    Q_PROPERTY(quvec4 zyzw READ zyzw STORED false)
    Q_PROPERTY(quvec4 zywx READ zywx STORED false)
    Q_PROPERTY(quvec4 zywy READ zywy STORED false)
    Q_PROPERTY(quvec4 zywz READ zywz STORED false)
    Q_PROPERTY(quvec4 zyww READ zyww STORED false)
    Q_PROPERTY(quvec4 zzxx READ zzxx STORED false)
    Q_PROPERTY(quvec4 zzxy READ zzxy STORED false)
    Q_PROPERTY(quvec4 zzxz READ zzxz STORED false)
    Q_PROPERTY(quvec4 zzxw READ zzxw STORED false)
    Q_PROPERTY(quvec4 zzyx READ zzyx STORED false)
    Q_PROPERTY(quvec4 zzyy READ zzyy STORED false)
    Q_PROPERTY(quvec4 zzyz READ zzyz STORED false)
    Q_PROPERTY(quvec4 zzyw READ zzyw STORED false)
    Q_PROPERTY(quvec4 zzzx READ zzzx STORED false)
    Q_PROPERTY(quvec4 zzzy READ zzzy STORED false)
    Q_PROPERTY(quvec4 zzzz READ zzzz STORED false)
    Q_PROPERTY(quvec4 zzzw READ zzzw STORED false)
    Q_PROPERTY(quvec4 zzwx READ zzwx STORED false)
    Q_PROPERTY(quvec4 zzwy READ zzwy STORED false)
    Q_PROPERTY(quvec4 zzwz READ zzwz STORED false)
    Q_PROPERTY(quvec4 zzww READ zzww STORED false)
    Q_PROPERTY(quvec4 zwxx READ zwxx STORED false)
    Q_PROPERTY(quvec4 zwxy READ zwxy STORED false)
    Q_PROPERTY(quvec4 zwxz READ zwxz STORED false)
    Q_PROPERTY(quvec4 zwxw READ zwxw STORED false)
    Q_PROPERTY(quvec4 zwyx READ zwyx STORED false)
    Q_PROPERTY(quvec4 zwyy READ zwyy STORED false)
    Q_PROPERTY(quvec4 zwyz READ zwyz STORED false)
    Q_PROPERTY(quvec4 zwyw READ zwyw STORED false)
    Q_PROPERTY(quvec4 zwzx READ zwzx STORED false)
    Q_PROPERTY(quvec4 zwzy READ zwzy STORED false)
    Q_PROPERTY(quvec4 zwzz READ zwzz STORED false)
    Q_PROPERTY(quvec4 zwzw READ zwzw STORED false)
    Q_PROPERTY(quvec4 zwwx READ zwwx STORED false)
    Q_PROPERTY(quvec4 zwwy READ zwwy STORED false)
    Q_PROPERTY(quvec4 zwwz READ zwwz STORED false)
    Q_PROPERTY(quvec4 zwww READ zwww STORED false)
    Q_PROPERTY(quvec4 wxxx READ wxxx STORED false)
    Q_PROPERTY(quvec4 wxxy READ wxxy STORED false)
    Q_PROPERTY(quvec4 wxxz READ wxxz STORED false)
    Q_PROPERTY(quvec4 wxxw READ wxxw STORED false)
    Q_PROPERTY(quvec4 wxyx READ wxyx STORED false)
    Q_PROPERTY(quvec4 wxyy READ wxyy STORED false)
    Q_PROPERTY(quvec4 wxyz READ wxyz STORED false)
    Q_PROPERTY(quvec4 wxyw READ wxyw STORED false)
    Q_PROPERTY(quvec4 wxzx READ wxzx STORED false)
    Q_PROPERTY(quvec4 wxzy READ wxzy STORED false)
    Q_PROPERTY(quvec4 wxzz READ wxzz STORED false)
    Q_PROPERTY(quvec4 wxzw READ wxzw STORED false)
    Q_PROPERTY(quvec4 wxwx READ wxwx STORED false)
    Q_PROPERTY(quvec4 wxwy READ wxwy STORED false)
    Q_PROPERTY(quvec4 wxwz READ wxwz STORED false)
    Q_PROPERTY(quvec4 wxww READ wxww STORED false)
    Q_PROPERTY(quvec4 wyxx READ wyxx STORED false)
    Q_PROPERTY(quvec4 wyxy READ wyxy STORED false)
    Q_PROPERTY(quvec4 wyxz READ wyxz STORED false)
    Q_PROPERTY(quvec4 wyxw READ wyxw STORED false)
    Q_PROPERTY(quvec4 wyyx READ wyyx STORED false)
    Q_PROPERTY(quvec4 wyyy READ wyyy STORED false)
    Q_PROPERTY(quvec4 wyyz READ wyyz STORED false)
    Q_PROPERTY(quvec4 wyyw READ wyyw STORED false)
    Q_PROPERTY(quvec4 wyzx READ wyzx STORED false)
    Q_PROPERTY(quvec4 wyzy READ wyzy STORED false)
    Q_PROPERTY(quvec4 wyzz READ wyzz STORED false)
    Q_PROPERTY(quvec4 wyzw READ wyzw STORED false)
    Q_PROPERTY(quvec4 wywx READ wywx STORED false)
    Q_PROPERTY(quvec4 wywy READ wywy STORED false)
    Q_PROPERTY(quvec4 wywz READ wywz STORED false)
    Q_PROPERTY(quvec4 wyww READ wyww STORED false)
    Q_PROPERTY(quvec4 wzxx READ wzxx STORED false)
    Q_PROPERTY(quvec4 wzxy READ wzxy STORED false)
    Q_PROPERTY(quvec4 wzxz READ wzxz STORED false)
    Q_PROPERTY(quvec4 wzxw READ wzxw STORED false)
    Q_PROPERTY(quvec4 wzyx READ wzyx STORED false)
    Q_PROPERTY(quvec4 wzyy READ wzyy STORED false)
    Q_PROPERTY(quvec4 wzyz READ wzyz STORED false)
    Q_PROPERTY(quvec4 wzyw READ wzyw STORED false)
    Q_PROPERTY(quvec4 wzzx READ wzzx STORED false)
    Q_PROPERTY(quvec4 wzzy READ wzzy STORED false)
    Q_PROPERTY(quvec4 wzzz READ wzzz STORED false)
    Q_PROPERTY(quvec4 wzzw READ wzzw STORED false)
    Q_PROPERTY(quvec4 wzwx READ wzwx STORED false)
    Q_PROPERTY(quvec4 wzwy READ wzwy STORED false)
    Q_PROPERTY(quvec4 wzwz READ wzwz STORED false)
    Q_PROPERTY(quvec4 wzww READ wzww STORED false)
    Q_PROPERTY(quvec4 wwxx READ wwxx STORED false)
    Q_PROPERTY(quvec4 wwxy READ wwxy STORED false)
    Q_PROPERTY(quvec4 wwxz READ wwxz STORED false)
    Q_PROPERTY(quvec4 wwxw READ wwxw STORED false)
    Q_PROPERTY(quvec4 wwyx READ wwyx STORED false)
    Q_PROPERTY(quvec4 wwyy READ wwyy STORED false)
    Q_PROPERTY(quvec4 wwyz READ wwyz STORED false)
    Q_PROPERTY(quvec4 wwyw READ wwyw STORED false)
    Q_PROPERTY(quvec4 wwzx READ wwzx STORED false)
    Q_PROPERTY(quvec4 wwzy READ wwzy STORED false)
    Q_PROPERTY(quvec4 wwzz READ wwzz STORED false)
    Q_PROPERTY(quvec4 wwzw READ wwzw STORED false)
    Q_PROPERTY(quvec4 wwwx READ wwwx STORED false)
    Q_PROPERTY(quvec4 wwwy READ wwwy STORED false)
    Q_PROPERTY(quvec4 wwwz READ wwwz STORED false)
    Q_PROPERTY(quvec4 wwww READ wwww STORED false)

public:

    using vector4<unsigned int>::vector4;

    using QGenericMatrix<1,4,unsigned int>::operator==;

    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class QUICKOPENGL_EXPORT qdvec4 : public vector4<double>
{
    Q_GADGET
    Q_PROPERTY(double x READ x WRITE setX)
    Q_PROPERTY(double y READ y WRITE setY)
    Q_PROPERTY(double z READ z WRITE setZ)
    Q_PROPERTY(double w READ w WRITE setW)
    Q_PROPERTY(double r READ r WRITE setR)
    Q_PROPERTY(double g READ g WRITE setG)
    Q_PROPERTY(double b READ b WRITE setB)
    Q_PROPERTY(double a READ a WRITE setA)

    Q_PROPERTY(qdvec2 xx READ xx STORED false)
    Q_PROPERTY(qdvec2 xy READ xy STORED false)
    Q_PROPERTY(qdvec2 xz READ xz STORED false)
    Q_PROPERTY(qdvec2 xw READ xw STORED false)
    Q_PROPERTY(qdvec2 yx READ yx STORED false)
    Q_PROPERTY(qdvec2 yy READ yy STORED false)
    Q_PROPERTY(qdvec2 yz READ yz STORED false)
    Q_PROPERTY(qdvec2 yw READ yw STORED false)
    Q_PROPERTY(qdvec2 zx READ zx STORED false)
    Q_PROPERTY(qdvec2 zy READ zy STORED false)
    Q_PROPERTY(qdvec2 zz READ zz STORED false)
    Q_PROPERTY(qdvec2 zw READ zw STORED false)
    Q_PROPERTY(qdvec2 wx READ wx STORED false)
    Q_PROPERTY(qdvec2 wy READ wy STORED false)
    Q_PROPERTY(qdvec2 wz READ wz STORED false)
    Q_PROPERTY(qdvec2 ww READ ww STORED false)

    Q_PROPERTY(qdvec3 xxx READ xxx STORED false)
    Q_PROPERTY(qdvec3 xxy READ xxy STORED false)
    Q_PROPERTY(qdvec3 xxz READ xxz STORED false)
    Q_PROPERTY(qdvec3 xxw READ xxw STORED false)
    Q_PROPERTY(qdvec3 xyx READ xyx STORED false)
    Q_PROPERTY(qdvec3 xyy READ xyy STORED false)
    Q_PROPERTY(qdvec3 xyz READ xyz STORED false)
    Q_PROPERTY(qdvec3 xyw READ xyw STORED false)
    Q_PROPERTY(qdvec3 xzx READ xzx STORED false)
    Q_PROPERTY(qdvec3 xzy READ xzy STORED false)
    Q_PROPERTY(qdvec3 xzz READ xzz STORED false)
    Q_PROPERTY(qdvec3 xzw READ xzw STORED false)
    Q_PROPERTY(qdvec3 xwx READ xwx STORED false)
    Q_PROPERTY(qdvec3 xwy READ xwy STORED false)
    Q_PROPERTY(qdvec3 xwz READ xwz STORED false)
    Q_PROPERTY(qdvec3 xww READ xww STORED false)
    Q_PROPERTY(qdvec3 yxx READ yxx STORED false)
    Q_PROPERTY(qdvec3 yxy READ yxy STORED false)
    Q_PROPERTY(qdvec3 yxz READ yxz STORED false)
    Q_PROPERTY(qdvec3 yxw READ yxw STORED false)
    Q_PROPERTY(qdvec3 yyx READ yyx STORED false)
    Q_PROPERTY(qdvec3 yyy READ yyy STORED false)
    Q_PROPERTY(qdvec3 yyz READ yyz STORED false)
    Q_PROPERTY(qdvec3 yyw READ yyw STORED false)
    Q_PROPERTY(qdvec3 yzx READ yzx STORED false)
    Q_PROPERTY(qdvec3 yzy READ yzy STORED false)
    Q_PROPERTY(qdvec3 yzz READ yzz STORED false)
    Q_PROPERTY(qdvec3 yzw READ yzw STORED false)
    Q_PROPERTY(qdvec3 ywx READ ywx STORED false)
    Q_PROPERTY(qdvec3 ywy READ ywy STORED false)
    Q_PROPERTY(qdvec3 ywz READ ywz STORED false)
    Q_PROPERTY(qdvec3 yww READ yww STORED false)
    Q_PROPERTY(qdvec3 zxx READ zxx STORED false)
    Q_PROPERTY(qdvec3 zxy READ zxy STORED false)
    Q_PROPERTY(qdvec3 zxz READ zxz STORED false)
    Q_PROPERTY(qdvec3 zxw READ zxw STORED false)
    Q_PROPERTY(qdvec3 zyx READ zyx STORED false)
    Q_PROPERTY(qdvec3 zyy READ zyy STORED false)
    Q_PROPERTY(qdvec3 zyz READ zyz STORED false)
    Q_PROPERTY(qdvec3 zyw READ zyw STORED false)
    Q_PROPERTY(qdvec3 zzx READ zzx STORED false)
    Q_PROPERTY(qdvec3 zzy READ zzy STORED false)
    Q_PROPERTY(qdvec3 zzz READ zzz STORED false)
    Q_PROPERTY(qdvec3 zzw READ zzw STORED false)
    Q_PROPERTY(qdvec3 zwx READ zwx STORED false)
    Q_PROPERTY(qdvec3 zwy READ zwy STORED false)
    Q_PROPERTY(qdvec3 zwz READ zwz STORED false)
    Q_PROPERTY(qdvec3 zww READ zww STORED false)
    Q_PROPERTY(qdvec3 wxx READ wxx STORED false)
    Q_PROPERTY(qdvec3 wxy READ wxy STORED false)
    Q_PROPERTY(qdvec3 wxz READ wxz STORED false)
    Q_PROPERTY(qdvec3 wxw READ wxw STORED false)
    Q_PROPERTY(qdvec3 wyx READ wyx STORED false)
    Q_PROPERTY(qdvec3 wyy READ wyy STORED false)
    Q_PROPERTY(qdvec3 wyz READ wyz STORED false)
    Q_PROPERTY(qdvec3 wyw READ wyw STORED false)
    Q_PROPERTY(qdvec3 wzx READ wzx STORED false)
    Q_PROPERTY(qdvec3 wzy READ wzy STORED false)
    Q_PROPERTY(qdvec3 wzz READ wzz STORED false)
    Q_PROPERTY(qdvec3 wzw READ wzw STORED false)
    Q_PROPERTY(qdvec3 wwx READ wwx STORED false)
    Q_PROPERTY(qdvec3 wwy READ wwy STORED false)
    Q_PROPERTY(qdvec3 wwz READ wwz STORED false)
    Q_PROPERTY(qdvec3 www READ www STORED false)


    Q_PROPERTY(qdvec4 xxxx READ xxxx STORED false)
    Q_PROPERTY(qdvec4 xxxy READ xxxy STORED false)
    Q_PROPERTY(qdvec4 xxxz READ xxxz STORED false)
    Q_PROPERTY(qdvec4 xxxw READ xxxw STORED false)
    Q_PROPERTY(qdvec4 xxyx READ xxyx STORED false)
    Q_PROPERTY(qdvec4 xxyy READ xxyy STORED false)
    Q_PROPERTY(qdvec4 xxyz READ xxyz STORED false)
    Q_PROPERTY(qdvec4 xxyw READ xxyw STORED false)
    Q_PROPERTY(qdvec4 xxzx READ xxzx STORED false)
    Q_PROPERTY(qdvec4 xxzy READ xxzy STORED false)
    Q_PROPERTY(qdvec4 xxzz READ xxzz STORED false)
    Q_PROPERTY(qdvec4 xxzw READ xxzw STORED false)
    Q_PROPERTY(qdvec4 xxwx READ xxwx STORED false)
    Q_PROPERTY(qdvec4 xxwy READ xxwy STORED false)
    Q_PROPERTY(qdvec4 xxwz READ xxwz STORED false)
    Q_PROPERTY(qdvec4 xxww READ xxww STORED false)
    Q_PROPERTY(qdvec4 xyxx READ xyxx STORED false)
    Q_PROPERTY(qdvec4 xyxy READ xyxy STORED false)
    Q_PROPERTY(qdvec4 xyxz READ xyxz STORED false)
    Q_PROPERTY(qdvec4 xyxw READ xyxw STORED false)
    Q_PROPERTY(qdvec4 xyyx READ xyyx STORED false)
    Q_PROPERTY(qdvec4 xyyy READ xyyy STORED false)
    Q_PROPERTY(qdvec4 xyyz READ xyyz STORED false)
    Q_PROPERTY(qdvec4 xyyw READ xyyw STORED false)
    Q_PROPERTY(qdvec4 xyzx READ xyzx STORED false)
    Q_PROPERTY(qdvec4 xyzy READ xyzy STORED false)
    Q_PROPERTY(qdvec4 xyzz READ xyzz STORED false)
    Q_PROPERTY(qdvec4 xyzw READ xyzw STORED false)
    Q_PROPERTY(qdvec4 xywx READ xywx STORED false)
    Q_PROPERTY(qdvec4 xywy READ xywy STORED false)
    Q_PROPERTY(qdvec4 xywz READ xywz STORED false)
    Q_PROPERTY(qdvec4 xyww READ xyww STORED false)
    Q_PROPERTY(qdvec4 xzxx READ xzxx STORED false)
    Q_PROPERTY(qdvec4 xzxy READ xzxy STORED false)
    Q_PROPERTY(qdvec4 xzxz READ xzxz STORED false)
    Q_PROPERTY(qdvec4 xzxw READ xzxw STORED false)
    Q_PROPERTY(qdvec4 xzyx READ xzyx STORED false)
    Q_PROPERTY(qdvec4 xzyy READ xzyy STORED false)
    Q_PROPERTY(qdvec4 xzyz READ xzyz STORED false)
    Q_PROPERTY(qdvec4 xzyw READ xzyw STORED false)
    Q_PROPERTY(qdvec4 xzzx READ xzzx STORED false)
    Q_PROPERTY(qdvec4 xzzy READ xzzy STORED false)
    Q_PROPERTY(qdvec4 xzzz READ xzzz STORED false)
    Q_PROPERTY(qdvec4 xzzw READ xzzw STORED false)
    Q_PROPERTY(qdvec4 xzwx READ xzwx STORED false)
    Q_PROPERTY(qdvec4 xzwy READ xzwy STORED false)
    Q_PROPERTY(qdvec4 xzwz READ xzwz STORED false)
    Q_PROPERTY(qdvec4 xzww READ xzww STORED false)
    Q_PROPERTY(qdvec4 xwxx READ xwxx STORED false)
    Q_PROPERTY(qdvec4 xwxy READ xwxy STORED false)
    Q_PROPERTY(qdvec4 xwxz READ xwxz STORED false)
    Q_PROPERTY(qdvec4 xwxw READ xwxw STORED false)
    Q_PROPERTY(qdvec4 xwyx READ xwyx STORED false)
    Q_PROPERTY(qdvec4 xwyy READ xwyy STORED false)
    Q_PROPERTY(qdvec4 xwyz READ xwyz STORED false)
    Q_PROPERTY(qdvec4 xwyw READ xwyw STORED false)
    Q_PROPERTY(qdvec4 xwzx READ xwzx STORED false)
    Q_PROPERTY(qdvec4 xwzy READ xwzy STORED false)
    Q_PROPERTY(qdvec4 xwzz READ xwzz STORED false)
    Q_PROPERTY(qdvec4 xwzw READ xwzw STORED false)
    Q_PROPERTY(qdvec4 xwwx READ xwwx STORED false)
    Q_PROPERTY(qdvec4 xwwy READ xwwy STORED false)
    Q_PROPERTY(qdvec4 xwwz READ xwwz STORED false)
    Q_PROPERTY(qdvec4 xwww READ xwww STORED false)
    Q_PROPERTY(qdvec4 yxxx READ yxxx STORED false)
    Q_PROPERTY(qdvec4 yxxy READ yxxy STORED false)
    Q_PROPERTY(qdvec4 yxxz READ yxxz STORED false)
    Q_PROPERTY(qdvec4 yxxw READ yxxw STORED false)
    Q_PROPERTY(qdvec4 yxyx READ yxyx STORED false)
    Q_PROPERTY(qdvec4 yxyy READ yxyy STORED false)
    Q_PROPERTY(qdvec4 yxyz READ yxyz STORED false)
    Q_PROPERTY(qdvec4 yxyw READ yxyw STORED false)
    Q_PROPERTY(qdvec4 yxzx READ yxzx STORED false)
    Q_PROPERTY(qdvec4 yxzy READ yxzy STORED false)
    Q_PROPERTY(qdvec4 yxzz READ yxzz STORED false)
    Q_PROPERTY(qdvec4 yxzw READ yxzw STORED false)
    Q_PROPERTY(qdvec4 yxwx READ yxwx STORED false)
    Q_PROPERTY(qdvec4 yxwy READ yxwy STORED false)
    Q_PROPERTY(qdvec4 yxwz READ yxwz STORED false)
    Q_PROPERTY(qdvec4 yxww READ yxww STORED false)
    Q_PROPERTY(qdvec4 yyxx READ yyxx STORED false)
    Q_PROPERTY(qdvec4 yyxy READ yyxy STORED false)
    Q_PROPERTY(qdvec4 yyxz READ yyxz STORED false)
    Q_PROPERTY(qdvec4 yyxw READ yyxw STORED false)
    Q_PROPERTY(qdvec4 yyyx READ yyyx STORED false)
    Q_PROPERTY(qdvec4 yyyy READ yyyy STORED false)
    Q_PROPERTY(qdvec4 yyyz READ yyyz STORED false)
    Q_PROPERTY(qdvec4 yyyw READ yyyw STORED false)
    Q_PROPERTY(qdvec4 yyzx READ yyzx STORED false)
    Q_PROPERTY(qdvec4 yyzy READ yyzy STORED false)
    Q_PROPERTY(qdvec4 yyzz READ yyzz STORED false)
    Q_PROPERTY(qdvec4 yyzw READ yyzw STORED false)
    Q_PROPERTY(qdvec4 yywx READ yywx STORED false)
    Q_PROPERTY(qdvec4 yywy READ yywy STORED false)
    Q_PROPERTY(qdvec4 yywz READ yywz STORED false)
    Q_PROPERTY(qdvec4 yyww READ yyww STORED false)
    Q_PROPERTY(qdvec4 yzxx READ yzxx STORED false)
    Q_PROPERTY(qdvec4 yzxy READ yzxy STORED false)
    Q_PROPERTY(qdvec4 yzxz READ yzxz STORED false)
    Q_PROPERTY(qdvec4 yzxw READ yzxw STORED false)
    Q_PROPERTY(qdvec4 yzyx READ yzyx STORED false)
    Q_PROPERTY(qdvec4 yzyy READ yzyy STORED false)
    Q_PROPERTY(qdvec4 yzyz READ yzyz STORED false)
    Q_PROPERTY(qdvec4 yzyw READ yzyw STORED false)
    Q_PROPERTY(qdvec4 yzzx READ yzzx STORED false)
    Q_PROPERTY(qdvec4 yzzy READ yzzy STORED false)
    Q_PROPERTY(qdvec4 yzzz READ yzzz STORED false)
    Q_PROPERTY(qdvec4 yzzw READ yzzw STORED false)
    Q_PROPERTY(qdvec4 yzwx READ yzwx STORED false)
    Q_PROPERTY(qdvec4 yzwy READ yzwy STORED false)
    Q_PROPERTY(qdvec4 yzwz READ yzwz STORED false)
    Q_PROPERTY(qdvec4 yzww READ yzww STORED false)
    Q_PROPERTY(qdvec4 ywxx READ ywxx STORED false)
    Q_PROPERTY(qdvec4 ywxy READ ywxy STORED false)
    Q_PROPERTY(qdvec4 ywxz READ ywxz STORED false)
    Q_PROPERTY(qdvec4 ywxw READ ywxw STORED false)
    Q_PROPERTY(qdvec4 ywyx READ ywyx STORED false)
    Q_PROPERTY(qdvec4 ywyy READ ywyy STORED false)
    Q_PROPERTY(qdvec4 ywyz READ ywyz STORED false)
    Q_PROPERTY(qdvec4 ywyw READ ywyw STORED false)
    Q_PROPERTY(qdvec4 ywzx READ ywzx STORED false)
    Q_PROPERTY(qdvec4 ywzy READ ywzy STORED false)
    Q_PROPERTY(qdvec4 ywzz READ ywzz STORED false)
    Q_PROPERTY(qdvec4 ywzw READ ywzw STORED false)
    Q_PROPERTY(qdvec4 ywwx READ ywwx STORED false)
    Q_PROPERTY(qdvec4 ywwy READ ywwy STORED false)
    Q_PROPERTY(qdvec4 ywwz READ ywwz STORED false)
    Q_PROPERTY(qdvec4 ywww READ ywww STORED false)
    Q_PROPERTY(qdvec4 zxxx READ zxxx STORED false)
    Q_PROPERTY(qdvec4 zxxy READ zxxy STORED false)
    Q_PROPERTY(qdvec4 zxxz READ zxxz STORED false)
    Q_PROPERTY(qdvec4 zxxw READ zxxw STORED false)
    Q_PROPERTY(qdvec4 zxyx READ zxyx STORED false)
    Q_PROPERTY(qdvec4 zxyy READ zxyy STORED false)
    Q_PROPERTY(qdvec4 zxyz READ zxyz STORED false)
    Q_PROPERTY(qdvec4 zxyw READ zxyw STORED false)
    Q_PROPERTY(qdvec4 zxzx READ zxzx STORED false)
    Q_PROPERTY(qdvec4 zxzy READ zxzy STORED false)
    Q_PROPERTY(qdvec4 zxzz READ zxzz STORED false)
    Q_PROPERTY(qdvec4 zxzw READ zxzw STORED false)
    Q_PROPERTY(qdvec4 zxwx READ zxwx STORED false)
    Q_PROPERTY(qdvec4 zxwy READ zxwy STORED false)
    Q_PROPERTY(qdvec4 zxwz READ zxwz STORED false)
    Q_PROPERTY(qdvec4 zxww READ zxww STORED false)
    Q_PROPERTY(qdvec4 zyxx READ zyxx STORED false)
    Q_PROPERTY(qdvec4 zyxy READ zyxy STORED false)
    Q_PROPERTY(qdvec4 zyxz READ zyxz STORED false)
    Q_PROPERTY(qdvec4 zyxw READ zyxw STORED false)
    Q_PROPERTY(qdvec4 zyyx READ zyyx STORED false)
    Q_PROPERTY(qdvec4 zyyy READ zyyy STORED false)
    Q_PROPERTY(qdvec4 zyyz READ zyyz STORED false)
    Q_PROPERTY(qdvec4 zyyw READ zyyw STORED false)
    Q_PROPERTY(qdvec4 zyzx READ zyzx STORED false)
    Q_PROPERTY(qdvec4 zyzy READ zyzy STORED false)
    Q_PROPERTY(qdvec4 zyzz READ zyzz STORED false)
    Q_PROPERTY(qdvec4 zyzw READ zyzw STORED false)
    Q_PROPERTY(qdvec4 zywx READ zywx STORED false)
    Q_PROPERTY(qdvec4 zywy READ zywy STORED false)
    Q_PROPERTY(qdvec4 zywz READ zywz STORED false)
    Q_PROPERTY(qdvec4 zyww READ zyww STORED false)
    Q_PROPERTY(qdvec4 zzxx READ zzxx STORED false)
    Q_PROPERTY(qdvec4 zzxy READ zzxy STORED false)
    Q_PROPERTY(qdvec4 zzxz READ zzxz STORED false)
    Q_PROPERTY(qdvec4 zzxw READ zzxw STORED false)
    Q_PROPERTY(qdvec4 zzyx READ zzyx STORED false)
    Q_PROPERTY(qdvec4 zzyy READ zzyy STORED false)
    Q_PROPERTY(qdvec4 zzyz READ zzyz STORED false)
    Q_PROPERTY(qdvec4 zzyw READ zzyw STORED false)
    Q_PROPERTY(qdvec4 zzzx READ zzzx STORED false)
    Q_PROPERTY(qdvec4 zzzy READ zzzy STORED false)
    Q_PROPERTY(qdvec4 zzzz READ zzzz STORED false)
    Q_PROPERTY(qdvec4 zzzw READ zzzw STORED false)
    Q_PROPERTY(qdvec4 zzwx READ zzwx STORED false)
    Q_PROPERTY(qdvec4 zzwy READ zzwy STORED false)
    Q_PROPERTY(qdvec4 zzwz READ zzwz STORED false)
    Q_PROPERTY(qdvec4 zzww READ zzww STORED false)
    Q_PROPERTY(qdvec4 zwxx READ zwxx STORED false)
    Q_PROPERTY(qdvec4 zwxy READ zwxy STORED false)
    Q_PROPERTY(qdvec4 zwxz READ zwxz STORED false)
    Q_PROPERTY(qdvec4 zwxw READ zwxw STORED false)
    Q_PROPERTY(qdvec4 zwyx READ zwyx STORED false)
    Q_PROPERTY(qdvec4 zwyy READ zwyy STORED false)
    Q_PROPERTY(qdvec4 zwyz READ zwyz STORED false)
    Q_PROPERTY(qdvec4 zwyw READ zwyw STORED false)
    Q_PROPERTY(qdvec4 zwzx READ zwzx STORED false)
    Q_PROPERTY(qdvec4 zwzy READ zwzy STORED false)
    Q_PROPERTY(qdvec4 zwzz READ zwzz STORED false)
    Q_PROPERTY(qdvec4 zwzw READ zwzw STORED false)
    Q_PROPERTY(qdvec4 zwwx READ zwwx STORED false)
    Q_PROPERTY(qdvec4 zwwy READ zwwy STORED false)
    Q_PROPERTY(qdvec4 zwwz READ zwwz STORED false)
    Q_PROPERTY(qdvec4 zwww READ zwww STORED false)
    Q_PROPERTY(qdvec4 wxxx READ wxxx STORED false)
    Q_PROPERTY(qdvec4 wxxy READ wxxy STORED false)
    Q_PROPERTY(qdvec4 wxxz READ wxxz STORED false)
    Q_PROPERTY(qdvec4 wxxw READ wxxw STORED false)
    Q_PROPERTY(qdvec4 wxyx READ wxyx STORED false)
    Q_PROPERTY(qdvec4 wxyy READ wxyy STORED false)
    Q_PROPERTY(qdvec4 wxyz READ wxyz STORED false)
    Q_PROPERTY(qdvec4 wxyw READ wxyw STORED false)
    Q_PROPERTY(qdvec4 wxzx READ wxzx STORED false)
    Q_PROPERTY(qdvec4 wxzy READ wxzy STORED false)
    Q_PROPERTY(qdvec4 wxzz READ wxzz STORED false)
    Q_PROPERTY(qdvec4 wxzw READ wxzw STORED false)
    Q_PROPERTY(qdvec4 wxwx READ wxwx STORED false)
    Q_PROPERTY(qdvec4 wxwy READ wxwy STORED false)
    Q_PROPERTY(qdvec4 wxwz READ wxwz STORED false)
    Q_PROPERTY(qdvec4 wxww READ wxww STORED false)
    Q_PROPERTY(qdvec4 wyxx READ wyxx STORED false)
    Q_PROPERTY(qdvec4 wyxy READ wyxy STORED false)
    Q_PROPERTY(qdvec4 wyxz READ wyxz STORED false)
    Q_PROPERTY(qdvec4 wyxw READ wyxw STORED false)
    Q_PROPERTY(qdvec4 wyyx READ wyyx STORED false)
    Q_PROPERTY(qdvec4 wyyy READ wyyy STORED false)
    Q_PROPERTY(qdvec4 wyyz READ wyyz STORED false)
    Q_PROPERTY(qdvec4 wyyw READ wyyw STORED false)
    Q_PROPERTY(qdvec4 wyzx READ wyzx STORED false)
    Q_PROPERTY(qdvec4 wyzy READ wyzy STORED false)
    Q_PROPERTY(qdvec4 wyzz READ wyzz STORED false)
    Q_PROPERTY(qdvec4 wyzw READ wyzw STORED false)
    Q_PROPERTY(qdvec4 wywx READ wywx STORED false)
    Q_PROPERTY(qdvec4 wywy READ wywy STORED false)
    Q_PROPERTY(qdvec4 wywz READ wywz STORED false)
    Q_PROPERTY(qdvec4 wyww READ wyww STORED false)
    Q_PROPERTY(qdvec4 wzxx READ wzxx STORED false)
    Q_PROPERTY(qdvec4 wzxy READ wzxy STORED false)
    Q_PROPERTY(qdvec4 wzxz READ wzxz STORED false)
    Q_PROPERTY(qdvec4 wzxw READ wzxw STORED false)
    Q_PROPERTY(qdvec4 wzyx READ wzyx STORED false)
    Q_PROPERTY(qdvec4 wzyy READ wzyy STORED false)
    Q_PROPERTY(qdvec4 wzyz READ wzyz STORED false)
    Q_PROPERTY(qdvec4 wzyw READ wzyw STORED false)
    Q_PROPERTY(qdvec4 wzzx READ wzzx STORED false)
    Q_PROPERTY(qdvec4 wzzy READ wzzy STORED false)
    Q_PROPERTY(qdvec4 wzzz READ wzzz STORED false)
    Q_PROPERTY(qdvec4 wzzw READ wzzw STORED false)
    Q_PROPERTY(qdvec4 wzwx READ wzwx STORED false)
    Q_PROPERTY(qdvec4 wzwy READ wzwy STORED false)
    Q_PROPERTY(qdvec4 wzwz READ wzwz STORED false)
    Q_PROPERTY(qdvec4 wzww READ wzww STORED false)
    Q_PROPERTY(qdvec4 wwxx READ wwxx STORED false)
    Q_PROPERTY(qdvec4 wwxy READ wwxy STORED false)
    Q_PROPERTY(qdvec4 wwxz READ wwxz STORED false)
    Q_PROPERTY(qdvec4 wwxw READ wwxw STORED false)
    Q_PROPERTY(qdvec4 wwyx READ wwyx STORED false)
    Q_PROPERTY(qdvec4 wwyy READ wwyy STORED false)
    Q_PROPERTY(qdvec4 wwyz READ wwyz STORED false)
    Q_PROPERTY(qdvec4 wwyw READ wwyw STORED false)
    Q_PROPERTY(qdvec4 wwzx READ wwzx STORED false)
    Q_PROPERTY(qdvec4 wwzy READ wwzy STORED false)
    Q_PROPERTY(qdvec4 wwzz READ wwzz STORED false)
    Q_PROPERTY(qdvec4 wwzw READ wwzw STORED false)
    Q_PROPERTY(qdvec4 wwwx READ wwwx STORED false)
    Q_PROPERTY(qdvec4 wwwy READ wwwy STORED false)
    Q_PROPERTY(qdvec4 wwwz READ wwwz STORED false)
    Q_PROPERTY(qdvec4 wwww READ wwww STORED false)

public:

    Q_INVOKABLE double dotProduct(const qdvec4 &other) const;
    Q_INVOKABLE qdvec4 times(const qdmat4 &matrix) const;
    Q_INVOKABLE qdvec4 times(const qdvec4 &other) const;
    Q_INVOKABLE qdvec4 times(double factor) const;
    Q_INVOKABLE qdvec4 plus(const qdvec4 &other) const;
    Q_INVOKABLE qdvec4 minus(const qdvec4 &other) const;
    Q_INVOKABLE qdvec4 normalized() const;
    Q_INVOKABLE double length() const;

    using vector4<double>::vector4;
    using QGenericMatrix<1,4,double>::operator==;

    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};



class QUICKOPENGL_EXPORT qbvec4 : public vector4<bool>
{
    Q_GADGET
    Q_PROPERTY(bool x READ x WRITE setX)
    Q_PROPERTY(bool y READ y WRITE setY)
    Q_PROPERTY(bool z READ z WRITE setZ)
    Q_PROPERTY(bool w READ w WRITE setW)
    Q_PROPERTY(bool r READ r WRITE setR)
    Q_PROPERTY(bool g READ g WRITE setG)
    Q_PROPERTY(bool b READ b WRITE setB)
    Q_PROPERTY(bool a READ a WRITE setA)

    Q_PROPERTY(qbvec2 xx READ xx STORED false)
    Q_PROPERTY(qbvec2 xy READ xy STORED false)
    Q_PROPERTY(qbvec2 xz READ xz STORED false)
    Q_PROPERTY(qbvec2 xw READ xw STORED false)
    Q_PROPERTY(qbvec2 yx READ yx STORED false)
    Q_PROPERTY(qbvec2 yy READ yy STORED false)
    Q_PROPERTY(qbvec2 yz READ yz STORED false)
    Q_PROPERTY(qbvec2 yw READ yw STORED false)
    Q_PROPERTY(qbvec2 zx READ zx STORED false)
    Q_PROPERTY(qbvec2 zy READ zy STORED false)
    Q_PROPERTY(qbvec2 zz READ zz STORED false)
    Q_PROPERTY(qbvec2 zw READ zw STORED false)
    Q_PROPERTY(qbvec2 wx READ wx STORED false)
    Q_PROPERTY(qbvec2 wy READ wy STORED false)
    Q_PROPERTY(qbvec2 wz READ wz STORED false)
    Q_PROPERTY(qbvec2 ww READ ww STORED false)

    Q_PROPERTY(qbvec3 xxx READ xxx STORED false)
    Q_PROPERTY(qbvec3 xxy READ xxy STORED false)
    Q_PROPERTY(qbvec3 xxz READ xxz STORED false)
    Q_PROPERTY(qbvec3 xxw READ xxw STORED false)
    Q_PROPERTY(qbvec3 xyx READ xyx STORED false)
    Q_PROPERTY(qbvec3 xyy READ xyy STORED false)
    Q_PROPERTY(qbvec3 xyz READ xyz STORED false)
    Q_PROPERTY(qbvec3 xyw READ xyw STORED false)
    Q_PROPERTY(qbvec3 xzx READ xzx STORED false)
    Q_PROPERTY(qbvec3 xzy READ xzy STORED false)
    Q_PROPERTY(qbvec3 xzz READ xzz STORED false)
    Q_PROPERTY(qbvec3 xzw READ xzw STORED false)
    Q_PROPERTY(qbvec3 xwx READ xwx STORED false)
    Q_PROPERTY(qbvec3 xwy READ xwy STORED false)
    Q_PROPERTY(qbvec3 xwz READ xwz STORED false)
    Q_PROPERTY(qbvec3 xww READ xww STORED false)
    Q_PROPERTY(qbvec3 yxx READ yxx STORED false)
    Q_PROPERTY(qbvec3 yxy READ yxy STORED false)
    Q_PROPERTY(qbvec3 yxz READ yxz STORED false)
    Q_PROPERTY(qbvec3 yxw READ yxw STORED false)
    Q_PROPERTY(qbvec3 yyx READ yyx STORED false)
    Q_PROPERTY(qbvec3 yyy READ yyy STORED false)
    Q_PROPERTY(qbvec3 yyz READ yyz STORED false)
    Q_PROPERTY(qbvec3 yyw READ yyw STORED false)
    Q_PROPERTY(qbvec3 yzx READ yzx STORED false)
    Q_PROPERTY(qbvec3 yzy READ yzy STORED false)
    Q_PROPERTY(qbvec3 yzz READ yzz STORED false)
    Q_PROPERTY(qbvec3 yzw READ yzw STORED false)
    Q_PROPERTY(qbvec3 ywx READ ywx STORED false)
    Q_PROPERTY(qbvec3 ywy READ ywy STORED false)
    Q_PROPERTY(qbvec3 ywz READ ywz STORED false)
    Q_PROPERTY(qbvec3 yww READ yww STORED false)
    Q_PROPERTY(qbvec3 zxx READ zxx STORED false)
    Q_PROPERTY(qbvec3 zxy READ zxy STORED false)
    Q_PROPERTY(qbvec3 zxz READ zxz STORED false)
    Q_PROPERTY(qbvec3 zxw READ zxw STORED false)
    Q_PROPERTY(qbvec3 zyx READ zyx STORED false)
    Q_PROPERTY(qbvec3 zyy READ zyy STORED false)
    Q_PROPERTY(qbvec3 zyz READ zyz STORED false)
    Q_PROPERTY(qbvec3 zyw READ zyw STORED false)
    Q_PROPERTY(qbvec3 zzx READ zzx STORED false)
    Q_PROPERTY(qbvec3 zzy READ zzy STORED false)
    Q_PROPERTY(qbvec3 zzz READ zzz STORED false)
    Q_PROPERTY(qbvec3 zzw READ zzw STORED false)
    Q_PROPERTY(qbvec3 zwx READ zwx STORED false)
    Q_PROPERTY(qbvec3 zwy READ zwy STORED false)
    Q_PROPERTY(qbvec3 zwz READ zwz STORED false)
    Q_PROPERTY(qbvec3 zww READ zww STORED false)
    Q_PROPERTY(qbvec3 wxx READ wxx STORED false)
    Q_PROPERTY(qbvec3 wxy READ wxy STORED false)
    Q_PROPERTY(qbvec3 wxz READ wxz STORED false)
    Q_PROPERTY(qbvec3 wxw READ wxw STORED false)
    Q_PROPERTY(qbvec3 wyx READ wyx STORED false)
    Q_PROPERTY(qbvec3 wyy READ wyy STORED false)
    Q_PROPERTY(qbvec3 wyz READ wyz STORED false)
    Q_PROPERTY(qbvec3 wyw READ wyw STORED false)
    Q_PROPERTY(qbvec3 wzx READ wzx STORED false)
    Q_PROPERTY(qbvec3 wzy READ wzy STORED false)
    Q_PROPERTY(qbvec3 wzz READ wzz STORED false)
    Q_PROPERTY(qbvec3 wzw READ wzw STORED false)
    Q_PROPERTY(qbvec3 wwx READ wwx STORED false)
    Q_PROPERTY(qbvec3 wwy READ wwy STORED false)
    Q_PROPERTY(qbvec3 wwz READ wwz STORED false)
    Q_PROPERTY(qbvec3 www READ www STORED false)


    Q_PROPERTY(qbvec4 xxxx READ xxxx STORED false)
    Q_PROPERTY(qbvec4 xxxy READ xxxy STORED false)
    Q_PROPERTY(qbvec4 xxxz READ xxxz STORED false)
    Q_PROPERTY(qbvec4 xxxw READ xxxw STORED false)
    Q_PROPERTY(qbvec4 xxyx READ xxyx STORED false)
    Q_PROPERTY(qbvec4 xxyy READ xxyy STORED false)
    Q_PROPERTY(qbvec4 xxyz READ xxyz STORED false)
    Q_PROPERTY(qbvec4 xxyw READ xxyw STORED false)
    Q_PROPERTY(qbvec4 xxzx READ xxzx STORED false)
    Q_PROPERTY(qbvec4 xxzy READ xxzy STORED false)
    Q_PROPERTY(qbvec4 xxzz READ xxzz STORED false)
    Q_PROPERTY(qbvec4 xxzw READ xxzw STORED false)
    Q_PROPERTY(qbvec4 xxwx READ xxwx STORED false)
    Q_PROPERTY(qbvec4 xxwy READ xxwy STORED false)
    Q_PROPERTY(qbvec4 xxwz READ xxwz STORED false)
    Q_PROPERTY(qbvec4 xxww READ xxww STORED false)
    Q_PROPERTY(qbvec4 xyxx READ xyxx STORED false)
    Q_PROPERTY(qbvec4 xyxy READ xyxy STORED false)
    Q_PROPERTY(qbvec4 xyxz READ xyxz STORED false)
    Q_PROPERTY(qbvec4 xyxw READ xyxw STORED false)
    Q_PROPERTY(qbvec4 xyyx READ xyyx STORED false)
    Q_PROPERTY(qbvec4 xyyy READ xyyy STORED false)
    Q_PROPERTY(qbvec4 xyyz READ xyyz STORED false)
    Q_PROPERTY(qbvec4 xyyw READ xyyw STORED false)
    Q_PROPERTY(qbvec4 xyzx READ xyzx STORED false)
    Q_PROPERTY(qbvec4 xyzy READ xyzy STORED false)
    Q_PROPERTY(qbvec4 xyzz READ xyzz STORED false)
    Q_PROPERTY(qbvec4 xyzw READ xyzw STORED false)
    Q_PROPERTY(qbvec4 xywx READ xywx STORED false)
    Q_PROPERTY(qbvec4 xywy READ xywy STORED false)
    Q_PROPERTY(qbvec4 xywz READ xywz STORED false)
    Q_PROPERTY(qbvec4 xyww READ xyww STORED false)
    Q_PROPERTY(qbvec4 xzxx READ xzxx STORED false)
    Q_PROPERTY(qbvec4 xzxy READ xzxy STORED false)
    Q_PROPERTY(qbvec4 xzxz READ xzxz STORED false)
    Q_PROPERTY(qbvec4 xzxw READ xzxw STORED false)
    Q_PROPERTY(qbvec4 xzyx READ xzyx STORED false)
    Q_PROPERTY(qbvec4 xzyy READ xzyy STORED false)
    Q_PROPERTY(qbvec4 xzyz READ xzyz STORED false)
    Q_PROPERTY(qbvec4 xzyw READ xzyw STORED false)
    Q_PROPERTY(qbvec4 xzzx READ xzzx STORED false)
    Q_PROPERTY(qbvec4 xzzy READ xzzy STORED false)
    Q_PROPERTY(qbvec4 xzzz READ xzzz STORED false)
    Q_PROPERTY(qbvec4 xzzw READ xzzw STORED false)
    Q_PROPERTY(qbvec4 xzwx READ xzwx STORED false)
    Q_PROPERTY(qbvec4 xzwy READ xzwy STORED false)
    Q_PROPERTY(qbvec4 xzwz READ xzwz STORED false)
    Q_PROPERTY(qbvec4 xzww READ xzww STORED false)
    Q_PROPERTY(qbvec4 xwxx READ xwxx STORED false)
    Q_PROPERTY(qbvec4 xwxy READ xwxy STORED false)
    Q_PROPERTY(qbvec4 xwxz READ xwxz STORED false)
    Q_PROPERTY(qbvec4 xwxw READ xwxw STORED false)
    Q_PROPERTY(qbvec4 xwyx READ xwyx STORED false)
    Q_PROPERTY(qbvec4 xwyy READ xwyy STORED false)
    Q_PROPERTY(qbvec4 xwyz READ xwyz STORED false)
    Q_PROPERTY(qbvec4 xwyw READ xwyw STORED false)
    Q_PROPERTY(qbvec4 xwzx READ xwzx STORED false)
    Q_PROPERTY(qbvec4 xwzy READ xwzy STORED false)
    Q_PROPERTY(qbvec4 xwzz READ xwzz STORED false)
    Q_PROPERTY(qbvec4 xwzw READ xwzw STORED false)
    Q_PROPERTY(qbvec4 xwwx READ xwwx STORED false)
    Q_PROPERTY(qbvec4 xwwy READ xwwy STORED false)
    Q_PROPERTY(qbvec4 xwwz READ xwwz STORED false)
    Q_PROPERTY(qbvec4 xwww READ xwww STORED false)
    Q_PROPERTY(qbvec4 yxxx READ yxxx STORED false)
    Q_PROPERTY(qbvec4 yxxy READ yxxy STORED false)
    Q_PROPERTY(qbvec4 yxxz READ yxxz STORED false)
    Q_PROPERTY(qbvec4 yxxw READ yxxw STORED false)
    Q_PROPERTY(qbvec4 yxyx READ yxyx STORED false)
    Q_PROPERTY(qbvec4 yxyy READ yxyy STORED false)
    Q_PROPERTY(qbvec4 yxyz READ yxyz STORED false)
    Q_PROPERTY(qbvec4 yxyw READ yxyw STORED false)
    Q_PROPERTY(qbvec4 yxzx READ yxzx STORED false)
    Q_PROPERTY(qbvec4 yxzy READ yxzy STORED false)
    Q_PROPERTY(qbvec4 yxzz READ yxzz STORED false)
    Q_PROPERTY(qbvec4 yxzw READ yxzw STORED false)
    Q_PROPERTY(qbvec4 yxwx READ yxwx STORED false)
    Q_PROPERTY(qbvec4 yxwy READ yxwy STORED false)
    Q_PROPERTY(qbvec4 yxwz READ yxwz STORED false)
    Q_PROPERTY(qbvec4 yxww READ yxww STORED false)
    Q_PROPERTY(qbvec4 yyxx READ yyxx STORED false)
    Q_PROPERTY(qbvec4 yyxy READ yyxy STORED false)
    Q_PROPERTY(qbvec4 yyxz READ yyxz STORED false)
    Q_PROPERTY(qbvec4 yyxw READ yyxw STORED false)
    Q_PROPERTY(qbvec4 yyyx READ yyyx STORED false)
    Q_PROPERTY(qbvec4 yyyy READ yyyy STORED false)
    Q_PROPERTY(qbvec4 yyyz READ yyyz STORED false)
    Q_PROPERTY(qbvec4 yyyw READ yyyw STORED false)
    Q_PROPERTY(qbvec4 yyzx READ yyzx STORED false)
    Q_PROPERTY(qbvec4 yyzy READ yyzy STORED false)
    Q_PROPERTY(qbvec4 yyzz READ yyzz STORED false)
    Q_PROPERTY(qbvec4 yyzw READ yyzw STORED false)
    Q_PROPERTY(qbvec4 yywx READ yywx STORED false)
    Q_PROPERTY(qbvec4 yywy READ yywy STORED false)
    Q_PROPERTY(qbvec4 yywz READ yywz STORED false)
    Q_PROPERTY(qbvec4 yyww READ yyww STORED false)
    Q_PROPERTY(qbvec4 yzxx READ yzxx STORED false)
    Q_PROPERTY(qbvec4 yzxy READ yzxy STORED false)
    Q_PROPERTY(qbvec4 yzxz READ yzxz STORED false)
    Q_PROPERTY(qbvec4 yzxw READ yzxw STORED false)
    Q_PROPERTY(qbvec4 yzyx READ yzyx STORED false)
    Q_PROPERTY(qbvec4 yzyy READ yzyy STORED false)
    Q_PROPERTY(qbvec4 yzyz READ yzyz STORED false)
    Q_PROPERTY(qbvec4 yzyw READ yzyw STORED false)
    Q_PROPERTY(qbvec4 yzzx READ yzzx STORED false)
    Q_PROPERTY(qbvec4 yzzy READ yzzy STORED false)
    Q_PROPERTY(qbvec4 yzzz READ yzzz STORED false)
    Q_PROPERTY(qbvec4 yzzw READ yzzw STORED false)
    Q_PROPERTY(qbvec4 yzwx READ yzwx STORED false)
    Q_PROPERTY(qbvec4 yzwy READ yzwy STORED false)
    Q_PROPERTY(qbvec4 yzwz READ yzwz STORED false)
    Q_PROPERTY(qbvec4 yzww READ yzww STORED false)
    Q_PROPERTY(qbvec4 ywxx READ ywxx STORED false)
    Q_PROPERTY(qbvec4 ywxy READ ywxy STORED false)
    Q_PROPERTY(qbvec4 ywxz READ ywxz STORED false)
    Q_PROPERTY(qbvec4 ywxw READ ywxw STORED false)
    Q_PROPERTY(qbvec4 ywyx READ ywyx STORED false)
    Q_PROPERTY(qbvec4 ywyy READ ywyy STORED false)
    Q_PROPERTY(qbvec4 ywyz READ ywyz STORED false)
    Q_PROPERTY(qbvec4 ywyw READ ywyw STORED false)
    Q_PROPERTY(qbvec4 ywzx READ ywzx STORED false)
    Q_PROPERTY(qbvec4 ywzy READ ywzy STORED false)
    Q_PROPERTY(qbvec4 ywzz READ ywzz STORED false)
    Q_PROPERTY(qbvec4 ywzw READ ywzw STORED false)
    Q_PROPERTY(qbvec4 ywwx READ ywwx STORED false)
    Q_PROPERTY(qbvec4 ywwy READ ywwy STORED false)
    Q_PROPERTY(qbvec4 ywwz READ ywwz STORED false)
    Q_PROPERTY(qbvec4 ywww READ ywww STORED false)
    Q_PROPERTY(qbvec4 zxxx READ zxxx STORED false)
    Q_PROPERTY(qbvec4 zxxy READ zxxy STORED false)
    Q_PROPERTY(qbvec4 zxxz READ zxxz STORED false)
    Q_PROPERTY(qbvec4 zxxw READ zxxw STORED false)
    Q_PROPERTY(qbvec4 zxyx READ zxyx STORED false)
    Q_PROPERTY(qbvec4 zxyy READ zxyy STORED false)
    Q_PROPERTY(qbvec4 zxyz READ zxyz STORED false)
    Q_PROPERTY(qbvec4 zxyw READ zxyw STORED false)
    Q_PROPERTY(qbvec4 zxzx READ zxzx STORED false)
    Q_PROPERTY(qbvec4 zxzy READ zxzy STORED false)
    Q_PROPERTY(qbvec4 zxzz READ zxzz STORED false)
    Q_PROPERTY(qbvec4 zxzw READ zxzw STORED false)
    Q_PROPERTY(qbvec4 zxwx READ zxwx STORED false)
    Q_PROPERTY(qbvec4 zxwy READ zxwy STORED false)
    Q_PROPERTY(qbvec4 zxwz READ zxwz STORED false)
    Q_PROPERTY(qbvec4 zxww READ zxww STORED false)
    Q_PROPERTY(qbvec4 zyxx READ zyxx STORED false)
    Q_PROPERTY(qbvec4 zyxy READ zyxy STORED false)
    Q_PROPERTY(qbvec4 zyxz READ zyxz STORED false)
    Q_PROPERTY(qbvec4 zyxw READ zyxw STORED false)
    Q_PROPERTY(qbvec4 zyyx READ zyyx STORED false)
    Q_PROPERTY(qbvec4 zyyy READ zyyy STORED false)
    Q_PROPERTY(qbvec4 zyyz READ zyyz STORED false)
    Q_PROPERTY(qbvec4 zyyw READ zyyw STORED false)
    Q_PROPERTY(qbvec4 zyzx READ zyzx STORED false)
    Q_PROPERTY(qbvec4 zyzy READ zyzy STORED false)
    Q_PROPERTY(qbvec4 zyzz READ zyzz STORED false)
    Q_PROPERTY(qbvec4 zyzw READ zyzw STORED false)
    Q_PROPERTY(qbvec4 zywx READ zywx STORED false)
    Q_PROPERTY(qbvec4 zywy READ zywy STORED false)
    Q_PROPERTY(qbvec4 zywz READ zywz STORED false)
    Q_PROPERTY(qbvec4 zyww READ zyww STORED false)
    Q_PROPERTY(qbvec4 zzxx READ zzxx STORED false)
    Q_PROPERTY(qbvec4 zzxy READ zzxy STORED false)
    Q_PROPERTY(qbvec4 zzxz READ zzxz STORED false)
    Q_PROPERTY(qbvec4 zzxw READ zzxw STORED false)
    Q_PROPERTY(qbvec4 zzyx READ zzyx STORED false)
    Q_PROPERTY(qbvec4 zzyy READ zzyy STORED false)
    Q_PROPERTY(qbvec4 zzyz READ zzyz STORED false)
    Q_PROPERTY(qbvec4 zzyw READ zzyw STORED false)
    Q_PROPERTY(qbvec4 zzzx READ zzzx STORED false)
    Q_PROPERTY(qbvec4 zzzy READ zzzy STORED false)
    Q_PROPERTY(qbvec4 zzzz READ zzzz STORED false)
    Q_PROPERTY(qbvec4 zzzw READ zzzw STORED false)
    Q_PROPERTY(qbvec4 zzwx READ zzwx STORED false)
    Q_PROPERTY(qbvec4 zzwy READ zzwy STORED false)
    Q_PROPERTY(qbvec4 zzwz READ zzwz STORED false)
    Q_PROPERTY(qbvec4 zzww READ zzww STORED false)
    Q_PROPERTY(qbvec4 zwxx READ zwxx STORED false)
    Q_PROPERTY(qbvec4 zwxy READ zwxy STORED false)
    Q_PROPERTY(qbvec4 zwxz READ zwxz STORED false)
    Q_PROPERTY(qbvec4 zwxw READ zwxw STORED false)
    Q_PROPERTY(qbvec4 zwyx READ zwyx STORED false)
    Q_PROPERTY(qbvec4 zwyy READ zwyy STORED false)
    Q_PROPERTY(qbvec4 zwyz READ zwyz STORED false)
    Q_PROPERTY(qbvec4 zwyw READ zwyw STORED false)
    Q_PROPERTY(qbvec4 zwzx READ zwzx STORED false)
    Q_PROPERTY(qbvec4 zwzy READ zwzy STORED false)
    Q_PROPERTY(qbvec4 zwzz READ zwzz STORED false)
    Q_PROPERTY(qbvec4 zwzw READ zwzw STORED false)
    Q_PROPERTY(qbvec4 zwwx READ zwwx STORED false)
    Q_PROPERTY(qbvec4 zwwy READ zwwy STORED false)
    Q_PROPERTY(qbvec4 zwwz READ zwwz STORED false)
    Q_PROPERTY(qbvec4 zwww READ zwww STORED false)
    Q_PROPERTY(qbvec4 wxxx READ wxxx STORED false)
    Q_PROPERTY(qbvec4 wxxy READ wxxy STORED false)
    Q_PROPERTY(qbvec4 wxxz READ wxxz STORED false)
    Q_PROPERTY(qbvec4 wxxw READ wxxw STORED false)
    Q_PROPERTY(qbvec4 wxyx READ wxyx STORED false)
    Q_PROPERTY(qbvec4 wxyy READ wxyy STORED false)
    Q_PROPERTY(qbvec4 wxyz READ wxyz STORED false)
    Q_PROPERTY(qbvec4 wxyw READ wxyw STORED false)
    Q_PROPERTY(qbvec4 wxzx READ wxzx STORED false)
    Q_PROPERTY(qbvec4 wxzy READ wxzy STORED false)
    Q_PROPERTY(qbvec4 wxzz READ wxzz STORED false)
    Q_PROPERTY(qbvec4 wxzw READ wxzw STORED false)
    Q_PROPERTY(qbvec4 wxwx READ wxwx STORED false)
    Q_PROPERTY(qbvec4 wxwy READ wxwy STORED false)
    Q_PROPERTY(qbvec4 wxwz READ wxwz STORED false)
    Q_PROPERTY(qbvec4 wxww READ wxww STORED false)
    Q_PROPERTY(qbvec4 wyxx READ wyxx STORED false)
    Q_PROPERTY(qbvec4 wyxy READ wyxy STORED false)
    Q_PROPERTY(qbvec4 wyxz READ wyxz STORED false)
    Q_PROPERTY(qbvec4 wyxw READ wyxw STORED false)
    Q_PROPERTY(qbvec4 wyyx READ wyyx STORED false)
    Q_PROPERTY(qbvec4 wyyy READ wyyy STORED false)
    Q_PROPERTY(qbvec4 wyyz READ wyyz STORED false)
    Q_PROPERTY(qbvec4 wyyw READ wyyw STORED false)
    Q_PROPERTY(qbvec4 wyzx READ wyzx STORED false)
    Q_PROPERTY(qbvec4 wyzy READ wyzy STORED false)
    Q_PROPERTY(qbvec4 wyzz READ wyzz STORED false)
    Q_PROPERTY(qbvec4 wyzw READ wyzw STORED false)
    Q_PROPERTY(qbvec4 wywx READ wywx STORED false)
    Q_PROPERTY(qbvec4 wywy READ wywy STORED false)
    Q_PROPERTY(qbvec4 wywz READ wywz STORED false)
    Q_PROPERTY(qbvec4 wyww READ wyww STORED false)
    Q_PROPERTY(qbvec4 wzxx READ wzxx STORED false)
    Q_PROPERTY(qbvec4 wzxy READ wzxy STORED false)
    Q_PROPERTY(qbvec4 wzxz READ wzxz STORED false)
    Q_PROPERTY(qbvec4 wzxw READ wzxw STORED false)
    Q_PROPERTY(qbvec4 wzyx READ wzyx STORED false)
    Q_PROPERTY(qbvec4 wzyy READ wzyy STORED false)
    Q_PROPERTY(qbvec4 wzyz READ wzyz STORED false)
    Q_PROPERTY(qbvec4 wzyw READ wzyw STORED false)
    Q_PROPERTY(qbvec4 wzzx READ wzzx STORED false)
    Q_PROPERTY(qbvec4 wzzy READ wzzy STORED false)
    Q_PROPERTY(qbvec4 wzzz READ wzzz STORED false)
    Q_PROPERTY(qbvec4 wzzw READ wzzw STORED false)
    Q_PROPERTY(qbvec4 wzwx READ wzwx STORED false)
    Q_PROPERTY(qbvec4 wzwy READ wzwy STORED false)
    Q_PROPERTY(qbvec4 wzwz READ wzwz STORED false)
    Q_PROPERTY(qbvec4 wzww READ wzww STORED false)
    Q_PROPERTY(qbvec4 wwxx READ wwxx STORED false)
    Q_PROPERTY(qbvec4 wwxy READ wwxy STORED false)
    Q_PROPERTY(qbvec4 wwxz READ wwxz STORED false)
    Q_PROPERTY(qbvec4 wwxw READ wwxw STORED false)
    Q_PROPERTY(qbvec4 wwyx READ wwyx STORED false)
    Q_PROPERTY(qbvec4 wwyy READ wwyy STORED false)
    Q_PROPERTY(qbvec4 wwyz READ wwyz STORED false)
    Q_PROPERTY(qbvec4 wwyw READ wwyw STORED false)
    Q_PROPERTY(qbvec4 wwzx READ wwzx STORED false)
    Q_PROPERTY(qbvec4 wwzy READ wwzy STORED false)
    Q_PROPERTY(qbvec4 wwzz READ wwzz STORED false)
    Q_PROPERTY(qbvec4 wwzw READ wwzw STORED false)
    Q_PROPERTY(qbvec4 wwwx READ wwwx STORED false)
    Q_PROPERTY(qbvec4 wwwy READ wwwy STORED false)
    Q_PROPERTY(qbvec4 wwwz READ wwwz STORED false)
    Q_PROPERTY(qbvec4 wwww READ wwww STORED false)

public:

    using vector4<bool>::vector4;
    using QGenericMatrix<1,4,bool>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

inline QDebug operator << (QDebug debug, const qvec4 &value)
{
    debug.nospace() << "vec4(" << value.x() << ", " << value.y() << ", " << value.z() << ", " << value.w() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qivec4 &value)
{
    debug.nospace() << "ivec4(" << value.x() << ", " << value.y() << ", " << value.z() << ", " << value.w() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const quvec4 &value)
{
    debug.nospace() << "uvec4(" << value.x() << ", " << value.y() << ", " << value.z() << ", " << value.w() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qdvec4 &value)
{
    debug.nospace() << "dvec4(" << value.x() << ", " << value.y() << ", " << value.z() << ", " << value.w() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qbvec4 &value)
{
    debug.nospace() << "bvec4(" << value.x() << ", " << value.y() << ", " << value.z() << ", " << value.w() << ")";
    return debug.space();
}

Q_DECLARE_TYPEINFO(qvec4, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qivec4, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(quvec4, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdvec4, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qbvec4, Q_MOVABLE_TYPE);

Q_DECLARE_METATYPE(qvec4)
Q_DECLARE_METATYPE(qivec4)
Q_DECLARE_METATYPE(quvec4)
Q_DECLARE_METATYPE(qdvec4)
Q_DECLARE_METATYPE(qbvec4)

#endif // QUICKVECTOR4_H

/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#ifndef QUICKOPENGL_QUICKOPENGLTHREAD_H
#define QUICKOPENGL_QUICKOPENGLTHREAD_H

#include <QThread>
#include <QScopedPointer>
#include <QThreadPool>
#include <QHash>
#include <QMutex>
#include <QOpenGLContext>
/*!
 * \brief QuickOpenGL Thread Pool
 *
 * Threads in this thread pool own an OpenGL Context that is shared with QSGRenderThread, and an
 * offscreen surface.
 *
 * The static function #globalInstance() should only be called from QSGRenderThread.
 *
 * The thread pool contains only one thread which never expires.
 *
 * The purpose of the threadpool is to handoff OpenGL tasks from QSGRenderThread which would cause
 * an OpenGL synchronisation point, such as buffer reads etc.
 */
class QuickOpenGLThreadPool
{
public:

    /*!
     * \brief Get the global instance of the thread pool for the current context
     * \return the global instance of the thread pool for the current context
     *
     * This function should only be called from QSGRenderThread.  This is so that the threads
     * within the thread pool can share the QSGRenderThread openGL context.
     */
    static QThreadPool *globalInstance();

private:

    /*!
     * \brief Thread pool mutex
     */
    static QMutex threadPoolMutex;

    /*!
     * \brief Private hash map of threadpool for context
     */
    static QHash<QOpenGLContext *, QThreadPool *> threadPool;

};


#endif // QUICKOPENGLTHREAD_H

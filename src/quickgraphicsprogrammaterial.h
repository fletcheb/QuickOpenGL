/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/



#ifndef QUICKGRAPHICSPROGRAMMATERIAL_H
#define QUICKGRAPHICSPROGRAMMATERIAL_H

#include <QSGMaterial>
#include "introspection.h"

class QuickGraphicsProgramMaterial : public QSGMaterial
{
public:

    QuickGraphicsProgramMaterial();
    virtual ~QuickGraphicsProgramMaterial() override;

    QSGMaterialType *type() const override;
    QSGMaterialShader *createShader() const override;

    // List of geometry attributes
    QList<Introspection::Attribute> attributes;

    // Hash table of material properties
    QVariantHash properties;

    // MD5 hash of the combined shader source code
    QByteArray identifier;

};

#endif // QUICKGRAPHICSPROGRAMMATERIAL_H

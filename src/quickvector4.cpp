#include "quickvector4.h"
#include "quickmatrix4.h"


/*!
 * \qmlbasictype vec4
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The vec4 basic type represents a GLSL vec4 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty float vec4::x
 *
 * This property holds the x component of the vec4.
 */

/*!
 * \qmlproperty float vec4::y
 *
 * This property holds the y component of the vec4.
 */

/*!
 * \qmlproperty float vec4::z
 *
 * This property holds the z component of the vec4.
 */

/*!
 * \qmlproperty float vec4::w
 *
 * This property holds the w component of the vec4.
 */

/*!
 * \qmlproperty vec2 vec4::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty vec2 vec4::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty vec2 vec4::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty vec2 vec4::xw
 *
 * This property holds the swizzled xw value
 */

/*!
 * \qmlproperty vec2 vec4::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty vec2 vec4::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty vec2 vec4::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty vec2 vec4::yw
 *
 * This property holds the swizzled yw value
 */

/*!
 * \qmlproperty vec2 vec4::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty vec2 vec4::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty vec2 vec4::zz
 *
 * This property holds the swizzled zz value
 */

/*!
 * \qmlproperty vec2 vec4::zw
 *
 * This property holds the swizzled zw value
 */

/*!
 * \qmlproperty vec2 vec4::wx
 *
 * This property holds the swizzled wx value
 */

/*!
 * \qmlproperty vec2 vec4::wy
 *
 * This property holds the swizzled wy value
 */

/*!
 * \qmlproperty vec2 vec4::wz
 *
 * This property holds the swizzled wz value
 */

/*!
 * \qmlproperty vec2 vec4::ww
 *
 * This property holds the swizzled ww value
 */


/*!
 * \qmlproperty vec3 vec4::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty vec3 vec4::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty vec3 vec4::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty vec3 vec4::xxw
 *
 * This property holds the swizzled xxw value
 */

/*!
 * \qmlproperty vec3 vec4::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty vec3 vec4::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty vec3 vec4::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty vec3 vec4::xyw
 *
 * This property holds the swizzled xyw value
 */

/*!
 * \qmlproperty vec3 vec4::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty vec3 vec4::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty vec3 vec4::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty vec3 vec4::xzw
 *
 * This property holds the swizzled xzw value
 */

/*!
 * \qmlproperty vec3 vec4::xwx
 *
 * This property holds the swizzled xwx value
 */

/*!
 * \qmlproperty vec3 vec4::xwy
 *
 * This property holds the swizzled xwy value
 */

/*!
 * \qmlproperty vec3 vec4::xwz
 *
 * This property holds the swizzled xwz value
 */

/*!
 * \qmlproperty vec3 vec4::xww
 *
 * This property holds the swizzled xww value
 */

/*!
 * \qmlproperty vec3 vec4::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty vec3 vec4::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty vec3 vec4::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty vec3 vec4::yxw
 *
 * This property holds the swizzled yxw value
 */

/*!
 * \qmlproperty vec3 vec4::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty vec3 vec4::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty vec3 vec4::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty vec3 vec4::yyw
 *
 * This property holds the swizzled yyw value
 */

/*!
 * \qmlproperty vec3 vec4::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty vec3 vec4::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty vec3 vec4::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty vec3 vec4::yzw
 *
 * This property holds the swizzled yzw value
 */

/*!
 * \qmlproperty vec3 vec4::ywx
 *
 * This property holds the swizzled ywx value
 */

/*!
 * \qmlproperty vec3 vec4::ywy
 *
 * This property holds the swizzled ywy value
 */

/*!
 * \qmlproperty vec3 vec4::ywz
 *
 * This property holds the swizzled ywz value
 */

/*!
 * \qmlproperty vec3 vec4::yww
 *
 * This property holds the swizzled yww value
 */

/*!
 * \qmlproperty vec3 vec4::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty vec3 vec4::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty vec3 vec4::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty vec3 vec4::zxw
 *
 * This property holds the swizzled zxw value
 */

/*!
 * \qmlproperty vec3 vec4::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty vec3 vec4::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty vec3 vec4::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty vec3 vec4::zyw
 *
 * This property holds the swizzled zyw value
 */

/*!
 * \qmlproperty vec3 vec4::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty vec3 vec4::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty vec3 vec4::zzz
 *
 * This property holds the swizzled zzz value
 */

/*!
 * \qmlproperty vec3 vec4::zzw
 *
 * This property holds the swizzled zzw value
 */

/*!
 * \qmlproperty vec3 vec4::zwx
 *
 * This property holds the swizzled zwx value
 */

/*!
 * \qmlproperty vec3 vec4::zwy
 *
 * This property holds the swizzled zwy value
 */

/*!
 * \qmlproperty vec3 vec4::zwz
 *
 * This property holds the swizzled zwz value
 */

/*!
 * \qmlproperty vec3 vec4::zww
 *
 * This property holds the swizzled zww value
 */

/*!
 * \qmlproperty vec3 vec4::wxx
 *
 * This property holds the swizzled wxx value
 */

/*!
 * \qmlproperty vec3 vec4::wxy
 *
 * This property holds the swizzled wxy value
 */

/*!
 * \qmlproperty vec3 vec4::wxz
 *
 * This property holds the swizzled wxz value
 */

/*!
 * \qmlproperty vec3 vec4::wxw
 *
 * This property holds the swizzled wxw value
 */

/*!
 * \qmlproperty vec3 vec4::wyx
 *
 * This property holds the swizzled wyx value
 */

/*!
 * \qmlproperty vec3 vec4::wyy
 *
 * This property holds the swizzled wyy value
 */

/*!
 * \qmlproperty vec3 vec4::wyz
 *
 * This property holds the swizzled wyz value
 */

/*!
 * \qmlproperty vec3 vec4::wyw
 *
 * This property holds the swizzled wyw value
 */

/*!
 * \qmlproperty vec3 vec4::wzx
 *
 * This property holds the swizzled wzx value
 */

/*!
 * \qmlproperty vec3 vec4::wzy
 *
 * This property holds the swizzled wzy value
 */

/*!
 * \qmlproperty vec3 vec4::wzz
 *
 * This property holds the swizzled wzz value
 */

/*!
 * \qmlproperty vec3 vec4::wzw
 *
 * This property holds the swizzled wzw value
 */

/*!
 * \qmlproperty vec3 vec4::wwx
 *
 * This property holds the swizzled wwx value
 */

/*!
 * \qmlproperty vec3 vec4::wwy
 *
 * This property holds the swizzled wwy value
 */

/*!
 * \qmlproperty vec3 vec4::wwz
 *
 * This property holds the swizzled wwz value
 */

/*!
 * \qmlproperty vec3 vec4::www
 *
 * This property holds the swizzled www value
 */



/*!
 * \qmlproperty vec4 vec4::xxxx
 *
 * This property holds the swizzled xxxx value
 */

/*!
 * \qmlproperty vec4 vec4::xxxy
 *
 * This property holds the swizzled xxxy value
 */

/*!
 * \qmlproperty vec4 vec4::xxxz
 *
 * This property holds the swizzled xxxz value
 */

/*!
 * \qmlproperty vec4 vec4::xxxw
 *
 * This property holds the swizzled xxxw value
 */

/*!
 * \qmlproperty vec4 vec4::xxyx
 *
 * This property holds the swizzled xxyx value
 */

/*!
 * \qmlproperty vec4 vec4::xxyy
 *
 * This property holds the swizzled xxyy value
 */

/*!
 * \qmlproperty vec4 vec4::xxyz
 *
 * This property holds the swizzled xxyz value
 */

/*!
 * \qmlproperty vec4 vec4::xxyw
 *
 * This property holds the swizzled xxyw value
 */

/*!
 * \qmlproperty vec4 vec4::xxzx
 *
 * This property holds the swizzled xxzx value
 */

/*!
 * \qmlproperty vec4 vec4::xxzy
 *
 * This property holds the swizzled xxzy value
 */

/*!
 * \qmlproperty vec4 vec4::xxzz
 *
 * This property holds the swizzled xxzz value
 */

/*!
 * \qmlproperty vec4 vec4::xxzw
 *
 * This property holds the swizzled xxzw value
 */

/*!
 * \qmlproperty vec4 vec4::xxwx
 *
 * This property holds the swizzled xxwx value
 */

/*!
 * \qmlproperty vec4 vec4::xxwy
 *
 * This property holds the swizzled xxwy value
 */

/*!
 * \qmlproperty vec4 vec4::xxwz
 *
 * This property holds the swizzled xxwz value
 */

/*!
 * \qmlproperty vec4 vec4::xxww
 *
 * This property holds the swizzled xxww value
 */

/*!
 * \qmlproperty vec4 vec4::xyxx
 *
 * This property holds the swizzled xyxx value
 */

/*!
 * \qmlproperty vec4 vec4::xyxy
 *
 * This property holds the swizzled xyxy value
 */

/*!
 * \qmlproperty vec4 vec4::xyxz
 *
 * This property holds the swizzled xyxz value
 */

/*!
 * \qmlproperty vec4 vec4::xyxw
 *
 * This property holds the swizzled xyxw value
 */

/*!
 * \qmlproperty vec4 vec4::xyyx
 *
 * This property holds the swizzled xyyx value
 */

/*!
 * \qmlproperty vec4 vec4::xyyy
 *
 * This property holds the swizzled xyyy value
 */

/*!
 * \qmlproperty vec4 vec4::xyyz
 *
 * This property holds the swizzled xyyz value
 */

/*!
 * \qmlproperty vec4 vec4::xyyw
 *
 * This property holds the swizzled xyyw value
 */

/*!
 * \qmlproperty vec4 vec4::xyzx
 *
 * This property holds the swizzled xyzx value
 */

/*!
 * \qmlproperty vec4 vec4::xyzy
 *
 * This property holds the swizzled xyzy value
 */

/*!
 * \qmlproperty vec4 vec4::xyzz
 *
 * This property holds the swizzled xyzz value
 */

/*!
 * \qmlproperty vec4 vec4::xyzw
 *
 * This property holds the swizzled xyzw value
 */

/*!
 * \qmlproperty vec4 vec4::xywx
 *
 * This property holds the swizzled xywx value
 */

/*!
 * \qmlproperty vec4 vec4::xywy
 *
 * This property holds the swizzled xywy value
 */

/*!
 * \qmlproperty vec4 vec4::xywz
 *
 * This property holds the swizzled xywz value
 */

/*!
 * \qmlproperty vec4 vec4::xyww
 *
 * This property holds the swizzled xyww value
 */

/*!
 * \qmlproperty vec4 vec4::xzxx
 *
 * This property holds the swizzled xzxx value
 */

/*!
 * \qmlproperty vec4 vec4::xzxy
 *
 * This property holds the swizzled xzxy value
 */

/*!
 * \qmlproperty vec4 vec4::xzxz
 *
 * This property holds the swizzled xzxz value
 */

/*!
 * \qmlproperty vec4 vec4::xzxw
 *
 * This property holds the swizzled xzxw value
 */

/*!
 * \qmlproperty vec4 vec4::xzyx
 *
 * This property holds the swizzled xzyx value
 */

/*!
 * \qmlproperty vec4 vec4::xzyy
 *
 * This property holds the swizzled xzyy value
 */

/*!
 * \qmlproperty vec4 vec4::xzyz
 *
 * This property holds the swizzled xzyz value
 */

/*!
 * \qmlproperty vec4 vec4::xzyw
 *
 * This property holds the swizzled xzyw value
 */

/*!
 * \qmlproperty vec4 vec4::xzzx
 *
 * This property holds the swizzled xzzx value
 */

/*!
 * \qmlproperty vec4 vec4::xzzy
 *
 * This property holds the swizzled xzzy value
 */

/*!
 * \qmlproperty vec4 vec4::xzzz
 *
 * This property holds the swizzled xzzz value
 */

/*!
 * \qmlproperty vec4 vec4::xzzw
 *
 * This property holds the swizzled xzzw value
 */

/*!
 * \qmlproperty vec4 vec4::xzwx
 *
 * This property holds the swizzled xzwx value
 */

/*!
 * \qmlproperty vec4 vec4::xzwy
 *
 * This property holds the swizzled xzwy value
 */

/*!
 * \qmlproperty vec4 vec4::xzwz
 *
 * This property holds the swizzled xzwz value
 */

/*!
 * \qmlproperty vec4 vec4::xzww
 *
 * This property holds the swizzled xzww value
 */

/*!
 * \qmlproperty vec4 vec4::xwxx
 *
 * This property holds the swizzled xwxx value
 */

/*!
 * \qmlproperty vec4 vec4::xwxy
 *
 * This property holds the swizzled xwxy value
 */

/*!
 * \qmlproperty vec4 vec4::xwxz
 *
 * This property holds the swizzled xwxz value
 */

/*!
 * \qmlproperty vec4 vec4::xwxw
 *
 * This property holds the swizzled xwxw value
 */

/*!
 * \qmlproperty vec4 vec4::xwyx
 *
 * This property holds the swizzled xwyx value
 */

/*!
 * \qmlproperty vec4 vec4::xwyy
 *
 * This property holds the swizzled xwyy value
 */

/*!
 * \qmlproperty vec4 vec4::xwyz
 *
 * This property holds the swizzled xwyz value
 */

/*!
 * \qmlproperty vec4 vec4::xwyw
 *
 * This property holds the swizzled xwyw value
 */

/*!
 * \qmlproperty vec4 vec4::xwzx
 *
 * This property holds the swizzled xwzx value
 */

/*!
 * \qmlproperty vec4 vec4::xwzy
 *
 * This property holds the swizzled xwzy value
 */

/*!
 * \qmlproperty vec4 vec4::xwzz
 *
 * This property holds the swizzled xwzz value
 */

/*!
 * \qmlproperty vec4 vec4::xwzw
 *
 * This property holds the swizzled xwzw value
 */

/*!
 * \qmlproperty vec4 vec4::xwwx
 *
 * This property holds the swizzled xwwx value
 */

/*!
 * \qmlproperty vec4 vec4::xwwy
 *
 * This property holds the swizzled xwwy value
 */

/*!
 * \qmlproperty vec4 vec4::xwwz
 *
 * This property holds the swizzled xwwz value
 */

/*!
 * \qmlproperty vec4 vec4::xwww
 *
 * This property holds the swizzled xwww value
 */

/*!
 * \qmlproperty vec4 vec4::yxxx
 *
 * This property holds the swizzled yxxx value
 */

/*!
 * \qmlproperty vec4 vec4::yxxy
 *
 * This property holds the swizzled yxxy value
 */

/*!
 * \qmlproperty vec4 vec4::yxxz
 *
 * This property holds the swizzled yxxz value
 */

/*!
 * \qmlproperty vec4 vec4::yxxw
 *
 * This property holds the swizzled yxxw value
 */

/*!
 * \qmlproperty vec4 vec4::yxyx
 *
 * This property holds the swizzled yxyx value
 */

/*!
 * \qmlproperty vec4 vec4::yxyy
 *
 * This property holds the swizzled yxyy value
 */

/*!
 * \qmlproperty vec4 vec4::yxyz
 *
 * This property holds the swizzled yxyz value
 */

/*!
 * \qmlproperty vec4 vec4::yxyw
 *
 * This property holds the swizzled yxyw value
 */

/*!
 * \qmlproperty vec4 vec4::yxzx
 *
 * This property holds the swizzled yxzx value
 */

/*!
 * \qmlproperty vec4 vec4::yxzy
 *
 * This property holds the swizzled yxzy value
 */

/*!
 * \qmlproperty vec4 vec4::yxzz
 *
 * This property holds the swizzled yxzz value
 */

/*!
 * \qmlproperty vec4 vec4::yxzw
 *
 * This property holds the swizzled yxzw value
 */

/*!
 * \qmlproperty vec4 vec4::yxwx
 *
 * This property holds the swizzled yxwx value
 */

/*!
 * \qmlproperty vec4 vec4::yxwy
 *
 * This property holds the swizzled yxwy value
 */

/*!
 * \qmlproperty vec4 vec4::yxwz
 *
 * This property holds the swizzled yxwz value
 */

/*!
 * \qmlproperty vec4 vec4::yxww
 *
 * This property holds the swizzled yxww value
 */

/*!
 * \qmlproperty vec4 vec4::yyxx
 *
 * This property holds the swizzled yyxx value
 */

/*!
 * \qmlproperty vec4 vec4::yyxy
 *
 * This property holds the swizzled yyxy value
 */

/*!
 * \qmlproperty vec4 vec4::yyxz
 *
 * This property holds the swizzled yyxz value
 */

/*!
 * \qmlproperty vec4 vec4::yyxw
 *
 * This property holds the swizzled yyxw value
 */

/*!
 * \qmlproperty vec4 vec4::yyyx
 *
 * This property holds the swizzled yyyx value
 */

/*!
 * \qmlproperty vec4 vec4::yyyy
 *
 * This property holds the swizzled yyyy value
 */

/*!
 * \qmlproperty vec4 vec4::yyyz
 *
 * This property holds the swizzled yyyz value
 */

/*!
 * \qmlproperty vec4 vec4::yyyw
 *
 * This property holds the swizzled yyyw value
 */

/*!
 * \qmlproperty vec4 vec4::yyzx
 *
 * This property holds the swizzled yyzx value
 */

/*!
 * \qmlproperty vec4 vec4::yyzy
 *
 * This property holds the swizzled yyzy value
 */

/*!
 * \qmlproperty vec4 vec4::yyzz
 *
 * This property holds the swizzled yyzz value
 */

/*!
 * \qmlproperty vec4 vec4::yyzw
 *
 * This property holds the swizzled yyzw value
 */

/*!
 * \qmlproperty vec4 vec4::yywx
 *
 * This property holds the swizzled yywx value
 */

/*!
 * \qmlproperty vec4 vec4::yywy
 *
 * This property holds the swizzled yywy value
 */

/*!
 * \qmlproperty vec4 vec4::yywz
 *
 * This property holds the swizzled yywz value
 */

/*!
 * \qmlproperty vec4 vec4::yyww
 *
 * This property holds the swizzled yyww value
 */

/*!
 * \qmlproperty vec4 vec4::yzxx
 *
 * This property holds the swizzled yzxx value
 */

/*!
 * \qmlproperty vec4 vec4::yzxy
 *
 * This property holds the swizzled yzxy value
 */

/*!
 * \qmlproperty vec4 vec4::yzxz
 *
 * This property holds the swizzled yzxz value
 */

/*!
 * \qmlproperty vec4 vec4::yzxw
 *
 * This property holds the swizzled yzxw value
 */

/*!
 * \qmlproperty vec4 vec4::yzyx
 *
 * This property holds the swizzled yzyx value
 */

/*!
 * \qmlproperty vec4 vec4::yzyy
 *
 * This property holds the swizzled yzyy value
 */

/*!
 * \qmlproperty vec4 vec4::yzyz
 *
 * This property holds the swizzled yzyz value
 */

/*!
 * \qmlproperty vec4 vec4::yzyw
 *
 * This property holds the swizzled yzyw value
 */

/*!
 * \qmlproperty vec4 vec4::yzzx
 *
 * This property holds the swizzled yzzx value
 */

/*!
 * \qmlproperty vec4 vec4::yzzy
 *
 * This property holds the swizzled yzzy value
 */

/*!
 * \qmlproperty vec4 vec4::yzzz
 *
 * This property holds the swizzled yzzz value
 */

/*!
 * \qmlproperty vec4 vec4::yzzw
 *
 * This property holds the swizzled yzzw value
 */

/*!
 * \qmlproperty vec4 vec4::yzwx
 *
 * This property holds the swizzled yzwx value
 */

/*!
 * \qmlproperty vec4 vec4::yzwy
 *
 * This property holds the swizzled yzwy value
 */

/*!
 * \qmlproperty vec4 vec4::yzwz
 *
 * This property holds the swizzled yzwz value
 */

/*!
 * \qmlproperty vec4 vec4::yzww
 *
 * This property holds the swizzled yzww value
 */

/*!
 * \qmlproperty vec4 vec4::ywxx
 *
 * This property holds the swizzled ywxx value
 */

/*!
 * \qmlproperty vec4 vec4::ywxy
 *
 * This property holds the swizzled ywxy value
 */

/*!
 * \qmlproperty vec4 vec4::ywxz
 *
 * This property holds the swizzled ywxz value
 */

/*!
 * \qmlproperty vec4 vec4::ywxw
 *
 * This property holds the swizzled ywxw value
 */

/*!
 * \qmlproperty vec4 vec4::ywyx
 *
 * This property holds the swizzled ywyx value
 */

/*!
 * \qmlproperty vec4 vec4::ywyy
 *
 * This property holds the swizzled ywyy value
 */

/*!
 * \qmlproperty vec4 vec4::ywyz
 *
 * This property holds the swizzled ywyz value
 */

/*!
 * \qmlproperty vec4 vec4::ywyw
 *
 * This property holds the swizzled ywyw value
 */

/*!
 * \qmlproperty vec4 vec4::ywzx
 *
 * This property holds the swizzled ywzx value
 */

/*!
 * \qmlproperty vec4 vec4::ywzy
 *
 * This property holds the swizzled ywzy value
 */

/*!
 * \qmlproperty vec4 vec4::ywzz
 *
 * This property holds the swizzled ywzz value
 */

/*!
 * \qmlproperty vec4 vec4::ywzw
 *
 * This property holds the swizzled ywzw value
 */

/*!
 * \qmlproperty vec4 vec4::ywwx
 *
 * This property holds the swizzled ywwx value
 */

/*!
 * \qmlproperty vec4 vec4::ywwy
 *
 * This property holds the swizzled ywwy value
 */

/*!
 * \qmlproperty vec4 vec4::ywwz
 *
 * This property holds the swizzled ywwz value
 */

/*!
 * \qmlproperty vec4 vec4::ywww
 *
 * This property holds the swizzled ywww value
 */

/*!
 * \qmlproperty vec4 vec4::zxxx
 *
 * This property holds the swizzled zxxx value
 */

/*!
 * \qmlproperty vec4 vec4::zxxy
 *
 * This property holds the swizzled zxxy value
 */

/*!
 * \qmlproperty vec4 vec4::zxxz
 *
 * This property holds the swizzled zxxz value
 */

/*!
 * \qmlproperty vec4 vec4::zxxw
 *
 * This property holds the swizzled zxxw value
 */

/*!
 * \qmlproperty vec4 vec4::zxyx
 *
 * This property holds the swizzled zxyx value
 */

/*!
 * \qmlproperty vec4 vec4::zxyy
 *
 * This property holds the swizzled zxyy value
 */

/*!
 * \qmlproperty vec4 vec4::zxyz
 *
 * This property holds the swizzled zxyz value
 */

/*!
 * \qmlproperty vec4 vec4::zxyw
 *
 * This property holds the swizzled zxyw value
 */

/*!
 * \qmlproperty vec4 vec4::zxzx
 *
 * This property holds the swizzled zxzx value
 */

/*!
 * \qmlproperty vec4 vec4::zxzy
 *
 * This property holds the swizzled zxzy value
 */

/*!
 * \qmlproperty vec4 vec4::zxzz
 *
 * This property holds the swizzled zxzz value
 */

/*!
 * \qmlproperty vec4 vec4::zxzw
 *
 * This property holds the swizzled zxzw value
 */

/*!
 * \qmlproperty vec4 vec4::zxwx
 *
 * This property holds the swizzled zxwx value
 */

/*!
 * \qmlproperty vec4 vec4::zxwy
 *
 * This property holds the swizzled zxwy value
 */

/*!
 * \qmlproperty vec4 vec4::zxwz
 *
 * This property holds the swizzled zxwz value
 */

/*!
 * \qmlproperty vec4 vec4::zxww
 *
 * This property holds the swizzled zxww value
 */

/*!
 * \qmlproperty vec4 vec4::zyxx
 *
 * This property holds the swizzled zyxx value
 */

/*!
 * \qmlproperty vec4 vec4::zyxy
 *
 * This property holds the swizzled zyxy value
 */

/*!
 * \qmlproperty vec4 vec4::zyxz
 *
 * This property holds the swizzled zyxz value
 */

/*!
 * \qmlproperty vec4 vec4::zyxw
 *
 * This property holds the swizzled zyxw value
 */

/*!
 * \qmlproperty vec4 vec4::zyyx
 *
 * This property holds the swizzled zyyx value
 */

/*!
 * \qmlproperty vec4 vec4::zyyy
 *
 * This property holds the swizzled zyyy value
 */

/*!
 * \qmlproperty vec4 vec4::zyyz
 *
 * This property holds the swizzled zyyz value
 */

/*!
 * \qmlproperty vec4 vec4::zyyw
 *
 * This property holds the swizzled zyyw value
 */

/*!
 * \qmlproperty vec4 vec4::zyzx
 *
 * This property holds the swizzled zyzx value
 */

/*!
 * \qmlproperty vec4 vec4::zyzy
 *
 * This property holds the swizzled zyzy value
 */

/*!
 * \qmlproperty vec4 vec4::zyzz
 *
 * This property holds the swizzled zyzz value
 */

/*!
 * \qmlproperty vec4 vec4::zyzw
 *
 * This property holds the swizzled zyzw value
 */

/*!
 * \qmlproperty vec4 vec4::zywx
 *
 * This property holds the swizzled zywx value
 */

/*!
 * \qmlproperty vec4 vec4::zywy
 *
 * This property holds the swizzled zywy value
 */

/*!
 * \qmlproperty vec4 vec4::zywz
 *
 * This property holds the swizzled zywz value
 */

/*!
 * \qmlproperty vec4 vec4::zyww
 *
 * This property holds the swizzled zyww value
 */

/*!
 * \qmlproperty vec4 vec4::zzxx
 *
 * This property holds the swizzled zzxx value
 */

/*!
 * \qmlproperty vec4 vec4::zzxy
 *
 * This property holds the swizzled zzxy value
 */

/*!
 * \qmlproperty vec4 vec4::zzxz
 *
 * This property holds the swizzled zzxz value
 */

/*!
 * \qmlproperty vec4 vec4::zzxw
 *
 * This property holds the swizzled zzxw value
 */

/*!
 * \qmlproperty vec4 vec4::zzyx
 *
 * This property holds the swizzled zzyx value
 */

/*!
 * \qmlproperty vec4 vec4::zzyy
 *
 * This property holds the swizzled zzyy value
 */

/*!
 * \qmlproperty vec4 vec4::zzyz
 *
 * This property holds the swizzled zzyz value
 */

/*!
 * \qmlproperty vec4 vec4::zzyw
 *
 * This property holds the swizzled zzyw value
 */

/*!
 * \qmlproperty vec4 vec4::zzzx
 *
 * This property holds the swizzled zzzx value
 */

/*!
 * \qmlproperty vec4 vec4::zzzy
 *
 * This property holds the swizzled zzzy value
 */

/*!
 * \qmlproperty vec4 vec4::zzzz
 *
 * This property holds the swizzled zzzz value
 */

/*!
 * \qmlproperty vec4 vec4::zzzw
 *
 * This property holds the swizzled zzzw value
 */

/*!
 * \qmlproperty vec4 vec4::zzwx
 *
 * This property holds the swizzled zzwx value
 */

/*!
 * \qmlproperty vec4 vec4::zzwy
 *
 * This property holds the swizzled zzwy value
 */

/*!
 * \qmlproperty vec4 vec4::zzwz
 *
 * This property holds the swizzled zzwz value
 */

/*!
 * \qmlproperty vec4 vec4::zzww
 *
 * This property holds the swizzled zzww value
 */

/*!
 * \qmlproperty vec4 vec4::zwxx
 *
 * This property holds the swizzled zwxx value
 */

/*!
 * \qmlproperty vec4 vec4::zwxy
 *
 * This property holds the swizzled zwxy value
 */

/*!
 * \qmlproperty vec4 vec4::zwxz
 *
 * This property holds the swizzled zwxz value
 */

/*!
 * \qmlproperty vec4 vec4::zwxw
 *
 * This property holds the swizzled zwxw value
 */

/*!
 * \qmlproperty vec4 vec4::zwyx
 *
 * This property holds the swizzled zwyx value
 */

/*!
 * \qmlproperty vec4 vec4::zwyy
 *
 * This property holds the swizzled zwyy value
 */

/*!
 * \qmlproperty vec4 vec4::zwyz
 *
 * This property holds the swizzled zwyz value
 */

/*!
 * \qmlproperty vec4 vec4::zwyw
 *
 * This property holds the swizzled zwyw value
 */

/*!
 * \qmlproperty vec4 vec4::zwzx
 *
 * This property holds the swizzled zwzx value
 */

/*!
 * \qmlproperty vec4 vec4::zwzy
 *
 * This property holds the swizzled zwzy value
 */

/*!
 * \qmlproperty vec4 vec4::zwzz
 *
 * This property holds the swizzled zwzz value
 */

/*!
 * \qmlproperty vec4 vec4::zwzw
 *
 * This property holds the swizzled zwzw value
 */

/*!
 * \qmlproperty vec4 vec4::zwwx
 *
 * This property holds the swizzled zwwx value
 */

/*!
 * \qmlproperty vec4 vec4::zwwy
 *
 * This property holds the swizzled zwwy value
 */

/*!
 * \qmlproperty vec4 vec4::zwwz
 *
 * This property holds the swizzled zwwz value
 */

/*!
 * \qmlproperty vec4 vec4::zwww
 *
 * This property holds the swizzled zwww value
 */

/*!
 * \qmlproperty vec4 vec4::wxxx
 *
 * This property holds the swizzled wxxx value
 */

/*!
 * \qmlproperty vec4 vec4::wxxy
 *
 * This property holds the swizzled wxxy value
 */

/*!
 * \qmlproperty vec4 vec4::wxxz
 *
 * This property holds the swizzled wxxz value
 */

/*!
 * \qmlproperty vec4 vec4::wxxw
 *
 * This property holds the swizzled wxxw value
 */

/*!
 * \qmlproperty vec4 vec4::wxyx
 *
 * This property holds the swizzled wxyx value
 */

/*!
 * \qmlproperty vec4 vec4::wxyy
 *
 * This property holds the swizzled wxyy value
 */

/*!
 * \qmlproperty vec4 vec4::wxyz
 *
 * This property holds the swizzled wxyz value
 */

/*!
 * \qmlproperty vec4 vec4::wxyw
 *
 * This property holds the swizzled wxyw value
 */

/*!
 * \qmlproperty vec4 vec4::wxzx
 *
 * This property holds the swizzled wxzx value
 */

/*!
 * \qmlproperty vec4 vec4::wxzy
 *
 * This property holds the swizzled wxzy value
 */

/*!
 * \qmlproperty vec4 vec4::wxzz
 *
 * This property holds the swizzled wxzz value
 */

/*!
 * \qmlproperty vec4 vec4::wxzw
 *
 * This property holds the swizzled wxzw value
 */

/*!
 * \qmlproperty vec4 vec4::wxwx
 *
 * This property holds the swizzled wxwx value
 */

/*!
 * \qmlproperty vec4 vec4::wxwy
 *
 * This property holds the swizzled wxwy value
 */

/*!
 * \qmlproperty vec4 vec4::wxwz
 *
 * This property holds the swizzled wxwz value
 */

/*!
 * \qmlproperty vec4 vec4::wxww
 *
 * This property holds the swizzled wxww value
 */

/*!
 * \qmlproperty vec4 vec4::wyxx
 *
 * This property holds the swizzled wyxx value
 */

/*!
 * \qmlproperty vec4 vec4::wyxy
 *
 * This property holds the swizzled wyxy value
 */

/*!
 * \qmlproperty vec4 vec4::wyxz
 *
 * This property holds the swizzled wyxz value
 */

/*!
 * \qmlproperty vec4 vec4::wyxw
 *
 * This property holds the swizzled wyxw value
 */

/*!
 * \qmlproperty vec4 vec4::wyyx
 *
 * This property holds the swizzled wyyx value
 */

/*!
 * \qmlproperty vec4 vec4::wyyy
 *
 * This property holds the swizzled wyyy value
 */

/*!
 * \qmlproperty vec4 vec4::wyyz
 *
 * This property holds the swizzled wyyz value
 */

/*!
 * \qmlproperty vec4 vec4::wyyw
 *
 * This property holds the swizzled wyyw value
 */

/*!
 * \qmlproperty vec4 vec4::wyzx
 *
 * This property holds the swizzled wyzx value
 */

/*!
 * \qmlproperty vec4 vec4::wyzy
 *
 * This property holds the swizzled wyzy value
 */

/*!
 * \qmlproperty vec4 vec4::wyzz
 *
 * This property holds the swizzled wyzz value
 */

/*!
 * \qmlproperty vec4 vec4::wyzw
 *
 * This property holds the swizzled wyzw value
 */

/*!
 * \qmlproperty vec4 vec4::wywx
 *
 * This property holds the swizzled wywx value
 */

/*!
 * \qmlproperty vec4 vec4::wywy
 *
 * This property holds the swizzled wywy value
 */

/*!
 * \qmlproperty vec4 vec4::wywz
 *
 * This property holds the swizzled wywz value
 */

/*!
 * \qmlproperty vec4 vec4::wyww
 *
 * This property holds the swizzled wyww value
 */

/*!
 * \qmlproperty vec4 vec4::wzxx
 *
 * This property holds the swizzled wzxx value
 */

/*!
 * \qmlproperty vec4 vec4::wzxy
 *
 * This property holds the swizzled wzxy value
 */

/*!
 * \qmlproperty vec4 vec4::wzxz
 *
 * This property holds the swizzled wzxz value
 */

/*!
 * \qmlproperty vec4 vec4::wzxw
 *
 * This property holds the swizzled wzxw value
 */

/*!
 * \qmlproperty vec4 vec4::wzyx
 *
 * This property holds the swizzled wzyx value
 */

/*!
 * \qmlproperty vec4 vec4::wzyy
 *
 * This property holds the swizzled wzyy value
 */

/*!
 * \qmlproperty vec4 vec4::wzyz
 *
 * This property holds the swizzled wzyz value
 */

/*!
 * \qmlproperty vec4 vec4::wzyw
 *
 * This property holds the swizzled wzyw value
 */

/*!
 * \qmlproperty vec4 vec4::wzzx
 *
 * This property holds the swizzled wzzx value
 */

/*!
 * \qmlproperty vec4 vec4::wzzy
 *
 * This property holds the swizzled wzzy value
 */

/*!
 * \qmlproperty vec4 vec4::wzzz
 *
 * This property holds the swizzled wzzz value
 */

/*!
 * \qmlproperty vec4 vec4::wzzw
 *
 * This property holds the swizzled wzzw value
 */

/*!
 * \qmlproperty vec4 vec4::wzwx
 *
 * This property holds the swizzled wzwx value
 */

/*!
 * \qmlproperty vec4 vec4::wzwy
 *
 * This property holds the swizzled wzwy value
 */

/*!
 * \qmlproperty vec4 vec4::wzwz
 *
 * This property holds the swizzled wzwz value
 */

/*!
 * \qmlproperty vec4 vec4::wzww
 *
 * This property holds the swizzled wzww value
 */

/*!
 * \qmlproperty vec4 vec4::wwxx
 *
 * This property holds the swizzled wwxx value
 */

/*!
 * \qmlproperty vec4 vec4::wwxy
 *
 * This property holds the swizzled wwxy value
 */

/*!
 * \qmlproperty vec4 vec4::wwxz
 *
 * This property holds the swizzled wwxz value
 */

/*!
 * \qmlproperty vec4 vec4::wwxw
 *
 * This property holds the swizzled wwxw value
 */

/*!
 * \qmlproperty vec4 vec4::wwyx
 *
 * This property holds the swizzled wwyx value
 */

/*!
 * \qmlproperty vec4 vec4::wwyy
 *
 * This property holds the swizzled wwyy value
 */

/*!
 * \qmlproperty vec4 vec4::wwyz
 *
 * This property holds the swizzled wwyz value
 */

/*!
 * \qmlproperty vec4 vec4::wwyw
 *
 * This property holds the swizzled wwyw value
 */

/*!
 * \qmlproperty vec4 vec4::wwzx
 *
 * This property holds the swizzled wwzx value
 */

/*!
 * \qmlproperty vec4 vec4::wwzy
 *
 * This property holds the swizzled wwzy value
 */

/*!
 * \qmlproperty vec4 vec4::wwzz
 *
 * This property holds the swizzled wwzz value
 */

/*!
 * \qmlproperty vec4 vec4::wwzw
 *
 * This property holds the swizzled wwzw value
 */

/*!
 * \qmlproperty vec4 vec4::wwwx
 *
 * This property holds the swizzled wwwx value
 */

/*!
 * \qmlproperty vec4 vec4::wwwy
 *
 * This property holds the swizzled wwwy value
 */

/*!
 * \qmlproperty vec4 vec4::wwwz
 *
 * This property holds the swizzled wwwz value
 */

/*!
 * \qmlproperty vec4 vec4::wwww
 *
 * This property holds the swizzled wwww value
 */


/*!
 * \qmlbasictype ivec4
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The ivec4 basic type represents a GLSL ivec4 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty int ivec4::x
 *
 * This property holds the x component of the ivec4.
 */

/*!
 * \qmlproperty int ivec4::y
 *
 * This property holds the y component of the ivec4.
 */

/*!
 * \qmlproperty int ivec4::z
 *
 * This property holds the z component of the ivec4.
 */

/*!
 * \qmlproperty int ivec4::w
 *
 * This property holds the w component of the ivec4.
 */

/*!
 * \qmlproperty ivec2 ivec4::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty ivec2 ivec4::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty ivec2 ivec4::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty ivec2 ivec4::xw
 *
 * This property holds the swizzled xw value
 */

/*!
 * \qmlproperty ivec2 ivec4::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty ivec2 ivec4::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty ivec2 ivec4::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty ivec2 ivec4::yw
 *
 * This property holds the swizzled yw value
 */

/*!
 * \qmlproperty ivec2 ivec4::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty ivec2 ivec4::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty ivec2 ivec4::zz
 *
 * This property holds the swizzled zz value
 */

/*!
 * \qmlproperty ivec2 ivec4::zw
 *
 * This property holds the swizzled zw value
 */

/*!
 * \qmlproperty ivec2 ivec4::wx
 *
 * This property holds the swizzled wx value
 */

/*!
 * \qmlproperty ivec2 ivec4::wy
 *
 * This property holds the swizzled wy value
 */

/*!
 * \qmlproperty ivec2 ivec4::wz
 *
 * This property holds the swizzled wz value
 */

/*!
 * \qmlproperty ivec2 ivec4::ww
 *
 * This property holds the swizzled ww value
 */



/*!
 * \qmlproperty ivec3 ivec4::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty ivec3 ivec4::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty ivec3 ivec4::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty ivec3 ivec4::xxw
 *
 * This property holds the swizzled xxw value
 */

/*!
 * \qmlproperty ivec3 ivec4::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty ivec3 ivec4::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty ivec3 ivec4::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty ivec3 ivec4::xyw
 *
 * This property holds the swizzled xyw value
 */

/*!
 * \qmlproperty ivec3 ivec4::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty ivec3 ivec4::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty ivec3 ivec4::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty ivec3 ivec4::xzw
 *
 * This property holds the swizzled xzw value
 */

/*!
 * \qmlproperty ivec3 ivec4::xwx
 *
 * This property holds the swizzled xwx value
 */

/*!
 * \qmlproperty ivec3 ivec4::xwy
 *
 * This property holds the swizzled xwy value
 */

/*!
 * \qmlproperty ivec3 ivec4::xwz
 *
 * This property holds the swizzled xwz value
 */

/*!
 * \qmlproperty ivec3 ivec4::xww
 *
 * This property holds the swizzled xww value
 */

/*!
 * \qmlproperty ivec3 ivec4::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty ivec3 ivec4::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty ivec3 ivec4::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty ivec3 ivec4::yxw
 *
 * This property holds the swizzled yxw value
 */

/*!
 * \qmlproperty ivec3 ivec4::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty ivec3 ivec4::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty ivec3 ivec4::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty ivec3 ivec4::yyw
 *
 * This property holds the swizzled yyw value
 */

/*!
 * \qmlproperty ivec3 ivec4::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty ivec3 ivec4::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty ivec3 ivec4::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty ivec3 ivec4::yzw
 *
 * This property holds the swizzled yzw value
 */

/*!
 * \qmlproperty ivec3 ivec4::ywx
 *
 * This property holds the swizzled ywx value
 */

/*!
 * \qmlproperty ivec3 ivec4::ywy
 *
 * This property holds the swizzled ywy value
 */

/*!
 * \qmlproperty ivec3 ivec4::ywz
 *
 * This property holds the swizzled ywz value
 */

/*!
 * \qmlproperty ivec3 ivec4::yww
 *
 * This property holds the swizzled yww value
 */

/*!
 * \qmlproperty ivec3 ivec4::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty ivec3 ivec4::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty ivec3 ivec4::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty ivec3 ivec4::zxw
 *
 * This property holds the swizzled zxw value
 */

/*!
 * \qmlproperty ivec3 ivec4::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty ivec3 ivec4::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty ivec3 ivec4::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty ivec3 ivec4::zyw
 *
 * This property holds the swizzled zyw value
 */

/*!
 * \qmlproperty ivec3 ivec4::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty ivec3 ivec4::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty ivec3 ivec4::zzz
 *
 * This property holds the swizzled zzz value
 */

/*!
 * \qmlproperty ivec3 ivec4::zzw
 *
 * This property holds the swizzled zzw value
 */

/*!
 * \qmlproperty ivec3 ivec4::zwx
 *
 * This property holds the swizzled zwx value
 */

/*!
 * \qmlproperty ivec3 ivec4::zwy
 *
 * This property holds the swizzled zwy value
 */

/*!
 * \qmlproperty ivec3 ivec4::zwz
 *
 * This property holds the swizzled zwz value
 */

/*!
 * \qmlproperty ivec3 ivec4::zww
 *
 * This property holds the swizzled zww value
 */

/*!
 * \qmlproperty ivec3 ivec4::wxx
 *
 * This property holds the swizzled wxx value
 */

/*!
 * \qmlproperty ivec3 ivec4::wxy
 *
 * This property holds the swizzled wxy value
 */

/*!
 * \qmlproperty ivec3 ivec4::wxz
 *
 * This property holds the swizzled wxz value
 */

/*!
 * \qmlproperty ivec3 ivec4::wxw
 *
 * This property holds the swizzled wxw value
 */

/*!
 * \qmlproperty ivec3 ivec4::wyx
 *
 * This property holds the swizzled wyx value
 */

/*!
 * \qmlproperty ivec3 ivec4::wyy
 *
 * This property holds the swizzled wyy value
 */

/*!
 * \qmlproperty ivec3 ivec4::wyz
 *
 * This property holds the swizzled wyz value
 */

/*!
 * \qmlproperty ivec3 ivec4::wyw
 *
 * This property holds the swizzled wyw value
 */

/*!
 * \qmlproperty ivec3 ivec4::wzx
 *
 * This property holds the swizzled wzx value
 */

/*!
 * \qmlproperty ivec3 ivec4::wzy
 *
 * This property holds the swizzled wzy value
 */

/*!
 * \qmlproperty ivec3 ivec4::wzz
 *
 * This property holds the swizzled wzz value
 */

/*!
 * \qmlproperty ivec3 ivec4::wzw
 *
 * This property holds the swizzled wzw value
 */

/*!
 * \qmlproperty ivec3 ivec4::wwx
 *
 * This property holds the swizzled wwx value
 */

/*!
 * \qmlproperty ivec3 ivec4::wwy
 *
 * This property holds the swizzled wwy value
 */

/*!
 * \qmlproperty ivec3 ivec4::wwz
 *
 * This property holds the swizzled wwz value
 */

/*!
 * \qmlproperty ivec3 ivec4::www
 *
 * This property holds the swizzled www value
 */


/*!
 * \qmlproperty ivec4 ivec4::xxxx
 *
 * This property holds the swizzled xxxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxxy
 *
 * This property holds the swizzled xxxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxxz
 *
 * This property holds the swizzled xxxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxxw
 *
 * This property holds the swizzled xxxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxyx
 *
 * This property holds the swizzled xxyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxyy
 *
 * This property holds the swizzled xxyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxyz
 *
 * This property holds the swizzled xxyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxyw
 *
 * This property holds the swizzled xxyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxzx
 *
 * This property holds the swizzled xxzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxzy
 *
 * This property holds the swizzled xxzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxzz
 *
 * This property holds the swizzled xxzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxzw
 *
 * This property holds the swizzled xxzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxwx
 *
 * This property holds the swizzled xxwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxwy
 *
 * This property holds the swizzled xxwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxwz
 *
 * This property holds the swizzled xxwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xxww
 *
 * This property holds the swizzled xxww value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyxx
 *
 * This property holds the swizzled xyxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyxy
 *
 * This property holds the swizzled xyxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyxz
 *
 * This property holds the swizzled xyxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyxw
 *
 * This property holds the swizzled xyxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyyx
 *
 * This property holds the swizzled xyyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyyy
 *
 * This property holds the swizzled xyyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyyz
 *
 * This property holds the swizzled xyyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyyw
 *
 * This property holds the swizzled xyyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyzx
 *
 * This property holds the swizzled xyzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyzy
 *
 * This property holds the swizzled xyzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyzz
 *
 * This property holds the swizzled xyzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyzw
 *
 * This property holds the swizzled xyzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xywx
 *
 * This property holds the swizzled xywx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xywy
 *
 * This property holds the swizzled xywy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xywz
 *
 * This property holds the swizzled xywz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xyww
 *
 * This property holds the swizzled xyww value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzxx
 *
 * This property holds the swizzled xzxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzxy
 *
 * This property holds the swizzled xzxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzxz
 *
 * This property holds the swizzled xzxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzxw
 *
 * This property holds the swizzled xzxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzyx
 *
 * This property holds the swizzled xzyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzyy
 *
 * This property holds the swizzled xzyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzyz
 *
 * This property holds the swizzled xzyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzyw
 *
 * This property holds the swizzled xzyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzzx
 *
 * This property holds the swizzled xzzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzzy
 *
 * This property holds the swizzled xzzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzzz
 *
 * This property holds the swizzled xzzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzzw
 *
 * This property holds the swizzled xzzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzwx
 *
 * This property holds the swizzled xzwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzwy
 *
 * This property holds the swizzled xzwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzwz
 *
 * This property holds the swizzled xzwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xzww
 *
 * This property holds the swizzled xzww value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwxx
 *
 * This property holds the swizzled xwxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwxy
 *
 * This property holds the swizzled xwxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwxz
 *
 * This property holds the swizzled xwxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwxw
 *
 * This property holds the swizzled xwxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwyx
 *
 * This property holds the swizzled xwyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwyy
 *
 * This property holds the swizzled xwyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwyz
 *
 * This property holds the swizzled xwyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwyw
 *
 * This property holds the swizzled xwyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwzx
 *
 * This property holds the swizzled xwzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwzy
 *
 * This property holds the swizzled xwzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwzz
 *
 * This property holds the swizzled xwzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwzw
 *
 * This property holds the swizzled xwzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwwx
 *
 * This property holds the swizzled xwwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwwy
 *
 * This property holds the swizzled xwwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwwz
 *
 * This property holds the swizzled xwwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::xwww
 *
 * This property holds the swizzled xwww value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxxx
 *
 * This property holds the swizzled yxxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxxy
 *
 * This property holds the swizzled yxxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxxz
 *
 * This property holds the swizzled yxxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxxw
 *
 * This property holds the swizzled yxxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxyx
 *
 * This property holds the swizzled yxyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxyy
 *
 * This property holds the swizzled yxyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxyz
 *
 * This property holds the swizzled yxyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxyw
 *
 * This property holds the swizzled yxyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxzx
 *
 * This property holds the swizzled yxzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxzy
 *
 * This property holds the swizzled yxzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxzz
 *
 * This property holds the swizzled yxzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxzw
 *
 * This property holds the swizzled yxzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxwx
 *
 * This property holds the swizzled yxwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxwy
 *
 * This property holds the swizzled yxwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxwz
 *
 * This property holds the swizzled yxwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yxww
 *
 * This property holds the swizzled yxww value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyxx
 *
 * This property holds the swizzled yyxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyxy
 *
 * This property holds the swizzled yyxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyxz
 *
 * This property holds the swizzled yyxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyxw
 *
 * This property holds the swizzled yyxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyyx
 *
 * This property holds the swizzled yyyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyyy
 *
 * This property holds the swizzled yyyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyyz
 *
 * This property holds the swizzled yyyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyyw
 *
 * This property holds the swizzled yyyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyzx
 *
 * This property holds the swizzled yyzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyzy
 *
 * This property holds the swizzled yyzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyzz
 *
 * This property holds the swizzled yyzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyzw
 *
 * This property holds the swizzled yyzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yywx
 *
 * This property holds the swizzled yywx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yywy
 *
 * This property holds the swizzled yywy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yywz
 *
 * This property holds the swizzled yywz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yyww
 *
 * This property holds the swizzled yyww value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzxx
 *
 * This property holds the swizzled yzxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzxy
 *
 * This property holds the swizzled yzxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzxz
 *
 * This property holds the swizzled yzxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzxw
 *
 * This property holds the swizzled yzxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzyx
 *
 * This property holds the swizzled yzyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzyy
 *
 * This property holds the swizzled yzyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzyz
 *
 * This property holds the swizzled yzyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzyw
 *
 * This property holds the swizzled yzyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzzx
 *
 * This property holds the swizzled yzzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzzy
 *
 * This property holds the swizzled yzzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzzz
 *
 * This property holds the swizzled yzzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzzw
 *
 * This property holds the swizzled yzzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzwx
 *
 * This property holds the swizzled yzwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzwy
 *
 * This property holds the swizzled yzwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzwz
 *
 * This property holds the swizzled yzwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::yzww
 *
 * This property holds the swizzled yzww value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywxx
 *
 * This property holds the swizzled ywxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywxy
 *
 * This property holds the swizzled ywxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywxz
 *
 * This property holds the swizzled ywxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywxw
 *
 * This property holds the swizzled ywxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywyx
 *
 * This property holds the swizzled ywyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywyy
 *
 * This property holds the swizzled ywyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywyz
 *
 * This property holds the swizzled ywyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywyw
 *
 * This property holds the swizzled ywyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywzx
 *
 * This property holds the swizzled ywzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywzy
 *
 * This property holds the swizzled ywzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywzz
 *
 * This property holds the swizzled ywzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywzw
 *
 * This property holds the swizzled ywzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywwx
 *
 * This property holds the swizzled ywwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywwy
 *
 * This property holds the swizzled ywwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywwz
 *
 * This property holds the swizzled ywwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::ywww
 *
 * This property holds the swizzled ywww value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxxx
 *
 * This property holds the swizzled zxxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxxy
 *
 * This property holds the swizzled zxxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxxz
 *
 * This property holds the swizzled zxxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxxw
 *
 * This property holds the swizzled zxxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxyx
 *
 * This property holds the swizzled zxyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxyy
 *
 * This property holds the swizzled zxyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxyz
 *
 * This property holds the swizzled zxyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxyw
 *
 * This property holds the swizzled zxyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxzx
 *
 * This property holds the swizzled zxzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxzy
 *
 * This property holds the swizzled zxzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxzz
 *
 * This property holds the swizzled zxzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxzw
 *
 * This property holds the swizzled zxzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxwx
 *
 * This property holds the swizzled zxwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxwy
 *
 * This property holds the swizzled zxwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxwz
 *
 * This property holds the swizzled zxwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zxww
 *
 * This property holds the swizzled zxww value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyxx
 *
 * This property holds the swizzled zyxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyxy
 *
 * This property holds the swizzled zyxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyxz
 *
 * This property holds the swizzled zyxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyxw
 *
 * This property holds the swizzled zyxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyyx
 *
 * This property holds the swizzled zyyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyyy
 *
 * This property holds the swizzled zyyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyyz
 *
 * This property holds the swizzled zyyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyyw
 *
 * This property holds the swizzled zyyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyzx
 *
 * This property holds the swizzled zyzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyzy
 *
 * This property holds the swizzled zyzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyzz
 *
 * This property holds the swizzled zyzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyzw
 *
 * This property holds the swizzled zyzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zywx
 *
 * This property holds the swizzled zywx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zywy
 *
 * This property holds the swizzled zywy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zywz
 *
 * This property holds the swizzled zywz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zyww
 *
 * This property holds the swizzled zyww value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzxx
 *
 * This property holds the swizzled zzxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzxy
 *
 * This property holds the swizzled zzxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzxz
 *
 * This property holds the swizzled zzxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzxw
 *
 * This property holds the swizzled zzxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzyx
 *
 * This property holds the swizzled zzyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzyy
 *
 * This property holds the swizzled zzyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzyz
 *
 * This property holds the swizzled zzyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzyw
 *
 * This property holds the swizzled zzyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzzx
 *
 * This property holds the swizzled zzzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzzy
 *
 * This property holds the swizzled zzzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzzz
 *
 * This property holds the swizzled zzzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzzw
 *
 * This property holds the swizzled zzzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzwx
 *
 * This property holds the swizzled zzwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzwy
 *
 * This property holds the swizzled zzwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzwz
 *
 * This property holds the swizzled zzwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zzww
 *
 * This property holds the swizzled zzww value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwxx
 *
 * This property holds the swizzled zwxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwxy
 *
 * This property holds the swizzled zwxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwxz
 *
 * This property holds the swizzled zwxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwxw
 *
 * This property holds the swizzled zwxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwyx
 *
 * This property holds the swizzled zwyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwyy
 *
 * This property holds the swizzled zwyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwyz
 *
 * This property holds the swizzled zwyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwyw
 *
 * This property holds the swizzled zwyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwzx
 *
 * This property holds the swizzled zwzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwzy
 *
 * This property holds the swizzled zwzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwzz
 *
 * This property holds the swizzled zwzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwzw
 *
 * This property holds the swizzled zwzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwwx
 *
 * This property holds the swizzled zwwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwwy
 *
 * This property holds the swizzled zwwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwwz
 *
 * This property holds the swizzled zwwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::zwww
 *
 * This property holds the swizzled zwww value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxxx
 *
 * This property holds the swizzled wxxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxxy
 *
 * This property holds the swizzled wxxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxxz
 *
 * This property holds the swizzled wxxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxxw
 *
 * This property holds the swizzled wxxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxyx
 *
 * This property holds the swizzled wxyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxyy
 *
 * This property holds the swizzled wxyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxyz
 *
 * This property holds the swizzled wxyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxyw
 *
 * This property holds the swizzled wxyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxzx
 *
 * This property holds the swizzled wxzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxzy
 *
 * This property holds the swizzled wxzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxzz
 *
 * This property holds the swizzled wxzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxzw
 *
 * This property holds the swizzled wxzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxwx
 *
 * This property holds the swizzled wxwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxwy
 *
 * This property holds the swizzled wxwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxwz
 *
 * This property holds the swizzled wxwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wxww
 *
 * This property holds the swizzled wxww value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyxx
 *
 * This property holds the swizzled wyxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyxy
 *
 * This property holds the swizzled wyxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyxz
 *
 * This property holds the swizzled wyxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyxw
 *
 * This property holds the swizzled wyxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyyx
 *
 * This property holds the swizzled wyyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyyy
 *
 * This property holds the swizzled wyyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyyz
 *
 * This property holds the swizzled wyyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyyw
 *
 * This property holds the swizzled wyyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyzx
 *
 * This property holds the swizzled wyzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyzy
 *
 * This property holds the swizzled wyzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyzz
 *
 * This property holds the swizzled wyzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyzw
 *
 * This property holds the swizzled wyzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wywx
 *
 * This property holds the swizzled wywx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wywy
 *
 * This property holds the swizzled wywy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wywz
 *
 * This property holds the swizzled wywz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wyww
 *
 * This property holds the swizzled wyww value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzxx
 *
 * This property holds the swizzled wzxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzxy
 *
 * This property holds the swizzled wzxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzxz
 *
 * This property holds the swizzled wzxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzxw
 *
 * This property holds the swizzled wzxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzyx
 *
 * This property holds the swizzled wzyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzyy
 *
 * This property holds the swizzled wzyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzyz
 *
 * This property holds the swizzled wzyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzyw
 *
 * This property holds the swizzled wzyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzzx
 *
 * This property holds the swizzled wzzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzzy
 *
 * This property holds the swizzled wzzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzzz
 *
 * This property holds the swizzled wzzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzzw
 *
 * This property holds the swizzled wzzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzwx
 *
 * This property holds the swizzled wzwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzwy
 *
 * This property holds the swizzled wzwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzwz
 *
 * This property holds the swizzled wzwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wzww
 *
 * This property holds the swizzled wzww value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwxx
 *
 * This property holds the swizzled wwxx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwxy
 *
 * This property holds the swizzled wwxy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwxz
 *
 * This property holds the swizzled wwxz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwxw
 *
 * This property holds the swizzled wwxw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwyx
 *
 * This property holds the swizzled wwyx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwyy
 *
 * This property holds the swizzled wwyy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwyz
 *
 * This property holds the swizzled wwyz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwyw
 *
 * This property holds the swizzled wwyw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwzx
 *
 * This property holds the swizzled wwzx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwzy
 *
 * This property holds the swizzled wwzy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwzz
 *
 * This property holds the swizzled wwzz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwzw
 *
 * This property holds the swizzled wwzw value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwwx
 *
 * This property holds the swizzled wwwx value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwwy
 *
 * This property holds the swizzled wwwy value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwwz
 *
 * This property holds the swizzled wwwz value
 */

/*!
 * \qmlproperty ivec4 ivec4::wwww
 *
 * This property holds the swizzled wwww value
 */

/*!
 * \qmlbasictype uvec4
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The uvec4 basic type represents a GLSL uvec4 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty uint uvec4::x
 *
 * This property holds the x component of the uvec4.
 */

/*!
 * \qmlproperty uint uvec4::y
 *
 * This property holds the y component of the uvec4.
 */

/*!
 * \qmlproperty uint uvec4::z
 *
 * This property holds the z component of the uvec4.
 */

/*!
 * \qmlproperty uint uvec4::w
 *
 * This property holds the w component of the uvec4.
 */

/*!
 * \qmlproperty uvec2 uvec4::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty uvec2 uvec4::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty uvec2 uvec4::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty uvec2 uvec4::xw
 *
 * This property holds the swizzled xw value
 */

/*!
 * \qmlproperty uvec2 uvec4::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty uvec2 uvec4::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty uvec2 uvec4::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty uvec2 uvec4::yw
 *
 * This property holds the swizzled yw value
 */

/*!
 * \qmlproperty uvec2 uvec4::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty uvec2 uvec4::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty uvec2 uvec4::zz
 *
 * This property holds the swizzled zz value
 */

/*!
 * \qmlproperty uvec2 uvec4::zw
 *
 * This property holds the swizzled zw value
 */

/*!
 * \qmlproperty uvec2 uvec4::wx
 *
 * This property holds the swizzled wx value
 */

/*!
 * \qmlproperty uvec2 uvec4::wy
 *
 * This property holds the swizzled wy value
 */

/*!
 * \qmlproperty uvec2 uvec4::wz
 *
 * This property holds the swizzled wz value
 */

/*!
 * \qmlproperty uvec2 uvec4::ww
 *
 * This property holds the swizzled ww value
 */



/*!
 * \qmlproperty uvec3 uvec4::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty uvec3 uvec4::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty uvec3 uvec4::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty uvec3 uvec4::xxw
 *
 * This property holds the swizzled xxw value
 */

/*!
 * \qmlproperty uvec3 uvec4::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty uvec3 uvec4::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty uvec3 uvec4::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty uvec3 uvec4::xyw
 *
 * This property holds the swizzled xyw value
 */

/*!
 * \qmlproperty uvec3 uvec4::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty uvec3 uvec4::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty uvec3 uvec4::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty uvec3 uvec4::xzw
 *
 * This property holds the swizzled xzw value
 */

/*!
 * \qmlproperty uvec3 uvec4::xwx
 *
 * This property holds the swizzled xwx value
 */

/*!
 * \qmlproperty uvec3 uvec4::xwy
 *
 * This property holds the swizzled xwy value
 */

/*!
 * \qmlproperty uvec3 uvec4::xwz
 *
 * This property holds the swizzled xwz value
 */

/*!
 * \qmlproperty uvec3 uvec4::xww
 *
 * This property holds the swizzled xww value
 */

/*!
 * \qmlproperty uvec3 uvec4::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty uvec3 uvec4::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty uvec3 uvec4::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty uvec3 uvec4::yxw
 *
 * This property holds the swizzled yxw value
 */

/*!
 * \qmlproperty uvec3 uvec4::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty uvec3 uvec4::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty uvec3 uvec4::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty uvec3 uvec4::yyw
 *
 * This property holds the swizzled yyw value
 */

/*!
 * \qmlproperty uvec3 uvec4::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty uvec3 uvec4::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty uvec3 uvec4::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty uvec3 uvec4::yzw
 *
 * This property holds the swizzled yzw value
 */

/*!
 * \qmlproperty uvec3 uvec4::ywx
 *
 * This property holds the swizzled ywx value
 */

/*!
 * \qmlproperty uvec3 uvec4::ywy
 *
 * This property holds the swizzled ywy value
 */

/*!
 * \qmlproperty uvec3 uvec4::ywz
 *
 * This property holds the swizzled ywz value
 */

/*!
 * \qmlproperty uvec3 uvec4::yww
 *
 * This property holds the swizzled yww value
 */

/*!
 * \qmlproperty uvec3 uvec4::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty uvec3 uvec4::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty uvec3 uvec4::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty uvec3 uvec4::zxw
 *
 * This property holds the swizzled zxw value
 */

/*!
 * \qmlproperty uvec3 uvec4::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty uvec3 uvec4::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty uvec3 uvec4::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty uvec3 uvec4::zyw
 *
 * This property holds the swizzled zyw value
 */

/*!
 * \qmlproperty uvec3 uvec4::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty uvec3 uvec4::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty uvec3 uvec4::zzz
 *
 * This property holds the swizzled zzz value
 */

/*!
 * \qmlproperty uvec3 uvec4::zzw
 *
 * This property holds the swizzled zzw value
 */

/*!
 * \qmlproperty uvec3 uvec4::zwx
 *
 * This property holds the swizzled zwx value
 */

/*!
 * \qmlproperty uvec3 uvec4::zwy
 *
 * This property holds the swizzled zwy value
 */

/*!
 * \qmlproperty uvec3 uvec4::zwz
 *
 * This property holds the swizzled zwz value
 */

/*!
 * \qmlproperty uvec3 uvec4::zww
 *
 * This property holds the swizzled zww value
 */

/*!
 * \qmlproperty uvec3 uvec4::wxx
 *
 * This property holds the swizzled wxx value
 */

/*!
 * \qmlproperty uvec3 uvec4::wxy
 *
 * This property holds the swizzled wxy value
 */

/*!
 * \qmlproperty uvec3 uvec4::wxz
 *
 * This property holds the swizzled wxz value
 */

/*!
 * \qmlproperty uvec3 uvec4::wxw
 *
 * This property holds the swizzled wxw value
 */

/*!
 * \qmlproperty uvec3 uvec4::wyx
 *
 * This property holds the swizzled wyx value
 */

/*!
 * \qmlproperty uvec3 uvec4::wyy
 *
 * This property holds the swizzled wyy value
 */

/*!
 * \qmlproperty uvec3 uvec4::wyz
 *
 * This property holds the swizzled wyz value
 */

/*!
 * \qmlproperty uvec3 uvec4::wyw
 *
 * This property holds the swizzled wyw value
 */

/*!
 * \qmlproperty uvec3 uvec4::wzx
 *
 * This property holds the swizzled wzx value
 */

/*!
 * \qmlproperty uvec3 uvec4::wzy
 *
 * This property holds the swizzled wzy value
 */

/*!
 * \qmlproperty uvec3 uvec4::wzz
 *
 * This property holds the swizzled wzz value
 */

/*!
 * \qmlproperty uvec3 uvec4::wzw
 *
 * This property holds the swizzled wzw value
 */

/*!
 * \qmlproperty uvec3 uvec4::wwx
 *
 * This property holds the swizzled wwx value
 */

/*!
 * \qmlproperty uvec3 uvec4::wwy
 *
 * This property holds the swizzled wwy value
 */

/*!
 * \qmlproperty uvec3 uvec4::wwz
 *
 * This property holds the swizzled wwz value
 */

/*!
 * \qmlproperty uvec3 uvec4::www
 *
 * This property holds the swizzled www value
 */



/*!
 * \qmlproperty uvec4 uvec4::xxxx
 *
 * This property holds the swizzled xxxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxxy
 *
 * This property holds the swizzled xxxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxxz
 *
 * This property holds the swizzled xxxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxxw
 *
 * This property holds the swizzled xxxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxyx
 *
 * This property holds the swizzled xxyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxyy
 *
 * This property holds the swizzled xxyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxyz
 *
 * This property holds the swizzled xxyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxyw
 *
 * This property holds the swizzled xxyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxzx
 *
 * This property holds the swizzled xxzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxzy
 *
 * This property holds the swizzled xxzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxzz
 *
 * This property holds the swizzled xxzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxzw
 *
 * This property holds the swizzled xxzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxwx
 *
 * This property holds the swizzled xxwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxwy
 *
 * This property holds the swizzled xxwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxwz
 *
 * This property holds the swizzled xxwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xxww
 *
 * This property holds the swizzled xxww value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyxx
 *
 * This property holds the swizzled xyxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyxy
 *
 * This property holds the swizzled xyxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyxz
 *
 * This property holds the swizzled xyxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyxw
 *
 * This property holds the swizzled xyxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyyx
 *
 * This property holds the swizzled xyyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyyy
 *
 * This property holds the swizzled xyyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyyz
 *
 * This property holds the swizzled xyyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyyw
 *
 * This property holds the swizzled xyyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyzx
 *
 * This property holds the swizzled xyzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyzy
 *
 * This property holds the swizzled xyzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyzz
 *
 * This property holds the swizzled xyzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyzw
 *
 * This property holds the swizzled xyzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xywx
 *
 * This property holds the swizzled xywx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xywy
 *
 * This property holds the swizzled xywy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xywz
 *
 * This property holds the swizzled xywz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xyww
 *
 * This property holds the swizzled xyww value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzxx
 *
 * This property holds the swizzled xzxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzxy
 *
 * This property holds the swizzled xzxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzxz
 *
 * This property holds the swizzled xzxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzxw
 *
 * This property holds the swizzled xzxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzyx
 *
 * This property holds the swizzled xzyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzyy
 *
 * This property holds the swizzled xzyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzyz
 *
 * This property holds the swizzled xzyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzyw
 *
 * This property holds the swizzled xzyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzzx
 *
 * This property holds the swizzled xzzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzzy
 *
 * This property holds the swizzled xzzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzzz
 *
 * This property holds the swizzled xzzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzzw
 *
 * This property holds the swizzled xzzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzwx
 *
 * This property holds the swizzled xzwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzwy
 *
 * This property holds the swizzled xzwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzwz
 *
 * This property holds the swizzled xzwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xzww
 *
 * This property holds the swizzled xzww value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwxx
 *
 * This property holds the swizzled xwxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwxy
 *
 * This property holds the swizzled xwxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwxz
 *
 * This property holds the swizzled xwxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwxw
 *
 * This property holds the swizzled xwxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwyx
 *
 * This property holds the swizzled xwyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwyy
 *
 * This property holds the swizzled xwyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwyz
 *
 * This property holds the swizzled xwyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwyw
 *
 * This property holds the swizzled xwyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwzx
 *
 * This property holds the swizzled xwzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwzy
 *
 * This property holds the swizzled xwzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwzz
 *
 * This property holds the swizzled xwzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwzw
 *
 * This property holds the swizzled xwzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwwx
 *
 * This property holds the swizzled xwwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwwy
 *
 * This property holds the swizzled xwwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwwz
 *
 * This property holds the swizzled xwwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::xwww
 *
 * This property holds the swizzled xwww value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxxx
 *
 * This property holds the swizzled yxxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxxy
 *
 * This property holds the swizzled yxxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxxz
 *
 * This property holds the swizzled yxxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxxw
 *
 * This property holds the swizzled yxxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxyx
 *
 * This property holds the swizzled yxyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxyy
 *
 * This property holds the swizzled yxyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxyz
 *
 * This property holds the swizzled yxyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxyw
 *
 * This property holds the swizzled yxyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxzx
 *
 * This property holds the swizzled yxzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxzy
 *
 * This property holds the swizzled yxzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxzz
 *
 * This property holds the swizzled yxzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxzw
 *
 * This property holds the swizzled yxzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxwx
 *
 * This property holds the swizzled yxwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxwy
 *
 * This property holds the swizzled yxwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxwz
 *
 * This property holds the swizzled yxwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yxww
 *
 * This property holds the swizzled yxww value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyxx
 *
 * This property holds the swizzled yyxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyxy
 *
 * This property holds the swizzled yyxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyxz
 *
 * This property holds the swizzled yyxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyxw
 *
 * This property holds the swizzled yyxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyyx
 *
 * This property holds the swizzled yyyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyyy
 *
 * This property holds the swizzled yyyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyyz
 *
 * This property holds the swizzled yyyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyyw
 *
 * This property holds the swizzled yyyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyzx
 *
 * This property holds the swizzled yyzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyzy
 *
 * This property holds the swizzled yyzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyzz
 *
 * This property holds the swizzled yyzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyzw
 *
 * This property holds the swizzled yyzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yywx
 *
 * This property holds the swizzled yywx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yywy
 *
 * This property holds the swizzled yywy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yywz
 *
 * This property holds the swizzled yywz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yyww
 *
 * This property holds the swizzled yyww value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzxx
 *
 * This property holds the swizzled yzxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzxy
 *
 * This property holds the swizzled yzxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzxz
 *
 * This property holds the swizzled yzxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzxw
 *
 * This property holds the swizzled yzxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzyx
 *
 * This property holds the swizzled yzyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzyy
 *
 * This property holds the swizzled yzyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzyz
 *
 * This property holds the swizzled yzyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzyw
 *
 * This property holds the swizzled yzyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzzx
 *
 * This property holds the swizzled yzzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzzy
 *
 * This property holds the swizzled yzzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzzz
 *
 * This property holds the swizzled yzzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzzw
 *
 * This property holds the swizzled yzzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzwx
 *
 * This property holds the swizzled yzwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzwy
 *
 * This property holds the swizzled yzwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzwz
 *
 * This property holds the swizzled yzwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::yzww
 *
 * This property holds the swizzled yzww value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywxx
 *
 * This property holds the swizzled ywxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywxy
 *
 * This property holds the swizzled ywxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywxz
 *
 * This property holds the swizzled ywxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywxw
 *
 * This property holds the swizzled ywxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywyx
 *
 * This property holds the swizzled ywyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywyy
 *
 * This property holds the swizzled ywyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywyz
 *
 * This property holds the swizzled ywyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywyw
 *
 * This property holds the swizzled ywyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywzx
 *
 * This property holds the swizzled ywzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywzy
 *
 * This property holds the swizzled ywzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywzz
 *
 * This property holds the swizzled ywzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywzw
 *
 * This property holds the swizzled ywzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywwx
 *
 * This property holds the swizzled ywwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywwy
 *
 * This property holds the swizzled ywwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywwz
 *
 * This property holds the swizzled ywwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::ywww
 *
 * This property holds the swizzled ywww value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxxx
 *
 * This property holds the swizzled zxxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxxy
 *
 * This property holds the swizzled zxxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxxz
 *
 * This property holds the swizzled zxxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxxw
 *
 * This property holds the swizzled zxxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxyx
 *
 * This property holds the swizzled zxyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxyy
 *
 * This property holds the swizzled zxyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxyz
 *
 * This property holds the swizzled zxyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxyw
 *
 * This property holds the swizzled zxyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxzx
 *
 * This property holds the swizzled zxzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxzy
 *
 * This property holds the swizzled zxzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxzz
 *
 * This property holds the swizzled zxzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxzw
 *
 * This property holds the swizzled zxzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxwx
 *
 * This property holds the swizzled zxwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxwy
 *
 * This property holds the swizzled zxwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxwz
 *
 * This property holds the swizzled zxwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zxww
 *
 * This property holds the swizzled zxww value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyxx
 *
 * This property holds the swizzled zyxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyxy
 *
 * This property holds the swizzled zyxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyxz
 *
 * This property holds the swizzled zyxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyxw
 *
 * This property holds the swizzled zyxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyyx
 *
 * This property holds the swizzled zyyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyyy
 *
 * This property holds the swizzled zyyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyyz
 *
 * This property holds the swizzled zyyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyyw
 *
 * This property holds the swizzled zyyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyzx
 *
 * This property holds the swizzled zyzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyzy
 *
 * This property holds the swizzled zyzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyzz
 *
 * This property holds the swizzled zyzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyzw
 *
 * This property holds the swizzled zyzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zywx
 *
 * This property holds the swizzled zywx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zywy
 *
 * This property holds the swizzled zywy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zywz
 *
 * This property holds the swizzled zywz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zyww
 *
 * This property holds the swizzled zyww value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzxx
 *
 * This property holds the swizzled zzxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzxy
 *
 * This property holds the swizzled zzxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzxz
 *
 * This property holds the swizzled zzxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzxw
 *
 * This property holds the swizzled zzxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzyx
 *
 * This property holds the swizzled zzyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzyy
 *
 * This property holds the swizzled zzyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzyz
 *
 * This property holds the swizzled zzyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzyw
 *
 * This property holds the swizzled zzyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzzx
 *
 * This property holds the swizzled zzzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzzy
 *
 * This property holds the swizzled zzzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzzz
 *
 * This property holds the swizzled zzzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzzw
 *
 * This property holds the swizzled zzzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzwx
 *
 * This property holds the swizzled zzwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzwy
 *
 * This property holds the swizzled zzwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzwz
 *
 * This property holds the swizzled zzwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zzww
 *
 * This property holds the swizzled zzww value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwxx
 *
 * This property holds the swizzled zwxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwxy
 *
 * This property holds the swizzled zwxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwxz
 *
 * This property holds the swizzled zwxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwxw
 *
 * This property holds the swizzled zwxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwyx
 *
 * This property holds the swizzled zwyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwyy
 *
 * This property holds the swizzled zwyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwyz
 *
 * This property holds the swizzled zwyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwyw
 *
 * This property holds the swizzled zwyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwzx
 *
 * This property holds the swizzled zwzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwzy
 *
 * This property holds the swizzled zwzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwzz
 *
 * This property holds the swizzled zwzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwzw
 *
 * This property holds the swizzled zwzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwwx
 *
 * This property holds the swizzled zwwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwwy
 *
 * This property holds the swizzled zwwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwwz
 *
 * This property holds the swizzled zwwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::zwww
 *
 * This property holds the swizzled zwww value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxxx
 *
 * This property holds the swizzled wxxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxxy
 *
 * This property holds the swizzled wxxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxxz
 *
 * This property holds the swizzled wxxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxxw
 *
 * This property holds the swizzled wxxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxyx
 *
 * This property holds the swizzled wxyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxyy
 *
 * This property holds the swizzled wxyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxyz
 *
 * This property holds the swizzled wxyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxyw
 *
 * This property holds the swizzled wxyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxzx
 *
 * This property holds the swizzled wxzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxzy
 *
 * This property holds the swizzled wxzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxzz
 *
 * This property holds the swizzled wxzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxzw
 *
 * This property holds the swizzled wxzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxwx
 *
 * This property holds the swizzled wxwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxwy
 *
 * This property holds the swizzled wxwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxwz
 *
 * This property holds the swizzled wxwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wxww
 *
 * This property holds the swizzled wxww value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyxx
 *
 * This property holds the swizzled wyxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyxy
 *
 * This property holds the swizzled wyxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyxz
 *
 * This property holds the swizzled wyxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyxw
 *
 * This property holds the swizzled wyxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyyx
 *
 * This property holds the swizzled wyyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyyy
 *
 * This property holds the swizzled wyyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyyz
 *
 * This property holds the swizzled wyyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyyw
 *
 * This property holds the swizzled wyyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyzx
 *
 * This property holds the swizzled wyzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyzy
 *
 * This property holds the swizzled wyzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyzz
 *
 * This property holds the swizzled wyzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyzw
 *
 * This property holds the swizzled wyzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wywx
 *
 * This property holds the swizzled wywx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wywy
 *
 * This property holds the swizzled wywy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wywz
 *
 * This property holds the swizzled wywz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wyww
 *
 * This property holds the swizzled wyww value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzxx
 *
 * This property holds the swizzled wzxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzxy
 *
 * This property holds the swizzled wzxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzxz
 *
 * This property holds the swizzled wzxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzxw
 *
 * This property holds the swizzled wzxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzyx
 *
 * This property holds the swizzled wzyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzyy
 *
 * This property holds the swizzled wzyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzyz
 *
 * This property holds the swizzled wzyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzyw
 *
 * This property holds the swizzled wzyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzzx
 *
 * This property holds the swizzled wzzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzzy
 *
 * This property holds the swizzled wzzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzzz
 *
 * This property holds the swizzled wzzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzzw
 *
 * This property holds the swizzled wzzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzwx
 *
 * This property holds the swizzled wzwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzwy
 *
 * This property holds the swizzled wzwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzwz
 *
 * This property holds the swizzled wzwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wzww
 *
 * This property holds the swizzled wzww value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwxx
 *
 * This property holds the swizzled wwxx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwxy
 *
 * This property holds the swizzled wwxy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwxz
 *
 * This property holds the swizzled wwxz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwxw
 *
 * This property holds the swizzled wwxw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwyx
 *
 * This property holds the swizzled wwyx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwyy
 *
 * This property holds the swizzled wwyy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwyz
 *
 * This property holds the swizzled wwyz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwyw
 *
 * This property holds the swizzled wwyw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwzx
 *
 * This property holds the swizzled wwzx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwzy
 *
 * This property holds the swizzled wwzy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwzz
 *
 * This property holds the swizzled wwzz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwzw
 *
 * This property holds the swizzled wwzw value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwwx
 *
 * This property holds the swizzled wwwx value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwwy
 *
 * This property holds the swizzled wwwy value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwwz
 *
 * This property holds the swizzled wwwz value
 */

/*!
 * \qmlproperty uvec4 uvec4::wwww
 *
 * This property holds the swizzled wwww value
 */


/*!
 * \qmlbasictype dvec4
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The dvec4 basic type represents a GLSL dvec4 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty double dvec4::x
 *
 * This property holds the x component of the dvec4.
 */

/*!
 * \qmlproperty double dvec4::y
 *
 * This property holds the y component of the dvec4.
 */

/*!
 * \qmlproperty double dvec4::z
 *
 * This property holds the z component of the dvec4.
 */

/*!
 * \qmlproperty double dvec4::w
 *
 * This property holds the w component of the dvec4.
 */

/*!
 * \qmlproperty dvec2 dvec4::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty dvec2 dvec4::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty dvec2 dvec4::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty dvec2 dvec4::xw
 *
 * This property holds the swizzled xw value
 */

/*!
 * \qmlproperty dvec2 dvec4::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty dvec2 dvec4::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty dvec2 dvec4::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty dvec2 dvec4::yw
 *
 * This property holds the swizzled yw value
 */

/*!
 * \qmlproperty dvec2 dvec4::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty dvec2 dvec4::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty dvec2 dvec4::zz
 *
 * This property holds the swizzled zz value
 */

/*!
 * \qmlproperty dvec2 dvec4::zw
 *
 * This property holds the swizzled zw value
 */

/*!
 * \qmlproperty dvec2 dvec4::wx
 *
 * This property holds the swizzled wx value
 */

/*!
 * \qmlproperty dvec2 dvec4::wy
 *
 * This property holds the swizzled wy value
 */

/*!
 * \qmlproperty dvec2 dvec4::wz
 *
 * This property holds the swizzled wz value
 */

/*!
 * \qmlproperty dvec2 dvec4::ww
 *
 * This property holds the swizzled ww value
 */



/*!
 * \qmlproperty dvec3 dvec4::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty dvec3 dvec4::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty dvec3 dvec4::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty dvec3 dvec4::xxw
 *
 * This property holds the swizzled xxw value
 */

/*!
 * \qmlproperty dvec3 dvec4::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty dvec3 dvec4::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty dvec3 dvec4::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty dvec3 dvec4::xyw
 *
 * This property holds the swizzled xyw value
 */

/*!
 * \qmlproperty dvec3 dvec4::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty dvec3 dvec4::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty dvec3 dvec4::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty dvec3 dvec4::xzw
 *
 * This property holds the swizzled xzw value
 */

/*!
 * \qmlproperty dvec3 dvec4::xwx
 *
 * This property holds the swizzled xwx value
 */

/*!
 * \qmlproperty dvec3 dvec4::xwy
 *
 * This property holds the swizzled xwy value
 */

/*!
 * \qmlproperty dvec3 dvec4::xwz
 *
 * This property holds the swizzled xwz value
 */

/*!
 * \qmlproperty dvec3 dvec4::xww
 *
 * This property holds the swizzled xww value
 */

/*!
 * \qmlproperty dvec3 dvec4::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty dvec3 dvec4::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty dvec3 dvec4::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty dvec3 dvec4::yxw
 *
 * This property holds the swizzled yxw value
 */

/*!
 * \qmlproperty dvec3 dvec4::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty dvec3 dvec4::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty dvec3 dvec4::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty dvec3 dvec4::yyw
 *
 * This property holds the swizzled yyw value
 */

/*!
 * \qmlproperty dvec3 dvec4::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty dvec3 dvec4::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty dvec3 dvec4::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty dvec3 dvec4::yzw
 *
 * This property holds the swizzled yzw value
 */

/*!
 * \qmlproperty dvec3 dvec4::ywx
 *
 * This property holds the swizzled ywx value
 */

/*!
 * \qmlproperty dvec3 dvec4::ywy
 *
 * This property holds the swizzled ywy value
 */

/*!
 * \qmlproperty dvec3 dvec4::ywz
 *
 * This property holds the swizzled ywz value
 */

/*!
 * \qmlproperty dvec3 dvec4::yww
 *
 * This property holds the swizzled yww value
 */

/*!
 * \qmlproperty dvec3 dvec4::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty dvec3 dvec4::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty dvec3 dvec4::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty dvec3 dvec4::zxw
 *
 * This property holds the swizzled zxw value
 */

/*!
 * \qmlproperty dvec3 dvec4::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty dvec3 dvec4::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty dvec3 dvec4::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty dvec3 dvec4::zyw
 *
 * This property holds the swizzled zyw value
 */

/*!
 * \qmlproperty dvec3 dvec4::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty dvec3 dvec4::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty dvec3 dvec4::zzz
 *
 * This property holds the swizzled zzz value
 */

/*!
 * \qmlproperty dvec3 dvec4::zzw
 *
 * This property holds the swizzled zzw value
 */

/*!
 * \qmlproperty dvec3 dvec4::zwx
 *
 * This property holds the swizzled zwx value
 */

/*!
 * \qmlproperty dvec3 dvec4::zwy
 *
 * This property holds the swizzled zwy value
 */

/*!
 * \qmlproperty dvec3 dvec4::zwz
 *
 * This property holds the swizzled zwz value
 */

/*!
 * \qmlproperty dvec3 dvec4::zww
 *
 * This property holds the swizzled zww value
 */

/*!
 * \qmlproperty dvec3 dvec4::wxx
 *
 * This property holds the swizzled wxx value
 */

/*!
 * \qmlproperty dvec3 dvec4::wxy
 *
 * This property holds the swizzled wxy value
 */

/*!
 * \qmlproperty dvec3 dvec4::wxz
 *
 * This property holds the swizzled wxz value
 */

/*!
 * \qmlproperty dvec3 dvec4::wxw
 *
 * This property holds the swizzled wxw value
 */

/*!
 * \qmlproperty dvec3 dvec4::wyx
 *
 * This property holds the swizzled wyx value
 */

/*!
 * \qmlproperty dvec3 dvec4::wyy
 *
 * This property holds the swizzled wyy value
 */

/*!
 * \qmlproperty dvec3 dvec4::wyz
 *
 * This property holds the swizzled wyz value
 */

/*!
 * \qmlproperty dvec3 dvec4::wyw
 *
 * This property holds the swizzled wyw value
 */

/*!
 * \qmlproperty dvec3 dvec4::wzx
 *
 * This property holds the swizzled wzx value
 */

/*!
 * \qmlproperty dvec3 dvec4::wzy
 *
 * This property holds the swizzled wzy value
 */

/*!
 * \qmlproperty dvec3 dvec4::wzz
 *
 * This property holds the swizzled wzz value
 */

/*!
 * \qmlproperty dvec3 dvec4::wzw
 *
 * This property holds the swizzled wzw value
 */

/*!
 * \qmlproperty dvec3 dvec4::wwx
 *
 * This property holds the swizzled wwx value
 */

/*!
 * \qmlproperty dvec3 dvec4::wwy
 *
 * This property holds the swizzled wwy value
 */

/*!
 * \qmlproperty dvec3 dvec4::wwz
 *
 * This property holds the swizzled wwz value
 */

/*!
 * \qmlproperty dvec3 dvec4::www
 *
 * This property holds the swizzled www value
 */



/*!
 * \qmlproperty dvec4 dvec4::xxxx
 *
 * This property holds the swizzled xxxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxxy
 *
 * This property holds the swizzled xxxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxxz
 *
 * This property holds the swizzled xxxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxxw
 *
 * This property holds the swizzled xxxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxyx
 *
 * This property holds the swizzled xxyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxyy
 *
 * This property holds the swizzled xxyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxyz
 *
 * This property holds the swizzled xxyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxyw
 *
 * This property holds the swizzled xxyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxzx
 *
 * This property holds the swizzled xxzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxzy
 *
 * This property holds the swizzled xxzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxzz
 *
 * This property holds the swizzled xxzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxzw
 *
 * This property holds the swizzled xxzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxwx
 *
 * This property holds the swizzled xxwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxwy
 *
 * This property holds the swizzled xxwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxwz
 *
 * This property holds the swizzled xxwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xxww
 *
 * This property holds the swizzled xxww value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyxx
 *
 * This property holds the swizzled xyxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyxy
 *
 * This property holds the swizzled xyxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyxz
 *
 * This property holds the swizzled xyxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyxw
 *
 * This property holds the swizzled xyxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyyx
 *
 * This property holds the swizzled xyyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyyy
 *
 * This property holds the swizzled xyyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyyz
 *
 * This property holds the swizzled xyyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyyw
 *
 * This property holds the swizzled xyyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyzx
 *
 * This property holds the swizzled xyzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyzy
 *
 * This property holds the swizzled xyzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyzz
 *
 * This property holds the swizzled xyzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyzw
 *
 * This property holds the swizzled xyzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xywx
 *
 * This property holds the swizzled xywx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xywy
 *
 * This property holds the swizzled xywy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xywz
 *
 * This property holds the swizzled xywz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xyww
 *
 * This property holds the swizzled xyww value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzxx
 *
 * This property holds the swizzled xzxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzxy
 *
 * This property holds the swizzled xzxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzxz
 *
 * This property holds the swizzled xzxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzxw
 *
 * This property holds the swizzled xzxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzyx
 *
 * This property holds the swizzled xzyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzyy
 *
 * This property holds the swizzled xzyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzyz
 *
 * This property holds the swizzled xzyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzyw
 *
 * This property holds the swizzled xzyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzzx
 *
 * This property holds the swizzled xzzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzzy
 *
 * This property holds the swizzled xzzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzzz
 *
 * This property holds the swizzled xzzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzzw
 *
 * This property holds the swizzled xzzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzwx
 *
 * This property holds the swizzled xzwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzwy
 *
 * This property holds the swizzled xzwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzwz
 *
 * This property holds the swizzled xzwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xzww
 *
 * This property holds the swizzled xzww value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwxx
 *
 * This property holds the swizzled xwxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwxy
 *
 * This property holds the swizzled xwxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwxz
 *
 * This property holds the swizzled xwxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwxw
 *
 * This property holds the swizzled xwxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwyx
 *
 * This property holds the swizzled xwyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwyy
 *
 * This property holds the swizzled xwyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwyz
 *
 * This property holds the swizzled xwyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwyw
 *
 * This property holds the swizzled xwyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwzx
 *
 * This property holds the swizzled xwzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwzy
 *
 * This property holds the swizzled xwzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwzz
 *
 * This property holds the swizzled xwzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwzw
 *
 * This property holds the swizzled xwzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwwx
 *
 * This property holds the swizzled xwwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwwy
 *
 * This property holds the swizzled xwwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwwz
 *
 * This property holds the swizzled xwwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::xwww
 *
 * This property holds the swizzled xwww value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxxx
 *
 * This property holds the swizzled yxxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxxy
 *
 * This property holds the swizzled yxxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxxz
 *
 * This property holds the swizzled yxxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxxw
 *
 * This property holds the swizzled yxxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxyx
 *
 * This property holds the swizzled yxyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxyy
 *
 * This property holds the swizzled yxyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxyz
 *
 * This property holds the swizzled yxyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxyw
 *
 * This property holds the swizzled yxyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxzx
 *
 * This property holds the swizzled yxzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxzy
 *
 * This property holds the swizzled yxzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxzz
 *
 * This property holds the swizzled yxzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxzw
 *
 * This property holds the swizzled yxzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxwx
 *
 * This property holds the swizzled yxwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxwy
 *
 * This property holds the swizzled yxwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxwz
 *
 * This property holds the swizzled yxwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yxww
 *
 * This property holds the swizzled yxww value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyxx
 *
 * This property holds the swizzled yyxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyxy
 *
 * This property holds the swizzled yyxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyxz
 *
 * This property holds the swizzled yyxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyxw
 *
 * This property holds the swizzled yyxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyyx
 *
 * This property holds the swizzled yyyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyyy
 *
 * This property holds the swizzled yyyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyyz
 *
 * This property holds the swizzled yyyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyyw
 *
 * This property holds the swizzled yyyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyzx
 *
 * This property holds the swizzled yyzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyzy
 *
 * This property holds the swizzled yyzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyzz
 *
 * This property holds the swizzled yyzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyzw
 *
 * This property holds the swizzled yyzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yywx
 *
 * This property holds the swizzled yywx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yywy
 *
 * This property holds the swizzled yywy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yywz
 *
 * This property holds the swizzled yywz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yyww
 *
 * This property holds the swizzled yyww value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzxx
 *
 * This property holds the swizzled yzxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzxy
 *
 * This property holds the swizzled yzxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzxz
 *
 * This property holds the swizzled yzxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzxw
 *
 * This property holds the swizzled yzxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzyx
 *
 * This property holds the swizzled yzyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzyy
 *
 * This property holds the swizzled yzyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzyz
 *
 * This property holds the swizzled yzyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzyw
 *
 * This property holds the swizzled yzyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzzx
 *
 * This property holds the swizzled yzzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzzy
 *
 * This property holds the swizzled yzzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzzz
 *
 * This property holds the swizzled yzzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzzw
 *
 * This property holds the swizzled yzzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzwx
 *
 * This property holds the swizzled yzwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzwy
 *
 * This property holds the swizzled yzwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzwz
 *
 * This property holds the swizzled yzwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::yzww
 *
 * This property holds the swizzled yzww value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywxx
 *
 * This property holds the swizzled ywxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywxy
 *
 * This property holds the swizzled ywxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywxz
 *
 * This property holds the swizzled ywxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywxw
 *
 * This property holds the swizzled ywxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywyx
 *
 * This property holds the swizzled ywyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywyy
 *
 * This property holds the swizzled ywyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywyz
 *
 * This property holds the swizzled ywyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywyw
 *
 * This property holds the swizzled ywyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywzx
 *
 * This property holds the swizzled ywzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywzy
 *
 * This property holds the swizzled ywzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywzz
 *
 * This property holds the swizzled ywzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywzw
 *
 * This property holds the swizzled ywzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywwx
 *
 * This property holds the swizzled ywwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywwy
 *
 * This property holds the swizzled ywwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywwz
 *
 * This property holds the swizzled ywwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::ywww
 *
 * This property holds the swizzled ywww value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxxx
 *
 * This property holds the swizzled zxxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxxy
 *
 * This property holds the swizzled zxxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxxz
 *
 * This property holds the swizzled zxxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxxw
 *
 * This property holds the swizzled zxxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxyx
 *
 * This property holds the swizzled zxyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxyy
 *
 * This property holds the swizzled zxyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxyz
 *
 * This property holds the swizzled zxyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxyw
 *
 * This property holds the swizzled zxyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxzx
 *
 * This property holds the swizzled zxzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxzy
 *
 * This property holds the swizzled zxzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxzz
 *
 * This property holds the swizzled zxzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxzw
 *
 * This property holds the swizzled zxzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxwx
 *
 * This property holds the swizzled zxwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxwy
 *
 * This property holds the swizzled zxwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxwz
 *
 * This property holds the swizzled zxwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zxww
 *
 * This property holds the swizzled zxww value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyxx
 *
 * This property holds the swizzled zyxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyxy
 *
 * This property holds the swizzled zyxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyxz
 *
 * This property holds the swizzled zyxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyxw
 *
 * This property holds the swizzled zyxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyyx
 *
 * This property holds the swizzled zyyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyyy
 *
 * This property holds the swizzled zyyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyyz
 *
 * This property holds the swizzled zyyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyyw
 *
 * This property holds the swizzled zyyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyzx
 *
 * This property holds the swizzled zyzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyzy
 *
 * This property holds the swizzled zyzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyzz
 *
 * This property holds the swizzled zyzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyzw
 *
 * This property holds the swizzled zyzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zywx
 *
 * This property holds the swizzled zywx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zywy
 *
 * This property holds the swizzled zywy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zywz
 *
 * This property holds the swizzled zywz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zyww
 *
 * This property holds the swizzled zyww value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzxx
 *
 * This property holds the swizzled zzxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzxy
 *
 * This property holds the swizzled zzxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzxz
 *
 * This property holds the swizzled zzxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzxw
 *
 * This property holds the swizzled zzxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzyx
 *
 * This property holds the swizzled zzyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzyy
 *
 * This property holds the swizzled zzyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzyz
 *
 * This property holds the swizzled zzyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzyw
 *
 * This property holds the swizzled zzyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzzx
 *
 * This property holds the swizzled zzzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzzy
 *
 * This property holds the swizzled zzzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzzz
 *
 * This property holds the swizzled zzzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzzw
 *
 * This property holds the swizzled zzzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzwx
 *
 * This property holds the swizzled zzwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzwy
 *
 * This property holds the swizzled zzwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzwz
 *
 * This property holds the swizzled zzwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zzww
 *
 * This property holds the swizzled zzww value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwxx
 *
 * This property holds the swizzled zwxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwxy
 *
 * This property holds the swizzled zwxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwxz
 *
 * This property holds the swizzled zwxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwxw
 *
 * This property holds the swizzled zwxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwyx
 *
 * This property holds the swizzled zwyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwyy
 *
 * This property holds the swizzled zwyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwyz
 *
 * This property holds the swizzled zwyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwyw
 *
 * This property holds the swizzled zwyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwzx
 *
 * This property holds the swizzled zwzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwzy
 *
 * This property holds the swizzled zwzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwzz
 *
 * This property holds the swizzled zwzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwzw
 *
 * This property holds the swizzled zwzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwwx
 *
 * This property holds the swizzled zwwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwwy
 *
 * This property holds the swizzled zwwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwwz
 *
 * This property holds the swizzled zwwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::zwww
 *
 * This property holds the swizzled zwww value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxxx
 *
 * This property holds the swizzled wxxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxxy
 *
 * This property holds the swizzled wxxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxxz
 *
 * This property holds the swizzled wxxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxxw
 *
 * This property holds the swizzled wxxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxyx
 *
 * This property holds the swizzled wxyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxyy
 *
 * This property holds the swizzled wxyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxyz
 *
 * This property holds the swizzled wxyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxyw
 *
 * This property holds the swizzled wxyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxzx
 *
 * This property holds the swizzled wxzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxzy
 *
 * This property holds the swizzled wxzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxzz
 *
 * This property holds the swizzled wxzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxzw
 *
 * This property holds the swizzled wxzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxwx
 *
 * This property holds the swizzled wxwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxwy
 *
 * This property holds the swizzled wxwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxwz
 *
 * This property holds the swizzled wxwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wxww
 *
 * This property holds the swizzled wxww value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyxx
 *
 * This property holds the swizzled wyxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyxy
 *
 * This property holds the swizzled wyxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyxz
 *
 * This property holds the swizzled wyxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyxw
 *
 * This property holds the swizzled wyxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyyx
 *
 * This property holds the swizzled wyyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyyy
 *
 * This property holds the swizzled wyyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyyz
 *
 * This property holds the swizzled wyyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyyw
 *
 * This property holds the swizzled wyyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyzx
 *
 * This property holds the swizzled wyzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyzy
 *
 * This property holds the swizzled wyzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyzz
 *
 * This property holds the swizzled wyzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyzw
 *
 * This property holds the swizzled wyzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wywx
 *
 * This property holds the swizzled wywx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wywy
 *
 * This property holds the swizzled wywy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wywz
 *
 * This property holds the swizzled wywz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wyww
 *
 * This property holds the swizzled wyww value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzxx
 *
 * This property holds the swizzled wzxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzxy
 *
 * This property holds the swizzled wzxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzxz
 *
 * This property holds the swizzled wzxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzxw
 *
 * This property holds the swizzled wzxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzyx
 *
 * This property holds the swizzled wzyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzyy
 *
 * This property holds the swizzled wzyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzyz
 *
 * This property holds the swizzled wzyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzyw
 *
 * This property holds the swizzled wzyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzzx
 *
 * This property holds the swizzled wzzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzzy
 *
 * This property holds the swizzled wzzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzzz
 *
 * This property holds the swizzled wzzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzzw
 *
 * This property holds the swizzled wzzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzwx
 *
 * This property holds the swizzled wzwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzwy
 *
 * This property holds the swizzled wzwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzwz
 *
 * This property holds the swizzled wzwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wzww
 *
 * This property holds the swizzled wzww value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwxx
 *
 * This property holds the swizzled wwxx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwxy
 *
 * This property holds the swizzled wwxy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwxz
 *
 * This property holds the swizzled wwxz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwxw
 *
 * This property holds the swizzled wwxw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwyx
 *
 * This property holds the swizzled wwyx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwyy
 *
 * This property holds the swizzled wwyy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwyz
 *
 * This property holds the swizzled wwyz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwyw
 *
 * This property holds the swizzled wwyw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwzx
 *
 * This property holds the swizzled wwzx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwzy
 *
 * This property holds the swizzled wwzy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwzz
 *
 * This property holds the swizzled wwzz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwzw
 *
 * This property holds the swizzled wwzw value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwwx
 *
 * This property holds the swizzled wwwx value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwwy
 *
 * This property holds the swizzled wwwy value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwwz
 *
 * This property holds the swizzled wwwz value
 */

/*!
 * \qmlproperty dvec4 dvec4::wwww
 *
 * This property holds the swizzled wwww value
 */

/*!
 * \qmlbasictype bvec4
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The bvec4 basic type represents a GLSL bvec4 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty bool bvec4::x
 *
 * This property holds the x component of the bvec4.
 */

/*!
 * \qmlproperty bool bvec4::y
 *
 * This property holds the y component of the bvec4.
 */

/*!
 * \qmlproperty bool bvec4::z
 *
 * This property holds the z component of the bvec4.
 */

/*!
 * \qmlproperty bool bvec4::w
 *
 * This property holds the w component of the bvec4.
 */

/*!
 * \qmlproperty bvec2 bvec4::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty bvec2 bvec4::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty bvec2 bvec4::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty bvec2 bvec4::xw
 *
 * This property holds the swizzled xw value
 */

/*!
 * \qmlproperty bvec2 bvec4::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty bvec2 bvec4::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty bvec2 bvec4::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty bvec2 bvec4::yw
 *
 * This property holds the swizzled yw value
 */

/*!
 * \qmlproperty bvec2 bvec4::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty bvec2 bvec4::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty bvec2 bvec4::zz
 *
 * This property holds the swizzled zz value
 */

/*!
 * \qmlproperty bvec2 bvec4::zw
 *
 * This property holds the swizzled zw value
 */

/*!
 * \qmlproperty bvec2 bvec4::wx
 *
 * This property holds the swizzled wx value
 */

/*!
 * \qmlproperty bvec2 bvec4::wy
 *
 * This property holds the swizzled wy value
 */

/*!
 * \qmlproperty bvec2 bvec4::wz
 *
 * This property holds the swizzled wz value
 */

/*!
 * \qmlproperty bvec2 bvec4::ww
 *
 * This property holds the swizzled ww value
 */



/*!
 * \qmlproperty bvec3 bvec4::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty bvec3 bvec4::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty bvec3 bvec4::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty bvec3 bvec4::xxw
 *
 * This property holds the swizzled xxw value
 */

/*!
 * \qmlproperty bvec3 bvec4::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty bvec3 bvec4::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty bvec3 bvec4::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty bvec3 bvec4::xyw
 *
 * This property holds the swizzled xyw value
 */

/*!
 * \qmlproperty bvec3 bvec4::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty bvec3 bvec4::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty bvec3 bvec4::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty bvec3 bvec4::xzw
 *
 * This property holds the swizzled xzw value
 */

/*!
 * \qmlproperty bvec3 bvec4::xwx
 *
 * This property holds the swizzled xwx value
 */

/*!
 * \qmlproperty bvec3 bvec4::xwy
 *
 * This property holds the swizzled xwy value
 */

/*!
 * \qmlproperty bvec3 bvec4::xwz
 *
 * This property holds the swizzled xwz value
 */

/*!
 * \qmlproperty bvec3 bvec4::xww
 *
 * This property holds the swizzled xww value
 */

/*!
 * \qmlproperty bvec3 bvec4::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty bvec3 bvec4::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty bvec3 bvec4::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty bvec3 bvec4::yxw
 *
 * This property holds the swizzled yxw value
 */

/*!
 * \qmlproperty bvec3 bvec4::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty bvec3 bvec4::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty bvec3 bvec4::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty bvec3 bvec4::yyw
 *
 * This property holds the swizzled yyw value
 */

/*!
 * \qmlproperty bvec3 bvec4::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty bvec3 bvec4::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty bvec3 bvec4::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty bvec3 bvec4::yzw
 *
 * This property holds the swizzled yzw value
 */

/*!
 * \qmlproperty bvec3 bvec4::ywx
 *
 * This property holds the swizzled ywx value
 */

/*!
 * \qmlproperty bvec3 bvec4::ywy
 *
 * This property holds the swizzled ywy value
 */

/*!
 * \qmlproperty bvec3 bvec4::ywz
 *
 * This property holds the swizzled ywz value
 */

/*!
 * \qmlproperty bvec3 bvec4::yww
 *
 * This property holds the swizzled yww value
 */

/*!
 * \qmlproperty bvec3 bvec4::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty bvec3 bvec4::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty bvec3 bvec4::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty bvec3 bvec4::zxw
 *
 * This property holds the swizzled zxw value
 */

/*!
 * \qmlproperty bvec3 bvec4::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty bvec3 bvec4::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty bvec3 bvec4::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty bvec3 bvec4::zyw
 *
 * This property holds the swizzled zyw value
 */

/*!
 * \qmlproperty bvec3 bvec4::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty bvec3 bvec4::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty bvec3 bvec4::zzz
 *
 * This property holds the swizzled zzz value
 */

/*!
 * \qmlproperty bvec3 bvec4::zzw
 *
 * This property holds the swizzled zzw value
 */

/*!
 * \qmlproperty bvec3 bvec4::zwx
 *
 * This property holds the swizzled zwx value
 */

/*!
 * \qmlproperty bvec3 bvec4::zwy
 *
 * This property holds the swizzled zwy value
 */

/*!
 * \qmlproperty bvec3 bvec4::zwz
 *
 * This property holds the swizzled zwz value
 */

/*!
 * \qmlproperty bvec3 bvec4::zww
 *
 * This property holds the swizzled zww value
 */

/*!
 * \qmlproperty bvec3 bvec4::wxx
 *
 * This property holds the swizzled wxx value
 */

/*!
 * \qmlproperty bvec3 bvec4::wxy
 *
 * This property holds the swizzled wxy value
 */

/*!
 * \qmlproperty bvec3 bvec4::wxz
 *
 * This property holds the swizzled wxz value
 */

/*!
 * \qmlproperty bvec3 bvec4::wxw
 *
 * This property holds the swizzled wxw value
 */

/*!
 * \qmlproperty bvec3 bvec4::wyx
 *
 * This property holds the swizzled wyx value
 */

/*!
 * \qmlproperty bvec3 bvec4::wyy
 *
 * This property holds the swizzled wyy value
 */

/*!
 * \qmlproperty bvec3 bvec4::wyz
 *
 * This property holds the swizzled wyz value
 */

/*!
 * \qmlproperty bvec3 bvec4::wyw
 *
 * This property holds the swizzled wyw value
 */

/*!
 * \qmlproperty bvec3 bvec4::wzx
 *
 * This property holds the swizzled wzx value
 */

/*!
 * \qmlproperty bvec3 bvec4::wzy
 *
 * This property holds the swizzled wzy value
 */

/*!
 * \qmlproperty bvec3 bvec4::wzz
 *
 * This property holds the swizzled wzz value
 */

/*!
 * \qmlproperty bvec3 bvec4::wzw
 *
 * This property holds the swizzled wzw value
 */

/*!
 * \qmlproperty bvec3 bvec4::wwx
 *
 * This property holds the swizzled wwx value
 */

/*!
 * \qmlproperty bvec3 bvec4::wwy
 *
 * This property holds the swizzled wwy value
 */

/*!
 * \qmlproperty bvec3 bvec4::wwz
 *
 * This property holds the swizzled wwz value
 */

/*!
 * \qmlproperty bvec3 bvec4::www
 *
 * This property holds the swizzled www value
 */



/*!
 * \qmlproperty bvec4 bvec4::xxxx
 *
 * This property holds the swizzled xxxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxxy
 *
 * This property holds the swizzled xxxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxxz
 *
 * This property holds the swizzled xxxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxxw
 *
 * This property holds the swizzled xxxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxyx
 *
 * This property holds the swizzled xxyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxyy
 *
 * This property holds the swizzled xxyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxyz
 *
 * This property holds the swizzled xxyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxyw
 *
 * This property holds the swizzled xxyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxzx
 *
 * This property holds the swizzled xxzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxzy
 *
 * This property holds the swizzled xxzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxzz
 *
 * This property holds the swizzled xxzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxzw
 *
 * This property holds the swizzled xxzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxwx
 *
 * This property holds the swizzled xxwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxwy
 *
 * This property holds the swizzled xxwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxwz
 *
 * This property holds the swizzled xxwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xxww
 *
 * This property holds the swizzled xxww value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyxx
 *
 * This property holds the swizzled xyxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyxy
 *
 * This property holds the swizzled xyxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyxz
 *
 * This property holds the swizzled xyxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyxw
 *
 * This property holds the swizzled xyxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyyx
 *
 * This property holds the swizzled xyyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyyy
 *
 * This property holds the swizzled xyyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyyz
 *
 * This property holds the swizzled xyyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyyw
 *
 * This property holds the swizzled xyyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyzx
 *
 * This property holds the swizzled xyzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyzy
 *
 * This property holds the swizzled xyzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyzz
 *
 * This property holds the swizzled xyzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyzw
 *
 * This property holds the swizzled xyzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xywx
 *
 * This property holds the swizzled xywx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xywy
 *
 * This property holds the swizzled xywy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xywz
 *
 * This property holds the swizzled xywz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xyww
 *
 * This property holds the swizzled xyww value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzxx
 *
 * This property holds the swizzled xzxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzxy
 *
 * This property holds the swizzled xzxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzxz
 *
 * This property holds the swizzled xzxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzxw
 *
 * This property holds the swizzled xzxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzyx
 *
 * This property holds the swizzled xzyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzyy
 *
 * This property holds the swizzled xzyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzyz
 *
 * This property holds the swizzled xzyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzyw
 *
 * This property holds the swizzled xzyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzzx
 *
 * This property holds the swizzled xzzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzzy
 *
 * This property holds the swizzled xzzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzzz
 *
 * This property holds the swizzled xzzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzzw
 *
 * This property holds the swizzled xzzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzwx
 *
 * This property holds the swizzled xzwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzwy
 *
 * This property holds the swizzled xzwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzwz
 *
 * This property holds the swizzled xzwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xzww
 *
 * This property holds the swizzled xzww value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwxx
 *
 * This property holds the swizzled xwxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwxy
 *
 * This property holds the swizzled xwxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwxz
 *
 * This property holds the swizzled xwxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwxw
 *
 * This property holds the swizzled xwxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwyx
 *
 * This property holds the swizzled xwyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwyy
 *
 * This property holds the swizzled xwyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwyz
 *
 * This property holds the swizzled xwyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwyw
 *
 * This property holds the swizzled xwyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwzx
 *
 * This property holds the swizzled xwzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwzy
 *
 * This property holds the swizzled xwzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwzz
 *
 * This property holds the swizzled xwzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwzw
 *
 * This property holds the swizzled xwzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwwx
 *
 * This property holds the swizzled xwwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwwy
 *
 * This property holds the swizzled xwwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwwz
 *
 * This property holds the swizzled xwwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::xwww
 *
 * This property holds the swizzled xwww value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxxx
 *
 * This property holds the swizzled yxxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxxy
 *
 * This property holds the swizzled yxxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxxz
 *
 * This property holds the swizzled yxxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxxw
 *
 * This property holds the swizzled yxxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxyx
 *
 * This property holds the swizzled yxyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxyy
 *
 * This property holds the swizzled yxyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxyz
 *
 * This property holds the swizzled yxyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxyw
 *
 * This property holds the swizzled yxyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxzx
 *
 * This property holds the swizzled yxzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxzy
 *
 * This property holds the swizzled yxzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxzz
 *
 * This property holds the swizzled yxzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxzw
 *
 * This property holds the swizzled yxzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxwx
 *
 * This property holds the swizzled yxwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxwy
 *
 * This property holds the swizzled yxwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxwz
 *
 * This property holds the swizzled yxwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yxww
 *
 * This property holds the swizzled yxww value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyxx
 *
 * This property holds the swizzled yyxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyxy
 *
 * This property holds the swizzled yyxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyxz
 *
 * This property holds the swizzled yyxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyxw
 *
 * This property holds the swizzled yyxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyyx
 *
 * This property holds the swizzled yyyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyyy
 *
 * This property holds the swizzled yyyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyyz
 *
 * This property holds the swizzled yyyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyyw
 *
 * This property holds the swizzled yyyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyzx
 *
 * This property holds the swizzled yyzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyzy
 *
 * This property holds the swizzled yyzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyzz
 *
 * This property holds the swizzled yyzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyzw
 *
 * This property holds the swizzled yyzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yywx
 *
 * This property holds the swizzled yywx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yywy
 *
 * This property holds the swizzled yywy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yywz
 *
 * This property holds the swizzled yywz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yyww
 *
 * This property holds the swizzled yyww value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzxx
 *
 * This property holds the swizzled yzxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzxy
 *
 * This property holds the swizzled yzxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzxz
 *
 * This property holds the swizzled yzxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzxw
 *
 * This property holds the swizzled yzxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzyx
 *
 * This property holds the swizzled yzyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzyy
 *
 * This property holds the swizzled yzyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzyz
 *
 * This property holds the swizzled yzyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzyw
 *
 * This property holds the swizzled yzyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzzx
 *
 * This property holds the swizzled yzzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzzy
 *
 * This property holds the swizzled yzzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzzz
 *
 * This property holds the swizzled yzzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzzw
 *
 * This property holds the swizzled yzzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzwx
 *
 * This property holds the swizzled yzwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzwy
 *
 * This property holds the swizzled yzwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzwz
 *
 * This property holds the swizzled yzwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::yzww
 *
 * This property holds the swizzled yzww value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywxx
 *
 * This property holds the swizzled ywxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywxy
 *
 * This property holds the swizzled ywxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywxz
 *
 * This property holds the swizzled ywxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywxw
 *
 * This property holds the swizzled ywxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywyx
 *
 * This property holds the swizzled ywyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywyy
 *
 * This property holds the swizzled ywyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywyz
 *
 * This property holds the swizzled ywyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywyw
 *
 * This property holds the swizzled ywyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywzx
 *
 * This property holds the swizzled ywzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywzy
 *
 * This property holds the swizzled ywzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywzz
 *
 * This property holds the swizzled ywzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywzw
 *
 * This property holds the swizzled ywzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywwx
 *
 * This property holds the swizzled ywwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywwy
 *
 * This property holds the swizzled ywwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywwz
 *
 * This property holds the swizzled ywwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::ywww
 *
 * This property holds the swizzled ywww value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxxx
 *
 * This property holds the swizzled zxxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxxy
 *
 * This property holds the swizzled zxxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxxz
 *
 * This property holds the swizzled zxxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxxw
 *
 * This property holds the swizzled zxxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxyx
 *
 * This property holds the swizzled zxyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxyy
 *
 * This property holds the swizzled zxyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxyz
 *
 * This property holds the swizzled zxyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxyw
 *
 * This property holds the swizzled zxyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxzx
 *
 * This property holds the swizzled zxzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxzy
 *
 * This property holds the swizzled zxzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxzz
 *
 * This property holds the swizzled zxzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxzw
 *
 * This property holds the swizzled zxzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxwx
 *
 * This property holds the swizzled zxwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxwy
 *
 * This property holds the swizzled zxwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxwz
 *
 * This property holds the swizzled zxwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zxww
 *
 * This property holds the swizzled zxww value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyxx
 *
 * This property holds the swizzled zyxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyxy
 *
 * This property holds the swizzled zyxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyxz
 *
 * This property holds the swizzled zyxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyxw
 *
 * This property holds the swizzled zyxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyyx
 *
 * This property holds the swizzled zyyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyyy
 *
 * This property holds the swizzled zyyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyyz
 *
 * This property holds the swizzled zyyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyyw
 *
 * This property holds the swizzled zyyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyzx
 *
 * This property holds the swizzled zyzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyzy
 *
 * This property holds the swizzled zyzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyzz
 *
 * This property holds the swizzled zyzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyzw
 *
 * This property holds the swizzled zyzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zywx
 *
 * This property holds the swizzled zywx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zywy
 *
 * This property holds the swizzled zywy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zywz
 *
 * This property holds the swizzled zywz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zyww
 *
 * This property holds the swizzled zyww value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzxx
 *
 * This property holds the swizzled zzxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzxy
 *
 * This property holds the swizzled zzxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzxz
 *
 * This property holds the swizzled zzxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzxw
 *
 * This property holds the swizzled zzxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzyx
 *
 * This property holds the swizzled zzyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzyy
 *
 * This property holds the swizzled zzyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzyz
 *
 * This property holds the swizzled zzyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzyw
 *
 * This property holds the swizzled zzyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzzx
 *
 * This property holds the swizzled zzzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzzy
 *
 * This property holds the swizzled zzzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzzz
 *
 * This property holds the swizzled zzzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzzw
 *
 * This property holds the swizzled zzzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzwx
 *
 * This property holds the swizzled zzwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzwy
 *
 * This property holds the swizzled zzwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzwz
 *
 * This property holds the swizzled zzwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zzww
 *
 * This property holds the swizzled zzww value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwxx
 *
 * This property holds the swizzled zwxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwxy
 *
 * This property holds the swizzled zwxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwxz
 *
 * This property holds the swizzled zwxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwxw
 *
 * This property holds the swizzled zwxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwyx
 *
 * This property holds the swizzled zwyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwyy
 *
 * This property holds the swizzled zwyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwyz
 *
 * This property holds the swizzled zwyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwyw
 *
 * This property holds the swizzled zwyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwzx
 *
 * This property holds the swizzled zwzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwzy
 *
 * This property holds the swizzled zwzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwzz
 *
 * This property holds the swizzled zwzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwzw
 *
 * This property holds the swizzled zwzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwwx
 *
 * This property holds the swizzled zwwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwwy
 *
 * This property holds the swizzled zwwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwwz
 *
 * This property holds the swizzled zwwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::zwww
 *
 * This property holds the swizzled zwww value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxxx
 *
 * This property holds the swizzled wxxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxxy
 *
 * This property holds the swizzled wxxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxxz
 *
 * This property holds the swizzled wxxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxxw
 *
 * This property holds the swizzled wxxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxyx
 *
 * This property holds the swizzled wxyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxyy
 *
 * This property holds the swizzled wxyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxyz
 *
 * This property holds the swizzled wxyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxyw
 *
 * This property holds the swizzled wxyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxzx
 *
 * This property holds the swizzled wxzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxzy
 *
 * This property holds the swizzled wxzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxzz
 *
 * This property holds the swizzled wxzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxzw
 *
 * This property holds the swizzled wxzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxwx
 *
 * This property holds the swizzled wxwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxwy
 *
 * This property holds the swizzled wxwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxwz
 *
 * This property holds the swizzled wxwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wxww
 *
 * This property holds the swizzled wxww value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyxx
 *
 * This property holds the swizzled wyxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyxy
 *
 * This property holds the swizzled wyxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyxz
 *
 * This property holds the swizzled wyxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyxw
 *
 * This property holds the swizzled wyxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyyx
 *
 * This property holds the swizzled wyyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyyy
 *
 * This property holds the swizzled wyyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyyz
 *
 * This property holds the swizzled wyyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyyw
 *
 * This property holds the swizzled wyyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyzx
 *
 * This property holds the swizzled wyzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyzy
 *
 * This property holds the swizzled wyzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyzz
 *
 * This property holds the swizzled wyzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyzw
 *
 * This property holds the swizzled wyzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wywx
 *
 * This property holds the swizzled wywx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wywy
 *
 * This property holds the swizzled wywy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wywz
 *
 * This property holds the swizzled wywz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wyww
 *
 * This property holds the swizzled wyww value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzxx
 *
 * This property holds the swizzled wzxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzxy
 *
 * This property holds the swizzled wzxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzxz
 *
 * This property holds the swizzled wzxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzxw
 *
 * This property holds the swizzled wzxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzyx
 *
 * This property holds the swizzled wzyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzyy
 *
 * This property holds the swizzled wzyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzyz
 *
 * This property holds the swizzled wzyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzyw
 *
 * This property holds the swizzled wzyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzzx
 *
 * This property holds the swizzled wzzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzzy
 *
 * This property holds the swizzled wzzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzzz
 *
 * This property holds the swizzled wzzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzzw
 *
 * This property holds the swizzled wzzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzwx
 *
 * This property holds the swizzled wzwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzwy
 *
 * This property holds the swizzled wzwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzwz
 *
 * This property holds the swizzled wzwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wzww
 *
 * This property holds the swizzled wzww value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwxx
 *
 * This property holds the swizzled wwxx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwxy
 *
 * This property holds the swizzled wwxy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwxz
 *
 * This property holds the swizzled wwxz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwxw
 *
 * This property holds the swizzled wwxw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwyx
 *
 * This property holds the swizzled wwyx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwyy
 *
 * This property holds the swizzled wwyy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwyz
 *
 * This property holds the swizzled wwyz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwyw
 *
 * This property holds the swizzled wwyw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwzx
 *
 * This property holds the swizzled wwzx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwzy
 *
 * This property holds the swizzled wwzy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwzz
 *
 * This property holds the swizzled wwzz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwzw
 *
 * This property holds the swizzled wwzw value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwwx
 *
 * This property holds the swizzled wwwx value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwwy
 *
 * This property holds the swizzled wwwy value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwwz
 *
 * This property holds the swizzled wwwz value
 */

/*!
 * \qmlproperty bvec4 bvec4::wwww
 *
 * This property holds the swizzled wwww value
 */

/*!
 * \qmlmethod float vec4::dotProduct(vec4 other)
 * \param other the other vector
 * \return the dot product of this vector and the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} dotProduct method.
 * \sa vector4d
 */
float qvec4::dotProduct(const qvec4 &other) const
{
    return x()*other.x() + y()*other.y() + z()*other.z() + w()*other.w();

}

/*!
 * \qmlmethod vec4 vec4::times(mat4 matrix)
 * \param matrix the matrix
 * \return the result of transforming this vector with the matrix (applied post-vector)
 *
 * This method performs the same function as the equivalent QML \l{vector4d} times method.
 * \sa vector4d
 */
qvec4 qvec4::times(const qmat4 &matrix) const
{
    return *this*matrix;
}

/*!
 * \qmlmethod vec4 vec4::times(vec4 other)
 * \param other the other vector
 * \return the result of multiplying this vector by the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} times method.
 * \sa vector4d
 */
qvec4 qvec4::times(const qvec4 &other) const
{
    return qvec4(x()*other.x(), y()*other.y(), z()*other.z(), w()*other.w());
}

/*!
 * \qmlmethod vec4 vec4::times(float factor)
 * \param factor the scalar factor
 * \return the result of multiplying this vector by the scalar factor
 *
 * This method performs the same function as the equivalent QML \l{vector4d} times method.
 * \sa vector4d
 */
qvec4 qvec4::times(float factor) const
{
    return *this*factor;
}

/*!
 * \qmlmethod vec4 vec4::plus(vec4 other)
 * \param other the other vector
 * \return the result of adding the other vector to this vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} plus method.
 * \sa vector4d
 */
qvec4 qvec4::plus(const qvec4 &other) const
{
    return *this+other;
}

/*!
 * \qmlmethod vec4 vec4::minus(vec4 other)
 * \param other the other vector
 * \return the result of subtracting the other vector from this vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} minus method.
 * \sa vector4d
 */
qvec4 qvec4::minus(const qvec4 &other) const
{
    return *this-other;
}

/*!
 * \qmlmethod vec4 vec4::normalized()
 * \return the normalized vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} normalized method.
 * \sa vector4d
 */
qvec4 qvec4::normalized() const
{
    return *this/length();
}

/*!
 * \qmlmethod double vec4::length()
 * \return the length of the vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} length method.
 * \sa vector4d
 */
float qvec4::length() const
{
    return std::sqrt(x()*x()+y()*y()+z()*z()+w()*w());
}

/*!
 * \qmlmethod float dvec4::dotProduct(dvec4 other)
 * \param other the other vector
 * \return the dot product of this vector and the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} dotProduct method.
 * \sa vector4d
 */
double qdvec4::dotProduct(const qdvec4 &other) const
{
    return x()*other.x() + y()*other.y() + z()*other.z() + w()*other.w();
}

/*!
 * \qmlmethod dvec4 dvec4::times(dmat4 matrix)
 * \param matrix the matrix
 * \return the result of transforming this vector with the matrix (applied post-vector)
 *
 * This method performs the same function as the equivalent QML \l{vector4d} times method.
 * \sa vector4d
 */
qdvec4 qdvec4::times(const qdmat4 &matrix) const
{
    return *this*matrix;
}

/*!
 * \qmlmethod dvec4 dvec4::times(dvec4 other)
 * \param other the other vector
 * \return the result of multiplying this vector by the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} times method.
 * \sa vector4d
 */
qdvec4 qdvec4::times(const qdvec4 &other) const
{
    return qdvec4(x()*other.x(), y()*other.y(), z()*other.z(), w()*other.w());
}

/*!
 * \qmlmethod dvec4 dvec4::times(double factor)
 * \param factor the scalar factor
 * \return the result of multiplying this vector by the scalar factor
 *
 * This method performs the same function as the equivalent QML \l{vector4d} times method.
 * \sa vector4d
 */
qdvec4 qdvec4::times(double factor) const
{
    return *this*factor;
}

/*!
 * \qmlmethod dvec4 dvec4::plus(dvec4 other)
 * \param other the other vector
 * \return the result of adding the other vector to this vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} plus method.
 * \sa vector4d
 */
qdvec4 qdvec4::plus(const qdvec4 &other) const
{
    return *this+other;
}

/*!
 * \qmlmethod dvec4 dvec4::minus(dvec4 other)
 * \param other the other dvector
 * \return the result of subtracting the other dvector from this dvector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} minus method.
 * \sa vector4d
 */
qdvec4 qdvec4::minus(const qdvec4 &other) const
{
    return *this-other;
}

/*!
 * \qmlmethod dvec4 dvec4::normalized()
 * \return the normalized vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} normalized method.
 * \sa vector4d
 */
qdvec4 qdvec4::normalized() const
{
    return *this/length();
}


/*!
 * \qmlmethod double dvec4::length()
 * \return the length of the vector
 *
 * This method performs the same function as the equivalent QML \l{vector4d} length method.
 * \sa vector4d
 */
double qdvec4::length() const
{
    return std::sqrt(x()*x()+y()*y()+z()*z()+w()*w());
}

/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksguniformbuffer.h"
#include <QOpenGLBuffer>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLExtraFunctions>
#include "support.h"

class QuickSGUniformBufferPrivate : public QObject
{
    Q_OBJECT

public:

    QuickSGUniformBufferPrivate(QuickSGUniformBuffer *q);

    QOpenGLBuffer buffer;

protected:

    QuickSGUniformBuffer *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickSGUniformBuffer)
};

QuickSGUniformBufferPrivate::QuickSGUniformBufferPrivate(QuickSGUniformBuffer *q) :
    buffer(QOpenGLBuffer::Type(GL_UNIFORM_BUFFER)),
    q_ptr(q)
{
}


QuickSGUniformBuffer::QuickSGUniformBuffer() :
d_ptr(new QuickSGUniformBufferPrivate(this))
{
}

QuickSGUniformBuffer::~QuickSGUniformBuffer()
{

}

int QuickSGUniformBuffer::bufferId()
{
    Q_D(QuickSGUniformBuffer);

    if(!d->buffer.isCreated())
    {
        d->buffer.create();debugGL;
        emit bufferIdChanged(int(d->buffer.bufferId()));
    }

    return int(d->buffer.bufferId());
}

void QuickSGUniformBuffer::bind()
{
    Q_D(QuickSGUniformBuffer);

    if(!d->buffer.isCreated())
    {
        d->buffer.create();debugGL;
        emit bufferIdChanged(int(d->buffer.bufferId()));
    }

    d->buffer.bind();debugGL;

}

void QuickSGUniformBuffer::release()
{
    Q_D(QuickSGUniformBuffer);

    d->buffer.release();debugGL;
}

void QuickSGUniformBuffer::bindBufferBase(unsigned int bindingIndex)
{
    Q_D(QuickSGUniformBuffer);
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
    glExtra->initializeOpenGLFunctions();debugGL;
    glExtra->glBindBufferBase(GL_UNIFORM_BUFFER, bindingIndex, d->buffer.bufferId());debugGL;

}

void QuickSGUniformBuffer::bufferData(const QByteArray &data)
{
    Q_D(QuickSGUniformBuffer);

    bind();
    d->buffer.allocate(data.constData(), data.count());debugGL;

}

#include "quicksguniformbuffer.moc"


/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#ifndef QUICKSAMPLEROBJECT_H
#define QUICKSAMPLEROBJECT_H

#include <QScopedPointer>
#include <QOpenGLContext>
#include <QQuickItem>

class QuickSamplerObjectPrivate;

class QuickSamplerObject : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(Filter magFilter READ magFilter WRITE setMagFilter NOTIFY magFilterChanged)
    Q_PROPERTY(Filter minFilter READ minFilter WRITE setMinFilter NOTIFY minFilterChanged)
    Q_PROPERTY(double lodBias READ lodBias WRITE setLodBias NOTIFY lodBiasChanged)
    Q_PROPERTY(double maxLod READ maxLod WRITE setMaxLod NOTIFY maxLodChanged)
    Q_PROPERTY(double minLod READ minLod WRITE setMinLod NOTIFY minLodChanged)
    Q_PROPERTY(Wrap wrapS READ wrapS WRITE setwrapS NOTIFY wrapSChanged)
    Q_PROPERTY(Wrap wrapT READ wrapT WRITE setwrapT NOTIFY wrapTChanged)
    Q_PROPERTY(Wrap wrapR READ wrapR WRITE setwrapR NOTIFY wrapRChanged)
    Q_PROPERTY(QColor borderColor READ borderColor WRITE setBorderColor NOTIFY borderColorChanged)

    Q_PROPERTY(QQuickItem * source READ source WRITE setSource NOTIFY sourceChanged)

public:

    enum Filter
    {
        FilterNearest                 = 0x2600,   // GL_NEAREST
        FilterLinear                  = 0x2601,   // GL_LINEAR
        FilterNearestMipMapNearest    = 0x2700,   // GL_NEAREST_MIPMAP_NEAREST
        FilterNearestMipMapLinear     = 0x2702,   // GL_NEAREST_MIPMAP_LINEAR
        FilterLinearMipMapNearest     = 0x2701,   // GL_LINEAR_MIPMAP_NEAREST
        FilterLinearMipMapLinear      = 0x2703    // GL_LINEAR_MIPMAP_LINEAR
    };
    Q_ENUM(Filter)

    enum Wrap
    {
        WrapRepeat         = 0x2901, // GL_REPEAT
        WrapMirroredRepeat = 0x8370, // GL_MIRRORED_REPEAT
        WrapClampToEdge    = 0x812F, // GL_CLAMP_TO_EDGE
        WrapClampToBorder  = 0x812D  // GL_CLAMP_TO_BORDER
    };
    Q_ENUM(Wrap)

    QuickSamplerObject(QQuickItem *parent = 0);

    ~QuickSamplerObject();

    bool isTextureProvider() const override;
    QSGTextureProvider *textureProvider() const override;

    Filter magFilter() const;
    Filter minFilter() const;
    double lodBias() const;
    double maxLod() const;
    double minLod() const;
    Wrap wrapS() const;
    Wrap wrapT() const;
    Wrap wrapR() const;
    QColor borderColor() const;
    QQuickItem * source() const;


signals:

    void magFilterChanged(Filter magFilter);
    void minFilterChanged(Filter minFilter);
    void maxLodChanged(double maxLod);
    void minLodChanged(double minLod);
    void lodBiasChanged(double lodBias);
    void wrapSChanged(Wrap wrapS);
    void wrapTChanged(Wrap wrapT);
    void wrapRChanged(Wrap wrapR);
    void borderColorChanged(QColor borderColor);
    void sourceChanged(QQuickItem * source);

public slots:

    void setMagFilter(Filter magFilter);
    void setMinFilter(Filter minFilter);
    void setLodBias(double lodBias);
    void setMaxLod(double maxLod);
    void setMinLod(double minLod);
    void setwrapS(Wrap wrapS);
    void setwrapT(Wrap wrapT);
    void setwrapR(Wrap wrapR);
    void setBorderColor(QColor borderColor);
    void setSource(QQuickItem * source);

protected:

    QScopedPointer<QuickSamplerObjectPrivate> d_ptr;

    virtual void releaseResources() override;
    QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *) override;


private:

    Q_DECLARE_PRIVATE(QuickSamplerObject)

};


#endif // QUICKSAMPLEROBJECT_H

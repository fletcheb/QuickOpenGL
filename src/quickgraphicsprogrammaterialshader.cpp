/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include <QElapsedTimer>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLFunctions_4_0_Core>
#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLExtraFunctions>
#include "quickgraphicsprogrammaterialshader.h"
#include "quickgraphicsprogrammaterial.h"
#include "introspection.h"
#include "support.h"

class QuickGraphicsProgramMaterialShaderPrivate : public QObject
{
    Q_OBJECT


public:

    QuickGraphicsProgramMaterialShaderPrivate(QuickGraphicsProgramMaterialShader *q);

    // Shader source code
    QByteArray vertexShader;
    QByteArray fragmentShader;
    QByteArray geometryShader;
    QByteArray tessellationControlShader;
    QByteArray tessellationEvaluationShader;

    // Scene graph attribute names
    char **attributeNames;

    // State variables for activate / deactivate
    bool glLineSmooth;
    GLenum glBlendSrcRgb;
    GLenum glBlendSrcAlpha;
    GLenum glBlendDstRgb;
    GLenum glBlendDstAlpha;
    GLenum glColorLogicOp;
    GLenum glLogicOpMode;
    GLenum glPolygonMode;
    bool glProgramPointSize;
    float glPointFadeThresholdSize;
    int glPointSpriteCoordOrigin;
    bool glCullFace;
    GLenum glCullFaceMode;
    GLenum glFrontFace;
    GLenum glActiveTexture;
    GLint glFrameBufferBinding;
    GLenum glSamplerBinding;

    // Introspected properties
    QList<Introspection::Uniform> uniforms;
    QList<Introspection::UniformBlock> uniformBlocks;
    QList<Introspection::ShaderStorageBlock> shaderStorageBlocks;
    QList<Introspection::Attribute> attributes;
    QHash<GLuint, QList<Introspection::SubroutineUniform>> subroutineUniforms;

    int debugLevel;

    static bool beforeRenderingConnection;
    static bool afterRenderingConnection;
    static unsigned int frameId;

    QString qmlItemName;
    QQuickWindow *window;

#ifdef QT_DEBUG
    static QElapsedTimer debugTimer;
    static QHash<QuickGraphicsProgramMaterialShader *, QList<QPair<qint64, QString>>> debugEvents;
#endif

public slots:

    void onBeforeRendering();
    void onAfterRendering();

protected:

    QuickGraphicsProgramMaterialShader *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickGraphicsProgramMaterialShader)

};

bool QuickGraphicsProgramMaterialShaderPrivate::beforeRenderingConnection = false;
bool QuickGraphicsProgramMaterialShaderPrivate::afterRenderingConnection = false;
unsigned int QuickGraphicsProgramMaterialShaderPrivate::frameId = 0;

#ifdef QT_DEBUG
QElapsedTimer QuickGraphicsProgramMaterialShaderPrivate::debugTimer;
QHash<QuickGraphicsProgramMaterialShader *, QList<QPair<qint64, QString>>> QuickGraphicsProgramMaterialShaderPrivate::debugEvents;
#endif

QuickGraphicsProgramMaterialShaderPrivate::QuickGraphicsProgramMaterialShaderPrivate(QuickGraphicsProgramMaterialShader *q) :
    attributeNames(nullptr),
    glLineSmooth(false),
    glBlendSrcRgb(GL_ONE),
    glBlendSrcAlpha(GL_ONE),
    glBlendDstRgb(GL_ZERO),
    glBlendDstAlpha(GL_ZERO),
    glColorLogicOp(false),
    glLogicOpMode(GL_COPY),
    glPolygonMode(GL_FILL),
    glProgramPointSize(false),
    glPointFadeThresholdSize(1.0f),
    glPointSpriteCoordOrigin(GL_LOWER_LEFT),
    glCullFace(false),
    glCullFaceMode(GL_BACK),
    glFrontFace(GL_CCW),
    glActiveTexture(0),
    glFrameBufferBinding(0),
    glSamplerBinding(0),
    debugLevel(QuickOpenGL::debugLevel()),
    window(nullptr),
    q_ptr(q)
{
#ifdef QT_DEBUG
    debugTimer.start();
#endif

}

void QuickGraphicsProgramMaterialShaderPrivate::onBeforeRendering()
{
#ifdef QT_DEBUG
    debugEvents.clear();
    debugTimer.restart();
#endif
}

void QuickGraphicsProgramMaterialShaderPrivate::onAfterRendering()
{
#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        qint64 elapsed = debugTimer.nsecsElapsed();

        QMap<QString, QMap<QString, QList<double>>> results;

        for(auto it=debugEvents.constBegin(); it!=debugEvents.constEnd();++it)
        {
            QMap<QString, QList<double>> result;
            QHash<QString, qint64> hash;

            for(auto event: it.value())
            {
                if(hash.contains(event.second))
                {
                    result[event.second].append(double(event.first-hash[event.second])*1e-6);
                    hash.remove(event.second);
                }
                else
                {
                    hash[event.second]=event.first;
                }
            }

            QString name = it.key()->qmlItemName();
            for(int i=0;results.contains(name);i++)
            {
                name = QString("%1_%2").arg(it.key()->qmlItemName()).arg(i);
            }
            results[name] = result;
        }


        qDebug() << "quickopengl.scenegraph.time.graphicsprogram.materialshader: frame" << frameId << "total scenegraph render time" << double(elapsed)*1e-6 << "ms ";

        if(results.count())
        {
            double totalSum = 0;

            QString string;
            QTextStream stream(&string);

            stream << qSetFieldWidth(40) << "QuickOpenGL QML Item" << qSetFieldWidth(10) << "Calls";
            for(const QString &field: results.first().keys())
            {
                stream << qSetFieldWidth(16) << field;
            }
            stream << "Total";
            stream << Qt::endl;

            for(auto result = results.begin(); result!= results.end(); ++result)
            {
                stream << qSetFieldWidth(40)  << result.key();

                stream << qSetFieldWidth(10) << result->first().count() << qSetFieldWidth(16);

                double sum = 0;
                for(auto it = result->begin(); it!=result->end();++it)
                {
                    double total = 0;
                    for(double value: it.value())
                    {
                        total += value;
                    }

                    sum += total;

                    stream << total;
                }
                totalSum += sum;
                stream << sum;
                stream << Qt::endl;
            }

            stream << qSetPadChar('-') <<  qSetFieldWidth(40) << "" << qSetFieldWidth(10) << "";
            for(int i=0;i<results.first().count();++i)
            {
                stream << qSetFieldWidth(16) << "";
            }

            stream << qSetPadChar(' ') << totalSum;
            qDebug().nospace() << qPrintable(string);
        }
    }
    debugEvents.clear();
#endif
    frameId++;
}


QuickGraphicsProgramMaterialShader::QuickGraphicsProgramMaterialShader() :
  d_ptr(new QuickGraphicsProgramMaterialShaderPrivate(this))
{

}

QuickGraphicsProgramMaterialShader::~QuickGraphicsProgramMaterialShader()
{
    Q_D(QuickGraphicsProgramMaterialShader);

    if(d->attributeNames)
    {
        for(int i=0;d->attributeNames[i];++i)
        {
            delete[] d->attributeNames[i];
        }
        delete[] d->attributeNames;
    }

}

void QuickGraphicsProgramMaterialShader::setVertexShader(const QByteArray &vertexShader)
{
    Q_D(QuickGraphicsProgramMaterialShader);

    d->vertexShader = vertexShader;

}

void QuickGraphicsProgramMaterialShader::setFragmentShader(const QByteArray &fragmentShader)
{
    Q_D(QuickGraphicsProgramMaterialShader);

    d->fragmentShader = fragmentShader;

}

void QuickGraphicsProgramMaterialShader::setGeometryShader(const QByteArray &geometryShader)
{
    Q_D(QuickGraphicsProgramMaterialShader);

    d->geometryShader = geometryShader;

}

void QuickGraphicsProgramMaterialShader::setTessellationControlShader(const QByteArray &tessellationControlShader)
{
    Q_D(QuickGraphicsProgramMaterialShader);

    d->tessellationControlShader = tessellationControlShader;

}

void QuickGraphicsProgramMaterialShader::setTessellationEvaluationShader(const QByteArray &tessellationEvaluationShader)
{
    Q_D(QuickGraphicsProgramMaterialShader);

    d->tessellationEvaluationShader = tessellationEvaluationShader;

}

void QuickGraphicsProgramMaterialShader::setQmlItem(const QObject *qmlItem)
{
    Q_D(QuickGraphicsProgramMaterialShader);

    d->qmlItemName = qmlItem->objectName().isEmpty()?qmlItem->metaObject()->className():qmlItem->objectName();

}

QString QuickGraphicsProgramMaterialShader::qmlItemName() const
{
    Q_D(const QuickGraphicsProgramMaterialShader);

    return d->qmlItemName;
}

void QuickGraphicsProgramMaterialShader::setQQuickWindow(QQuickWindow *window)
{
    Q_D(QuickGraphicsProgramMaterialShader);
    d->window = window;

    if(!QuickGraphicsProgramMaterialShaderPrivate::beforeRenderingConnection)
    {
        QuickGraphicsProgramMaterialShaderPrivate::beforeRenderingConnection = QObject::connect(d->window, &QQuickWindow::beforeRendering, d, &QuickGraphicsProgramMaterialShaderPrivate::onBeforeRendering, Qt::DirectConnection);
    }

    if(!QuickGraphicsProgramMaterialShaderPrivate::afterRenderingConnection)
    {
        QuickGraphicsProgramMaterialShaderPrivate::afterRenderingConnection = QObject::connect(d->window, &QQuickWindow::afterRendering, d, &QuickGraphicsProgramMaterialShaderPrivate::onAfterRendering, Qt::DirectConnection);
    }

}

void QuickGraphicsProgramMaterialShader::setAttributeNames(const QList<Introspection::Attribute> &attributes)

{
    Q_D(QuickGraphicsProgramMaterialShader);

    // determine the number of attribute tuples
    int tuples = 0;
    for(int i=0;i<attributes.count();++i)
    {
        tuples += QuickOpenGL::tupleCount(attributes[i].type)*attributes[i].arraySize;
    }

    // allocate memory for attribute names
    d->attributeNames = new char *[tuples+1];
    int tuple = 0;

    // for each attribute
    for(int i=0;i<attributes.count();i++)
    {
        // for each tuple in the attribute
        for(int j=0;j<QuickOpenGL::tupleCount(attributes[i].type)*attributes[i].arraySize;++j)
        {
            // allocate memory
            d->attributeNames[tuple] = new char[attributes.at(i).name.count() + (j==0?1:3)];

            // copy the name
            memcpy(&d->attributeNames[tuple][0], attributes.at(i).name.constData(), size_t(attributes.at(i).name.count()));

            // if this is the first tuple, use the base name
            if(j==0)
            {
                d->attributeNames[tuple][attributes.at(i).name.count()]=0;
            }
            // this is not the first tuple, use the base name + _A, _B, _C...
            else
            {
                d->attributeNames[tuple][attributes.at(i).name.count()]='_';
                d->attributeNames[tuple][attributes.at(i).name.count()+1]=char('A'+(j-1));
                d->attributeNames[tuple][attributes.at(i).name.count()+2]=0;
            }

            ++tuple;
        }
    }

    d->attributeNames[tuples] = nullptr;

}


void QuickGraphicsProgramMaterialShader::activate()
{
    Q_D(QuickGraphicsProgramMaterialShader);

    debugGLquiet;

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("activate")));
    }
#endif

    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();

    //
    // Get the state of things we may change that are not controlled by the scene graph
    //
    gl->glGetIntegerv(GL_BLEND_SRC_RGB, reinterpret_cast<int *>(&d->glBlendSrcRgb));debugGL;
    gl->glGetIntegerv(GL_BLEND_SRC_ALPHA, reinterpret_cast<int *>(&d->glBlendSrcAlpha));debugGL;
    gl->glGetIntegerv(GL_BLEND_DST_RGB, reinterpret_cast<int *>(&d->glBlendDstRgb));debugGL;
    gl->glGetIntegerv(GL_BLEND_DST_ALPHA, reinterpret_cast<int *>(&d->glBlendDstAlpha));debugGL;
    d->glCullFace = gl->glIsEnabled(GL_CULL_FACE);debugGL;
    gl->glGetIntegerv(GL_CULL_FACE_MODE, reinterpret_cast<int *>(&d->glCullFaceMode));debugGL;
    gl->glGetIntegerv(GL_FRONT_FACE, reinterpret_cast<int *>(&d->glFrontFace));debugGL;
    gl->glGetIntegerv(GL_ACTIVE_TEXTURE, reinterpret_cast<int *>(&d->glActiveTexture));debugGL;
    gl->glGetIntegerv(GL_SAMPLER_BINDING, reinterpret_cast<int *>(&d->glSamplerBinding));debugGL;
    gl->glGetIntegerv(GL_FRAMEBUFFER_BINDING, &d->glFrameBufferBinding);debugGL;

#ifndef QT_OPENGL_ES
    d->glLineSmooth = gl->glIsEnabled(GL_LINE_SMOOTH);debugGL;
    d->glColorLogicOp = gl->glIsEnabled(GL_COLOR_LOGIC_OP);debugGL;
    gl->glGetIntegerv(GL_LOGIC_OP_MODE, reinterpret_cast<int *>(&d->glLogicOpMode));debugGL;
    gl->glGetIntegerv(GL_POLYGON_MODE, reinterpret_cast<int *>(&d->glPolygonMode));debugGL;
    d->glProgramPointSize = gl->glIsEnabled(GL_PROGRAM_POINT_SIZE);debugGL;
    gl->glGetFloatv(GL_POINT_FADE_THRESHOLD_SIZE, &d->glPointFadeThresholdSize);debugGL;
    gl->glGetIntegerv(GL_POINT_SPRITE_COORD_ORIGIN, &d->glPointSpriteCoordOrigin);debugGL;
#endif

    QSGMaterialShader::activate();

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("activate")));
    }
#endif

}

void QuickGraphicsProgramMaterialShader::deactivate()
{
    Q_D(QuickGraphicsProgramMaterialShader);

    debugGLquiet;

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("deactivate")));
    }
#endif


    QOpenGLExtraFunctions *gl = QOpenGLContext::currentContext()->extraFunctions();
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();
    //
    // Return the state of things that we may have changed to their values when we were activated
    //
    d->glCullFace?gl->glEnable(GL_CULL_FACE):gl->glDisable(GL_CULL_FACE);debugGL;
    gl->glCullFace(d->glCullFaceMode);debugGL;
    gl->glFrontFace(d->glFrontFace);debugGL;
    gl->glActiveTexture(d->glActiveTexture);debugGL;
    gl->glBindSampler(d->glActiveTexture-GL_TEXTURE0, d->glSamplerBinding);debugGL;

#ifndef QT_OPENGL_ES
    d->glProgramPointSize?gl->glEnable(GL_PROGRAM_POINT_SIZE):gl->glDisable(GL_PROGRAM_POINT_SIZE);debugGL;
    d->glLineSmooth?gl->glEnable(GL_LINE_SMOOTH):gl->glDisable(GL_LINE_SMOOTH);debugGL;
    gl->glBlendFuncSeparate(d->glBlendSrcRgb, d->glBlendDstRgb, d->glBlendSrcAlpha, d->glBlendDstAlpha);debugGL;
    d->glColorLogicOp?gl->glEnable(GL_COLOR_LOGIC_OP):gl->glDisable(GL_COLOR_LOGIC_OP);debugGL;

    // Get the OpenGL functions object
    QOpenGLFunctions_3_3_Core *gl33 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_3_3_Core>();debugGL;
    Q_ASSERT(gl33);
    bool ok = gl33->initializeOpenGLFunctions();
    Q_ASSERT(ok);
    gl33->glLogicOp(d->glLogicOpMode);debugGL;
    gl33->glPolygonMode(GL_FRONT_AND_BACK, d->glPolygonMode);debugGL;
    gl33->glPointParameterf(GL_POINT_FADE_THRESHOLD_SIZE, d->glPointFadeThresholdSize);debugGL;
    gl33->glPointParameteri(GL_POINT_SPRITE_COORD_ORIGIN, d->glPointSpriteCoordOrigin);debugGL;
#endif
    QSGMaterialShader::deactivate();

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("deactivate")));
    }
#endif

}

void QuickGraphicsProgramMaterialShader::updateState(const QSGMaterialShader::RenderState &state, QSGMaterial *newMaterial, QSGMaterial *oldMaterial)
{
    Q_D(QuickGraphicsProgramMaterialShader);

    debugGLquiet;

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("updateState")));
    }
#endif

#ifndef QT_OPENGL_ES
    // Get the OpenGL functions object
    QOpenGLFunctions_3_3_Core *gl33 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_3_3_Core>();debugGL;
    Q_ASSERT(gl33);
    bool ok = gl33->initializeOpenGLFunctions();
    Q_ASSERT(ok);
#endif

    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();debugGL;
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();

    Q_ASSERT(oldMaterial == nullptr || newMaterial->type() == oldMaterial->type());

    QuickGraphicsProgramMaterial *material = static_cast<QuickGraphicsProgramMaterial *>(newMaterial);

    // add the Qt scene graph intrinsic uniform properties - uniforms set later
    material->properties[QStringLiteral("qt_Matrix")] = state.combinedMatrix();
    material->properties[QStringLiteral("qt_CombinedMatrix")] = state.combinedMatrix();
    material->properties[QStringLiteral("qt_ModelViewMatrix")] = state.modelViewMatrix();
    material->properties[QStringLiteral("qt_ProjectionMatrix")] = state.projectionMatrix();
    material->properties[QStringLiteral("qt_Opacity")] = state.opacity();
    material->properties[QStringLiteral("qt_ViewPort")] = state.viewportRect();
    material->properties[QStringLiteral("qt_FrameID")] = QuickGraphicsProgramMaterialShaderPrivate::frameId;

    // note: point size and line width is set by the scene graph after we have been called.  They are material properties.
#ifndef QT_OPENGL_ES
    // set line smoothing state
    Q_ASSERT(material->properties.contains(QStringLiteral("lineSmooth")));
    if(material->properties[QStringLiteral("lineSmooth")].toBool())
    {
        glExtra->glEnable(GL_LINE_SMOOTH);debugGL;
    }
    else
    {
        glExtra->glDisable(GL_LINE_SMOOTH);debugGL;
    }
#endif

    // set the blending state
    Q_ASSERT(material->properties.contains(QStringLiteral("blend")));
    if(material->properties[QStringLiteral("blend")].toBool()||state.opacity()<1.0f)
    {
        glExtra->glEnable(GL_BLEND);debugGL;

        Q_ASSERT(material->properties.contains(QStringLiteral("blendSrcRgb")));
        Q_ASSERT(material->properties.contains(QStringLiteral("blendDstRgb")));
        Q_ASSERT(material->properties.contains(QStringLiteral("blendSrcAlpha")));
        Q_ASSERT(material->properties.contains(QStringLiteral("blendDstAlpha")));

        GLenum sfactorRGB = material->properties[QStringLiteral("blendSrcRgb")].toUInt();
        GLenum dfactorRGB = material->properties[QStringLiteral("blendDstRgb")].toUInt();
        GLenum sfactorAlpha = material->properties[QStringLiteral("blendSrcAlpha")].toUInt();
        GLenum dfactorAlpha = material->properties[QStringLiteral("blendDstAlpha")].toUInt();

        glExtra->glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);debugGL;
    }

#ifndef QT_OPENGL_ES
    // set the logic state
    Q_ASSERT(material->properties.contains(QStringLiteral("logicOp")));
    if(material->properties[QStringLiteral("logicOp")].toInt()>0)
    {
        glExtra->glEnable(GL_COLOR_LOGIC_OP);debugGL;
        gl33->glLogicOp(material->properties[QStringLiteral("logicOp")].toUInt());debugGL;
    }
    else
    {
        glExtra->glDisable(GL_COLOR_LOGIC_OP);debugGL;
    }

    // set the polygon mode state
    Q_ASSERT(material->properties.contains(QStringLiteral("polygonMode")));
    gl33->glPolygonMode(GL_FRONT_AND_BACK, material->properties[QStringLiteral("polygonMode")].toUInt());debugGL;

    // set the point fade threshold size state
    Q_ASSERT(material->properties.contains(QStringLiteral("pointFadeThresholdSize")));
    gl33->glPointParameterf(GL_POINT_FADE_THRESHOLD_SIZE, material->properties[QStringLiteral("pointFadeThresholdSize")].toFloat());debugGL;

    // set the point sprite coordinate origin
    Q_ASSERT(material->properties.contains(QStringLiteral("pointSpriteCoordOrigin")));
    gl33->glPointParameteri(GL_POINT_SPRITE_COORD_ORIGIN, material->properties[QStringLiteral("pointSpriteCoordOrigin")].toInt());debugGL;

    // set the program point size state
    Q_ASSERT(material->properties.contains(QStringLiteral("programPointSize")));
    if(material->properties.contains(QStringLiteral("programPointSize")))
    {
        glExtra->glEnable(GL_PROGRAM_POINT_SIZE);debugGL;
    }
    else
    {
        glExtra->glDisable(GL_PROGRAM_POINT_SIZE);debugGL;
    }
#endif

    // set the cull mode state
    Q_ASSERT(material->properties.contains(QStringLiteral("cullFace")));
    if(material->properties[QStringLiteral("cullFace")].toInt())
    {
        glExtra->glEnable(GL_CULL_FACE);debugGL;
        glExtra->glCullFace(material->properties[QStringLiteral("cullFace")].toUInt());debugGL;
    }
    else
    {
        glExtra->glDisable(GL_CULL_FACE);debugGL;
    }

    // set the front face state
    Q_ASSERT(material->properties.contains(QStringLiteral("frontFace")));

    uint frontFace = material->properties[QStringLiteral("frontFace")].toUInt();

    // if we have a texture bound to the frame buffer (layer.enabled=true somewhere in our parent
    // hierarchy), swap our front face winding order. If we don't do this, faces point in opposite
    // direction to when we are rendering into the default framebuffer - not sure why, but suspect
    // the scene graph flips the y axis when rendering into a texture.
    if(d->glFrameBufferBinding!=0)
    {
        GLint frameBufferType = GL_NONE;
        glExtra->glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &frameBufferType);debugGL;
        if(frameBufferType==GL_TEXTURE)
        {
            frontFace  = frontFace ==GL_CW?GL_CCW:GL_CW;
        }
    }

    glExtra->glFrontFace(frontFace);debugGL;

#ifndef QT_OPENGL_ES

    // if we are dealing with geometry which contains patches
    Q_ASSERT(material->properties.contains("patchVertices"));
    if(material->properties["patchVertices"].toInt()>0)
    {
        QOpenGLFunctions_4_0_Core *gl40 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_0_Core>();
        if(gl40&&gl40->initializeOpenGLFunctions())
        {
            gl40->glPatchParameteri(GL_PATCH_VERTICES, material->properties["patchVertices"].toInt());debugGL;

            Q_ASSERT(material->properties.contains("patchDefaultOuterLevel")&&material->properties["patchDefaultOuterLevel"].canConvert<QVariantList>());
            QVariantList variantList = material->properties["patchDefaultOuterLevel"].value<QVariantList>();
            QVector<float> level(4, 0.0f);
            for(int i=0;i<std::min(variantList.count(), 4);++i)
            {
                level[i] = variantList.at(i).toFloat();
            }
            gl40->glPatchParameterfv(GL_PATCH_DEFAULT_OUTER_LEVEL, level.constData());debugGL;

            Q_ASSERT(material->properties.contains("patchDefaultInnerLevel")&&material->properties["patchDefaultInnerLevel"].canConvert<QVariantList>());
            variantList = material->properties["patchDefaultInnerLevel"].value<QVariantList>();
            level.fill(0.0f, 2);
            for(int i=0;i<std::min(variantList.count(), 2);++i)
            {
                 level[i] = variantList.at(i).toFloat();
            }
            gl40->glPatchParameterfv(GL_PATCH_DEFAULT_INNER_LEVEL, level.constData());debugGL;
        }
        else
        {
            Q_ASSERT(material->properties.contains("QmlItem")&&material->properties["QmlItem"].canConvert<QObject *>());
            qWarning() << "QuickOpenGL:" << material->properties["QmlItem"].value<QObject *>() << "OpenGL 4 required for tessellation shaders. Current context is OpenGL" << qPrintable(QString("%1.%2").arg(QOpenGLContext::currentContext()->format().version().first).arg(QOpenGLContext::currentContext()->format().version().second));
        }
    }
#endif


    Q_ASSERT(material->properties.contains("memoryBarrier"));
    if(material->properties["memoryBarrier"].toUInt())
    {
        glExtra->glMemoryBarrier(material->properties["memoryBarrier"].toUInt());debugGL;
    }

    QSGMaterialShader::updateState(state, newMaterial, oldMaterial);

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("updateState")));
    }
#endif

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("uniforms")));
    }
#endif

    Introspection::setUniforms(d->uniforms, material->properties);

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("uniforms")));
    }
#endif

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("uniformBlocks")));
    }
#endif

    Introspection::setUniformBlocks(d->uniformBlocks, material->properties, program()->programId());

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("uniformBlocks")));
    }
#endif

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("ssbos")));
    }
#endif

    Introspection::setShaderStorageBlocks(d->shaderStorageBlocks, material->properties);

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("ssbos")));
    }
#endif

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("subroutines")));
    }
#endif

#ifndef QT_OPENGL_ES
    Introspection::setSubroutineUniforms(GL_VERTEX_SHADER, d->subroutineUniforms[GL_VERTEX_SHADER], material->properties);
    Introspection::setSubroutineUniforms(GL_TESS_CONTROL_SHADER,  d->subroutineUniforms[GL_TESS_CONTROL_SHADER], material->properties);
    Introspection::setSubroutineUniforms(GL_TESS_EVALUATION_SHADER,  d->subroutineUniforms[GL_TESS_EVALUATION_SHADER], material->properties);
    Introspection::setSubroutineUniforms(GL_GEOMETRY_SHADER, d->subroutineUniforms[GL_GEOMETRY_SHADER], material->properties);
    Introspection::setSubroutineUniforms(GL_FRAGMENT_SHADER, d->subroutineUniforms[GL_FRAGMENT_SHADER], material->properties);
#endif

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QuickGraphicsProgramMaterialShaderPrivate::debugEvents[this].append(qMakePair(d->debugTimer.nsecsElapsed(), QStringLiteral("subroutines")));
    }
#endif

}

const char * const *QuickGraphicsProgramMaterialShader::attributeNames() const
{
    Q_D(const QuickGraphicsProgramMaterialShader);
    return d->attributeNames;
}

void QuickGraphicsProgramMaterialShader::compile()
{

    Q_D(QuickGraphicsProgramMaterialShader);

    if(!d->geometryShader.isEmpty())
    {
        program()->addCacheableShaderFromSourceCode(QOpenGLShader::Geometry, d->geometryShader);
    }
    if(!d->tessellationControlShader.isEmpty())
    {
        program()->addCacheableShaderFromSourceCode(QOpenGLShader::TessellationControl, d->tessellationControlShader);
    }
    if(!d->tessellationEvaluationShader.isEmpty())
    {
        program()->addCacheableShaderFromSourceCode(QOpenGLShader::TessellationEvaluation, d->tessellationEvaluationShader);
    }

    //
    // Don't call QSGMaterialShader::compile() - it does not know how to deal with attributes with more than one tuple.
    //
    program()->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, vertexShader());
    program()->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShader());

    int maxVertexAttribs = 0;
    QOpenGLFunctions *funcs = QOpenGLContext::currentContext()->functions();debugGL;
    funcs->glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxVertexAttribs);
    for (int i = 0; i<d->attributes.count(); ++i) {
        if (i >= maxVertexAttribs) {
            qFatal("List of attribute names is either too long or not null-terminated.\n"
                   "Maximum number of attributes on this hardware is %i.\n"
                   "Vertex shader:\n%s\n"
                   "Fragment shader:\n%s\n",
                   maxVertexAttribs, vertexShader(), fragmentShader());
        }

        program()->bindAttributeLocation(d->attributes.at(i).name, d->attributes.at(i).location);
    }

    if (!program()->link()) {
        qWarning("QSGMaterialShader: Shader compilation failed:");
        qWarning() << program()->log();
    }

    //
    // End doing what QSGMaterialShader::compile() does
    //

    d->uniforms = Introspection::activeUniforms(program()->programId());

    int version = QOpenGLContext::currentContext()->format().majorVersion()*100+QOpenGLContext::currentContext()->format().minorVersion()*10;


    // qmlItem gives us some context for debug messages
    d->uniformBlocks = Introspection::activeUniformBlocks(program()->programId());
    if((version>=430) || ((version>=310)&&QOpenGLContext::currentContext()->isOpenGLES()))
    {
        d->shaderStorageBlocks = Introspection::activeShaderStorageBlocks(program()->programId());
    }
    if(version>=400)
    {
        d->subroutineUniforms[GL_VERTEX_SHADER] = Introspection::activeSubroutineUniforms(program()->programId(), GL_VERTEX_SHADER);
        d->subroutineUniforms[GL_TESS_CONTROL_SHADER] = Introspection::activeSubroutineUniforms(program()->programId(), GL_TESS_CONTROL_SHADER);
        d->subroutineUniforms[GL_TESS_EVALUATION_SHADER] = Introspection::activeSubroutineUniforms(program()->programId(), GL_TESS_EVALUATION_SHADER);
        d->subroutineUniforms[GL_GEOMETRY_SHADER] = Introspection::activeSubroutineUniforms(program()->programId(), GL_GEOMETRY_SHADER);
        d->subroutineUniforms[GL_FRAGMENT_SHADER] = Introspection::activeSubroutineUniforms(program()->programId(), GL_FRAGMENT_SHADER);
    }
}

void QuickGraphicsProgramMaterialShader::initialize()
{

    QSGMaterialShader::initialize();
}

const char *QuickGraphicsProgramMaterialShader::vertexShader() const
{
    Q_D(const QuickGraphicsProgramMaterialShader);

    return d->vertexShader;
}

const char *QuickGraphicsProgramMaterialShader::fragmentShader() const
{
    Q_D(const QuickGraphicsProgramMaterialShader);

    return d->fragmentShader;

}

#include "quickgraphicsprogrammaterialshader.moc"

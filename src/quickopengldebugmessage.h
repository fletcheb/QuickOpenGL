/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKOPENGLDEBUGMESSAGE_H
#define QUICKOPENGLDEBUGMESSAGE_H

#include <QOpenGLDebugMessage>


class QuickOpenGLDebugMessage : QOpenGLDebugMessage
{
    Q_GADGET

    Q_PROPERTY(Source source READ source)
    Q_PROPERTY(Type type READ type)
    Q_PROPERTY(Severity severity READ severity)
    Q_PROPERTY(int id READ id)
    Q_PROPERTY(QString message READ message)

public:

    enum Source {
        SourceInvalid        = 0x00000000,
        SourceAPI            = 0x00000001,
        SourceWindowSystem  = 0x00000002,
        SourceShaderCompiler = 0x00000004,
        SourceThirdParty    = 0x00000008,
        SourceApplication   = 0x00000010,
        SourceOther         = 0x00000020,
        SourceLast          = OtherSource, // private API
        SourceAny           = 0xffffffff
    };
    Q_ENUM(Source)
    Q_DECLARE_FLAGS(Sources, Source)
    Q_FLAG(Sources)

    enum Type {
        TypeInvalid           = 0x00000000,
        TypeError             = 0x00000001,
        TypeDeprecatedBehavior= 0x00000002,
        TypeUndefinedBehavior = 0x00000004,
        TypePortability       = 0x00000008,
        TypePerformance       = 0x00000010,
        TypeOther             = 0x00000020,
        TypeMarker            = 0x00000040,
        TypeGroupPush         = 0x00000080,
        TypeGroupPop          = 0x00000100,
        TypeLast              = GroupPopType, // private API
        TypeAny               = 0xffffffff
    };
    Q_ENUM(Type)
    Q_DECLARE_FLAGS(Types, Type)
    Q_FLAG(Types)

    enum Severity {
        SeverityInvalid     = 0x00000000,
        SeverityHigh        = 0x00000001,
        SeverityMedium      = 0x00000002,
        SeverityLow         = 0x00000004,
        SeverityNotification= 0x00000008,
        SeverityLast        = NotificationSeverity, // private API
        SeverityAny         = 0xffffffff
    };
    Q_ENUM(Severity)
    Q_DECLARE_FLAGS(Severities, Severity)
    Q_FLAG(Severities)

    QuickOpenGLDebugMessage();
    QuickOpenGLDebugMessage(const QOpenGLDebugMessage &other);

    Source source() const;
    Type type() const;
    Severity severity() const;
};


Q_DECLARE_METATYPE(QuickOpenGLDebugMessage)

#endif // QUICKOPENGLDEBUGMESSAGE_H

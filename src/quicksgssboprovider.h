/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKSHADERSTORAGEBUFFEROBJECTPROVIDER_H
#define QUICKSHADERSTORAGEBUFFEROBJECTPROVIDER_H

#include <QObject>
#include <QScopedPointer>
#include "introspection.h"
#include "quickshaderstoragebuffer.h"

class QuickSGSSBO;
class QuickSGSSBOProviderPrivate;

class QuickSGSSBOProvider : public QObject
{
    Q_OBJECT

public:

    QuickSGSSBOProvider();
    virtual ~QuickSGSSBOProvider();

    void setData(const QByteArray &data);
    QByteArray data() const;

    void setUpdated(bool updated);
    bool updated() const;

    void setSyncToOpenGL(QuickShaderStorageBuffer::SyncWrite syncToOpenGL);
    QuickShaderStorageBuffer::SyncWrite syncToOpenGL() const;

    void setSyncFromOpenGL(bool syncFromOpenGL);
    bool syncFromOpenGL() const;

    void setSize(int size);
    int size() const;

    void setUsagePattern(QuickShaderStorageBuffer::UsagePattern usagePattern);
    QuickShaderStorageBuffer::UsagePattern usagePattern() const;

    virtual QuickSGSSBO *ssbo();


    bool setShaderStorageBlock(const Introspection::ShaderStorageBlock &shaderStorageBlock);
    Introspection::ShaderStorageBlock shaderStorageBlock() const;

public slots:

    void onAfterRendering();

signals:

    void ssboChanged();
    void ssboVariables(QVariantMap data);
    void ssboData(QByteArray data);

protected:

    QScopedPointer<QuickSGSSBOProviderPrivate> d_ptr;

private:

    Q_DECLARE_PRIVATE(QuickSGSSBOProvider)

};

Q_DECLARE_METATYPE(QuickSGSSBOProvider *)

#endif // QUICKSHADERSTORAGEBUFFEROBJECTPROVIDER_H

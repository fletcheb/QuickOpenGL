/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKSGTEXTURE_H
#define QUICKSGTEXTURE_H

#include <QOpenGLTexture>
#include <QFutureWatcher>
#include <QSGTexture>
#include <QFuture>


#include "quicktexture.h"

class QuickSGTexture : public QSGTexture
{
    Q_OBJECT

    Q_PROPERTY(QuickTexture::Target target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(QuickTexture::Format format READ format WRITE setFormat NOTIFY formatChanged)
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(int depth READ depth WRITE setDepth NOTIFY depthChanged)
    Q_PROPERTY(int layers READ layers WRITE setLayers NOTIFY layersChanged)
    Q_PROPERTY(int mipLevels READ mipLevels WRITE setMipLevels NOTIFY mipLevelsChanged)
    Q_PROPERTY(int imageLayer READ imageLayer WRITE setImageLayer NOTIFY imageLayerChanged)
    Q_PROPERTY(int imageLevel READ imageLevel WRITE setImageLevel NOTIFY imageLevelChanged)
    Q_PROPERTY(QuickTexture::Access imageAccess READ imageAccess WRITE setImageAccess NOTIFY imageAccessChanged)
    Q_PROPERTY(QuickTexture::Wrap wrapS READ wrapS WRITE setWrapS NOTIFY wrapSChanged)
    Q_PROPERTY(QuickTexture::Wrap wrapT READ wrapT WRITE setWrapT NOTIFY wrapTChanged)
    Q_PROPERTY(QuickTexture::Wrap wrapR READ wrapR WRITE setWrapR NOTIFY wrapRChanged)
    Q_PROPERTY(QuickTexture::Filter magFilter READ magFilter WRITE setMagFilter NOTIFY magFilterChanged)
    Q_PROPERTY(QuickTexture::Filter minFilter READ minFilter WRITE setMinFilter NOTIFY minFilterChanged)
    Q_PROPERTY(double lodBias READ lodBias WRITE setLodBias NOTIFY lodBiasChanged)
    Q_PROPERTY(double maxLod READ maxLod WRITE setMaxLod NOTIFY maxLodChanged)
    Q_PROPERTY(double minLod READ minLod WRITE setMinLod NOTIFY minLodChanged)
    Q_PROPERTY(QColor borderColor READ borderColor WRITE setBorderColor NOTIFY borderColorChanged)
    Q_PROPERTY(bool zeroTexture READ zeroTexture WRITE setZeroTexture NOTIFY zeroTextureChanged)    

public:

    QuickSGTexture();
    ~QuickSGTexture() override;

    // Overrides
    int textureId() const override;
    QSize textureSize() const override;
    bool hasAlphaChannel() const override;
    bool hasMipmaps() const override;
    void bind() override;

    // Properties
    QuickTexture::Target target() const;
    QuickTexture::Format format() const;
    int width() const;
    int height() const;
    int depth() const;
    int layers() const;
    int imageLayer() const;
    QuickTexture::Access imageAccess() const;
    int mipLevels() const;
    QuickTexture::Wrap wrapS() const;
    QuickTexture::Wrap wrapT() const;
    QuickTexture::Wrap wrapR() const;
    QuickTexture::Filter magFilter() const;
    QuickTexture::Filter minFilter() const;
    double lodBias() const;
    double maxLod() const;
    bool zeroTexture() const;
    double minLod() const;
    QColor borderColor() const;
    int imageLevel() const;

    QFuture<QList<QByteArray>> getTextureData();
    void setTextureData(const QList<QByteArray> &data);
    void setFilename(const QString &filename);

public slots:

    void onBeforeSynchronizing();

    void setTarget(QuickTexture::Target target);
    void setFormat(QuickTexture::Format format);
    void setWidth(int width);
    void setHeight(int height);
    void setDepth(int depth);
    void setLayers(int layers);
    void setImageLayer(int imageLayer);
    void setImageAccess(QuickTexture::Access imageAccess);
    void setMipLevels(int mipLevels);
    void setWrapS(QuickTexture::Wrap wrapS);
    void setWrapT(QuickTexture::Wrap wrapT);
    void setWrapR(QuickTexture::Wrap wrapR);
    void setMagFilter(QuickTexture::Filter magFilter);
    void setMinFilter(QuickTexture::Filter minFilter);
    void setLodBias(double lodBias);
    void setMinLod(double minLod);
    void setMaxLod(double maxLod);
    void setZeroTexture(bool zeroTexture);
    void setBorderColor(QColor borderColor);
    void setImageLevel(int imageLevel);
    void textureCreated();


signals:

    void targetChanged(QuickTexture::Target target);
    void formatChanged(QuickTexture::Format format);
    void widthChanged(int width);
    void heightChanged(int height);
    void depthChanged(int depth);
    void layersChanged(int layers);
    void imageLayerChanged(int imageLayer);
    void imageAccessChanged(QuickTexture::Access imageAccess);
    void mipLevelsChanged(int mipLevels);
    void wrapSChanged(QuickTexture::Wrap wrapS);
    void wrapTChanged(QuickTexture::Wrap wrapT);
    void wrapRChanged(QuickTexture::Wrap wrapR);
    void magFilterChanged(QuickTexture::Filter magFilter);
    void minFilterChanged(QuickTexture::Filter minFilter);
    void lodBiasChanged(double lodBias);
    void maxLodChanged(double maxLod);
    void zeroTextureChanged(bool zeroTexture);
    void minLodChanged(double minLod);
    void borderColorChanged(QColor borderColor);
    void imageLevelChanged(int imageLevel);

protected slots:

    void update();

protected:

    void updateTexture();

    QOpenGLTexture *m_texture;

    QuickTexture::Target m_target;
    QuickTexture::Format m_format;
    int m_width;
    int m_height;
    int m_depth;
    int m_layers;
    bool m_update;
    int m_imageLayer;
    int m_imageLevel;
    QuickTexture::Access m_imageAccess;
    int m_mipLevels;
    QuickTexture::Wrap m_wrapS;
    QuickTexture::Wrap m_wrapT;
    QuickTexture::Wrap m_wrapR;
    QuickTexture::Filter m_magFilter;
    QuickTexture::Filter m_minFilter;
    double m_lodBias;
    double m_minLod;
    double m_maxLod;
    QColor m_borderColor;
    bool m_zeroTexture;
    bool m_textureWritten;
    QFutureWatcher<QOpenGLTexture *> watcher;

};

Q_DECLARE_METATYPE(QOpenGLTexture::Target)

#endif // QUICKSGTEXTURE_H

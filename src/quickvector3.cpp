#include "quickvector3.h"
#include "quickmatrix4.h"
#include "quickvector4.h"

/*!
 * \qmlbasictype vec3
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The vec3 basic type represents a GLSL vec3 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty float vec3::x
 *
 * This property holds the x component of the vec3.
 */

/*!
 * \qmlproperty float vec3::y
 *
 * This property holds the y component of the vec3.
 */

/*!
 * \qmlproperty float vec3::z
 *
 * This property holds the z component of the vec3.
 */

/*!
 * \qmlproperty vec2 vec3::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty vec2 vec3::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty vec2 vec3::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty vec2 vec3::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty vec2 vec3::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty vec2 vec3::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty vec2 vec3::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty vec2 vec3::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty vec2 vec3::zz
 *
 * This property holds the swizzled zz value
 */


/*!
 * \qmlproperty vec3 vec3::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty vec3 vec3::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty vec3 vec3::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty vec3 vec3::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty vec3 vec3::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty vec3 vec3::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty vec3 vec3::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty vec3 vec3::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty vec3 vec3::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty vec3 vec3::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty vec3 vec3::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty vec3 vec3::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty vec3 vec3::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty vec3 vec3::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty vec3 vec3::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty vec3 vec3::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty vec3 vec3::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty vec3 vec3::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty vec3 vec3::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty vec3 vec3::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty vec3 vec3::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty vec3 vec3::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty vec3 vec3::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty vec3 vec3::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty vec3 vec3::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty vec3 vec3::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty vec3 vec3::zzz
 *
 * This property holds the swizzled zzz value
 */


/*!
 * \qmlbasictype ivec3
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The ivec3 basic type represents a GLSL ivec3 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty int ivec3::x
 *
 * This property holds the x component of the ivec3.
 */

/*!
 * \qmlproperty int ivec3::y
 *
 * This property holds the y component of the ivec3.
 */

/*!
 * \qmlproperty int ivec3::z
 *
 * This property holds the z component of the ivec3.
 */


/*!
 * \qmlproperty ivec2 ivec3::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty ivec2 ivec3::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty ivec2 ivec3::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty ivec2 ivec3::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty ivec2 ivec3::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty ivec2 ivec3::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty ivec2 ivec3::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty ivec2 ivec3::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty ivec2 ivec3::zz
 *
 * This property holds the swizzled zz value
 */



/*!
 * \qmlproperty ivec3 ivec3::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty ivec3 ivec3::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty ivec3 ivec3::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty ivec3 ivec3::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty ivec3 ivec3::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty ivec3 ivec3::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty ivec3 ivec3::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty ivec3 ivec3::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty ivec3 ivec3::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty ivec3 ivec3::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty ivec3 ivec3::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty ivec3 ivec3::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty ivec3 ivec3::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty ivec3 ivec3::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty ivec3 ivec3::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty ivec3 ivec3::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty ivec3 ivec3::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty ivec3 ivec3::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty ivec3 ivec3::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty ivec3 ivec3::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty ivec3 ivec3::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty ivec3 ivec3::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty ivec3 ivec3::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty ivec3 ivec3::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty ivec3 ivec3::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty ivec3 ivec3::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty ivec3 ivec3::zzz
 *
 * This property holds the swizzled zzz value
 */

/*!
 * \qmlbasictype uvec3
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The uvec3 basic type represents a GLSL uvec3 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty uint uvec3::x
 *
 * This property holds the x component of the uvec3.
 */

/*!
 * \qmlproperty uint uvec3::y
 *
 * This property holds the y component of the uvec3.
 */

/*!
 * \qmlproperty uint uvec3::z
 *
 * This property holds the z component of the uvec3.
 */


/*!
 * \qmlproperty uvec2 uvec3::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty uvec2 uvec3::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty uvec2 uvec3::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty uvec2 uvec3::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty uvec2 uvec3::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty uvec2 uvec3::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty uvec2 uvec3::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty uvec2 uvec3::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty uvec2 uvec3::zz
 *
 * This property holds the swizzled zz value
 */



/*!
 * \qmlproperty uvec3 uvec3::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty uvec3 uvec3::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty uvec3 uvec3::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty uvec3 uvec3::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty uvec3 uvec3::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty uvec3 uvec3::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty uvec3 uvec3::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty uvec3 uvec3::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty uvec3 uvec3::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty uvec3 uvec3::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty uvec3 uvec3::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty uvec3 uvec3::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty uvec3 uvec3::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty uvec3 uvec3::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty uvec3 uvec3::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty uvec3 uvec3::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty uvec3 uvec3::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty uvec3 uvec3::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty uvec3 uvec3::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty uvec3 uvec3::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty uvec3 uvec3::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty uvec3 uvec3::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty uvec3 uvec3::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty uvec3 uvec3::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty uvec3 uvec3::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty uvec3 uvec3::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty uvec3 uvec3::zzz
 *
 * This property holds the swizzled zzz value
 */
/*!
 * \qmlbasictype dvec3
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The dvec3 basic type represents a GLSL dvec3 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty double dvec3::x
 *
 * This property holds the x component of the dvec3.
 */

/*!
 * \qmlproperty double dvec3::y
 *
 * This property holds the y component of the dvec3.
 */

/*!
 * \qmlproperty double dvec3::z
 *
 * This property holds the z component of the dvec3.
 */


/*!
 * \qmlproperty dvec2 dvec3::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty dvec2 dvec3::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty dvec2 dvec3::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty dvec2 dvec3::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty dvec2 dvec3::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty dvec2 dvec3::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty dvec2 dvec3::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty dvec2 dvec3::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty dvec2 dvec3::zz
 *
 * This property holds the swizzled zz value
 */



/*!
 * \qmlproperty dvec3 dvec3::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty dvec3 dvec3::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty dvec3 dvec3::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty dvec3 dvec3::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty dvec3 dvec3::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty dvec3 dvec3::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty dvec3 dvec3::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty dvec3 dvec3::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty dvec3 dvec3::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty dvec3 dvec3::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty dvec3 dvec3::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty dvec3 dvec3::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty dvec3 dvec3::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty dvec3 dvec3::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty dvec3 dvec3::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty dvec3 dvec3::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty dvec3 dvec3::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty dvec3 dvec3::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty dvec3 dvec3::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty dvec3 dvec3::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty dvec3 dvec3::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty dvec3 dvec3::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty dvec3 dvec3::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty dvec3 dvec3::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty dvec3 dvec3::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty dvec3 dvec3::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty dvec3 dvec3::zzz
 *
 * This property holds the swizzled zzz value
 */

/*!
 * \qmlbasictype bvec3
 * \inqmlmodule QuickOpenGL
 * \ingroup QuickOpenGL QML Basic Types
 *
 *
 * \brief The bvec3 basic type represents a GLSL bvec3 data type
 *
 * \sa https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)
 */

/*!
 * \qmlproperty bool bvec3::x
 *
 * This property holds the x component of the bvec3.
 */

/*!
 * \qmlproperty bool bvec3::y
 *
 * This property holds the y component of the bvec3.
 */

/*!
 * \qmlproperty bool bvec3::z
 *
 * This property holds the z component of the bvec3.
 */


/*!
 * \qmlproperty bvec2 bvec3::xx
 *
 * This property holds the swizzled xx value
 */

/*!
 * \qmlproperty bvec2 bvec3::xy
 *
 * This property holds the swizzled xy value
 */

/*!
 * \qmlproperty bvec2 bvec3::xz
 *
 * This property holds the swizzled xz value
 */

/*!
 * \qmlproperty bvec2 bvec3::yx
 *
 * This property holds the swizzled yx value
 */

/*!
 * \qmlproperty bvec2 bvec3::yy
 *
 * This property holds the swizzled yy value
 */

/*!
 * \qmlproperty bvec2 bvec3::yz
 *
 * This property holds the swizzled yz value
 */

/*!
 * \qmlproperty bvec2 bvec3::zx
 *
 * This property holds the swizzled zx value
 */

/*!
 * \qmlproperty bvec2 bvec3::zy
 *
 * This property holds the swizzled zy value
 */

/*!
 * \qmlproperty bvec2 bvec3::zz
 *
 * This property holds the swizzled zz value
 */



/*!
 * \qmlproperty bvec3 bvec3::xxx
 *
 * This property holds the swizzled xxx value
 */

/*!
 * \qmlproperty bvec3 bvec3::xxy
 *
 * This property holds the swizzled xxy value
 */

/*!
 * \qmlproperty bvec3 bvec3::xxz
 *
 * This property holds the swizzled xxz value
 */

/*!
 * \qmlproperty bvec3 bvec3::xyx
 *
 * This property holds the swizzled xyx value
 */

/*!
 * \qmlproperty bvec3 bvec3::xyy
 *
 * This property holds the swizzled xyy value
 */

/*!
 * \qmlproperty bvec3 bvec3::xyz
 *
 * This property holds the swizzled xyz value
 */

/*!
 * \qmlproperty bvec3 bvec3::xzx
 *
 * This property holds the swizzled xzx value
 */

/*!
 * \qmlproperty bvec3 bvec3::xzy
 *
 * This property holds the swizzled xzy value
 */

/*!
 * \qmlproperty bvec3 bvec3::xzz
 *
 * This property holds the swizzled xzz value
 */

/*!
 * \qmlproperty bvec3 bvec3::yxx
 *
 * This property holds the swizzled yxx value
 */

/*!
 * \qmlproperty bvec3 bvec3::yxy
 *
 * This property holds the swizzled yxy value
 */

/*!
 * \qmlproperty bvec3 bvec3::yxz
 *
 * This property holds the swizzled yxz value
 */

/*!
 * \qmlproperty bvec3 bvec3::yyx
 *
 * This property holds the swizzled yyx value
 */

/*!
 * \qmlproperty bvec3 bvec3::yyy
 *
 * This property holds the swizzled yyy value
 */

/*!
 * \qmlproperty bvec3 bvec3::yyz
 *
 * This property holds the swizzled yyz value
 */

/*!
 * \qmlproperty bvec3 bvec3::yzx
 *
 * This property holds the swizzled yzx value
 */

/*!
 * \qmlproperty bvec3 bvec3::yzy
 *
 * This property holds the swizzled yzy value
 */

/*!
 * \qmlproperty bvec3 bvec3::yzz
 *
 * This property holds the swizzled yzz value
 */

/*!
 * \qmlproperty bvec3 bvec3::zxx
 *
 * This property holds the swizzled zxx value
 */

/*!
 * \qmlproperty bvec3 bvec3::zxy
 *
 * This property holds the swizzled zxy value
 */

/*!
 * \qmlproperty bvec3 bvec3::zxz
 *
 * This property holds the swizzled zxz value
 */

/*!
 * \qmlproperty bvec3 bvec3::zyx
 *
 * This property holds the swizzled zyx value
 */

/*!
 * \qmlproperty bvec3 bvec3::zyy
 *
 * This property holds the swizzled zyy value
 */

/*!
 * \qmlproperty bvec3 bvec3::zyz
 *
 * This property holds the swizzled zyz value
 */

/*!
 * \qmlproperty bvec3 bvec3::zzx
 *
 * This property holds the swizzled zzx value
 */

/*!
 * \qmlproperty bvec3 bvec3::zzy
 *
 * This property holds the swizzled zzy value
 */

/*!
 * \qmlproperty bvec3 bvec3::zzz
 *
 * This property holds the swizzled zzz value
 */

/*!
 * \qmlmethod dvec3 dvec3::crossProduct(dvec3 other)
 * \param other the other vector
 * \return the cross product of this vector and the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} crossProduct method.
 * \sa vector3d
 */
qdvec3 qdvec3::crossProduct(const qdvec3 &other) const
{
    return qdvec3(y() * other.z() - z() * other.y(),
                  z() * other.x() - x() * other.z(),
                  x() * other.y() - y() * other.x());
}

/*!
 * \qmlmethod double dvec3::dotProduct(dvec3 other)
 * \param other the other vector
 * \return the dot product of this vector and the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} dotProduct method.
 * \sa vector3d
 */
double qdvec3::dotProduct(const qdvec3 &other) const
{
    return x() * other.x() + y() * other.y() + z() * other.z();
}

/*!
 * \qmlmethod dvec3 dvec3::times(dmat4 matrix)
 * \param matrix the matrix
 * \return the result of transforming this vector with the matrix (applied post-vector)
 *
 * This method performs the same function as the equivalent QML \l{vector3d} times method.
 * \sa vector3d
 */
qdvec3 qdvec3::times(const qdmat4 &matrix) const
{
    qdvec4 temp = qdvec4(*this, 1.0)*matrix;
    return temp.xyz()/temp.w();
}

/*!
 * \qmlmethod dvec3 dvec3::times(double factor)
 * \param factor the scaling factor
 * \return the result of scaling this vector by factor
 *
 * This method performs the same function as the equivalent QML \l{vector3d} times method.
 * \sa vector3d
 */
qdvec3 qdvec3::times(double factor) const
{
    return qdvec3((*this)*factor);
}

/*!
 * \qmlmethod dvec3 dvec3::times(dvec3 other)
 * \param other the other vector
 * \return the result of multiplying this vector by the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} times method.
 * \sa vector3d
 */
qdvec3 qdvec3::times(const qdvec3 &other) const
{
    return qdvec3((*this)*other);
}

/*!
 * \qmlmethod dvec3 dvec3::plus(dvec3 other)
 * \param other the other vector
 * \return the result of adding the other vector to this vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} plus method.
 * \sa vector3d
 */
qdvec3 qdvec3::plus(const qdvec3 &other) const
{
    return qdvec3(*this + other);
}

/*!
 * \qmlmethod dvec3 dvec3::minus(dvec3 other)
 * \param other the other vector
 * \return the result of subtracting the other vector from this vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} minus method.
 * \sa vector3d
 */
qdvec3 qdvec3::minus(const qdvec3 &other) const
{
    return qdvec3(*this - other);
}

/*!
 * \qmlmethod dvec3 dvec3::normalized()
 * \return the normalized vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} normalized method.
 * \sa vector3d
 */
qdvec3 qdvec3::normalized() const
{
    return (*this)*(1.0/std::sqrt(x()*x() + y()*y() + z()*z()));
}

/*!
 * \qmlmethod double dvec3::length()
 * \return the length of the vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} length method.
 * \sa vector3d
 */
double qdvec3::length() const
{
    return std::sqrt(x()*x() + y()*y() + z()*z());
}

/*!
 * \qmlmethod vec3 vec3::crossProduct(vec3 other)
 * \param other the other vector
 * \return the cross product of this vector and the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} crossProduct method.
 * \sa vector3d
 */
qvec3 qvec3::crossProduct(const qvec3 &other) const
{
    return qvec3(y() * other.z() - z() * other.y(),
                  z() * other.x() - x() * other.z(),
                  x() * other.y() - y() * other.x());
}

/*!
 * \qmlmethod float vec3::dotProduct(vec3 other)
 * \param other the other vector
 * \return the dot product of this vector and the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} dotProduct method.
 * \sa vector3d
 */
float qvec3::dotProduct(const qvec3 &other) const
{
    return x() * other.x() + y() * other.y() + z() * other.z();

}

/*!
 * \qmlmethod vec3 vec3::times(mat4 matrix)
 * \param matrix the matrix
 * \return the result of transforming this vector with the matrix (applied post-vector)
 *
 * This method performs the same function as the equivalent QML \l{vector3d} times method.
 * \sa vector3d
 */
qvec3 qvec3::times(const qmat4 &other) const
{
    qvec4 temp = qvec4(*this, 1.0)*other;
    return temp.xyz()/temp.w();
}

/*!
 * \qmlmethod vec3 vec3::times(float factor)
 * \param factor the scaling factor
 * \return the result of scaling this vector by factor
 *
 * This method performs the same function as the equivalent QML \l{vector3d} times method.
 * \sa vector3d
 */
qvec3 qvec3::times(float factor) const
{
    return qvec3((*this)*factor);
}

/*!
 * \qmlmethod vec3 vec3::times(vec3 other)
 * \param other the other vector
 * \return the result of multiplying this vector by the other vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} times method.
 * \sa vector3d
 */
qvec3 qvec3::times(const qvec3 &other) const
{
    return qvec3((*this)*other);
}

/*!
 * \qmlmethod vec3 vec3::plus(vec3 other)
 * \param other the other vector
 * \return the result of adding the other vector to this vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} plus method.
 * \sa vector3d
 */
qvec3 qvec3::plus(const qvec3 &other) const
{
    return qvec3(*this + other);
}

/*!
 * \qmlmethod vec3 vec3::minus(vec3 other)
 * \param other the other vector
 * \return the result of subtracting the other vector from this vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} minus method.
 * \sa vector3d
 */
qvec3 qvec3::minus(const qvec3 &other) const
{
    return qvec3(*this - other);
}

/*!
 * \qmlmethod vec3 vec3::normalized()
 * \return the normalized vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} normalized method.
 * \sa vector3d
 */
qvec3 qvec3::normalized() const
{
    return (*this)*(1.0f/std::sqrt(x()*x() + y()*y() + z()*z()));
}

/*!
 * \qmlmethod float vec3::length()
 * \return the length of the vector
 *
 * This method performs the same function as the equivalent QML \l{vector3d} length method.
 * \sa vector3d
 */
float qvec3::length() const
{
    return std::sqrt(x()*x() + y()*y() + z()*z());
}

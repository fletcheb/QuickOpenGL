/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include <QThread>
#include <QQuickWindow>
#include <QOpenGLFunctions_4_3_Core>
#ifdef QT_DEBUG
#include <QElapsedTimer>
#endif

#include "quicksgssbo.h"
#include "quicksgssboprovider.h"
#include "quickshaderstoragebuffer.h"
#include "support.h"
#include "quickopenglthreadpool.h"
/*!
 * \qmltype ShaderStorageBuffer
 * \brief The ShaderStorageBuffer QML type represents an OpenGL shader storage buffer object.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickShaderStorageBuffer
 * \ingroup QuickOpenGL QML Types
 *
 *
 * ShaderStorageBuffer represents an OpenGL shader storage buffer object
 * (SSBO). SSBOs are used to store and retrieve data from within GLSL, and are
 * defined in GLSL by buffer interface blocks.
 *
 * \l GraphicsProgram and \l ComputeProgram introspect SSBO interface blocks
 * within an OpenGL program. They then interact with ShaderStorageBuffer to
 * match introspected interface block variable names with ShaderStorageBuffer
 * QML properties. If a ShaderStorageBuffer is bound to interface blocks in
 * multiple programs, ShaderStorageBuffer warns if the GLSL interface block
 * definitions are inconsistent.
 *
 * ShaderStorageBuffer can be set to synchronise data from QML to OpenGL via \l
 * syncToOpenGL, and from OpenGL to QML via \l syncFromOpenGL. Enabling
 * synchronisation in both directions simultaneously may lead to unpredictible
 * behaviour, due to race conditions. This is because ShaderStorageBuffer
 * buffer reads occur asynchronously.
 *
 * ShaderStorageBuffer variables are defined as QML properties within
 * ShaderStorageBuffer. They should be defined as types that can be converted
 * to and from the GLSL data type. If the QML property type is undefined,
 * ShaderStorageBuffer will write the GLSL data type to the property.
 * ShaderStorageBuffer supports structs and arrays.
 *
 * If the ShaderStorageBuffer \l size (bytes) property is set to less than the
 * introspected GLSL SSBO size, the introspected size is used. If \l size is
 * greater than the introspected size, the SSBO is allocated the
 * ShaderStorageBuffer \l size. Setting a \l size is useful when the last GLSL
 * interface block variable is an unsized array. GLSL then sizes the array to
 * fill the SSBO. If synchronisation with QML is not enabled, matching
 * ShaderStorageBuffer properties are not required.
 *
 * ShaderStorageBuffer initializes the SSBO with QML defined values if QML to
 * OpenGL synchronisation is enabled, otherwise it initalizes the buffer with
 * zeros.
 *
 * Example usage:
 *
 * \qml
 * ShaderStorageBuffer
 * {
 *     id: ssbo
 *     syncFromOpenGL: true
 *     usagePattern: ShaderStorageBuffer.DynamicDraw
 *
 *     property color color: "#ff000000"
 *     onColorChanged: console.log(color)
 * }
 *
 * GraphicsProgram
 * {
 *     anchors.fill: parent
 *
 *     property var mouse: mouseArea.mapToItem(null, mouseArea.mouseX, mouseArea.mouseY)
 *     property var ssbo: ssbo
 *
 *     fragmentShader: "
 *         #version 430
 *
 *         in vec2 coord;
 *         layout (origin_upper_left,pixel_center_integer) in vec4 gl_FragCoord;
 *         out vec4 qt_FragColor;
 *         uniform vec2 mouse;
 *
 *         buffer ssbo
 *         {
 *             vec4 color;
 *         };
 *
 *         void main()
 *         {
 *             qt_FragColor = vec4(coord, 0, 1);
 *
 *             if(all(equal(gl_FragCoord.xy, mouse)))
 *             {
 *                 color = qt_FragColor;
 *             }
 *
 *         }"
 * }
 * \endqml
 *
 *
 * \sa https://www.khronos.org/opengl/wiki/Shader_Storage_Buffer_Object
 */
class QuickShaderStorageBufferPrivate : public QObject
{
    Q_OBJECT

    QuickShaderStorageBufferPrivate(QuickShaderStorageBuffer *q);

    QuickShaderStorageBuffer::UsagePattern usagePattern;
    QuickShaderStorageBuffer::SyncWrite syncToOpenGL;
    bool syncFromOpenGL;
    int size;
    int bufferObject;
    QByteArray data;

    QList<int> ignoreUpdatePropertyIndices;
    QList<int> updatedPropertyIndices;
    bool dataUpdated;
    QHash<QString, QList<Introspection::ShaderStorageBlock::Variable>> baseVariables;

    static bool setBufferData(QByteArray &buffer, const Introspection::ShaderStorageBlock::Variable &variable, const QVariantList &property);
    template <typename Type> static void setBufferData(const QVariantList &list, QByteArray &data, const Introspection::ShaderStorageBlock::Variable &variable);

    static bool isDouble(double d);

    QuickSGSSBOProvider *ssboProvider;

    void synchronizeProperties();

public slots:

    void setBufferId(uint bufferId);
    void onSceneGraphInvalidated();
    void onAfterSynchronizing();
    void onSsboVariables(QVariantMap data);
    void onSsboData(QByteArray data);
    void update();


protected:

    QuickShaderStorageBuffer *q_ptr;


private:

    Q_DECLARE_PUBLIC(QuickShaderStorageBuffer)
};


QuickShaderStorageBufferPrivate::QuickShaderStorageBufferPrivate(QuickShaderStorageBuffer *q):
    usagePattern(QuickShaderStorageBuffer::DynamicRead),
    syncToOpenGL(QuickShaderStorageBuffer::SyncWriteNever),
    syncFromOpenGL(false),
    size(-1),
    bufferObject(-1),
    dataUpdated(false),
    ssboProvider(nullptr),
    q_ptr(q)
{

}

template<typename Type>
void QuickShaderStorageBufferPrivate::setBufferData(const QVariantList &list, QByteArray &buffer, const Introspection::ShaderStorageBlock::Variable &variable)
{

    for(int i=0;i<qMin(variable.arraySize, list.count());++i)
    {
        if(list.at(i).canConvert<Type>())
        {
            Q_ASSERT(buffer.size()>=variable.offset+i*variable.arrayStride+int(sizeof(Type)));
            reinterpret_cast<Type &>(buffer.data()[variable.offset + i*variable.arrayStride]) = list.at(i).value<Type>();
        }
    }

}


bool QuickShaderStorageBufferPrivate::setBufferData(QByteArray &buffer, const Introspection::ShaderStorageBlock::Variable &variable, const QVariantList &property)
{
    bool result = true;

    // do the appropriate thing for the uniform type
    switch(variable.type)
    {
    case GL_FLOAT:
        setBufferData<float>(property, buffer, variable);
        break;
    case GL_FLOAT_VEC2:
        setBufferData<qvec2>(property, buffer, variable);
        break;
    case GL_FLOAT_VEC3:
        setBufferData<qvec3>(property, buffer, variable);
        break;
    case GL_FLOAT_VEC4:
        setBufferData<qvec4>(property, buffer, variable);
        break;
    case GL_DOUBLE:
        setBufferData<double>(property, buffer, variable);
        break;
    case GL_DOUBLE_VEC2:
        setBufferData<qdvec2>(property, buffer, variable);
        break;
    case GL_DOUBLE_VEC3:
        setBufferData<qdvec3>(property, buffer, variable);
        break;
    case GL_DOUBLE_VEC4:
        setBufferData<qdvec4>(property, buffer, variable);
        break;
    case GL_INT:
        setBufferData<int>(property, buffer, variable);
        break;
    case GL_INT_VEC2:
        setBufferData<qivec2>(property, buffer, variable);
        break;
    case GL_INT_VEC3:
        setBufferData<qivec3>(property, buffer, variable);
        break;
    case GL_INT_VEC4:
        setBufferData<qivec4>(property, buffer, variable);
        break;
    case GL_UNSIGNED_INT:
        setBufferData<unsigned int>(property, buffer, variable);
        break;
    case GL_UNSIGNED_INT_VEC2:
        setBufferData<quvec2>(property, buffer, variable);
        break;
    case GL_UNSIGNED_INT_VEC3:
        setBufferData<quvec3>(property, buffer, variable);
        break;
    case GL_UNSIGNED_INT_VEC4:
        setBufferData<quvec4>(property, buffer, variable);
        break;
    case GL_BOOL:
        setBufferData<bool>(property, buffer, variable);
        break;
    case GL_BOOL_VEC2:
        setBufferData<qbvec2>(property, buffer, variable);
        break;
    case GL_BOOL_VEC3:
        setBufferData<qbvec3>(property, buffer, variable);
        break;
    case GL_BOOL_VEC4:
        setBufferData<qbvec4>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT2:
        setBufferData<qmat2>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT3:
        setBufferData<qmat3>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT4:
        setBufferData<qmat4>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT2x3:
        setBufferData<qmat2x3>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT2x4:
        setBufferData<qmat2x4>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT3x2:
        setBufferData<qmat3x2>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT3x4:
        setBufferData<qmat3x4>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT4x2:
        setBufferData<qmat4x2>(property, buffer, variable);
        break;
    case GL_FLOAT_MAT4x3:
        setBufferData<qmat4x3>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT2:
        setBufferData<qdmat2>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT3:
        setBufferData<qdmat3>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT4:
        setBufferData<qdmat4>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT2x3:
        setBufferData<qdmat2x3>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT2x4:
        setBufferData<qdmat2x4>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT3x2:
        setBufferData<qdmat3x2>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT3x4:
        setBufferData<qdmat3x4>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT4x2:
        setBufferData<qdmat4x2>(property, buffer, variable);
        break;
    case GL_DOUBLE_MAT4x3:
        setBufferData<qdmat4x3>(property, buffer, variable);
        break;
    case GL_UNSIGNED_INT64_ARB:
        setBufferData<quint64>(property, buffer, variable);
        break;
    case GL_INT64_ARB:
        setBufferData<qint64>(property, buffer, variable);
        break;
    default:
        result = false;
        break;
    }


    return result;
}

/*!
 * \internal isDouble from Q4V::Value::setDouble
 *
 * Do the same check as Q4V::Value::setDouble to avoid an assert if the value is
 * nan
 */
bool QuickShaderStorageBufferPrivate::isDouble(double d)
{
    static const quint64 NaNEncodeMask  = 0xfffc000000000000ll;
    quint64 _val;
    memcpy(&_val, &d, 8);
    _val ^= NaNEncodeMask;
    return (_val >> (64-14));
}

void QuickShaderStorageBufferPrivate::synchronizeProperties()
{
    Q_Q(QuickShaderStorageBuffer);


    if(!ssboProvider || !ssboProvider->shaderStorageBlock().isValid())
    {
        return;
    }

#ifdef QT_DEBUG
    QElapsedTimer elapsedTimer;
    elapsedTimer.start();

    QStringList updatedProperties;
#endif

    Introspection::ShaderStorageBlock shaderStorageBlock = ssboProvider->shaderStorageBlock();


    if(baseVariables.isEmpty())
    {
        for(const Introspection::ShaderStorageBlock::Variable &variable: shaderStorageBlock.variables)
        {
            // remove array subscripts and struct postscripts from the variable name
            QRegularExpressionMatch match = QRegularExpression(QStringLiteral("([\\w\\d]+)")).match(variable.name);
            Q_ASSERT(match.hasMatch());
            baseVariables[match.captured(1)].append(variable);
        }
    }

    // the buffer size
    int bufferSize = qMax(shaderStorageBlock.size, size);

    bool updated = ssboProvider->updated()|(data.size()!=bufferSize);

    // resize to buffer size (pad out with zeros)
    if(data.size()<bufferSize)
    {
        data.append(bufferSize-data.size(), 0);
        updated = true;
    }

    // add our updated QMetaObject properties to our property list
    for(int index: updatedPropertyIndices)
    {
        QMetaProperty property = q->metaObject()->property(index);

        if(baseVariables.contains(property.name()))
        {

#ifdef QT_DEBUG
            updatedProperties.append(property.name());
#endif

            QVariant value = Introspection::conditionVariant(property.read(q), q);

            for(Introspection::ShaderStorageBlock::Variable variable: baseVariables[property.name()])
            {
                int levels = ((variable.topLevelArraySize==0)&&(variable.topLevelArrayStride!=0))?value.toList().count():variable.topLevelArraySize;

                if(data.size()<variable.offset+QuickOpenGL::sizeOfType(variable.type)*variable.arraySize + (levels-1)*variable.topLevelArrayStride)
                {
                    data.append(variable.offset+QuickOpenGL::sizeOfType(variable.type)*variable.arraySize + (levels-1)*variable.topLevelArrayStride - data.size(), 0);
                }

                for(int level=0;level<levels;level++)
                {

                    QString name = variable.name;

                    if(level>0)
                    {
                        name.replace(name.indexOf("[0]")+1,1,QString("%1").arg(level));
                    }

                    QVariantList list = Introspection::conditionProperty(value, name, variable.type, variable.arraySize, "shader storage block variable", q);

                    int arraySize = ((variable.arraySize==0)&&(variable.arrayStride!=0))?list.count():variable.arraySize;

                    if(data.size()<variable.offset + arraySize*variable.arrayStride)
                    {
                        data.append(variable.offset + arraySize*variable.arrayStride - data.size(), 0);
                    }

                    variable.arraySize = arraySize;

                    if(!setBufferData(data, variable, list))
                    {
                        qWarning() << "QuickOpenGL:" << qobject_cast<QObject *>(q) << variable.name << "has unsupported type" << QuickOpenGL::GLenum(variable.type);
                    }

                    variable.offset += variable.topLevelArrayStride;

                }
            }

            updated = true;
        }
    }

#ifdef QT_DEBUG
                if((QuickOpenGL::debugLevel()&DEBUG_TIMING)&&updatedProperties.count())
                {
                    QString string("Buffer Update [");
                    QTextStream stream(&string);

                    for(int i=0; i<updatedProperties.count(); ++i)
                    {
                        stream << updatedProperties[i] << (i==updatedProperties.count()-1?"":", ");
                    }

                    stream << "]";

                    qDebug().nospace() << "quickopengl.scenegraph.time.shaderstoragebuffer.synchronizeProperties: " << reinterpret_cast<QObject *>(q) << " took " << elapsedTimer.nsecsElapsed()*1e-6 << "ms (" << qPrintable(string) <<")";
                }
#endif

    ssboProvider->setData(data);
    ssboProvider->setUpdated(updated);
}

void QuickShaderStorageBufferPrivate::setBufferId(uint bufferId)
{
    Q_Q(QuickShaderStorageBuffer);

    bufferObject = int(bufferId);
    emit q->bufferObjectChanged(bufferObject);
}

void QuickShaderStorageBufferPrivate::onSceneGraphInvalidated()
{
    if(ssboProvider)
    {
        delete ssboProvider;
        ssboProvider = nullptr;
    }
}

void QuickShaderStorageBufferPrivate::onAfterSynchronizing()
{
    Q_Q(QuickShaderStorageBuffer);

    if(dataUpdated&&((syncToOpenGL==QuickShaderStorageBuffer::SyncWriteOnce)||(syncToOpenGL==QuickShaderStorageBuffer::SyncWriteOnUpdated)||(syncToOpenGL==QuickShaderStorageBuffer::SyncWriteAlways)))
    {
        if(ssboProvider&&ssboProvider->shaderStorageBlock().isValid())
        {
            int size = ssboProvider->shaderStorageBlock().size;
            int stride = ssboProvider->shaderStorageBlock().variables.values().first().topLevelArrayStride?ssboProvider->shaderStorageBlock().variables.values().first().topLevelArrayStride:ssboProvider->shaderStorageBlock().variables.values().first().arrayStride;


            if((data.size()!=size)&&(data.size()%stride!=0))
            {
                qWarning() << "QuickOpenGL:" << reinterpret_cast<QObject *>(q) << "provided data size" << data.size() << "not compatible with GLSL shader storage block size" << size << "or topLevelArrayStride" << stride;
            }
        }
        ssboProvider->setData(data);
        ssboProvider->setUpdated(true);
        dataUpdated = false;
    }
    else if(updatedPropertyIndices.count()&&((syncToOpenGL==QuickShaderStorageBuffer::SyncWriteOnce)||(syncToOpenGL==QuickShaderStorageBuffer::SyncWriteOnUpdated)||(syncToOpenGL==QuickShaderStorageBuffer::SyncWriteAlways)))
    {
        synchronizeProperties();
    }

    if(ssboProvider)
    {
#ifdef QT_DEBUG
        QElapsedTimer elapsedTimer;
        elapsedTimer.start();
#endif

        ssboProvider->setSyncFromOpenGL(syncFromOpenGL);
        ssboProvider->setSyncToOpenGL(syncToOpenGL);
        ssboProvider->setSize(size);
        ssboProvider->setUsagePattern(usagePattern);

        // The shader storage buffer object
        QuickSGSSBO *ssbo = ssboProvider->ssbo();
        ssbo->setUsagePattern(ssboProvider->usagePattern());

        QuickShaderStorageBuffer::SyncWrite syncToOpenGL = ssboProvider->syncToOpenGL();

        // if we are to push data to OpenGL
        if((!ssbo->synced())&&((syncToOpenGL==QuickShaderStorageBuffer::SyncWriteAlways)||
                               ((syncToOpenGL==QuickShaderStorageBuffer::SyncWriteOnUpdated)&&ssboProvider->updated())||
                               ((syncToOpenGL==QuickShaderStorageBuffer::SyncWriteOnce)&&(ssbo->size()==0))))
        {
            ssboProvider->setUpdated(false);
            ssbo->setSynced(true);
            ssbo->setSize(ssboProvider->data().size());
            ssbo->bufferData(ssboProvider->data());
#ifdef QT_DEBUG
            if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
            {
                qDebug().nospace() << "quickopengl.scenegraph.time.shaderstoragebuffer.write: bufferId " << ssbo->bufferId() << " " << reinterpret_cast<QObject *>(q) << " usage pattern " << QuickOpenGL::GLenum(usagePattern) << " size " << ssbo->size()<< " took " << double(elapsedTimer.nsecsElapsed())*1e-6 << "ms";
            }
#endif
        }
        else
        {

#ifndef QT_OPENGL_ES
            // Get the OpenGL functions object
            QOpenGLFunctions_4_3_Core *gl43 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_3_Core>();
            if(!gl43)
            {
                return;
            }
            bool ok = gl43->initializeOpenGLFunctions();
            Q_ASSERT(ok);

            // determine the ssbo size
            int size = qMax(qMax(ssboProvider->shaderStorageBlock().size, ssboProvider->size()), ssboProvider->data().size());

            // if the size has changed
            if(!gl43->glIsBuffer(ssbo->bufferId())||(ssbo->size()!=size)||((!ssbo->synced())&&(syncToOpenGL==QuickShaderStorageBuffer::SyncWriteZeroAlways)))
            {
                // reinitialise the ssbo
                ssbo->setSize(size);
                ssbo->bufferData(size);
                ssbo->setSynced(true);
#ifdef QT_DEBUG
                if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
                {
                    qDebug().nospace() << "quickopengl.scenegraph.time.shaderstoragebuffer.write: bufferId " << ssbo->bufferId() << " " << reinterpret_cast<QObject *>(q) << " usage pattern " << QuickOpenGL::GLenum(usagePattern) << " size " << ssbo->size() << " took " << double(elapsedTimer.nsecsElapsed())*1e-6 << "ms";
                }
#endif
            }
#endif
        }


    }

    if(ssboProvider->shaderStorageBlock().isValid())
    {
        updatedPropertyIndices.clear();
    }
}

/*!
 * \internal
 * \brief Traverse variant structures a and b and run a comparator
 * \param a variant a
 * \param b variant b
 * \param functionPointer pointer to comparator function
 * \return false if structures are different or comparator function returns false
 */
bool variantStructureComparator(QVariant &a, QVariant &b, bool (*functionPointer)(QVariant &a, QVariant &b))
{
    bool result = true;

    if(a.canConvert<QVariantList>()&&b.canConvert<QVariantList>())
    {
        QVariantList listA = a.value<QVariantList>();
        QVariantList listB = b.value<QVariantList>();

        if(listA.count()==listB.count())
        {
            for(int i=0;result&&(i<listA.count());++i)
            {
                result &= variantStructureComparator(listA[i], listB[i], functionPointer);
            }
        }
        else
        {
            result = false;
        }
    }
    else if (a.canConvert<QVariantMap>()&&b.canConvert<QVariantMap>())
    {
        QVariantMap mapA = a.value<QVariantMap>();
        QVariantMap mapB = b.value<QVariantMap>();

        if(mapA.count()==mapB.count())
        {
            for(QVariantMap::Iterator it=mapA.begin();result&&(it!=mapA.end());++it)
            {
                if(mapB.contains(it.key()))
                {
                    result &= variantStructureComparator(it.value(), mapB[it.key()], functionPointer);
                }
                else
                {
                    result = false;
                }
            }
        }
        else
        {
            result = false;
        }
    }
    else
    {
        result = functionPointer(a,b);
    }

    return result;
}

bool canConvert(QVariant &a, QVariant &b)
{
    return (!a.isValid()&&!b.isValid())||b.canConvert(a.userType());
}

/*!
 * \internal
 * \brief Compare structures and types of a and b
 * \param a variant a
 * \param b variant b
 * \return true if structures are equivalent
 */
bool structureEquivalent(QVariant &a, QVariant &b)
{
    return variantStructureComparator(a, b, &canConvert);
}

bool convert(QVariant &a, QVariant &b)
{
    return b.convert(a.userType());
}

/*!
 * \internal
 * \brief Convert structure b types to those of structure a
 * \param a variant a
 * \param b variant b
 * \return true on success
 */
bool convertTypes(QVariant &a, QVariant &b)
{
    return variantStructureComparator(a,b, &convert);
}

bool isEqual(QVariant &a, QVariant &b)
{

    QVariant temp = b;
    bool result = b.convert(a.userType());
    result &= (a==b)||(!a.isValid()&&!b.isValid());
    return result;
}

/*!
 * \internal
 * \brief Compare equivalance of structure and values of a and b
 * \param a variant a
 * \param b variant b
 * \return true if values are equivalent
 */
bool valueEquivalent(QVariant &a, QVariant &b)
{
    return variantStructureComparator(a, b, &isEqual);
}

/*!
 * \internal
 * \brief QuickShaderStorageBufferPrivate::onSsboData
 * \param data the hash containing property - value pairs
 *
 * This function takes data read from the OpenGL ssbo and writes it into our
 * QML properties.  It is called via a queued connection, so will run in
 * the main thread.
 */
void QuickShaderStorageBufferPrivate::onSsboVariables(QVariantMap data)
{
    Q_Q(QuickShaderStorageBuffer);

#ifdef QT_DEBUG
    QScopedPointer<QElapsedTimer> elapsedTimer;
    QStringList updated;

    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        elapsedTimer.reset(new QElapsedTimer);
        elapsedTimer->start();
    }
#endif

    // for each key, value pair
    for(QVariantMap::iterator iterator = data.begin(); iterator != data.end(); ++iterator)
    {
        // get the index of the key in the metaobject's property list
        int index = q->metaObject()->indexOfProperty(iterator.key().toLocal8Bit());

        // if the metaobject contains this property
        if(index!=-1)
        {
            // get the current value of the property, conditioning it from QJSValue
            QVariant current = Introspection::conditionVariant(q->metaObject()->property(index).read(q), nullptr, false);

            bool converted = false;

            // if the current value or has the same structure and type
            if(structureEquivalent(current, iterator.value()))
            {
                converted = true;

                // if the value is different
                if(!valueEquivalent(current, iterator.value()))
                {
                    // convert the structure to have the same types
                    converted = convertTypes(current, iterator.value());

                    // Avoid Q4V::Value::setDouble asserting if the value is nan
                    if(!((current.userType()==QMetaType::Double)&&!isDouble(iterator.value().toDouble())))
                    {

                        // add this index to the list of updated indices to be ignored
                        ignoreUpdatePropertyIndices.append(index);

                        // write the new value to the property
                        converted &= q->metaObject()->property(index).write(q, iterator.value());
#ifdef QT_DEBUG
                        updated.append(iterator.key());
#endif
                    }

                }
            }
            else
            {
                converted = true;

                // Avoid Q4V::Value::setDouble asserting if the value is nan
                if(!((current.userType()==QMetaType::Double)&&!isDouble(iterator.value().toDouble())))
                {
                    // add this index to the list of updated indices to be ignored
                    ignoreUpdatePropertyIndices.append(index);

                    // write the new value to the property
                    converted = q->metaObject()->property(index).write(q, iterator.value());
#ifdef QT_DEBUG
                    updated.append(iterator.key());
#endif

                }
            }

            if(!converted)
            {
                qWarning() << "QuickOpenGL" <<  static_cast<QObject *>(q) << "could not convert" << iterator.key() << "to QML type";
            }
        }
        else
        {
            qWarning() << "QuickOpenGL:" << static_cast<QObject *>(q) << "could not match OpenGL shader storage block variable" << iterator.key() << "to a property";
        }
    }

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        qDebug().nospace() << "quickopengl.qml.time.shaderstoragebuffer.write: " << reinterpret_cast<QObject *>(q) << " took " << double(elapsedTimer->nsecsElapsed())*1e-6 << "ms " << updated;
    }
#endif

}

void QuickShaderStorageBufferPrivate::onSsboData(QByteArray data)
{
    Q_Q(QuickShaderStorageBuffer);

    this->data = data;
    emit q->dataChanged(this->data);

}

/*!
 * \internal
 * \brief Update slot called when a QuickShaderStorageBuffer meta property is changed
 *
 * This function identifies the QuickShaderStorageBuffer property which has
 * changed, and adds it to the list of properties to be copied to the provider
 * next time updatePaintNode is called. It then triggers a scene graph update
 * by calling QuickShaderStorageBuffer:update.
 */
void QuickShaderStorageBufferPrivate::update()
{
    Q_Q(QuickShaderStorageBuffer);

    // Get the name of the notify signal we have received
    QByteArray signalName = sender()->metaObject()->method(senderSignalIndex()).name();
    Q_ASSERT(signalName.endsWith(QByteArrayLiteral("Changed")));

    // Get the index of the property
    int propertyIndex = sender()->metaObject()->indexOfProperty(signalName.left(signalName.count()-7));
    Q_ASSERT(propertyIndex!=-1);

    // Add the property index to the list of updated properties to be copied
    if(!updatedPropertyIndices.contains(propertyIndex))
    {
        updatedPropertyIndices.append(propertyIndex);
    }

    // If we caused the update with a sync from OpenGL, ignore this
    if(ignoreUpdatePropertyIndices.contains(propertyIndex))
    {
        ignoreUpdatePropertyIndices.removeOne(propertyIndex);
        return;
    }

    // Trigger an update
    q->update();

}

QuickShaderStorageBuffer::QuickShaderStorageBuffer(QQuickItem *parent) :
    QQuickItem(parent),
    d_ptr(new QuickShaderStorageBufferPrivate(this))
{
    setFlag(QQuickItem::ItemHasContents);
}

QuickShaderStorageBuffer::~QuickShaderStorageBuffer()
{
    Q_D(QuickShaderStorageBuffer);

    // we should have already cleaned up by now
    Q_ASSERT(d->ssboProvider==nullptr);

}

QuickSGSSBOProvider *QuickShaderStorageBuffer::ssboProvider()
{
    Q_D(QuickShaderStorageBuffer);


    if(window()&&window()->openglContext()&&window()->openglContext()->thread()&&(window()->openglContext()->thread()==QThread::currentThread()))
    {

        // if we don't already have a texture provider, create one
        if(!d->ssboProvider)
        {
            d->ssboProvider = new QuickSGSSBOProvider;

            // queue signal from QSGRender thread to main thread
            connect(d->ssboProvider, &QuickSGSSBOProvider::ssboVariables, d, &QuickShaderStorageBufferPrivate::onSsboVariables, Qt::QueuedConnection);
            connect(d->ssboProvider, &QuickSGSSBOProvider::ssboData, d, &QuickShaderStorageBufferPrivate::onSsboData, Qt::QueuedConnection);

            connect(window(), &QQuickWindow::sceneGraphInvalidated, d, &QuickShaderStorageBufferPrivate::onSceneGraphInvalidated, Qt::DirectConnection);
            connect(window(), &QQuickWindow::afterSynchronizing, d, &QuickShaderStorageBufferPrivate::onAfterSynchronizing, Qt::DirectConnection);
            connect(window(), &QQuickWindow::afterRendering, d->ssboProvider, &QuickSGSSBOProvider::onAfterRendering, Qt::DirectConnection);
            connect(d->ssboProvider->ssbo(), &QuickSGSSBO::sizeChanged, this, &QuickShaderStorageBuffer::setSize, Qt::QueuedConnection);
            connect(d->ssboProvider->ssbo(), &QuickSGSSBO::bufferIdChanged, d, &QuickShaderStorageBufferPrivate::setBufferId, Qt::QueuedConnection);

            ssboProvider()->ssbo()->setProperty("QmlItem", QVariant::fromValue(this));
        }

        return d->ssboProvider;
    }
    else
    {
        qWarning("QuickShaderStorageBuffer::ssboProvider: can only be queried on the rendering thread of an exposed window");
    }

    return nullptr;
}

/*!
 * \qmlproperty enumeration ShaderStorageBuffer::usagePattern
 *
 * This property holds the SSBO's expected usage pattern.  It specifies the a
 * hint to OpenGL as to how the SSBO's data store will be accessed.  This enables
 * the OpenGL implementation to make decisions that may improve performance.
 *
 * The usage pattern is defined by the buffer access frequency and nature.
 *
 * \table
 * \header
 * \li Access Frequency
 * \li Description
 * \row
 * \li STREAM
 * \li The data store contents will be modified once and used at most a few times.
 * \row
 * \li STATIC
 * \li The data store contents will be modified once and used many times.
 * \row
 * \li DYNAMIC
 * \li The data store contents will be modified repeatedly and used many times.
 * \endtable
 *
 * \table
 * \header
 * \li Access Nature
 * \li Description
 * \row
 * \li DRAW
 * \li The data store contents are modified by the application, and used as the source for GL drawing and image specification commands.
 * \row
 * \li READ
 * \li The data store contents are modified by reading data from the GL, and used to return that data when queried by the application.
 * \row
 * \li COPY
 * \li The data store contents are modified by reading data from the GL, and used as the source for GL drawing and image specification commands.
 * \endtable
 *
 * usagePattern takes the folowing values:
 *
 * \value StreamDraw
 *   GL_STREAM_DRAW
 * \value StreamRead
 *   GL_STREAM_READ
 * \value StreamCopy
 *   GL_STREAM_COPY
 * \value StaticDraw
 *   GL_STATIC_DRAW
 * \value StaticRead
 *   GL_STATIC_READ
 * \value StaticCopy
 *   GL_STATIC_COPY
 * \value DynamicDraw
 *   GL_DYNAMIC_DRAW
 * \value DynamicRead
 *   GL_DYNAMIC_READ
 * \value DynamicCopy
 *   GL_DYNAMIC_COPY
 *
 *  \sa https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBufferData.xhtml
 */
QuickShaderStorageBuffer::UsagePattern QuickShaderStorageBuffer::usagePattern() const
{
    Q_D(const QuickShaderStorageBuffer);

    return d->usagePattern;
}

void QuickShaderStorageBuffer::setUsagePattern(QuickShaderStorageBuffer::UsagePattern usagePattern)
{
    Q_D(QuickShaderStorageBuffer);

    if (d->usagePattern == usagePattern)
        return;

    d->usagePattern = usagePattern;
    emit usagePatternChanged(usagePattern);

    update();
}

/*!
 * \qmlproperty enumeration ShaderStorageBuffer::syncToOpenGL
 *
 * This property holds the SSBO synchronisation write to OpenGL value.  It
 * specifies the frequency of writing QML values to the OpenGL buffer.  The
 * default value is SyncWriteNone.
 *
 * syncToOpenGL takes the following values:
 *
 * \value SyncWriteNone
 *   Never write QML values to OpenGL
 * \value SyncWriteOnce
 *   Write QML values to OpenGL on ssbo creation
 * \value SyncWriteOnUpdated
 *   Write QML values to OpenGL whenever the QML values change
 * \value SyncWriteAlways
 *   Write QML values to OpenGL on every scenegraph update
 *
 */
QuickShaderStorageBuffer::SyncWrite QuickShaderStorageBuffer::syncToOpenGL() const
{
    Q_D(const QuickShaderStorageBuffer);

    return d->syncToOpenGL;
}

void QuickShaderStorageBuffer::setSyncToOpenGL(SyncWrite syncToOpenGL)
{
    Q_D(QuickShaderStorageBuffer);

    if (d->syncToOpenGL == syncToOpenGL)
        return;

    d->syncToOpenGL = syncToOpenGL;
    emit syncToOpenGLChanged(syncToOpenGL);

    if(d->syncFromOpenGL&&(d->syncToOpenGL==SyncWriteAlways))
    {
        qWarning() << "QuickOpenGL:" << static_cast<QObject *>(this) << "syncToOpenGL and syncFromOpenGL both set - race conditions may occur";
    }

    update();
}

/*!
 * \qmlproperty bool ShaderStorageBuffer::syncFromOpenGL
 *
 * This property hold the SSBO synchronisation read from OpenGL value.  It
 * specifies whether OpenGL SSBO values should be read back into QML.  The
 * default value is false.
 */
bool QuickShaderStorageBuffer::syncFromOpenGL() const
{
    Q_D(const QuickShaderStorageBuffer);

    return d->syncFromOpenGL;
}

void QuickShaderStorageBuffer::setSyncFromOpenGL(bool syncFromOpenGL)
{
    Q_D(QuickShaderStorageBuffer);

    if (d->syncFromOpenGL == syncFromOpenGL)
        return;

    d->syncFromOpenGL = syncFromOpenGL;
    emit syncFromOpenGLChanged(syncFromOpenGL);

    if(d->syncFromOpenGL&&(d->syncToOpenGL==SyncWriteAlways))
    {
        qWarning() << "QuickOpenGL:" << static_cast<QObject *>(this) << "syncToOpenGL and syncFromOpenGL both set - race conditions may occur";
    }
    update();
}

/*!
 * \qmlproperty int ShaderStorageBuffer::size
 *
 * This property holds the SSBO's size.  If it is less than the introspected
 * GLSL size it is ignored.  The default value is -1.
 */
int QuickShaderStorageBuffer::size() const
{
    Q_D(const QuickShaderStorageBuffer);

    return d->size;
}

void QuickShaderStorageBuffer::setSize(int size)
{
    Q_D(QuickShaderStorageBuffer);

    if (d->size == size)
        return;

    d->size = size;
    emit sizeChanged(size);

    update();
}

void QuickShaderStorageBuffer::setData(QVariant data)
{
    Q_D(QuickShaderStorageBuffer);

    if(data.isValid()&&(!data.canConvert<QByteArray>()))
    {
        qWarning() << "QuickOpenGL:" << static_cast<QObject *>(this) << "cannot convert" << data << "to QByteArray";
        return;
    }

    if(d->data == data.value<QByteArray>())
        return;

    d->data = data.value<QByteArray>();
    d->dataUpdated = true;
    emit dataChanged(d->data);
    update();
}

void QuickShaderStorageBuffer::releaseResources()
{
    Q_D(QuickShaderStorageBuffer);

    if(d->ssboProvider)
    {
        window()->scheduleRenderJob(cleanup(d->ssboProvider), QQuickWindow::BeforeSynchronizingStage);
        d->ssboProvider = nullptr;
    }

}

void QuickShaderStorageBuffer::componentComplete()
{
    Q_D(QuickShaderStorageBuffer);

    // find our private class update slot
    QMetaMethod slot = d->metaObject()->method(d->metaObject()->indexOfMethod("update()"));
    Q_ASSERT(slot.isValid());

    // get our meta object
    const QMetaObject *object = metaObject()->superClass();

    // for each of our properties
    for(int index=object->propertyOffset();index<metaObject()->propertyCount();index++)
    {
        QMetaProperty property = metaObject()->property(index);

        // connect the notify signal to the update slot
        if(property.hasNotifySignal())
        {
            connect(this, property.notifySignal(), d, slot);
        }

        // add the index to the updated indices list
        d->updatedPropertyIndices.append(index);
    }

    QQuickItem::componentComplete();

}

/*!
 * \qmlproperty int ShaderStorageBuffer::bufferObject
 *
 * This read only property holds the shader storage buffer's OpenGL buffer object. It may be used
 * to help decipher OpenGL debug and error messages. Its default value is -1 until an OpenGL buffer
 * has been generated.
 *
 * \sa https://www.khronos.org/opengl/wiki/Buffer_Object
 */

int QuickShaderStorageBuffer::bufferObject() const
{
    Q_D(const QuickShaderStorageBuffer);

    return d->bufferObject;
}

/*!
 * \qmlproperty var ShaderStorageBuffer::data
 *
 * This property holds the shader storage buffer's data. It must be able to be converted to
 * QByteArray. This property allows the shader storage buffer to be written or read directly from
 * QML via either a JavaScript ArrayBuffer, or a Qt type able to be converted to and/or from
 * QByteArray.
 *
 */
QVariant QuickShaderStorageBuffer::data() const
{
    Q_D(const QuickShaderStorageBuffer);

    return d->data;
}

#include "quickshaderstoragebuffer.moc"




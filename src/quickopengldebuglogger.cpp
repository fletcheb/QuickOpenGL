/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quickopengldebuglogger.h"
#include <QQuickWindow>
#include <QSurfaceFormat>
#include <QOpenGLDebugLogger>


/*!
 * \qmltype OpenGLDebugLogger
 * \brief The OpenGLDebugLogger QML type represents an OpenGL KHR_debug log.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickOpenGLDebugLogger
 * \ingroup QuickOpenGL QML Types
 * 
 *
 * OpenGLDebugLogger represents an OpenGL
 * \l{https://www.khronos.org/registry/OpenGL/extensions/KHR/KHR_debug.txt}
 * {GL_KHR_debug} log. It allows a QML application to access detailed OpenGL
 * debug information.  It utilizes \l QOpenGLDebugLogger.
 *
 * \qml
 * OpenGLDebugLogger {
 *   onMessageLogged: console.log(message)
 * }
 * \endqml
 *
 * \sa https://www.khronos.org/registry/OpenGL/extensions/KHR/KHR_debug.txt
 * \sa OpenGLDebugMessage
 * \sa QOpenGLDebugLogger
 */


/*!
 * \qmlsignal OpenGLDebugLogger::messageLogged(message)
 *
 * This signal is emitted when an OpenGL debug message is logged.  It includes
 * the \l OpenGLDebugMessage message.
 *
 * \sa QOpenGLDebugLogger::messageLogged
 */

class QuickOpenGLDebugLoggerPrivate : public QObject
{
    Q_OBJECT

public:

    QuickOpenGLDebugLoggerPrivate(QuickOpenGLDebugLogger *q);

    QOpenGLDebugLogger logger;

public slots:

    void onWindowChanged(QQuickWindow *window);
    void onSceneGraphInitialized();
    void onSceneGraphInvalidated();
    void onMessageLogged(const QOpenGLDebugMessage &message);

protected:

    QuickOpenGLDebugLogger *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickOpenGLDebugLogger)

};

QuickOpenGLDebugLoggerPrivate::QuickOpenGLDebugLoggerPrivate(QuickOpenGLDebugLogger *q) :
    q_ptr(q)
{
}

void QuickOpenGLDebugLoggerPrivate::onWindowChanged(QQuickWindow *window)
{
    if(window)
    {
        connect(window, &QQuickWindow::sceneGraphInvalidated, this, &QuickOpenGLDebugLoggerPrivate::onSceneGraphInvalidated, Qt::DirectConnection);
        connect(window, &QQuickWindow::sceneGraphInitialized, this, &QuickOpenGLDebugLoggerPrivate::onSceneGraphInitialized, Qt::DirectConnection);
    }
}

void QuickOpenGLDebugLoggerPrivate::onSceneGraphInitialized()
{

    if(!QOpenGLContext::currentContext()->hasExtension(QByteArrayLiteral("GL_KHR_debug")))
    {
        qWarning() << "QuickOpenGL" << this << "OpenGL context does not support GL_KHR_debug extension";
    }
    else if(!QOpenGLContext::currentContext()->format().options().testFlag(QSurfaceFormat::DebugContext))
    {
        qWarning() << "QuickOpenGL" << this << "OpenGL context is not a Debug Context";
    }
    else
    {
        logger.initialize();
        logger.startLogging();
    }
}

void QuickOpenGLDebugLoggerPrivate::onSceneGraphInvalidated()
{
    logger.stopLogging();
}

void QuickOpenGLDebugLoggerPrivate::onMessageLogged(const QOpenGLDebugMessage &message)
{
    Q_Q(QuickOpenGLDebugLogger);

    emit q->messageLogged(QuickOpenGLDebugMessage(message));

}

QuickOpenGLDebugLogger::QuickOpenGLDebugLogger(QQuickItem *parent) :
    QQuickItem(parent),
    d_ptr(new QuickOpenGLDebugLoggerPrivate(this))
{
    Q_D(QuickOpenGLDebugLogger);

    connect(this, &QQuickItem::windowChanged, d, &QuickOpenGLDebugLoggerPrivate::onWindowChanged, Qt::DirectConnection);
    connect(&d->logger, &QOpenGLDebugLogger::messageLogged, d, &QuickOpenGLDebugLoggerPrivate::onMessageLogged, Qt::QueuedConnection);

    // set the default surface format to be a debug context
    QSurfaceFormat surfaceFormat = QSurfaceFormat::defaultFormat();
    surfaceFormat.setOption(QSurfaceFormat::DebugContext);
    QSurfaceFormat::setDefaultFormat(surfaceFormat);
}

QuickOpenGLDebugLogger::~QuickOpenGLDebugLogger()
{

}

#include "quickopengldebuglogger.moc"

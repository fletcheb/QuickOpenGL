/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#ifndef QUICKOPENGLSINGLETON_H
#define QUICKOPENGLSINGLETON_H

#include <QObject>
#include "quickvector2.h"
#include "quickvector3.h"
#include "quickvector4.h"
#include "quickmatrix3.h"
#include "quickmatrix4.h"
#include "quickbuffer.h"
#include "support.h"

class QuickOpenGLTypeInstantiator : public QObject
{
    Q_OBJECT


public:


    explicit QuickOpenGLTypeInstantiator(QObject *parent = nullptr);

    Q_INVOKABLE QuickBuffer buffer(int reserveSize = 0) const;

    Q_INVOKABLE qvec2 vec2() const;
    Q_INVOKABLE qvec2 vec2(const QVariant &a) const;
    Q_INVOKABLE qvec2 vec2(const QVariant &a, const QVariant &b) const;

    Q_INVOKABLE qivec2 ivec2() const;
    Q_INVOKABLE qivec2 ivec2(const QVariant &a) const;
    Q_INVOKABLE qivec2 ivec2(const QVariant &a, const QVariant &b) const;

    Q_INVOKABLE quvec2 uvec2() const;
    Q_INVOKABLE quvec2 uvec2(const QVariant &a) const;
    Q_INVOKABLE quvec2 uvec2(const QVariant &a, const QVariant &b) const;

    Q_INVOKABLE qbvec2 bvec2() const;
    Q_INVOKABLE qbvec2 bvec2(const QVariant &a) const;
    Q_INVOKABLE qbvec2 bvec2(const QVariant &a, const QVariant &b) const;

    Q_INVOKABLE qdvec2 dvec2() const;
    Q_INVOKABLE qdvec2 dvec2(const QVariant &a) const;
    Q_INVOKABLE qdvec2 dvec2(const QVariant &a, const QVariant &b) const;

    Q_INVOKABLE qvec3 vec3() const;
    Q_INVOKABLE qvec3 vec3(const QVariant &a) const;
    Q_INVOKABLE qvec3 vec3(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qvec3 vec3(const QVariant &a, const QVariant &b, const QVariant &c) const;

    Q_INVOKABLE qivec3 ivec3() const;
    Q_INVOKABLE qivec3 ivec3(const QVariant &a) const;
    Q_INVOKABLE qivec3 ivec3(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qivec3 ivec3(const QVariant &a, const QVariant &b, const QVariant &c) const;

    Q_INVOKABLE quvec3 uvec3() const;
    Q_INVOKABLE quvec3 uvec3(const QVariant &a) const;
    Q_INVOKABLE quvec3 uvec3(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE quvec3 uvec3(const QVariant &a, const QVariant &b, const QVariant &c) const;

    Q_INVOKABLE qbvec3 bvec3() const;
    Q_INVOKABLE qbvec3 bvec3(const QVariant &a) const;
    Q_INVOKABLE qbvec3 bvec3(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qbvec3 bvec3(const QVariant &a, const QVariant &b, const QVariant &c) const;

    Q_INVOKABLE qdvec3 dvec3() const;
    Q_INVOKABLE qdvec3 dvec3(const QVariant &a) const ;
    Q_INVOKABLE qdvec3 dvec3(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qdvec3 dvec3(const QVariant &a, const QVariant &b, const QVariant &c) const;

    Q_INVOKABLE qvec4 vec4() const;
    Q_INVOKABLE qvec4 vec4(const QVariant &a) const;
    Q_INVOKABLE qvec4 vec4(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qvec4 vec4(const QVariant &a, const QVariant &b, const QVariant &c) const;
    Q_INVOKABLE qvec4 vec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;

    Q_INVOKABLE qivec4 ivec4() const;
    Q_INVOKABLE qivec4 ivec4(const QVariant &a) const;
    Q_INVOKABLE qivec4 ivec4(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qivec4 ivec4(const QVariant &a, const QVariant &b, const QVariant &c) const;
    Q_INVOKABLE qivec4 ivec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;

    Q_INVOKABLE quvec4 uvec4() const;
    Q_INVOKABLE quvec4 uvec4(const QVariant &a) const;
    Q_INVOKABLE quvec4 uvec4(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE quvec4 uvec4(const QVariant &a, const QVariant &b, const QVariant &c) const;
    Q_INVOKABLE quvec4 uvec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;

    Q_INVOKABLE qbvec4 bvec4() const;
    Q_INVOKABLE qbvec4 bvec4(const QVariant &a) const;
    Q_INVOKABLE qbvec4 bvec4(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qbvec4 bvec4(const QVariant &a, const QVariant &b, const QVariant &c) const;
    Q_INVOKABLE qbvec4 bvec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;

    Q_INVOKABLE qdvec4 dvec4() const;
    Q_INVOKABLE qdvec4 dvec4(const QVariant &a) const;
    Q_INVOKABLE qdvec4 dvec4(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qdvec4 dvec4(const QVariant &a, const QVariant &b, const QVariant &c) const;
    Q_INVOKABLE qdvec4 dvec4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;


    Q_INVOKABLE qmat3x2 mat3x2() const;
    Q_INVOKABLE qmat3x2 mat3x2(const QVariant &a) const;
    Q_INVOKABLE qmat3x2 mat3x2(const QVariant &a, const QVariant &b, const QVariant &c) const;
    Q_INVOKABLE qmat3x2 mat3x2(float a, float b,
                               float c, float d,
                               float e, float f);

    Q_INVOKABLE qdmat3x2 dmat3x2() const;
    Q_INVOKABLE qdmat3x2 dmat3x2(const QVariant &a) const;
    Q_INVOKABLE qdmat3x2 dmat3x2(const QVariant &a, const QVariant &b, const QVariant &c) const;
    Q_INVOKABLE qdmat3x2 dmat3x2(double a, double b,
                               double c, double d,
                               double e, double f);

    Q_INVOKABLE qmat4x2 mat4x2() const;
    Q_INVOKABLE qmat4x2 mat4x2(const QVariant &a) const;
    Q_INVOKABLE qmat4x2 mat4x2(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;
    Q_INVOKABLE qmat4x2 mat4x2(float a, float b,
                               float c, float d,
                               float e, float f,
                               float g, float h);

    Q_INVOKABLE qdmat4x2 dmat4x2() const;
    Q_INVOKABLE qdmat4x2 dmat4x2(const QVariant &a) const;
    Q_INVOKABLE qdmat4x2 dmat4x2(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;
    Q_INVOKABLE qdmat4x2 dmat4x2(double a, double b,
                                 double c, double d,
                                 double e, double f,
                                 double g, double h);

    Q_INVOKABLE qmat4x3 mat4x3() const;
    Q_INVOKABLE qmat4x3 mat4x3(const QVariant &a) const;
    Q_INVOKABLE qmat4x3 mat4x3(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;
    Q_INVOKABLE qmat4x3 mat4x3(float a, float b, float c,
                               float d, float e, float f,
                               float g, float h, float i,
                               float j, float k, float l);

    Q_INVOKABLE qdmat4x3 dmat4x3() const;
    Q_INVOKABLE qdmat4x3 dmat4x3(const QVariant &a) const;
    Q_INVOKABLE qdmat4x3 dmat4x3(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;
    Q_INVOKABLE qdmat4x3 dmat4x3(double a, double b, double c,
                                 double d, double e, double f,
                                 double g, double h, double i,
                                 double j, double k, double l);

    Q_INVOKABLE qmat2 mat2() const;
    Q_INVOKABLE qmat2 mat2(const QVariant &a) const;
    Q_INVOKABLE qmat2 mat2(const QVariant &a, const QVariant &b) const;
    Q_INVOKABLE qmat2 mat2(float a, float b,
                           float c, float d) const;

    Q_INVOKABLE qmat4 mat4() const;
    Q_INVOKABLE qmat4 mat4(const QVariant &a) const;
    Q_INVOKABLE qmat4 mat4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;
    Q_INVOKABLE qmat4 mat4(float a, float b, float c, float d,
                           float e, float f, float g, float h,
                           float i, float j, float k, float l,
                           float m, float n, float o, float p) const;

    Q_INVOKABLE qdmat4 dmat4() const;
    Q_INVOKABLE qdmat4 dmat4(const QVariant &a) const;
    Q_INVOKABLE qdmat4 dmat4(const QVariant &a, const QVariant &b, const QVariant &c, const QVariant &d) const;
    Q_INVOKABLE qdmat4 dmat4(double a, double b, double c, double d,
                             double e, double f, double g, double h,
                             double i, double j, double k, double l,
                             double m, double n, double o, double p) const;

};





#endif // QUICKOPENGLSINGLETON_H

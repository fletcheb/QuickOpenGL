/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksguniformbufferprovider.h"
#include <QThread>

class QuickSGUniformBufferProviderPrivate : public QObject
{
    Q_OBJECT

public:

    QuickSGUniformBufferProviderPrivate(QuickSGUniformBufferProvider *q);

    QuickSGUniformBuffer uniformBuffer;
    QMap<uint, Introspection::UniformBlock> uniformBlocks;
    QByteArray data;
    bool updated;

protected:

    QuickSGUniformBufferProvider *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickSGUniformBufferProvider)
};

QuickSGUniformBufferProviderPrivate::QuickSGUniformBufferProviderPrivate(QuickSGUniformBufferProvider *q):
    updated(true),
    q_ptr(q)
{

}

QuickSGUniformBufferProvider::QuickSGUniformBufferProvider():
    d_ptr(new QuickSGUniformBufferProviderPrivate(this))
{
}

QuickSGUniformBufferProvider::~QuickSGUniformBufferProvider()
{
}

void QuickSGUniformBufferProvider::setData(const QByteArray &buffer)
{
    Q_D(QuickSGUniformBufferProvider);
    d->data = buffer;
}

QByteArray QuickSGUniformBufferProvider::data() const
{
    Q_D(const QuickSGUniformBufferProvider);
    return d->data;
}

bool QuickSGUniformBufferProvider::updated() const
{
    Q_D(const QuickSGUniformBufferProvider);

    return d->updated;

}

void QuickSGUniformBufferProvider::setUpdated(bool updated)
{
    Q_D(QuickSGUniformBufferProvider);

    if(updated&&(d->updated!=updated))
    {
        emit uniformBufferChanged();
    }
    d->updated = updated;
}

QuickSGUniformBuffer *QuickSGUniformBufferProvider::uniformBuffer()
{
    Q_D(QuickSGUniformBufferProvider);

    return &d->uniformBuffer;
}

bool QuickSGUniformBufferProvider::setUniformBlock(const Introspection::UniformBlock &uniformBlock)
{
    Q_D(QuickSGUniformBufferProvider);

    bool result = true;

    // add the uniform block for this program to the map table
    d->uniformBlocks[uniformBlock.programId] = uniformBlock;

    // compare against the first added uniform block
    for(int i=1;(i<d->uniformBlocks.count())&&result;++i)
    {
        result &= d->uniformBlocks.values().at(0) == d->uniformBlocks.values().at(i);
    }

    return result;
}

Introspection::UniformBlock QuickSGUniformBufferProvider::uniformBlock() const
{
    Q_D(const QuickSGUniformBufferProvider);

    return d->uniformBlocks.count()?d->uniformBlocks.first():Introspection::UniformBlock();

}

#include "quicksguniformbufferprovider.moc"

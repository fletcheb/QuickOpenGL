/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include <QOpenGLFunctions_4_3_Core>
#include <QOpenGLExtraFunctions>
#include <QOpenGLShader>
#include <QOpenGLShaderProgram>
#include <QQuickWindow>
#include <QRegularExpression>
#include <QSGTextureProvider>
#include <QThread>

#ifdef QT_DEBUG
#include <QElapsedTimer>
#endif

#include "quickcomputeprogram.h"
#include "openglextension_arb_compute_variable_group_size.h"
#include "introspection.h"
#include "support.h"
#include "quicksguniformbufferprovider.h"
#include "quicksgssboprovider.h"

Q_DECLARE_METATYPE(QMetaObject::Connection)

/*!
 * \qmltype ComputeProgram
 * \brief The ComputeProgram QML type represents an OpenGL compute program.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickComputeProgram
 * \ingroup QuickOpenGL QML Types
 *
 * ComputeProgram represents an OpenGL program with a compute shader stage. The
 * program is dispatched by the scene graph on \l
 * QQuickWindow::beforeRendering.
 *
 * ComputeProgram allows custom OpenGL Shader Language (GLSL) compute shaders
 * to be specified directly in QML. Like \l GraphicsProgram, it introspects the
 * OpenGL program and matches GLSL uniforms, uniform blocks, shader storage
 * blocks and subroutine uniforms with user defined QML properties.
 *
 * By default, ComputeProgram will dispatch the compute shader when GLSL matched QML properties
 * change and when ComputeProgram group sizes change. This behaviour can be disabled via the \l
 * dispatchOnUniformPropertyChanges and \l dispatchOnGroupsAndGroupSizeChanges properties. The
 * shader may then be dispatched manually by calling the \l dispatch method. \l dispatch takes a
 * snapshot of the ComputeShader state at the time it is called, then dispatches on the next scene
 * graph render. The ComputeShader state at the time of dispatch is returned via the \l dispatched
 * signal.
 *
 *
 * Example usage:
 *
 * \qml
 * Texture
 * {
 *     id: texture
 *     target: Texture.Target2D
 *     format: Texture.FormatR32F
 *     width: parent.width
 *     height: parent.height
 * }
 *
 * ComputeProgram
 * {
 *     xGroups: texture.width
 *     yGroups: texture.height
 *
 *     property var image : texture
 *     property real frequency: 5.0
 *
 *     computeShader: "
 *             #version 430 core
 *
 *             layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
 *
 *             layout(r32f) uniform image2D image;
 *             uniform float frequency;
 *
 *             void main()
 *             {
 *                 ivec2 size = imageSize(image);
 *                 vec2 xy = vec2(gl_GlobalInvocationID.xy) - vec2(size-ivec2(1))*0.5;
 *                 float value = (1.0 + sin(frequency*dot(xy,xy)/float(size.y)))*0.5;
 *                 imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(value));
 *             }
 *         "
 * }
 *\endqml
 *
 * \sa https://www.khronos.org/opengl/wiki/Compute_Shader
 */


/*!
 * \qmlsignal ComputeProgram::dispatched(state)
 *
 * This signal is emitted when the compute program is dispatched.  It includes
 * a copy of the state of the ComputeProgram QML properties for which the
 * dispatch occured.
 *
 * The state may be enumerated in QML via:
 *
 * \qml
 *  onDispatched: console.log(this + ' dispatched with state:\n' + printState(state))
 *
 *  function printState(state, depth)
 *  {
 *      depth = (typeof depth !== 'undefined') ?  depth : 0
 *      var result = '';
 *      for(var object in state)
 *      {
 *          if(object instanceof Array)
 *              result += printState(object, ++depth)
 *          else
 *          {
 *              for(var i=0;i<depth;++i)
 *                  result += '    '
 *
 *              result +=  '    ' + object + ':\t' + state[object] + '\n'
 *          }
 *      }
 *      return result
 *  }
 * \endqml
 *
 */
class QuickComputeProgramPrivate : public QObject
{
    Q_OBJECT

    QuickComputeProgramPrivate(QuickComputeProgram *q);

    // OpenGL compute program
    QOpenGLShaderProgram *program;

    // GLSL compute shader source code
    QByteArray computeShader;

    // Compute shader dispatch groups
    int xGroups;
    int yGroups;
    int zGroups;

    // ARB_compute_variable_group_size dispatch group sizes
    int xGroupSize;
    int yGroupSize;
    int zGroupSize;

    int glMaxComputeWorkGroupInvocations;
    qivec3 glMaxComputeWorkGroupCount;
    qivec3 glMaxComputeWorkGroupSize;

    bool hasComputeVariableGroupSizeExtension;
    bool shaderRequiresVariableGroupSize;

    bool dispatchOnUniformPropertyChanges;
    bool dispatchOnGroupsAndGroupSizeChanges;

    QuickComputeProgram::Barriers memoryBarrier;
    QOpenGLExtension_ARB_compute_variable_group_size *glExtensionComputeVariableGroupSize;

    // Introspected values
    QList<Introspection::Uniform> uniforms;
    QHash<QString, Introspection::UniformBlock> uniformBlocks;
    QHash<QString, Introspection::ShaderStorageBlock> shaderStorageBlocks;
    QList<Introspection::SubroutineUniform> subroutineUniforms;
    QHash<QString, QList<Introspection::Uniform>> baseUniforms;

    QVariantMap qmlUniforms;
    QVariantMap qmlSubroutineUniforms;
    QVariantMap qmlUniformBlocks;
    QVariantMap qmlShaderStorageBlocks;

    QVariantHash glProperties;

    QList<QVariantHash> jobQueue;
    QList<QVariantHash> dispatchQueue;

    QHash<QObject *, int> dispatchCount;

    bool initialized;

    void introspect();
    void connectProperties();
    void updateProviders();

    QVariantHash properties() const;

public slots:

    void update();
    void onBeforeRendering();
    void onGroupSizingChanged();
    void onWindowChanged(QQuickWindow *window);
    void initialize();

protected:

    QuickComputeProgram *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickComputeProgram)

};

QuickComputeProgramPrivate::QuickComputeProgramPrivate(QuickComputeProgram *q):
    program(nullptr),
    xGroups(1),
    yGroups(1),
    zGroups(1),
    xGroupSize(1),
    yGroupSize(1),
    zGroupSize(1),
    glMaxComputeWorkGroupInvocations(0),
    hasComputeVariableGroupSizeExtension(false),
    shaderRequiresVariableGroupSize(false),
    dispatchOnUniformPropertyChanges(true),
    dispatchOnGroupsAndGroupSizeChanges(true),
    memoryBarrier(QuickComputeProgram::BarrierNone),
    glExtensionComputeVariableGroupSize(nullptr),
    initialized(false),
    q_ptr(q)
{
}

/*!
 * \internal
 * \brief Create and introspect the OpenGL program
 */
void QuickComputeProgramPrivate::introspect()
{
    Q_Q(QuickComputeProgram);

    Q_ASSERT(program==nullptr);

    // Create our compute program
    program = new QOpenGLShaderProgram;
    program->addShaderFromSourceCode(QOpenGLShader::Compute, computeShader);
    program->link();

    // introspect our compute program
    uniformBlocks.clear();
    for(const Introspection::UniformBlock &uniformBlock : Introspection::activeUniformBlocks(program->programId()))
    {
        uniformBlocks[uniformBlock.name] = uniformBlock;
    }

    uniforms = Introspection::activeUniforms(program->programId());

    baseUniforms.clear();
    for(const Introspection::Uniform &uniform: uniforms)
    {
        // Get the base name
        QRegularExpressionMatch match = QRegularExpression(QStringLiteral("([\\w\\d]+)")).match(uniform.name);
        Q_ASSERT(match.hasMatch());
        baseUniforms[match.captured(1)].append(uniform);
    }


    shaderStorageBlocks.clear();
    for(const Introspection::ShaderStorageBlock &shaderStorageBlock : Introspection::activeShaderStorageBlocks(program->programId()))
    {
        shaderStorageBlocks[shaderStorageBlock.name] = shaderStorageBlock;
    }

    subroutineUniforms = Introspection::activeSubroutineUniforms(program->programId(), GL_COMPUTE_SHADER);

    qmlShaderStorageBlocks.clear();
    for(const Introspection::ShaderStorageBlock &block: shaderStorageBlocks)
    {
        QVariantMap object;
        object["name"] = block.name;
        object["index"] = block.index;
        object["binding"] = block.binding;
        object["size"] = block.size;

        QVariantMap variables;
        for(const Introspection::ShaderStorageBlock::Variable &variable: block.variables)
        {
            QVariantMap object;
            object["name"] = QString(variable.name);
            object["type"] = QuickOpenGL::toString(variable.type);
            object["offset"] = variable.offset;
            object["blockIndex"] = variable.blockIndex;
            object["arrayStride"] = variable.arrayStride;
            object["arraySize"] = variable.arraySize;
            object["matrixStride"] = variable.matrixStride;
            object["isRowMajor"] = variable.isRowMajor;
            object["topLevelArraySize"] = variable.topLevelArraySize;
            object["topLevelArrayStride"] = variable.topLevelArrayStride;
            variables[variable.name] = object;
        }

        object["variables"] = variables;
        qmlShaderStorageBlocks[block.name] = object;
    }
    emit q->shaderStorageBlocksChanged(qmlShaderStorageBlocks);

    qmlUniforms.clear();
    for(const Introspection::Uniform &uniform: uniforms)
    {
        QVariantMap object;
        object["name"] = QString(uniform.name);
        object["type"] = QuickOpenGL::toString(uniform.type);
        object["location"] = uniform.location;
        object["arraySize"] = uniform.arraySize;
        object["blockIndex"] = uniform.blockIndex;
        object["offset"] = uniform.offset;
        object["arrayStride"] = uniform.arrayStride;
        object["matrixStride"] = uniform.matrixStride;
        object["isRowMajor"] = uniform.isRowMajor;
        object["atomicCounterBufferIndex"] = uniform.atomicCounterBufferIndex;
        qmlUniforms[uniform.name] = object;
    }
    emit q->uniformsChanged(qmlUniforms);

    qmlSubroutineUniforms.clear();


    for(const Introspection::SubroutineUniform &uniform: subroutineUniforms)
    {
        QVariantMap object;
        object["name"] = QString(uniform.name);
        object["location"] = uniform.location;

        QVariantMap routines;
        for(QHash<QByteArray, int>::const_iterator it = uniform.compatibleSubroutines.constBegin();it!=uniform.compatibleSubroutines.constEnd();++it)
        {
            QVariantMap object;
            object["name"] = QString(it.key());
            object["index"] = it.value();
            routines[it.key()] = object;
        }
        object["compatibleSubroutines"] = routines;
        qmlSubroutineUniforms[uniform.name] = object;
    }

    emit q->subroutineUniformsChanged(qmlSubroutineUniforms);

    qmlUniformBlocks.clear();
    for(const Introspection::UniformBlock &block: uniformBlocks)
    {
        QVariantMap object;
        object["name"] = block.name;
        object["index"] = block.index;
        object["binding"] = block.binding;
        object["size"] = block.size;

        QVariantMap uniforms;
        for(const Introspection::Uniform &uniform: block.uniforms)
        {
            QVariantMap object;
            object["name"] = QString(uniform.name);
            object["type"] = QuickOpenGL::toString(uniform.type);
            object["location"] = uniform.location;
            object["arraySize"] = uniform.arraySize;
            object["blockIndex"] = uniform.blockIndex;
            object["offset"] = uniform.offset;
            object["arrayStride"] = uniform.arrayStride;
            object["matrixStride"] = uniform.matrixStride;
            object["isRowMajor"] = uniform.isRowMajor;
            object["atomicCounterBufferIndex"] = uniform.atomicCounterBufferIndex;
            uniforms[uniform.name] = object;
        }
        object["uniforms"] = uniforms;
        qmlUniformBlocks[block.name] = object;
    }
    emit q->uniformBlocksChanged(qmlUniformBlocks);

}

/*!
 * \internal
 * \brief Connect introspected OpenGL property name QML notify signals
 */
void QuickComputeProgramPrivate::connectProperties()
{
    Q_Q(QuickComputeProgram);

    // list of property names
    QStringList names;

    // add the uniform names
    for(const Introspection::Uniform &uniform: uniforms)
    {
        QString name = uniform.name;
        name.remove(QRegExp("\\[[0-9]\\]"));
        if(!names.contains(name))
        {
            names << name;
        }
    }

    // add the uniform block names
    for(const Introspection::UniformBlock &uniformBlock: uniformBlocks)
    {
        QString name = uniformBlock.name;
        name.remove(QRegExp("\\[[0-9]\\]"));
        if(!names.contains(name))
        {
            names << name;
        }
    }

    // add the shader storage block names
    for(const Introspection::ShaderStorageBlock &shaderStorageBlock: shaderStorageBlocks)
    {
        QString name = shaderStorageBlock.name;
        name.remove(QRegExp("\\[[0-9]\\]"));
        if(!names.contains(name))
        {
            names << name;
        }
    }

    // add the subroutine uniform names
    for(const Introspection::SubroutineUniform &subroutineUniform: subroutineUniforms)
    {
        QString name = subroutineUniform.name;
        name.remove(QRegExp("\\[[0-9]\\]"));
        if(!names.contains(name))
        {
            names << name;
        }
    }

    QMetaMethod slot = metaObject()->method(metaObject()->indexOfMethod("update()"));
    Q_ASSERT(slot.isValid());

    // process the names
    for(const QString &name: names)
    {
        // attempt to get the index of the property name
        int index = q->metaObject()->indexOfProperty(name.toLocal8Bit());

        // if the index is valid
        if(index!=-1)
        {
            // get the property
            QMetaProperty property = q->metaObject()->property(index);

            // if the property has a notify signal, connect it
            if(property.hasNotifySignal())
            {
                QMetaObject::Connection connection = connect(q, property.notifySignal(), this, slot);
                Q_ASSERT(connection);
            }
        }
        else
        {
            qWarning() << "QuickOpenGL:" << reinterpret_cast<QObject *>(q) << "is missing required compute shader property" << name;
        }
    }

}

// flatten a QVariantList of QVariantList etc structure and convert to a type
template<class T> QList<T> flatten(const QVariant &variant)
{
    QList<T> result;
    if(variant.canConvert<QVariantList>())
    {
        for(const QVariant &v: variant.value<QVariantList>())
        {
            result += flatten<T>(v);
        }
    }
    else if(variant.canConvert<T>())
    {
        result.append(variant.value<T>());
    }

    return result;
}

/*!
 * \internal
 * \brief Update bound image list
 *
 * Problem: bindImageTexture and setUniform connects our dispatch signal to
 * QSGTextureProvider::textureChanged for any texture that is one of our
 * properties, and has image access write. This means that every time we emit
 * dispatch we then receive a textureChanged signal which could trigger another
 * dispatch. We stop this emit incest by keeping a count of the number of times
 * dispatch has been emitted, and the number of times textureChanged has been
 * received for each image property.
 */
void QuickComputeProgramPrivate::updateProviders()
{
    Q_Q(QuickComputeProgram);

    // the connection name we are looking for
    QString connectionName = QString("dispatch%1").arg(qint64(q));

    // process the uniforms
    for(const Introspection::Uniform &uniform: uniforms)
    {
        // get the uniform name
        QString name = uniform.name;

        // remove any array indexing information
        name.remove(QRegExp("\\[[0-9]\\]"));

        // attempt to get the index of the property name
        int index = q->metaObject()->indexOfProperty(name.toLocal8Bit());

        // if the index is valid
        if(index!=-1)
        {
            // read the property value
            QVariant variant = Introspection::conditionVariant(q->metaObject()->property(index).read(q), this);

            // flatten the variant (could be a QVariantList of QVariantLists) into QObject pointers
            QList<QObject *> providers = flatten<QObject *>(variant);

            for(QObject *provider: providers)
            {
                QObject *object = provider;

                if(qobject_cast<QuickSGImageHandleProvider *>(object))
                {
                    object = qobject_cast<QuickSGImageHandleProvider *>(object)->sourceProvider();
                }

                QVariant connection = object->property(connectionName.toLocal8Bit());

                if(!connection.isValid())
                {

                    if(object->metaObject()->indexOfSignal("textureChanged()")!=-1)
                    {
                        connection = QVariant::fromValue(QObject::connect(q, SIGNAL(dispatched(QVariantMap)), object, SIGNAL(textureChanged())));
                    }

                    Q_ASSERT(connection.value<QMetaObject::Connection>());
                    object->setProperty(connectionName.toLocal8Bit().constData(), connection);

                }

                // create a dispatch count record to avoid emitted signal incest
                if(!dispatchCount.contains(provider))
                {
                    dispatchCount[provider] = 0;
                }

            }
        }
    }
}

/*!
 * \internal
 * \brief Get the QML properties for our GLSL variables
 * \return a hash table of property name - value pairs
 */
QVariantHash QuickComputeProgramPrivate::properties() const
{
    Q_Q(const QuickComputeProgram);

    QVariantHash properties;
    for(const Introspection::Uniform &uniform: uniforms)
    {
        QString name = QString(uniform.name).remove(QRegExp("\\[[0-9]\\]"));
        QVariant property = q->property(name.toLocal8Bit());
        if(q->property(name.toLocal8Bit()).isValid())
        {
            properties[name] = q->property(name.toLocal8Bit());
        }
    }

    for(const Introspection::UniformBlock &uniformBlock: uniformBlocks)
    {
        QString name = QString(uniformBlock.name).remove(QRegExp("\\[[0-9]\\]"));
        if(q->property(name.toLocal8Bit()).isValid())
        {
            properties[name] = q->property(name.toLocal8Bit());
        }
    }

    for(const Introspection::ShaderStorageBlock &shaderStorageBlock: shaderStorageBlocks)
    {
        QString name = QString(shaderStorageBlock.name).remove(QRegExp("\\[[0-9]\\]"));
        if(q->property(name.toLocal8Bit()).isValid())
        {
            properties[name] = q->property(name.toLocal8Bit());
        }
    }

    for(const Introspection::SubroutineUniform &subroutineUniform: subroutineUniforms)
    {
        QString name = QString(subroutineUniform.name).remove(QRegExp("\\[[0-9]\\]"));
        if(q->property(name.toLocal8Bit()).isValid())
        {
            properties[name] = q->property(name.toLocal8Bit());
        }
    }

    return properties;
}



/*!
 * \internal
 * \brief Handle updates from properties
 */
void QuickComputeProgramPrivate::update()
{
    Q_Q(QuickComputeProgram);

    if(dispatchCount.contains(sender()))
    {

        if(--dispatchCount[sender()]<0)
        {
            dispatchCount[sender()]=0;

            if(dispatchOnUniformPropertyChanges)
            {
                q->update();
            }
        }
    }
    else
    {
        if(dispatchOnUniformPropertyChanges)
        {
            q->update();
        }
    }

}

/*!
 * \internal
 * \brief Dispatch compute jobs
 *
 * Run after scene graph synchronisation and before rendering
 */
void QuickComputeProgramPrivate::onBeforeRendering()
{
    Q_Q(QuickComputeProgram);

//    QElapsedTimer timer;
//    timer.start();

    if(program)
    {

        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();
        int glActiveTexture;
        int glSamplerBinding;
        glExtra->glGetIntegerv(GL_ACTIVE_TEXTURE, &glActiveTexture);debugGL;
        glExtra->glGetIntegerv(GL_SAMPLER_BINDING, &glSamplerBinding);debugGL;

        // bind the program
        program->bind();debugGL;

        for(QVariantHash &properties: dispatchQueue)
        {

            int xGroups = properties["xGroups"].toInt();
            int yGroups = properties["yGroups"].toInt();
            int zGroups = properties["zGroups"].toInt();

            int xGroupSize = properties["xGroupSize"].toInt();
            int yGroupSize = properties["yGroupSize"].toInt();
            int zGroupSize = properties["zGroupSize"].toInt();

            if(!(xGroups&&yGroups&&zGroups&&xGroupSize&&yGroupSize&&zGroupSize))
            {
                continue;
            }

            // set the property uniforms
            Introspection::setUniforms(uniforms, properties);debugGL;
            Introspection::setUniformBlocks(uniformBlocks.values(), properties, program->programId());debugGL;
            Introspection::setShaderStorageBlocks(shaderStorageBlocks.values(), properties);debugGL;
            Introspection::setSubroutineUniforms(GL_COMPUTE_SHADER, subroutineUniforms, properties);

            // if there is a memory barrier set
            if(memoryBarrier)
            {
                glExtra->glMemoryBarrier(memoryBarrier);debugGL;
            }

            // if our shader requires the GL ARB_compute_variable_group_size extension
            if(shaderRequiresVariableGroupSize)
            {

                if(!glExtensionComputeVariableGroupSize&&hasComputeVariableGroupSizeExtension)
                {
                    glExtensionComputeVariableGroupSize = new QOpenGLExtension_ARB_compute_variable_group_size;
                    glExtensionComputeVariableGroupSize->initializeOpenGLFunctions();
                }

                if(glExtensionComputeVariableGroupSize)
                {
                    glExtensionComputeVariableGroupSize->glDispatchComputeGroupSizeARB(xGroups, yGroups, zGroups, xGroupSize, yGroupSize, zGroupSize);debugGL;
                }
                else
                {
                    qWarning() << "QuickOpenGL:" << q << "Attempting to dispatch a variable group size compute shader in a context that does not support the GL_ARB_compute_variable_group_size extension";
                }
            }
            else
            {
                glExtra->glDispatchCompute(xGroups, yGroups, zGroups);debugGL;
            }

        }

//        qDebug() << Q_FUNC_INFO << double(timer.nsecsElapsed())*1e-6 << dispatchQueue.count();

        dispatchQueue.clear();

        program->release();debugGL;

        glExtra->glActiveTexture(glActiveTexture);debugGL;
        glExtra->glBindSampler(glActiveTexture-GL_TEXTURE0, glSamplerBinding);debugGL;

    }

}

void QuickComputeProgramPrivate::onGroupSizingChanged()
{
    Q_Q(QuickComputeProgram);

    if(dispatchOnGroupsAndGroupSizeChanges)
    {
        q->update();
    }
}

void QuickComputeProgramPrivate::onWindowChanged(QQuickWindow *window)
{
    if(window)
    {
        connect(window, &QQuickWindow::sceneGraphInitialized, this, &QuickComputeProgramPrivate::initialize, Qt::DirectConnection);
        connect(window, &QQuickWindow::beforeRendering, this, &QuickComputeProgramPrivate::onBeforeRendering, Qt::DirectConnection);
    }
}

void QuickComputeProgramPrivate::initialize()
{
    Q_Q(QuickComputeProgram);

    // check if we have the ARB compute variable group size extension
    hasComputeVariableGroupSizeExtension = QOpenGLContext::currentContext()->hasExtension(QByteArrayLiteral("GL_ARB_compute_variable_group_size"));
    emit q->hasComputeVariableGroupSizeExtensionChanged(hasComputeVariableGroupSizeExtension);

    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
    glExtra->initializeOpenGLFunctions();

    glExtra->glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &glMaxComputeWorkGroupInvocations);debugGL;
    glExtra->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &glMaxComputeWorkGroupCount(0,0));debugGL;
    glExtra->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &glMaxComputeWorkGroupCount(1,0));debugGL;
    glExtra->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &glMaxComputeWorkGroupCount(2,0));debugGL;
    glExtra->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &glMaxComputeWorkGroupSize(0,0));debugGL;
    glExtra->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &glMaxComputeWorkGroupSize(1,0));debugGL;
    glExtra->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &glMaxComputeWorkGroupSize(2,0));debugGL;


    // introspect our OpenGL program
    introspect();

    // connect our properties
    connectProperties();

    // get our properties
    QVariantHash properties = Introspection::conditionVariant(this->properties(), this).toHash();

    // update our texture providers
    updateProviders();

    //        // set our uniforms etc to ensure update signals from properties are connected
    //        d->program->bind();
    //        Introspection::setUniforms(d->uniforms, properties);debugGL;
    //        Introspection::setUniformBlocks(d->uniformBlocks.values(), properties, d->program->programId());debugGL;
    //        Introspection::setShaderStorageBlocks(d->shaderStorageBlocks.values(), properties);debugGL;
    //        Introspection::setSubroutineUniforms(GL_COMPUTE_SHADER, d->subroutineUniforms, properties);
    //        QOpenGLContext::currentContext()->functions()->glActiveTexture(GL_TEXTURE0);
    //        d->program->release();


    // set the initialized flag
    initialized = true;

}

QuickComputeProgram::QuickComputeProgram(QQuickItem *parent):
    QQuickItem(parent),
    d_ptr(new QuickComputeProgramPrivate(this))
{
    Q_D(QuickComputeProgram);

    setFlag(ItemHasContents, true);

    connect(this, &QuickComputeProgram::xGroupsChanged, d, &QuickComputeProgramPrivate::onGroupSizingChanged);
    connect(this, &QuickComputeProgram::yGroupsChanged, d, &QuickComputeProgramPrivate::onGroupSizingChanged);
    connect(this, &QuickComputeProgram::zGroupsChanged, d, &QuickComputeProgramPrivate::onGroupSizingChanged);
    connect(this, &QuickComputeProgram::xGroupSizeChanged, d, &QuickComputeProgramPrivate::onGroupSizingChanged);
    connect(this, &QuickComputeProgram::yGroupSizeChanged, d, &QuickComputeProgramPrivate::onGroupSizingChanged);
    connect(this, &QuickComputeProgram::zGroupSizeChanged, d, &QuickComputeProgramPrivate::onGroupSizingChanged);

    connect(this, &QQuickItem::windowChanged, d, &QuickComputeProgramPrivate::onWindowChanged, Qt::DirectConnection);

}

QuickComputeProgram::~QuickComputeProgram()
{
}

/*!
 * \qmlproperty string ComputeProgram::computeShader
 *
 * This property holds the compute program's compute shader source code, or the
 * name of a file containing the code.
 *
 * A qrc resource file may be specified as follows:
 *
 * \code
 * computeShader: ':/myshader.comp'
 * \endcode
 *
 * \note To aid debugging, when inlining shader source code in QML, add a
 * '#line xxx' statement on the line following the #version statement. xxx
 * should equal the .qml file line number at which the #line statement
 * resides plus one. The GLSL compiler will then report errors using the .qml
 * file line numbers.
 *
 * \sa https://www.khronos.org/opengl/wiki/Compute_Shader
 *
 */
QByteArray QuickComputeProgram::computeShader() const
{
    Q_D(const QuickComputeProgram);

    return d->computeShader;
}

void QuickComputeProgram::setComputeShader(QByteArray computeShader)
{
    Q_D(QuickComputeProgram);

    if(d->initialized)
    {
        qWarning() << "QuickOpenGL" << this << "cannot change compute shader after scenegraph node initialized";
        return;
    }

    if (d->computeShader == computeShader.trimmed())
        return;

    // if the shader does not contain "void main()"
    if(!QString(computeShader).contains(QRegExp("void \\s*main\\s*\\([\\svoid]*\\)")))
    {
        QString filename = QString(computeShader);
        if(filename.startsWith("qrc"))
        {
            filename.remove(0,3);
        }

        QFile file(filename);
        if(file.exists()&&file.open(QIODevice::ReadOnly))
        {
            computeShader = file.readAll();
        }
        else
        {
            qWarning() << "QuickOpenGL" << this << "could not open compute shader source file" << computeShader;
        }
    }

    d->computeShader = computeShader.trimmed();

    QRegularExpression re("#extension[ ]+GL_ARB_compute_variable_group_size[ :]+enable", QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch match = re.match(d->computeShader);

    d->shaderRequiresVariableGroupSize = match.hasMatch();

    emit computeShaderChanged(d->computeShader);

    update();
}

/*!
 * \qmlproperty int ComputeProgram::xGroups
 *
 * This property holds the computer program's number of x groups to be
 * dispatched. The default value is 1.
 */
int QuickComputeProgram::xGroups() const
{
    Q_D(const QuickComputeProgram);

    return d->xGroups;
}

void QuickComputeProgram::setXGroups(int xGroups)
{
    Q_D(QuickComputeProgram);

    if (d->xGroups == xGroups)
        return;

    d->xGroups = xGroups;
    emit xGroupsChanged(xGroups);
}

/*!
 * \qmlproperty int ComputeProgram::yGroups
 *
 * This property holds the computer program's number of y groups to be
 * dispatched. The default value is 1.
 */
int QuickComputeProgram::yGroups() const
{
    Q_D(const QuickComputeProgram);

    return d->yGroups;
}

void QuickComputeProgram::setYGroups(int yGroups)
{
    Q_D(QuickComputeProgram);

    if (d->yGroups == yGroups)
        return;

    d->yGroups = yGroups;
    emit yGroupsChanged(yGroups);
}

/*!
 * \qmlproperty int ComputeProgram::zGroups
 *
 * This property holds the computer program's number of z groups to be
 * dispatched. The default value is 1.
 */
int QuickComputeProgram::zGroups() const
{
    Q_D(const QuickComputeProgram);

    return d->zGroups;
}

void QuickComputeProgram::setZGroups(int zGroups)
{
    Q_D(QuickComputeProgram);

    if (d->zGroups == zGroups)
        return;

    d->zGroups = zGroups;
    emit zGroupsChanged(zGroups);
}

/*!
 * \qmlproperty int ComputeProgram::xGroupSize
 *
 * This property holds the compute program's x group size to be
 * dispatched. The default value is 1.
 *
 * \note This property requires the OpenGL GL_ARB_compute_variable_group_size
 * extension to be present.
 *
 * \sa hasComputeVariableGroupSizeExtension
 */
int QuickComputeProgram::xGroupSize() const
{
    Q_D(const QuickComputeProgram);

    return d->xGroupSize;
}

void QuickComputeProgram::setXGroupSize(int xGroupSize)
{
    Q_D(QuickComputeProgram);

    if(d->initialized&&!d->shaderRequiresVariableGroupSize)
    {
        qWarning() << "QuickOpenGL:" << this << "attempting to set xGroupSize for a compute shader that has not enabled GL_ARB_compute_variable_group_size extension";
    }

    if (d->xGroupSize == xGroupSize)
        return;

    d->xGroupSize = xGroupSize;
    emit xGroupSizeChanged(xGroupSize);

}

/*!
 * \qmlproperty int ComputeProgram::yGroupSize
 *
 * This property holds the compute program's y group size to be
 * dispatched. The default value is 1.
 *
 * \note This property requires the OpenGL GL_ARB_compute_variable_group_size
 * extension to be present.
 *
 * \sa hasComputeVariableGroupSizeExtension
 */
int QuickComputeProgram::yGroupSize() const
{
    Q_D(const QuickComputeProgram);

    return d->yGroupSize;
}

void QuickComputeProgram::setYGroupSize(int yGroupSize)
{
    Q_D(QuickComputeProgram);

    if(d->initialized&&!d->shaderRequiresVariableGroupSize)
    {
        qWarning() << "QuickOpenGL:" << this << "attempting to set yGroupSize for a compute shader that has not enabled GL_ARB_compute_variable_group_size extension";
    }

    if (d->yGroupSize == yGroupSize)
        return;

    d->yGroupSize = yGroupSize;
    emit yGroupSizeChanged(yGroupSize);
}

/*!
 * \qmlproperty int ComputeProgram::zGroupSize
 *
 * This property holds the compute program's z group size to be
 * dispatched. The default value is 1.
 *
 * \note This property requires the OpenGL GL_ARB_compute_variable_group_size
 * extension to be present.
 *
 * \sa hasComputeVariableGroupSizeExtension
 */
int QuickComputeProgram::zGroupSize() const
{
    Q_D(const QuickComputeProgram);

    return d->zGroupSize;
}

void QuickComputeProgram::setZGroupSize(int zGroupSize)
{
    Q_D(QuickComputeProgram);

    if(d->initialized&&!d->shaderRequiresVariableGroupSize)
    {
        qWarning() << "QuickOpenGL:" << this << "attempting to set zGroupSize for a compute shader that has not enabled GL_ARB_compute_variable_group_size extension";
    }

    if (d->zGroupSize == zGroupSize)
        return;

    d->zGroupSize = zGroupSize;
    emit zGroupSizeChanged(zGroupSize);

}

/*!
 * \qmlproperty enumeration ComputeProgram::memoryBarrier
 *
 * This property holds the compute program's OpenGL memory barrier state. It
 * defines a barrier ordering the memory transactions issued prior to the
 * command relative to those issued after the barrier. For the purposes of this
 * ordering, memory transactions performed by shaders are considered to be
 * issued by the rendering command that triggered the execution of the shader.
 * memoryBarrier is a bitfield indicating the set of operations that are
 * synchronized with shader stores.
 *
 * memoryBarrier represents how the data written from an image load/store
 * shader operation is intended to be used. If an image is written to in a
 * rendering command, then read from as a texture, BarrierTextureFetch should
 * be used. If instead the image is attached to an FBO and blending is used to
 * do a read/modify/write operation, then BarrierFramebuffer should be used.
 *
 * The memory barrier is inserted prior to insertion of ComputeProgram's
 * dispatch command. The default value is BarrierNone.
 *
 * memoryBarrier is a bitwise combination of:
 *
 * \value BarrierNone
 *   No memory barrier.
 * \value BarrierVertexAttribArray
 *   GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT
 * \value BarrierElementArray
 *   GL_ELEMENT_ARRAY_BARRIER_BIT
 * \value BarrierUniform
 *   GL_UNIFORM_BARRIER_BIT
 * \value BarrierTextureFetch
 *   GL_TEXTURE_FETCH_BARRIER_BIT
 * \value BarrierShaderImageAccess
 *   GL_SHADER_IMAGE_ACCESS_BARRIER_BIT
 * \value BarrierCommand
 *   GL_COMMAND_BARRIER_BIT,
 * \value BarrierPixelBuffer
 *   GL_PIXEL_BUFFER_BARRIER_BIT
 * \value BarrierTextureUpdate
 *   GL_TEXTURE_UPDATE_BARRIER_BIT
 * \value BarrierBufferUpdate
 *   GL_BUFFER_UPDATE_BARRIER_BIT
 * \value BarrierFramebuffer
 *   GL_FRAMEBUFFER_BARRIER_BIT
 * \value BarrierTransformFeedback
 *   GL_TRANSFORM_FEEDBACK_BARRIER_BIT
 * \value BarrierAtomicCounter
 *   GL_ATOMIC_COUNTER_BARRIER_BIT
 * \value BarrierShaderStorage
 *   GL_SHADER_STORAGE_BARRIER_BIT
 * \value BarrierClientMappedBuffer
 *   GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT
 * \value BarrierQueryBuffer
 *   GL_QUERY_BUFFER_BARRIER_BIT
 * \value BarrierAll
 *   GL_ALL_BARRIER_BITS
 *
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glMemoryBarrier
 */
QuickComputeProgram::Barriers QuickComputeProgram::memoryBarrier() const
{
    Q_D(const QuickComputeProgram);

    return d->memoryBarrier;
}

void QuickComputeProgram::setMemoryBarrier(Barriers memoryBarrier)
{
    Q_D(QuickComputeProgram);

    if (d->memoryBarrier == memoryBarrier)
        return;

    d->memoryBarrier = memoryBarrier;
    emit memoryBarrierChanged(memoryBarrier);
}

/*!
 * \qmlproperty bool ComputeProgram::dispatchOnUniformPropertyChanges
 *
 * This property holds a flag to enable automatic dispatch of the compute shader when QML properties
 * associated with GLSL uniforms change.  The default value is true.
 */
bool QuickComputeProgram::dispatchOnUniformPropertyChanges() const
{
    Q_D(const QuickComputeProgram);

    return d->dispatchOnUniformPropertyChanges;
}

void QuickComputeProgram::setDispatchOnUniformPropertyChanges(bool dispatchOnUniformPropertyChanges)
{
    Q_D(QuickComputeProgram);

    if (d->dispatchOnUniformPropertyChanges == dispatchOnUniformPropertyChanges)
        return;

    d->dispatchOnUniformPropertyChanges = dispatchOnUniformPropertyChanges;
    emit dispatchOnUniformPropertyChangesChanged(dispatchOnUniformPropertyChanges);
}

/*!
 * \qmlproperty bool ComputeProgram::dispatchOnGroupsAndGroupSizeChanges
 *
 * This property holds a flag to enable automatic dispatch of the compute shader when QML specified
 * compute group and group sizes change.  The default value is true.
 */
bool QuickComputeProgram::dispatchOnGroupsAndGroupSizeChanges() const
{
    Q_D(const QuickComputeProgram);

    return d->dispatchOnGroupsAndGroupSizeChanges;
}

void QuickComputeProgram::setDispatchOnGroupsAndGroupSizeChanges(bool dispatchOnGroupsAndGroupSizeChanges)
{
    Q_D(QuickComputeProgram);
    if (d->dispatchOnGroupsAndGroupSizeChanges == dispatchOnGroupsAndGroupSizeChanges)
        return;

    d->dispatchOnGroupsAndGroupSizeChanges = dispatchOnGroupsAndGroupSizeChanges;
    emit dispatchOnGroupsAndGroupSizeChangesChanged(dispatchOnGroupsAndGroupSizeChanges);
}

/*!
 * \qmlproperty bool ComputeProgram::hasComputeVariableGroupSizeExtension
 *
 * This property reflects support for the GL_ARB_compute_variable_group_size within the current
 * OpenGL context.
 *
 * \sa https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_compute_variable_group_size.txt
 */
bool QuickComputeProgram::hasComputeVariableGroupSizeExtension() const
{
    Q_D(const QuickComputeProgram);

    return d->hasComputeVariableGroupSizeExtension;
}

QVariantMap QuickComputeProgram::uniforms() const
{
    Q_D(const QuickComputeProgram);

    return d->qmlUniforms;
}

QVariantMap QuickComputeProgram::subroutineUniforms() const
{
    Q_D(const QuickComputeProgram);

    return d->qmlSubroutineUniforms;
}

QVariantMap QuickComputeProgram::uniformBlocks() const
{
    Q_D(const QuickComputeProgram);

    return d->qmlUniformBlocks;
}

QVariantMap QuickComputeProgram::shaderStorageBlocks() const
{
    Q_D(const QuickComputeProgram);

    return d->qmlShaderStorageBlocks;
}

/*!
 * \qmlmethod ComputeProgram::dispatch()
 *
 * This method saves the current ComputeProgram state, and schedules it for dispatch on the next
 * scene graph update.  The state includes the current value of all compute shader uniforms.
 */
void QuickComputeProgram::dispatch()
{
    Q_D(QuickComputeProgram);

    if(d->xGroups&&d->yGroups&&d->zGroups&&d->xGroupSize&&d->yGroupSize&&d->zGroupSize)
    {
        // capture the current state into a job
        QVariantHash job = d->properties();
        job["xGroups"] = d->xGroups;
        job["yGroups"] = d->yGroups;
        job["zGroups"] = d->zGroups;
        job["xGroupSize"] = d->xGroupSize;
        job["yGroupSize"] = d->yGroupSize;
        job["zGroupSize"] = d->zGroupSize;

        // add the job to the queue
        d->jobQueue.append(job);

        update();
    }
}

void QuickComputeProgram::invalidateSceneGraph()
{
    Q_D(QuickComputeProgram);

    if(d->program)
    {
        delete d->program;
        d->program = nullptr;
    }

    if(d->glExtensionComputeVariableGroupSize)
    {
        delete d->glExtensionComputeVariableGroupSize;
        d->glExtensionComputeVariableGroupSize = nullptr;
    }

}


QSGNode *QuickComputeProgram::updatePaintNode(QSGNode *, QQuickItem::UpdatePaintNodeData *)
{
    Q_D(QuickComputeProgram);

    if(!d->initialized)
    {
        d->initialize();
    }

#ifdef QT_DEBUG
    QScopedPointer<QElapsedTimer> elapsedTimer;
    QList<QPair<QString, qint64>> debugEvents;

    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        elapsedTimer.reset(new QElapsedTimer);
        elapsedTimer->start();
    }
#endif


    if((d->dispatchOnGroupsAndGroupSizeChanges||d->dispatchOnUniformPropertyChanges)&&d->xGroups&&d->yGroups&&d->zGroups&&d->xGroupSize&&d->yGroupSize&&d->zGroupSize)
    {
        QVariantHash job = d->properties();
        job["xGroups"] = d->xGroups;
        job["yGroups"] = d->yGroups;
        job["zGroups"] = d->zGroups;
        job["xGroupSize"] = d->xGroupSize;
        job["yGroupSize"] = d->yGroupSize;
        job["zGroupSize"] = d->zGroupSize;

        // add the job to the queue
        d->jobQueue.append(job);
    }

    // condition the properties (can only be done in updatePaintNode)
    for(QVariantHash job: d->jobQueue)
    {
        int xGroups = job["xGroups"].toInt();
        int yGroups = job["yGroups"].toInt();
        int zGroups = job["zGroups"].toInt();
        int xGroupSize = job["xGroupSize"].toInt();
        int yGroupSize = job["yGroupSize"].toInt();
        int zGroupSize = job["zGroupSize"].toInt();

        if(xGroups>d->glMaxComputeWorkGroupCount.x()||yGroups>d->glMaxComputeWorkGroupCount.y()||zGroups>d->glMaxComputeWorkGroupCount.z())
        {
            qWarning().nospace() << "QuickOpenGL: " << this << " Attempting to dispatch with work group count: (" << xGroups << ", " << yGroups << ", " << zGroups << ") larger than hardware supports (" << d->glMaxComputeWorkGroupCount.x() << ", " << d->glMaxComputeWorkGroupCount.y() << ", " << d->glMaxComputeWorkGroupCount.z() << ")";
            continue;
        }

        if(d->shaderRequiresVariableGroupSize&&(xGroupSize>d->glMaxComputeWorkGroupSize.x()||yGroupSize>d->glMaxComputeWorkGroupSize.y()||zGroupSize>d->glMaxComputeWorkGroupSize.z()))
        {
            qWarning().nospace() << "QuickOpenGL: " << this << " Attempting to dispatch with work group size: (" << xGroupSize << ", " << yGroupSize << ", " << zGroupSize << ") larger than hardware supports (" << d->glMaxComputeWorkGroupSize.x() << ", " << d->glMaxComputeWorkGroupSize.y() << ", " << d->glMaxComputeWorkGroupSize.z() << ")";
            continue;
        }

        for(int &dispatches:d->dispatchCount)
        {
            dispatches++;
        }

        emit dispatched(QVariant::fromValue(job).value<QVariantMap>());

        QVariantHash properties = Introspection::conditionVariant(job, d).toHash();

        QVariantHash result;
        for(const QString &property: job.keys())
        {
            QVariant variant = Introspection::conditionVariant(job[property]);
            result[property] = variant;

            if(d->baseUniforms.contains(property))
            {
                for(const Introspection::Uniform &uniform: d->baseUniforms[property])
                {
                    result[uniform.name] = Introspection::conditionProperty(variant, uniform.name, uniform.type, uniform.arraySize, "uniform", this);
                }
            }

            else if(d->uniformBlocks.contains(property))
            {
                if(variant.canConvert<QuickSGUniformBufferProvider *>())
                {
                    QuickSGUniformBufferProvider *provider = variant.value<QuickSGUniformBufferProvider *>();
                    bool ok = provider->setUniformBlock(d->uniformBlocks[property]);

                    if(!ok)
                    {
                        qWarning()<< "QuickOpenGL:" << static_cast<const QObject *>(this) << "uniform block property" << QString(property)  << ":" << variant << "has inconsistent OpenGL uniform block definitions";
                    }
                }
                else
                {
                    qWarning()<< "QuickOpenGL:" << static_cast<const QObject *>(this) << "uniform block property" << QString(property)  << ":" << variant << "cannot be converted to \"QuickSGUniformBufferProvider *\"";
                }
            }
            else if(d->shaderStorageBlocks.contains(property))
            {
                if(variant.canConvert<QuickSGSSBOProvider *>())
                {
                    QuickSGSSBOProvider *provider = variant.value<QuickSGSSBOProvider *>();
                    bool ok = provider->setShaderStorageBlock(d->shaderStorageBlocks[property]);

                    if(!ok)
                    {
                        qWarning()<< "QuickOpenGL:" << static_cast<const QObject *>(this) << "shader storage block property" << QString(property)  << ":" << variant << "has inconsistent OpenGL shader storage block definitions";
                    }

                }
                else
                {
                    qWarning()<< "QuickOpenGL:" << static_cast<const QObject *>(this) << "shader storage block property" << QString(property)  << ":" << variant << "cannot be converted to \"QuickSGSSBOProvider *\"";
                }

            }

        }

        d->dispatchQueue.append(result);
    }

    d->jobQueue.clear();

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        QString string;
        QTextStream stream(&string);

        for(int i=0;i<debugEvents.count();++i)
        {
            stream << debugEvents[i].first << ": ";
            stream << double(i?debugEvents[i].second-debugEvents[i-1].second:debugEvents[i].second)*1e-6 << "ms" << (i==debugEvents.count()-1?"":", ");
        }
        qDebug().nospace() << "QuickOpenGL: " << reinterpret_cast<QObject *>(this) << " updatePaintNode took " << double(elapsedTimer->nsecsElapsed())*1e-6 << "ms (" << string << ")";
    }
#endif
    return nullptr;
}

void QuickComputeProgram::releaseResources()
{
    Q_D(QuickComputeProgram);

    if(d->program)
    {
        window()->scheduleRenderJob(cleanup(d->program), QQuickWindow::NoStage);
        d->program = nullptr;
    }

    if(d->glExtensionComputeVariableGroupSize)
    {
        window()->scheduleRenderJob(cleanup(d->glExtensionComputeVariableGroupSize), QQuickWindow::NoStage);
        d->glExtensionComputeVariableGroupSize = nullptr;
    }
}

#include "quickcomputeprogram.moc"


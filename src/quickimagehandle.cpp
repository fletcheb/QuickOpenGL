/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include <QOpenGLFunctions_3_3_Core>
#include <QQuickWindow>
#include <QSGTextureProvider>
#include <QThread>
#include "quickimagehandle.h"
#include "quicksgimagehandleprovider.h"
#include "openglextension_arb_bindless_texture.h"
#include "support.h"


/*!
 * \qmltype ImageHandle
 * \brief The ImageHandle QML type represents an OpenGL image handle.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickImageHandle
 * \ingroup QuickOpenGL QML Types
 *
 *
 * ImageHandle allows OpenGL ARB bindless textures to be used directly in
 * QML. It takes a texture provided by a source QML object, and generates an
 * OpenGL ARB bindless image handle. It is intended to be used as a \l QML
 * property within a GraphicsProgram or \l ComputeProgram matched to a
 * corresponding OpenGL shader uint64_t uniform.
 *
 * Example Usage:
 *
 * \qml
 *  // Texture with mip levels
 *  Texture
 *  {
 *      id: imageTexture
 *      target: Texture.Target2D
 *      format: Texture.FormatR32F
 *      width: 1024
 *      height: 1024
 *      mipLevels: 10
 *      minFilter: Texture.FilterNearestMipMapNearest
 *      magFilter: Texture.FilterNearest
 *  }
 *
 *  // List of image handles
 *  property var imageHandles: []
 *
 *  // Create an image handle for each texture mip level
 *  Repeater
 *  {
 *      id: imageRepeater
 *      model: imageTexture.mipLevels
 *
 *      ImageHandle
 *      {
 *          source: imageTexture
 *          level: index
 *          format: imageTexture.format
 *      }
 *
 *      onItemAdded: {imageHandles.push(item); imageHandles = imageHandles}
 *  }
 *
 *  // Write a checkerboard pattern to each image handle
 *  ComputeProgram
 *  {
 *      xGroups: imageTexture.width
 *      yGroups: imageTexture.height
 *      zGroups: outputImage.length
 *
 *      property var outputImage : imageHandles
 *
 *      computeShader: "
 *              #version 430 core
 *              #extension GL_ARB_gpu_shader_int64 : enable
 *              #extension GL_ARB_bindless_texture : require
 *
 *              layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
 *
 *              uniform uint64_t outputImage[10];
 *
 *              void main(void)
 *              {
 *                  layout(r32f) image2D image = layout (r32f) image2D(outputImage[gl_GlobalInvocationID.z]);
 *                  if(all(lessThan(gl_GlobalInvocationID.xy,imageSize(image))))
 *                  {
 *                      imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4((gl_GlobalInvocationID.x+gl_GlobalInvocationID.y)%2));
 *                  }
 *
 *              }
 *          "
 *  }
 * \endqml
 *
 * \sa Texture
 * \sa ImageHandle
 * \sa GraphicsProgram
 * \sa ComputeProgram
 * \sa https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_bindless_texture.txt
 */

class QuickImageHandlePrivate : public QObject
{
    Q_OBJECT

public:

    QuickImageHandlePrivate(QuickImageHandle *q);

    QQuickItem * source;
    int level;
    bool layered;
    int layer;
    QuickImageHandle::Access access;
    QuickImageHandle::Format format;
    QuickSGImageHandleProvider *imageHandleProvider;

public slots:

    void onSceneGraphInvalidated();

protected:

    QuickImageHandle *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickImageHandle)
};

QuickImageHandlePrivate::QuickImageHandlePrivate(QuickImageHandle *q):
    source(nullptr),
    level(0),
    layered(true),
    layer(0),
    access(QuickImageHandle::AccessReadWrite),
    format(QuickImageHandle::FormatRGBA32F),
    imageHandleProvider(nullptr),
    q_ptr(q)
{

}

void QuickImageHandlePrivate::onSceneGraphInvalidated()
{
    if(imageHandleProvider)
    {
        delete imageHandleProvider;
        imageHandleProvider = nullptr;
    }
}



QuickImageHandle::QuickImageHandle(QQuickItem *parent):
    QQuickItem(parent),
    d_ptr(new QuickImageHandlePrivate(this))
{
    setFlag(QQuickItem::ItemHasContents, true);
}

QuickImageHandle::~QuickImageHandle()
{

}

/*!
 * \qmlproperty object ImageHandle::source
 *
 * This property holds the source texture provider object. It provides the
 * texture for which an image handle is generated. The default value is null
 */
QQuickItem *QuickImageHandle::source() const
{
    Q_D(const QuickImageHandle);

    return d->source;
}

void QuickImageHandle::setSource(QQuickItem *source)
{
    Q_D(QuickImageHandle);

    if (d->source == source)
        return;

    d->source = source;
    emit sourceChanged(source);

    if(d->source&&(!d->source->isTextureProvider()))
    {
        qWarning() << "QuickOpenGL:" << this << "source set to" << source << "which is not a texture provider";
    }

    update();
}


/*!
 * \qmlproperty int ImageHandle::level
 *
 * This property holds the source texture mip level for which an image handle is
 * generated.  The default value is 0.
 */
int QuickImageHandle::level() const
{
    Q_D(const QuickImageHandle);

    return d->level;
}


void QuickImageHandle::setLevel(int level)
{
    Q_D(QuickImageHandle);

    if (d->level == level)
        return;

    d->level = level;
    emit levelChanged(level);

    update();
}

/*!
 * \qmlproperty bool ImageHandle::layered
 *
 * This property holds the layered property of the image handle. If true, a
 * handle is created for the entire texture level. If false, a handle is
 * created only for the specified \l layer of the texture level. The default
 * value is false.
 *
 * \sa layer
 */
bool QuickImageHandle::layered() const
{
    Q_D(const QuickImageHandle);

    return d->layered;
}

void QuickImageHandle::setLayered(bool layered)
{
    Q_D(QuickImageHandle);

    if (d->layered == layered)
        return;

    d->layered = layered;
    emit layeredChanged(layered);

    update();
}

/*!
 * \qmlproperty int ImageHandle::layer
 *
 * This property holds the layer of the image handle. If \l layered is false an
 * image handle is created for on this layer of the texture level. The default
 * value is 0.
 *
 */
int QuickImageHandle::layer() const
{
    Q_D(const QuickImageHandle);

    return d->layer;
}

void QuickImageHandle::setLayer(int layer)
{
    Q_D(QuickImageHandle);

    if (d->layer == layer)
        return;

    d->layer = layer;
    emit layerChanged(layer);

    update();
}

/*!
 * \qmlproperty enumeration ImageHandle::format
 *
 * This property holds the format used to interpret the texels of the image.
 * The default value is FormatRGBA32F.
 *
 * format takes the following values:
 *
 * \value FormatR8_UNorm
 *  GL_R8
 * \value FormatRG8_UNorm
 *  GL_RG8
 * \value FormatRGB8_UNorm
 *  GL_RGB8
 * \value FormatRGBA8_UNorm
 *  GL_RGBA8
 * \value FormatR16_UNorm
 *  GL_R16
 * \value FormatRG16_UNorm
 *  GL_RG16
 * \value FormatRGB16_UNorm
 *  GL_RGB16
 * \value FormatRGBA16_UNorm
 *  GL_RGBA16
 * \value FormatR8_SNorm
 *  GL_R8_SNORM
 * \value FormatRG8_SNorm
 *  GL_RG8_SNORM
 * \value FormatRGB8_SNorm
 *  GL_RGB8_SNORM
 * \value FormatRGBA8_SNorm
 *  GL_RGBA8_SNORM
 * \value FormatR16_SNorm
 *  GL_R16_SNORM
 * \value FormatRG16_SNorm
 *  GL_RG16_SNORM
 * \value FormatRGB16_SNorm
 *  GL_RGB16_SNORM
 * \value FormatRGBA16_SNorm
 *  GL_RGBA16_SNORM
 * \value FormatR8U
 *  GL_R8UI
 * \value FormatRG8U
 *  GL_RG8UI
 * \value FormatRGB8U
 *  GL_RGB8UI
 * \value FormatRGBA8U
 *  GL_RGBA8UI
 * \value FormatR16U
 *  GL_R16UI
 * \value FormatRG16U
 *  GL_RG16UI
 * \value FormatRGB16U
 *  GL_RGB16UI
 * \value FormatRGBA16U
 *  GL_RGBA16UI
 * \value FormatR32U
 *  GL_R32UI
 * \value FormatRG32U
 *  GL_RG32UI
 * \value FormatRGB32U
 *  GL_RGB32UI
 * \value FormatRGBA32U
 *  GL_RGBA32UI
 * \value FormatR8I
 *  GL_R8I
 * \value FormatRG8I
 *  GL_RG8I
 * \value FormatRGB8I
 *  GL_RGB8I
 * \value FormatRGBA8I
 *  GL_RGBA8I
 * \value FormatR16I
 *  GL_R16I
 * \value FormatRG16I
 *  GL_RG16I
 * \value FormatRGB16I
 *  GL_RGB16I
 * \value FormatRGBA16I
 *  GL_RGBA16I
 * \value FormatR32I
 *  GL_R32I
 * \value FormatRG32I
 *  GL_RG32I
 * \value FormatRGB32I
 *  GL_RGB32I
 * \value FormatRGBA32I
 *  GL_RGBA32I
 * \value FormatR16F
 *  GL_R16F
 * \value FormatRG16F
 *  GL_RG16F
 * \value FormatRGB16F
 *  GL_RGB16F
 * \value FormatRGBA16F
 *  GL_RGBA16F
 * \value FormatR32F
 *  GL_R32F
 * \value FormatRG32F
 *  GL_RG32F
 * \value FormatRGB32F
 *  GL_RGB32F
 * \value FormatRGBA32F
 *  GL_RGBA32F
 * \value FormatRGB9E5
 *  GL_RGB9_E5
 * \value FormatRG11B10F
 *  GL_R11F_G11F_B10F
 * \value FormatRG3B2
 *  GL_R3_G3_B2
 * \value FormatR5G6B5
 *  GL_RGB565
 * \value FormatRGB5A1
 *  GL_RGB5_A1
 * \value FormatRGBA4
 *  GL_RGBA4
 * \value FormatRGB10A2
 *  GL_RGB10_A2UI
 * \value FormatD16
 *  GL_DEPTH_COMPONENT16
 * \value FormatD24
 *  GL_DEPTH_COMPONENT24
 * \value FormatD24S8
 *  GL_DEPTH24_STENCIL8
 * \value FormatD32
 *  GL_DEPTH_COMPONENT32
 * \value FormatD32F
 *  GL_DEPTH_COMPONENT32F
 * \value FormatD32FS8X24
 *  GL_DEPTH32F_STENCIL8
 * \value FormatS8
 *  GL_STENCIL_INDEX8
 * \value FormatRGB_DXT1
 *  GL_COMPRESSED_RGB_S3TC_DXT1_EXT
 * \value FormatRGBA_DXT1
 *  GL_COMPRESSED_RGBA_S3TC_DXT1_EXT
 * \value FormatRGBA_DXT3
 *  GL_COMPRESSED_RGBA_S3TC_DXT3_EXT
 * \value FormatRGBA_DXT5
 *  GL_COMPRESSED_RGBA_S3TC_DXT5_EXT
 * \value FormatR_ATI1N_UNorm
 *  GL_COMPRESSED_RED_RGTC1
 * \value FormatR_ATI1N_SNorm
 *  GL_COMPRESSED_SIGNED_RED_RGTC1
 * \value FormatRG_ATI2N_UNorm
 *  GL_COMPRESSED_RG_RGTC2
 * \value FormatRG_ATI2N_SNorm
 *  GL_COMPRESSED_SIGNED_RG_RGTC2
 * \value FormatRGB_BP_UNSIGNED_FLOAT
 *  GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB
 * \value FormatRGB_BP_SIGNED_FLOAT
 *  GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB
 * \value FormatRGB_BP_UNorm
 *  GL_COMPRESSED_RGBA_BPTC_UNORM_ARB
 * \value FormatR11_EAC_UNorm
 *  GL_COMPRESSED_R11_EAC
 * \value FormatR11_EAC_SNorm
 *  GL_COMPRESSED_SIGNED_R11_EAC
 * \value FormatRG11_EAC_UNorm
 *  GL_COMPRESSED_RG11_EAC
 * \value FormatRG11_EAC_SNorm
 *  GL_COMPRESSED_SIGNED_RG11_EAC
 * \value FormatRGB8_ETC2
 *  GL_COMPRESSED_RGB8_ETC2
 * \value FormatSRGB8_ETC2
 *  GL_COMPRESSED_SRGB8_ETC2
 * \value FormatRGB8_PunchThrough_Alpha1_ETC2
 *  GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2
 * \value FormatSRGB8_PunchThrough_Alpha1_ETC2
 *  GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2
 * \value FormatRGBA8_ETC2_EAC
 *  GL_COMPRESSED_RGBA8_ETC2_EAC
 * \value FormatSRGB8_Alpha8_ETC2_EAC
 *  GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC
 * \value FormatSRGB8
 *  GL_SRGB8
 * \value FormatSRGB8_Alpha8
 *  GL_SRGB8_ALPHA8
 * \value FormatSRGB_DXT1
 *  GL_COMPRESSED_SRGB_S3TC_DXT1_EXT
 * \value FormatSRGB_Alpha_DXT1
 *  GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT
 * \value FormatSRGB_Alpha_DXT3
 *  GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT
 * \value FormatSRGB_Alpha_DXT5
 *  GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT
 * \value FormatSRGB_BP_UNorm
 *  GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM_ARB
 * \value FormatDepthFormat
 *  GL_DEPTH_COMPONENT
 * \value FormatAlphaFormat
 *  GL_ALPHA
 * \value FormatRGBFormat
 *  GL_RGB
 * \value FormatRGBAFormat
 *  GL_RGBA
 * \value FormatLuminanceFormat
 *  GL_LUMINANCE
 * \value FormatLuminanceAlphaFormat
 *  GL_LUMINANCE_ALPHA
 *
 * \sa https://www.khronos.org/opengl/wiki/Image_Format
 */
QuickImageHandle::Format QuickImageHandle::format() const
{
    Q_D(const QuickImageHandle);

    return d->format;
}

void QuickImageHandle::setFormat(QuickImageHandle::Format format)
{
    Q_D(QuickImageHandle);

    if (d->format == format)
        return;

    d->format = format;
    emit formatChanged(format);
    update();
}

/*!
 * \qmlproperty enumeration ImageHandle::access
 *
 * This property holds the type of image access that will be performed by
 * shaders on the image handle. The default value is AccessReadWrite.
 *
 * access takes the following values
 *
 * \value AccessReadOnly
 *   Shaders read from the image
 * \value AccessWriteOnly
 *   Shaders write to the image
 * \value AccessReadWrite
 *   Shaders read from and write to the image
 */
QuickImageHandle::Access QuickImageHandle::access() const
{
    Q_D(const QuickImageHandle);

    return d->access;
}

void QuickImageHandle::setAccess(QuickImageHandle::Access access)
{
    Q_D(QuickImageHandle);

    if (d->access == access)
        return;

    d->access = access;
    emit accessChanged(access);
    update();
}

void QuickImageHandle::releaseResources()
{
    Q_D(QuickImageHandle);
    if(d->imageHandleProvider)
    {
        window()->scheduleRenderJob(cleanup(d->imageHandleProvider), QQuickWindow::NoStage);
        d->imageHandleProvider = nullptr;
    }
}

QuickSGImageHandleProvider *QuickImageHandle::imageHandleProvider()
{
    Q_D(QuickImageHandle);

    if(window()&&window()->openglContext()&&window()->openglContext()->thread()&&(window()->openglContext()->thread()==QThread::currentThread()))
    {
        if(!d->imageHandleProvider)
        {
            d->imageHandleProvider = new QuickSGImageHandleProvider();
            connect(window(), &QQuickWindow::sceneGraphInvalidated, d, &QuickImageHandlePrivate::onSceneGraphInvalidated, Qt::DirectConnection);
        }

        if(d->source&&d->source->isTextureProvider()&&d->source->textureProvider())
        {
            d->imageHandleProvider->setLayered(d->layered);
            d->imageHandleProvider->setLayer(d->layer);
            d->imageHandleProvider->setLevel(d->level);
            d->imageHandleProvider->setAccess(d->access);
            d->imageHandleProvider->setFormat(d->format);

            d->imageHandleProvider->setSourceProvider(d->source->textureProvider());
        }
        return d->imageHandleProvider;
    }

    return nullptr;
}







#include "quickimagehandle.moc"



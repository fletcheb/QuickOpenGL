/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quickopengl_plugin.h"
#include "quickcomputeprogram.h"
#include "quickgraphicsprogram.h"
#include "quicktexture.h"
#include "quickshaderstoragebuffer.h"
#include "quicksampler.h"
#include "quicktextureview.h"
#include "quickuniformbuffer.h"
#include "quicktexturehandle.h"
#include "quickimagehandle.h"
#include "quickopenglproperties.h"
#include "quickopengldebuglogger.h"
#include "quickopengldebug.h"
#include <qqml.h>
#include <QDebug>
#include <QMetaType>
#include <QColor>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QPointF>
#include <QSizeF>
#include <QPoint>
#include <QSize>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QRect>
#include <QSize>
#include <QPoint>
#include <QJSEngine>
#include <QJSValue>
#include <QString>
#include <QGeoCoordinate>

#include <QGenericMatrix>
#include "support.h"

#include "quickopengltypeinstantiator.h"
#include "openglextension_arb_bindless_texture.h"
#include <QThreadStorage>
#include <QScopedPointer>
#include <QSGTextureProvider>
#include <QtQml/qqmlextensionplugin.h>
#include <QtQml/QQmlExtensionPlugin>
#include "quicksgtexturehandleprovider.h"
#include "quicksgimagehandleprovider.h"
#include "quickopenglthreadpool.h"
#include "quickopengloffscreensurfacefactory.h"

static QObject *singleton_type_factory(QQmlEngine *engine, QJSEngine *jsEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(jsEngine)

#ifdef Q_OS_WIN
    // Offscreen surfaces can only be created on the GUI thread on windows
    QuickOpenGLOffscreenSurfaceFactory::instance();
#endif

    return new QuickOpenGLTypeInstantiator;
}

template <class Type> quint64 toUint64(const Type *from)
{
    from->makeResident();
    return from->handle();
}

template <class Type> quvec2 toUvec2(const Type *from)
{
    from->makeResident();
    return quvec2(quint32(from->handle()&0xffffff), quint32((from->handle()>>32)&0xffffff));
}

template <class Type> QVariantList listToQVariantList(const QList<Type> &from)
{
    QVariantList result;

    for(const Type &value: from)
    {
        result.append(QVariant::fromValue(value));
    }

    return result;
}

template <class Type> QVariantList vectorToQVariantList(const QVector<Type> &from)
{
    QVariantList result;

    for(const Type &value: from)
    {
        result.append(QVariant::fromValue(value));
    }

    return result;
}

QuickOpenGLPlugin::QuickOpenGLPlugin(QObject *parent) : QQmlExtensionPlugin(parent){}

void QuickOpenGLPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<QuickComputeProgram>(uri, 1, 0, "ComputeProgram");
    qmlRegisterType<QuickGraphicsProgram>(uri, 1, 0, "GraphicsProgram");
    qmlRegisterType<QuickTexture>(uri, 1, 0, "Texture");
    qmlRegisterType<QuickShaderStorageBuffer>(uri, 1, 0, "ShaderStorageBuffer");
    qmlRegisterType<QuickSamplerObject>(uri, 1, 0, "Sampler");
    qmlRegisterType<QuickTextureView>(uri, 1, 0, "TextureView");
    qmlRegisterType<QuickUniformBuffer>(uri, 1, 0, "UniformBuffer");
    qmlRegisterType<QuickTextureHandle>(uri, 1, 0, "TextureHandle");
    qmlRegisterType<QuickImageHandle>(uri, 1, 0, "ImageHandle");
    qmlRegisterType<QuickOpenGLDebugLogger>(uri, 1, 0, "OpenGLDebugLogger");
    qmlRegisterType<QuickOpenGLDebug>(uri, 1, 0, "QuickOpenGLDebug");
    qmlRegisterUncreatableType<QuickOpenGLProperties>(uri, 1, 0, "OpenGLProperties", "OpenGLProperties is only available via attached properties");
    qmlRegisterUncreatableType<QuickOpenGLDebugMessage>(uri, 1, 0, "OpenGLDebugMessage", "temp");
    qRegisterMetaType<QuickOpenGLDebugMessage::Source>();
    qRegisterMetaType<QuickOpenGLDebugMessage::Type>();
    qRegisterMetaType<QuickOpenGLDebugMessage::Severity>();

    bool ok;
    ok = QMetaType::registerConverter<QuickSGTextureHandleProvider *, quint64>(&toUint64<QuickSGTextureHandleProvider>);Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QuickSGTextureHandleProvider *, quvec2>(&toUvec2<QuickSGTextureHandleProvider>);Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QuickSGImageHandleProvider *, quint64>(&toUint64<QuickSGImageHandleProvider>);Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QuickSGImageHandleProvider *, quvec2>(&toUvec2<QuickSGImageHandleProvider>);Q_ASSERT(ok);

    ok = QMetaType::registerConverter<QList<double>, QVariantList>(&listToQVariantList<double>);Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QList<float>, QVariantList>(&listToQVariantList<float>);Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QList<int>, QVariantList>(&listToQVariantList<int>);Q_ASSERT(ok);

    ok = QMetaType::registerConverter<QVector<double>, QVariantList>(&vectorToQVariantList<double>);Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector<float>, QVariantList>(&vectorToQVariantList<float>);Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector<int>, QVariantList>(&vectorToQVariantList<int>);Q_ASSERT(ok);

    qRegisterMetaType<QuickBuffer>();
    ok = QMetaType::registerConverter<QuickBuffer, QByteArray>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qvec2)==8);
    qRegisterMetaType<qvec2>();
    ok = QMetaType::registerEqualsComparator<qvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QGeoCoordinate, qvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector2D, qvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPointF, qvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QSizeF, qvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPoint, qvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QSize, qvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec2, QGeoCoordinate>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec2, QVector2D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec2, QPointF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec2, QSizeF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec2, QPoint>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec2, QSize>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qivec2)==8);
    qRegisterMetaType<qivec2>();
    ok = QMetaType::registerEqualsComparator<qivec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qivec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector2D, qivec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPointF, qivec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QSizeF, qivec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPoint, qivec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QSize, qivec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec2, QVector2D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec2, QPointF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec2, QSizeF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec2, QPoint>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec2, QSize>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(quvec2)==8);
    qRegisterMetaType<quvec2>();
    ok = QMetaType::registerEqualsComparator<quvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<quvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector2D, quvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPointF, quvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QSizeF, quvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPoint, quvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QSize, quvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec2, QVector2D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec2, QPointF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec2, QSizeF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec2, QPoint>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec2, QSize>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdvec2)==16);
    qRegisterMetaType<qdvec2>();
    ok = QMetaType::registerEqualsComparator<qdvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qdvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QGeoCoordinate, qdvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector2D, qdvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPointF, qdvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QSizeF, qdvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPoint, qdvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QSize, qdvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec2, QGeoCoordinate>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec2, QVector2D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec2, QPointF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec2, QSizeF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec2, QPoint>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec2, QSize>(); Q_ASSERT(ok);        

    Q_ASSERT(sizeof(qbvec2)==2);
    qRegisterMetaType<qbvec2>();
    ok = QMetaType::registerEqualsComparator<qbvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qbvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector2D, qbvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QPoint, qbvec2>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qbvec2, QVector2D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qbvec2, QPoint>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qvec3)==12);
    qRegisterMetaType<qvec3>();
    ok = QMetaType::registerEqualsComparator<qvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QGeoCoordinate, qvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector3D, qvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QString, qvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QColor, qvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec3, QGeoCoordinate>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec3, QVector3D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec3, QString>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec3, QColor>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qivec3)==12);
    qRegisterMetaType<qivec3>();
    ok = QMetaType::registerEqualsComparator<qivec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qivec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector3D, qivec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QString, qivec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QColor, qivec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec3, QVector3D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec3, QString>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec3, QColor>(); Q_ASSERT(ok);


    Q_ASSERT(sizeof(quvec3)==12);
    qRegisterMetaType<quvec3>();
    ok = QMetaType::registerEqualsComparator<quvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<quvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector3D, quvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QString, quvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QColor, quvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec3, QVector3D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec3, QString>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec3, QColor>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdvec3)==24);
    qRegisterMetaType<qdvec3>();
    ok = QMetaType::registerEqualsComparator<qdvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qdvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QGeoCoordinate, qdvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector3D, qdvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QString, qdvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QColor, qdvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec3, QGeoCoordinate>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec3, QVector3D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec3, QString>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec3, QColor>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qbvec3)==3);
    qRegisterMetaType<qbvec3>();
    ok = QMetaType::registerEqualsComparator<qbvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qbvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector3D, qbvec3>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qbvec3, QVector3D>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qvec4)==16);
    qRegisterMetaType<qvec4>();
    ok = QMetaType::registerEqualsComparator<qvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector4D, qvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QString, qvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QQuaternion, qvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QRect, qvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QRectF, qvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QColor, qvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec4, QVector4D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec4, QString>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec4, QQuaternion>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec4, QRect>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec4, QRectF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qvec4, QColor>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qivec4)==16);
    qRegisterMetaType<qivec4>();
    ok = QMetaType::registerEqualsComparator<qivec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qivec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector4D, qivec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QString, qivec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QRect, qivec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QRectF, qivec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QColor, qivec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec4, QVector4D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec4, QString>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec4, QRect>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec4, QRectF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qivec4, QColor>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(quvec4)==16);
    qRegisterMetaType<quvec4>();
    ok = QMetaType::registerEqualsComparator<quvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<quvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector4D, quvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QString, quvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QRect, quvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QRectF, quvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QColor, quvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec4, QVector4D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec4, QString>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec4, QRect>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec4, QRectF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<quvec4, QColor>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdvec4)==32);
    qRegisterMetaType<qdvec4>();
    ok = QMetaType::registerEqualsComparator<qdvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qdvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector4D, qdvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QString, qdvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QQuaternion, qdvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QRect, qdvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QRectF, qdvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QColor, qdvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec4, QVector4D>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec4, QString>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec4, QQuaternion>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec4, QRect>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec4, QRectF>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdvec4, QColor>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qbvec4)==4);
    qRegisterMetaType<qbvec4>();
    ok = QMetaType::registerEqualsComparator<qbvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qbvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QVector4D, qbvec4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qbvec4, QVector4D>(); Q_ASSERT(ok);


    Q_ASSERT(sizeof(qmat4)==64);
    qRegisterMetaType<qmat4>();
    ok = QMetaType::registerEqualsComparator<qmat4>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qmat4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QMatrix4x4, qmat4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qmat4, QMatrix4x4>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat4)==128);
    qRegisterMetaType<qdmat4>();
    ok = QMetaType::registerEqualsComparator<qdmat4>(); Q_ASSERT(ok);
    ok = QMetaType::registerDebugStreamOperator<qdmat4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<QMatrix4x4, qdmat4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdmat4, QMatrix4x4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qmat4, qdmat4>(); Q_ASSERT(ok);
    ok = QMetaType::registerConverter<qdmat4, qmat4>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qmat2)==16);
    qRegisterMetaType<qmat2>();
    ok = QMetaType::registerEqualsComparator<qmat2>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qmat3)==36);
    qRegisterMetaType<qmat3>();
    ok = QMetaType::registerEqualsComparator<qmat3>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qmat2x3)==24);
    qRegisterMetaType<qmat2x3>();
    ok = QMetaType::registerEqualsComparator<qmat2x3>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qmat2x4)==32);
    qRegisterMetaType<qmat2x4>();
    ok = QMetaType::registerEqualsComparator<qmat2x4>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qmat3x2)==24);
    qRegisterMetaType<qmat3x2>();
    ok = QMetaType::registerEqualsComparator<qmat3x2>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qmat3x4)==48);
    qRegisterMetaType<qmat3x4>();
    ok = QMetaType::registerEqualsComparator<qmat3x4>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qmat4x2)==32);
    qRegisterMetaType<qmat4x2>();
    ok = QMetaType::registerEqualsComparator<qmat4x2>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qmat4x3)==48);
    qRegisterMetaType<qmat4x3>();
    ok = QMetaType::registerEqualsComparator<qmat4x3>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat2)==32);
    qRegisterMetaType<qdmat2>();
    ok = QMetaType::registerEqualsComparator<qdmat2>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat3)==72);
    qRegisterMetaType<qdmat3>();
    ok = QMetaType::registerEqualsComparator<qdmat3>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat2x3)==48);
    qRegisterMetaType<qdmat2x3>();
    ok = QMetaType::registerEqualsComparator<qdmat2x3>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat2x4)==64);
    qRegisterMetaType<qdmat2x4>();
    ok = QMetaType::registerEqualsComparator<qdmat2x4>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat3x2)==48);
    qRegisterMetaType<qdmat3x2>();
    ok = QMetaType::registerEqualsComparator<qdmat3x2>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat3x4)==96);
    qRegisterMetaType<qdmat3x4>();
    ok = QMetaType::registerEqualsComparator<qdmat3x4>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat4x2)==64);
    qRegisterMetaType<qdmat4x2>();
    ok = QMetaType::registerEqualsComparator<qdmat4x2>(); Q_ASSERT(ok);

    Q_ASSERT(sizeof(qdmat4x3)==96);
    qRegisterMetaType<qdmat4x3>();
    ok = QMetaType::registerEqualsComparator<qdmat4x3>(); Q_ASSERT(ok);

    qmlRegisterSingletonType<QuickOpenGLTypeInstantiator>(uri, 1, 0, "GL", singleton_type_factory);

    qmlRegisterModule(uri, 1, 0);

}



/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicktexture.h"
#include <QQuickWindow>
#include <QThread>
#include <QSGTextureProvider>
#include <QSGTexture>
#include <QOpenGLTexture>
#include <QOpenGLFunctions_3_3_Core>
#include <QUrl>
#include "support.h"
#include "quicksgtexture.h"
#include "quicksgtextureprovider.h"
#include <QGLContext>

/*!
 * \qmltype Texture
 * \brief The Texture QML type represents an OpenGL texture.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickTexture
 * \ingroup QuickOpenGL QML Types
 *
 *
 * Texture represents an OpenGL texture object. It manages a \l QSGTextureProvider in
 * the \l{Qt Quick Scene Graph}.
 *
 * Texture allows specification of an OpenGL texture object directly in QML. An
 * OpenGL texture object contains one or more images that all have the same
 * image format. A texture can be used in two ways. It can be the source of a
 * texture access from a Shader, or it can be used as a render target.
 *
 * Texture is also able to syncrhonize its internal data between OpenGL and QML.  This
 * is controled via the \l{syncToOpenGL} and \l{syncFromOpenGL} properties.  Data may
 * be accessed via the \l{data} property.
 *
 * \sa GraphicsProgram
 * \sa ComputeProgram
 * \sa Sampler
 * \sa TextureHandle
 * \sa ImageHandle
 * \sa QOpenGLTexture
 * \sa https://www.khronos.org/opengl/wiki/Texture
 */

/*!
 * \qmlproperty int Texture::width
 *
 * This property holds the width of the texture in texels. It is used for
 * all texture targets. The default value is 1.
 *
 * layers takes values in the range (0, \l{OpenGLProperties::glMaxTextureSize}]
 *
 * \sa target
 * \sa OpenGLProperties::glMaxTextureSize
 */

/*!
 * \qmlproperty int Texture::height
 *
 * This property holds the height of the texture in texels. It is used for
 * texture targets with more than one dimension. The default value is 1.
 *
 * layers takes values in the range (0, \l{OpenGLProperties::glMaxTextureSize}]
 *
 * \sa target
 * \sa OpenGLProperties::glMaxTextureSize
 */
class QuickTexturePrivate : public QObject
{
    Q_OBJECT

public:

    QuickTexturePrivate(QuickTexture *q);

    QuickTexture::Target target;
    QuickTexture::Format format;
    QuickTexture::Wrap wrapS;
    QuickTexture::Wrap wrapT;
    QuickTexture::Wrap wrapR;
    int layers;
    int depth;
    int imageLayer;
    int imageLevel;
    QuickTexture::Access imageAccess;
    QuickSGTextureProvider *textureProvider;
    int mipLevels;
    QuickTexture::Filter magFilter;
    QuickTexture::Filter minFilter;
    double lodBias;
    double maxLod;
    double minLod;
    QColor borderColor;

    bool zeroTexture;
    bool syncFromOpenGL;
    QuickTexture::SyncWrite syncToOpenGL;
    QVariant data;
    bool dataDirty;

public slots:

    void onSceneGraphInvalidated();

protected:

    QuickTexture *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickTexture)
};

QuickTexturePrivate::QuickTexturePrivate(QuickTexture *q) :
    target(QuickTexture::Target2D),
    format(QuickTexture::FormatRGBA32F),
    wrapS(QuickTexture::Wrap::WrapRepeat),
    wrapT(QuickTexture::Wrap::WrapRepeat),
    wrapR(QuickTexture::Wrap::WrapRepeat),
    layers(1),
    depth(1),
    imageLayer(-1),
    imageLevel(0),
    imageAccess(QuickTexture::AccessReadWrite),
    textureProvider(nullptr),
    mipLevels(1),
    magFilter(QuickTexture::FilterLinear),
    minFilter(QuickTexture::FilterNearestMipMapLinear),
    lodBias(0.0),
    maxLod(1000.0),
    minLod(-1000.0),
    zeroTexture(true),
    syncFromOpenGL(false),
    syncToOpenGL(QuickTexture::SyncWriteOnUpdated),
    dataDirty(false),
    q_ptr(q)
{
    q->setWidth(256);
    q->setHeight(256);
}

/*!
 * \internal
 * \brief Cleans up when the scene graph is invalidated
 */
void QuickTexturePrivate::onSceneGraphInvalidated()
{
    if(textureProvider)
    {
        delete textureProvider;
        textureProvider = nullptr;
    }
}

QuickTexture::QuickTexture(QQuickItem *parent) :
    QQuickItem(parent),
    d_ptr(new QuickTexturePrivate(this))
{
    setFlag(QQuickItem::ItemHasContents);
}


QuickTexture::~QuickTexture()
{
    Q_D(QuickTexture);

    // we should have cleaned up our texture provider by other means
    Q_ASSERT(d->textureProvider==nullptr);
}

/*!
 * \qmlproperty enumeration Texture::format
 *
 * This property holds the texture's image format. It describes the way that
 * images within the texture store their data. The default value is
 * FormatRGBA32F.
 *
 * format takes the following values:
 *
 * \value FormatR8_UNorm
 *  GL_R8
 * \value FormatRG8_UNorm
 *  GL_RG8
 * \value FormatRGB8_UNorm
 *  GL_RGB8
 * \value FormatRGBA8_UNorm
 *  GL_RGBA8
 * \value FormatR16_UNorm
 *  GL_R16
 * \value FormatRG16_UNorm
 *  GL_RG16
 * \value FormatRGB16_UNorm
 *  GL_RGB16
 * \value FormatRGBA16_UNorm
 *  GL_RGBA16
 * \value FormatR8_SNorm
 *  GL_R8_SNORM
 * \value FormatRG8_SNorm
 *  GL_RG8_SNORM
 * \value FormatRGB8_SNorm
 *  GL_RGB8_SNORM
 * \value FormatRGBA8_SNorm
 *  GL_RGBA8_SNORM
 * \value FormatR16_SNorm
 *  GL_R16_SNORM
 * \value FormatRG16_SNorm
 *  GL_RG16_SNORM
 * \value FormatRGB16_SNorm
 *  GL_RGB16_SNORM
 * \value FormatRGBA16_SNorm
 *  GL_RGBA16_SNORM
 * \value FormatR8U
 *  GL_R8UI
 * \value FormatRG8U
 *  GL_RG8UI
 * \value FormatRGB8U
 *  GL_RGB8UI
 * \value FormatRGBA8U
 *  GL_RGBA8UI
 * \value FormatR16U
 *  GL_R16UI
 * \value FormatRG16U
 *  GL_RG16UI
 * \value FormatRGB16U
 *  GL_RGB16UI
 * \value FormatRGBA16U
 *  GL_RGBA16UI
 * \value FormatR32U
 *  GL_R32UI
 * \value FormatRG32U
 *  GL_RG32UI
 * \value FormatRGB32U
 *  GL_RGB32UI
 * \value FormatRGBA32U
 *  GL_RGBA32UI
 * \value FormatR8I
 *  GL_R8I
 * \value FormatRG8I
 *  GL_RG8I
 * \value FormatRGB8I
 *  GL_RGB8I
 * \value FormatRGBA8I
 *  GL_RGBA8I
 * \value FormatR16I
 *  GL_R16I
 * \value FormatRG16I
 *  GL_RG16I
 * \value FormatRGB16I
 *  GL_RGB16I
 * \value FormatRGBA16I
 *  GL_RGBA16I
 * \value FormatR32I
 *  GL_R32I
 * \value FormatRG32I
 *  GL_RG32I
 * \value FormatRGB32I
 *  GL_RGB32I
 * \value FormatRGBA32I
 *  GL_RGBA32I
 * \value FormatR16F
 *  GL_R16F
 * \value FormatRG16F
 *  GL_RG16F
 * \value FormatRGB16F
 *  GL_RGB16F
 * \value FormatRGBA16F
 *  GL_RGBA16F
 * \value FormatR32F
 *  GL_R32F
 * \value FormatRG32F
 *  GL_RG32F
 * \value FormatRGB32F
 *  GL_RGB32F
 * \value FormatRGBA32F
 *  GL_RGBA32F
 * \value FormatRGB9E5
 *  GL_RGB9_E5
 * \value FormatRG11B10F
 *  GL_R11F_G11F_B10F
 * \value FormatRG3B2
 *  GL_R3_G3_B2
 * \value FormatR5G6B5
 *  GL_RGB565
 * \value FormatRGB5A1
 *  GL_RGB5_A1
 * \value FormatRGBA4
 *  GL_RGBA4
 * \value FormatRGB10A2
 *  GL_RGB10_A2UI
 * \value FormatD16
 *  GL_DEPTH_COMPONENT16
 * \value FormatD24
 *  GL_DEPTH_COMPONENT24
 * \value FormatD24S8
 *  GL_DEPTH24_STENCIL8
 * \value FormatD32
 *  GL_DEPTH_COMPONENT32
 * \value FormatD32F
 *  GL_DEPTH_COMPONENT32F
 * \value FormatD32FS8X24
 *  GL_DEPTH32F_STENCIL8
 * \value FormatS8
 *  GL_STENCIL_INDEX8
 * \value FormatRGB_DXT1
 *  GL_COMPRESSED_RGB_S3TC_DXT1_EXT
 * \value FormatRGBA_DXT1
 *  GL_COMPRESSED_RGBA_S3TC_DXT1_EXT
 * \value FormatRGBA_DXT3
 *  GL_COMPRESSED_RGBA_S3TC_DXT3_EXT
 * \value FormatRGBA_DXT5
 *  GL_COMPRESSED_RGBA_S3TC_DXT5_EXT
 * \value FormatR_ATI1N_UNorm
 *  GL_COMPRESSED_RED_RGTC1
 * \value FormatR_ATI1N_SNorm
 *  GL_COMPRESSED_SIGNED_RED_RGTC1
 * \value FormatRG_ATI2N_UNorm
 *  GL_COMPRESSED_RG_RGTC2
 * \value FormatRG_ATI2N_SNorm
 *  GL_COMPRESSED_SIGNED_RG_RGTC2
 * \value FormatRGB_BP_UNSIGNED_FLOAT
 *  GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB
 * \value FormatRGB_BP_SIGNED_FLOAT
 *  GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB
 * \value FormatRGB_BP_UNorm
 *  GL_COMPRESSED_RGBA_BPTC_UNORM_ARB
 * \value FormatR11_EAC_UNorm
 *  GL_COMPRESSED_R11_EAC
 * \value FormatR11_EAC_SNorm
 *  GL_COMPRESSED_SIGNED_R11_EAC
 * \value FormatRG11_EAC_UNorm
 *  GL_COMPRESSED_RG11_EAC
 * \value FormatRG11_EAC_SNorm
 *  GL_COMPRESSED_SIGNED_RG11_EAC
 * \value FormatRGB8_ETC2
 *  GL_COMPRESSED_RGB8_ETC2
 * \value FormatSRGB8_ETC2
 *  GL_COMPRESSED_SRGB8_ETC2
 * \value FormatRGB8_PunchThrough_Alpha1_ETC2
 *  GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2
 * \value FormatSRGB8_PunchThrough_Alpha1_ETC2
 *  GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2
 * \value FormatRGBA8_ETC2_EAC
 *  GL_COMPRESSED_RGBA8_ETC2_EAC
 * \value FormatSRGB8_Alpha8_ETC2_EAC
 *  GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC
 * \value FormatSRGB8
 *  GL_SRGB8
 * \value FormatSRGB8_Alpha8
 *  GL_SRGB8_ALPHA8
 * \value FormatSRGB_DXT1
 *  GL_COMPRESSED_SRGB_S3TC_DXT1_EXT
 * \value FormatSRGB_Alpha_DXT1
 *  GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT
 * \value FormatSRGB_Alpha_DXT3
 *  GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT
 * \value FormatSRGB_Alpha_DXT5
 *  GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT
 * \value FormatSRGB_BP_UNorm
 *  GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM_ARB
 * \value FormatDepthFormat
 *  GL_DEPTH_COMPONENT
 * \value FormatAlphaFormat
 *  GL_ALPHA
 * \value FormatRGBFormat
 *  GL_RGB
 * \value FormatRGBAFormat
 *  GL_RGBA
 * \value FormatLuminanceFormat
 *  GL_LUMINANCE
 * \value FormatLuminanceAlphaFormat
 *  GL_LUMINANCE_ALPHA
 *
 * \sa https://www.khronos.org/opengl/wiki/Image_Format
 */
QuickTexture::Format QuickTexture::format() const
{
    Q_D(const QuickTexture);

    return d->format;
}

void QuickTexture::setFormat(QuickTexture::Format format)
{
    Q_D(QuickTexture);

    if (d->format == format)
        return;

    d->format = format;
    emit formatChanged(format);
    update();
}

/*!
 * \qmlproperty int Texture::layers
 *
 * This property holds the number of layers in the texture in texels. It is
 * used only for array textures, i.e. textures with \l target Target1DArray,
 * Target2DArray, TargetCubeMapArray, Target 2DMultisampleArray. The default
 * value is 1.
 *
 * layers takes values in the range (0, \l{OpenGLProperties::glMaxArrayTextureLayers}]
 *
 * \sa target
 * \sa OpenGLProperties::glMaxArrayTextureLayers
 * \sa https://www.khronos.org/opengl/wiki/Array_Texture
 */
int QuickTexture::layers() const
{
    Q_D(const QuickTexture);

    return d->layers;
}

void QuickTexture::setLayers(int layers)
{
    Q_D(QuickTexture);

    if (d->layers == layers)
        return;

    d->layers = layers;
    emit layersChanged(layers);
    update();
}

/*!
 * \qmlproperty int Texture::imageLayer
 *
 * This property holds the layer to be bound to an image unit. It is only used
 * for array textures i.e. textures with \l target Target1DArray,
 * Target2DArray, TargetCubeMapArray, Target 2DMultisampleArray. The default
 * value is -1 (all layers bound).
 *
 * imageLayer takes the values
 *
 * \value -1
 *   Layered image texture binding not used
 * \value [0, layers)
 *   The layer to be bound to the image unit
 *
 * \sa target
 * \sa layers
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glBindImageTexture
 * \sa https://www.khronos.org/opengl/wiki/Array_Texture
 */

int QuickTexture::imageLayer() const
{
    Q_D(const QuickTexture);

    return d->imageLayer;
}

void QuickTexture::setImageLayer(int imageLayer)
{
    Q_D(QuickTexture);

    if (d->imageLayer == imageLayer)
        return;

    d->imageLayer = imageLayer;
    emit imageLayerChanged(imageLayer);
    update();
}

/*!
 * \qmlproperty enumeration Texture::imageAccess
 *
 * This property holds the type of image access that will be performed by
 * shaders on the image. It is used when the texture is bound to an image unit.
 * The default value is AccessReadWrite.
 *
 * imageAccess takes the following values
 *
 * \value AccessReadOnly
 *   Shaders read from the image
 * \value AccessWriteOnly
 *   Shaders write to the image
 * \value AccessReadWrite
 *   Shaders read from and write to the image
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glBindImageTexture
 */
QuickTexture::Access QuickTexture::imageAccess() const
{
    Q_D(const QuickTexture);

    return d->imageAccess;
}

void QuickTexture::setImageAccess(QuickTexture::Access imageAccess)
{
    Q_D(QuickTexture);

    if (d->imageAccess == imageAccess)
        return;

    d->imageAccess = imageAccess;
    emit imageAccessChanged(imageAccess);
    update();
}

/*!
 * \qmlproperty int Texture::depth
 *
 * This property holds the depth of the texture in texels. It is only used for
 * 3D textures, i.e. textures with \l target Target3D.  The default value is 1.
 *
 * depth takes values in the range (0, \l {OpenGLProperties::glMax3DTextureSize}]
 *
 */
int QuickTexture::depth() const
{
    Q_D(const QuickTexture);

    return d->depth;
}

void QuickTexture::setDepth(int depth)
{
    Q_D(QuickTexture);

    if (d->depth == depth)
        return;

    d->depth = depth;
    emit depthChanged(depth);
    update();
}

/*!
 * \qmlproperty enumeration Texture::target
 *
 * This property holds the texture target.  The default value is Target2D.
 *
 * target takes the following values:
 *
 * \value Target1D
 *   GL_TEXTURE_1D
 * \value Target1DArray
 *   GL_TEXTURE_1D_ARRAY
 * \value Target2D
 *   GL_TEXTURE_2D
 * \value Target2DArray
 *   GL_TEXTURE_2D_ARRAY
 * \value Target3D
 *   GL_TEXTURE_3D
 * \value TargetCubeMap
 *   GL_TEXTURE_CUBE_MAP
 * \value TargetCubeMapArray
 *   GL_TEXTURE_CUBE_MAP_ARRAY
 * \value Target2DMultisample
 *   GL_TEXTURE_2D_MULTISAMPLE
 * \value Target2DMultisampleArray
 *   GL_TEXTURE_2D_MULTISAMPLE_ARRAY
 * \value TargetRectangle
 *   GL_TEXTURE_RECTANGLE
 *
 * \sa https://www.khronos.org/opengl/wiki/Texture
 */
QuickTexture::Target QuickTexture::target() const
{
    Q_D(const QuickTexture);

    return d->target;
}

void QuickTexture::setTarget(QuickTexture::Target target)
{
    Q_D(QuickTexture);

    if (d->target == target)
        return;

    d->target = target;
    emit targetChanged(target);
    update();
}


/*!
 * \qmlproperty int Texture::mipLevels
 *
 * This property holds the number of mip levels within the texture. The default
 * value is 1.
 *
 * \sa https://www.khronos.org/opengl/wiki/Texture
 */
int QuickTexture::mipLevels() const
{
    Q_D(const QuickTexture);

    return d->mipLevels;
}

void QuickTexture::setMipLevels(int mipLevels)
{
    Q_D(QuickTexture);
    if (d->mipLevels == mipLevels)
        return;

    d->mipLevels = mipLevels;
    emit mipLevelsChanged(mipLevels);
    update();
}

/*!
 * \qmlproperty enumeration Texture::magFilter
 *
 * This property holds the texture magnification filter type. The default value
 * is FilterLinear.
 *
 * magFilter takes the following values:
 *
 * \value FilterNearest
 *   GL_NEAREST
 * \value FilterLinear
 *   GL_LINEAR
 *
 * \sa minFilter
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
QuickTexture::Filter QuickTexture::magFilter() const
{
    Q_D(const QuickTexture);

    return d->magFilter;
}

void QuickTexture::setMagFilter(QuickTexture::Filter magFilter)
{
    Q_D(QuickTexture);
    if (d->magFilter == magFilter)
        return;

    d->magFilter = magFilter;
    emit magFilterChanged(magFilter);
    update();
}

/*!
 * \qmlproperty enumeration Texture::minFilter
 *
 * This property holds the texture minification filter type. The default value
 * is FilterNearestMipMapLinear.
 *
 * minFilter takes the following values:
 *
 * \value FilterNearest
 *   GL_NEAREST
 * \value FilterLinear
 *   GL_LINEAR
 * \value FilterNearestMipMapNearest
 *   GL_NEAREST_MIPMAP_NEAREST
 * \value FilterNearestMipMapLinear
 *   GL_NEAREST_MIPMAP_LINEAR
 * \value FilterLinearMipMapNearest
 *   GL_LINEAR_MIPMAP_NEAREST
 * \value FilterLinearMipMapLinear
 *   GL_LINEAR_MIPMAP_LINEAR
 *
 * \sa magFilter
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
QuickTexture::Filter QuickTexture::minFilter() const
{
    Q_D(const QuickTexture);

    return d->minFilter;
}

void QuickTexture::setMinFilter(QuickTexture::Filter minFilter)
{
    Q_D(QuickTexture);
    if (d->minFilter == minFilter)
        return;

    d->minFilter = minFilter;
    emit minFilterChanged(minFilter);
    update();
}

/*!
 * \qmlproperty enumeration Texture::wrapS
 *
 * This property holds the texture s coordinate wrap parameter. The default
 * value is WrapRepeat.
 *
 * wrapS takes the following values:
 *
 * \value WrapRepeat
 *   GL_REPEAT
 * \value WrapMirroredRepeat
 *   GL_MIRRORED_REPEAT
 * \value WrapClampToEdge
 *   GL_CLAMP_TO_EDGE
 * \value WrapClampToBorder
 *   GL_CLAMP_TO_BORDER
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
QuickTexture::Wrap QuickTexture::wrapS() const
{
    Q_D(const QuickTexture);

    return d->wrapS;
}

void QuickTexture::setwrapS(QuickTexture::Wrap wrapS)
{
    Q_D(QuickTexture);

    if (d->wrapS == wrapS)
        return;

    d->wrapS = wrapS;
    emit wrapSChanged(wrapS);
    update();
}

/*!
 * \qmlproperty enumeration Texture::wrapT
 *
 * This property holds the texture t coordinate wrap parameter. The default
 * value is WrapRepeat.
 *
 * wrapT takes the following values:
 *
 * \value WrapRepeat
 *   GL_REPEAT
 * \value WrapMirroredRepeat
 *   GL_MIRRORED_REPEAT
 * \value WrapClampToEdge
 *   GL_CLAMP_TO_EDGE
 * \value WrapClampToBorder
 *   GL_CLAMP_TO_BORDER
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
QuickTexture::Wrap QuickTexture::wrapT() const
{
    Q_D(const QuickTexture);

    return d->wrapT;
}

void QuickTexture::setwrapT(QuickTexture::Wrap wrapT)
{
    Q_D(QuickTexture);
    if (d->wrapT == wrapT)
        return;

    d->wrapT = wrapT;
    emit wrapTChanged(wrapT);
    update();
}

/*!
 * \qmlproperty enumeration Texture::wrapR
 *
 * This property holds the texture r coordinate wrap parameter. The default
 * value is WrapRepeat.
 *
 * wrapR takes the following values:
 *
 * \value WrapRepeat
 *   GL_REPEAT
 * \value WrapMirroredRepeat
 *   GL_MIRRORED_REPEAT
 * \value WrapClampToEdge
 *   GL_CLAMP_TO_EDGE
 * \value WrapClampToBorder
 *   GL_CLAMP_TO_BORDER
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
QuickTexture::Wrap QuickTexture::wrapR() const
{
    Q_D(const QuickTexture);

    return d->wrapR;
}

void QuickTexture::setwrapR(QuickTexture::Wrap wrapR)
{
    Q_D(QuickTexture);
    if (d->wrapR == wrapR)
        return;

    d->wrapR = wrapR;
    emit wrapRChanged(wrapR);
    update();
}

/*!
 * \qmlproperty real Texture::lodBias
 *
 * This property holds the texture level of detail bias. It specifies a fixed
 * bias value that is to be added to the level-of-detail parameter for the
 * texture before texture sampling.  The default value is 0.
 *
 * lodBias takes values in the range [-OpenGLParameters::glMaxTextureLodBias,
 * OpenGLParameters::glMaxTextureLodBias]
 *
 * \sa minLod
 * \sa maxLod
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
double QuickTexture::lodBias() const
{
    Q_D(const QuickTexture);

    return d->lodBias;
}

void QuickTexture::setLodBias(double lodBias)
{
    Q_D(QuickTexture);
    if (d->lodBias == lodBias)
        return;

    d->lodBias = lodBias;
    emit lodBiasChanged(lodBias);
    update();
}

/*!
 * \qmlproperty bool Texture::zeroTexture
 *
 * This property holds the texture's zero initialisation flag.  If enabled,
 * the texture is initialized with zeros.  The default is true.
 *
 */
bool QuickTexture::zeroTexture() const
{
    Q_D(const QuickTexture);
    return d->zeroTexture;
}

void QuickTexture::setZeroTexture(bool zeroTexture)
{
    Q_D(QuickTexture);

    if (d->zeroTexture == zeroTexture)
        return;

    d->zeroTexture = zeroTexture;
    emit zeroTextureChanged(zeroTexture);
    update();
}


/*!
 * \qmlproperty real Texture::maxLod
 *
 * This property holds the texture's maximum level-of-detail parameter. It
 * specifies the lowest resolution mipmap (highest mipmap level) available for
 * selection. The default value is 1000.
 *
 * \sa minLod
 * \sa maxLod
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
double QuickTexture::maxLod() const
{
    Q_D(const QuickTexture);
    return d->maxLod;
}

void QuickTexture::setMaxLod(double maxLod)
{
    Q_D(QuickTexture);

    if (d->maxLod==maxLod)
        return;

    d->maxLod = maxLod;
    emit maxLodChanged(d->maxLod);
    update();
}

/*!
 * \qmlproperty real Texture::minLod
 *
 * This property holds the texture's minimum level-of-detail parameter. It
 * specifies the highest resolution mipmap (lowest mipmap level) available for
 * selection. The default value is -1000.
 *
 * \sa maxLod
 * \sa lodBias
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
double QuickTexture::minLod() const
{
    Q_D(const QuickTexture);
    return d->minLod;
}

void QuickTexture::setMinLod(double minLod)
{
    Q_D(QuickTexture);

    if (d->minLod == minLod)
        return;

    d->minLod = minLod;
    emit minLodChanged(d->minLod);
    update();
}


/*!
 * \qmlproperty color Texture::borderColor
 *
 * This property holds the texture's border color.  The default value is #000000.
 *
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glTexParameter
 */
QColor QuickTexture::borderColor() const
{
    Q_D(const QuickTexture);
    return d->borderColor;
}

void QuickTexture::setBorderColor(QColor borderColor)
{
    Q_D(QuickTexture);

    if (d->borderColor == borderColor)
        return;

    d->borderColor = borderColor;
    emit borderColorChanged(d->borderColor);
    update();
}

/*!
 * \qmlproperty int Texture::imageLevel
 *
 * This property holds the texture's image MIP level.  It specifies the image
 * level used when the texture is bound as an image texture.  The default value
 * is 0.
 *
 * \note To bind more than one MIP level image, see \l ImageHandle.
 *
 * \sa ImageHandle
 * \sa https://www.khronos.org/opengl/wiki/GLAPI/glBindImageTexture
 */
int QuickTexture::imageLevel() const
{
    Q_D(const QuickTexture);

    return d->imageLevel;

}

void QuickTexture::setImageLevel(int imageLevel)
{
    Q_D(QuickTexture);

    if (d->imageLevel == imageLevel)
        return;

    d->imageLevel = imageLevel;
    emit imageLevelChanged(d->imageLevel);
    update();
}


/*!
 * \qmlproperty var Texture::data
 *
 * This property holds the texture data.
 *
 * When \l syncToOpenGL is enabled, data may be loaded from QML into a texture. Data must be able
 * to be converted to a QByteArray containing the texture data in the format expected by OpenGL.
 * For mipmapped textures, data should be a list of objects able to be converted to QByteArray,
 * each containing texture data in the format expected by OpenGL for the relevant mip level.
 *
 * When \l syncFromOpenGL is enabled, data is copied from the OpenGL texture.  It is returned as
 * a \l{buffer}.
 *
 * \sa syncToOpenGL
 * \sa syncFromOpenGL
 * \sa buffer
 */

QVariant QuickTexture::data() const
{
    Q_D(const QuickTexture);

    return d->data;
}

void QuickTexture::setData(const QVariant &data)
{
    Q_D(QuickTexture);

    if (d->data == data)
        return;

    d->data = data;

    if(data.canConvert<QVariantList>())
    {
        for(const QVariant &item: data.toList())
        {
            if(!item.canConvert<QByteArray>())
            {
                qWarning() << "QuickOpenGL:" << qobject_cast<QObject *>(this) << "cannot convert data to QByteArray";
                break;
            }
        }
    }
    else if(!data.canConvert<QByteArray>())
    {
        qWarning() << "QuickOpenGL:" << qobject_cast<QObject *>(this) << "cannot convert data to QByteArray";
    }

    d->dataDirty = true;

    emit dataChanged(d->data);

    // if this update was not due to a texture read
    if(sender()!=d->textureProvider)
    {
        update();
    }

}

void QuickTexture::setSyncFromOpenGL(bool syncFromOpenGL)
{
    Q_D(QuickTexture);

    if (d->syncFromOpenGL == syncFromOpenGL)
        return;

    d->syncFromOpenGL = syncFromOpenGL;
    emit syncFromOpenGLChanged(d->syncFromOpenGL);
    update();
}

void QuickTexture::setSyncToOpenGL(QuickTexture::SyncWrite syncToOpenGL)
{
    Q_D(QuickTexture);
    if (d->syncToOpenGL == syncToOpenGL)
        return;

    d->syncToOpenGL = syncToOpenGL;
    emit syncToOpenGLChanged(d->syncToOpenGL);
    update();
}

void QuickTexture::releaseResources()
{
    Q_D(QuickTexture);

    if(d->textureProvider)
    {
        window()->scheduleRenderJob(cleanup(d->textureProvider), QQuickWindow::BeforeSynchronizingStage);
        d->textureProvider = nullptr;
    }

}

bool QuickTexture::isTextureProvider() const
{
    return true;
}

QSGTextureProvider *QuickTexture::textureProvider() const
{
    Q_D(const QuickTexture);


    // if we are in the thread that owns the OpenGL context
    if(window()&&window()->openglContext()&&window()->openglContext()->thread()&&(window()->openglContext()->thread()==QThread::currentThread()))
    {
        bool writeTextureData = false;

        // cast away the const
        QuickTexturePrivate *dd = const_cast<QuickTexturePrivate*>(d);

        // if we don't already have a texture provider
        if(!dd->textureProvider)
        {
            // create a new texture provider
            dd->textureProvider = new QuickSGTextureProvider;

            dd->textureProvider->setProperty("QmlItem", QVariant::fromValue(const_cast<QuickTexture *>(this)));
            dd->textureProvider->setSyncFromOpenGL(d->syncFromOpenGL);

            connect(window(), &QQuickWindow::sceneGraphInvalidated, dd, &QuickTexturePrivate::onSceneGraphInvalidated, Qt::DirectConnection);
            connect(window(), &QQuickWindow::afterRendering, dd->textureProvider, &QuickSGTextureProvider::onAfterRendering, Qt::DirectConnection);
            connect(window(), &QQuickWindow::beforeSynchronizing, qobject_cast<QuickSGTexture *>(dd->textureProvider->texture()), &QuickSGTexture::onBeforeSynchronizing, Qt::DirectConnection);

            connect(dd->textureProvider, &QuickSGTextureProvider::textureDataChanged, this, &QuickTexture::setData, Qt::QueuedConnection);

            writeTextureData |= d->data.isValid()&&(d->syncToOpenGL==QuickTexture::SyncWriteOnce);

            // Get the texture
            QuickSGTexture *texture = qobject_cast<QuickSGTexture *>(dd->textureProvider->texture());

            // Add a record of the owner QQuickItem to the texture for debug message context
            texture->setProperty("QuickTexture", QVariant::fromValue(const_cast<QObject *>(qobject_cast<const QObject *>(this))));
        }

        QuickSGTexture *texture = qobject_cast<QuickSGTexture *>(dd->textureProvider->texture());

        // Apply properties to the texture
        texture->setTarget(d->target);
        texture->setFormat(d->format);
        texture->setWidth(width());
        texture->setHeight(height());
        texture->setDepth(d->depth);
        texture->setLayers(d->layers);
        texture->setImageLayer(d->imageLayer);
        texture->setImageLevel(d->imageLevel);
        texture->setImageAccess(d->imageAccess);
        texture->setWrapS(d->wrapS);
        texture->setWrapT(d->wrapT);
        texture->setWrapR(d->wrapR);
        texture->setMipLevels(d->mipLevels);
        texture->setMagFilter(d->magFilter);
        texture->setMinFilter(d->minFilter);
        texture->setLodBias(d->lodBias);
        texture->setMaxLod(d->maxLod);
        texture->setMinLod(d->minLod);
        texture->setBorderColor(d->borderColor);
        texture->setZeroTexture(d->zeroTexture);


        // if data has been specified and we are to write it to the texture
        writeTextureData |= d->data.isValid()&&(d->syncToOpenGL==QuickTexture::SyncWriteAlways);
        writeTextureData |= d->data.isValid()&&((d->syncToOpenGL==QuickTexture::SyncWriteOnUpdated)&&d->dataDirty);

        if(writeTextureData)
        {
            QList<QByteArray> data;

            // future capability - load texture from file
/*            if(d->data.canConvert<QString>()&&QFile::exists(d->data.value<QString>()))
            {
                texture->setFilename(d->data.value<QString>());
            }
            else */
            if(d->data.canConvert<QVariantList>())
            {
                for(const QVariant &item: d->data.toList())
                {
                    if(item.canConvert<QByteArray>())
                    {
                        data.append(item.value<QByteArray>());
                    }
                }
            }
            else
            {
                if(d->data.canConvert<QByteArray>())
                {
                    data.append(d->data.value<QByteArray>());
                }
            }

            if(data.count())
            {
                QuickSGTexture *texture = qobject_cast<QuickSGTexture *>(dd->textureProvider->texture());
                texture->setTextureData(data);
            }

            dd->dataDirty = false;
        }

        // return the texture provider
        return d->textureProvider;
    }
    else
    {
        qWarning("QQuickImage::textureProvider: can only be queried on the rendering thread of an exposed window");
    }

    return nullptr;
}

QSGNode *QuickTexture::updatePaintNode(QSGNode *, UpdatePaintNodeData *)
{

    textureProvider();
    return nullptr;
}

/*!
 * \qmlproperty bool Texture::syncFromOpenGL
 *
 * This property holds a flag indicating whether QuickOpenGL should retrieve texture data from OpenGL
 * and set it to the Texture's \l{data} property.  The default value is false.
 *
 */

bool QuickTexture::syncFromOpenGL() const
{
    Q_D(const QuickTexture);

    return d->syncFromOpenGL;
}

/*!
 * \qmlproperty enumeration Texture::syncToOpenGL
 *
 * This property holds a flag indicating whether QuickOpenGL should write the Texture's \l{data}
 * property to OpenGL. The default value is SyncWriteOnUpdated.
 *
 * syncToOpenGL takes the following values:
 *
 * \value SyncWriteNone
 *   Never write QML values to OpenGL
 * \value SyncWriteOnce
 *   Write QML values to OpenGL on textue creation
 * \value SyncWriteOnUpdated
 *   Write QML values to OpenGL whenever the QML values change
 * \value SyncWriteAlways
 *   Write QML values to OpenGL on every scenegraph update
 *
 */

QuickTexture::SyncWrite QuickTexture::syncToOpenGL() const
{
    Q_D(const QuickTexture);
    return d->syncToOpenGL;
}

#include "quicktexture.moc"

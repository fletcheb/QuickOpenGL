/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#include "openglextension_arb_compute_variable_group_size.h"
#include <QOpenGLContext>

class QOpenGLExtension_ARB_compute_variable_group_sizePrivate : public QAbstractOpenGLExtensionPrivate
{
public:
    void (QOPENGLF_APIENTRYP DispatchComputeGroupSizeARB)(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z, GLuint group_size_x, GLuint group_size_y, GLuint group_size_z);
};


QOpenGLExtension_ARB_compute_variable_group_size::QOpenGLExtension_ARB_compute_variable_group_size()
 : QAbstractOpenGLExtension(*(new QOpenGLExtension_ARB_compute_variable_group_sizePrivate))
{
}

bool QOpenGLExtension_ARB_compute_variable_group_size::initializeOpenGLFunctions()
{
    if (isInitialized())
        return true;

    QOpenGLContext *context = QOpenGLContext::currentContext();
    if (!context) {
        qWarning("A current OpenGL context is required to resolve OpenGL extension functions");
        return false;
    }

    // Resolve the functions
    Q_D(QOpenGLExtension_ARB_compute_variable_group_size);

    d->DispatchComputeGroupSizeARB = reinterpret_cast<void (QOPENGLF_APIENTRYP)(GLuint, GLuint, GLuint, GLuint, GLuint, GLuint)>(context->getProcAddress("glDispatchComputeGroupSizeARB"));
    QAbstractOpenGLExtension::initializeOpenGLFunctions();
    return d->DispatchComputeGroupSizeARB!=nullptr;
}

void QOpenGLExtension_ARB_compute_variable_group_size::glDispatchComputeGroupSizeARB(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z, GLuint group_size_x, GLuint group_size_y, GLuint group_size_z)
{
    Q_D(QOpenGLExtension_ARB_compute_variable_group_size);
    d->DispatchComputeGroupSizeARB(num_groups_x, num_groups_y, num_groups_z, group_size_x, group_size_y, group_size_z);
}



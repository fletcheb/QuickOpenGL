/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKSGTEXTUREVIEWTEXTUREPROVIDER_H
#define QUICKSGTEXTUREVIEWTEXTUREPROVIDER_H

#include <QSGTextureProvider>
#include "quicksgtextureviewtexture.h"

class QuickSGTextureViewTextureProvider : public QSGTextureProvider
{
    Q_OBJECT

    Q_PROPERTY(QSGTextureProvider * sourceProvider READ sourceProvider WRITE setSourceProvider NOTIFY sourceProviderChanged)

public:

    QuickSGTextureViewTextureProvider();

    QSGTexture *texture() const override;

    QSGTextureProvider * sourceProvider() const;

public slots:
    void setSourceProvider(QSGTextureProvider * sourceProvider);
    void onSourceProviderDestroyed();

signals:
    void sourceProviderChanged(QSGTextureProvider * sourceProvider);

protected:

    QuickSGTextureViewTexture m_texture;
    QSGTextureProvider * m_sourceProvider;

};

#endif // QUICKSGTEXTUREVIEWTEXTUREPROVIDER_H

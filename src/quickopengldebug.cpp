#include "quickopengldebug.h"


/*!
 * \qmltype QuickOpenGLDebug
 * \brief The QuickOpenGLDebug QML type controls output of console debug information.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickOpenGLDebug
 * \ingroup QuickOpenGL QML Types
 *
 *
 * QuickOpenGLDebug controls output of console debug information.  This information includes
 * timing of OpenGL operations performed by QuickOpenGL.
 *
 * The flags may be set in the system environment as environment variables named either
 * DEBUG_QUICKOPENGL, or QUICKOPENGL_DEBUG.  Supported values are:
 *
 * \table
 * \header
 *      \li Value
 *      \li QML Enumeration
 *      \li Description
 * \row
 *      \li 1
 *      \li FlagTiming
 *      \li Enable debug output of timings
 * \endtable
 *
 * The flags may be controlled dynamically within a QML application as follows:
 *
 * \qml
 * QuickOpenGLDebug {
 *   flags: QuickOpenGLDebug.FlagTiming
 * }
 * \endqml
 *
 */

/*!
 * \qmlproperty enum QuickOpenGLDebug::flags
 *
 * This property holds the QuickOpenGL debug flags.  Supported values are:
 *
 * \table
 * \header
 *      \li Value
 *      \li QML Enumeration
 *      \li Description
 * \row
 *      \li 1
 *      \li FlagTiming
 *      \li Enable debug output of timings
 * \endtable
 */

extern int g_debugLevel;

QuickOpenGLDebug::QuickOpenGLDebug(QObject *parent) :
    QObject(parent)
{

}

QuickOpenGLDebug::Flags QuickOpenGLDebug::flags() const
{
    return Flags(g_debugLevel);
}

void QuickOpenGLDebug::setFlags(Flags flags)
{
    g_debugLevel = int(flags);
}

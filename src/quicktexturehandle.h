/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef TEXTUREHANDLE_H
#define TEXTUREHANDLE_H

#include <QQuickItem>
#include <QScopedPointer>



class QuickSGTextureHandleProvider;
class QuickTextureHandlePrivate;

class QuickTextureHandle : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QQuickItem * source READ source WRITE setSource NOTIFY sourceChanged)

public:

    QuickTextureHandle(QQuickItem *parent = 0);
    ~QuickTextureHandle();

    virtual QuickSGTextureHandleProvider *textureHandleProvider();

    QQuickItem * source() const;

signals:

    void sourceChanged(QQuickItem * source);

public slots:

void setSource(QQuickItem * source);

protected:

    QScopedPointer<QuickTextureHandlePrivate> d_ptr;
    QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *) override;
    void releaseResources() override;
private:

    Q_DECLARE_PRIVATE(QuickTextureHandle)

};


#endif // TEXTUREHANDLE_H

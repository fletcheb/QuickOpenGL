TEMPLATE = lib
QT += qml quick opengl openglextensions concurrent positioning
TARGET = $$qtLibraryTarget(quickopengl)
CONFIG += qt plugin c++11 qt_docs_targets file_copies
VERSION = 1.0
DEFINES += QT_DEPRECATED_WARNINGS

#export symbols w/ MSVC/mingw (creates the import .lib)
windows: DEFINES += QUICKOPENGL_LIBRARY
windows: CONFIG -= debug_and_release #suppresses the creation of debug/release subfolders in build dir
windows: CONFIG += skip_target_version_ext #don't stick the version number on the library filenames

# QML import name
uri = QuickOpenGL

DESTDIR = $$uri

SOURCES += \
    quickopengl_plugin.cpp \
    quickcomputeprogram.cpp \
    quickgraphicsprogram.cpp \
    introspection.cpp \
    quickgraphicsprogrammaterial.cpp \
    quickgraphicsprogrammaterialshader.cpp \
    support.cpp \
    quicktexture.cpp \
    quicksgtexture.cpp \
    quicksgtextureprovider.cpp \
    quicksgssboprovider.cpp \
    quicksgssbo.cpp \
    quickopengltypeinstantiator.cpp \
    quickopenglthreadpool.cpp \
    quickshaderstoragebuffer.cpp \
    quicksampler.cpp \
    openglextension_arb_bindless_texture.cpp \
    openglextension_arb_compute_variable_group_size.cpp \
    quickuniformbuffer.cpp \
    quicksguniformbufferprovider.cpp \
    quicksguniformbuffer.cpp \
    quicktexturehandle.cpp \
    quickimagehandle.cpp \
    quicksgtexturehandleprovider.cpp \
    quicksgimagehandleprovider.cpp \
    quickopenglproperties.cpp \
    quicksgsamplertextureprovider.cpp \
    quicksgtextureviewtextureprovider.cpp \
    quicktextureview.cpp \
    quicksgtextureviewtexture.cpp \
    quickopengldebuglogger.cpp \
    quickopengldebugmessage.cpp \
    quickvector4.cpp \
    quickvector3.cpp \
    quickvector2.cpp \
    quickopengldebug.cpp \
    quickopengloffscreensurfacefactory.cpp \
    quickbuffer.cpp \
    quickmatrix4.cpp \
    quickmatrix3.cpp \
    quickmatrix2.cpp

HEADERS += \
    quickopengl_plugin.h \
    quickcomputeprogram.h \
    quickgraphicsprogram.h \
    introspection.h \
    quickgraphicsprogrammaterial.h \
    quickgraphicsprogrammaterialshader.h \
    support.h \
    quicktexture.h \
    quicksgtexture.h \
    quicksgtextureprovider.h \
    quicksgssbo.h \
    quicksgssboprovider.h \
    quickvector2.h \
    quickvector3.h \
    quickvector4.h \
    quickopengltypeinstantiator.h \
    quickopengl_global.h \
    quickopenglthreadpool.h \
    quicktextureview.h \
    quickshaderstoragebuffer.h \
    quicksampler.h \
    openglextension_arb_bindless_texture.h \
    openglextension_arb_compute_variable_group_size.h \
    quickuniformbuffer.h \
    quicksguniformbufferprovider.h \
    quicksguniformbuffer.h \
    quicktexturehandle.h \
    quickimagehandle.h \
    quicksgtexturehandleprovider.h \
    quicksgimagehandleprovider.h \
    quickopenglproperties.h \
    quicksgsamplertextureprovider.h \
    quicksgtextureviewtextureprovider.h \
    quicksgtextureviewtexture.h \
    quickopengldebuglogger.h \
    quickopengldebugmessage.h \
    quickopengldebug.h \
    quickopengloffscreensurfacefactory.h \
    quickbuffer.h \
    quickmatrix4.h \
    quickmatrix3.h \
    quickmatrix2.h

DISTFILES += qmldir \
    MapGraphicsProgram.qml

windows{

    COPIES += distfiles
    distfiles.files += $$DISTFILES
    distfiles.path += $$OUT_PWD/$$DESTDIR

    QMAKE_POST_LINK += $$_PRO_FILE_PWD_/dump.bat $$system_quote($$[QT_INSTALL_BINS]/qmlplugindump) $$uri $$VERSION $$system_quote($$OUT_PWD) $$system_quote($$OUT_PWD/$$DESTDIR/plugins.qmltypes)

    message("TODO:install")
}
else {

    for(file, DISTFILES) {
        infile = $$_PRO_FILE_PWD_/$$file
        outfile = $$OUT_PWD/$$DESTDIR/$$file
        QMAKE_POST_LINK += $(SYMLINK) \"$$replace(infile, /, $$QMAKE_DIR_SEP)\" \"$$replace(outfile, /, $$QMAKE_DIR_SEP)\";

    }
    QMAKE_POST_LINK += $$[QT_INSTALL_BINS]/qmlplugindump -nonrelocatable $$uri $$VERSION $$OUT_PWD > "$$OUT_PWD/$$DESTDIR/plugins.qmltypes";
}

INCLUDEFILES += quickmatrix2.h \
                quickmatrix3.h \
                quickmatrix4.h \
                quickvector2.h \
                quickvector3.h \
                quickvector4.h \
                quickopengl_global.h

unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(uri, \\., /)
    target.path = $$installPath

    distfiles.files = $$DISTFILES
    distfiles.path = $$installPath

    qmltypes.path = $$installPath
    qmltypes.files = $$OUT_PWD/$$DESTDIR/plugins.qmltypes

    includes.files =$$INCLUDEFILES
    includes.path = $$installPath/include

    INSTALLS += target distfiles qmltypes includes
}

ios {
    CONFIG += static
    QMAKE_MOC_OPTIONS += -Muri=$$uri
    # In static builds, the QML engine reads also the qmldir file from the resources.
    $${uri}.files += $$DISTFILES
    # qt-project.org/imports is the path used for locating imports inside the resources
    $${uri}.prefix = /qt-project.org/imports/$${uri}
    RESOURCES += $${uri}
}

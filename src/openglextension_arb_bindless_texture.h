/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#ifndef OPENGLEXTENSION_ARB_BINDLESS_TEXTURE_H
#define OPENGLEXTENSION_ARB_BINDLESS_TEXTURE_H

#include <QtOpenGLExtensions/QOpenGLExtensions>

#ifndef GL_UNSIGNED_INT64_ARB
#define GL_UNSIGNED_INT64_ARB 0x140F
#endif

class QOpenGLExtension_ARB_bindless_texturePrivate;

class QOpenGLExtension_ARB_bindless_texture : public QAbstractOpenGLExtension
{
public:

    QOpenGLExtension_ARB_bindless_texture();

    bool initializeOpenGLFunctions() Q_DECL_FINAL;

    GLuint64 glGetTextureHandleARB(GLuint texture);
    GLuint64 glGetTextureSamplerHandleARB(GLuint texture, GLuint sampler);

    void glMakeTextureHandleResidentARB(GLuint64 handle);
    void glMakeTextureHandleNonResidentARB(GLuint64 handle);

    GLuint64 glGetImageHandleARB(GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);

    void glMakeImageHandleResidentARB(GLuint64 handle, GLenum access);
    void glMakeImageHandleNonResidentARB(GLuint64 handle);

    void glUniformHandleui64ARB(GLint location, GLuint64 value);
    void glUniformHandleui64vARB(GLint location, GLsizei count, const GLuint64 *value);
    void glProgramUniformHandleui64ARB(GLuint program, GLint location, GLuint64 value);
    void glProgramUniformHandleui64vARB(GLuint program, GLint location, GLsizei count, const GLuint64 *values);

    GLboolean glIsTextureHandleResidentARB(GLuint64 handle);
    GLboolean glIsImageHandleResidentARB(GLuint64 handle);

    void glVertexAttribL1ui64ARB(GLuint index, GLuint64 x);
    void glVertexAttribL1ui64vARB(GLuint index, const GLuint64 *v);
    void glGetVertexAttribLui64vARB(GLuint index, GLenum pname, GLuint64 *params);

protected:

    Q_DECLARE_PRIVATE(QOpenGLExtension_ARB_bindless_texture)
};

#endif // OPENGLEXTENSION_ARB_BINDLESS_TEXTURE_H

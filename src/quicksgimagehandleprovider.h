/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/


#ifndef QUICKSGIMAGEHANDLEPROVIDER_H
#define QUICKSGIMAGEHANDLEPROVIDER_H

#include <QObject>
#include <QScopedPointer>
#include <QSGTextureProvider>
#include "quickimagehandle.h"

class QuickSGImageHandleProviderPrivate;


class QuickSGImageHandleProvider : public QObject
{
    Q_OBJECT

public:
    QuickSGImageHandleProvider();
    ~QuickSGImageHandleProvider();

    quint64 handle() const;

    void makeResident() const;

    void makeNonResident() const;

    void setSourceProvider(QSGTextureProvider *sourceProvider);
    QSGTextureProvider *sourceProvider() const;

    void setAccess(QuickImageHandle::Access access);
    QuickImageHandle::Access access() const;

    void setLevel(int level);

    void setLayered(bool layered);

    void setLayer(int layer);

    void setFormat(QuickImageHandle::Format format);

public slots:

    void onSourceProviderDestroyed();

signals:

   void imageChanged();

protected:

   QScopedPointer<QuickSGImageHandleProviderPrivate> d_ptr;

private:

   Q_DECLARE_PRIVATE(QuickSGImageHandleProvider)

};

#endif // QUICKSGIMAGEHANDLEPROVIDER_H

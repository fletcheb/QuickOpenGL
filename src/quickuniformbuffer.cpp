/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quickuniformbuffer.h"
#include <QQuickWindow>
#include <QThread>
#include <QOpenGLContext>
#include <QElapsedTimer>
#include "quicksguniformbufferprovider.h"
#include "introspection.h"
#include "support.h"

/*!
 * \qmltype UniformBuffer
 * \brief The UniformBuffer QML type represents an OpenGL uniform buffer object.
 * \inqmlmodule QuickOpenGL
 * \instantiates QuickUniformBuffer
 * \ingroup QuickOpenGL QML Types
 *
 *
 * UniformBuffer allows OpenGL uniform buffer objects to be used directly in
 * QML. A uniform buffer object stores uniform data for a shader program. They
 * can be used to share uniforms between different programs, and quickly change
 * between sets of uniforms for the same program object.
 *
 * Example usage:
 *
 * \qml
 * UniformBuffer
 * {
 *     id: uniformBuffer
 *     property var vertexColor: ['#ff0000', '#00ff00', '#0000ff', '#000000']
 * }
 *
 * GraphicsProgram
 * {
 *     anchors.fill: parent
 *     property var block : uniformBuffer
 *
 *     vertexShader: "
 *         #version 420
 *
 *         in vec4 qt_Vertex;
 *         in int gl_VertexID;
 *         out vec3 color;
 *
 *         uniform mat4 qt_Matrix;
 *
 *         uniform block
 *         {
 *             vec3 vertexColor[4];
 *         };
 *
 *         void main()
 *         {
 *             color = vertexColor[gl_VertexID];
 *             gl_Position = qt_Matrix * qt_Vertex;
 *         }"
 *
 *
 *     fragmentShader: "
 *         #version 420
 *
 *         in vec3 color;
 *         out vec4 fragColor;
 *
 *         void main()
 *         {
 *             fragColor = vec4(color, 1.0);
 *         }"
 * }
 * \endqml
 *
 *
 *
 * \sa https://www.khronos.org/opengl/wiki/Uniform_Buffer_Object
 */
class QuickUniformBufferPrivate : public QObject
{
    Q_OBJECT

public:

    QuickUniformBufferPrivate(QuickUniformBuffer *q);

    QuickSGUniformBufferProvider *uniformBufferProvider;

    QList<int> updatedPropertyIndices;

    QHash<QString, QList<Introspection::Uniform>> baseUniforms;

    QByteArray data;
    int bufferObject;

    static bool setBufferData(QByteArray &data, const Introspection::Uniform &uniform, const QVariantList &property);
    template <typename Type> static void setBufferData(const QVariantList &list, QByteArray &data, const Introspection::Uniform &uniform);

public slots:

    void onBufferIdChanged(int bufferId);
    void onAfterSynchronizing();
    void synchronizeProperties();
    void onSceneGraphInvalidated();
    void update();

protected:

    QuickUniformBuffer *q_ptr;

private:

    Q_DECLARE_PUBLIC(QuickUniformBuffer)
};

QuickUniformBufferPrivate::QuickUniformBufferPrivate(QuickUniformBuffer *q):
    uniformBufferProvider(nullptr),
    bufferObject(-1),
    q_ptr(q)
{

}

void QuickUniformBufferPrivate::onBufferIdChanged(int bufferId)
{
    Q_Q(QuickUniformBuffer);

    bufferObject = bufferId;
    emit q->bufferObjectChanged(bufferObject);
}

void QuickUniformBufferPrivate::onAfterSynchronizing()
{
    synchronizeProperties();
}

template <typename Type> void QuickUniformBufferPrivate::setBufferData(const QVariantList &list, QByteArray &buffer, const Introspection::Uniform &uniform)
{

    for(int i=0;i<qMin(uniform.arraySize, list.count());++i)
    {
        if(list.at(i).canConvert<Type>())
        {
            Q_ASSERT(buffer.size()>=(uniform.offset+i*uniform.arrayStride+int(sizeof(Type))));
            reinterpret_cast<Type &>(buffer.data()[uniform.offset +i*uniform.arrayStride]) = list.at(i).value<Type>();
        }
    }
}

bool QuickUniformBufferPrivate::setBufferData(QByteArray &buffer, const Introspection::Uniform &uniform, const QVariantList &property)
{
    bool result = true;

    // do the appropriate thing for the uniform type
    switch(uniform.type)
    {
    case GL_FLOAT:
        setBufferData<float>(property, buffer, uniform);
        break;
    case GL_FLOAT_VEC2:
        setBufferData<qvec2>(property, buffer, uniform);
        break;
    case GL_FLOAT_VEC3:
        setBufferData<qvec3>(property, buffer, uniform);
        break;
    case GL_FLOAT_VEC4:
        setBufferData<qvec4>(property, buffer, uniform);
        break;
    case GL_DOUBLE:
        setBufferData<double>(property, buffer, uniform);
        break;
    case GL_DOUBLE_VEC2:
        setBufferData<qdvec2>(property, buffer, uniform);
        break;
    case GL_DOUBLE_VEC3:
        setBufferData<qdvec3>(property, buffer, uniform);
        break;
    case GL_DOUBLE_VEC4:
        setBufferData<qdvec4>(property, buffer, uniform);
        break;
    case GL_INT:
        setBufferData<int>(property, buffer, uniform);
        break;
    case GL_INT_VEC2:
        setBufferData<qivec2>(property, buffer, uniform);
        break;
    case GL_INT_VEC3:
        setBufferData<qivec3>(property, buffer, uniform);
        break;
    case GL_INT_VEC4:
        setBufferData<qivec4>(property, buffer, uniform);
        break;
    case GL_UNSIGNED_INT:
        setBufferData<unsigned int>(property, buffer, uniform);
        break;
    case GL_UNSIGNED_INT_VEC2:
        setBufferData<quvec2>(property, buffer, uniform);
        break;
    case GL_UNSIGNED_INT_VEC3:
        setBufferData<quvec3>(property, buffer, uniform);
        break;
    case GL_UNSIGNED_INT_VEC4:
        setBufferData<quvec4>(property, buffer, uniform);
        break;
    case GL_BOOL:
        setBufferData<bool>(property, buffer, uniform);
        break;
    case GL_BOOL_VEC2:
        setBufferData<qbvec2>(property, buffer, uniform);
        break;
    case GL_BOOL_VEC3:
        setBufferData<qbvec3>(property, buffer, uniform);
        break;
    case GL_BOOL_VEC4:
        setBufferData<qbvec4>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT2:
        setBufferData<qmat2>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT3:
        setBufferData<qmat3>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT4:
        setBufferData<qmat4>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT2x3:
        setBufferData<qmat2x3>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT2x4:
        setBufferData<qmat2x4>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT3x2:
        setBufferData<qmat3x2>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT3x4:
        setBufferData<qmat3x4>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT4x2:
        setBufferData<qmat4x2>(property, buffer, uniform);
        break;
    case GL_FLOAT_MAT4x3:
        setBufferData<qmat4x3>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT2:
        setBufferData<qdmat2>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT3:
        setBufferData<qdmat3>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT4:
        setBufferData<qdmat4>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT2x3:
        setBufferData<qdmat2x3>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT2x4:
        setBufferData<qdmat2x4>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT3x2:
        setBufferData<qdmat3x2>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT3x4:
        setBufferData<qdmat3x4>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT4x2:
        setBufferData<qdmat4x2>(property, buffer, uniform);
        break;
    case GL_DOUBLE_MAT4x3:
        setBufferData<qdmat4x3>(property, buffer, uniform);
        break;
    case GL_UNSIGNED_INT64_ARB:
        setBufferData<quint64>(property, buffer, uniform);
        break;
    default:
        qWarning() << Q_FUNC_INFO << "unsupported OpenGL type" << QuickOpenGL::GLenum(uniform.type);
        result = false;
        break;
    }

    return result;
}

void QuickUniformBufferPrivate::synchronizeProperties()
{
    Q_Q(QuickUniformBuffer);

    if(!uniformBufferProvider || !uniformBufferProvider->uniformBlock().isValid())
    {
        return;
    }

    if(baseUniforms.isEmpty())
    {
        for(const Introspection::Uniform &uniform: uniformBufferProvider->uniformBlock().uniforms)
        {
            // Get the base name
            QRegularExpressionMatch match = QRegularExpression(QStringLiteral("([\\w\\d]+)")).match(uniform.name);
            Q_ASSERT(match.hasMatch());
            baseUniforms[match.captured(1)].append(uniform);

            if(q->metaObject()->indexOfProperty(match.captured(1).toLocal8Bit().constData())==-1)
            {
                qWarning() << "QuickOpenGL:" << qobject_cast<QObject *>(q) << "no property matching uniform" << uniform.name << "of type" << QuickOpenGL::GLenum(uniform.type) << "found";
            }
        }
    }

    bool updated = uniformBufferProvider->updated() || (data.size()!=uniformBufferProvider->uniformBlock().size);

    if(data.size()!=uniformBufferProvider->uniformBlock().size)
    {
        data.fill(0, uniformBufferProvider->uniformBlock().size-data.size());
        data.resize(uniformBufferProvider->uniformBlock().size);
    }

    // add our updated QMetaObject properties to our property list
    for(int index: updatedPropertyIndices)
    {
        QMetaProperty property = q->metaObject()->property(index);

        if(baseUniforms.contains(property.name()))
        {
            QVariant variant = Introspection::conditionVariant(property.read(q), q);
            for(const Introspection::Uniform &uniform: baseUniforms[property.name()])
            {
                QVariantList property = Introspection::conditionProperty(variant, uniform.name, uniform.type, uniform.arraySize, "uniform block uniform", q);

                if(!setBufferData(data, uniform, property))
                {
                    qWarning() << "QuickOpenGL:" << qobject_cast<QObject *>(q) << uniform.name << "has unsupported type" << QuickOpenGL::GLenum(uniform.type);
                }
            }

            updated =true;
        }
    }

    uniformBufferProvider->setData(data);
    uniformBufferProvider->setUpdated(updated);
    updatedPropertyIndices.clear();

}

void QuickUniformBufferPrivate::onSceneGraphInvalidated()
{
    if(uniformBufferProvider)
    {
        delete uniformBufferProvider;
        uniformBufferProvider = nullptr;
    }
}

void QuickUniformBufferPrivate::update()
{
    Q_Q(QuickUniformBuffer);

    // Get the name of the notify signal we have received
    QByteArray signalName = sender()->metaObject()->method(senderSignalIndex()).name();
    Q_ASSERT(signalName.endsWith(QByteArrayLiteral("Changed")));

    // Get the index of the property
    int propertyIndex = sender()->metaObject()->indexOfProperty(signalName.left(signalName.count()-7));
    Q_ASSERT(propertyIndex!=-1);

    // Add the property index to the list of updated properties to be copied
    if(!updatedPropertyIndices.contains(propertyIndex))
    {
        updatedPropertyIndices.append(propertyIndex);
    }

    // Trigger an update
    q->update();
}


QuickUniformBuffer::QuickUniformBuffer(QQuickItem *parent):
    QQuickItem(parent),
    d_ptr(new QuickUniformBufferPrivate(this))

{
    setFlag(QQuickItem::ItemHasContents);
}

QuickUniformBuffer::~QuickUniformBuffer()
{
    Q_D(QuickUniformBuffer);

    // we should have already cleaned up by now
    Q_ASSERT(d->uniformBufferProvider==nullptr);
}

QuickSGUniformBufferProvider *QuickUniformBuffer::uniformBufferProvider()
{
    Q_D(QuickUniformBuffer);

    if(window()&&window()->openglContext()&&window()->openglContext()->thread()&&(window()->openglContext()->thread()==QThread::currentThread()))
    {

        // if we don't already have a provider, create one
        if(!d->uniformBufferProvider)
        {
            d->uniformBufferProvider = new QuickSGUniformBufferProvider;
            connect(window(), &QQuickWindow::sceneGraphInvalidated, d, &QuickUniformBufferPrivate::onSceneGraphInvalidated, Qt::DirectConnection);
            connect(d->uniformBufferProvider->uniformBuffer(), &QuickSGUniformBuffer::bufferIdChanged, d, &QuickUniformBufferPrivate::onBufferIdChanged);
            connect(window(), &QQuickWindow::afterSynchronizing, d, &QuickUniformBufferPrivate::onAfterSynchronizing, Qt::DirectConnection);
        }

        return d->uniformBufferProvider;
    }
    else
    {
        qWarning("QuickUinformBuffer::uniformBufferProvider: can only be queried on the rendering thread of an exposed window");
    }

    return nullptr;

}

void QuickUniformBuffer::componentComplete()
{
    Q_D(QuickUniformBuffer);

    // find our private class update slot
    QMetaMethod slot = d->metaObject()->method(d->metaObject()->indexOfMethod("update()"));
    Q_ASSERT(slot.isValid());

    // get our meta object
    const QMetaObject *object = metaObject()->superClass();

    // for each of our properties
    for(int index=object->propertyOffset();index<metaObject()->propertyCount();index++)
    {
        QMetaProperty property = metaObject()->property(index);

        // connect the notify signal to the update slot
        if(property.hasNotifySignal())
        {
            connect(this, property.notifySignal(), d, slot);
        }

        // add the index to the updated indices list
        d->updatedPropertyIndices.append(index);
    }

    QQuickItem::componentComplete();
}

/*!
 * \qmlproperty int UniformBuffer::bufferObject
 *
 * This read only property holds the uniform buffer's OpenGL buffer object. It may be used to help
 * decipher OpenGL debug and error messages. Its default value is -1 until an OpenGL buffer has
 * been generated.
 *
 * \sa https://www.khronos.org/opengl/wiki/Buffer_Object
 */
int QuickUniformBuffer::bufferObject() const
{
    Q_D(const QuickUniformBuffer);

    return d->bufferObject;
}


#include "quickuniformbuffer.moc"

/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "support.h"
#include <QDebug>
#include <QProcessEnvironment>

namespace QuickOpenGL
{

::GLenum formatClass(::GLenum format)
{
    ::GLenum result = GL_NONE;

    switch(format)
    {
    case GL_RGBA32F:
    case GL_RGBA32UI:
    case GL_RGBA32I:
        result = GL_VIEW_CLASS_128_BITS;
        break;
    case GL_RGB32F:
    case GL_RGB32UI:
    case GL_RGB32I:
        result = GL_VIEW_CLASS_96_BITS;
        break;
    case GL_RGBA16F:
    case GL_RG32F:
    case GL_RGBA16UI:
    case GL_RG32UI:
    case GL_RGBA16I:
    case GL_RG32I:
    case GL_RGBA16:
    case GL_RGBA16_SNORM:
        result = GL_VIEW_CLASS_64_BITS;
        break;
    case GL_RGB16:
    case GL_RGB16_SNORM:
    case GL_RGB16F:
    case GL_RGB16UI:
    case GL_RGB16I:
        result = GL_VIEW_CLASS_48_BITS;
        break;
    case GL_RG16F:
    case GL_R11F_G11F_B10F:
    case GL_R32F:
    case GL_RGB10_A2UI:
    case GL_RGBA8UI:
    case GL_RG16UI:
    case GL_R32UI:
    case GL_RGBA8I:
    case GL_RG16I:
    case GL_R32I:
    case GL_RGB10_A2:
    case GL_RGBA8:
    case GL_RG16:
    case GL_RGBA8_SNORM:
    case GL_RG16_SNORM:
    case GL_SRGB8_ALPHA8:
    case GL_RGB9_E5:
        result = GL_VIEW_CLASS_32_BITS;
        break;
    case GL_RGB8:
    case GL_RGB8_SNORM:
    case GL_SRGB8:
    case GL_RGB8UI:
    case GL_RGB8I:
        result = GL_VIEW_CLASS_24_BITS;
        break;
    case GL_R16F:
    case GL_RG8UI:
    case GL_R16UI:
    case GL_RG8I:
    case GL_R16I:
    case GL_RG8:
    case GL_R16:
    case GL_RG8_SNORM:
    case GL_R16_SNORM:
        result = GL_VIEW_CLASS_16_BITS;
        break;
    case GL_R8UI:
    case GL_R8I:
    case GL_R8:
    case GL_R8_SNORM:
        result = GL_VIEW_CLASS_8_BITS;
        break;
    case GL_COMPRESSED_RED_RGTC1:
    case GL_COMPRESSED_SIGNED_RED_RGTC1:
        result = GL_VIEW_CLASS_RGTC1_RED;
        break;
    case GL_COMPRESSED_RG_RGTC2:
    case GL_COMPRESSED_SIGNED_RG_RGTC2:
        result = GL_VIEW_CLASS_RGTC2_RG;
        break;
    case GL_COMPRESSED_RGBA_BPTC_UNORM:
    case GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM:
        result = GL_VIEW_CLASS_BPTC_UNORM;
        break;
    case GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT:
    case GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT:
        result = GL_VIEW_CLASS_BPTC_FLOAT;
        break;
    default:
        break;
    }

    return result;
}

int tupleCount(::GLenum type)
{
    int result = 0;

    switch(type)
    {
    case GL_FLOAT:
    case GL_DOUBLE:
    case GL_INT:
    case GL_UNSIGNED_INT:
    case GL_BOOL:
    case GL_UNSIGNED_INT64_ARB:
    case GL_INT64_ARB:
        result = 1;
        break;

    case GL_FLOAT_VEC2:
    case GL_DOUBLE_VEC2:
    case GL_INT_VEC2:
    case GL_UNSIGNED_INT_VEC2:
    case GL_BOOL_VEC2:
        result = 1;
        break;

    case GL_FLOAT_VEC3:
    case GL_DOUBLE_VEC3:
    case GL_INT_VEC3:
    case GL_UNSIGNED_INT_VEC3:
    case GL_BOOL_VEC3:
        result = 1;
        break;

    case GL_FLOAT_VEC4:
    case GL_DOUBLE_VEC4:
    case GL_INT_VEC4:
    case GL_UNSIGNED_INT_VEC4:
    case GL_BOOL_VEC4:
        result = 1;
        break;

    case GL_FLOAT_MAT2:
    case GL_DOUBLE_MAT2:
        result = 2;
        break;

    case GL_FLOAT_MAT3:
    case GL_DOUBLE_MAT3:
        result = 3;
        break;

    case GL_FLOAT_MAT4:
    case GL_DOUBLE_MAT4:
        result = 4;
        break;

    case GL_FLOAT_MAT2x3:
    case GL_DOUBLE_MAT2x3:
        result = 2;
        break;

    case GL_FLOAT_MAT2x4:
    case GL_DOUBLE_MAT2x4:
        result = 2;
        break;

    case GL_FLOAT_MAT3x2:
    case GL_DOUBLE_MAT3x2:
        result = 3;
        break;

    case GL_FLOAT_MAT3x4:
    case GL_DOUBLE_MAT3x4:
        result = 3;
        break;

    case GL_FLOAT_MAT4x2:
    case GL_DOUBLE_MAT4x2:
        result = 4;
        break;

    case GL_FLOAT_MAT4x3:
    case GL_DOUBLE_MAT4x3:
        result = 4;
        break;

    default:
        qDebug() << Q_FUNC_INFO << "unsupported attribute type" << QuickOpenGL::GLenum(type);
    }

    return result;
}

::GLenum primitiveType(::GLenum type)
{
    ::GLenum result = GL_INVALID_ENUM;

    switch(type)
    {
    case GL_FLOAT:
    case GL_FLOAT_VEC2:
    case GL_FLOAT_VEC3:
    case GL_FLOAT_VEC4:
    case GL_FLOAT_MAT2:
    case GL_FLOAT_MAT3:
    case GL_FLOAT_MAT4:
    case GL_FLOAT_MAT2x3:
    case GL_FLOAT_MAT2x4:
    case GL_FLOAT_MAT3x2:
    case GL_FLOAT_MAT3x4:
    case GL_FLOAT_MAT4x2:
    case GL_FLOAT_MAT4x3:
        result = GL_FLOAT;
        break;

    case GL_DOUBLE:
    case GL_DOUBLE_VEC2:
    case GL_DOUBLE_VEC3:
    case GL_DOUBLE_VEC4:
    case GL_DOUBLE_MAT2:
    case GL_DOUBLE_MAT3:
    case GL_DOUBLE_MAT4:
    case GL_DOUBLE_MAT2x3:
    case GL_DOUBLE_MAT2x4:
    case GL_DOUBLE_MAT3x2:
    case GL_DOUBLE_MAT3x4:
    case GL_DOUBLE_MAT4x2:
    case GL_DOUBLE_MAT4x3:
        result = GL_DOUBLE;
        break;
    case GL_INT:
    case GL_INT_VEC2:
    case GL_INT_VEC3:
    case GL_INT_VEC4:
        result = GL_INT;
        break;
    case GL_UNSIGNED_INT:
    case GL_UNSIGNED_INT_VEC2:
    case GL_UNSIGNED_INT_VEC3:
    case GL_UNSIGNED_INT_VEC4:
        result = GL_UNSIGNED_INT;
        break;
    case GL_BOOL:
    case GL_BOOL_VEC2:
    case GL_BOOL_VEC3:
    case GL_BOOL_VEC4:
        result = GL_BOOL;
        break;
    case GL_UNSIGNED_INT64_ARB:
        result =  GL_UNSIGNED_INT64_ARB;
        break;
    case GL_INT64_ARB:
        result = GL_INT64_ARB;
        break;
    default:
        qDebug() << Q_FUNC_INFO << "unsupported attribute type" << QuickOpenGL::GLenum(type);
        break;
    }

    return result;

}

int tupleSize(::GLenum type)
{
    int result = 0;

    switch(type)
    {
    case GL_FLOAT:
    case GL_DOUBLE:
    case GL_INT:
    case GL_UNSIGNED_INT:
    case GL_BOOL:
    case GL_UNSIGNED_INT64_ARB:
    case GL_INT64_ARB:
        result = 1;
        break;

    case GL_FLOAT_VEC2:
    case GL_DOUBLE_VEC2:
    case GL_INT_VEC2:
    case GL_UNSIGNED_INT_VEC2:
    case GL_BOOL_VEC2:
        result = 2;
        break;

    case GL_FLOAT_VEC3:
    case GL_DOUBLE_VEC3:
    case GL_INT_VEC3:
    case GL_UNSIGNED_INT_VEC3:
    case GL_BOOL_VEC3:
        result = 3;
        break;

    case GL_FLOAT_VEC4:
    case GL_DOUBLE_VEC4:
    case GL_INT_VEC4:
    case GL_UNSIGNED_INT_VEC4:
    case GL_BOOL_VEC4:
        result = 4;
        break;

    case GL_FLOAT_MAT2:
    case GL_DOUBLE_MAT2:
        result = 2;
        break;

    case GL_FLOAT_MAT3:
    case GL_DOUBLE_MAT3:
        result = 3;
        break;

    case GL_FLOAT_MAT4:
    case GL_DOUBLE_MAT4:
        result = 4;
        break;

    case GL_FLOAT_MAT2x3:
    case GL_DOUBLE_MAT2x3:
        result = 3;
        break;

    case GL_FLOAT_MAT2x4:
    case GL_DOUBLE_MAT2x4:
        result = 4;
        break;

    case GL_FLOAT_MAT3x2:
    case GL_DOUBLE_MAT3x2:
        result = 2;
        break;

    case GL_FLOAT_MAT3x4:
    case GL_DOUBLE_MAT3x4:
        result = 4;
        break;

    case GL_FLOAT_MAT4x2:
    case GL_DOUBLE_MAT4x2:
        result = 2;
        break;

    case GL_FLOAT_MAT4x3:
    case GL_DOUBLE_MAT4x3:
        result = 3;
        break;

    default:
        qDebug() << Q_FUNC_INFO << "unsupported attribute type" << QuickOpenGL::GLenum(type);
    }

    return result;
}



int primitiveSize(::GLenum type)
{
    int result = 0;

    switch(type)
    {
    case GL_INT:
    case GL_FLOAT:
    case GL_UNSIGNED_INT:
        result = 4;
        break;
    case GL_DOUBLE:
    case GL_UNSIGNED_INT64_ARB:
    case GL_INT64_ARB:
        result = 8;
        break;
    case GL_BOOL:
        result = 1;
        break;
    default:
        qDebug() << Q_FUNC_INFO << "unsupported primitive type" << QuickOpenGL::GLenum(type);

    }

    return result;
}

bool isImage(::GLenum type)
{
    bool result = false;

    switch(type)
    {
    case GL_IMAGE_1D:
    case GL_IMAGE_2D:
    case GL_IMAGE_3D:
    case GL_IMAGE_2D_RECT:
    case GL_IMAGE_CUBE:
    case GL_IMAGE_BUFFER:
    case GL_IMAGE_1D_ARRAY:
    case GL_IMAGE_2D_ARRAY:
    case GL_IMAGE_2D_MULTISAMPLE:
    case GL_IMAGE_2D_MULTISAMPLE_ARRAY:
    case GL_INT_IMAGE_1D:
    case GL_INT_IMAGE_2D:
    case GL_INT_IMAGE_3D:
    case GL_INT_IMAGE_2D_RECT:
    case GL_INT_IMAGE_CUBE:
    case GL_INT_IMAGE_BUFFER:
    case GL_INT_IMAGE_1D_ARRAY:
    case GL_INT_IMAGE_2D_ARRAY:
    case GL_INT_IMAGE_2D_MULTISAMPLE:
    case GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
    case GL_UNSIGNED_INT_IMAGE_1D:
    case GL_UNSIGNED_INT_IMAGE_2D:
    case GL_UNSIGNED_INT_IMAGE_3D:
    case GL_UNSIGNED_INT_IMAGE_2D_RECT:
    case GL_UNSIGNED_INT_IMAGE_CUBE:
    case GL_UNSIGNED_INT_IMAGE_BUFFER:
    case GL_UNSIGNED_INT_IMAGE_1D_ARRAY:
    case GL_UNSIGNED_INT_IMAGE_2D_ARRAY:
    case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE:
    case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
        result = true;
        break;
    default:
        break;
    }

    return result;
}


QString toString(::GLenum value)
{
    QString name;

    switch(value)
    {
    case GL_NO_ERROR:
        name = "GL_NO_ERROR";
        break;
    case GL_INVALID_ENUM:
        name = "GL_INVALID_ENUM";
        break;
    case GL_INVALID_VALUE:
        name = "GL_INVALID_VALUE";
        break;
    case GL_INVALID_OPERATION:
        name = "GL_INVALID_OPERATION";
        break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        name = "GL_INVALID_FRAMEBUFFER_OPERATION";
        break;
    case GL_OUT_OF_MEMORY:
        name = "GL_OUT_OF_MEMORY";
        break;
    case GL_STACK_UNDERFLOW:
        name = "GL_STACK_UNDERFLOW";
        break;
    case GL_STACK_OVERFLOW:
        name = "GL_STACK_OVERFLOW";
        break;
    case GL_FLOAT:
        name = "GL_FLOAT";
        break;
    case GL_FLOAT_VEC2:
        name = "GL_FLOAT_VEC2";
        break;
    case GL_FLOAT_VEC3:
        name = "GL_FLOAT_VEC3";
        break;
    case GL_FLOAT_VEC4:
        name = "GL_FLOAT_VEC4";
        break;
    case GL_DOUBLE:
        name = "GL_DOUBLE";
        break;
    case GL_DOUBLE_VEC2:
        name = "GL_DOUBLE_VEC2";
        break;
    case GL_DOUBLE_VEC3:
        name = "GL_DOUBLE_VEC3";
        break;
    case GL_DOUBLE_VEC4:
        name = "GL_DOUBLE_VEC4";
        break;
    case GL_INT:
        name = "GL_INT";
        break;
    case GL_INT_VEC2:
        name = "GL_INT_VEC2";
        break;
    case GL_INT_VEC3:
        name = "GL_INT_VEC3";
        break;
    case GL_INT_VEC4:
        name = "GL_INT_VEC4";
        break;
    case GL_UNSIGNED_INT:
        name = "GL_UNSIGNED_INT";
        break;
    case GL_UNSIGNED_INT_VEC2:
        name = "GL_UNSIGNED_INT_VEC2";
        break;
    case GL_UNSIGNED_INT_VEC3:
        name = "GL_UNSIGNED_INT_VEC3";
        break;
    case GL_UNSIGNED_INT_VEC4:
        name = "GL_UNSIGNED_INT_VEC4";
        break;
    case GL_BOOL:
        name = "GL_BOOL";
        break;
    case GL_BOOL_VEC2:
        name = "GL_BOOL_VEC2";
        break;
    case GL_BOOL_VEC3:
        name = "GL_BOOL_VEC3";
        break;
    case GL_BOOL_VEC4:
        name = "GL_BOOL_VEC4";
        break;
    case GL_FLOAT_MAT2:
        name = "GL_FLOAT_MAT2";
        break;
    case GL_FLOAT_MAT3:
        name = "GL_FLOAT_MAT3";
        break;
    case GL_FLOAT_MAT4:
        name = "GL_FLOAT_MAT4";
        break;
    case GL_FLOAT_MAT2x3:
        name = "GL_FLOAT_MAT2x3";
        break;
    case GL_FLOAT_MAT2x4:
        name = "GL_FLOAT_MAT2x4";
        break;
    case GL_FLOAT_MAT3x2:
        name = "GL_FLOAT_MAT3x2";
        break;
    case GL_FLOAT_MAT3x4:
        name = "GL_FLOAT_MAT3x4";
        break;
    case GL_FLOAT_MAT4x2:
        name = "GL_FLOAT_MAT4x2";
        break;
    case GL_FLOAT_MAT4x3:
        name = "GL_FLOAT_MAT4x3";
        break;
    case GL_DOUBLE_MAT2:
        name = "GL_DOUBLE_MAT2";
        break;
    case GL_DOUBLE_MAT3:
        name = "GL_DOUBLE_MAT3";
        break;
    case GL_DOUBLE_MAT4:
        name = "GL_DOUBLE_MAT4";
        break;
    case GL_DOUBLE_MAT2x3:
        name = "GL_DOUBLE_MAT2x3";
        break;
    case GL_DOUBLE_MAT2x4:
        name = "GL_DOUBLE_MAT2x4";
        break;
    case GL_DOUBLE_MAT3x2:
        name = "GL_DOUBLE_MAT3x2";
        break;
    case GL_DOUBLE_MAT3x4:
        name = "GL_DOUBLE_MAT3x4";
        break;
    case GL_DOUBLE_MAT4x2:
        name = "GL_DOUBLE_MAT4x2";
        break;
    case GL_DOUBLE_MAT4x3:
        name = "GL_DOUBLE_MAT4x3";
        break;
    case GL_SAMPLER_1D:
        name = "GL_SAMPLER_1D";
        break;
    case GL_SAMPLER_2D:
        name = "GL_SAMPLER_2D";
        break;
    case GL_SAMPLER_3D:
        name = "GL_SAMPLER_3D";
        break;
    case GL_SAMPLER_CUBE:
        name = "GL_SAMPLER_CUBE";
        break;
    case GL_SAMPLER_1D_SHADOW:
        name = "GL_SAMPLER_1D_SHADOW";
        break;
    case GL_SAMPLER_2D_SHADOW:
        name = "GL_SAMPLER_2D_SHADOW";
        break;
    case GL_SAMPLER_1D_ARRAY:
        name = "GL_SAMPLER_1D_ARRAY";
        break;
    case GL_SAMPLER_2D_ARRAY:
        name = "GL_SAMPLER_2D_ARRAY";
        break;
    case GL_SAMPLER_1D_ARRAY_SHADOW:
        name = "GL_SAMPLER_1D_ARRAY_SHADOW";
        break;
    case GL_SAMPLER_2D_ARRAY_SHADOW:
        name = "GL_SAMPLER_2D_ARRAY_SHADOW";
        break;
    case GL_SAMPLER_2D_MULTISAMPLE:
        name = "GL_SAMPLER_2D_MULTISAMPLE";
        break;
    case GL_SAMPLER_2D_MULTISAMPLE_ARRAY:
        name = "GL_SAMPLER_2D_MULTISAMPLE_ARRAY";
        break;
    case GL_SAMPLER_CUBE_SHADOW:
        name = "GL_SAMPLER_CUBE_SHADOW";
        break;
    case GL_SAMPLER_BUFFER:
        name = "GL_SAMPLER_BUFFER";
        break;
    case GL_SAMPLER_2D_RECT:
        name = "GL_SAMPLER_2D_RECT";
        break;
    case GL_SAMPLER_2D_RECT_SHADOW:
        name = "GL_SAMPLER_2D_RECT_SHADOW";
        break;
    case GL_INT_SAMPLER_1D:
        name = "GL_INT_SAMPLER_1D";
        break;
    case GL_INT_SAMPLER_2D:
        name = "GL_INT_SAMPLER_2D";
        break;
    case GL_INT_SAMPLER_3D:
        name = "GL_INT_SAMPLER_3D";
        break;
    case GL_INT_SAMPLER_CUBE:
        name = "GL_INT_SAMPLER_CUBE";
        break;
    case GL_INT_SAMPLER_1D_ARRAY:
        name = "GL_INT_SAMPLER_1D_ARRAY";
        break;
    case GL_INT_SAMPLER_2D_ARRAY:
        name = "GL_INT_SAMPLER_2D_ARRAY";
        break;
    case GL_INT_SAMPLER_2D_MULTISAMPLE:
        name = "GL_INT_SAMPLER_2D_MULTISAMPLE";
        break;
    case GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
        name = "GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY";
        break;
    case GL_INT_SAMPLER_BUFFER:
        name = "GL_INT_SAMPLER_BUFFER";
        break;
    case GL_INT_SAMPLER_2D_RECT:
        name = "GL_INT_SAMPLER_2D_RECT";
        break;
    case GL_UNSIGNED_INT_SAMPLER_1D:
        name = "GL_UNSIGNED_INT_SAMPLER_1D";
        break;
    case GL_UNSIGNED_INT_SAMPLER_2D:
        name = "GL_UNSIGNED_INT_SAMPLER_2D";
        break;
    case GL_UNSIGNED_INT_SAMPLER_3D:
        name = "GL_UNSIGNED_INT_SAMPLER_3D";
        break;
    case GL_UNSIGNED_INT_SAMPLER_CUBE:
        name = "GL_UNSIGNED_INT_SAMPLER_CUBE";
        break;
    case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
        name = "GL_UNSIGNED_INT_SAMPLER_1D_ARRAY";
        break;
    case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
        name = "GL_UNSIGNED_INT_SAMPLER_2D_ARRAY";
        break;
    case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE:
        name = "GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE";
        break;
    case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
        name = "GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY";
        break;
    case GL_UNSIGNED_INT_SAMPLER_BUFFER:
        name = "GL_UNSIGNED_INT_SAMPLER_BUFFER";
        break;
    case GL_UNSIGNED_INT_SAMPLER_2D_RECT:
        name = "GL_UNSIGNED_INT_SAMPLER_2D_RECT";
        break;
    case GL_IMAGE_1D:
        name = "GL_IMAGE_1D";
        break;
    case GL_IMAGE_2D:
        name = "GL_IMAGE_2D";
        break;
    case GL_IMAGE_3D:
        name = "GL_IMAGE_3D";
        break;
    case GL_IMAGE_2D_RECT:
        name = "GL_IMAGE_2D_RECT";
        break;
    case GL_IMAGE_CUBE:
        name = "GL_IMAGE_CUBE";
        break;
    case GL_IMAGE_BUFFER:
        name = "GL_IMAGE_BUFFER";
        break;
    case GL_IMAGE_1D_ARRAY:
        name = "GL_IMAGE_1D_ARRAY";
        break;
    case GL_IMAGE_2D_ARRAY:
        name = "GL_IMAGE_2D_ARRAY";
        break;
    case GL_IMAGE_2D_MULTISAMPLE:
        name = "GL_IMAGE_2D_MULTISAMPLE";
        break;
    case GL_IMAGE_2D_MULTISAMPLE_ARRAY:
        name = "GL_IMAGE_2D_MULTISAMPLE_ARRAY";
        break;
    case GL_INT_IMAGE_1D:
        name = "GL_INT_IMAGE_1D";
        break;
    case GL_INT_IMAGE_2D:
        name = "GL_INT_IMAGE_2D";
        break;
    case GL_INT_IMAGE_3D:
        name = "GL_INT_IMAGE_3D";
        break;
    case GL_INT_IMAGE_2D_RECT:
        name = "GL_INT_IMAGE_2D_RECT";
        break;
    case GL_INT_IMAGE_CUBE:
        name = "GL_INT_IMAGE_CUBE";
        break;
    case GL_INT_IMAGE_BUFFER:
        name = "GL_INT_IMAGE_BUFFER";
        break;
    case GL_INT_IMAGE_1D_ARRAY:
        name = "GL_INT_IMAGE_1D_ARRAY";
        break;
    case GL_INT_IMAGE_2D_ARRAY:
        name = "GL_INT_IMAGE_2D_ARRAY";
        break;
    case GL_INT_IMAGE_2D_MULTISAMPLE:
        name = "GL_INT_IMAGE_2D_MULTISAMPLE";
        break;
    case GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
        name = "GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY";
        break;
    case GL_UNSIGNED_INT_IMAGE_1D:
        name = "GL_UNSIGNED_INT_IMAGE_1D";
        break;
    case GL_UNSIGNED_INT_IMAGE_2D:
        name = "GL_UNSIGNED_INT_IMAGE_2D";
        break;
    case GL_UNSIGNED_INT_IMAGE_3D:
        name = "GL_UNSIGNED_INT_IMAGE_3D";
        break;
    case GL_UNSIGNED_INT_IMAGE_2D_RECT:
        name = "GL_UNSIGNED_INT_IMAGE_2D_RECT";
        break;
    case GL_UNSIGNED_INT_IMAGE_CUBE:
        name = "GL_UNSIGNED_INT_IMAGE_CUBE";
        break;
    case GL_UNSIGNED_INT_IMAGE_BUFFER:
        name = "GL_UNSIGNED_INT_IMAGE_BUFFER";
        break;
    case GL_UNSIGNED_INT_IMAGE_1D_ARRAY:
        name = "GL_UNSIGNED_INT_IMAGE_1D_ARRAY";
        break;
    case GL_UNSIGNED_INT_IMAGE_2D_ARRAY:
        name = "GL_UNSIGNED_INT_IMAGE_2D_ARRAY";
        break;
    case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE:
        name = "GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE";
        break;
    case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY:
        name = "GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY";
        break;
    case GL_UNSIGNED_INT_ATOMIC_COUNTER:
        name = "GL_UNSIGNED_INT_ATOMIC_COUNTER";
        break;
    case GL_TEXTURE_1D:
        name = "GL_TEXTURE_1D";
        break;
    case GL_TEXTURE_2D:
        name = "GL_TEXTURE_2D";
        break;
    case GL_TEXTURE_3D:
        name = "GL_TEXTURE_3D";
        break;
    case GL_TEXTURE_1D_ARRAY:
        name = "GL_TEXTURE_1D_ARRAY";
        break;
    case GL_TEXTURE_2D_ARRAY:
        name = "GL_TEXTURE_2D_ARRAY";
        break;
    case GL_ALREADY_SIGNALED:
        name = "GL_ALREADY_SIGNALED";
        break;
    case GL_TIMEOUT_EXPIRED:
        name = "GL_TIMEOUT_EXPIRED";
        break;
    case GL_CONDITION_SATISFIED:
        name = "GL_CONDITION_SATISFIED";
        break;
    case GL_WAIT_FAILED:
        name = "GL_WAIT_FAILED";
        break;
    case GL_R16F:
        name = "GL_R16F";
        break;
    case GL_RG16F:
        name = "GL_RG16F";
        break;
    case GL_RGB16F:
        name = "GL_RGB16F";
        break;
    case GL_RGBA16F:
        name = "GL_RGBA16F";
        break;
    case GL_R32F:
        name = "GL_R32F";
        break;
    case GL_RG32F:
        name = "GL_RG32F";
        break;
    case GL_RGB32F:
        name = "GL_RGB32F";
        break;
    case GL_RGBA32F:
        name = "GL_RGBA32F";
        break;
    case GL_R8I:
        name = "GL_R8I";
        break;
    case GL_RG8I:
        name = "GL_RG8I";
        break;
    case GL_RGB8I:
        name = "GL_RGB8I";
        break;
    case GL_RGBA8I:
        name = "GL_RGBA8I";
        break;
    case GL_R16I:
        name = "GL_R16I";
        break;
    case GL_RG16I:
        name = "GL_RG16I";
        break;
    case GL_RGB16I:
        name = "GL_RGB16I";
        break;
    case GL_RGBA16I:
        name = "GL_RGBA16I";
        break;
    case GL_R32I:
        name = "GL_R32I";
        break;
    case GL_RG32I:
        name = "GL_RG32I";
        break;
    case GL_RGB32I:
        name = "GL_RGB32I";
        break;
    case GL_RGBA32I:
        name = "GL_RGBA32I";
        break;
    case GL_TEXTURE_CUBE_MAP:
        name = "GL_TEXTURE_CUBE_MAP";
        break;
    case GL_TEXTURE_CUBE_MAP_ARRAY:
        name = "GL_TEXTURE_CUBE_MAP_ARRAY";
        break;
    case GL_TEXTURE_2D_MULTISAMPLE:
        name = "GL_TEXTURE_2D_MULTISAMPLE";
        break;
    case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:
        name = "GL_TEXTURE_2D_MULTISAMPLE_ARRAY";
        break;
    case GL_TEXTURE_RECTANGLE:
        name = "GL_TEXTURE_RECTANGLE";
        break;
    case GL_TEXTURE_BUFFER:
        name = "GL_TEXTURE_BUFFER";
        break;
    case GL_SHADER_STORAGE_BUFFER:
        name = "GL_SHADER_STORAGE_BUFFER";
        break;
    case GL_STREAM_DRAW:
        name = "GL_STREAM_DRAW";
        break;
    case GL_STREAM_READ:
        name = "GL_STREAM_READ";
        break;
    case GL_STREAM_COPY:
        name = "GL_STREAM_COPY";
        break;
    case GL_STATIC_DRAW:
        name = "GL_STATIC_DRAW";
        break;
    case GL_STATIC_READ:
        name = "GL_STATIC_READ";
        break;
    case GL_STATIC_COPY:
        name = "GL_STATIC_COPY";
        break;
    case GL_DYNAMIC_DRAW:
        name = "GL_DYNAMIC_DRAW";
        break;
    case GL_DYNAMIC_READ:
        name = "GL_DYNAMIC_READ";
        break;
    case GL_DYNAMIC_COPY:
        name = "GL_DYNAMIC_COPY";
        break;
    case GL_UNSIGNED_INT64_ARB:
        name = "GL_UNSIGNED_INT64_ARB";
        break;
    case GL_INT64_ARB:
        name = "GL_INT64_ARB";
        break;
    case GL_VERTEX_SHADER:
        name = "GL_VERTEX_SHADER";
        break;
    case  GL_TESS_CONTROL_SHADER:
        name = "GL_TESS_CONTROL_SHADER";
        break;
    case  GL_TESS_EVALUATION_SHADER:
        name = "GL_TESS_EVALUATION_SHADER";
        break;
    case  GL_GEOMETRY_SHADER:
        name = "GL_GEOMETRY_SHADER";
        break;
    case   GL_FRAGMENT_SHADER:
        name = "GL_FRAGMENT_SHADER";
        break;
    default:
        break;
    }

    return name;
}

::GLenum internalFormatToPixelType(::GLenum internalFormat)
{
    ::GLenum pixelType = GL_FLOAT;
    switch(internalFormat)
    {
    case GL_R16F:
    case GL_R32F:
    case GL_RG16F:
    case GL_RG32F:
    case GL_RGB16F:
    case GL_RGB32F:
    case GL_RGBA16F:
    case GL_RGBA32F:
        pixelType = GL_FLOAT;
        break;
    case GL_R8_SNORM:
    case GL_R8:
    case GL_R16_SNORM:
    case GL_R8I:
    case GL_RGB8:
    case GL_RG8:
    case GL_RG8_SNORM:
    case GL_RG8I:
    case GL_RGB8I:
    case GL_RGB8_SNORM:
    case GL_RGBA8:
    case GL_RGBA8I:
    case GL_RGBA8_SNORM:
        pixelType = GL_BYTE;
        break;
    case GL_RGBA16:
    case GL_R16:
    case GL_R16I:
    case GL_RG16:
    case GL_RG16I:
    case GL_RG16_SNORM:
    case GL_RGB16:
    case GL_RGB16I:
    case GL_RGB16_SNORM:
    case GL_RGBA16I:
    case GL_RGBA16_SNORM:
        pixelType = GL_SHORT;
        break;
    case GL_R32I:
    case GL_RG32I:
    case GL_RGB32I:
    case GL_RGBA32I:
        pixelType = GL_INT;
        break;
    case GL_R8UI:
    case GL_RG8UI:
    case GL_RGB8UI:
    case GL_RGBA8UI:
        pixelType = GL_UNSIGNED_BYTE;
        break;
    case GL_R16UI:
    case GL_RG16UI:
    case GL_RGB16UI:
    case GL_RGBA16UI:
        pixelType = GL_UNSIGNED_SHORT;
        break;
    case GL_R32UI:
    case GL_RG32UI:
    case GL_RGB32UI:
    case GL_RGBA32UI:
        pixelType = GL_UNSIGNED_INT;
        break;
    case GL_RGB4:
    case GL_RGBA4:
        pixelType = GL_UNSIGNED_SHORT_4_4_4_4;
        break;
    case GL_RGB5:
        pixelType = GL_UNSIGNED_SHORT_5_5_5_1;
        break;
    case GL_RGB10:
        pixelType = GL_UNSIGNED_INT_10_10_10_2;
        break;
    case GL_RGB12:
    case GL_RGBA2:
    case GL_RGBA12:
    default:
        qDebug() << Q_FUNC_INFO << "unhandled internal format" << QuickOpenGL::GLenum(internalFormat);
        break;
    }

    return pixelType;

}

::GLenum baseInternalFormat(::GLenum sizedInternalFormat)
{
    ::GLenum pixelFormat = GL_RGBA;
    switch(sizedInternalFormat)
    {
    case GL_R8I:
    case GL_R8UI:
    case GL_R16I:
    case GL_R16UI:
    case GL_R32I:
    case GL_R32UI:
        pixelFormat = GL_RED_INTEGER;
        break;
    case GL_R8:
    case GL_R8_SNORM:
    case GL_R16:
    case GL_R16_SNORM:
    case GL_R16F:
    case GL_R32F:
        pixelFormat = GL_RED;
        break;
    case GL_RG8I:
    case GL_RG8UI:
    case GL_RG16I:
    case GL_RG16UI:
    case GL_RG32I:
    case GL_RG32UI:
        pixelFormat = GL_RG_INTEGER;
        break;
    case GL_RG8:
    case GL_RG8_SNORM:
    case GL_RG16_SNORM:
    case GL_RG16:
    case GL_RG16F:
    case GL_RG32F:
        pixelFormat = GL_RG;
        break;
    case GL_RGB8I:
    case GL_RGB8UI:
    case GL_RGB16I:
    case GL_RGB16UI:
    case GL_RGB32I:
    case GL_RGB32UI:
        pixelFormat = GL_RGB_INTEGER;
        break;
    case GL_RGB4:
    case GL_RGB5:
    case GL_RGB8:
    case GL_RGB8_SNORM:
    case GL_RGB10:
    case GL_RGB12:
    case GL_RGB16:
    case GL_RGB16_SNORM:
    case GL_RGB16F:
    case GL_RGB32F:
        pixelFormat = GL_RGB;
        break;
    case GL_RGBA8I:
    case GL_RGBA8UI:
    case GL_RGBA16I:
    case GL_RGBA16UI:
    case GL_RGBA32I:
    case GL_RGBA32UI:
        pixelFormat = GL_RGBA_INTEGER;
        break;
    case GL_RGBA2:
    case GL_RGBA4:
    case GL_RGBA8:
    case GL_RGBA8_SNORM:
    case GL_RGBA16:
    case GL_RGBA16_SNORM:
    case GL_RGBA12:
    case GL_RGBA16F:
    case GL_RGBA32F:
        pixelFormat = GL_RGBA;
        break;
    default:
        break;
    }

    return pixelFormat;

}

int internalFormatToComponents(::GLenum internalFormat)
{
    int components = 4;
    switch(internalFormat)
    {
    case GL_R8I:
    case GL_R8UI:
    case GL_R16I:
    case GL_R16UI:
    case GL_R32I:
    case GL_R32UI:
    case GL_R8:
    case GL_R8_SNORM:
    case GL_R16:
    case GL_R16_SNORM:
    case GL_R16F:
    case GL_R32F:
        components = 1;
        break;
    case GL_RG8I:
    case GL_RG8UI:
    case GL_RG16I:
    case GL_RG16UI:
    case GL_RG32I:
    case GL_RG32UI:
    case GL_RG8:
    case GL_RG8_SNORM:
    case GL_RG16_SNORM:
    case GL_RG16:
    case GL_RG16F:
    case GL_RG32F:
        components = 2;
        break;
    case GL_RGB8I:
    case GL_RGB8UI:
    case GL_RGB16I:
    case GL_RGB16UI:
    case GL_RGB32I:
    case GL_RGB32UI:
    case GL_RGB4:
    case GL_RGB5:
    case GL_RGB8:
    case GL_RGB8_SNORM:
    case GL_RGB10:
    case GL_RGB12:
    case GL_RGB16:
    case GL_RGB16_SNORM:
    case GL_RGB16F:
    case GL_RGB32F:
        components = 3;
        break;
    case GL_RGBA8I:
    case GL_RGBA8UI:
    case GL_RGBA16I:
    case GL_RGBA16UI:
    case GL_RGBA32I:
    case GL_RGBA32UI:
    case GL_RGBA2:
    case GL_RGBA4:
    case GL_RGBA8:
    case GL_RGBA8_SNORM:
    case GL_RGBA16:
    case GL_RGBA16_SNORM:
    case GL_RGBA12:
    case GL_RGBA16F:
    case GL_RGBA32F:
        components = 4;
        break;
    default:
        break;
    }

    return components;

}

int internalFormatSize(::GLenum internalFormat)
{
    int size = 4;
    switch(internalFormat)
    {
    case GL_R8I:
    case GL_R8UI:
    case GL_R8:
    case GL_R8_SNORM:
    case GL_RGBA2:
        size = 1;
        break;

    case GL_RG8I:
    case GL_RG8UI:
    case GL_RG8:
    case GL_RG8_SNORM:
    case GL_R16I:
    case GL_R16UI:
    case GL_R16:
    case GL_R16_SNORM:
    case GL_R16F:
    case GL_RGB4:
    case GL_RGB5:
    case GL_RGBA4:
        size = 2;
        break;

    case GL_RGB8I:
    case GL_RGB8UI:
    case GL_RGB8:
    case GL_RGB8_SNORM:
        size = 3;
        break;

    case GL_R32F:
    case GL_R32I:
    case GL_R32UI:
    case GL_RG16I:
    case GL_RG16UI:
    case GL_RG16_SNORM:
    case GL_RG16:
    case GL_RG16F:
    case GL_RGBA8I:
    case GL_RGBA8UI:
    case GL_RGB10:
    case GL_RGBA8:
    case GL_RGBA8_SNORM:
        size = 4;
        break;

    case GL_RGB12:
        size = 5;
        break;

    case GL_RGB16I:
    case GL_RGB16UI:
    case GL_RGB16:
    case GL_RGB16_SNORM:
    case GL_RGB16F:
    case GL_RGBA12:
        size = 6;
        break;

    case GL_RG32I:
    case GL_RG32UI:
    case GL_RG32F:
    case GL_RGBA16I:
    case GL_RGBA16UI:
    case GL_RGBA16:
    case GL_RGBA16_SNORM:
    case GL_RGBA16F:
        size = 8;
        break;

    case GL_RGB32I:
    case GL_RGB32UI:
    case GL_RGB32F:
        size = 12;
        break;
    case GL_RGBA32I:
    case GL_RGBA32UI:
    case GL_RGBA32F:
        size = 16;
        break;
    default:
        qDebug() << Q_FUNC_INFO << "unhandled internal format" << QuickOpenGL::GLenum(internalFormat);
        break;
    }

    return size;
}


bool isSampler(::GLenum type)
{
    bool result = false;

    switch(type)
    {
    case GL_SAMPLER_1D:
    case GL_INT_SAMPLER_1D:
    case GL_UNSIGNED_INT_SAMPLER_1D:
    case GL_SAMPLER_2D:
    case GL_INT_SAMPLER_2D:
    case GL_UNSIGNED_INT_SAMPLER_2D:
    case GL_SAMPLER_3D:
    case GL_INT_SAMPLER_3D:
    case GL_UNSIGNED_INT_SAMPLER_3D:
    case GL_SAMPLER_CUBE:
    case GL_INT_SAMPLER_CUBE:
    case GL_UNSIGNED_INT_SAMPLER_CUBE:
    case GL_SAMPLER_1D_ARRAY:
    case GL_INT_SAMPLER_1D_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
    case GL_SAMPLER_2D_ARRAY:
    case GL_INT_SAMPLER_2D_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
    case GL_SAMPLER_BUFFER:
    case GL_INT_SAMPLER_BUFFER:
    case GL_UNSIGNED_INT_SAMPLER_BUFFER:
        result = true;
        break;
    default:
        break;
    }

    return result;
}

int sizeOfType(::GLenum type)
{
    return tupleCount(type)*tupleSize(type)*primitiveSize(primitiveType(type));
}

::GLenum samplerToTarget(::GLenum sampler)
{
    ::GLenum result = GL_ZERO;
    switch (sampler)
    {
    case GL_SAMPLER_1D:
    case GL_INT_SAMPLER_1D:
    case GL_UNSIGNED_INT_SAMPLER_1D:
        result = GL_TEXTURE_1D;
        break;
    case GL_SAMPLER_2D:
    case GL_INT_SAMPLER_2D:
    case GL_UNSIGNED_INT_SAMPLER_2D:
        result = GL_TEXTURE_2D;
        break;
    case GL_SAMPLER_3D:
    case GL_INT_SAMPLER_3D:
    case GL_UNSIGNED_INT_SAMPLER_3D:
        result = GL_TEXTURE_3D;
        break;
    case GL_SAMPLER_CUBE:
    case GL_INT_SAMPLER_CUBE:
    case GL_UNSIGNED_INT_SAMPLER_CUBE:
        result = GL_TEXTURE_CUBE_MAP;
        break;
    case GL_SAMPLER_1D_ARRAY:
    case GL_INT_SAMPLER_1D_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
        result = GL_TEXTURE_1D_ARRAY;
        break;
    case GL_SAMPLER_2D_ARRAY:
    case GL_INT_SAMPLER_2D_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
        result = GL_TEXTURE_2D_ARRAY;
        break;
    case GL_SAMPLER_BUFFER:
    case GL_INT_SAMPLER_BUFFER:
    case GL_UNSIGNED_INT_SAMPLER_BUFFER:
        result = GL_TEXTURE_BUFFER;
        break;
    default:
        qWarning() << "QuickOpenGL: unsupported sampler type" << Qt::hex << sampler;
        break;
    }

    return result;
}
}



#if defined (QT_DEBUG)

QDebug operator << (QDebug debug, QuickOpenGL::GLenum value)
{

    QString name = QuickOpenGL::toString(value);

    debug << qPrintable(name.isEmpty()?QString("0x%1").arg(int(value), 0, 16):name);

    return debug;
}

#endif

extern int g_debugLevel;
int g_debugLevel = QProcessEnvironment::systemEnvironment().value("DEBUG_QUICKOPENGL", "0").toInt() | QProcessEnvironment::systemEnvironment().value("QUICKOPENGL_DEBUG", "0").toInt();

int QuickOpenGL::debugLevel()
{
    return g_debugLevel;
}

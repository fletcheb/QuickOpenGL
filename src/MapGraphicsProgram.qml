import QtQuick 2.10
import QuickOpenGL 1.0
import QtLocation 5.2
import QtPositioning 5.2

/*!
    \qmltype MapGraphicsProgram
    \inherits GraphicsProgram
    \inqmlmodule QuickOpenGL
    \ingroup QuickOpenGL QML Types
    \brief The MapGraphicsProgram QML type represents an OpenGL graphics program in a QML Map type.

    MapGraphicsProgram is a QuickOpenGL GraphicsProgram integrated with the QML \l{QtLocation}
    \l{Map} type. When instantiated with a \l{Map} in its parent tree, MapGraphicsProgram generates
    a transformation matrix which when applied to vertices specified in Web Mercator coordinates,
    places the vertices at the appropriate location on the rendered map.

    MapGraphicsProgram may be thought of as a more flexible and customisable style of QtLocation
    Map Item object (e.g. \l{MapCircle}, \l{MapRectangle}, \l{MapPolyline}, \l{MapPolygon}, \l{MapRoute},
    \l{MapQuickItem}). It does not however identify as a QtLocation Map Item, and therefore cannot be
    used with functions such as \l{Map::addMapItem} and \l{Map::fitViewportToMapItems}.

    \section1 Transformation Matrix

    The map transformation matrix is stored as a double precision floating point
    \l{QuickOpenGL::dmat4} property named \l{qt_MapMatrix}. Double precision is required if the map
    is to be scaled to high zoom levels.  It is used to transform web mercator coordinates to
    screen coordinates.

    \section2 Coordinate Conversion

    Coordinates may be converted from \l{QtPositioning::coordinate} values to
    \l{https://en.wikipedia.org/wiki/Web_Mercator}{Web Mercator} x, y values via the
    \l{MapGraphicsProgram::geoToMapProjection} function. Alternately, projection to Web Mercator
    may be performed in GLSL via a function such as:

    \qml
        // v.x = longitude (deg), v.y = latitude (deg)
        vec2 wgs84toWebMercator(vec2 v)
        {
            return vec2(v.x/360.0+0.5, 0.5-log(tan(M_PI/4.0 + M_PI/2.0*v.y/180.0))/(2.0*M_PI));
        }
    \endqml

    \note OpenGL does not support double precision transcendental functions which may be problematic
    for applications requiring precise positioning.

    It should be noted that single precision floating point values do not provide sufficient
    resolution to accurately convey position on the earth's surface for many applications. The Qt
    Quick scene graph does not currently allow double precision vertex attributes to be passed
    correctly. It is possible to provide sufficient precision by scaling values into 32bit integer
    based attributes.


    \section1 Default Vertices and Vertex Shader
    The default vertices are a rectangle in Web Mercator coordinates over central London.

    \qml
        vertices: [{qt_Vertex: Qt.vector4d(geoToMapProjection(QtPositioning.coordinate(52, -0.5)).x, geoToMapProjection(QtPositioning.coordinate(52, -0.5)).y, 0, 1), qt_MultiTexCoord0: Qt.vector2d(0, 0)},
                   {qt_Vertex: Qt.vector4d(geoToMapProjection(QtPositioning.coordinate(51, -0.5)).x, geoToMapProjection(QtPositioning.coordinate(51, -0.5)).y, 0, 1), qt_MultiTexCoord0: Qt.vector2d(0, 1)},
                   {qt_Vertex: Qt.vector4d(geoToMapProjection(QtPositioning.coordinate(52, 0.5)).x, geoToMapProjection(QtPositioning.coordinate(52, 0.5)).y, 0, 1), qt_MultiTexCoord0: Qt.vector2d(1, 0)},
                   {qt_Vertex: Qt.vector4d(geoToMapProjection(QtPositioning.coordinate(51, 0.5)).x, geoToMapProjection(QtPositioning.coordinate(51, 0.5)).y, 0, 1), qt_MultiTexCoord0: Qt.vector2d(1, 1)}]
    \endqml

    The default vertex shader demonstrates how to apply the transformation to the Web Mercator vertices.

    \qml
     #version 400 core

     uniform mat4 qt_Matrix;
     uniform dmat4 qt_MapMatrix;
     in vec4 qt_Vertex;
     in vec2 qt_MultiTexCoord0;

     out vec2 coord;

     void main()
     {
         coord = qt_MultiTexCoord0;
         gl_Position = vec4(dmat4(qt_Matrix) * qt_MapMatrix * dvec4(qt_Vertex));
     };
    \endqml


 */

GraphicsProgram
{
    id: root
    anchors.fill: parent

    property alias sharedData: d.sharedData
    property var map: findMap()

    onMapChanged:
    {
        if(map && (map instanceof Map))
        {
            for(var i in map.children)
            {
                if('__MapGraphicsProgram_SharedData__' in map.children[i])
                {
                    d.sharedData = map.children[i]
                    break
                }
            }

            if(!d.sharedData)
            {
                d.sharedData = sharedDataComponent.createObject(map)
            }
        }
    }

    function findMap()
    {
        var object = parent
        while(object!=null)
        {
            if(object instanceof Map)
            {
                return object
            }

           object = object.parent
        }
        return null
    }

    /*!
        dvec2 geoToMapProjection(coord)

        Projects geospatial \l{QtPositioning::coordinate} coord to Web Mercator coordinates.
     */
    function geoToMapProjection(coord)
    {
        if(coord.constructor===Array)
        {
            var projected = []

            for(var i=0;i<coord.length;++i)
            {
                var lon = coord[i].longitude / 360.0 + 0.5
                var lat = 0.5 - (Math.log(Math.tan((Math.PI/4.0) + (Math.PI/2.0)*coord[i].latitude/180.0))/Math.PI)/2.0
                projected.push(GL.dvec2(lon, lat<0.0?0.0:lat>1.0?1.0:lat));
            }

            return projected

        }

        var latitude = 0.5 - (Math.log(Math.tan((Math.PI/4.0) + (Math.PI/2.0)*coord.latitude/180.0))/Math.PI)/2.0
        return GL.dvec2(coord.longitude / 360.0 + 0.5, latitude<0.0?0.0:latitude>1.0?1.0:latitude);

    }

    Component
    {
        id: sharedDataComponent

        //
        // Perform same transformation as Map
        //
        // Calculations need to be performed in double precision
        //
        Item
        {
            property var __MapGraphicsProgramSharedData__
            property Map map: parent
            property var center: GL.dvec3(geoToMapProjection(map.center), 0.0).times(sideLength)
            property real defaultTileSize: 256
            property real sideLength: Math.pow(2, Math.floor(map.zoomLevel))*defaultTileSize
            property real aperture: Math.tan(map.fieldOfView*Math.PI/180.0*0.5)
            property real altitude: map.height/(2.0*Math.pow(2.0, map.zoomLevel-Math.floor(map.zoomLevel))*defaultTileSize)
            property var eye0: GL.dvec3(center.xy, altitude*defaultTileSize/aperture);
            property var view0: eye0.minus(center)
            property var bearingRotation: GL.dmat4().rotate(map.bearing, view0)
            property var side0: view0.crossProduct(GL.dvec3(0,1,0)).normalized()
            property var up0: bearingRotation.times(GL.dvec4(side0.crossProduct(view0).normalized(),1)).xyz
            property var side1: up0.crossProduct(view0).normalized()
            property var tiltRotation: GL.dmat4().rotate(-map.tilt, side1)
            property var eye1: tiltRotation.times(GL.dvec4(view0,1)).xyz.plus(center)
            property var view1: eye1.minus(center)
            property var up1: view1.crossProduct(side1).normalized()
            property real nearPlane: 1.0
            property real farPlane: (altitude + 2097152.0)*defaultTileSize
            property real aspectRatio: map.width/map.height
            property var modelViewProjection: GL.dmat4().scale(GL.dvec3(0.5*map.width, 0.5*map.height, 1.0)).translate(GL.dvec3(1.0,1.0,0.0)).frustum(-aperture*aspectRatio, aperture*aspectRatio, -aperture, aperture, nearPlane, farPlane).lookAt(eye1,center,up1).scale(GL.dvec3(sideLength, sideLength, 1.0))
        }
    }

    QtObject
    {
        id: d
        property var sharedData: null
    }


    // find the map object
    Component.onCompleted:
    {
//        var object = parent

//        while(object && !(object instanceof Map))
//        {
//            object = object.parent
//        }

//        if(!object)
//        {
//            console.log(this, 'ERROR: Could not find Map in parent tree!')
//        }
//        else
//        {
//            var map = object

//            for(object in map.children)
//            {
//                if('__MapGraphicsProgram_SharedData__' in map.children[object])
//                {
//                    d.sharedData = map.children[object]
//                    break
//                }
//            }

//            if(!d.sharedData)
//            {
//                d.sharedData = sharedDataComponent.createObject(map)
//            }
//        }
    }


    vertices: [{qt_Vertex: Qt.vector4d(geoToMapProjection(QtPositioning.coordinate(52, -0.5)).x, geoToMapProjection(QtPositioning.coordinate(52, -0.5)).y, 0, 1), qt_MultiTexCoord0: Qt.vector2d(0, 0)},
               {qt_Vertex: Qt.vector4d(geoToMapProjection(QtPositioning.coordinate(51, -0.5)).x, geoToMapProjection(QtPositioning.coordinate(51, -0.5)).y, 0, 1), qt_MultiTexCoord0: Qt.vector2d(0, 1)},
               {qt_Vertex: Qt.vector4d(geoToMapProjection(QtPositioning.coordinate(52, 0.5)).x, geoToMapProjection(QtPositioning.coordinate(52, 0.5)).y, 0, 1), qt_MultiTexCoord0: Qt.vector2d(1, 0)},
               {qt_Vertex: Qt.vector4d(geoToMapProjection(QtPositioning.coordinate(51, 0.5)).x, geoToMapProjection(QtPositioning.coordinate(51, 0.5)).y, 0, 1), qt_MultiTexCoord0: Qt.vector2d(1, 1)}]

    /*!
        \qmlproperty dmat4 qt_MapMatrix

        The transformation matrix required to map from Web Mercator to screen space.

        This matrix is double precision floating point.  It should be applied as follows:
        \code
        gl_Position = vec4(dmat4(qt_Matrix) * qt_MapMatrix * dvec4(qt_Vertex));        
        \endcode

     */
    property var qt_MapMatrix: d.sharedData.modelViewProjection


    vertexShader: '
         #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'400') + '
         #ifdef GL_ES
         precision highp float;
         uniform mat4 qt_MapMatrix;
         #else
         uniform dmat4 qt_MapMatrix;
         #endif

         uniform mat4 qt_Matrix;
         in vec4 qt_Vertex;
         in vec2 qt_MultiTexCoord0;

         out vec2 coord;

         void main()
         {
             coord = qt_MultiTexCoord0;
         #ifdef GL_ES
             gl_Position = vec4(qt_Matrix * qt_MapMatrix * qt_Vertex);
         #else
             gl_Position = vec4(dmat4(qt_Matrix) * qt_MapMatrix * dvec4(qt_Vertex));
         #endif
         }'

    fragmentShader: '
         #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'400') + '
         #ifdef GL_ES
         precision highp float;
         #endif

         in vec2 coord;
         uniform float qt_Opacity;
         out vec4 fragColor;
         void main()
         {
             fragColor = vec4(coord, 0.0, qt_Opacity);
         }'

}

/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgimagehandleprovider.h"
#include <QOpenGLFunctions_3_3_Core>
#include "openglextension_arb_bindless_texture.h"
#include "support.h"

class QuickSGImageHandleProviderPrivate
{
public:

    QuickSGImageHandleProviderPrivate();

    QOpenGLExtension_ARB_bindless_texture *glArbBindless;
    QSGTextureProvider *sourceProvider;
    QuickImageHandle::Access access;
    int level;
    bool layered;
    int layer;
    QuickImageHandle::Format format;

};

QuickSGImageHandleProviderPrivate::QuickSGImageHandleProviderPrivate() :
    glArbBindless(nullptr),
    sourceProvider(nullptr),
    access(QuickImageHandle::AccessReadWrite),
    level(0),
    layered(false),
    layer(0),
    format(QuickImageHandle::FormatRGBA32F)
{
}

QuickSGImageHandleProvider::QuickSGImageHandleProvider() :
    d_ptr(new QuickSGImageHandleProviderPrivate)
{
    Q_D(QuickSGImageHandleProvider);

    d->glArbBindless = new QOpenGLExtension_ARB_bindless_texture;
    Q_CHECK_PTR(d->glArbBindless);

    if(d->glArbBindless)
    {
        bool ok = d->glArbBindless->initializeOpenGLFunctions();
        Q_ASSERT(ok);
    }
    else
    {
        qWarning() << "QuickOpenGL: OpenGL ARB Bindless texture extension not supported";
    }
}

QuickSGImageHandleProvider::~QuickSGImageHandleProvider()
{
    Q_D(QuickSGImageHandleProvider);

    if(d->glArbBindless)
    {
        makeNonResident();
        delete d->glArbBindless;
    }
}

quint64 QuickSGImageHandleProvider::handle() const
{
    Q_D(const QuickSGImageHandleProvider);

    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();

    quint64 handle = 0;

    if(sourceProvider()&&sourceProvider()->texture()&&gl->glIsTexture(sourceProvider()->texture()->textureId()))
    {
        handle = d->glArbBindless->glGetImageHandleARB(sourceProvider()->texture()->textureId(), d->level, d->layered, d->layer, d->format);debugGL;
    }

    return handle;
}

void QuickSGImageHandleProvider::makeResident() const
{
    Q_D(const QuickSGImageHandleProvider);

    Q_ASSERT(QOpenGLContext::currentContext());

    quint64 handle = this->handle();

    if(d->glArbBindless&&handle&&!d->glArbBindless->glIsImageHandleResidentARB(handle))
    {
        d->glArbBindless->glMakeImageHandleResidentARB(handle, d->access);debugGL;
    }
}

void QuickSGImageHandleProvider::makeNonResident() const
{
    Q_D(const QuickSGImageHandleProvider);

    Q_ASSERT(QOpenGLContext::currentContext());
    quint64 handle = this->handle();

    if(d->glArbBindless&&handle&&d->glArbBindless->glIsImageHandleResidentARB(handle))
    {
        d->glArbBindless->glMakeImageHandleNonResidentARB(handle);debugGL;
    }
}

void QuickSGImageHandleProvider::setSourceProvider(QSGTextureProvider *sourceProvider)
{

    Q_D(QuickSGImageHandleProvider);


    // if the source provider is not changing or our hardware doesnt support ARB Bindless extension, bail out
    if((sourceProvider==d->sourceProvider)||(!d->glArbBindless))
    {
        return;
    }

    // if we already have a source provider, disconnect connected signals
    if(d->sourceProvider)
    {
        disconnect(d->sourceProvider, &QSGTextureProvider::textureChanged, this, &QuickSGImageHandleProvider::imageChanged);
        disconnect(d->sourceProvider, &QObject::destroyed, this, &QuickSGImageHandleProvider::onSourceProviderDestroyed);
    }

    // take a copy of the source provider
    d->sourceProvider = sourceProvider;

    // the texture removed from any texture atlas
    QSGTexture *texture = sourceProvider->texture()->isAtlasTexture()?sourceProvider->texture()->removedFromAtlas():sourceProvider->texture();

    // some QSGTextures are not created until bind is called, so call it, then unbind it
    QOpenGLFunctions *gl = QOpenGLContext::currentContext()->functions();
    Q_ASSERT(gl);
    gl->initializeOpenGLFunctions();

    texture->bind();debugGL;
    GLuint target = sourceProvider->texture()->property("target").isValid()?sourceProvider->texture()->property("target").toUInt():GL_TEXTURE_2D;
    gl->glBindTexture(target, 0);debugGL;

    // connect the source provider textureChanged signal to our textureChanged signal
    connect(d->sourceProvider, &QSGTextureProvider::textureChanged, this, &QuickSGImageHandleProvider::imageChanged);
    connect(d->sourceProvider, &QObject::destroyed, this, &QuickSGImageHandleProvider::onSourceProviderDestroyed);

    emit imageChanged();

}

QSGTextureProvider *QuickSGImageHandleProvider::sourceProvider() const
{
    Q_D(const QuickSGImageHandleProvider);
    return d->sourceProvider;
}


void QuickSGImageHandleProvider::setAccess(QuickImageHandle::Access access)
{
    Q_D(QuickSGImageHandleProvider);

    if(d->access==access)
        return;

    d->access = access;

    emit imageChanged();
}

QuickImageHandle::Access QuickSGImageHandleProvider::access() const
{
    Q_D(const QuickSGImageHandleProvider);
    return d->access;
}

void QuickSGImageHandleProvider::setLevel(int level)
{
    Q_D(QuickSGImageHandleProvider);

    if(d->level==level)
        return;

    d->level = level;

    emit imageChanged();
}

void QuickSGImageHandleProvider::setLayered(bool layered)
{
    Q_D(QuickSGImageHandleProvider);
    if(d->layered==layered)
        return;

    d->layered = layered;

    emit imageChanged();

}

void QuickSGImageHandleProvider::setLayer(int layer)
{
    Q_D(QuickSGImageHandleProvider);
    if(d->layer==layer)
        return;

    d->layer = layer;

    emit imageChanged();

}

void QuickSGImageHandleProvider::setFormat(QuickImageHandle::Format format)
{
    Q_D(QuickSGImageHandleProvider);

    if(d->format==format)
        return;

    d->format = format;

    emit imageChanged();

}

void QuickSGImageHandleProvider::onSourceProviderDestroyed()
{
    Q_D(QuickSGImageHandleProvider);
    d->sourceProvider = nullptr;
}

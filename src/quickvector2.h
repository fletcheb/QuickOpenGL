/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKVECTOR2_H
#define QUICKVECTOR2_H

#include <QObject>
#include <QGenericMatrix>
#include <QVariant>
#include <QDebug>
#include <QGeoCoordinate>
#include <QVector2D>
#include <QPointF>
#include <QPoint>
#include <QSizeF>
#include <QSize>
#include "quickopengl_global.h"

class qdvec2;
class qvec2;

template <typename Type> class vector2 : public QGenericMatrix<1,2,Type>
{
public:

    vector2(const QGenericMatrix<1,2,Type> &other)
    {
        memcpy(reinterpret_cast<void *>(this), &other, sizeof(vector2<Type>));
    }

    vector2(Type x = Type(0), Type y = Type(0))
    {
        setX(x);
        setY(y);
    }

    vector2(const QGeoCoordinate &coordinate)
    {
        setX(Type(coordinate.longitude()));
        setY(Type(coordinate.latitude()));
    }

    vector2(const QVector2D &vector2d)
    {
        setX(Type(vector2d.x()));
        setY(Type(vector2d.y()));
    }

    vector2(const QPointF &pointF)
    {
        setX(Type(pointF.x()));
        setY(Type(pointF.y()));
    }

    vector2(const QPoint &point)
    {
        setX(Type(point.x()));
        setY(Type(point.y()));
    }

    vector2(const QSize &size)
    {
        setX(Type(size.width()));
        setY(Type(size.height()));
    }

    vector2(const QSizeF &sizeF)
    {
        setX(Type(sizeF.width()));
        setY(Type(sizeF.height()));
    }

    operator QGeoCoordinate() const
    {
        return QGeoCoordinate(double(y()), double(x()), 0.0);
    }

    operator QVector2D() const
    {
        return QVector2D(float(x()), float(y()));
    }

    operator QPointF() const
    {
        return QPointF(qreal(x()), qreal(y()));
    }

    operator QPoint() const
    {
        return QPoint(int(x()), int(y()));
    }

    operator QSizeF() const
    {
        return QSizeF(qreal(x()), qreal(y()));
    }

    operator QSize() const
    {
        return QSize(int(x()), int(y()));
    }

    vector2<Type> xx() const {return vector2<Type>(x(),x());}
    vector2<Type> xy() const {return vector2<Type>(x(),y());}
    vector2<Type> yx() const {return vector2<Type>(y(),x());}
    vector2<Type> yy() const {return vector2<Type>(y(),y());}

    Type x() const
    {
        return this->operator()(0,0);
    }

    Type y() const
    {
        return this->operator()(1,0);
    }

    void setX(Type x)
    {
        this->operator()(0,0) = x;
    }

    void setY(Type y)
    {
        this->operator()(1,0) = y;
    }


};


class QUICKOPENGL_EXPORT qvec2 : public vector2<float>
{
    Q_GADGET

    Q_PROPERTY(float x READ x WRITE setX)
    Q_PROPERTY(float y READ y WRITE setY)

    Q_PROPERTY(qvec2 xx READ xx STORED false)
    Q_PROPERTY(qvec2 xy READ xy STORED false)
    Q_PROPERTY(qvec2 yx READ yx STORED false)
    Q_PROPERTY(qvec2 yy READ yy STORED false)

public:

    using vector2<float>::vector2;
    using QGenericMatrix<1,2,float>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

    operator qdvec2() const;
    Q_INVOKABLE float dotProduct(const qvec2 &other);
    Q_INVOKABLE qvec2 times(const qvec2 &other);
    Q_INVOKABLE qvec2 times(float factor);
    Q_INVOKABLE qvec2 plus(const qvec2 &other);
    Q_INVOKABLE qvec2 minus(const qvec2 &other);
    Q_INVOKABLE qvec2 divide(float factor);
    Q_INVOKABLE qvec2 divide(const qvec2 &other);

    Q_INVOKABLE qvec2 normalized();
    Q_INVOKABLE float length();

};

class QUICKOPENGL_EXPORT qivec2 : public vector2<int>
{
    Q_GADGET
    Q_PROPERTY(int x READ x WRITE setX)
    Q_PROPERTY(int y READ y WRITE setY)

    Q_PROPERTY(qivec2 xx READ xx STORED false)
    Q_PROPERTY(qivec2 xy READ xy STORED false)
    Q_PROPERTY(qivec2 yx READ yx STORED false)
    Q_PROPERTY(qivec2 yy READ yy STORED false)

public:

    using vector2<int>::vector2;
    using QGenericMatrix<1,2,int>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class QUICKOPENGL_EXPORT quvec2 : public vector2<unsigned int>
{
    Q_GADGET
    Q_PROPERTY(unsigned int x READ x WRITE setX)
    Q_PROPERTY(unsigned int y READ y WRITE setY)

    Q_PROPERTY(quvec2 xx READ xx STORED false)
    Q_PROPERTY(quvec2 xy READ xy STORED false)
    Q_PROPERTY(quvec2 yx READ yx STORED false)
    Q_PROPERTY(quvec2 yy READ yy STORED false)

public:

    using vector2<unsigned int>::vector2;
    using QGenericMatrix<1,2,unsigned int>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};

class QUICKOPENGL_EXPORT qdvec2 : public vector2<double>
{
    Q_GADGET
    Q_PROPERTY(double x READ x WRITE setX)
    Q_PROPERTY(double y READ y WRITE setY)

    Q_PROPERTY(qdvec2 xx READ xx STORED false)
    Q_PROPERTY(qdvec2 xy READ xy STORED false)
    Q_PROPERTY(qdvec2 yx READ yx STORED false)
    Q_PROPERTY(qdvec2 yy READ yy STORED false)

public:

    Q_INVOKABLE double dotProduct(const qdvec2 &other);
    Q_INVOKABLE qdvec2 times(const qdvec2 &other);
    Q_INVOKABLE qdvec2 times(double factor);
    Q_INVOKABLE qdvec2 plus(const qdvec2 &other);
    Q_INVOKABLE qdvec2 minus(const qdvec2 &other);
    Q_INVOKABLE qdvec2 normalized();
    Q_INVOKABLE double length();

    using vector2<double>::vector2;
    using QGenericMatrix<1,2,double>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }

    operator qvec2() const;


};

class QUICKOPENGL_EXPORT qbvec2 : public vector2<bool>
{
    Q_GADGET
    Q_PROPERTY(bool x READ x WRITE setX)
    Q_PROPERTY(bool y READ y WRITE setY)

    Q_PROPERTY(qbvec2 xx READ xx STORED false)
    Q_PROPERTY(qbvec2 xy READ xy STORED false)
    Q_PROPERTY(qbvec2 yx READ yx STORED false)
    Q_PROPERTY(qbvec2 yy READ yy STORED false)

public:

    using vector2<bool>::vector2;
    using QGenericMatrix<1,2,bool>::operator==;
    operator QVariant() const
    {
        return QVariant::fromValue(*this);
    }
};


inline QDebug operator << (QDebug debug, const qvec2 &value)
{
    debug.nospace() << "vec2(" << value.x() << ", " << value.y() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qivec2 &value)
{
    debug.nospace() << "ivec2(" << value.x() << ", " << value.y() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const quvec2 &value)
{
    debug.nospace() << "uvec2(" << value.x() << ", " << value.y() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qdvec2 &value)
{
    debug.nospace() << "dvec2(" << value.x() << ", " << value.y() << ")";
    return debug.space();
}

inline QDebug operator << (QDebug debug, const qbvec2 &value)
{
    debug.nospace() << "bvec2(" << value.x() << ", " << value.y() << ")";
    return debug.space();
}

Q_DECLARE_TYPEINFO(qvec2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qivec2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(quvec2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qdvec2, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(qbvec2, Q_MOVABLE_TYPE);

Q_DECLARE_METATYPE(qvec2)
Q_DECLARE_METATYPE(qivec2)
Q_DECLARE_METATYPE(quvec2)
Q_DECLARE_METATYPE(qdvec2)
Q_DECLARE_METATYPE(qbvec2)

#endif // QUICKVECTOR2_H

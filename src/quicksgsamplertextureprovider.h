/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef QUICKSGSAMPLERTEXTUREPROVIDER_H
#define QUICKSGSAMPLERTEXTUREPROVIDER_H

#include <QSGTextureProvider>
#include "quicksampler.h"


class QuickSGSamplerTextureProvider : public QSGTextureProvider
{
    Q_OBJECT

public:

    QuickSGSamplerTextureProvider();
    ~QuickSGSamplerTextureProvider();

    QSGTexture *texture() const override;

    void setSourceProvider(QSGTextureProvider *sourceTextureProvider);
    void setMagFilter(QuickSamplerObject::Filter magFilter);
    void setMinFilter(QuickSamplerObject::Filter minFilter);
    void setLodBias(double lodBias);
    void setMaxLod(double maxLod);
    void setMinLod(double minLod);
    void setWrapS(QuickSamplerObject::Wrap wrapS);
    void setWrapT(QuickSamplerObject::Wrap wrapT);
    void setWrapR(QuickSamplerObject::Wrap wrapR);
    void setBorderColor(const QColor &borderColor);

public slots:

    void onSourceProviderDestroyed();

protected:

    GLuint m_sampler;
    QSGTextureProvider *m_sourceProvider;
    QuickSamplerObject::Filter m_magFilter;
    QuickSamplerObject::Filter m_minFilter;
    double m_lodBias;
    double m_maxLod;
    double m_minLod;
    QuickSamplerObject::Wrap m_wrapS;
    QuickSamplerObject::Wrap m_wrapT;
    QuickSamplerObject::Wrap m_wrapR;
    QColor m_borderColor;

};

#endif // QUICKSGSAMPLERTEXTUREPROVIDER_H

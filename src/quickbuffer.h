#ifndef QUICKBUFFER_H
#define QUICKBUFFER_H

#include <QSharedDataPointer>
#include <QObject>
#include "quickvector2.h"
#include "quickvector3.h"
#include "quickvector4.h"
#include "quickmatrix2.h"
#include "quickmatrix3.h"
#include "quickmatrix4.h"
#include "support.h"

class QuickBufferData;

class QuickBuffer
{
    Q_GADGET

    Q_PROPERTY(int size READ size WRITE setSize)

public:

    QuickBuffer(int reserveSize = 0);

    QuickBuffer(const QuickBuffer &);

    QuickBuffer(const QByteArray &byteArray);

    QuickBuffer &operator=(const QuickBuffer &);

    ~QuickBuffer();

    int size() const;
    void setSize(int size);

    Q_INVOKABLE void addFloat(float value);
    Q_INVOKABLE void addVec2(const qvec2 &value);
    Q_INVOKABLE void addVec3(const qvec3 &value);
    Q_INVOKABLE void addVec4(const qvec4 &value);

    Q_INVOKABLE void addInt(int value);
    Q_INVOKABLE void addIvec2(const qivec2 &value);
    Q_INVOKABLE void addIvec3(const qivec3 &value);
    Q_INVOKABLE void addIvec4(const qivec4 &value);

    Q_INVOKABLE void addUint(unsigned int value);
    Q_INVOKABLE void addUvec2(const quvec2 &value);
    Q_INVOKABLE void addUvec3(const quvec3 &value);
    Q_INVOKABLE void addUvec4(const quvec4 &value);

    Q_INVOKABLE void addDouble(double value);
    Q_INVOKABLE void addDvec2(const qdvec2 &value);
    Q_INVOKABLE void addDvec3(const qdvec3 &value);
    Q_INVOKABLE void addDvec4(const qdvec4 &value);

    Q_INVOKABLE void addMat2(const qmat2 &value);
    Q_INVOKABLE void addMat2x3(const qmat2x3 &value);
    Q_INVOKABLE void addMat2x4(const qmat2x4 &value);

    Q_INVOKABLE void addMat3x2(const qmat3x2 &value);
    Q_INVOKABLE void addMat3(const qmat3 &value);
    Q_INVOKABLE void addMat3x4(const qmat3x4 &value);

    Q_INVOKABLE void addMat4x2(const qmat4x2 &value);
    Q_INVOKABLE void addMat4x3(const qmat4x3 &value);
    Q_INVOKABLE void addMat4(const qmat4 &value);

    Q_INVOKABLE void addDmat2(const qdmat2 &value);
    Q_INVOKABLE void addDmat2x3(const qdmat2x3 &value);
    Q_INVOKABLE void addDmat2x4(const qdmat2x4 &value);

    Q_INVOKABLE void addDmat3x2(const qdmat3x2 &value);
    Q_INVOKABLE void addDmat3(const qdmat3 &value);
    Q_INVOKABLE void addDmat3x4(const qdmat3x4 &value);

    Q_INVOKABLE void addDmat4x2(const qdmat4x2 &value);
    Q_INVOKABLE void addDmat4x3(const qdmat4x3 &value);
    Q_INVOKABLE void addDmat4(const qdmat4 &value);

    Q_INVOKABLE void addBuffer(const QuickBuffer &value);

    Q_INVOKABLE QVariant intAt(unsigned int index) const;
    Q_INVOKABLE QVariant ivec2At(unsigned int index) const;
    Q_INVOKABLE QVariant ivec3At(unsigned int index) const;
    Q_INVOKABLE QVariant ivec4At(unsigned int index) const;

    Q_INVOKABLE QVariant uintAt(unsigned int index) const;
    Q_INVOKABLE QVariant uvec2At(unsigned int index) const;
    Q_INVOKABLE QVariant uvec3At(unsigned int index) const;
    Q_INVOKABLE QVariant uvec4At(unsigned int index) const;

    Q_INVOKABLE QVariant floatAt(unsigned int index) const;
    Q_INVOKABLE QVariant vec2At(unsigned int index) const;
    Q_INVOKABLE QVariant vec3At(unsigned int index) const;
    Q_INVOKABLE QVariant vec4At(unsigned int index) const;

    Q_INVOKABLE QVariant doubleAt(unsigned int index) const;
    Q_INVOKABLE QVariant dvec2At(unsigned int index) const;
    Q_INVOKABLE QVariant dvec3At(unsigned int index) const;
    Q_INVOKABLE QVariant dvec4At(unsigned int index) const;

    Q_INVOKABLE QVariant intAtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant ivec2AtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant ivec3AtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant ivec4AtOffset(unsigned int offset) const;

    Q_INVOKABLE QVariant uintAtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant uvec2AtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant uvec3AtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant uvec4AtOffset(unsigned int offset) const;

    Q_INVOKABLE QVariant floatAtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant vec2AtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant vec3AtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant vec4AtOffset(unsigned int offset) const;

    Q_INVOKABLE QVariant doubleAtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant dvec2AtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant dvec3AtOffset(unsigned int offset) const;
    Q_INVOKABLE QVariant dvec4AtOffset(unsigned int offset) const;

    Q_INVOKABLE void setIntAtOffset(const int &value, unsigned int offset);
    Q_INVOKABLE void setIvec2AtOffset(const qivec2 &value, unsigned int offset);
    Q_INVOKABLE void setIvec3AtOffset(const qivec3 &value, unsigned int offset);
    Q_INVOKABLE void setIvec4AtOffset(const qivec4 &value, unsigned int offset);

    Q_INVOKABLE void setUintAtOffset(const uint &value, unsigned int offset);
    Q_INVOKABLE void setUivec2AtOffset(const quvec2 &value, unsigned int offset);
    Q_INVOKABLE void setUivec3AtOffset(const quvec3 &value, unsigned int offset);
    Q_INVOKABLE void setUivec4AtOffset(const quvec4 &value, unsigned int offset);

    Q_INVOKABLE void setFloatAtOffset(const float &value, unsigned int offset);
    Q_INVOKABLE void setVec2AtOffset(const qvec2 &value, unsigned int offset);
    Q_INVOKABLE void setVec3AtOffset(const qvec3 &value, unsigned int offset);
    Q_INVOKABLE void setVec4AtOffset(const qvec4 &value, unsigned int offset);

    Q_INVOKABLE void setDoubleAtOffset(const double &value, unsigned int offset);
    Q_INVOKABLE void setDvec2AtOffset(const qdvec2 &value, unsigned int offset);
    Q_INVOKABLE void setDvec3AtOffset(const qdvec3 &value, unsigned int offset);
    Q_INVOKABLE void setDvec4AtOffset(const qdvec4 &value, unsigned int offset);

    Q_INVOKABLE void setIntAt(const int &value, unsigned int index);
    Q_INVOKABLE void setIvec2At(const qivec2 &value, unsigned int index);
    Q_INVOKABLE void setIvec3At(const qivec3 &value, unsigned int index);
    Q_INVOKABLE void setIvec4At(const qivec4 &value, unsigned int index);

    Q_INVOKABLE void setUintAt(const uint &value, unsigned int index);
    Q_INVOKABLE void setUvec2At(const quvec2 &value, unsigned int index);
    Q_INVOKABLE void setUvec3At(const quvec3 &value, unsigned int index);
    Q_INVOKABLE void setUvec4At(const quvec4 &value, unsigned int index);

    Q_INVOKABLE void setFloatAt(const float &value, unsigned int index);
    Q_INVOKABLE void setVec2At(const qvec2 &value, unsigned int index);
    Q_INVOKABLE void setVec3At(const qvec3 &value, unsigned int index);
    Q_INVOKABLE void setVec4At(const qvec4 &value, unsigned int index);

    Q_INVOKABLE void setDoubleAt(const double &value, unsigned int index);
    Q_INVOKABLE void setDvec2At(const qdvec2 &value, unsigned int index);
    Q_INVOKABLE void setDvec3At(const qdvec3 &value, unsigned int index);
    Q_INVOKABLE void setDvec4At(const qdvec4 &value, unsigned int index);

    Q_INVOKABLE void setBufferAtOffset(const QuickBuffer &value, unsigned int offset);

    operator QByteArray() const;

private:

    QSharedDataPointer<QuickBufferData> data;
};

Q_DECLARE_METATYPE(QuickBuffer)

#endif // QUICKBUFFER_H

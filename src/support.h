/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#ifndef SUPPORT_H
#define SUPPORT_H

#include <QtGlobal>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QDebug>
#include <QRunnable>
#include "quickvector2.h"
#include "quickvector3.h"
#include "quickvector4.h"
#include "quickmatrix2.h"
#include "quickmatrix3.h"
#include "quickmatrix4.h"
#include <QDir>

#ifndef GL_MAX_TESS_CONTROL_ATOMIC_CFuOUNTERS
#define GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS 0x92D3
#endif
#ifndef GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS
#define GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS 0x90D8
#endif
#ifndef GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS
#define GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS 0x92D4
#endif
#ifndef GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS
#define GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS 0x90D9
#endif
#ifndef GL_MAX_UNIFORM_LOCATIONS
#define GL_MAX_UNIFORM_LOCATIONS 0x826E
#endif
#ifndef GL_MAX_VERTEX_ATOMIC_COUNTERS
#define GL_MAX_VERTEX_ATOMIC_COUNTERS 0x92D2
#endif
#ifndef GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS
#define GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS 0x90D6
#endif
#ifndef GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET
#define GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET 0x82D9
#endif
#ifndef GL_MAX_VERTEX_ATTRIB_BINDINGS
#define GL_MAX_VERTEX_ATTRIB_BINDINGS 0x82DA
#endif
#ifndef GL_MAX_ELEMENT_INDEX
#define GL_MAX_ELEMENT_INDEX 0x8D6B
#endif
#ifndef GL_INT64_ARB
#define GL_INT64_ARB 0x140E
#endif
#ifndef GL_COMPUTE_SHADER
#define GL_COMPUTE_SHADER 0x91B9
#endif
#ifndef GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS
#define GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS 0x90DB
#endif
#ifndef GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS
#define GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS 0x90DC
#endif
#ifndef GL_MAX_COMPUTE_UNIFORM_BLOCKS
#define GL_MAX_COMPUTE_UNIFORM_BLOCKS 0x91BB
#endif
#ifndef GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS
#define GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS 0x91BC
#endif
#ifndef GL_MAX_COMPUTE_UNIFORM_COMPONENTS
#define GL_MAX_COMPUTE_UNIFORM_COMPONENTS 0x8263
#endif
#ifndef GL_MAX_COMPUTE_ATOMIC_COUNTERS
#define GL_MAX_COMPUTE_ATOMIC_COUNTERS 0x8265
#endif
#ifndef GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS
#define GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS 0x8264
#endif
#ifndef GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS
#define GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS 0x8266
#endif
#ifndef GL_MAX_DEBUG_GROUP_STACK_DEPTH
#define GL_MAX_DEBUG_GROUP_STACK_DEPTH 0x826C
#endif
#ifndef GL_MAX_COMBINED_ATOMIC_COUNTERS
#define GL_MAX_COMBINED_ATOMIC_COUNTERS 0x92D7
#endif
#ifndef GL_MAX_FRAGMENT_ATOMIC_COUNTERS
#define GL_MAX_FRAGMENT_ATOMIC_COUNTERS 0x92D6
#endif
#ifndef GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS
#define GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS 0x90DA
#endif
#ifndef GL_MAX_FRAMEBUFFER_WIDTH
#define GL_MAX_FRAMEBUFFER_WIDTH 0x9315
#endif
#ifndef GL_MAX_FRAMEBUFFER_HEIGHT
#define GL_MAX_FRAMEBUFFER_HEIGHT 0x9316
#endif
#ifndef GL_MAX_FRAMEBUFFER_LAYERS
#define GL_MAX_FRAMEBUFFER_LAYERS 0x9317
#endif
#ifndef GL_MAX_FRAMEBUFFER_SAMPLES
#define GL_MAX_FRAMEBUFFER_SAMPLES 0x9318
#endif
#ifndef GL_MAX_GEOMETRY_ATOMIC_COUNTERS
#define GL_MAX_GEOMETRY_ATOMIC_COUNTERS 0x92D5
#endif
#ifndef GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS
#define GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS 0x90D7
#endif
#ifndef GL_MAX_LABEL_LENGTH
#define GL_MAX_LABEL_LENGTH 0x82E8
#endif

#ifndef GL_VIEW_CLASS_128_BITS
#define GL_VIEW_CLASS_128_BITS 0x82C4
#endif

#ifndef GL_VIEW_CLASS_96_BITS
#define GL_VIEW_CLASS_96_BITS 0x82C5
#endif

#ifndef GL_VIEW_CLASS_64_BITS
#define GL_VIEW_CLASS_64_BITS 0x82C6
#endif

#ifndef GL_VIEW_CLASS_48_BITS
#define GL_VIEW_CLASS_48_BITS 0x82C7
#endif

#ifndef GL_VIEW_CLASS_32_BITS
#define GL_VIEW_CLASS_32_BITS 0x82C8
#endif

#ifndef GL_VIEW_CLASS_24_BITS
#define GL_VIEW_CLASS_24_BITS 0x82C9
#endif

#ifndef GL_VIEW_CLASS_16_BITS
#define GL_VIEW_CLASS_16_BITS 0x82CA
#endif

#ifndef GL_VIEW_CLASS_8_BITS
#define GL_VIEW_CLASS_8_BITS 0x82CB
#endif

#ifndef GL_VIEW_CLASS_RGTC1_RED
#define GL_VIEW_CLASS_RGTC1_RED 0x82D0
#endif

#ifndef GL_VIEW_CLASS_RGTC2_RG
#define GL_VIEW_CLASS_RGTC2_RG 0x82D1
#endif

#ifndef GL_COMPRESSED_RGBA_BPTC_UNORM
#define GL_COMPRESSED_RGBA_BPTC_UNORM 0x8E8C
#endif

#ifndef GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM
#define GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM 0x8E8D
#endif

#ifndef GL_VIEW_CLASS_BPTC_UNORM
#define GL_VIEW_CLASS_BPTC_UNORM 0x82D2
#endif

#ifndef GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT
#define GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT 0x8E8E
#endif

#ifndef GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT
#define GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT 0x8E8F
#endif

#ifndef GL_VIEW_CLASS_BPTC_FLOAT
#define GL_VIEW_CLASS_BPTC_FLOAT 0x82D3
#endif

#ifndef GL_UNSIGNED_INT64_ARB
#define GL_UNSIGNED_INT64_ARB 0x140F
#endif

#ifndef GL_TEXTURE_IMMUTABLE_FORMAT
#define GL_TEXTURE_IMMUTABLE_FORMAT 0x912F
#endif

#ifndef GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX
#define GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX 0x92DA
#endif

#ifndef GL_SHADER_STORAGE_BLOCK
#define GL_SHADER_STORAGE_BLOCK 0x92E6
#endif

#ifndef GL_ACTIVE_RESOURCES
#define GL_ACTIVE_RESOURCES 0x92F5
#endif

#ifndef GL_MAX_NAME_LENGTH
#define GL_MAX_NAME_LENGTH 0x92F6
#endif

#ifndef GL_BUFFER_BINDING
#define GL_BUFFER_BINDING 0x9302
#endif

#ifndef GL_BUFFER_DATA_SIZE
#define GL_BUFFER_DATA_SIZE 0x9303
#endif

#ifndef GL_NUM_ACTIVE_VARIABLES
#define GL_NUM_ACTIVE_VARIABLES 0x9304
#endif

#ifndef GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS
#define GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS 0x90DD
#endif

#ifndef GL_ACTIVE_VARIABLES
#define GL_ACTIVE_VARIABLES 0x9305
#endif

#ifndef GL_BUFFER_VARIABLE
#define GL_BUFFER_VARIABLE 0x92E5
#endif

#ifndef GL_TYPE
#define GL_TYPE 0x92FA
#endif

#ifndef GL_ARRAY_SIZE
#define GL_ARRAY_SIZE 0x92FB
#endif

#ifndef GL_OFFSET
#define GL_OFFSET 0x92FC
#endif

#ifndef GL_BLOCK_INDEX
#define GL_BLOCK_INDEX 0x92FD
#endif

#ifndef GL_ARRAY_STRIDE
#define GL_ARRAY_STRIDE 0x92FE
#endif

#ifndef GL_MATRIX_STRIDE
#define GL_MATRIX_STRIDE 0x92FF
#endif

#ifndef GL_IS_ROW_MAJOR
#define GL_IS_ROW_MAJOR 0x9300
#endif


#ifndef GL_TOP_LEVEL_ARRAY_SIZE
#define GL_TOP_LEVEL_ARRAY_SIZE 0x930C
#endif

#ifndef GL_TOP_LEVEL_ARRAY_STRIDE
#define GL_TOP_LEVEL_ARRAY_STRIDE 0x930D
#endif

#ifndef GL_IMAGE_1D
#define GL_IMAGE_1D 0x904C
#endif

#ifndef GL_IMAGE_2D
#define GL_IMAGE_2D 0x904D
#endif

#ifndef GL_IMAGE_3D
#define GL_IMAGE_3D 0x904E
#endif

#ifndef GL_IMAGE_2D_RECT
#define GL_IMAGE_2D_RECT 0x904F
#endif

#ifndef GL_IMAGE_CUBE
#define GL_IMAGE_CUBE 0x9050
#endif

#ifndef GL_IMAGE_BUFFER
#define GL_IMAGE_BUFFER 0x9051
#endif

#ifndef GL_IMAGE_1D_ARRAY
#define GL_IMAGE_1D_ARRAY 0x9052
#endif

#ifndef GL_IMAGE_2D_ARRAY
#define GL_IMAGE_2D_ARRAY 0x9053
#endif

#ifndef GL_IMAGE_CUBE_MAP_ARRAY
#define GL_IMAGE_CUBE_MAP_ARRAY 0x9054
#endif

#ifndef GL_IMAGE_2D_MULTISAMPLE
#define GL_IMAGE_2D_MULTISAMPLE 0x9055
#endif

#ifndef GL_IMAGE_2D_MULTISAMPLE_ARRAY
#define GL_IMAGE_2D_MULTISAMPLE_ARRAY 0x9056
#endif

#ifndef GL_INT_IMAGE_1D
#define GL_INT_IMAGE_1D 0x9057
#endif

#ifndef GL_INT_IMAGE_2D
#define GL_INT_IMAGE_2D 0x9058
#endif

#ifndef GL_INT_IMAGE_3D
#define GL_INT_IMAGE_3D 0x9059
#endif

#ifndef GL_INT_IMAGE_2D_RECT
#define GL_INT_IMAGE_2D_RECT 0x905A
#endif

#ifndef GL_INT_IMAGE_CUBE
#define GL_INT_IMAGE_CUBE 0x905B
#endif

#ifndef GL_INT_IMAGE_BUFFER
#define GL_INT_IMAGE_BUFFER 0x905C
#endif

#ifndef GL_INT_IMAGE_1D_ARRAY
#define GL_INT_IMAGE_1D_ARRAY 0x905D
#endif

#ifndef GL_INT_IMAGE_2D_ARRAY
#define GL_INT_IMAGE_2D_ARRAY 0x905E
#endif

#ifndef GL_INT_IMAGE_CUBE_MAP_ARRAY
#define GL_INT_IMAGE_CUBE_MAP_ARRAY 0x905F
#endif

#ifndef GL_INT_IMAGE_2D_MULTISAMPLE
#define GL_INT_IMAGE_2D_MULTISAMPLE 0x9060
#endif

#ifndef GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY
#define GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY 0x9061
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_1D
#define GL_UNSIGNED_INT_IMAGE_1D 0x9062
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_2D
#define GL_UNSIGNED_INT_IMAGE_2D 0x9063
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_3D
#define GL_UNSIGNED_INT_IMAGE_3D 0x9064
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_2D_RECT
#define GL_UNSIGNED_INT_IMAGE_2D_RECT 0x9065
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_CUBE
#define GL_UNSIGNED_INT_IMAGE_CUBE 0x9066
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_BUFFER
#define GL_UNSIGNED_INT_IMAGE_BUFFER 0x9067
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_1D_ARRAY
#define GL_UNSIGNED_INT_IMAGE_1D_ARRAY 0x9068
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_2D_ARRAY
#define GL_UNSIGNED_INT_IMAGE_2D_ARRAY 0x9069
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY
#define GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY 0x906A
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE
#define GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE 0x906B
#endif

#ifndef GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY
#define GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY 0x906C
#endif

#ifndef GL_UNSIGNED_INT_ATOMIC_COUNTER
#define GL_UNSIGNED_INT_ATOMIC_COUNTER 0x92DB
#endif

#ifndef GL_MAX_COMPUTE_VARIABLE_GROUP_SIZE_ARB
#define GL_MAX_COMPUTE_VARIABLE_GROUP_SIZE_ARB 0x9345
#endif

#ifndef GL_SHADER_STORAGE_BUFFER
#define GL_SHADER_STORAGE_BUFFER 0x90D2
#endif

#ifndef GL_MAX_COMPUTE_WORK_GROUP_COUNT
#define GL_MAX_COMPUTE_WORK_GROUP_COUNT   0x91BE
#endif

#ifndef GL_MAX_COMPUTE_VARIABLE_GROUP_INVOCATIONS_ARB
#define GL_MAX_COMPUTE_VARIABLE_GROUP_INVOCATIONS_ARB 0x9344
#endif

#ifndef GL_MAX_COMPUTE_WORK_GROUP_SIZE
#define GL_MAX_COMPUTE_WORK_GROUP_SIZE 0x91BF
#endif

#ifndef GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS
#define GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS 0x90EB
#endif


#ifndef GL_PATCH_VERTICES
#define GL_PATCH_VERTICES 0x8E72
#endif

#ifndef GL_SHADER_STORAGE_BUFFER
#define GL_SHADER_STORAGE_BUFFER 0x90D2
#endif

#ifndef GL_MAP_READ_BIT
#define GL_MAP_READ_BIT                   0x0001
#endif

#ifndef GL_MAP_WRITE_BIT
#define GL_MAP_WRITE_BIT                  0x0002
#endif

#ifndef GL_MAP_INVALIDATE_RANGE_BIT
#define GL_MAP_INVALIDATE_RANGE_BIT       0x0004
#endif

#ifndef GL_MAP_INVALIDATE_BUFFER_BIT
#define GL_MAP_INVALIDATE_BUFFER_BIT      0x0008
#endif

#ifndef GL_MAP_FLUSH_EXPLICIT_BIT
#define GL_MAP_FLUSH_EXPLICIT_BIT         0x0010
#endif

#ifndef GL_MAP_UNSYNCHRONIZED_BIT
#define GL_MAP_UNSYNCHRONIZED_BIT         0x0020
#endif

#ifndef GL_DYNAMIC_STORAGE_BIT
#define GL_DYNAMIC_STORAGE_BIT            0x0100
#endif

#ifndef GL_CLIENT_STORAGE_BIT
#define GL_CLIENT_STORAGE_BIT             0x0200
#endif

#ifndef GL_MAP_PERSISTENT_BIT
#define GL_MAP_PERSISTENT_BIT             0x0040
#endif

#ifndef GL_MAP_COHERENT_BIT
#define GL_MAP_COHERENT_BIT               0x0080
#endif

#ifndef GL_SHADER_STORAGE_BARRIER_BIT
#define GL_SHADER_STORAGE_BARRIER_BIT     0x00002000
#endif

#ifndef GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT
#define GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT 0x00004000
#endif

#ifndef GL_TEXTURE_UPDATE_BARRIER_BIT
#define GL_TEXTURE_UPDATE_BARRIER_BIT 0x00000100
#endif

#ifndef GL_DOUBLE
#define GL_DOUBLE                         0x140A
#endif

#ifndef GL_DOUBLE_VEC2
#define GL_DOUBLE_VEC2                    0x8FFC
#endif

#ifndef GL_DOUBLE_VEC3
#define GL_DOUBLE_VEC3                    0x8FFD
#endif

#ifndef GL_DOUBLE_VEC4
#define GL_DOUBLE_VEC4                    0x8FFE
#endif

#ifndef GL_DOUBLE_MAT2
#define GL_DOUBLE_MAT2                    0x8F46
#endif

#ifndef GL_DOUBLE_MAT3
#define GL_DOUBLE_MAT3                    0x8F47
#endif

#ifndef GL_DOUBLE_MAT4
#define GL_DOUBLE_MAT4                    0x8F48
#endif

#ifndef GL_DOUBLE_MAT2x3
#define GL_DOUBLE_MAT2x3                  0x8F49
#endif

#ifndef GL_DOUBLE_MAT2x4
#define GL_DOUBLE_MAT2x4                  0x8F4A
#endif

#ifndef GL_DOUBLE_MAT3x2
#define GL_DOUBLE_MAT3x2                  0x8F4B
#endif

#ifndef GL_DOUBLE_MAT3x4
#define GL_DOUBLE_MAT3x4                  0x8F4C
#endif

#ifndef GL_DOUBLE_MAT4x2
#define GL_DOUBLE_MAT4x2                  0x8F4D
#endif

#ifndef GL_DOUBLE_MAT4x3
#define GL_DOUBLE_MAT4x3                  0x8F4E
#endif

#ifndef GL_STACK_UNDERFLOW
#define GL_STACK_UNDERFLOW                0x0504
#endif

#ifndef GL_STACK_OVERFLOW
#define GL_STACK_OVERFLOW                 0x0503
#endif

#ifndef GL_SAMPLER_1D_SHADOW
#define GL_SAMPLER_1D_SHADOW              0x8B61
#endif

#ifndef GL_SAMPLER_1D_ARRAY
#define GL_SAMPLER_1D_ARRAY               0x8DC0
#endif

#ifndef GL_SAMPLER_1D_ARRAY_SHADOW
#define GL_SAMPLER_1D_ARRAY_SHADOW        0x8DC3
#endif

#ifndef GL_SAMPLER_2D_MULTISAMPLE
#define GL_SAMPLER_2D_MULTISAMPLE         0x9108
#endif

#ifndef GL_SAMPLER_2D_MULTISAMPLE_ARRAY
#define GL_SAMPLER_2D_MULTISAMPLE_ARRAY   0x910B
#endif

#ifndef GL_SAMPLER_BUFFER
#define GL_SAMPLER_BUFFER                 0x8DC2
#endif

#ifndef GL_SAMPLER_2D_RECT
#define GL_SAMPLER_2D_RECT                0x8B63
#endif

#ifndef GL_SAMPLER_2D_RECT_SHADOW
#define GL_SAMPLER_2D_RECT_SHADOW         0x8B64
#endif

#ifndef GL_INT_SAMPLER_1D
#define GL_INT_SAMPLER_1D                 0x8DC9
#endif

#ifndef GL_INT_SAMPLER_1D_ARRAY
#define GL_INT_SAMPLER_1D_ARRAY           0x8DCE
#endif

#ifndef GL_INT_SAMPLER_2D_MULTISAMPLE
#define GL_INT_SAMPLER_2D_MULTISAMPLE     0x9109
#endif

#ifndef GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY
#define GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY 0x910C
#endif

#ifndef GL_INT_SAMPLER_BUFFER
#define GL_INT_SAMPLER_BUFFER             0x8DD0
#endif

#ifndef GL_INT_SAMPLER_2D_RECT
#define GL_INT_SAMPLER_2D_RECT            0x8DCD
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_1D
#define GL_UNSIGNED_INT_SAMPLER_1D        0x8DD1
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_1D_ARRAY
#define GL_UNSIGNED_INT_SAMPLER_1D_ARRAY  0x8DD6
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE
#define GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE 0x910A
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY
#define GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY 0x910D
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_BUFFER
#define GL_UNSIGNED_INT_SAMPLER_BUFFER    0x8DD8
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_2D_RECT
#define GL_UNSIGNED_INT_SAMPLER_2D_RECT   0x8DD5
#endif

#ifndef GL_TEXTURE_1D
#define GL_TEXTURE_1D                     0x0DE0
#endif

#ifndef GL_TEXTURE_1D_ARRAY
#define GL_TEXTURE_1D_ARRAY               0x8C18
#endif

#ifndef GL_TEXTURE_CUBE_MAP_ARRAY
#define GL_TEXTURE_CUBE_MAP_ARRAY         0x9009
#endif

#ifndef GL_TEXTURE_2D_MULTISAMPLE
#define GL_TEXTURE_2D_MULTISAMPLE         0x9100
#endif

#ifndef GL_TEXTURE_2D_MULTISAMPLE_ARRAY
#define GL_TEXTURE_2D_MULTISAMPLE_ARRAY   0x9102

#endif

#ifndef GL_TEXTURE_RECTANGLE
#define GL_TEXTURE_RECTANGLE              0x84F5
#endif

#ifndef GL_TEXTURE_BUFFER
#define GL_TEXTURE_BUFFER                 0x8C2A
#endif

#ifndef GL_TESS_CONTROL_SHADER
#define GL_TESS_CONTROL_SHADER            0x8E88
#endif

#ifndef GL_TESS_EVALUATION_SHADER
#define GL_TESS_EVALUATION_SHADER         0x8E87
#endif

#ifndef GL_GEOMETRY_SHADER
#define GL_GEOMETRY_SHADER                0x8DD9
#endif

#ifndef GL_RGB4
#define GL_RGB4                           0x804F
#endif

#ifndef GL_RGB5
#define GL_RGB5                           0x8050
#endif

#ifndef GL_RGB10
#define GL_RGB10                          0x8052
#endif

#ifndef GL_RGB12
#define GL_RGB12                          0x8053
#endif

#ifndef GL_RGBA2
#define GL_RGBA2                          0x8055
#endif

#ifndef GL_RGBA12
#define GL_RGBA12                         0x805A
#endif

#ifndef GL_SAMPLER_1D
#define GL_SAMPLER_1D                     0x8B5D
#endif

#ifndef GL_INT_SAMPLER_1D
#define GL_INT_SAMPLER_1D                 0x8DC9
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_1D
#define GL_UNSIGNED_INT_SAMPLER_1D        0x8DD1
#endif

#ifndef GL_SAMPLER_1D_ARRAY
#define GL_SAMPLER_1D_ARRAY               0x8DC0
#endif

#ifndef GL_INT_SAMPLER_1D_ARRAY
#define GL_INT_SAMPLER_1D_ARRAY           0x8DCE
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_1D_ARRAY
#define GL_UNSIGNED_INT_SAMPLER_1D_ARRAY  0x8DD6
#endif

#ifndef GL_SAMPLER_BUFFER
#define GL_SAMPLER_BUFFER                 0x8DC2
#endif

#ifndef GL_INT_SAMPLER_BUFFER
#define GL_INT_SAMPLER_BUFFER             0x8DD0
#endif

#ifndef GL_UNSIGNED_INT_SAMPLER_BUFFER
#define GL_UNSIGNED_INT_SAMPLER_BUFFER    0x8DD8
#endif


#ifndef GL_RGBA16
#define GL_RGBA16                         0x805B
#endif

#ifndef GL_RGBA16_SNORM
#define GL_RGBA16_SNORM                   0x8F9B
#endif

#ifndef GL_RGB16
#define GL_RGB16                          0x8054
#endif

#ifndef GL_RGB16_SNORM
#define GL_RGB16_SNORM                    0x8F9A
#endif

#ifndef GL_RG16
#define GL_RG16                           0x822C
#endif

#ifndef GL_RG16_SNORM
#define GL_RG16_SNORM                     0x8F99
#endif

#ifndef GL_R16
#define GL_R16                            0x822A
#endif

#ifndef GL_RG16
#define GL_RG16                                              0x822C
#endif

#ifndef GL_RG16_SNORM
#define GL_RG16_SNORM                     0x8F99
#endif

#ifndef GL_R16
#define GL_R16                                               0x822A
#endif

#ifndef GL_R16_SNORM
#define GL_R16_SNORM                      0x8F98
#endif

#ifndef GL_COMPRESSED_RED_RGTC1
#define GL_COMPRESSED_RED_RGTC1                              0x8DBB
#endif

#ifndef GL_COMPRESSED_SIGNED_RED_RGTC1
#define GL_COMPRESSED_SIGNED_RED_RGTC1                       0x8DBC
#endif

#ifndef GL_COMPRESSED_RG_RGTC2
#define GL_COMPRESSED_RG_RGTC2                               0x8DBD
#endif

#ifndef GL_COMPRESSED_SIGNED_RG_RGTC2
#define GL_COMPRESSED_SIGNED_RG_RGTC2                        0x8DBE
#endif

#ifndef GL_UNSIGNED_INT_10_10_10_2
#define GL_UNSIGNED_INT_10_10_10_2        0x8036
#endif

#ifndef GL_READ_WRITE
#define GL_READ_WRITE                                  0x88BA
#endif

#ifndef GL_TEXTURE_BINDING_1D
#define GL_TEXTURE_BINDING_1D             0x8068
#endif

#ifndef GL_TEXTURE_BINDING_1D_ARRAY
#define GL_TEXTURE_BINDING_1D_ARRAY       0x8C1C
#endif

#ifndef GL_TEXTURE_BINDING_RECTANGLE
#define GL_TEXTURE_BINDING_RECTANGLE      0x84F6
#endif

#ifndef GL_TEXTURE_BINDING_BUFFER
#define GL_TEXTURE_BINDING_BUFFER         0x8C2C
#endif

#ifndef GL_TEXTURE_BINDING_CUBE_MAP_ARRAY
#define GL_TEXTURE_BINDING_CUBE_MAP_ARRAY 0x900A
#endif

#ifndef GL_TEXTURE_BINDING_2D_MULTISAMPLE
#define GL_TEXTURE_BINDING_2D_MULTISAMPLE 0x9104
#endif

#ifndef GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY
#define GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY 0x9105
#endif

#ifndef GL_TEXTURE_WIDTH
#define GL_TEXTURE_WIDTH                  0x1000
#endif

#ifndef GL_TEXTURE_HEIGHT
#define GL_TEXTURE_HEIGHT                 0x1001
#endif

#ifndef GL_TEXTURE_DEPTH
#define GL_TEXTURE_DEPTH                  0x8071
#endif

#ifndef GL_TEXTURE_LOD_BIAS
#define GL_TEXTURE_LOD_BIAS               0x8501
#endif

#ifndef GL_TEXTURE_BORDER_COLOR
#define GL_TEXTURE_BORDER_COLOR           0x1004
#endif

#ifndef GL_COPY
#define GL_COPY                           0x1503
#endif
#ifndef GL_FILL
#define GL_FILL                           0x1B02
#endif

#ifndef GL_LOWER_LEFT
#define GL_LOWER_LEFT                     0x8CA1
#endif

#ifndef GL_LINE_SMOOTH
#define GL_LINE_SMOOTH                    0x0B20
#endif

#ifndef GL_COLOR_LOGIC_OP
#define GL_COLOR_LOGIC_OP                 0x0BF2
#endif

#ifndef GL_LOGIC_OP_MODE
#define GL_LOGIC_OP_MODE                  0x0BF0
#endif

#ifndef GL_POLYGON_MODE
#define GL_POLYGON_MODE                   0x0B40
#endif

#ifndef GL_PROGRAM_POINT_SIZE
#define GL_PROGRAM_POINT_SIZE             0x8642
#endif

#ifndef GL_POINT_FADE_THRESHOLD_SIZE
#define GL_POINT_FADE_THRESHOLD_SIZE      0x8128
#endif

#ifndef GL_POINT_SPRITE_COORD_ORIGIN
#define GL_POINT_SPRITE_COORD_ORIGIN      0x8CA0
#endif

#ifndef GL_POINT_SPRITE
#define GL_POINT_SPRITE                   0x8861
#endif

#ifndef GL_TEXTURE_INTERNAL_FORMAT
#define GL_TEXTURE_INTERNAL_FORMAT        0x1003
#endif

#ifndef GL_TEXTURE_RED_TYPE
#define GL_TEXTURE_RED_TYPE                                  0x8C10
#endif

#ifndef GL_TEXTURE_GREEN_TYPE
#define GL_TEXTURE_GREEN_TYPE                                0x8C11
#endif

#ifndef GL_TEXTURE_BLUE_TYPE
#define GL_TEXTURE_BLUE_TYPE                                 0x8C12
#endif

#ifndef GL_TEXTURE_ALPHA_TYPE
#define GL_TEXTURE_ALPHA_TYPE                                0x8C13
#endif

#ifndef GL_TEXTURE_DEPTH_TYPE
#define GL_TEXTURE_DEPTH_TYPE                                0x8C16
#endif

#ifndef GL_TEXTURE_RED_SIZE
#define GL_TEXTURE_RED_SIZE               0x805C
#endif

#ifndef GL_TEXTURE_GREEN_SIZE
#define GL_TEXTURE_GREEN_SIZE             0x805D
#endif

#ifndef GL_TEXTURE_BLUE_SIZE
#define GL_TEXTURE_BLUE_SIZE              0x805E
#endif

#ifndef GL_TEXTURE_ALPHA_SIZE
#define GL_TEXTURE_ALPHA_SIZE             0x805F
#endif

#ifndef GL_TEXTURE_DEPTH_SIZE
#define GL_TEXTURE_DEPTH_SIZE             0x884A
#endif

#ifndef GL_TEXTURE_COMPRESSED
#define GL_TEXTURE_COMPRESSED             0x86A1
#endif

#ifndef GL_TEXTURE_COMPRESSED_IMAGE_SIZE
#define GL_TEXTURE_COMPRESSED_IMAGE_SIZE  0x86A0
#endif

#ifndef GL_LINE_WIDTH_RANGE
#define GL_LINE_WIDTH_RANGE               0x0B22
#endif

#ifndef GL_MAX_CLIP_DISTANCES
#define GL_MAX_CLIP_DISTANCES             0x0D32
#endif

#ifndef GL_MAX_COLOR_TEXTURE_SAMPLES
#define GL_MAX_COLOR_TEXTURE_SAMPLES      0x910E
#endif

#ifndef GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS
#define GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS 0x8A32
#endif

#ifndef GL_MAX_DEPTH_TEXTURE_SAMPLES
#define GL_MAX_DEPTH_TEXTURE_SAMPLES      0x910F
#endif

#ifndef GL_MAX_DUAL_SOURCE_DRAW_BUFFERS
#define GL_MAX_DUAL_SOURCE_DRAW_BUFFERS   0x88FC
#endif

#ifndef GL_MAX_GEOMETRY_INPUT_COMPONENTS
#define GL_MAX_GEOMETRY_INPUT_COMPONENTS  0x9123
#endif

#ifndef GL_MAX_GEOMETRY_OUTPUT_COMPONENTS
#define GL_MAX_GEOMETRY_OUTPUT_COMPONENTS 0x9124
#endif

#ifndef GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS
#define GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS 0x8C29
#endif

#ifndef GL_MAX_GEOMETRY_UNIFORM_BLOCKS
#define GL_MAX_GEOMETRY_UNIFORM_BLOCKS    0x8A2C
#endif

#ifndef GL_MAX_GEOMETRY_UNIFORM_COMPONENTS
#define GL_MAX_GEOMETRY_UNIFORM_COMPONENTS 0x8DDF
#endif

#ifndef GL_MAX_INTEGER_SAMPLES
#define GL_MAX_INTEGER_SAMPLES            0x9110
#endif

#ifndef GL_MAX_RECTANGLE_TEXTURE_SIZE
#define GL_MAX_RECTANGLE_TEXTURE_SIZE     0x84F8
#endif

#ifndef GL_MAX_SAMPLE_MASK_WORDS
#define GL_MAX_SAMPLE_MASK_WORDS          0x8E59
#endif

#ifndef GL_MAX_TEXTURE_BUFFER_SIZE
#define GL_MAX_TEXTURE_BUFFER_SIZE        0x8C2B
#endif

#ifndef GL_MAX_VARYING_FLOATS
#define GL_MAX_VARYING_FLOATS             0x8B4B
#endif

#ifndef GL_MAX_VIEWPORTS
#define GL_MAX_VIEWPORTS                  0x825B
#endif

#ifndef GL_POINT_SIZE_GRANULARITY
#define GL_POINT_SIZE_GRANULARITY         0x0B13
#endif

#ifndef GL_POINT_SIZE_RANGE
#define GL_POINT_SIZE_RANGE               0x0B12
#endif

#ifndef GL_SMOOTH_POINT_SIZE_RANGE
#define GL_SMOOTH_POINT_SIZE_RANGE        0x0B12
#endif

#ifndef GL_SMOOTH_LINE_WIDTH_RANGE
#define GL_SMOOTH_LINE_WIDTH_RANGE        0x0B22
#endif

#ifndef GL_SMOOTH_LINE_WIDTH_GRANULARITY
#define GL_SMOOTH_LINE_WIDTH_GRANULARITY  0x0B23
#endif

#ifndef GL_STEREO
#define GL_STEREO                         0x0C33
#endif

#ifndef GL_VIEWPORT_BOUNDS_RANGE
#define GL_VIEWPORT_BOUNDS_RANGE          0x825D
#endif

#ifndef GL_VIEWPORT_SUBPIXEL_BITS
#define GL_VIEWPORT_SUBPIXEL_BITS         0x825C
#endif

#ifndef GL_MAX_TESS_GEN_LEVEL
#define GL_MAX_TESS_GEN_LEVEL             0x8E7E
#endif

#ifndef GL_MAX_PATCH_VERTICES
#define GL_MAX_PATCH_VERTICES             0x8E7D
#endif

//#ifndef \1
//#endif

#define DEBUG_TIMING 0x1
#define DEBUG_TEXTURES 0x2
#define DEBUG_SSBOS 0x4

#if defined (QT_DEBUG)
#define debugGLquiet {QOpenGLFunctions gl; gl.initializeOpenGLFunctions(); while(gl.glGetError()!=GL_NO_ERROR);}
#else
#define debugGLquiet
#endif

#if defined (QT_DEBUG)
#define debugGL {QOpenGLFunctions gl; gl.initializeOpenGLFunctions(); GLenum error; while((error=gl.glGetError())!=GL_NO_ERROR) {qDebug().nospace() <<  "file://" << qPrintable(QDir::fromNativeSeparators(__FILE__)) << ":" << __LINE__ << " " << Q_FUNC_INFO << ": OpenGL error: " << QuickOpenGL::GLenum(error) << " ("<< int(error) <<")";}}(void)0
#else
#define debugGL
#endif


//#ifdef Q_OS_WIN
////currently the debugGL seems to break opengl on windows
//#undef debugGL
//#define debugGL
//#endif

namespace QuickOpenGL
{
int debugLevel();

int internalFormatSize(::GLenum internalFormat);
::GLenum baseInternalFormat(::GLenum sizedInternalFormat);
::GLenum internalFormatToPixelType(::GLenum internalFormat);
int internalFormatToComponents(::GLenum internalFormat);
::GLenum formatClass(::GLenum format);
::GLenum samplerToTarget(::GLenum sampler);
int tupleCount(::GLenum type);
::GLenum primitiveType(::GLenum type);
int tupleSize(::GLenum type);
int primitiveSize(::GLenum type);
int sizeOfType(::GLenum type);

bool isSampler(::GLenum type);
bool isImage(::GLenum type);
QString toString(::GLenum value);

enum GLenum{};

}


/*!
 * \brief QRunnable based class to cleanup objects
 *
 * ####Purpose
 *
 * The purpose of this class is to provide a QRunnable to delete an object in the QSGRenderThread.
 * This is achieved by instantiation the Cleanup object, then scheduling a render job via
 * QWindow::scheduleRenderJob in which the QRunnable is to run.  Cleanup automatically deletes
 * itself once run.
 *
 * This class should be used in QQuickItem::releaseResources overrides to delete OpenGL objects
 * in the QSGRenderThread where there is a valid and current QOpenGLContext.
 */
template <class T > class Cleanup : public QRunnable
{
public:

    /*!
     * \brief Constructor
     * \param object the object to clean up
     */
    Cleanup(T object) : m_object(object)
    {
        this->setAutoDelete(true);
    }

    /*!
     * \brief The run method
     */
    void run() override
    {
        delete m_object;
    }

protected:

    /*!
     * \brief The list of objects
     */
    T m_object;
};


/*!
 * \brief Cleanup helper function
 */
template <class T> static Cleanup<T> *cleanup(T object)
{
    return new Cleanup<T>(object);
}


#if defined (QT_DEBUG)
QDebug operator << (QDebug debug, QuickOpenGL::GLenum value);
#endif




#endif // SUPPORT_H


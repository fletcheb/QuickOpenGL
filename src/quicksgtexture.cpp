/****************************************************************************
**
** Copyright (C) 2015-19 Defence Science & Technology Group
**                       Department of Defence
**                       Commonwealth of Australia
**
** This file is part of the QuickOpenGL module.
**
** QuickOpenGL is free software: you can redistribute it and/or modify it
** under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or (at your
** option) any later version.
**
** QuickOpenGL is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
** FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
** for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with QuickOpenGL. If not, see <http://www.gnu.org/licenses/>.**
****************************************************************************/

#include "quicksgtexture.h"
#include "support.h"
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLFunctions_4_4_Core>
#include <QOpenGLExtraFunctions>
#include <QtConcurrentRun>
#include <QOffscreenSurface>
#include <QGuiApplication>
#include <QElapsedTimer>
#include <QtConcurrent>
#include "quickopenglthreadpool.h"
#include <QOpenGLPixelTransferOptions>
#include <QFile>


QList<QByteArray> readTexture(GLsync fenceSync, GLuint texture, GLenum target, QObject *object)
{

    QList<QByteArray> result;

    // get the OpenGL functions object
    Q_ASSERT(QOpenGLContext::currentContext());
    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

#ifdef QT_DEBUG
    QScopedPointer<QElapsedTimer> timer;
    qint64 syncTime =0;

    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        timer.reset(new QElapsedTimer);
        timer->start();
    }
#else
    Q_UNUSED(object)
#endif

    Q_ASSERT(glExtra->glIsSync(fenceSync));

    // wait for up to 1s for the fence sync
    glExtra->glClientWaitSync(fenceSync, 0, 1000000000);debugGL;

    glExtra->glDeleteSync(fenceSync);debugGL;

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        syncTime = timer->nsecsElapsed();
    }
#endif

    Q_ASSERT(glExtra->glIsTexture(texture));

    glExtra->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);debugGL;

    glExtra->glActiveTexture(GL_TEXTURE0);debugGL;

    glExtra->glBindTexture(target, texture);debugGL;


    GLint baseLevel = 0;
    glExtra->glGetTexParameteriv(target, GL_TEXTURE_BASE_LEVEL, &baseLevel);debugGL;

    GLint maxLevel = 0;
    glExtra->glGetTexParameteriv(target, GL_TEXTURE_MAX_LEVEL, &maxLevel);debugGL;

    for(int level=baseLevel;level<maxLevel;++level)
    {
        GLint width = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_WIDTH, &width);debugGL;

        GLint height = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_HEIGHT, &height);debugGL;

        GLint depth = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_DEPTH, &depth);debugGL;

        if((width==0)&&(height==0)&&(depth==0))
        {
            break;
        }        

        GLint internalFormat = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_INTERNAL_FORMAT, &internalFormat);debugGL;

        GLint redType = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_RED_TYPE, &redType);debugGL;

        GLint greenType = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_GREEN_TYPE, &greenType);debugGL;

        GLint blueType = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_BLUE_TYPE, &blueType);debugGL;

        GLint alphaType = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_ALPHA_TYPE, &alphaType);debugGL;

        GLint depthType = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_DEPTH_TYPE, &depthType);debugGL;

        // sizes in bits
        GLint redSize = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_RED_SIZE, &redSize);debugGL;

        GLint greenSize = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_GREEN_SIZE, &greenSize);debugGL;

        GLint blueSize = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_BLUE_SIZE, &blueSize);debugGL;

        GLint alphaSize = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_ALPHA_SIZE, &alphaSize);debugGL;

        GLint depthSize = 0;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_DEPTH_SIZE, &depthSize);debugGL;

        GLint compressed = GL_FALSE;
        glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_COMPRESSED, &compressed);debugGL;

        glExtra->glBindFramebuffer( GL_READ_FRAMEBUFFER, 0);debugGL;
        QByteArray buffer;

#ifndef QT_OPENGL_ES

        QOpenGLFunctions_3_3_Core *gl33 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_3_3_Core>();
        Q_ASSERT(gl33);
        gl33->initializeOpenGLFunctions();

        if(compressed==GL_TRUE)
        {
            GLint compressedSize = 0;
            glExtra->glGetTexLevelParameteriv(target, level, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &compressedSize);debugGL;

            buffer.resize(compressedSize);            
            gl33->glGetCompressedTexImage(target, level, buffer.data());debugGL;
        }
        else
        {
            GLenum format = QuickOpenGL::baseInternalFormat(GLenum(internalFormat));
            GLenum type = QuickOpenGL::internalFormatToPixelType(GLenum(internalFormat));
            int size = width*height*depth*((redSize>0?1:0)+(greenSize>0?1:0)+(blueSize>0?1:0)+(alphaSize>0?1:0)+(depthSize>0?1:0))*4;
            buffer.resize(size);
            gl33->glGetTexImage(target, level, format, type, buffer.data());debugGL;
        }
#else
        //TODO implement for OpenGL ES
        qWarning() << "QuickOpenGL:" << Q_FUNC_INFO << "not implemented for OpenGL ES";
#endif
        result.append(buffer);
    }


    glExtra->glBindTexture(target, 0);debugGL;

#ifdef QT_DEBUG
    if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
    {
        qint64 elapsed = timer->nsecsElapsed();
        qDebug().nospace() << "quickopengl.scenegraph.time.textureread: " << object << " reading texture id " << texture << " target " << QuickOpenGL::GLenum(target) << " took " << elapsed*1e-6 << "ms (wait sync: " << syncTime*1e-6 << "ms)";
    }
#endif

    return result;

}
QOpenGLTexture *createTexture(QOpenGLContext *rootContext, QOpenGLTexture *texture)
{
    QOpenGLContext *context = new QOpenGLContext;
    Q_ASSERT(rootContext->isValid());
    context->setShareContext(rootContext);
    bool ok = context->create();
    Q_ASSERT(ok);
    Q_ASSERT(context->isValid());

    QOffscreenSurface *surface = new QOffscreenSurface(QGuiApplication::primaryScreen());
    surface->setFormat(QSurfaceFormat::defaultFormat());
    surface->create();
    Q_ASSERT(surface->isValid());

    context->makeCurrent(surface);debugGL;

    texture->allocateStorage();debugGL;

#ifndef QT_OPENGL_ES
    QOpenGLFunctions_4_4_Core *gl44 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_4_Core>();
    if(gl44->initializeOpenGLFunctions())
    {
        int format;
        switch(texture->format())
        {
        case QOpenGLTexture::R16F:
        case QOpenGLTexture::RG16F:
        case QOpenGLTexture::RGB16F:
        case QOpenGLTexture::RGBA16F:
        case QOpenGLTexture::R32F:
        case QOpenGLTexture::RG32F:
        case QOpenGLTexture::RGB32F:
        case QOpenGLTexture::RGBA32F:
            format = GL_RGBA;
            break;
        case QOpenGLTexture::R8I:
        case QOpenGLTexture::RG8I:
        case QOpenGLTexture::RGB8I:
        case QOpenGLTexture::RGBA8I:
        case QOpenGLTexture::R16I:
        case QOpenGLTexture::RG16I:
        case QOpenGLTexture::RGB16I:
        case QOpenGLTexture::RGBA16I:
        case QOpenGLTexture::R32I:
        case QOpenGLTexture::RG32I:
        case QOpenGLTexture::RGB32I:
        case QOpenGLTexture::RGBA32I:
            format = GL_RGBA_INTEGER;
            break;
        default:
            format = GL_NONE;
            break;
        }

        gl44->glClearTexImage(texture->textureId(), 0, format, GL_UNSIGNED_BYTE, nullptr);debugGL;
    }
#endif

    context->doneCurrent();
    delete context;
    delete surface;

    return texture;
}


int QuickSGTexture::imageLayer() const
{
    return m_imageLayer;
}

QuickTexture::Access QuickSGTexture::imageAccess() const
{
    return m_imageAccess;
}

QuickTexture::Wrap QuickSGTexture::wrapS() const
{
    return m_wrapS;
}

int QuickSGTexture::mipLevels() const
{
    return m_mipLevels;
}

QuickTexture::Wrap QuickSGTexture::wrapT() const
{
    return m_wrapT;
}

QuickTexture::Wrap QuickSGTexture::wrapR() const
{
    return m_wrapR;
}

QuickTexture::Filter QuickSGTexture::magFilter() const
{
    return m_magFilter;
}

QuickTexture::Filter QuickSGTexture::minFilter() const
{
    return m_minFilter;
}

double QuickSGTexture::lodBias() const
{
    return m_lodBias;
}

double QuickSGTexture::maxLod() const
{
    return m_maxLod;
}

bool QuickSGTexture::zeroTexture() const
{
    return m_zeroTexture;
}

double QuickSGTexture::minLod() const
{
    return m_minLod;
}

QColor QuickSGTexture::borderColor() const
{
    return m_borderColor;
}

int QuickSGTexture::imageLevel() const
{
    return m_imageLevel;
}

QFuture<QList<QByteArray>> QuickSGTexture::getTextureData()
{
    Q_ASSERT(QOpenGLContext::currentContext());

    QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
    Q_ASSERT(glExtra);
    glExtra->initializeOpenGLFunctions();debugGL;

    // place a fence in the GL command stream
    GLsync fenceSync = glExtra->glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);debugGL;
    Q_ASSERT(fenceSync);

    glExtra->glMemoryBarrier(GL_TEXTURE_UPDATE_BARRIER_BIT);debugGL;

    return QtConcurrent::run(QuickOpenGLThreadPool::globalInstance(), &readTexture, fenceSync, textureId(), target(), this->property("QuickTexture").value<QObject *>());

}

void QuickSGTexture::setTextureData(const QList<QByteArray> &data)
{
    // only write the texture once per render
    if(m_textureWritten)
    {
        return;
    }

    if(m_mipLevels!=data.count())
    {
        qWarning() << "QuickOpenGL:" << this->property("QuickTexture").value<QObject *>() << "attempting to initialize texture with" << m_mipLevels << "with data with" << data.count() << "images";
    }
    else
    {

        bool zeroTexture = m_zeroTexture;
        m_zeroTexture = false;
        bind();
        m_zeroTexture = zeroTexture;

        for(int level=0; level<data.count(); ++level)
        {
            int width = std::max(1, m_width/(1<<level));
            int height = std::max(1, m_height/(1<<level));
            int depth = std::max(1, m_depth/(1<<level));

            QByteArray buffer = data.at(level);
            int size=0;
            switch(m_target)
            {
            case GL_TEXTURE_1D:
                size = width;
                break;
            case GL_TEXTURE_2D:
                size = width*height;
                break;
            case GL_TEXTURE_3D:
                size = width*height*depth;
                break;
            case GL_TEXTURE_1D_ARRAY:
                size = width*m_layers;
                break;
            case GL_TEXTURE_2D_ARRAY:
                size = width*height*m_layers;
                break;
            default:
                qWarning() << "QuickOpenGL:" << this->property("QuickTexture").value<QObject *>() << Q_FUNC_INFO << "unsupported target" << QuickOpenGL::GLenum(m_target);
                break;

            }

            size*=QuickOpenGL::internalFormatSize(m_format);

            if(buffer.size()!=size)
            {
                qWarning() << "QuickOpenGL:" << this->property("QuickTexture").value<QObject *>() << "attempting to initialize texture MIP level " << level <<  "of size" << size << "bytes with data of size" << buffer.count() << "bytes";
            }
            else
            {
                QOpenGLPixelTransferOptions options;
                options.setAlignment(1);
                if((m_target == GL_TEXTURE_1D_ARRAY) || (m_target == GL_TEXTURE_2D_ARRAY))
                {
                    m_texture->setData(level, 0, m_layers, QOpenGLTexture::CubeMapFace::CubeMapPositiveX, QOpenGLTexture::PixelFormat(QuickOpenGL::baseInternalFormat(m_format)), QOpenGLTexture::PixelType(QuickOpenGL::internalFormatToPixelType(m_format)), buffer.constData(), &options);
                }
                else
                {
                    m_texture->setData(level, QOpenGLTexture::PixelFormat(QuickOpenGL::baseInternalFormat(m_format)), QOpenGLTexture::PixelType(QuickOpenGL::internalFormatToPixelType(m_format)), buffer.constData(), &options);debugGL;
                }
            }
        }
    }

    m_textureWritten = true;
}

void QuickSGTexture::setFilename(const QString &filename)
{
    // future capability - load textures from file
    Q_UNUSED(filename);

    if(m_texture)
    {
        m_texture->destroy();
        delete m_texture;
    }
    //    m_texture = loadDds(filename);
    m_update = false;
}

void QuickSGTexture::onBeforeSynchronizing()
{
    m_textureWritten = false;
}

void QuickSGTexture::setImageLayer(int imageLayer)
{
    if (m_imageLayer == imageLayer)
        return;

    m_imageLayer = imageLayer;
    emit imageLayerChanged(imageLayer);
}

void QuickSGTexture::setImageAccess(QuickTexture::Access imageAccess)
{
    if (m_imageAccess == imageAccess)
        return;

    m_imageAccess = imageAccess;
    emit imageAccessChanged(imageAccess);
}

void QuickSGTexture::setWrapS(QuickTexture::Wrap wrapS)
{
    if (m_wrapS == wrapS)
        return;

    m_wrapS = wrapS;
    emit wrapSChanged(wrapS);
}

void QuickSGTexture::setMipLevels(int mipLevels)
{
    if (m_mipLevels == mipLevels)
        return;

    m_mipLevels = mipLevels;
    emit mipLevelsChanged(mipLevels);
}

void QuickSGTexture::setWrapT(QuickTexture::Wrap wrapT)
{
    if (m_wrapT == wrapT)
        return;

    m_wrapT = wrapT;
    emit wrapTChanged(wrapT);
}

void QuickSGTexture::setWrapR(QuickTexture::Wrap wrapR)
{
    if (m_wrapR == wrapR)
        return;

    m_wrapR = wrapR;
    emit wrapRChanged(wrapR);
}

void QuickSGTexture::setMagFilter(QuickTexture::Filter magFilter)
{
    if (m_magFilter == magFilter)
        return;

    m_magFilter = magFilter;
    emit magFilterChanged(magFilter);
}

void QuickSGTexture::setMinFilter(QuickTexture::Filter minFilter)
{
    if (m_minFilter == minFilter)
        return;

    m_minFilter = minFilter;
    emit minFilterChanged(minFilter);
}

void QuickSGTexture::setLodBias(double lodBias)
{
    if (m_lodBias == lodBias)
        return;

    m_lodBias = lodBias;
    emit lodBiasChanged(lodBias);
}

void QuickSGTexture::setMaxLod(double maxLodBias)
{
    if (m_maxLod == maxLodBias)
        return;

    m_maxLod = maxLodBias;
    emit maxLodChanged(maxLodBias);
}

void QuickSGTexture::textureCreated()
{
    m_texture = watcher.future().resultAt(0);
}

void QuickSGTexture::setMinLod(double minLod)
{
    if (m_minLod == minLod)
        return;

    m_minLod = minLod;
    emit minLodChanged(m_minLod);
}

void QuickSGTexture::setBorderColor(QColor borderColor)
{
    if (m_borderColor == borderColor)
        return;

    m_borderColor = borderColor;
    emit borderColorChanged(m_borderColor);
}

void QuickSGTexture::setImageLevel(int imageLevel)
{
    if (m_imageLevel == imageLevel)
        return;

    m_imageLevel = imageLevel;
    emit imageLevelChanged(m_imageLevel);
}

void QuickSGTexture::setZeroTexture(bool zeroTexture)
{
    if (m_zeroTexture == zeroTexture)
        return;

    m_zeroTexture = zeroTexture;
    emit zeroTextureChanged(zeroTexture);
}

void QuickSGTexture::update()
{
    m_update = true;
}

QuickSGTexture::QuickSGTexture() :
    m_texture(nullptr),
    m_target(QuickTexture::Target2D),
    m_format(QuickTexture::FormatNone),
    m_width(0),
    m_height(0),
    m_depth(0),
    m_layers(0),
    m_update(true),
    m_imageLayer(-1),
    m_imageLevel(0),
    m_imageAccess(QuickTexture::AccessWriteOnly),
    m_mipLevels(0),
    m_wrapS(QuickTexture::WrapRepeat),
    m_wrapT(QuickTexture::WrapRepeat),
    m_wrapR(QuickTexture::WrapRepeat),
    m_magFilter(QuickTexture::FilterLinear),
    m_minFilter(QuickTexture::FilterNearestMipMapLinear),
    m_lodBias(0.0),
    m_minLod(-1000.0),
    m_maxLod(1000.0),
    m_zeroTexture(true),
    m_textureWritten(false)
{

    connect(this, &QuickSGTexture::targetChanged, this, &QuickSGTexture::update);
    connect(this, &QuickSGTexture::formatChanged, this, &QuickSGTexture::update);
    connect(this, &QuickSGTexture::widthChanged, this, &QuickSGTexture::update);
    connect(this, &QuickSGTexture::heightChanged, this, &QuickSGTexture::update);
    connect(this, &QuickSGTexture::depthChanged, this, &QuickSGTexture::update);
    connect(this, &QuickSGTexture::layersChanged, this, &QuickSGTexture::update);
    connect(this, &QuickSGTexture::mipLevelsChanged, this, &QuickSGTexture::update);

    connect(&watcher, &QFutureWatcher<QOpenGLTexture *>::finished, this, &QuickSGTexture::textureCreated);

}

QuickSGTexture::~QuickSGTexture()
{
    if(m_texture)
    {
        delete m_texture; debugGL;
    }
}

int QuickSGTexture::textureId() const
{
    if(m_update)
    {
        QuickSGTexture *d = const_cast<QuickSGTexture *>(this);
        d->updateTexture();
    }

    return m_texture?m_texture->textureId():0;
}

QSize QuickSGTexture::textureSize() const
{
    if(m_update)
    {
        QuickSGTexture *d = const_cast<QuickSGTexture *>(this);
        d->updateTexture();
    }

    return m_texture?QSize(m_texture->width(), m_texture->height()):QSize(0,0);

}

bool QuickSGTexture::hasAlphaChannel() const
{
    return false;
}

bool QuickSGTexture::hasMipmaps() const
{
    return false;
}

void QuickSGTexture::bind()
{
    if(m_update)
    {
        updateTexture();
    }

    if(m_texture)
    {
        m_texture->bind();

        QOpenGLExtraFunctions *glExtra = QOpenGLContext::currentContext()->extraFunctions();
        Q_ASSERT(glExtra);
        glExtra->initializeOpenGLFunctions();

        // determine if the texture is an immutable format
        GLint textureImmutableFormat = 0;
        glExtra->glGetTexParameteriv(m_target, GL_TEXTURE_IMMUTABLE_FORMAT, &textureImmutableFormat);debugGL;

        // if the texture is not immutable, update properties
        if(!textureImmutableFormat)
        {
            m_texture->setMinMagFilters(QOpenGLTexture::Filter(m_minFilter), QOpenGLTexture::Filter(m_magFilter));debugGL;
            m_texture->setLevelofDetailBias(float(m_lodBias));debugGL;
            m_texture->setLevelOfDetailRange(float(m_minLod), float(m_maxLod));debugGL;
            m_texture->setBorderColor(m_borderColor);debugGL;

            switch(m_target)
            {
            case QuickTexture::Target1D:
            case QuickTexture::Target1DArray:
                m_texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::WrapMode(m_wrapS));debugGL;
                break;
            case QuickTexture::Target2D:
            case QuickTexture::Target2DArray:
            case QuickTexture::TargetRectangle:
            case QuickTexture::TargetCubeMap:
            case QuickTexture::TargetCubeMapArray:
            case QuickTexture::Target2DMultisample:
            case QuickTexture::Target2DMultisampleArray:
                m_texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::WrapMode(m_wrapS));debugGL;
                m_texture->setWrapMode(QOpenGLTexture::DirectionT, QOpenGLTexture::WrapMode(m_wrapT));debugGL;
                break;
            case QuickTexture::Target3D:
                m_texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::WrapMode(m_wrapS));debugGL;
                m_texture->setWrapMode(QOpenGLTexture::DirectionT, QOpenGLTexture::WrapMode(m_wrapT));debugGL;
                m_texture->setWrapMode(QOpenGLTexture::DirectionR, QOpenGLTexture::WrapMode(m_wrapR));debugGL;
                break;
            }
        }
    }
}

QuickTexture::Target QuickSGTexture::target() const
{
    return m_target;
}

QuickTexture::Format QuickSGTexture::format() const
{
    return m_format;
}

int QuickSGTexture::width() const
{
    return m_width;
}

int QuickSGTexture::height() const
{
    return m_height;
}

int QuickSGTexture::depth() const
{
    return m_depth;
}

int QuickSGTexture::layers() const
{
    return m_layers;
}

void QuickSGTexture::setTarget(QuickTexture::Target target)
{
    if (m_target == target)
        return;

    m_target = target;
    emit targetChanged(target);
}

void QuickSGTexture::setFormat(QuickTexture::Format format)
{
    if (m_format == format)
        return;

    m_format = format;
    emit formatChanged(format);
}

void QuickSGTexture::setWidth(int width)
{
    if (m_width == width)
        return;

    m_width = width;
    emit widthChanged(width);
}

void QuickSGTexture::setHeight(int height)
{
    if (m_height == height)
        return;

    m_height = height;
    emit heightChanged(height);
}

void QuickSGTexture::setDepth(int depth)
{
    if (m_depth == depth)
        return;

    m_depth = depth;
    emit depthChanged(depth);
}

void QuickSGTexture::setLayers(int layers)
{
    if (m_layers == layers)
        return;

    m_layers = layers;
    emit layersChanged(layers);
}

void QuickSGTexture::updateTexture()
{
    if(m_texture)
    {
        m_texture->destroy();debugGL;
        delete m_texture;debugGL;
        m_texture = nullptr;
    }

    if(m_width&&m_height)
    {
        m_texture = new QOpenGLTexture(QOpenGLTexture::Target(m_target));debugGL;
        m_texture->setFormat(QOpenGLTexture::TextureFormat(m_format));debugGL;
        m_texture->setAutoMipMapGenerationEnabled(false);debugGL;
        if((m_target==QuickTexture::Target1DArray)||(m_target == QuickTexture::Target2DArray)||(m_target==QuickTexture::TargetCubeMapArray)||(m_target==QuickTexture::Target2DMultisampleArray))
        {
            m_texture->setLayers(m_layers);debugGL;
        }
        m_texture->setSize(m_width, m_height, m_depth);debugGL;
        m_texture->setMipLevels(m_mipLevels);debugGL;
        switch(QOpenGLTexture::Target(m_target))
        {
        case QOpenGLTexture::Target1D:
        case QOpenGLTexture::Target1DArray:
            m_texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::WrapMode(m_wrapS));debugGL;
            break;
        case QOpenGLTexture::Target2D:
        case QOpenGLTexture::Target2DArray:
        case QOpenGLTexture::TargetRectangle:
        case QOpenGLTexture::TargetCubeMap:
        case QOpenGLTexture::TargetCubeMapArray:
        case QOpenGLTexture::Target2DMultisample:
        case QOpenGLTexture::Target2DMultisampleArray:
            m_texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::WrapMode(m_wrapS));debugGL;
            m_texture->setWrapMode(QOpenGLTexture::DirectionT, QOpenGLTexture::WrapMode(m_wrapT));debugGL;
            break;
        case QOpenGLTexture::Target3D:
            m_texture->setWrapMode(QOpenGLTexture::DirectionS, QOpenGLTexture::WrapMode(m_wrapS));debugGL;
            m_texture->setWrapMode(QOpenGLTexture::DirectionT, QOpenGLTexture::WrapMode(m_wrapT));debugGL;
            m_texture->setWrapMode(QOpenGLTexture::DirectionR, QOpenGLTexture::WrapMode(m_wrapR));debugGL;
            break;
        default:
            break;
        }
        m_texture->setMinificationFilter(QOpenGLTexture::Filter(m_minFilter));debugGL;
        m_texture->setMagnificationFilter(QOpenGLTexture::Filter(m_magFilter));debugGL;
        m_texture->setLevelofDetailBias(float(m_lodBias));debugGL;
        m_texture->setLevelOfDetailRange(float(m_minLod), float(m_maxLod));debugGL;
        m_texture->setBorderColor(m_borderColor);debugGL;

#ifdef QT_DEBUG
        QScopedPointer<QElapsedTimer> timer;

        if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
        {
            timer.reset(new QElapsedTimer());
            timer->start();
        }
#endif

        m_texture->allocateStorage();debugGL;

#ifdef QT_DEBUG
        qint64 allocate = 0;
        if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
        {
            allocate = timer->nsecsElapsed();
        }
#endif

#ifndef QT_OPENGL_ES
        QOpenGLFunctions_4_4_Core *gl44 = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_4_4_Core>();
        if(m_zeroTexture&&gl44&&gl44->initializeOpenGLFunctions())
        {
            int format;
            switch(QOpenGLTexture::TextureFormat(m_format))
            {
            case QOpenGLTexture::R16F:
            case QOpenGLTexture::RG16F:
            case QOpenGLTexture::RGB16F:
            case QOpenGLTexture::RGBA16F:
            case QOpenGLTexture::R32F:
            case QOpenGLTexture::RG32F:
            case QOpenGLTexture::RGB32F:
            case QOpenGLTexture::RGBA32F:
                format = GL_RGBA;
                break;
            case QOpenGLTexture::R8I:
            case QOpenGLTexture::RG8I:
            case QOpenGLTexture::RGB8I:
            case QOpenGLTexture::RGBA8I:
            case QOpenGLTexture::R16I:
            case QOpenGLTexture::RG16I:
            case QOpenGLTexture::RGB16I:
            case QOpenGLTexture::RGBA16I:
            case QOpenGLTexture::R32I:
            case QOpenGLTexture::RG32I:
            case QOpenGLTexture::RGB32I:
            case QOpenGLTexture::RGBA32I:
                format = GL_RGBA_INTEGER;
                break;
            default:
                format = GL_NONE;
                break;
            }
            for(int i=0;i<m_mipLevels;++i)
            {
                gl44->glClearTexImage(m_texture->textureId(), i, format, GL_UNSIGNED_BYTE, nullptr);debugGL;
            }
        }
#endif

#ifdef QT_DEBUG

        if(QuickOpenGL::debugLevel()&DEBUG_TIMING)
        {

            qint64 zero = timer->nsecsElapsed()-allocate;

            int bytesPerTexel = 0;
            switch(QOpenGLTexture::TextureFormat(m_format))
            {

            case QOpenGLTexture::R8I:
                bytesPerTexel=1;
                break;
            case QOpenGLTexture::R16F:
            case QOpenGLTexture::RG8I:
            case QOpenGLTexture::R16I:
                bytesPerTexel=2;
                break;
            case QOpenGLTexture::RGB8I:
                bytesPerTexel =3;
                break;
            case QOpenGLTexture::RG16F:
            case QOpenGLTexture::RG16I:
            case QOpenGLTexture::R32F:
            case QOpenGLTexture::RGBA8I:
            case QOpenGLTexture::R32I:
                bytesPerTexel=4;
                break;
            case QOpenGLTexture::RGB16F:
            case QOpenGLTexture::RGB16I:
                bytesPerTexel=6;
                break;
            case QOpenGLTexture::RGBA16F:
            case QOpenGLTexture::RGBA16I:
            case QOpenGLTexture::RG32F:
            case QOpenGLTexture::RG32I:
                bytesPerTexel=8;
                break;
            case QOpenGLTexture::RGB32F:
            case QOpenGLTexture::RGB32I:
                bytesPerTexel = 12;
                break;
            case QOpenGLTexture::RGBA32F:
            case QOpenGLTexture::RGBA32I:
                bytesPerTexel = 16;
                break;
            default:
                break;
            }


            int baseSize = (m_texture->width()*m_texture->height() *((m_texture->target()==QOpenGLTexture::Target1DArray||m_texture->target()==QOpenGLTexture::Target2DArray)?m_texture->layers():m_texture->depth())*bytesPerTexel) ;

            int size = baseSize;
            for(int i=1;i<m_texture->mipLevels();++i)
            {
                int width = std::max(1, m_texture->width()/(1<<i));
                int height = std::max(1, m_texture->height()/(1<<i));

                size += width*height*m_texture->layers()*bytesPerTexel;

            }


            qDebug()  << "QuickOpenGL:" << this->property("QuickTexture").value<QObject *>() << "allocating id" << m_texture->textureId() << "target" << QuickOpenGL::GLenum(m_texture->target()) << "format" << QuickOpenGL::GLenum(m_texture->format()) << "size" << m_texture->width() << "x" << m_texture->height() << "x" << ((m_texture->target()==QOpenGLTexture::Target1DArray||m_texture->target()==QOpenGLTexture::Target2DArray)?m_texture->layers():m_texture->depth()) << "with" << m_texture->mipLevels() << "MIP levels =" << size*1e-6 << "MBytes took" << double(allocate)*1e-6 << (m_zeroTexture?qPrintable(QString("ms, zeroing took %1 ms").arg(double(zero)*1e-6)):"ms");

        }

#endif

        m_update = false;
    }



}

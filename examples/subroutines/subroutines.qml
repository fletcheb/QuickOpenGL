import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 1.4
import QuickOpenGL 1.0

Window
{
    id: root
    visible: true
    width: 640
    height: 480
    title: "Subroutines"
    color: 'black'

    //![1]
    function vertex2DColor(x, y, color)
    {
        return {vertexPosition: Qt.vector2d(x,y), vertexColor: color}
    }
    //![1]

    //![3]
    GraphicsProgram
    {
        id: program
        anchors.fill: parent

        //![0]
        vertices : [vertex2DColor(10, height-10, '#ff0000'),
                    vertex2DColor(width-10, height-10, '#00ff00'),
                    vertex2DColor(width/2, 10, '#0000ff')]
        //![0]

        property string swizzle: slider.textValue

        onSubroutineUniformsChanged: console.log(this, '\nsubroutineUniforms', JSON.stringify(subroutineUniforms,null,'    '))

        //![2]
        vertexShader: "
            #version 400

            in vec2 vertexPosition;
            in vec3 vertexColor;

            out vec3 color;

            uniform mat4 qt_Matrix;

            subroutine vec3 colorize(vec3 value);

            subroutine (colorize) vec3 swizzleRGB(vec3 value)
            {
                return value.rgb;
            }

            subroutine (colorize) vec3 swizzleRBG(vec3 value)
            {
                return value.rbg;
            }

            subroutine (colorize) vec3 swizzleGBR(vec3 value)
            {
                return value.gbr;
            }

            subroutine (colorize) vec3 swizzleGRB(vec3 value)
            {
                return value.grb;
            }

            subroutine (colorize) vec3 swizzleBGR(vec3 value)
            {
                return value.bgr;
            }

            subroutine (colorize) vec3 swizzleBRG(vec3 value)
            {
                return value.brg;
            }

            subroutine uniform colorize swizzle;

            void main()
            {
                color = swizzle(vertexColor);
                gl_Position = qt_Matrix*vec4(vertexPosition, 0.0, 1.0);
            }"
        //![2]

        fragmentShader: "
            #version 400

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }"

    }
    //![3]

    Text
    {
        color: 'white'
        text: 'subroutine: ' + slider.textValue
    }

    //![4]
    Slider
    {
        id: slider
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        minimumValue: 0
        maximumValue: names.length-1
        stepSize: 1
        value: 0

        property var names: ['swizzleRGB', 'swizzleRBG', 'swizzleBRG', 'swizzleBGR', 'swizzleGRB', 'swizzleGBR']
        property string textValue: names[value]
    }
    //![4]

}

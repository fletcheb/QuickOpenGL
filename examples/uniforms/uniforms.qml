import QtQuick 2.9
import QtQuick.Window 2.3
import QuickOpenGL 1.0

Window {

    visible: true
    color: 'black'
    width: 640
    height: 480
    title: "Uniforms"

    //
    // Convenience function to enumerate object names and values
    //
    function enumerateObjectTree(object, name)
    {
        var result = ''
        if(typeof object === 'object')
        {
            for(var key in object)
            {
                result += enumerateObjectTree(object[key], (name===undefined?'':(name+'.'))+key)
            }
        }
        else
        {
            result = name + ': ' + object + '\n'
        }
        return result
    }

    Image
    {
        id: opengl
        source: 'opengl.jpg'
        visible: false
    }

    Image
    {
        id: qt
        source: 'qt.png'
        visible: false
    }

    Timer
    {
        interval: 500
        repeat: true
        running: true
        onTriggered:
        {
            triangle.index = triangle.index+1
        }
    }



    //
    // Graphics program to demonstrate passing uniforms
    //
    // It may be convenient to work within the QML scenegraph projection.  This can be done quite
    // easily using the qt_Matrix combined transformation matrix uniform, in the same manner as
    // the Quick2 ShaderEffect QML Type.
    //
    // In addition any property of GraphicsProgram that can be mapped to an OpenGL type is made
    // available as a uniform, including arrays of values. These properties include properties of
    // QQuickItem such as height and width.  OpenGL uniform blocks are not currently supported.
    //

    //![0]
    GraphicsProgram
    {
        id: triangle
        objectName: "Triangle"
        anchors.fill: parent

        vertices : [{vertexPosition: Qt.vector2d(10, height-10)},
                    {vertexPosition: Qt.vector2d(width-10, height-10)},
                    {vertexPosition: Qt.vector2d(width/2, 10)}]

        onUniformsChanged: console.log(this, 'Active uniforms:\n' + enumerateObjectTree(uniforms))

        property int index: 0
        property var vertexColor: ['#ff0000', '#00ff00', '#0000ff']

        vertexShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'410') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec2 vertexPosition;
            out vec3 color;

            uniform mat4 qt_Matrix;
            uniform vec3 vertexColor[3];
            uniform int index;

            void main()
            {
                color = vertexColor[(index+gl_VertexID)%3];
                gl_Position = qt_Matrix*vec4(vertexPosition, 0.0, 1.0);
            }'


        fragmentShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'410') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }'

    }
    //![0]

    Timer
    {
        running: true
        interval: 10000
        repeat: true
        onTriggered: image.index = (image.index+1)%image.sampler.length
    }

    //
    // Graphics program to demonstrate passing uniforms
    //
    // Any QQuickItem which is a texture provider may be used as a uniform sampler.  Unlike
    // ShaderEffect, GraphicsProgram supports samplers other than sampler2D.  Opacity is supported
    // through the qt_Opacity uniform.
    //
    //![1]
    GraphicsProgram
    {
        id: image
        objectName: "Image"
        anchors.fill: parent

        vertices: [{position: Qt.vector2d(0,1), texCoord: Qt.vector2d(0,1)},
                   {position: Qt.vector2d(1,1), texCoord: Qt.vector2d(1,1)},
                   {position: Qt.vector2d(0,0), texCoord: Qt.vector2d(0,0)},
                   {position: Qt.vector2d(1,0), texCoord: Qt.vector2d(1,0)}]

        onUniformsChanged: console.log(this, 'Active uniforms:\n' + enumerateObjectTree(uniforms))

        property var sampler: [opengl, qt]
        property int index: 0

        SequentialAnimation on opacity {
            running: true;
            loops: Animation.Infinite;
            NumberAnimation{from: 0; to: 1; duration: 5000}
            NumberAnimation{from: 1; to: 0; duration: 5000}
        }

        NumberAnimation on rotation {
            running: true;
            loops: Animation.Infinite;
            from: 0;
            to:360;
            duration: 5000
        }        

        vertexShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'410') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec2 position;
            in vec2 texCoord;

            out vec2 coord;

            uniform mat4 qt_Matrix;
            uniform float width;
            uniform float height;

            void main()
            {
                gl_Position = qt_Matrix*vec4(vec2(width, height)*0.2 + position*vec2(width,height)*0.6, 0.0, 1.0);
                coord = texCoord;
            }'


        fragmentShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'410') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec2 coord;

            out vec4 fragColor;

            uniform sampler2D sampler[2];
            uniform float qt_Opacity;
            uniform int index;

            void main()
            {                
                switch(index)
                {
                    case 0:
                        fragColor = texture(sampler[0], coord)*qt_Opacity;
                        break;
                    case 1:
                        fragColor = texture(sampler[1], coord)*qt_Opacity;
                        break;
                }

            }'

    }
    //![1]

}



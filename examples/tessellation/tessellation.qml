import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import QuickOpenGL 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: "tessellation"

    color: 'black'

    function vertex2D(x, y)
    {
        return {vertexPosition: Qt.vector2d(x,y)}
    }

    SplitView
    {
        anchors.fill: parent
        orientation: Qt.Vertical

        SplitView
        {
            Layout.fillHeight: true
            orientation: Qt.Horizontal

            //![0]
            GraphicsProgram
            {
                id: triangles
                objectName: 'triangles'
                width: 266
                Layout.fillWidth: true

                vertices : [vertex2D(10, height-10),
                            vertex2D(width-10, height-10),
                            vertex2D(width/2, 10)]

                polygonMode: GraphicsProgram.PolygonModeLine
                drawingMode: GraphicsProgram.DrawingModePatches

                patchVertices: 3
                patchDefaultOuterLevel: [outerLevel0.value, outerLevel1.value, outerLevel2.value, outerLevel3.value]
                patchDefaultInnerLevel: [innerLevel0.value, innerLevel1.value]

                vertexShader: "
                    #version 400
                    in vec2 vertexPosition;

                    void main()
                    {
                        gl_Position = vec4( vertexPosition, 0.0, 1.0 );
                    }"

                tessellationEvaluationShader: "
                    #version 400

                    layout( triangles, fractional_even_spacing, ccw ) in;

                    uniform mat4 qt_Matrix;

                    void main()
                    {
                        float u = gl_TessCoord.x;
                        float v = gl_TessCoord.y;
                        float w = gl_TessCoord.z;

                        vec4 p0 = gl_in[0].gl_Position;
                        vec4 p1 = gl_in[1].gl_Position;
                        vec4 p2 = gl_in[2].gl_Position;

                        // Linearly interpolate to the vertex position using the
                        // (u,v) barycentric coords
                        vec4 pos = (u * p0)
                                 + (v * p1)
                                 + (w * p2);

                        // Transform to clip-space
                        gl_Position = qt_Matrix * pos;
                    }"

                fragmentShader: "
                    #version 400

                    out vec4 fragColor;

                    void main()
                    {
                        fragColor = vec4(1.0);
                    }"
            }
            //![0]

            //![1]
            GraphicsProgram
            {
                id: quads
                objectName: 'quads'
                width: 266

                vertices : [vertex2D(10, height-10),
                            vertex2D(width-10, height-10),
                            vertex2D(width-10, 30),
                            vertex2D(10, 30)]

                polygonMode: GraphicsProgram.PolygonModeLine
                drawingMode: GraphicsProgram.DrawingModePatches

                patchVertices: 4
                patchDefaultOuterLevel: [outerLevel0.value, outerLevel1.value, outerLevel2.value, outerLevel3.value]
                patchDefaultInnerLevel: [innerLevel0.value, innerLevel1.value]

                vertexShader: "
                    #version 400
                    in vec2 vertexPosition;

                    void main()
                    {
                        gl_Position = vec4( vertexPosition, 0.0, 1.0 );
                    }"

                tessellationEvaluationShader: "
                    #version 400

                    layout( quads, fractional_even_spacing, ccw ) in;

                    uniform mat4 qt_Matrix;

                    void main()
                    {
                        float u = gl_TessCoord.x;
                        float v = gl_TessCoord.y;

                        vec4 p00 = gl_in[0].gl_Position;
                        vec4 p10 = gl_in[1].gl_Position;
                        vec4 p11 = gl_in[2].gl_Position;
                        vec4 p01 = gl_in[3].gl_Position;

                        // Linearly interpolate to the vertex position using the
                        // (u,v) barycentric coords
                        vec4 pos = p00 * ( 1.0 - u ) * ( 1.0 - v )
                                 + p10 * u * ( 1.0 - v )
                                 + p01 * v * ( 1.0 - u )
                                 + p11 * u * v;

                        // Transform to clip-space
                        gl_Position = qt_Matrix * pos;
                    }"

                fragmentShader: "
                    #version 400

                    out vec4 fragColor;

                    void main()
                    {
                        fragColor = vec4(1.0);
                    }"
            }
            //![1]

            //![2]
            GraphicsProgram
            {
                id: isolines

                objectName: 'isolines'
                width: 266

                vertices : [vertex2D(10, height-10-sinusoidAmplitude),
                    vertex2D(width-10, height-10-sinusoidAmplitude),
                    vertex2D(width-10, 10+sinusoidAmplitude),
                    vertex2D(10, 10+sinusoidAmplitude)]

                drawingMode: GraphicsProgram.DrawingModePatches

                patchVertices: 4
                patchDefaultOuterLevel: [outerLevel0.value, outerLevel1.value, outerLevel2.value, outerLevel3.value]
                patchDefaultInnerLevel: [innerLevel0.value, innerLevel1.value]

                property real sinusoidAmplitude: 64
                property real sinusoidFrequency: 4

                vertexShader: "
                    #version 400
                    in vec2 vertexPosition;

                    void main()
                    {
                        gl_Position = vec4( vertexPosition, 0.0, 1.0 );
                    }"

                tessellationEvaluationShader: "
                    #version 400

                    layout( isolines, fractional_even_spacing, ccw ) in;

                    uniform mat4 qt_Matrix;

                    uniform float sinusoidAmplitude;
                    uniform float sinusoidFrequency;

                    const float pi = 3.14159265359;

                    void main()
                    {
                        float u = gl_TessCoord.x;
                        float v = gl_TessCoord.y;

                        vec4 a = gl_in[0].gl_Position;
                        vec4 b = gl_in[1].gl_Position;
                        vec4 c = gl_in[2].gl_Position;
                        vec4 d = gl_in[3].gl_Position;

                        // Use the (u,v) parametric coords to calculate the vertex.
                        // u is the position along an isoline
                        // v is isoline number

                        // Let's make a sinusoidal curve as a function of u that varies
                        // in the v direction which we will define as orthogonal to u

                        // Interpolate in u along top and bottom edges of the patch
                        vec4 q0 = mix( a, b, u );
                        vec4 q1 = mix( d, c, u );

                        // Interpolate in v between the above positions. This gives the
                        // nominal position for our vertex
                        vec4 p = mix( q0, q1, v );

                        // Find vertical direction
                        vec4 vBasis = normalize( q1 - q0 );

                        // Offset vertex in this direction using sinusoid
                        vec4 pos = p + sinusoidAmplitude * vBasis * sin( sinusoidFrequency * 2.0 * pi * u );

                        // Transform to clip-space
                        gl_Position = qt_Matrix * pos;
                    }"

                fragmentShader: "
                    #version 400

                    out vec4 fragColor;

                    void main()
                    {
                        fragColor = vec4(1.0);
                    }"
            }
        }
        //![2]

        Item
        {
            height: 150
            GridLayout
            {

                anchors.fill: parent
                columns: 2

                Text
                {
                    color: 'white'
                    text: "Outer Level 0"
                }

                Slider
                {
                    id: outerLevel0
                    minimumValue: 1
                    maximumValue: OpenGLProperties.glMaxTessGenLevel
                    stepSize: 0.1
                    value: 1
                    Layout.fillWidth: true
                }

                Text
                {
                    color: 'white'
                    text: "Outer Level 1"
                }

                Slider
                {
                    id: outerLevel1
                    minimumValue: 1
                    maximumValue: OpenGLProperties.glMaxTessGenLevel
                    stepSize: 0.1
                    value: 1
                    Layout.fillWidth: true

                }

                Text
                {
                    color: 'white'
                    text: "Outer Level 2"
                }

                Slider
                {
                    id: outerLevel2
                    minimumValue: 1
                    maximumValue: OpenGLProperties.glMaxTessGenLevel
                    stepSize: 0.1
                    value: 1
                    Layout.fillWidth: true

                }

                Text
                {
                    color: 'white'
                    text: "Outer Level 3"
                }

                Slider
                {
                    id: outerLevel3
                    minimumValue: 1
                    maximumValue: OpenGLProperties.glMaxTessGenLevel
                    stepSize: 0.1
                    value: 1
                    Layout.fillWidth: true

                }
                Text
                {
                    color: 'white'
                    text: "Inner Level 0"
                }

                Slider
                {
                    id: innerLevel0
                    minimumValue: 1
                    maximumValue: OpenGLProperties.glMaxTessGenLevel
                    stepSize: 0.1
                    value: 1
                    Layout.fillWidth: true

                }

                Text
                {
                    color: 'white'
                    text: "Inner Level 1"
                }

                Slider
                {
                    id: innerLevel1
                    minimumValue: 1
                    maximumValue: OpenGLProperties.glMaxTessGenLevel
                    stepSize: 0.1
                    value: 1
                    Layout.fillWidth: true

                }
            }
        }


    }
}

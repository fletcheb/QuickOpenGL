TEMPLATE = app

QT += qml quick
CONFIG += c++11

QML2_IMPORT_PATH += $$OUT_PWD/../../src/
QML_IMPORT_PATH += $$OUT_PWD/../../src/
QMLPATHS += $$OUT_PWD/../../src/

DISTFILES += \
    doc/src/* \
    doc/images/*

HEADERS += ../example.h

VERSION = 1.0.0

android {
    androidmanifestupdate.commands =  sed -i \'\' -E -e \'s/(versionName=)(\"([0-9]\.?)+\")/\\1\"$$VERSION\"/g\' $$ANDROID_PACKAGE_SOURCE_DIR/AndroidManifest.xml
    QMAKE_EXTRA_TARGETS += androidmanifestupdate
    PRE_TARGETDEPS += androidmanifestupdate
}

ios {
    plistupdate.commands = /usr/libexec/PlistBuddy -c \"Set :CFBundleVersion $$VERSION\" $$QMAKE_INFO_PLIST
    QMAKE_EXTRA_TARGETS += plistupdate
    PRE_TARGETDEPS += plistupdate
}

CONFIG += install_ok

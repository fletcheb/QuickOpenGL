import QtQuick 2.10
import QtQuick.Window 2.10
import QuickOpenGL 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Debugging")

    //![0]
    QuickOpenGLDebug
    {
        flags: QuickOpenGLDebug.FlagTiming
    }
    //![0]

    //![1]
    OpenGLDebugLogger
    {
        onMessageLogged: console.log(message)
    }
    //![1]

    //![2]
    GraphicsProgram
    {
        objectName: 'This One!'
        anchors.fill: parent
    }
    //![2]
}

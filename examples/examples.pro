TEMPLATE = subdirs

SUBDIRS += \
    helloworld \
    vertices \
    uniforms \
    mapping

!ios {
    SUBDIRS += \
    subroutines \
    tessellation \
    uniformbuffers \
    samplers

    !macos {
        SUBDIRS += \
        textureviews \
        shaderstoragebuffers \
        compute \
        bindlesstextures \
        bindlessimages \
        debugging
    }
}


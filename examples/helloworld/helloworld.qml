import QtQuick 2.12
import QtQuick.Window 2.12
import QuickOpenGL 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    GraphicsProgram
    {
        anchors.fill: parent
    }

    Text
    {
        anchors.centerIn: parent
        text: qsTr("Hello World")
        font.pointSize: 50
    }
}

import QtQuick 2.9
import QtQuick.Window 2.3
import QuickOpenGL 1.0

Window
{

    visible: true
    color: 'black'
    width: 640
    height: 480

    //![0]
    ShaderStorageBuffer
    {
        id: ssbo
        syncFromOpenGL: true
        usagePattern: ShaderStorageBuffer.DynamicDraw

        property color color
    }
    //![0]

    //![2]
    GraphicsProgram
    {
        anchors.fill: parent

        property var mouse: GL.vec2(mouseArea.mapToItem(null, mouseArea.mouseX, mouseArea.mouseY)).times(Screen.devicePixelRatio)
        property var ssbo: ssbo

        onShaderStorageBlocksChanged: console.log(this, '\nshaderStorageBlocks' + JSON.stringify(shaderStorageBlocks, null, '    '))

        fragmentShader: "
            #version 430

            in vec2 coord;
            layout (origin_upper_left,pixel_center_integer) in vec4 gl_FragCoord;
            out vec4 qt_FragColor;
            uniform vec2 mouse;

            //![1]
            buffer ssbo
            {
                vec4 color;
            };
            //![1]

            void main()
            {
                qt_FragColor = vec4(coord, 0, 1);

                if(all(equal(gl_FragCoord.xy, mouse)))
                {
                    color = qt_FragColor;
                }

            }"
    }
    //![2]

    //![3]
    Text
    {
        text: ssbo.color
        color: ssbo.color
    }
    //![3]

    Text
    {
        anchors.right: parent.right
        text: ssbo.color
        color: ssbo.color
    }

    Text
    {
        anchors.bottom: parent.bottom
        text: ssbo.color
        color: ssbo.color
    }

    Text
    {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        text: ssbo.color
        color: ssbo.color
    }

    Text
    {
        anchors.centerIn: parent
        text: ssbo.color
        color: ssbo.color
    }


    MouseArea
    {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true

    }

}

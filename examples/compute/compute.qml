import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 1.4
import QuickOpenGL 1.0

Window
{
    visible: true
    width: 640
    height: 480
    title: "Compute"


    //![0]
    Texture
    {
        id: texture
        target: Texture.Target2D
        format: Texture.FormatR32F
        width: parent.width
        height: parent.height
    }
    //![0]   

    //![1]
    ComputeProgram
    {
        xGroups: texture.width
        yGroups: texture.height

        property var image : texture
        property real frequency: frequencySlider.value        

        computeShader: "
                #version 430 core

                layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

                layout(r32f) uniform image2D image;
                uniform float frequency;
                void main()
                {
                    ivec2 size = imageSize(image);
                    vec2 xy = vec2(gl_GlobalInvocationID.xy) - vec2(size-ivec2(1))*0.5;
                    float value = (1.0+(sin(0.5*radians(180.0)*frequency*dot(xy,xy)/float(size.y))))*0.5;
                    imageStore(image, ivec2(gl_GlobalInvocationID.xy), vec4(value));
                }
            "

        onDispatched: console.log(this + ' dispatched with state:\n' + printState(state))
    }
    //![1]

    //![3]
    function printState(state)
    {
        var result = '';
        for(var object in state)
        {
            result +=  '    ' + object + ':\t' + state[object] + '\n'
        }

        return result
    }
    //![3]

    //![2]
    GraphicsProgram
    {
        anchors.fill: parent
        property var sampler: texture
        memoryBarrier: GraphicsProgram.BarrierTextureFetch
        fragmentShader: "
           #version 330

           in vec2 coord;

           uniform float qt_Opacity;
           uniform sampler2D sampler;
           out vec4 fragColor;
           void main()
           {
               float value = texture(sampler, coord, 0).r;
               fragColor = vec4(vec3(value), 1.0) * qt_Opacity;
           }"
    }
    //![2]

    Slider
    {
        id: frequencySlider
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        minimumValue: 0.0
        maximumValue: 10.0
        value: 1.0
    }

}

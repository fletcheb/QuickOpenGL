#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QSurfaceFormat>
#include <QOpenGLContext>
#include <QOpenGLDebugLogger>
#include <QOffscreenSurface>
#include <QDebug>
#include <QtPlugin>

#ifdef QT_OPENGL_ES
#define QUICKOPENGL_EXAMPLE_MAIN(NAME, OPENGL_VERSION, OPENGL_EXTENSIONS, OPENGL_PROFILE) int main(int argc, char *argv[]) \
{ \
    QGuiApplication app(argc, argv); \
    QQmlApplicationEngine engine; \
    engine.load(QUrl(QStringLiteral("qrc:/")+QString(NAME))); \
    if(!engine.rootObjects().isEmpty()) { \
        QQuickWindow *window = qobject_cast<QQuickWindow *>(engine.rootObjects().first()); \
        if(window) \
        { \
            QOpenGLDebugLogger *logger = new QOpenGLDebugLogger(); \
            QObject::connect(window, &QQuickWindow::sceneGraphInitialized, [logger](){logger->initialize();logger->startLogging();}); \
            QObject::connect(window, &QQuickWindow::sceneGraphInvalidated, [logger](){logger->stopLogging();delete logger;}); \
            QObject::connect(logger, &QOpenGLDebugLogger::messageLogged, [](const QOpenGLDebugMessage &debugMessage){if(debugMessage.severity()&0x7) qDebug() << debugMessage;}); \
        } \
        return app.exec(); \
    } \
    return -1; \
}
#else
#define QUICKOPENGL_EXAMPLE_MAIN(NAME, OPENGL_VERSION, OPENGL_EXTENSIONS, OPENGL_PROFILE) int main(int argc, char *argv[]) \
{ \
    QSurfaceFormat format = QSurfaceFormat::defaultFormat(); \
    format.setOptions(QSurfaceFormat::DebugContext); \
    format.setMajorVersion(OPENGL_VERSION/100); \
    format.setMinorVersion((OPENGL_VERSION/10)%10); \
    format.setProfile(OPENGL_PROFILE); \
    QSurfaceFormat::setDefaultFormat(format); \
    QGuiApplication app(argc, argv); \
    QQmlApplicationEngine engine; \
    { \
        QOpenGLContext context; \
        context.create(); \
        QOffscreenSurface surface(QGuiApplication::primaryScreen()); \
        surface.create(); \
        context.makeCurrent(&surface); \
        if((context.format().majorVersion()*100+context.format().minorVersion()*10)<OPENGL_VERSION) \
        { \
            qDebug().nospace() << "This example requires OpenGL " << OPENGL_VERSION << ". This platform only supports OpenGL " << context.format().majorVersion() << context.format().minorVersion()*10 << "."; \
            return -1; \
        } \
        QStringList extensions(OPENGL_EXTENSIONS); \
        for(const QString &extension: extensions) \
        { \
            if(!context.hasExtension(extension.toLocal8Bit())) \
            { \
                qDebug().nospace() << "This example requies OpenGL extension " << extension << ", which is not supported on this platform"; \
                return -1; \
            } \
        } \
    }\
    engine.addImportPath("../../src/"); \
    engine.addImportPath("../../../../../src/"); \
    engine.load(QUrl(QStringLiteral("qrc:/")+QString(NAME))); \
    if(!engine.rootObjects().isEmpty()) { \
        QQuickWindow *window = qobject_cast<QQuickWindow *>(engine.rootObjects().first()); \
        if(window) \
        { \
            QOpenGLDebugLogger *logger = new QOpenGLDebugLogger(); \
            QObject::connect(window, &QQuickWindow::sceneGraphInitialized, [logger](){logger->initialize();logger->startLogging();}); \
            QObject::connect(window, &QQuickWindow::sceneGraphInvalidated, [logger](){logger->stopLogging();delete logger;}); \
            QObject::connect(logger, &QOpenGLDebugLogger::messageLogged, [](const QOpenGLDebugMessage &debugMessage){if(debugMessage.severity()&0x7) qDebug() << debugMessage;}); \
        } \
        return app.exec(); \
    } \
    return -1; \
}
#endif

import QtQuick 2.10
import QtQuick.Window 2.10
import QuickOpenGL 1.0

Window
{
    visible: true
    color: 'black'
    width: 640
    height: 480
    title: "Sampler Object"

    //![0]
    Image
    {
        id: image
        source: 'opengl.jpg'
        mipmap: true
        visible: false
    }
    //![0]

    //![1]
    Sampler
    {
        id: sampler
        source: image
        magFilter: Sampler.FilterNearest
        minFilter: Sampler.FilterNearest
    }
    //![1]


    //![2]
    Timer
    {
        interval: 100
        repeat: true
        running: true
        property var wraps: [Sampler.WrapClampToBorder, Sampler.WrapClampToEdge, Sampler.WrapMirroredRepeat, Sampler.WrapRepeat]
        onTriggered:
        {
            sampler.wrapS = wraps[((Math.random()*wraps.length).toFixed())%wraps.length]
            sampler.wrapT = wraps[((Math.random()*wraps.length).toFixed())%wraps.length]
            sampler.borderColor = Qt.rgba(Math.random(),Math.random(),Math.random(),1)
        }

    }
    //![2]

    //![3]
    GraphicsProgram
    {

        x:0
        y:0
        width: parent.width/2
        height: parent.height/2

        property var tex: sampler

        fragmentShader: "
            #version 330

            in vec2 coord;

            out vec4 qt_FragColor;

            uniform sampler2D tex;
            uniform float qt_Opacity;

            void main()
            {
                qt_FragColor = texture(tex, -1+coord*3)*qt_Opacity;
            }"

    }

    GraphicsProgram
    {

        x: parent.width/2
        y: parent.height/2
        width: parent.width/2
        height: parent.height/2

        property var tex: image

        fragmentShader: "
            #version 330

            in vec2 coord;

            out vec4 qt_FragColor;

            uniform sampler2D tex;
            uniform float qt_Opacity;

            void main()
            {
                qt_FragColor = texture(tex, -1+coord*3)*qt_Opacity;
            }"

    }
    //![3]
}

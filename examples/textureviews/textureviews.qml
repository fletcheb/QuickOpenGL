import QtQuick 2.6
import QtQuick.Window 2.2
import QuickOpenGL 1.0

Window
{
    visible: true
    color: 'black'
    width: 640
    height: 480
    title: "Texture View"

    //![0]
    // Texture with mip levels
    Texture
    {
        id: imageTexture
        target: Texture.Target2D
        format: Texture.FormatRGBA32F
        width: 16
        height: 16
        mipLevels: 5
        minFilter: Texture.FilterNearestMipMapNearest
        magFilter: Texture.FilterNearest
    }



    // list of image handles
    property var imageHandles: []

    // create an image handle for each texture mip level
    Repeater
    {
        id: imageRepeater
        model: imageTexture.mipLevels

        ImageHandle
        {
            source: imageTexture
            level: index
            layered: false
            format: imageTexture.format
            access: ImageHandle.AccessWriteOnly
        }

        onItemAdded: {imageHandles.push(item); imageHandles = imageHandles}
    }

    // write a checkerboard to each mip layer
    ComputeProgram
    {
        xGroups: imageTexture.width
        yGroups: imageTexture.height
        zGroups: outputImage.length

        property var outputImage : imageHandles

        computeShader: "
                #version 430 core
                #extension GL_ARB_bindless_texture : require

                layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

                layout(bindless_image, rgba32f) uniform image2D outputImage[5];

                void main(void)
                {

                    vec4 value;
                    switch(gl_GlobalInvocationID.z%3)
                    {
                        case 0:
                            value = vec4(1,0,0,1);
                            break;
                        case 1:
                            value = vec4(0,1,0,1);
                            break;
                        case 2:
                            value = vec4(0,0,1,1);
                            break;
                    }

                    if(all(lessThan(gl_GlobalInvocationID.xy,imageSize(outputImage[gl_GlobalInvocationID.z]))))
                    {
                        bool on = (gl_GlobalInvocationID.x+gl_GlobalInvocationID.y)%2==0;

                        imageStore(outputImage[gl_GlobalInvocationID.z], ivec2(gl_GlobalInvocationID.xy), on?value:vec4(0));
                    }

                }
            "
    }
    //![0]


    //![1]
    TextureView
    {
        id: mip0
        source: imageTexture
        format: TextureView.FormatRGBA32U
        target: TextureView.Target2D
        minimumLevel:0
        levels:1
    }

    TextureView
    {
        id: mip2
        source: imageTexture
        format: TextureView.FormatRGBA32I
        target: TextureView.Target2D
        minimumLevel:2
        levels:2
    }
    //![1]

    //![2]
    GraphicsProgram
    {

        x:0
        y:0
        width: parent.width/2
        height: parent.height/2

        property var tex: mip0

        fragmentShader: "
            #version 330

            in vec2 coord;

            out vec4 fragColor;

            uniform usampler2D tex;
            uniform float qt_Opacity;

            void main()
            {
                fragColor = texture(tex, coord)*qt_Opacity;
            }"

    }


    GraphicsProgram
    {

        x: parent.width/2
        y: parent.height/2
        width: parent.width/2
        height: parent.height/2

        property var tex: mip2


        fragmentShader: "
            #version 330

            in vec2 coord;

            out vec4 fragColor;

            uniform isampler2D tex;
            uniform float qt_Opacity;

            void main()
            {
                fragColor = texture(tex, coord)*qt_Opacity;
            }"

    }
    //![2]
}

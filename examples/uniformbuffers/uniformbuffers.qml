import QtQuick 2.4
import QtQuick.Window 2.2
import QuickOpenGL 1.0

Window {

    visible: true
    color: 'black'
    width: 640
    height: 480
    title: "Uniform Buffers"

    function vertex2D(x, y)
    {
        return {vertexPosition: Qt.vector2d(x,y)}
    }

    //![0]
    UniformBuffer
    {
        id: uniformBuffer
        property var vertexColor: ['#ff0000', '#00ff00', '#0000ff']
    }
    //![0]

    Timer
    {
        interval: 500
        running: true
        repeat: true
        onTriggered: uniformBuffer.vertexColor = [uniformBuffer.vertexColor[1], uniformBuffer.vertexColor[2], uniformBuffer.vertexColor[0]]
    }


    //![2]
    GraphicsProgram
    {
        anchors.fill: parent

        vertices : [vertex2D(10, height-10),
                    vertex2D(width-10, height-10),
                    vertex2D(width/2, 10)]

        property var block: uniformBuffer

        onUniformBlocksChanged: console.log(this, '\nuniformBlocks', JSON.stringify(uniformBlocks, null, '    '))

        vertexShader: "
            #version 140

            in vec2 vertexPosition;

            out vec3 color;

            uniform mat4 qt_Matrix;

            //![1]
            uniform block
            {
                vec3 vertexColor[3];
            };
            //![1]

            void main()
            {
                color = vertexColor[gl_VertexID];
                gl_Position = qt_Matrix*vec4(vertexPosition, 0.0, 1.0);
            }"


        fragmentShader: "
            #version 140

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }"

    }
    //![2]


}



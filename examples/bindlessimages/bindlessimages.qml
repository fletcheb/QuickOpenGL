import QtQuick 2.10
import QtQuick.Window 2.10
import QuickOpenGL 1.0

Window
{
    id: root
    visible: true
    width: 640
    height: 480
    title: "Bindless Images"

    QuickOpenGLDebug
    {
        flags: QuickOpenGLDebug.FlagTiming
    }

    //![0]
    // Texture with mip levels
    Texture
    {
        id: imageTexture
        target: Texture.Target2D
        format: Texture.FormatRGBA32F
        width: 256
        height: 256
        mipLevels: 9
        minFilter: Texture.FilterNearestMipMapNearest
        magFilter: Texture.FilterNearest
        syncFromOpenGL: true
        onDataChanged: console.log(data[1].vec4At(2))

    }
    //![0]

    //![1]
    // list of image handles
    property var imageHandles: []

    // create an image handle for each texture mip level
    Repeater
    {
        id: imageRepeater
        model: imageTexture.mipLevels

        ImageHandle
        {
            source: imageTexture
            level: index
            layered: false
            format: imageTexture.format
            access: ImageHandle.AccessWriteOnly

        }

        onItemAdded: {imageHandles.push(item); imageHandles = imageHandles}
    }
    //![1]

    //![2]
    // write a checkerboard to each mip layer
    ComputeProgram
    {
        xGroups: imageTexture.width
        yGroups: imageTexture.height
        zGroups: outputImage.length

        property var outputImage : imageHandles

        onDispatched: console.log(this + ' dispatched with state:\n' + printState(state))

        function printState(state, depth)
        {
            depth = (typeof depth !== 'undefined') ?  depth : 0
            var result = '';
            for(var object in state)
            {
                if(object instanceof Array)
                    result += printState(object, ++depth)
                else
                {
                    for(var i=0;i<depth;++i)
                        result += '    '

                    result +=  '    ' + object + ':\t' + state[object] + '\n'
                }
            }
            return result
        }

        computeShader: "
                #version 430 core
                #extension GL_ARB_bindless_texture : require
                "
                + (OpenGLProperties.vendor.includes("NVIDIA")?"#define NVIDIA":"") +
                "
                layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

                uniform uvec2 outputImage[9];

                void main(void)
                {
                #ifdef NVIDIA
                     layout(rgba32f) image2D image = layout(rgba32f) image2D(outputImage[gl_GlobalInvocationID.z]);
                #else
                    image2D image = image2D(outputImage[gl_GlobalInvocationID.z]);
                #endif

                    vec4 value;
                    switch(gl_GlobalInvocationID.z%3)
                    {
                        case 0:
                            value = vec4(1,0,0,1);
                            break;
                        case 1:
                            value = vec4(0,1,0,1);
                            break;
                        case 2:
                            value = vec4(0,0,1,1);
                            break;
                    }

                    if(all(lessThan(gl_GlobalInvocationID.xy,imageSize(image))))
                    {
                        bool on = (gl_GlobalInvocationID.x+gl_GlobalInvocationID.y)%2==0;

                        imageStore(image, ivec2(gl_GlobalInvocationID.xy), on?value:vec4(0));
                    }

                }
            "

    }
    //![2]


    Timer
    {
        interval: 1000
        running: true
        repeat: true
        onTriggered: {var level = graphicsProgram.level+1; level%=imageTexture.mipLevels; graphicsProgram.level=level;}
    }

    //![3]    
    GraphicsProgram
    {
        id: graphicsProgram
        anchors.fill: parent

        property var inputTexture: imageTexture
        property int level: 0
        onLevelChanged: console.log(level)

        fragmentShader: "
           #version 330

           in vec2 coord;

           uniform float qt_Opacity;
           uniform sampler2D inputTexture;
           uniform int level;

           out vec4 fragColor;

           void main()
           {
               vec4 value = textureLod(inputTexture, coord, level);
               fragColor = value * qt_Opacity;
           }"

    }
    //![3]



}

import QtQuick 2.10
import QtQuick.Window 2.10
import QuickOpenGL 1.0

Window {

    visible: true
    color: 'black'
    width: 640
    height: 480
    title: "Bindless Textures"

    //![0]
    Image {
        id: image
        source: 'opengl.jpg'
        visible: false
    }
    //![0]

    //![1]
    TextureHandle {
        id: textureHandle
        source: image
    }
    //![1]

    //![2]
    GraphicsProgram {
        anchors.fill: parent
        property var handle: textureHandle

        fragmentShader: "
               #version 330
               #extension GL_ARB_bindless_texture : require

               in vec2 coord;
               out vec4 fragColor;

               // can use uint64_t is #extension GL_ARB_gpu_shader_int64 is supported
               uniform uvec2 handle;

               void main()
               {
                 fragColor = texture(sampler2D(handle), coord);
               }"
    }
    //![2]
}

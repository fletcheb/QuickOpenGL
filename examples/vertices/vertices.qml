import QtQuick 2.12
import QtQuick.Window 2.12
import QuickOpenGL 1.0

Window {

    visible: true
    color: 'black'
    width: 640
    height: 480
    title: "Vertices"

    //
    // Convenience function to enumerate object names and values
    //
    function enumerateObjectTree(object, name)
    {
        var result = ''
        if(typeof object === 'object')
        {
            for(var key in object)
            {
                result += enumerateObjectTree(object[key], (name===undefined?'':(name+'.'))+key)
            }
        }
        else
        {
            result = name + ': ' + object + '\n'
        }
        return result
    }

    //
    // Convenience function to create a vertex (JavaScript object with key-value pairs)
    //
    // The key names must be the same as the vertex attribute names in the
    // vertex shader. QuickOpenGL introspects the attribute names and types
    // from OpenGL. It then attempts to convert the QML types to OpenGL types.
    //
    //![0]
    function vertex(x, y, color)
    {
        return {vertexPosition: Qt.vector2d(x,y), vertexColor: color}
    }
    //![0]

    //
    // Graphics program to demonstrate passing vertices from QML
    //
    // It should be noted that the Quick2 scenegraph is designed to be 2 dimensional. As such, the
    // scenegraph may alter the gl_Position z coordinate to make sure that QQuickItems stack in the
    // correct z order.  This is achieved by adding an attribute _qt_order, and a uniform _qt_zRange
    //
    // The following line may be added to the end of the vertex shader:
    //      gl_Position.z = (gl_Position.z * _qt_zRange + _qt_order) * gl_Position.w;
    //
    //
    // The vertices property is a QVariant
    //
    // QuickOpenGL understands lists of JavaScript object key-value pairs, and single integer
    // values.
    //
    // If a list of key-value pairs is presented, QuickOpenGL will attempt to match them with
    // the attributes found in the vertex shader.  This allows arbitrary vertex attributes to
    // be passed from QML.
    //
    //
    //![1]
    GraphicsProgram
    {
        objectName: 'Sensible Vertices'
        vertices : [vertex(-1.0, 0.0, '#ff0000'),
                    vertex(0.0, 0.0, Qt.rgba(0, 1, 0, 1)),
                    vertex(-0.5, 1.0, 'blue')]

        onAttributesChanged: console.log(this, 'Active attributes:\n' + enumerateObjectTree(attributes))


        vertexShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'330') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec2 vertexPosition;
            in vec3 vertexColor;
            out vec3 color;

            void main()
            {
                color = vertexColor;
                gl_Position = vec4(vertexPosition, 0.0, 1.0);
            }'


        onVertexShaderChanged: console.log(vertexShader)
        fragmentShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'330') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }'
    }
    //![1]


    //
    // Graphics program to demonstrate pulling vertices from elsewhere, and changing the drawing mode
    //
    //
    // The vertices property is a QVariant
    //
    // QuickOpenGL understands lists of JavaScript object key-value pairs, and single integer
    // values.
    //
    // If a single integer is presented, QuickOpenGL will assume that the vertex shader has
    // no attributes, and will draw the requested number of vertices, assuming that the vertex
    // shader pulls the vertex values itself.
    //
    //![2]
    GraphicsProgram
    {
        objectName: 'Pulled Vertices'
        vertices : 3

        drawingMode: GraphicsProgram.DrawingModeLineLoop
        lineWidth: OpenGLProperties.glLineWidthRange[1]

        onAttributesChanged: console.log(this, 'Active attributes:\n' + enumerateObjectTree(attributes))

        vertexShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'330') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            out vec3 color;

            void main()
            {
                switch(gl_VertexID)
                {
                    case 0:
                        gl_Position = vec4(0, -1, 0, 1);
                        color = vec3(1, 0, 0);
                        break;
                    case 1:
                        gl_Position = vec4(1, -1, 0, 1);
                        color = vec3(0, 1, 0);
                        break;
                    case 2:
                        gl_Position = vec4(0.5, 0, 0, 1);
                        color = vec3(0, 0, 1);
                        break;
                    default:
                        color = vec3(0);
                        break;
                }

            }'


        fragmentShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'330') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }'
    }
    //![2]

    //
    // Convenience function to create a vertex (JavaScript object with key-value pairs)
    //
    // This is a nonsense vertex to demonstrate vertex attibute structs and arrays
    //
    //![3]
    function crazyVertex(x, y, color)
    {
        return {
            vertex: {
                x: x,
                y: y,
                color: [{red: color[0].r, green: color[0].g, blue: color[0].b},
                        {red: color[1].r, green: color[1].g, blue: color[1].b}]
            }
        }
    }
    //![3]

    //
    // Graphics program to demonstrate vertex attribute structs and arrays
    //
    // Works on nVidia Linux, doesn't work on AMD windows or macOS
    //
    //![4]
    GraphicsProgram
    {
        objectName: 'Crazy Vertices'
        vertices : [crazyVertex(0.5, 0.0, [Qt.rgba(1,0,0,1), Qt.rgba(0,1,0,1)]),
                    crazyVertex(1.0, 1.0, [Qt.rgba(0,1,0,1), Qt.rgba(0,0,1,1)]),
                    crazyVertex(0.0, 1.0, [Qt.rgba(0,0,1,1), Qt.rgba(1,0,0,1)])]

        onAttributesChanged: console.log(this, 'Active attributes:\n' + enumerateObjectTree(attributes))

        vertexShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'330') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            struct Color
            {
                float red;
                float green;
                float blue;
            };

            struct Vertex {
                float x;
                float y;
                Color color[2];
            };

            in Vertex vertex;

            out vec3 color;

            void main()
            {
                color = vec3(vertex.color[0].red, vertex.color[0].green, vertex.color[0].blue);
                color += vec3(vertex.color[1].red, vertex.color[1].green, vertex.color[1].blue);
                gl_Position = vec4(vertex.x, vertex.y, 0.0, 1.0);
            }'


        fragmentShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'330') + '
            #ifdef GL_ES
            precision highp float;
            #endif


            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }'
    }
    //![4]


    //![5]
    GraphicsProgram
    {
        objectName: 'Simple Vertices'
        vertices: [GL.vec2(-0.5, -1), GL.vec2(0, 0), GL.vec2(-1, 0)]

        onAttributesChanged: console.log(this, 'Active attributes:\n' + enumerateObjectTree(attributes))

        vertexShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'330') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec2 position;
            //in int gl_VertexID;
            out vec3 color;

            void main()
            {
                gl_Position = vec4(position, 0, 1);

                switch(gl_VertexID)
                {
                    case 0:
                        color = vec3(1, 1, 0);
                        break;
                    case 1:
                        color = vec3(0, 1, 1);
                        break;
                    case 2:
                        color = vec3(1, 0, 1);
                        break;
                    default:
                        color = vec3(0);
                        break;
                }
            }'


        fragmentShader: '
            #version ' +  (OpenGLProperties.isOpenGLES?'300 es':'330') + '
            #ifdef GL_ES
            precision highp float;
            #endif

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }'

    }
    //![5]


}

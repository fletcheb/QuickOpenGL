import QtQuick 2.4
import QtQuick.Window 2.2
import QuickOpenGL 1.0

Window {

    visible: true
    color: 'black'
    width: 800
    height: 600
    title: "Uniforms"

    function vertex2D(x, y)
    {
        return {vertexPosition: Qt.vector2d(x,y)}
    }

    // Image to use as a texture provider (hidden)
    Image
    {
        id: image
        source: 'opengl.jpg'
        mipmap: true
        visible: false
    }


    //
    // Graphics program to demonstrate passing uniforms
    //
    // It may be convenient to work within the QML scenegraph projection.  This can be done quite
    // easily using the qt_Matrix combined transformation matrix uniform, in the same manner as
    // the Quick2 ShaderEffect QML Type.
    //
    // In addition any property of GraphicsProgram that can be mapped to an OpenGL type is made
    // available as a uniform, including arrays of values. These properties include properties of
    // QQuickItem such as height and width.  OpenGL uniform blocks are not currently supported.
    //
    GraphicsProgram
    {
        objectName: "graphicsProgram0"

        anchors.fill: parent

        vertices : [vertex2D(10, height-10),
                    vertex2D(width-10, height-10),
                    vertex2D(width/2, 10)]

        // incorrect uniform array size
        property var vertexColor: '#ff000000' //'['#ff0000', '#00ff00']
        property var unsupportedConversion: QtObject {}


        vertexShader: "
            #version 430

            in vec2 vertexPosition;

            out vec3 color;

            uniform mat4 qt_Matrix;
            uniform vec3 vertexColor[2][3];
            uniform float unsupportedConversion;
            uniform float missingUniform;

            uniform missingUniformBlock
            {
                float a;
            };

            buffer missingShaderStorageBlock
            {
                float b;
            };

            void main()
            {
                color = a*b*vertexColor[gl_VertexID][2]*unsupportedConversion*missingUniform;
                gl_Position = qt_Matrix*vec4(vertexPosition, 0.0, 1.0);
            }"


        fragmentShader: "
            #version 430

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }"

    }


    //
    // Graphics program to demonstrate passing uniforms
    //
    // Any QQuickItem which is a texture provider may be used as a uniform sampler.  Unlike
    // ShaderEffect, GraphicsProgram supports samplers other than sampler2D.  Opacity is supported
    // through the qt_Opacity uniform.
    //
    GraphicsProgram
    {
        objectName: "graphicsProgram1"

        anchors.fill: parent

        vertices: [{position: Qt.vector2d(0,1), texCoord: Qt.vector2d(0,1)},
                   {position: Qt.vector2d(1,1), texCoord: Qt.vector2d(1,1)},
                   {position: Qt.vector2d(0,0), texCoord: Qt.vector2d(0,0)},
                   {position: Qt.vector2d(1,0), texCoord: Qt.vector2d(1,0)}]

        property var tex: image

        property var textures: [QtObject]

        opacity: 0.5
        rotation: 10


        vertexShader: "
            #version 330

            in vec2 position;
            in vec2 texCoord;

            out vec2 coord;

            uniform mat4 qt_Matrix;
            uniform float width;
            uniform float height;

            void main()
            {
                gl_Position = qt_Matrix*vec4(vec2(width, height)*0.2 + position*vec2(width,height)*0.6, 0.0, 1.0);
                coord = texCoord;
            }"


        fragmentShader: "
            #version 330

            in vec2 coord;

            out vec4 fragColor;

            uniform sampler1DArray tex;
            uniform sampler2D textures[2];
            uniform sampler2D notTexture;
            uniform float qt_Opacity;

            void main()
            {
                fragColor = (texture(tex, coord)+texture(textures[0], coord)+texture(textures[1], coord))*qt_Opacity;
            }"

    }
}



TEMPLATE = app

QT += qml quick
CONFIG += c++11

# Additional import path used to resolve QML modules in Qt Creator's code model
QML2_IMPORT_PATH += ../../src/ ../../../src/

HEADERS += ../integration.h

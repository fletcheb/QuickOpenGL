TEMPLATE = subdirs

SUBDIRS += \
    subroutineerrorcatching \
    vertexerrorcatching \
    uniformerrorcatching \
    deletion

!macos {
    SUBDIRS += ssboerrorcatching
}

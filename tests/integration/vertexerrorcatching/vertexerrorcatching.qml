import QtQuick 2.4
import QtQuick.Window 2.2
import QuickOpenGL 1.0

Window {

    visible: true
    color: 'black'
    width: 800
    height: 600
    title: "Vertices"

    //
    // Graphics program to demonstrate passing vertices from QML
    //
    // It should be noted that the Quick2 scenegraph is designed to be 2 dimensional. As such, the
    // scenegraph may alter the gl_Position z coordinate to make sure that QQuickItems stack in the
    // correct z order.  This is achieved by adding an attribute _qt_order, and a uniform _qt_zRange
    //
    // The following line may be added to the end of the vertex shader:
    //      gl_Position.z = (gl_Position.z * _qt_zRange + _qt_order) * gl_Position.w;
    //
    GraphicsProgram
    {
        objectName: "graphicsProgramIncorrectVertices"
        anchors.fill: parent

        //
        // The vertices property is a QVariant
        //
        // QuickOpenGL understands lists of JavaScript object key-value pairs, and single integer
        // values.
        //
        // If a list of key-value pairs is presented, QuickOpenGL will attempt to match them with
        // the attributes found in the vertex shader.  This allows arbitrary vertex attributes to
        // be passed from QML.
        //
        vertices : [
                    // correctly formed vertex
                    {vertexPosition : Qt.vector2d(-0.5, 1.0), vertexColor: ['red', 'green'] },

                    // malformed vertex with incorrect "vertexColor" attribute array size
                    {vertexPosition : Qt.vector2d(-0.5, 1.0), vertexColor: '#ff0000' },

                    // malformed vertex which cannot be converted to a QVariantMap
                    123,

                    // malformed vertex missing vertex attribute "vertexPosition"
                    {incorrectAttributeName : Qt.vector2d(-0.5, 1.0), vertexColor: [Qt.rgba(0,0,0,1), Qt.rgba(1,1,1,1)]},

                    // malformed vertex with vertex attribute "vertexPosition" of incorrect type
                    {vertexPosition : Qt.vector3d(-0.5, 1.0, 0.0), vertexColor: ['red', 'green'] },

                    ]

        //
        // The vertex usage pattern
        //
        // This is used by the scene graph to help OpenGL determine where to store the vertices
        //
        vertexUsagePattern: GraphicsProgram.UsagePatternDynamic

        vertexShader: "
            #version 330

            // vertex attributes
            in vec2 vertexPosition;
            in vec3 vertexColor[2];

            out vec3 color;

            void main()
            {
                color = vertexColor[0]+vertexColor[1];
                gl_Position = vec4(vertexPosition, 0.0, 1.0);
            }"


        fragmentShader: "
            #version 330

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }"
    }


    //
    // Graphics program to demonstrate pulling vertices from elsewhere, and changing the drawing mode
    //
    GraphicsProgram
    {
        objectName: "graphicsProgramIncompatibleVertices"
        anchors.fill: parent


        //
        // The vertices property is a QVariant
        //
        // QuickOpenGL understands lists of JavaScript object key-value pairs, and single integer
        // values.
        //
        // If a single integer is presented, QuickOpenGL will assume that the vertex shader has
        // no attributes, and will draw the requested number of vertices, assuming that the vertex
        // shader pulls the vertex values itself.
        //
        vertices: QtObject{}

        //
        // The drawing mode selects the OpenGL drawing mode
        //
        drawingMode: GraphicsProgram.DrawingModeLineLoop

        //
        // The full range of OpenGL line widths and point sizes, etc are supported
        //
        lineWidth: OpenGLProperties.glAliasedLineWidthRange[1]

        vertexShader: "
            #version 330

            out vec3 color;

            void main()
            {
                switch(gl_VertexID)
                {
                    case 0:
                        gl_Position = vec4(0, -1, 0, 1);
                        color = vec3(1, 0, 0);
                        break;
                    case 1:
                        gl_Position = vec4(1, -1, 0, 1);
                        color = vec3(0, 1, 0);
                        break;
                    case 2:
                        gl_Position = vec4(0.5, 0, 0, 1);
                        color = vec3(0, 0, 1);
                        break;
                }

            }"


        fragmentShader: "
            #version 330

            in vec3 color;
            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(color, 1.0);
            }"
    }


}

import QtQuick 2.9
import QtQuick.Window 2.3
import QuickOpenGL 1.0

Window {

    id: root
    visible: true
    color: 'black'
    width: 800
    height: 600

    //
    // Shader Storage Buffer Object
    //
    // This shader storage buffer object is set to sync from OpenGL.  Its definition is different
    // to that described in the OpenGL shader.
    //
    // QuickOpenGL will print a warning that the shader storage block variable "missingProperty"
    // could not be matched with a QML property
    //
    ShaderStorageBuffer
    {
        id: ssboSyncFromOpenGL
        objectName: "ssboSyncFromOpenGL"
        syncFromOpenGL: true
        syncToOpenGL: ShaderStorageBuffer.SyncWriteNever
        usagePattern: ShaderStorageBuffer.DynamicDraw

        property color color: "#ff000000"
    }

    //
    // Shader Storage Buffer Object
    //
    // This shader storage buffer object is set to sync to OpenGL.  It contains an unsupported
    // property type that cannot be copied from QML to OpenGL, and an array which contains an
    // incorrect number of elements.
    //
    // QuickOpenGL will print a warning that the OpenGL shader storage block variable
    // "incorrectlySizedArray" contains 10 elements, while the QML property contains 2 elements.
    //
    // QuickOpenGL will print a warning that the unsupported QML property type QObject cannot be
    // converted to the OpenGL type GL_FLOAT_VEC4
    //
    ShaderStorageBuffer
    {
        id: ssboSyncToOpenGL
        objectName: "ssboSyncToOpenGL"
        syncToOpenGL: ShaderStorageBuffer.SyncWriteAlways
        syncFromOpenGL: false
        usagePattern: ShaderStorageBuffer.DynamicDraw

        property var unsupportedProperty: QtObject{}
        property var incorrectlySizedArray: [Qt.vector4d(1,2,3,4), Qt.vector4d(5,6,7,8)]
    }

    //
    // Shader Storage Buffer Object
    //
    ShaderStorageBuffer
    {
        id: ssboOther
        objectName: "ssboOther"
        size: 1024
    }

    //
    // Graphics Program
    //
    // Contains buffer definitions which differ from the QML SSBO definitions in order to test
    // QuickOpenGL error reporting
    //
    // Fragment shader has an error where ssboOther and ssboSyncFromOpenGL are both bound to the
    // same buffer binding.  QuickOpenGL will print a warning and reallocate bindings to correct
    // the problem.
    //
    // Note that the buffer block definitions in this program are different from those in the
    // compute program.  QuickOpenGL will report this error.
    //
    GraphicsProgram
    {        
        id: graphicsProgram
        objectName: "ssboErrorCatchingGraphicsProgram"
        anchors.fill: parent        

        property var mouse: GL.vec2(mouseArea.mapToItem(null, mouseArea.mouseX, mouseArea.mouseY)).times(Screen.devicePixelRatio)

        property var ssboSyncFromOpenGL: ssboSyncFromOpenGL
        property var ssboSyncToOpenGL: ssboSyncToOpenGL
        property var ssboOther: ssboOther

        memoryBarrier: GraphicsProgram.BarrierShaderStorage

        fragmentShader: "
            #version 430
            #line 49
            in vec2 coord;
            layout (origin_upper_left,pixel_center_integer) in vec4 gl_FragCoord;

            out vec4 qt_FragColor;

            uniform vec2 mouse;
            layout(binding = 0) buffer ssboSyncFromOpenGL
            {
                vec4 color;
                vec2 missingProperty;
            };

            layout(binding = 1) buffer ssboSyncToOpenGL
            {
                vec4 unsupportedProperty;
                vec4 incorrectlySizedArray[10];
            };

            layout(binding = 0) buffer ssboOther
            {
                vec4 temp[];
            };

            void main()
            {
                qt_FragColor = vec4(coord, 0, 1);

                missingProperty = vec2(0,0);

                if(all(equal(gl_FragCoord.xy, mouse)))
                {
                    color = qt_FragColor;
                }

            }"
    }

    //
    // Compute Program
    //
    // Contains buffer definitions which differ from the QML SSBO definitions in order to test
    // QuickOpenGL error reporting
    //
    // Compute shader has an error where ssboOther and ssboSyncToOpenGL are both bound to the
    // same buffer binding.  QuickOpenGL will print a warning and reallocate bindings to correct
    // the problem.
    //
    // Note that the buffer block definitions in this program are different from those in the
    // graphics program.  QuickOpenGL will report this error.
    //
    ComputeProgram
    {
        id: computeProgram
        objectName: 'ssboErrorCatchingComputeProgram'
        dispatchOnUniformPropertyChanges: true
        onDispatched: console.log("dispatched")
        property var mouse: graphicsProgram.mouse
        property var ssboSyncFromOpenGL: ssboSyncFromOpenGL
        property var ssboSyncToOpenGL: ssboSyncToOpenGL
        property var ssboOther: ssboOther

        computeShader: "
            #version 430 core
            #line 136

            layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

            uniform vec2 mouse;

            layout(binding = 0) buffer ssboSyncFromOpenGL
            {
                double inconsistentDefinitionA;
                vec4 color;
                vec2 missingProperty;
            };

            layout(binding = 1) buffer ssboSyncToOpenGL
            {
                int inconsistentDefinitionB;
                vec4 unsupportedProperty;
                vec4 incorrectlySizedArray[10];
            };

            layout(binding = 1) buffer ssboOther
            {
                vec4 temp[];
            };

            void main(void)
            {
                temp[0]=vec4(mouse,3,4);
                color = vec4(1,1,1,1);
                inconsistentDefinitionA=100.0;
                missingProperty = vec2(100,200);
            }
        "
    }

    Text
    {
        text: ssboSyncFromOpenGL.color
        color: ssboSyncFromOpenGL.color
    }

    Text
    {
        anchors.right: parent.right
        text: ssboSyncFromOpenGL.color
        color: ssboSyncFromOpenGL.color
    }

    Text
    {
        anchors.bottom: parent.bottom
        text: ssboSyncFromOpenGL.color
        color: ssboSyncFromOpenGL.color
    }

    Text
    {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        text: ssboSyncFromOpenGL.color
        color: ssboSyncFromOpenGL.color
    }

    Text
    {
        anchors.centerIn: parent
        text: ssboSyncFromOpenGL.color
        color: ssboSyncFromOpenGL.color
    }


    MouseArea
    {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true

    }

}

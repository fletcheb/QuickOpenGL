import QtQuick 2.11
import QtQuick.Window 2.11
import QuickOpenGL 1.0

Window {
    visible: true
    width: 640
    height: 480
    id:root

    Row
    {
        Repeater
        {
            id: repeater
            model: 10

            GraphicsProgram
            {
                width: root.width/Number(repeater.model)
                height: 100

                Texture
                {

                }

                ShaderStorageBuffer
                {

                }
            }

        }
    }

    Timer
    {
        interval: 1000
        repeat: true
        running: true
        onTriggered: repeater.model=repeater.model-1
    }

}

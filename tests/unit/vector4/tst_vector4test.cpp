#include <QString>
#include <QtTest>

#include "quickvector4.h"
#include "quickopengl_plugin.h"
#include "quickmatrix4.h"

class Vector4Test : public QObject
{
    Q_OBJECT

public:
    Vector4Test();

private Q_SLOTS:
    void testvec4();
    void testivec4();
    void testuvec4();
    void testdvec4();
    void testbvec4();
    void testSwizzle();

};

Vector4Test::Vector4Test()
{
    // register types
    QuickOpenGLPlugin plugin;
    plugin.registerTypes("QuickOpenGL");
}

void Vector4Test::testvec4()
{
    qvec4 v0(1.0f, 2.0f, 3.0f, 4.0f);
    QCOMPARE(v0.x(), 1.0f);
    QCOMPARE(v0.y(), 2.0f);
    QCOMPARE(v0.z(), 3.0f);
    QCOMPARE(v0.w(), 4.0f);

    v0.setX(5.0f);
    v0.setY(6.0f);
    v0.setZ(7.0f);
    v0.setW(8.0f);

    QCOMPARE(v0.x(), 5.0f);
    QCOMPARE(v0.y(), 6.0f);
    QCOMPARE(v0.z(), 7.0f);
    QCOMPARE(v0.w(), 8.0f);

    QCOMPARE(v0.r(), 5.0f);
    QCOMPARE(v0.g(), 6.0f);
    QCOMPARE(v0.b(), 7.0f);
    QCOMPARE(v0.a(), 8.0f);

    v0.setR(2.0f);
    v0.setG(4.0f);
    v0.setB(6.0f);
    v0.setA(8.0f);

    QCOMPARE(v0.x(), 2.0f);
    QCOMPARE(v0.y(), 4.0f);
    QCOMPARE(v0.z(), 6.0f);
    QCOMPARE(v0.w(), 8.0f);

    qvec4 v1;

    QCOMPARE(v1.x(), 0.0f);
    QCOMPARE(v1.y(), 0.0f);
    QCOMPARE(v1.z(), 0.0f);
    QCOMPARE(v1.w(), 0.0f);


    QCOMPARE(qvec4::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qvec4::staticMetaObject.property(1).name(), "y");
    QCOMPARE(qvec4::staticMetaObject.property(2).name(), "z");
    QCOMPARE(qvec4::staticMetaObject.property(3).name(), "w");
    QCOMPARE(qvec4::staticMetaObject.property(4).name(), "r");
    QCOMPARE(qvec4::staticMetaObject.property(5).name(), "g");
    QCOMPARE(qvec4::staticMetaObject.property(6).name(), "b");
    QCOMPARE(qvec4::staticMetaObject.property(7).name(), "a");

    qvec4::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(2.0f));
    qvec4::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(4.0f));
    qvec4::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(6.0f));
    qvec4::staticMetaObject.property(3).writeOnGadget(&v1, QVariant::fromValue(8.0f));

    QCOMPARE(v0, v1);

    QCOMPARE(qvec4::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(2.0f));
    QCOMPARE(qvec4::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(4.0f));
    QCOMPARE(qvec4::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(6.0f));
    QCOMPARE(qvec4::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(8.0f));
    QCOMPARE(qvec4::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(2.0f));
    QCOMPARE(qvec4::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(4.0f));
    QCOMPARE(qvec4::staticMetaObject.property(6).readOnGadget(&v1), QVariant::fromValue(6.0f));
    QCOMPARE(qvec4::staticMetaObject.property(7).readOnGadget(&v1), QVariant::fromValue(8.0f));

    v0.setR(0.0f/3.0f);
    v0.setG(1.0f/3.0f);
    v0.setB(2.0f/3.0f);
    v0.setA(3.0f/3.0f);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector4D>());
    QVERIFY(variant.canConvert<QColor>());
    QVERIFY(variant.canConvert<QString>());
    QVERIFY(variant.canConvert<QQuaternion>());
    QVERIFY(variant.canConvert<QRect>());
    QVERIFY(variant.canConvert<QRectF>());

    QVector4D vector4d = variant.value<QVector4D>();
    QCOMPARE(vector4d.x(), 0.0f/3.0f);
    QCOMPARE(vector4d.y(), 1.0f/3.0f);
    QCOMPARE(vector4d.z(), 2.0f/3.0f);
    QCOMPARE(vector4d.w(), 3.0f/3.0f);

    QColor color = variant.value<QColor>();
    QCOMPARE(color.redF(), 0.0);
    QCOMPARE(color.greenF(), 1.0/3.0);
    QCOMPARE(color.blueF(), 2.0/3.0);
    QCOMPARE(color.alphaF(), 1.0);

    QString string  = variant.value<QString>();
    QCOMPARE(string, QString("#ff0055aa"));

    QQuaternion quaternion = variant.value<QQuaternion>();
    QCOMPARE(quaternion.x(), 0.0f/3.0f);
    QCOMPARE(quaternion.y(), 1.0f/3.0f);
    QCOMPARE(quaternion.z(), 2.0f/3.0f);
    QCOMPARE(quaternion.scalar(), 3.0f/3.0f);

    QRectF rectF = variant.value<QRectF>();
    QCOMPARE(rectF.x(), double(0.0f/3.0f));
    QCOMPARE(rectF.y(), double(1.0f/3.0f));
    QCOMPARE(rectF.width(), double(2.0f/3.0f));
    QCOMPARE(rectF.height(), double(3.0f/3.0f));

    v0.setR(1.0f);
    v0.setG(2.0f);
    v0.setB(3.0f);
    v0.setA(4.0f);

    variant = QVariant::fromValue(v0);

    QRect rect = variant.value<QRect>();
    QCOMPARE(rect.x(), 1);
    QCOMPARE(rect.y(), 2);
    QCOMPARE(rect.width(), 3);
    QCOMPARE(rect.height(), 4);

    variant = QVariant::fromValue(vector4d);
    v0 = variant.value<qvec4>();
    QCOMPARE(v0.x(), 0.0f/3.0f);
    QCOMPARE(v0.y(), 1.0f/3.0f);
    QCOMPARE(v0.z(), 2.0f/3.0f);
    QCOMPARE(v0.w(), 3.0f/3.0f);

    variant = QVariant::fromValue(color);
    v0 = variant.value<qvec4>();
    QCOMPARE(v0.x(), 0.0f/3.0f);
    QCOMPARE(v0.y(), 1.0f/3.0f);
    QCOMPARE(v0.z(), 2.0f/3.0f);
    QCOMPARE(v0.w(), 3.0f/3.0f);

    variant = QVariant::fromValue(QColor(string));
    v0 = variant.value<qvec4>();
    QCOMPARE(v0.x(), 0.0f/3.0f);
    QCOMPARE(v0.y(), 1.0f/3.0f);
    QCOMPARE(v0.z(), 2.0f/3.0f);
    QCOMPARE(v0.w(), 3.0f/3.0f);

    variant = QVariant::fromValue(quaternion);
    v0 = variant.value<qvec4>();
    QCOMPARE(v0.x(), 0.0f/3.0f);
    QCOMPARE(v0.y(), 1.0f/3.0f);
    QCOMPARE(v0.z(), 2.0f/3.0f);
    QCOMPARE(v0.w(), 3.0f/3.0f);

    variant = QVariant::fromValue(rectF);
    v0 = variant.value<qvec4>();
    QCOMPARE(v0.x(), 0.0f/3.0f);
    QCOMPARE(v0.y(), 1.0f/3.0f);
    QCOMPARE(v0.z(), 2.0f/3.0f);
    QCOMPARE(v0.w(), 3.0f/3.0f);

    variant = QVariant::fromValue(rect);
    v0 = variant.value<qvec4>();
    QCOMPARE(v0.x(), 1.0f);
    QCOMPARE(v0.y(), 2.0f);
    QCOMPARE(v0.z(), 3.0f);
    QCOMPARE(v0.w(), 4.0f);

    qvec4 a(1,2,3,4);
    qvec4 b(5,6,7,8);
    qmat4 m(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
    m = m.transpose();
    QCOMPARE(a.dotProduct(b), 70.0);
    QCOMPARE(a.times(m), qvec4(120,130,140,150));
    QCOMPARE(a.times(b), qvec4(5,12,21,32));
    QCOMPARE(a.times(4.48f), qvec4(1.0f*4.48f, 2.0f*4.48f, 3.0f*4.48f, 4.0f*4.48f));
    QCOMPARE(a.plus(b), qvec4(6,8,10,12));
    QCOMPARE(a.minus(b), qvec4(-4,-4,-4,-4));
    QCOMPARE(a.normalized(), qvec4(1.0f/std::sqrt(30.0f), 2.0f/std::sqrt(30.0f), 3.0f/std::sqrt(30.0f), 4.0f/std::sqrt(30.0f)));
    QCOMPARE(a.length(), std::sqrt(30.0f));
}

void Vector4Test::testivec4()
{

    qivec4 v0(1,2,3,4);
    QCOMPARE(v0.x(), 1);
    QCOMPARE(v0.y(), 2);
    QCOMPARE(v0.z(), 3);
    QCOMPARE(v0.w(), 4);

    v0.setX(5);
    v0.setY(6);
    v0.setZ(7);
    v0.setW(8);

    QCOMPARE(v0.x(), 5);
    QCOMPARE(v0.y(), 6);
    QCOMPARE(v0.z(), 7);
    QCOMPARE(v0.w(), 8);

    QCOMPARE(v0.r(), 5);
    QCOMPARE(v0.g(), 6);
    QCOMPARE(v0.b(), 7);
    QCOMPARE(v0.a(), 8);

    v0.setR(2);
    v0.setG(4);
    v0.setB(6);
    v0.setA(8);

    QCOMPARE(v0.x(), 2);
    QCOMPARE(v0.y(), 4);
    QCOMPARE(v0.z(), 6);
    QCOMPARE(v0.w(), 8);

    qivec4 v1;

    QCOMPARE(v1.x(), 0);
    QCOMPARE(v1.y(), 0);
    QCOMPARE(v1.z(), 0);
    QCOMPARE(v1.w(), 0);


    QCOMPARE(qivec4::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qivec4::staticMetaObject.property(1).name(), "y");
    QCOMPARE(qivec4::staticMetaObject.property(2).name(), "z");
    QCOMPARE(qivec4::staticMetaObject.property(3).name(), "w");
    QCOMPARE(qivec4::staticMetaObject.property(4).name(), "r");
    QCOMPARE(qivec4::staticMetaObject.property(5).name(), "g");
    QCOMPARE(qivec4::staticMetaObject.property(6).name(), "b");
    QCOMPARE(qivec4::staticMetaObject.property(7).name(), "a");

    qivec4::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(2));
    qivec4::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(4));
    qivec4::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(6));
    qivec4::staticMetaObject.property(3).writeOnGadget(&v1, QVariant::fromValue(8));

    QCOMPARE(v0, v1);

    QCOMPARE(qivec4::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(2));
    QCOMPARE(qivec4::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(4));
    QCOMPARE(qivec4::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(6));
    QCOMPARE(qivec4::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(8));
    QCOMPARE(qivec4::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(2));
    QCOMPARE(qivec4::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(4));
    QCOMPARE(qivec4::staticMetaObject.property(6).readOnGadget(&v1), QVariant::fromValue(6));
    QCOMPARE(qivec4::staticMetaObject.property(7).readOnGadget(&v1), QVariant::fromValue(8));

    v0.setR(1);
    v0.setG(2);
    v0.setB(3);
    v0.setA(4);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector4D>());
    QVERIFY(variant.canConvert<QColor>());
    QVERIFY(variant.canConvert<QString>());
    QVERIFY(variant.canConvert<QRect>());
    QVERIFY(variant.canConvert<QRectF>());

    QVector4D vector4d = variant.value<QVector4D>();
    QCOMPARE(vector4d.x(), 1.0f);
    QCOMPARE(vector4d.y(), 2.0f);
    QCOMPARE(vector4d.z(), 3.0f);
    QCOMPARE(vector4d.w(), 4.0f);

    QColor color = variant.value<QColor>();
    QCOMPARE(color.red(), 1);
    QCOMPARE(color.green(), 2);
    QCOMPARE(color.blue(), 3);
    QCOMPARE(color.alpha(), 4);

    QString string  = variant.value<QString>();
    QCOMPARE(string, QString("#04010203"));


    QRectF rectF = variant.value<QRectF>();
    QCOMPARE(rectF.x(), 1.0);
    QCOMPARE(rectF.y(), 2.0);
    QCOMPARE(rectF.width(), 3.0);
    QCOMPARE(rectF.height(), 4.0);

    QRect rect = variant.value<QRect>();
    QCOMPARE(rect.x(), 1);
    QCOMPARE(rect.y(), 2);
    QCOMPARE(rect.width(), 3);
    QCOMPARE(rect.height(), 4);

    variant = QVariant::fromValue(vector4d);
    v0 = variant.value<qivec4>();
    QCOMPARE(v0.x(), 1);
    QCOMPARE(v0.y(), 2);
    QCOMPARE(v0.z(), 3);
    QCOMPARE(v0.w(), 4);

    variant = QVariant::fromValue(color);
    v0 = variant.value<qivec4>();
    QCOMPARE(v0.x(), 1);
    QCOMPARE(v0.y(), 2);
    QCOMPARE(v0.z(), 3);
    QCOMPARE(v0.w(), 4);

    variant = QVariant::fromValue(QColor(string));
    v0 = variant.value<qivec4>();
    QCOMPARE(v0.x(), 1);
    QCOMPARE(v0.y(), 2);
    QCOMPARE(v0.z(), 3);
    QCOMPARE(v0.w(), 4);


    variant = QVariant::fromValue(rectF);
    v0 = variant.value<qivec4>();
    QCOMPARE(v0.x(), 1);
    QCOMPARE(v0.y(), 2);
    QCOMPARE(v0.z(), 3);
    QCOMPARE(v0.w(), 4);

    variant = QVariant::fromValue(rect);
    v0 = variant.value<qivec4>();
    QCOMPARE(v0.x(), 1);
    QCOMPARE(v0.y(), 2);
    QCOMPARE(v0.z(), 3);
    QCOMPARE(v0.w(), 4);

}

void Vector4Test::testuvec4()
{
    quvec4 v0(1U,2U,3U,4U);
    QCOMPARE(v0.x(), 1U);
    QCOMPARE(v0.y(), 2U);
    QCOMPARE(v0.z(), 3U);
    QCOMPARE(v0.w(), 4U);

    v0.setX(5U);
    v0.setY(6U);
    v0.setZ(7U);
    v0.setW(8U);

    QCOMPARE(v0.x(), 5U);
    QCOMPARE(v0.y(), 6U);
    QCOMPARE(v0.z(), 7U);
    QCOMPARE(v0.w(), 8U);

    QCOMPARE(v0.r(), 5U);
    QCOMPARE(v0.g(), 6U);
    QCOMPARE(v0.b(), 7U);
    QCOMPARE(v0.a(), 8U);

    v0.setR(2U);
    v0.setG(4U);
    v0.setB(6U);
    v0.setA(8U);

    QCOMPARE(v0.x(), 2U);
    QCOMPARE(v0.y(), 4U);
    QCOMPARE(v0.z(), 6U);
    QCOMPARE(v0.w(), 8U);

    quvec4 v1;

    QCOMPARE(v1.x(), 0U);
    QCOMPARE(v1.y(), 0U);
    QCOMPARE(v1.z(), 0U);
    QCOMPARE(v1.w(), 0U);

    QCOMPARE(quvec4::staticMetaObject.property(0).name(), "x");
    QCOMPARE(quvec4::staticMetaObject.property(1).name(), "y");
    QCOMPARE(quvec4::staticMetaObject.property(2).name(), "z");
    QCOMPARE(quvec4::staticMetaObject.property(3).name(), "w");
    QCOMPARE(quvec4::staticMetaObject.property(4).name(), "r");
    QCOMPARE(quvec4::staticMetaObject.property(5).name(), "g");
    QCOMPARE(quvec4::staticMetaObject.property(6).name(), "b");
    QCOMPARE(quvec4::staticMetaObject.property(7).name(), "a");

    quvec4::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(2U));
    quvec4::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(4U));
    quvec4::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(6U));
    quvec4::staticMetaObject.property(3).writeOnGadget(&v1, QVariant::fromValue(8U));

    QCOMPARE(v0, v1);

    QCOMPARE(quvec4::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(2U));
    QCOMPARE(quvec4::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(4U));
    QCOMPARE(quvec4::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(6U));
    QCOMPARE(quvec4::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(8U));
    QCOMPARE(quvec4::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(2U));
    QCOMPARE(quvec4::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(4U));
    QCOMPARE(quvec4::staticMetaObject.property(6).readOnGadget(&v1), QVariant::fromValue(6U));
    QCOMPARE(quvec4::staticMetaObject.property(7).readOnGadget(&v1), QVariant::fromValue(8U));

    v0.setR(1U);
    v0.setG(2U);
    v0.setB(3U);
    v0.setA(4U);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector4D>());
    QVERIFY(variant.canConvert<QColor>());
    QVERIFY(variant.canConvert<QString>());
    QVERIFY(variant.canConvert<QRect>());
    QVERIFY(variant.canConvert<QRectF>());

    QVector4D vector4d = variant.value<QVector4D>();
    QCOMPARE(vector4d.x(), 1.0f);
    QCOMPARE(vector4d.y(), 2.0f);
    QCOMPARE(vector4d.z(), 3.0f);
    QCOMPARE(vector4d.w(), 4.0f);

    QColor color = variant.value<QColor>();
    QCOMPARE(color.red(), 1);
    QCOMPARE(color.green(), 2);
    QCOMPARE(color.blue(), 3);
    QCOMPARE(color.alpha(), 4);

    QString string  = variant.value<QString>();
    QCOMPARE(string, QString("#04010203"));

    QRectF rectF = variant.value<QRectF>();
    QCOMPARE(rectF.x(), 1.0);
    QCOMPARE(rectF.y(), 2.0);
    QCOMPARE(rectF.width(), 3.0);
    QCOMPARE(rectF.height(), 4.0);

    QRect rect = variant.value<QRect>();
    QCOMPARE(rect.x(), 1);
    QCOMPARE(rect.y(), 2);
    QCOMPARE(rect.width(), 3);
    QCOMPARE(rect.height(), 4);

    variant = QVariant::fromValue(vector4d);
    v0 = variant.value<quvec4>();
    QCOMPARE(v0.x(), 1U);
    QCOMPARE(v0.y(), 2U);
    QCOMPARE(v0.z(), 3U);
    QCOMPARE(v0.w(), 4U);

    variant = QVariant::fromValue(color);
    v0 = variant.value<quvec4>();
    QCOMPARE(v0.x(), 1U);
    QCOMPARE(v0.y(), 2U);
    QCOMPARE(v0.z(), 3U);
    QCOMPARE(v0.w(), 4U);

    variant = QVariant::fromValue(QColor(string));
    v0 = variant.value<quvec4>();
    QCOMPARE(v0.x(), 1U);
    QCOMPARE(v0.y(), 2U);
    QCOMPARE(v0.z(), 3U);
    QCOMPARE(v0.w(), 4U);

    variant = QVariant::fromValue(rectF);
    v0 = variant.value<quvec4>();
    QCOMPARE(v0.x(), 1U);
    QCOMPARE(v0.y(), 2U);
    QCOMPARE(v0.z(), 3U);
    QCOMPARE(v0.w(), 4U);

    variant = QVariant::fromValue(rect);
    v0 = variant.value<quvec4>();
    QCOMPARE(v0.x(), 1U);
    QCOMPARE(v0.y(), 2U);
    QCOMPARE(v0.z(), 3U);
    QCOMPARE(v0.w(), 4U);

}

void Vector4Test::testdvec4()
{
    qdvec4 v0(1.0, 2.0, 3.0, 4.0);
    QCOMPARE(v0.x(), 1.0);
    QCOMPARE(v0.y(), 2.0);
    QCOMPARE(v0.z(), 3.0);
    QCOMPARE(v0.w(), 4.0);

    v0.setX(5.0);
    v0.setY(6.0);
    v0.setZ(7.0);
    v0.setW(8.0);

    QCOMPARE(v0.x(), 5.0);
    QCOMPARE(v0.y(), 6.0);
    QCOMPARE(v0.z(), 7.0);
    QCOMPARE(v0.w(), 8.0);

    QCOMPARE(v0.r(), 5.0);
    QCOMPARE(v0.g(), 6.0);
    QCOMPARE(v0.b(), 7.0);
    QCOMPARE(v0.a(), 8.0);

    v0.setR(2.0);
    v0.setG(4.0);
    v0.setB(6.0);
    v0.setA(8.0);

    QCOMPARE(v0.x(), 2.0);
    QCOMPARE(v0.y(), 4.0);
    QCOMPARE(v0.z(), 6.0);
    QCOMPARE(v0.w(), 8.0);

    qdvec4 v1;

    QCOMPARE(v1.x(), 0.0);
    QCOMPARE(v1.y(), 0.0);
    QCOMPARE(v1.z(), 0.0);
    QCOMPARE(v1.w(), 0.0);


    QCOMPARE(qdvec4::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qdvec4::staticMetaObject.property(1).name(), "y");
    QCOMPARE(qdvec4::staticMetaObject.property(2).name(), "z");
    QCOMPARE(qdvec4::staticMetaObject.property(3).name(), "w");
    QCOMPARE(qdvec4::staticMetaObject.property(4).name(), "r");
    QCOMPARE(qdvec4::staticMetaObject.property(5).name(), "g");
    QCOMPARE(qdvec4::staticMetaObject.property(6).name(), "b");
    QCOMPARE(qdvec4::staticMetaObject.property(7).name(), "a");

    qdvec4::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(2.0));
    qdvec4::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(4.0));
    qdvec4::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(6.0));
    qdvec4::staticMetaObject.property(3).writeOnGadget(&v1, QVariant::fromValue(8.0));

    QCOMPARE(v0, v1);

    QCOMPARE(qdvec4::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(2.0));
    QCOMPARE(qdvec4::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(4.0));
    QCOMPARE(qdvec4::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(6.0));
    QCOMPARE(qdvec4::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(8.0));
    QCOMPARE(qdvec4::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(2.0));
    QCOMPARE(qdvec4::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(4.0));
    QCOMPARE(qdvec4::staticMetaObject.property(6).readOnGadget(&v1), QVariant::fromValue(6.0));
    QCOMPARE(qdvec4::staticMetaObject.property(7).readOnGadget(&v1), QVariant::fromValue(8.0));

    v0.setR(0.0/3.0);
    v0.setG(1.0/3.0);
    v0.setB(2.0/3.0);
    v0.setA(3.0/3.0);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector4D>());
    QVERIFY(variant.canConvert<QString>());
    QVERIFY(variant.canConvert<QQuaternion>());
    QVERIFY(variant.canConvert<QRect>());
    QVERIFY(variant.canConvert<QRectF>());

    QVector4D vector4d = variant.value<QVector4D>();
    QCOMPARE(vector4d.x(), 0.0f/3.0f);
    QCOMPARE(vector4d.y(), 1.0f/3.0f);
    QCOMPARE(vector4d.z(), 2.0f/3.0f);
    QCOMPARE(vector4d.w(), 3.0f/3.0f);

    QColor color = variant.value<QColor>();
    QCOMPARE(color.redF(), 0.0);
    QCOMPARE(color.greenF(), 1.0/3.0);
    QCOMPARE(color.blueF(), 2.0/3.0);
    QCOMPARE(color.alphaF(), 1.0);

    QQuaternion quaternion = variant.value<QQuaternion>();
    QCOMPARE(quaternion.x(), 0.0f/3.0f);
    QCOMPARE(quaternion.y(), 1.0f/3.0f);
    QCOMPARE(quaternion.z(), 2.0f/3.0f);
    QCOMPARE(quaternion.scalar(), 3.0f/3.0f);

    QRectF rectF = variant.value<QRectF>();
    QCOMPARE(rectF.x(), 0.0);
    QCOMPARE(rectF.y(), 1.0/3.0);
    QCOMPARE(rectF.width(), 2.0/3.0);
    QCOMPARE(rectF.height(), 1.0);

    v0.setR(1.0);
    v0.setG(2.0);
    v0.setB(3.0);
    v0.setA(4.0);

    variant = QVariant::fromValue(v0);

    QRect rect = variant.value<QRect>();
    QCOMPARE(rect.x(), 1);
    QCOMPARE(rect.y(), 2);
    QCOMPARE(rect.width(), 3);
    QCOMPARE(rect.height(), 4);

    variant = QVariant::fromValue(vector4d);
    v0 = variant.value<qdvec4>();
    QCOMPARE(v0.x(), 0.0/3.0);
    QCOMPARE(v0.y(), double(float(1.0/3.0)));
    QCOMPARE(v0.z(), double(float(2.0/3.0)));
    QCOMPARE(v0.w(), 1.0);

    variant = QVariant::fromValue(color);
    v0 = variant.value<qdvec4>();
    QCOMPARE(v0.x(), 0.0/3.0);
    QCOMPARE(v0.y(), 1.0/3.0);
    QCOMPARE(v0.z(), 2.0/3.0);
    QCOMPARE(v0.w(), 3.0/3.0);


    variant = QVariant::fromValue(quaternion);
    v0 = variant.value<qdvec4>();
    QCOMPARE(v0.x(), 0.0);
    QCOMPARE(v0.y(), double(float(1.0/3.0)));
    QCOMPARE(v0.z(), double(float(2.0/3.0)));
    QCOMPARE(v0.w(), 1.0);

    variant = QVariant::fromValue(rectF);
    v0 = variant.value<qdvec4>();
    QCOMPARE(v0.x(), 0.0/3.0);
    QCOMPARE(v0.y(), 1.0/3.0);
    QCOMPARE(v0.z(), 2.0/3.0);
    QCOMPARE(v0.w(), 3.0/3.0);

    variant = QVariant::fromValue(rect);
    v0 = variant.value<qdvec4>();
    QCOMPARE(v0.x(), 1.0);
    QCOMPARE(v0.y(), 2.0);
    QCOMPARE(v0.z(), 3.0);
    QCOMPARE(v0.w(), 4.0);

    qdvec4 a(1,2,3,4);
    qdvec4 b(5,6,7,8);
    qdmat4 m(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
    m = m.transpose();
    QCOMPARE(a.dotProduct(b), 70.0);
    QCOMPARE(a.times(m), qdvec4(120,130,140,150));
    QCOMPARE(a.times(b), qdvec4(5,12,21,32));
    QCOMPARE(a.times(4.48), qdvec4(1.0*4.48, 2.0*4.48, 3.0*4.48, 4.0*4.48));
    QCOMPARE(a.plus(b), qdvec4(6,8,10,12));
    QCOMPARE(a.minus(b), qdvec4(-4,-4,-4,-4));
    QCOMPARE(a.normalized(), qdvec4(1.0/std::sqrt(30.0), 2.0/std::sqrt(30.0), 3.0/std::sqrt(30.0), 4.0/std::sqrt(30.0)));
    QCOMPARE(a.length(), std::sqrt(30.0));
}

void Vector4Test::testbvec4()
{
    qbvec4 v0(true,false,true,false);
    QCOMPARE(v0.x(), true);
    QCOMPARE(v0.y(), false);
    QCOMPARE(v0.z(), true);
    QCOMPARE(v0.w(), false);

    v0.setX(false);
    v0.setY(true);
    v0.setZ(true);
    v0.setW(false);

    QCOMPARE(v0.x(), false);
    QCOMPARE(v0.y(), true);
    QCOMPARE(v0.z(), true);
    QCOMPARE(v0.w(), false);

    QCOMPARE(v0.r(), false);
    QCOMPARE(v0.g(), true);
    QCOMPARE(v0.b(), true);
    QCOMPARE(v0.a(), false);

    v0.setR(true);
    v0.setG(true);
    v0.setB(false);
    v0.setA(true);

    QCOMPARE(v0.x(), true);
    QCOMPARE(v0.y(), true);
    QCOMPARE(v0.z(), false);
    QCOMPARE(v0.w(), true);

    qbvec4 v1;

    QCOMPARE(v1.x(), false);
    QCOMPARE(v1.y(), false);
    QCOMPARE(v1.z(), false);
    QCOMPARE(v1.w(), false);


    QCOMPARE(qbvec4::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qbvec4::staticMetaObject.property(1).name(), "y");
    QCOMPARE(qbvec4::staticMetaObject.property(2).name(), "z");
    QCOMPARE(qbvec4::staticMetaObject.property(3).name(), "w");
    QCOMPARE(qbvec4::staticMetaObject.property(4).name(), "r");
    QCOMPARE(qbvec4::staticMetaObject.property(5).name(), "g");
    QCOMPARE(qbvec4::staticMetaObject.property(6).name(), "b");
    QCOMPARE(qbvec4::staticMetaObject.property(7).name(), "a");

    qbvec4::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(true));
    qbvec4::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(true));
    qbvec4::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(false));
    qbvec4::staticMetaObject.property(3).writeOnGadget(&v1, QVariant::fromValue(true));

    QCOMPARE(v0, v1);

    QCOMPARE(qbvec4::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(true));
    QCOMPARE(qbvec4::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(true));
    QCOMPARE(qbvec4::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(false));
    QCOMPARE(qbvec4::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(true));
    QCOMPARE(qbvec4::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(true));
    QCOMPARE(qbvec4::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(true));
    QCOMPARE(qbvec4::staticMetaObject.property(6).readOnGadget(&v1), QVariant::fromValue(false));
    QCOMPARE(qbvec4::staticMetaObject.property(7).readOnGadget(&v1), QVariant::fromValue(true));

    v0.setR(true);
    v0.setG(false);
    v0.setB(true);
    v0.setA(false);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector4D>());

    QVector4D vector4d = variant.value<QVector4D>();
    QCOMPARE(vector4d.x(), 1.0f);
    QCOMPARE(vector4d.y(), 0.0f);
    QCOMPARE(vector4d.z(), 1.0f);
    QCOMPARE(vector4d.w(), 0.0f);

    variant = QVariant::fromValue(vector4d);
    v0 = variant.value<qbvec4>();
    QCOMPARE(v0.x(), true);
    QCOMPARE(v0.y(), false);
    QCOMPARE(v0.z(), true);
    QCOMPARE(v0.w(), false);

}

void Vector4Test::testSwizzle()
{
    qvec4 v(0,1,2,3);

    QCOMPARE(v.xx(), vector2<float>(0, 0));
    QCOMPARE(v.xy(), vector2<float>(0, 1));
    QCOMPARE(v.xz(), vector2<float>(0, 2));
    QCOMPARE(v.xw(), vector2<float>(0, 3));
    QCOMPARE(v.yx(), vector2<float>(1, 0));
    QCOMPARE(v.yy(), vector2<float>(1, 1));
    QCOMPARE(v.yz(), vector2<float>(1, 2));
    QCOMPARE(v.yw(), vector2<float>(1, 3));
    QCOMPARE(v.zx(), vector2<float>(2, 0));
    QCOMPARE(v.zy(), vector2<float>(2, 1));
    QCOMPARE(v.zz(), vector2<float>(2, 2));
    QCOMPARE(v.zw(), vector2<float>(2, 3));
    QCOMPARE(v.wx(), vector2<float>(3, 0));
    QCOMPARE(v.wy(), vector2<float>(3, 1));
    QCOMPARE(v.wz(), vector2<float>(3, 2));
    QCOMPARE(v.ww(), vector2<float>(3, 3));


    QCOMPARE(v.xxx(), vector3<float>(0, 0, 0));
    QCOMPARE(v.xxy(), vector3<float>(0, 0, 1));
    QCOMPARE(v.xxz(), vector3<float>(0, 0, 2));
    QCOMPARE(v.xxw(), vector3<float>(0, 0, 3));
    QCOMPARE(v.xyx(), vector3<float>(0, 1, 0));
    QCOMPARE(v.xyy(), vector3<float>(0, 1, 1));
    QCOMPARE(v.xyz(), vector3<float>(0, 1, 2));
    QCOMPARE(v.xyw(), vector3<float>(0, 1, 3));
    QCOMPARE(v.xzx(), vector3<float>(0, 2, 0));
    QCOMPARE(v.xzy(), vector3<float>(0, 2, 1));
    QCOMPARE(v.xzz(), vector3<float>(0, 2, 2));
    QCOMPARE(v.xzw(), vector3<float>(0, 2, 3));
    QCOMPARE(v.xwx(), vector3<float>(0, 3, 0));
    QCOMPARE(v.xwy(), vector3<float>(0, 3, 1));
    QCOMPARE(v.xwz(), vector3<float>(0, 3, 2));
    QCOMPARE(v.xww(), vector3<float>(0, 3, 3));
    QCOMPARE(v.yxx(), vector3<float>(1, 0, 0));
    QCOMPARE(v.yxy(), vector3<float>(1, 0, 1));
    QCOMPARE(v.yxz(), vector3<float>(1, 0, 2));
    QCOMPARE(v.yxw(), vector3<float>(1, 0, 3));
    QCOMPARE(v.yyx(), vector3<float>(1, 1, 0));
    QCOMPARE(v.yyy(), vector3<float>(1, 1, 1));
    QCOMPARE(v.yyz(), vector3<float>(1, 1, 2));
    QCOMPARE(v.yyw(), vector3<float>(1, 1, 3));
    QCOMPARE(v.yzx(), vector3<float>(1, 2, 0));
    QCOMPARE(v.yzy(), vector3<float>(1, 2, 1));
    QCOMPARE(v.yzz(), vector3<float>(1, 2, 2));
    QCOMPARE(v.yzw(), vector3<float>(1, 2, 3));
    QCOMPARE(v.ywx(), vector3<float>(1, 3, 0));
    QCOMPARE(v.ywy(), vector3<float>(1, 3, 1));
    QCOMPARE(v.ywz(), vector3<float>(1, 3, 2));
    QCOMPARE(v.yww(), vector3<float>(1, 3, 3));
    QCOMPARE(v.zxx(), vector3<float>(2, 0, 0));
    QCOMPARE(v.zxy(), vector3<float>(2, 0, 1));
    QCOMPARE(v.zxz(), vector3<float>(2, 0, 2));
    QCOMPARE(v.zxw(), vector3<float>(2, 0, 3));
    QCOMPARE(v.zyx(), vector3<float>(2, 1, 0));
    QCOMPARE(v.zyy(), vector3<float>(2, 1, 1));
    QCOMPARE(v.zyz(), vector3<float>(2, 1, 2));
    QCOMPARE(v.zyw(), vector3<float>(2, 1, 3));
    QCOMPARE(v.zzx(), vector3<float>(2, 2, 0));
    QCOMPARE(v.zzy(), vector3<float>(2, 2, 1));
    QCOMPARE(v.zzz(), vector3<float>(2, 2, 2));
    QCOMPARE(v.zzw(), vector3<float>(2, 2, 3));
    QCOMPARE(v.zwx(), vector3<float>(2, 3, 0));
    QCOMPARE(v.zwy(), vector3<float>(2, 3, 1));
    QCOMPARE(v.zwz(), vector3<float>(2, 3, 2));
    QCOMPARE(v.zww(), vector3<float>(2, 3, 3));
    QCOMPARE(v.wxx(), vector3<float>(3, 0, 0));
    QCOMPARE(v.wxy(), vector3<float>(3, 0, 1));
    QCOMPARE(v.wxz(), vector3<float>(3, 0, 2));
    QCOMPARE(v.wxw(), vector3<float>(3, 0, 3));
    QCOMPARE(v.wyx(), vector3<float>(3, 1, 0));
    QCOMPARE(v.wyy(), vector3<float>(3, 1, 1));
    QCOMPARE(v.wyz(), vector3<float>(3, 1, 2));
    QCOMPARE(v.wyw(), vector3<float>(3, 1, 3));
    QCOMPARE(v.wzx(), vector3<float>(3, 2, 0));
    QCOMPARE(v.wzy(), vector3<float>(3, 2, 1));
    QCOMPARE(v.wzz(), vector3<float>(3, 2, 2));
    QCOMPARE(v.wzw(), vector3<float>(3, 2, 3));
    QCOMPARE(v.wwx(), vector3<float>(3, 3, 0));
    QCOMPARE(v.wwy(), vector3<float>(3, 3, 1));
    QCOMPARE(v.wwz(), vector3<float>(3, 3, 2));
    QCOMPARE(v.www(), vector3<float>(3, 3, 3));


    QCOMPARE(v.xxxx(), vector4<float>(0, 0, 0, 0));
    QCOMPARE(v.xxxy(), vector4<float>(0, 0, 0, 1));
    QCOMPARE(v.xxxz(), vector4<float>(0, 0, 0, 2));
    QCOMPARE(v.xxxw(), vector4<float>(0, 0, 0, 3));
    QCOMPARE(v.xxyx(), vector4<float>(0, 0, 1, 0));
    QCOMPARE(v.xxyy(), vector4<float>(0, 0, 1, 1));
    QCOMPARE(v.xxyz(), vector4<float>(0, 0, 1, 2));
    QCOMPARE(v.xxyw(), vector4<float>(0, 0, 1, 3));
    QCOMPARE(v.xxzx(), vector4<float>(0, 0, 2, 0));
    QCOMPARE(v.xxzy(), vector4<float>(0, 0, 2, 1));
    QCOMPARE(v.xxzz(), vector4<float>(0, 0, 2, 2));
    QCOMPARE(v.xxzw(), vector4<float>(0, 0, 2, 3));
    QCOMPARE(v.xxwx(), vector4<float>(0, 0, 3, 0));
    QCOMPARE(v.xxwy(), vector4<float>(0, 0, 3, 1));
    QCOMPARE(v.xxwz(), vector4<float>(0, 0, 3, 2));
    QCOMPARE(v.xxww(), vector4<float>(0, 0, 3, 3));
    QCOMPARE(v.xyxx(), vector4<float>(0, 1, 0, 0));
    QCOMPARE(v.xyxy(), vector4<float>(0, 1, 0, 1));
    QCOMPARE(v.xyxz(), vector4<float>(0, 1, 0, 2));
    QCOMPARE(v.xyxw(), vector4<float>(0, 1, 0, 3));
    QCOMPARE(v.xyyx(), vector4<float>(0, 1, 1, 0));
    QCOMPARE(v.xyyy(), vector4<float>(0, 1, 1, 1));
    QCOMPARE(v.xyyz(), vector4<float>(0, 1, 1, 2));
    QCOMPARE(v.xyyw(), vector4<float>(0, 1, 1, 3));
    QCOMPARE(v.xyzx(), vector4<float>(0, 1, 2, 0));
    QCOMPARE(v.xyzy(), vector4<float>(0, 1, 2, 1));
    QCOMPARE(v.xyzz(), vector4<float>(0, 1, 2, 2));
    QCOMPARE(v.xyzw(), vector4<float>(0, 1, 2, 3));
    QCOMPARE(v.xywx(), vector4<float>(0, 1, 3, 0));
    QCOMPARE(v.xywy(), vector4<float>(0, 1, 3, 1));
    QCOMPARE(v.xywz(), vector4<float>(0, 1, 3, 2));
    QCOMPARE(v.xyww(), vector4<float>(0, 1, 3, 3));
    QCOMPARE(v.xzxx(), vector4<float>(0, 2, 0, 0));
    QCOMPARE(v.xzxy(), vector4<float>(0, 2, 0, 1));
    QCOMPARE(v.xzxz(), vector4<float>(0, 2, 0, 2));
    QCOMPARE(v.xzxw(), vector4<float>(0, 2, 0, 3));
    QCOMPARE(v.xzyx(), vector4<float>(0, 2, 1, 0));
    QCOMPARE(v.xzyy(), vector4<float>(0, 2, 1, 1));
    QCOMPARE(v.xzyz(), vector4<float>(0, 2, 1, 2));
    QCOMPARE(v.xzyw(), vector4<float>(0, 2, 1, 3));
    QCOMPARE(v.xzzx(), vector4<float>(0, 2, 2, 0));
    QCOMPARE(v.xzzy(), vector4<float>(0, 2, 2, 1));
    QCOMPARE(v.xzzz(), vector4<float>(0, 2, 2, 2));
    QCOMPARE(v.xzzw(), vector4<float>(0, 2, 2, 3));
    QCOMPARE(v.xzwx(), vector4<float>(0, 2, 3, 0));
    QCOMPARE(v.xzwy(), vector4<float>(0, 2, 3, 1));
    QCOMPARE(v.xzwz(), vector4<float>(0, 2, 3, 2));
    QCOMPARE(v.xzww(), vector4<float>(0, 2, 3, 3));
    QCOMPARE(v.xwxx(), vector4<float>(0, 3, 0, 0));
    QCOMPARE(v.xwxy(), vector4<float>(0, 3, 0, 1));
    QCOMPARE(v.xwxz(), vector4<float>(0, 3, 0, 2));
    QCOMPARE(v.xwxw(), vector4<float>(0, 3, 0, 3));
    QCOMPARE(v.xwyx(), vector4<float>(0, 3, 1, 0));
    QCOMPARE(v.xwyy(), vector4<float>(0, 3, 1, 1));
    QCOMPARE(v.xwyz(), vector4<float>(0, 3, 1, 2));
    QCOMPARE(v.xwyw(), vector4<float>(0, 3, 1, 3));
    QCOMPARE(v.xwzx(), vector4<float>(0, 3, 2, 0));
    QCOMPARE(v.xwzy(), vector4<float>(0, 3, 2, 1));
    QCOMPARE(v.xwzz(), vector4<float>(0, 3, 2, 2));
    QCOMPARE(v.xwzw(), vector4<float>(0, 3, 2, 3));
    QCOMPARE(v.xwwx(), vector4<float>(0, 3, 3, 0));
    QCOMPARE(v.xwwy(), vector4<float>(0, 3, 3, 1));
    QCOMPARE(v.xwwz(), vector4<float>(0, 3, 3, 2));
    QCOMPARE(v.xwww(), vector4<float>(0, 3, 3, 3));
    QCOMPARE(v.yxxx(), vector4<float>(1, 0, 0, 0));
    QCOMPARE(v.yxxy(), vector4<float>(1, 0, 0, 1));
    QCOMPARE(v.yxxz(), vector4<float>(1, 0, 0, 2));
    QCOMPARE(v.yxxw(), vector4<float>(1, 0, 0, 3));
    QCOMPARE(v.yxyx(), vector4<float>(1, 0, 1, 0));
    QCOMPARE(v.yxyy(), vector4<float>(1, 0, 1, 1));
    QCOMPARE(v.yxyz(), vector4<float>(1, 0, 1, 2));
    QCOMPARE(v.yxyw(), vector4<float>(1, 0, 1, 3));
    QCOMPARE(v.yxzx(), vector4<float>(1, 0, 2, 0));
    QCOMPARE(v.yxzy(), vector4<float>(1, 0, 2, 1));
    QCOMPARE(v.yxzz(), vector4<float>(1, 0, 2, 2));
    QCOMPARE(v.yxzw(), vector4<float>(1, 0, 2, 3));
    QCOMPARE(v.yxwx(), vector4<float>(1, 0, 3, 0));
    QCOMPARE(v.yxwy(), vector4<float>(1, 0, 3, 1));
    QCOMPARE(v.yxwz(), vector4<float>(1, 0, 3, 2));
    QCOMPARE(v.yxww(), vector4<float>(1, 0, 3, 3));
    QCOMPARE(v.yyxx(), vector4<float>(1, 1, 0, 0));
    QCOMPARE(v.yyxy(), vector4<float>(1, 1, 0, 1));
    QCOMPARE(v.yyxz(), vector4<float>(1, 1, 0, 2));
    QCOMPARE(v.yyxw(), vector4<float>(1, 1, 0, 3));
    QCOMPARE(v.yyyx(), vector4<float>(1, 1, 1, 0));
    QCOMPARE(v.yyyy(), vector4<float>(1, 1, 1, 1));
    QCOMPARE(v.yyyz(), vector4<float>(1, 1, 1, 2));
    QCOMPARE(v.yyyw(), vector4<float>(1, 1, 1, 3));
    QCOMPARE(v.yyzx(), vector4<float>(1, 1, 2, 0));
    QCOMPARE(v.yyzy(), vector4<float>(1, 1, 2, 1));
    QCOMPARE(v.yyzz(), vector4<float>(1, 1, 2, 2));
    QCOMPARE(v.yyzw(), vector4<float>(1, 1, 2, 3));
    QCOMPARE(v.yywx(), vector4<float>(1, 1, 3, 0));
    QCOMPARE(v.yywy(), vector4<float>(1, 1, 3, 1));
    QCOMPARE(v.yywz(), vector4<float>(1, 1, 3, 2));
    QCOMPARE(v.yyww(), vector4<float>(1, 1, 3, 3));
    QCOMPARE(v.yzxx(), vector4<float>(1, 2, 0, 0));
    QCOMPARE(v.yzxy(), vector4<float>(1, 2, 0, 1));
    QCOMPARE(v.yzxz(), vector4<float>(1, 2, 0, 2));
    QCOMPARE(v.yzxw(), vector4<float>(1, 2, 0, 3));
    QCOMPARE(v.yzyx(), vector4<float>(1, 2, 1, 0));
    QCOMPARE(v.yzyy(), vector4<float>(1, 2, 1, 1));
    QCOMPARE(v.yzyz(), vector4<float>(1, 2, 1, 2));
    QCOMPARE(v.yzyw(), vector4<float>(1, 2, 1, 3));
    QCOMPARE(v.yzzx(), vector4<float>(1, 2, 2, 0));
    QCOMPARE(v.yzzy(), vector4<float>(1, 2, 2, 1));
    QCOMPARE(v.yzzz(), vector4<float>(1, 2, 2, 2));
    QCOMPARE(v.yzzw(), vector4<float>(1, 2, 2, 3));
    QCOMPARE(v.yzwx(), vector4<float>(1, 2, 3, 0));
    QCOMPARE(v.yzwy(), vector4<float>(1, 2, 3, 1));
    QCOMPARE(v.yzwz(), vector4<float>(1, 2, 3, 2));
    QCOMPARE(v.yzww(), vector4<float>(1, 2, 3, 3));
    QCOMPARE(v.ywxx(), vector4<float>(1, 3, 0, 0));
    QCOMPARE(v.ywxy(), vector4<float>(1, 3, 0, 1));
    QCOMPARE(v.ywxz(), vector4<float>(1, 3, 0, 2));
    QCOMPARE(v.ywxw(), vector4<float>(1, 3, 0, 3));
    QCOMPARE(v.ywyx(), vector4<float>(1, 3, 1, 0));
    QCOMPARE(v.ywyy(), vector4<float>(1, 3, 1, 1));
    QCOMPARE(v.ywyz(), vector4<float>(1, 3, 1, 2));
    QCOMPARE(v.ywyw(), vector4<float>(1, 3, 1, 3));
    QCOMPARE(v.ywzx(), vector4<float>(1, 3, 2, 0));
    QCOMPARE(v.ywzy(), vector4<float>(1, 3, 2, 1));
    QCOMPARE(v.ywzz(), vector4<float>(1, 3, 2, 2));
    QCOMPARE(v.ywzw(), vector4<float>(1, 3, 2, 3));
    QCOMPARE(v.ywwx(), vector4<float>(1, 3, 3, 0));
    QCOMPARE(v.ywwy(), vector4<float>(1, 3, 3, 1));
    QCOMPARE(v.ywwz(), vector4<float>(1, 3, 3, 2));
    QCOMPARE(v.ywww(), vector4<float>(1, 3, 3, 3));
    QCOMPARE(v.zxxx(), vector4<float>(2, 0, 0, 0));
    QCOMPARE(v.zxxy(), vector4<float>(2, 0, 0, 1));
    QCOMPARE(v.zxxz(), vector4<float>(2, 0, 0, 2));
    QCOMPARE(v.zxxw(), vector4<float>(2, 0, 0, 3));
    QCOMPARE(v.zxyx(), vector4<float>(2, 0, 1, 0));
    QCOMPARE(v.zxyy(), vector4<float>(2, 0, 1, 1));
    QCOMPARE(v.zxyz(), vector4<float>(2, 0, 1, 2));
    QCOMPARE(v.zxyw(), vector4<float>(2, 0, 1, 3));
    QCOMPARE(v.zxzx(), vector4<float>(2, 0, 2, 0));
    QCOMPARE(v.zxzy(), vector4<float>(2, 0, 2, 1));
    QCOMPARE(v.zxzz(), vector4<float>(2, 0, 2, 2));
    QCOMPARE(v.zxzw(), vector4<float>(2, 0, 2, 3));
    QCOMPARE(v.zxwx(), vector4<float>(2, 0, 3, 0));
    QCOMPARE(v.zxwy(), vector4<float>(2, 0, 3, 1));
    QCOMPARE(v.zxwz(), vector4<float>(2, 0, 3, 2));
    QCOMPARE(v.zxww(), vector4<float>(2, 0, 3, 3));
    QCOMPARE(v.zyxx(), vector4<float>(2, 1, 0, 0));
    QCOMPARE(v.zyxy(), vector4<float>(2, 1, 0, 1));
    QCOMPARE(v.zyxz(), vector4<float>(2, 1, 0, 2));
    QCOMPARE(v.zyxw(), vector4<float>(2, 1, 0, 3));
    QCOMPARE(v.zyyx(), vector4<float>(2, 1, 1, 0));
    QCOMPARE(v.zyyy(), vector4<float>(2, 1, 1, 1));
    QCOMPARE(v.zyyz(), vector4<float>(2, 1, 1, 2));
    QCOMPARE(v.zyyw(), vector4<float>(2, 1, 1, 3));
    QCOMPARE(v.zyzx(), vector4<float>(2, 1, 2, 0));
    QCOMPARE(v.zyzy(), vector4<float>(2, 1, 2, 1));
    QCOMPARE(v.zyzz(), vector4<float>(2, 1, 2, 2));
    QCOMPARE(v.zyzw(), vector4<float>(2, 1, 2, 3));
    QCOMPARE(v.zywx(), vector4<float>(2, 1, 3, 0));
    QCOMPARE(v.zywy(), vector4<float>(2, 1, 3, 1));
    QCOMPARE(v.zywz(), vector4<float>(2, 1, 3, 2));
    QCOMPARE(v.zyww(), vector4<float>(2, 1, 3, 3));
    QCOMPARE(v.zzxx(), vector4<float>(2, 2, 0, 0));
    QCOMPARE(v.zzxy(), vector4<float>(2, 2, 0, 1));
    QCOMPARE(v.zzxz(), vector4<float>(2, 2, 0, 2));
    QCOMPARE(v.zzxw(), vector4<float>(2, 2, 0, 3));
    QCOMPARE(v.zzyx(), vector4<float>(2, 2, 1, 0));
    QCOMPARE(v.zzyy(), vector4<float>(2, 2, 1, 1));
    QCOMPARE(v.zzyz(), vector4<float>(2, 2, 1, 2));
    QCOMPARE(v.zzyw(), vector4<float>(2, 2, 1, 3));
    QCOMPARE(v.zzzx(), vector4<float>(2, 2, 2, 0));
    QCOMPARE(v.zzzy(), vector4<float>(2, 2, 2, 1));
    QCOMPARE(v.zzzz(), vector4<float>(2, 2, 2, 2));
    QCOMPARE(v.zzzw(), vector4<float>(2, 2, 2, 3));
    QCOMPARE(v.zzwx(), vector4<float>(2, 2, 3, 0));
    QCOMPARE(v.zzwy(), vector4<float>(2, 2, 3, 1));
    QCOMPARE(v.zzwz(), vector4<float>(2, 2, 3, 2));
    QCOMPARE(v.zzww(), vector4<float>(2, 2, 3, 3));
    QCOMPARE(v.zwxx(), vector4<float>(2, 3, 0, 0));
    QCOMPARE(v.zwxy(), vector4<float>(2, 3, 0, 1));
    QCOMPARE(v.zwxz(), vector4<float>(2, 3, 0, 2));
    QCOMPARE(v.zwxw(), vector4<float>(2, 3, 0, 3));
    QCOMPARE(v.zwyx(), vector4<float>(2, 3, 1, 0));
    QCOMPARE(v.zwyy(), vector4<float>(2, 3, 1, 1));
    QCOMPARE(v.zwyz(), vector4<float>(2, 3, 1, 2));
    QCOMPARE(v.zwyw(), vector4<float>(2, 3, 1, 3));
    QCOMPARE(v.zwzx(), vector4<float>(2, 3, 2, 0));
    QCOMPARE(v.zwzy(), vector4<float>(2, 3, 2, 1));
    QCOMPARE(v.zwzz(), vector4<float>(2, 3, 2, 2));
    QCOMPARE(v.zwzw(), vector4<float>(2, 3, 2, 3));
    QCOMPARE(v.zwwx(), vector4<float>(2, 3, 3, 0));
    QCOMPARE(v.zwwy(), vector4<float>(2, 3, 3, 1));
    QCOMPARE(v.zwwz(), vector4<float>(2, 3, 3, 2));
    QCOMPARE(v.zwww(), vector4<float>(2, 3, 3, 3));
    QCOMPARE(v.wxxx(), vector4<float>(3, 0, 0, 0));
    QCOMPARE(v.wxxy(), vector4<float>(3, 0, 0, 1));
    QCOMPARE(v.wxxz(), vector4<float>(3, 0, 0, 2));
    QCOMPARE(v.wxxw(), vector4<float>(3, 0, 0, 3));
    QCOMPARE(v.wxyx(), vector4<float>(3, 0, 1, 0));
    QCOMPARE(v.wxyy(), vector4<float>(3, 0, 1, 1));
    QCOMPARE(v.wxyz(), vector4<float>(3, 0, 1, 2));
    QCOMPARE(v.wxyw(), vector4<float>(3, 0, 1, 3));
    QCOMPARE(v.wxzx(), vector4<float>(3, 0, 2, 0));
    QCOMPARE(v.wxzy(), vector4<float>(3, 0, 2, 1));
    QCOMPARE(v.wxzz(), vector4<float>(3, 0, 2, 2));
    QCOMPARE(v.wxzw(), vector4<float>(3, 0, 2, 3));
    QCOMPARE(v.wxwx(), vector4<float>(3, 0, 3, 0));
    QCOMPARE(v.wxwy(), vector4<float>(3, 0, 3, 1));
    QCOMPARE(v.wxwz(), vector4<float>(3, 0, 3, 2));
    QCOMPARE(v.wxww(), vector4<float>(3, 0, 3, 3));
    QCOMPARE(v.wyxx(), vector4<float>(3, 1, 0, 0));
    QCOMPARE(v.wyxy(), vector4<float>(3, 1, 0, 1));
    QCOMPARE(v.wyxz(), vector4<float>(3, 1, 0, 2));
    QCOMPARE(v.wyxw(), vector4<float>(3, 1, 0, 3));
    QCOMPARE(v.wyyx(), vector4<float>(3, 1, 1, 0));
    QCOMPARE(v.wyyy(), vector4<float>(3, 1, 1, 1));
    QCOMPARE(v.wyyz(), vector4<float>(3, 1, 1, 2));
    QCOMPARE(v.wyyw(), vector4<float>(3, 1, 1, 3));
    QCOMPARE(v.wyzx(), vector4<float>(3, 1, 2, 0));
    QCOMPARE(v.wyzy(), vector4<float>(3, 1, 2, 1));
    QCOMPARE(v.wyzz(), vector4<float>(3, 1, 2, 2));
    QCOMPARE(v.wyzw(), vector4<float>(3, 1, 2, 3));
    QCOMPARE(v.wywx(), vector4<float>(3, 1, 3, 0));
    QCOMPARE(v.wywy(), vector4<float>(3, 1, 3, 1));
    QCOMPARE(v.wywz(), vector4<float>(3, 1, 3, 2));
    QCOMPARE(v.wyww(), vector4<float>(3, 1, 3, 3));
    QCOMPARE(v.wzxx(), vector4<float>(3, 2, 0, 0));
    QCOMPARE(v.wzxy(), vector4<float>(3, 2, 0, 1));
    QCOMPARE(v.wzxz(), vector4<float>(3, 2, 0, 2));
    QCOMPARE(v.wzxw(), vector4<float>(3, 2, 0, 3));
    QCOMPARE(v.wzyx(), vector4<float>(3, 2, 1, 0));
    QCOMPARE(v.wzyy(), vector4<float>(3, 2, 1, 1));
    QCOMPARE(v.wzyz(), vector4<float>(3, 2, 1, 2));
    QCOMPARE(v.wzyw(), vector4<float>(3, 2, 1, 3));
    QCOMPARE(v.wzzx(), vector4<float>(3, 2, 2, 0));
    QCOMPARE(v.wzzy(), vector4<float>(3, 2, 2, 1));
    QCOMPARE(v.wzzz(), vector4<float>(3, 2, 2, 2));
    QCOMPARE(v.wzzw(), vector4<float>(3, 2, 2, 3));
    QCOMPARE(v.wzwx(), vector4<float>(3, 2, 3, 0));
    QCOMPARE(v.wzwy(), vector4<float>(3, 2, 3, 1));
    QCOMPARE(v.wzwz(), vector4<float>(3, 2, 3, 2));
    QCOMPARE(v.wzww(), vector4<float>(3, 2, 3, 3));
    QCOMPARE(v.wwxx(), vector4<float>(3, 3, 0, 0));
    QCOMPARE(v.wwxy(), vector4<float>(3, 3, 0, 1));
    QCOMPARE(v.wwxz(), vector4<float>(3, 3, 0, 2));
    QCOMPARE(v.wwxw(), vector4<float>(3, 3, 0, 3));
    QCOMPARE(v.wwyx(), vector4<float>(3, 3, 1, 0));
    QCOMPARE(v.wwyy(), vector4<float>(3, 3, 1, 1));
    QCOMPARE(v.wwyz(), vector4<float>(3, 3, 1, 2));
    QCOMPARE(v.wwyw(), vector4<float>(3, 3, 1, 3));
    QCOMPARE(v.wwzx(), vector4<float>(3, 3, 2, 0));
    QCOMPARE(v.wwzy(), vector4<float>(3, 3, 2, 1));
    QCOMPARE(v.wwzz(), vector4<float>(3, 3, 2, 2));
    QCOMPARE(v.wwzw(), vector4<float>(3, 3, 2, 3));
    QCOMPARE(v.wwwx(), vector4<float>(3, 3, 3, 0));
    QCOMPARE(v.wwwy(), vector4<float>(3, 3, 3, 1));
    QCOMPARE(v.wwwz(), vector4<float>(3, 3, 3, 2));
    QCOMPARE(v.wwww(), vector4<float>(3, 3, 3, 3));
}


QTEST_APPLESS_MAIN(Vector4Test)

#include "tst_vector4test.moc"

TEMPLATE = subdirs

SUBDIRS += \
    vector2 \
    vector3 \
    vector4 \
    matrix2 \
    matrix3 \
    matrix4 \
    graphicsprogram

!macos {
    SUBDIRS += shaderstoragebuffer
}

#include <QString>
#include <QtTest>

#include "quickmatrix3.h"
#include "quickopengl_plugin.h"

class Matrix3Test : public QObject
{
    Q_OBJECT

public:
    Matrix3Test();

private Q_SLOTS:

    void testmat3();
};

Matrix3Test::Matrix3Test()
{
    // register types
    QuickOpenGLPlugin plugin;
    plugin.registerTypes("QuickOpenGL");
}

void Matrix3Test::testmat3()
{
    QVERIFY2(true, "Failure");
}

QTEST_APPLESS_MAIN(Matrix3Test)

#include "tst_matrix3test.moc"

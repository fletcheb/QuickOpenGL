QT       += testlib gui positioning qml quick
CONFIG   += testcase no_testcase_installs
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app

INCLUDEPATH += ../../../src/
DEPENDPATH += ../../../src/

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../../src/QuickOpenGL/ -lquickopengl
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../../src/QuickOpenGL/ -lquickopengl
else:macx:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../../src/QuickOpenGL/ -lquickopengl_debug
else:unix: LIBS += -L$$OUT_PWD/../../../src/QuickOpenGL/ -lquickopengl

QMAKE_RPATHDIR += ../../../src/QuickOpenGL/

#include <QString>
#include <QtTest>

#include "quickvector3.h"
#include "quickopengl_plugin.h"
#include "quickmatrix4.h"

class Vector3Test : public QObject
{
    Q_OBJECT

public:
    Vector3Test();

private Q_SLOTS:

    void testvec3();
    void testivec3();
    void testuvec3();
    void testdvec3();
    void testbvec3();
    void testSwizzle();

};

Vector3Test::Vector3Test()
{
    // register types
    QuickOpenGLPlugin plugin;
    plugin.registerTypes("QuickOpenGL");
}

void Vector3Test::testvec3()
{
    qvec3 v0(1.0f, 2.0f, 3.0f);
    QCOMPARE(v0.x(), 1.0f);
    QCOMPARE(v0.y(), 2.0f);
    QCOMPARE(v0.z(), 3.0f);

    v0.setX(4.0f);
    v0.setY(5.0f);
    v0.setZ(6.0f);

    QCOMPARE(v0.x(), 4.0f);
    QCOMPARE(v0.y(), 5.0f);
    QCOMPARE(v0.z(), 6.0f);

    QCOMPARE(v0.r(), 4.0f);
    QCOMPARE(v0.g(), 5.0f);
    QCOMPARE(v0.b(), 6.0f);

    v0.setR(7.0f);
    v0.setG(8.0f);
    v0.setB(9.0f);

    QCOMPARE(v0.x(), 7.0f);
    QCOMPARE(v0.y(), 8.0f);
    QCOMPARE(v0.z(), 9.0f);

    qvec3 v1;

    QCOMPARE(v1.x(), 0.0f);
    QCOMPARE(v1.y(), 0.0f);
    QCOMPARE(v1.z(), 0.0f);

    QCOMPARE(qvec3::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qvec3::staticMetaObject.property(1).name(), "y");
    QCOMPARE(qvec3::staticMetaObject.property(2).name(), "z");
    QCOMPARE(qvec3::staticMetaObject.property(3).name(), "r");
    QCOMPARE(qvec3::staticMetaObject.property(4).name(), "g");
    QCOMPARE(qvec3::staticMetaObject.property(5).name(), "b");

    qvec3::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(7.0f));
    qvec3::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(8.0f));
    qvec3::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(9.0f));

    QCOMPARE(v0, v1);

    QCOMPARE(qvec3::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(7.0f));
    QCOMPARE(qvec3::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(8.0f));
    QCOMPARE(qvec3::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(9.0f));
    QCOMPARE(qvec3::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(7.0f));
    QCOMPARE(qvec3::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(8.0f));
    QCOMPARE(qvec3::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(9.0f));

    v0.setR(1.0f/3.0f);
    v0.setG(2.0f/3.0f);
    v0.setB(1.0f);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QGeoCoordinate>());
    QVERIFY(variant.canConvert<QVector3D>());
    QVERIFY(variant.canConvert<QColor>());
    QVERIFY(variant.canConvert<QString>());

    QGeoCoordinate geoCoordinate = variant.value<QGeoCoordinate>();
    QCOMPARE(geoCoordinate.longitude(), double(1.0f/3.0f));
    QCOMPARE(geoCoordinate.latitude(), double(2.0f/3.0f));
    QCOMPARE(geoCoordinate.altitude(), 1.0);

    QVector3D vector3d = variant.value<QVector3D>();
    QCOMPARE(vector3d.x(), 1.0f/3.0f);
    QCOMPARE(vector3d.y(), 2.0f/3.0f);
    QCOMPARE(vector3d.z(), 1.0f);

    QColor color = variant.value<QColor>();
    QCOMPARE(color.redF(), 1.0/3.0);
    QCOMPARE(color.greenF(), 2.0/3.0);
    QCOMPARE(color.blueF(), 1.0);

    QString string  = variant.value<QString>();
    QCOMPARE(string, QString("#55aaff"));

    variant = QVariant::fromValue(geoCoordinate);
    v0 = variant.value<qvec3>();
    QCOMPARE(v0.x(), 1.0f/3.0f);
    QCOMPARE(v0.y(), 2.0f/3.0f);
    QCOMPARE(v0.z(), 1.0f);

    variant = QVariant::fromValue(vector3d);
    v0 = variant.value<qvec3>();
    QCOMPARE(v0.x(), 1.0f/3.0f);
    QCOMPARE(v0.y(), 2.0f/3.0f);
    QCOMPARE(v0.z(), 1.0f);

    variant = QVariant::fromValue(color);
    v0 = variant.value<qvec3>();
    QCOMPARE(v0.x(), 1.0f/3.0f);
    QCOMPARE(v0.y(), 2.0f/3.0f);
    QCOMPARE(v0.z(), 1.0f);

    variant = QVariant::fromValue(QColor(string));
    v0 = variant.value<qvec3>();
    QCOMPARE(v0.x(), 1.0f/3.0f);
    QCOMPARE(v0.y(), 2.0f/3.0f);
    QCOMPARE(v0.z(), 1.0f);

    qvec3 a(1,2,3);
    qvec3 b(4,5,6);
    QCOMPARE(a.crossProduct(b), qvec3(-3,6,-3));
    QCOMPARE(a.dotProduct(b), 32.0f);
    qmat4 m(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
    m = m.transpose();
    QCOMPARE(a.times(m), qvec3(72.0f/93.0f, 79.0f/93.0f, 86.0f/93.0f));

    QCOMPARE(a.times(b), qvec3(4,10,18));
    QCOMPARE(a.times(4.48f), qvec3(1.0f*4.48f, 2.0f*4.48f, 3.0f*4.48f));
    QCOMPARE(a.plus(b), qvec3(5,7,9));
    QCOMPARE(a.minus(b), qvec3(-3,-3,-3));
    QCOMPARE(a.normalized(), qvec3(1.0f/std::sqrt(14.0f), 2.0f/std::sqrt(14.0f), 3.0f/std::sqrt(14.0f)));
    QCOMPARE(a.length(), std::sqrt(14.0f));

}

void Vector3Test::testivec3()
{
    qivec3 v0(1, 2, 3);
    QCOMPARE(v0.x(), 1);
    QCOMPARE(v0.y(), 2);
    QCOMPARE(v0.z(), 3);

    v0.setX(4);
    v0.setY(5);
    v0.setZ(6);

    QCOMPARE(v0.x(), 4);
    QCOMPARE(v0.y(), 5);
    QCOMPARE(v0.z(), 6);

    QCOMPARE(v0.r(), 4);
    QCOMPARE(v0.g(), 5);
    QCOMPARE(v0.b(), 6);

    v0.setR(7);
    v0.setG(8);
    v0.setB(9);

    QCOMPARE(v0.x(), 7);
    QCOMPARE(v0.y(), 8);
    QCOMPARE(v0.z(), 9);

    qivec3 v1;

    QCOMPARE(v1.x(), 0);
    QCOMPARE(v1.y(), 0);
    QCOMPARE(v1.z(), 0);

    QCOMPARE(qivec3::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qivec3::staticMetaObject.property(1).name(), "y");
    QCOMPARE(qivec3::staticMetaObject.property(2).name(), "z");
    QCOMPARE(qivec3::staticMetaObject.property(3).name(), "r");
    QCOMPARE(qivec3::staticMetaObject.property(4).name(), "g");
    QCOMPARE(qivec3::staticMetaObject.property(5).name(), "b");

    qivec3::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(7));
    qivec3::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(8));
    qivec3::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(9));

    QCOMPARE(v0, v1);

    QCOMPARE(qivec3::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(7));
    QCOMPARE(qivec3::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(8));
    QCOMPARE(qivec3::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(9));
    QCOMPARE(qivec3::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(7));
    QCOMPARE(qivec3::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(8));
    QCOMPARE(qivec3::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(9));

    v0.setR(32);
    v0.setG(64);
    v0.setB(128);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector3D>());
    QVERIFY(variant.canConvert<QColor>());
    QVERIFY(variant.canConvert<QString>());

    QVector3D vector3d = variant.value<QVector3D>();
    QCOMPARE(vector3d.x(), 32.0f);
    QCOMPARE(vector3d.y(), 64.0f);
    QCOMPARE(vector3d.z(), 128.0f);

    QColor color = variant.value<QColor>();
    QCOMPARE(color.red(), 32);
    QCOMPARE(color.green(), 64);
    QCOMPARE(color.blue(), 128);

    QString string  = variant.value<QString>();
    QCOMPARE(string, QString("#204080"));

    variant = QVariant::fromValue(vector3d);
    v0 = variant.value<qivec3>();
    QCOMPARE(v0.x(), 32);
    QCOMPARE(v0.y(), 64);
    QCOMPARE(v0.z(), 128);

    variant = QVariant::fromValue(color);
    v0 = variant.value<qivec3>();
    QCOMPARE(v0.x(), 32);
    QCOMPARE(v0.y(), 64);
    QCOMPARE(v0.z(), 128);

    variant = QVariant::fromValue(QColor(string));
    v0 = variant.value<qivec3>();
    QCOMPARE(v0.x(), 32);
    QCOMPARE(v0.y(), 64);
    QCOMPARE(v0.z(), 128);

}

void Vector3Test::testuvec3()
{

    quvec3 v0(1U, 2U, 3U);
    QCOMPARE(v0.x(), 1U);
    QCOMPARE(v0.y(), 2U);
    QCOMPARE(v0.z(), 3U);

    v0.setX(4U);
    v0.setY(5U);
    v0.setZ(6U);

    QCOMPARE(v0.x(), 4U);
    QCOMPARE(v0.y(), 5U);
    QCOMPARE(v0.z(), 6U);

    QCOMPARE(v0.r(), 4U);
    QCOMPARE(v0.g(), 5U);
    QCOMPARE(v0.b(), 6U);

    v0.setR(7U);
    v0.setG(8U);
    v0.setB(9U);

    QCOMPARE(v0.x(), 7U);
    QCOMPARE(v0.y(), 8U);
    QCOMPARE(v0.z(), 9U);

    quvec3 v1;

    QCOMPARE(v1.x(), 0U);
    QCOMPARE(v1.y(), 0U);
    QCOMPARE(v1.z(), 0U);

    QCOMPARE(quvec3::staticMetaObject.property(0).name(), "x");
    QCOMPARE(quvec3::staticMetaObject.property(1).name(), "y");
    QCOMPARE(quvec3::staticMetaObject.property(2).name(), "z");
    QCOMPARE(quvec3::staticMetaObject.property(3).name(), "r");
    QCOMPARE(quvec3::staticMetaObject.property(4).name(), "g");
    QCOMPARE(quvec3::staticMetaObject.property(5).name(), "b");

    quvec3::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(7U));
    quvec3::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(8U));
    quvec3::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(9U));

    QCOMPARE(v0, v1);

    QCOMPARE(quvec3::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(7U));
    QCOMPARE(quvec3::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(8U));
    QCOMPARE(quvec3::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(9U));
    QCOMPARE(quvec3::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(7U));
    QCOMPARE(quvec3::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(8U));
    QCOMPARE(quvec3::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(9U));

    v0.setR(32U);
    v0.setG(64U);
    v0.setB(128U);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector3D>());
    QVERIFY(variant.canConvert<QColor>());
    QVERIFY(variant.canConvert<QString>());

    QVector3D vector3d = variant.value<QVector3D>();
    QCOMPARE(vector3d.x(), 32.0f);
    QCOMPARE(vector3d.y(), 64.0f);
    QCOMPARE(vector3d.z(), 128.0f);

    QColor color = variant.value<QColor>();
    QCOMPARE(color.red(), 32);
    QCOMPARE(color.green(), 64);
    QCOMPARE(color.blue(), 128);

    QString string  = variant.value<QString>();
    QCOMPARE(string, QString("#204080"));

    variant = QVariant::fromValue(vector3d);
    v0 = variant.value<quvec3>();
    QCOMPARE(v0.x(), 32U);
    QCOMPARE(v0.y(), 64U);
    QCOMPARE(v0.z(), 128U);

    variant = QVariant::fromValue(color);
    v0 = variant.value<quvec3>();
    QCOMPARE(v0.x(), 32U);
    QCOMPARE(v0.y(), 64U);
    QCOMPARE(v0.z(), 128U);

    variant = QVariant::fromValue(QColor(string));
    v0 = variant.value<quvec3>();
    QCOMPARE(v0.x(), 32U);
    QCOMPARE(v0.y(), 64U);
    QCOMPARE(v0.z(), 128U);

}

void Vector3Test::testdvec3()
{
    qdvec3 v0(1.0, 2.0, 3.0);
    QCOMPARE(v0.x(), 1.0);
    QCOMPARE(v0.y(), 2.0);
    QCOMPARE(v0.z(), 3.0);

    v0.setX(4.0);
    v0.setY(5.0);
    v0.setZ(6.0);

    QCOMPARE(v0.x(), 4.0);
    QCOMPARE(v0.y(), 5.0);
    QCOMPARE(v0.z(), 6.0);

    QCOMPARE(v0.r(), 4.0);
    QCOMPARE(v0.g(), 5.0);
    QCOMPARE(v0.b(), 6.0);

    v0.setR(7.0);
    v0.setG(8.0);
    v0.setB(9.0);

    QCOMPARE(v0.x(), 7.0);
    QCOMPARE(v0.y(), 8.0);
    QCOMPARE(v0.z(), 9.0);

    qdvec3 v1;

    QCOMPARE(v1.x(), 0.0);
    QCOMPARE(v1.y(), 0.0);
    QCOMPARE(v1.z(), 0.0);

    QCOMPARE(qdvec3::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qdvec3::staticMetaObject.property(1).name(), "y");
    QCOMPARE(qdvec3::staticMetaObject.property(2).name(), "z");
    QCOMPARE(qdvec3::staticMetaObject.property(3).name(), "r");
    QCOMPARE(qdvec3::staticMetaObject.property(4).name(), "g");
    QCOMPARE(qdvec3::staticMetaObject.property(5).name(), "b");

    qdvec3::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(7.0));
    qdvec3::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(8.0));
    qdvec3::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(9.0));

    QCOMPARE(v0, v1);

    QCOMPARE(qdvec3::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(7.0));
    QCOMPARE(qdvec3::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(8.0));
    QCOMPARE(qdvec3::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(9.0));
    QCOMPARE(qdvec3::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(7.0));
    QCOMPARE(qdvec3::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(8.0));
    QCOMPARE(qdvec3::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(9.0));

    v0.setR(1.0/3.0);
    v0.setG(2.0/3.0);
    v0.setB(1.0);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QGeoCoordinate>());
    QVERIFY(variant.canConvert<QVector3D>());
    QVERIFY(variant.canConvert<QColor>());
    QVERIFY(variant.canConvert<QString>());

    QGeoCoordinate geoCoordinate = variant.value<QGeoCoordinate>();
    QCOMPARE(geoCoordinate.longitude(), 1.0/3.0);
    QCOMPARE(geoCoordinate.latitude(), 2.0/3.0);
    QCOMPARE(geoCoordinate.altitude(), 1.0);

    QVector3D vector3d = variant.value<QVector3D>();
    QCOMPARE(vector3d.x(), 1.0f/3.0f);
    QCOMPARE(vector3d.y(), 2.0f/3.0f);
    QCOMPARE(vector3d.z(), 1.0f);

    QColor color = variant.value<QColor>();
    QCOMPARE(color.redF(), 1.0/3.0);
    QCOMPARE(color.greenF(), 2.0/3.0);
    QCOMPARE(color.blueF(), 1.0);

    variant = QVariant::fromValue(geoCoordinate);
    v0 = variant.value<qdvec3>();
    QCOMPARE(v0.x(), 1.0/3.0);
    QCOMPARE(v0.y(), 2.0/3.0);
    QCOMPARE(v0.z(), 1.0);

    variant = QVariant::fromValue(vector3d);
    v0 = variant.value<qdvec3>();
    QCOMPARE(v0.x(), double(float(1.0/3.0)));
    QCOMPARE(v0.y(), double(float(2.0/3.0)));
    QCOMPARE(v0.z(), 1.0);

    variant = QVariant::fromValue(color);
    v0 = variant.value<qdvec3>();
    QCOMPARE(v0.x(), 1.0/3.0);
    QCOMPARE(v0.y(), 2.0/3.0);
    QCOMPARE(v0.z(), 1.0);

    qdvec3 a(1,2,3);
    qdvec3 b(4,5,6);
    QCOMPARE(a.crossProduct(b), qdvec3(-3,6,-3));
    QCOMPARE(a.dotProduct(b), 32.0);
    qdmat4 m(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
    m = m.transpose();
    QCOMPARE(a.times(m), qdvec3(72.0/93.0, 79.0/93.0, 86.0/93.0));

    QCOMPARE(a.times(b), qdvec3(4,10,18));
    QCOMPARE(a.times(4.48), qdvec3(1.0*4.48, 2.0*4.48, 3.0*4.48));
    QCOMPARE(a.plus(b), qdvec3(5,7,9));
    QCOMPARE(a.minus(b), qdvec3(-3,-3,-3));
    QCOMPARE(a.normalized(), qdvec3(1.0/std::sqrt(14.0), 2.0/std::sqrt(14.0), 3.0/std::sqrt(14.0)));
    QCOMPARE(a.length(), std::sqrt(14.0));
}

void Vector3Test::testbvec3()
{
    qbvec3 v0(false, true, false);
    QCOMPARE(v0.x(), false);
    QCOMPARE(v0.y(), true);
    QCOMPARE(v0.z(), false);

    v0.setX(true);
    v0.setY(false);
    v0.setZ(true);

    QCOMPARE(v0.x(), true);
    QCOMPARE(v0.y(), false);
    QCOMPARE(v0.z(), true);

    QCOMPARE(v0.r(), true);
    QCOMPARE(v0.g(), false);
    QCOMPARE(v0.b(), true);

    v0.setR(false);
    v0.setG(true);
    v0.setB(true);

    QCOMPARE(v0.x(), false);
    QCOMPARE(v0.y(), true);
    QCOMPARE(v0.z(), true);

    qbvec3 v1;

    QCOMPARE(v1.x(), false);
    QCOMPARE(v1.y(), false);
    QCOMPARE(v1.z(), false);

    QCOMPARE(qbvec3::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qbvec3::staticMetaObject.property(1).name(), "y");
    QCOMPARE(qbvec3::staticMetaObject.property(2).name(), "z");
    QCOMPARE(qbvec3::staticMetaObject.property(3).name(), "r");
    QCOMPARE(qbvec3::staticMetaObject.property(4).name(), "g");
    QCOMPARE(qbvec3::staticMetaObject.property(5).name(), "b");

    qbvec3::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(false));
    qbvec3::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(true));
    qbvec3::staticMetaObject.property(2).writeOnGadget(&v1, QVariant::fromValue(true));

    QCOMPARE(v0, v1);

    QCOMPARE(qbvec3::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(false));
    QCOMPARE(qbvec3::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(true));
    QCOMPARE(qbvec3::staticMetaObject.property(2).readOnGadget(&v1), QVariant::fromValue(true));
    QCOMPARE(qbvec3::staticMetaObject.property(3).readOnGadget(&v1), QVariant::fromValue(false));
    QCOMPARE(qbvec3::staticMetaObject.property(4).readOnGadget(&v1), QVariant::fromValue(true));
    QCOMPARE(qbvec3::staticMetaObject.property(5).readOnGadget(&v1), QVariant::fromValue(true));

    v0.setR(false);
    v0.setG(true);
    v0.setB(false);

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector3D>());

    QVector3D vector3d = variant.value<QVector3D>();
    QCOMPARE(vector3d.x(), 0.0f);
    QCOMPARE(vector3d.y(), 1.0f);
    QCOMPARE(vector3d.z(), 0.0f);

    variant = QVariant::fromValue(vector3d);
    v0 = variant.value<qbvec3>();
    QCOMPARE(v0.x(), false);
    QCOMPARE(v0.y(), true);
    QCOMPARE(v0.z(), false);

}

void Vector3Test::testSwizzle()
{
    qivec3 v(1,2,3);
    QCOMPARE(v.xx(), vector2<int>(1,1));
    QCOMPARE(v.xy(), vector2<int>(1,2));
    QCOMPARE(v.xz(), vector2<int>(1,3));
    QCOMPARE(v.yx(), vector2<int>(2,1));
    QCOMPARE(v.yy(), vector2<int>(2,2));
    QCOMPARE(v.yz(), vector2<int>(2,3));
    QCOMPARE(v.zx(), vector2<int>(3,1));
    QCOMPARE(v.zy(), vector2<int>(3,2));
    QCOMPARE(v.zz(), vector2<int>(3,3));

    QCOMPARE(v.xxx(), vector3<int>(1,1,1));
    QCOMPARE(v.xxy(), vector3<int>(1,1,2));
    QCOMPARE(v.xxz(), vector3<int>(1,1,3));
    QCOMPARE(v.xyx(), vector3<int>(1,2,1));
    QCOMPARE(v.xyy(), vector3<int>(1,2,2));
    QCOMPARE(v.xyz(), vector3<int>(1,2,3));
    QCOMPARE(v.xzx(), vector3<int>(1,3,1));
    QCOMPARE(v.xzy(), vector3<int>(1,3,2));
    QCOMPARE(v.xzz(), vector3<int>(1,3,3));
    QCOMPARE(v.yxx(), vector3<int>(2,1,1));
    QCOMPARE(v.yxy(), vector3<int>(2,1,2));
    QCOMPARE(v.yxz(), vector3<int>(2,1,3));
    QCOMPARE(v.yyx(), vector3<int>(2,2,1));
    QCOMPARE(v.yyy(), vector3<int>(2,2,2));
    QCOMPARE(v.yyz(), vector3<int>(2,2,3));
    QCOMPARE(v.yzx(), vector3<int>(2,3,1));
    QCOMPARE(v.yzy(), vector3<int>(2,3,2));
    QCOMPARE(v.yzz(), vector3<int>(2,3,3));
    QCOMPARE(v.zxx(), vector3<int>(3,1,1));
    QCOMPARE(v.zxy(), vector3<int>(3,1,2));
    QCOMPARE(v.zxz(), vector3<int>(3,1,3));
    QCOMPARE(v.zyx(), vector3<int>(3,2,1));
    QCOMPARE(v.zyy(), vector3<int>(3,2,2));
    QCOMPARE(v.zyz(), vector3<int>(3,2,3));
    QCOMPARE(v.zzx(), vector3<int>(3,3,1));
    QCOMPARE(v.zzy(), vector3<int>(3,3,2));
    QCOMPARE(v.zzz(), vector3<int>(3,3,3));

}


QTEST_APPLESS_MAIN(Vector3Test)

#include "tst_vector3test.moc"

#include <QString>
#include <QtTest>

#include "quickvector2.h"
#include "quickopengl_plugin.h"

class Vector2Test : public QObject
{
    Q_OBJECT

public:

    Vector2Test();

private Q_SLOTS:

    void testvec2();
    void testivec2();
    void testuvec2();
    void testdvec2();
    void testbvec2();
    void testSwizzle();
};

Vector2Test::Vector2Test()
{
    // register types
    QuickOpenGLPlugin plugin;
    plugin.registerTypes("QuickOpenGL");
}

void Vector2Test::testvec2()
{

    qvec2 v0(1.0f,2.0f);
    QCOMPARE(v0.x(), 1.0f);
    QCOMPARE(v0.y(), 2.0f);

    v0.setX(3.0f);
    v0.setY(4.0f);

    QCOMPARE(v0.x(), 3.0f);
    QCOMPARE(v0.y(), 4.0f);

    qvec2 v1;

    QCOMPARE(v1.x(), 0.0f);
    QCOMPARE(v1.y(), 0.0f);

    QCOMPARE(qvec2::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qvec2::staticMetaObject.property(1).name(), "y");

    qvec2::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(3.0f));
    qvec2::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(4.0f));

    QCOMPARE(v0, v1);

    QCOMPARE(qvec2::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(3.0f));
    QCOMPARE(qvec2::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(4.0f));

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QGeoCoordinate>());
    QVERIFY(variant.canConvert<QVector2D>());
    QVERIFY(variant.canConvert<QPointF>());
    QVERIFY(variant.canConvert<QPoint>());
    QVERIFY(variant.canConvert<QSizeF>());
    QVERIFY(variant.canConvert<QSize>());

    QGeoCoordinate geoCoordinate = variant.value<QGeoCoordinate>();
    QCOMPARE(geoCoordinate.longitude(), 3.0);
    QCOMPARE(geoCoordinate.latitude(), 4.0);
    QCOMPARE(geoCoordinate.altitude(), 0.0);

    QVector2D vector2d = variant.value<QVector2D>();
    QCOMPARE(vector2d.x(), 3.0f);
    QCOMPARE(vector2d.y(), 4.0f);

    QPointF pointF = variant.value<QPointF>();
    QCOMPARE(pointF.x(), 3.0);
    QCOMPARE(pointF.y(), 4.0);

    QPoint point = variant.value<QPoint>();
    QCOMPARE(point.x(), 3);
    QCOMPARE(point.y(), 4);

    QSizeF sizeF = variant.value<QSizeF>();
    QCOMPARE(sizeF.width(), 3.0);
    QCOMPARE(sizeF.height(), 4.0);

    QSize size = variant.value<QSize>();
    QCOMPARE(size.width(), 3);
    QCOMPARE(size.height(), 4);

    variant = QVariant::fromValue(geoCoordinate);
    v0 = variant.value<qvec2>();
    QCOMPARE(v0.x(), 3.0f);
    QCOMPARE(v0.y(), 4.0f);

    variant = QVariant::fromValue(vector2d);
    v0 = variant.value<qvec2>();
    QCOMPARE(v0.x(), 3.0f);
    QCOMPARE(v0.y(), 4.0f);

    variant = QVariant::fromValue(pointF);
    v0 = variant.value<qvec2>();
    QCOMPARE(v0.x(), 3.0f);
    QCOMPARE(v0.y(), 4.0f);

    variant = QVariant::fromValue(point);
    v0 = variant.value<qvec2>();
    QCOMPARE(v0.x(), 3.0f);
    QCOMPARE(v0.y(), 4.0f);

    variant = QVariant::fromValue(sizeF);
    v0 = variant.value<qvec2>();
    QCOMPARE(v0.x(), 3.0f);
    QCOMPARE(v0.y(), 4.0f);

    variant = QVariant::fromValue(size);
    v0 = variant.value<qvec2>();
    QCOMPARE(v0.x(), 3.0f);
    QCOMPARE(v0.y(), 4.0f);


    qvec2 a(1,2);
    qvec2 b(3,4);
    QCOMPARE(a.dotProduct(b), 11.0f);
    QCOMPARE(a.times(b), qvec2(3,8));
    QCOMPARE(a.times(4.48f), qvec2(4.48f, 8.96f));
    QCOMPARE(a.plus(b), qvec2(4,6));
    QCOMPARE(a.minus(b), qvec2(-2,-2));
    QCOMPARE(b.normalized(), qvec2(3.0f/5.0f, 4.0f/5.0f));
    QCOMPARE(b.length(), 5.0f);

}

void Vector2Test::testivec2()
{

    qivec2 v0(1, 2);
    QCOMPARE(v0.x(), 1);
    QCOMPARE(v0.y(), 2);

    v0.setX(3);
    v0.setY(4);

    QCOMPARE(v0.x(), 3);
    QCOMPARE(v0.y(), 4);

    qivec2 v1;

    QCOMPARE(v1.x(), 0);
    QCOMPARE(v1.y(), 0);

    QCOMPARE(qivec2::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qivec2::staticMetaObject.property(1).name(), "y");

    qivec2::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(3));
    qivec2::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(4));

    QCOMPARE(v0, v1);

    QCOMPARE(qivec2::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(3));
    QCOMPARE(qivec2::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(4));

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector2D>());
    QVERIFY(variant.canConvert<QPointF>());
    QVERIFY(variant.canConvert<QPoint>());
    QVERIFY(variant.canConvert<QSizeF>());
    QVERIFY(variant.canConvert<QSize>());

    QVector2D vector2d = variant.value<QVector2D>();
    QCOMPARE(vector2d.x(), 3.0f);
    QCOMPARE(vector2d.y(), 4.0f);

    QPointF pointF = variant.value<QPointF>();
    QCOMPARE(pointF.x(), 3.0);
    QCOMPARE(pointF.y(), 4.0);

    QPoint point = variant.value<QPoint>();
    QCOMPARE(point.x(), 3);
    QCOMPARE(point.y(), 4);

    QSizeF sizeF = variant.value<QSizeF>();
    QCOMPARE(sizeF.width(), 3.0);
    QCOMPARE(sizeF.height(), 4.0);

    QSize size = variant.value<QSize>();
    QCOMPARE(size.width(), 3);
    QCOMPARE(size.height(), 4);

    variant = QVariant::fromValue(vector2d);
    v0 = variant.value<qivec2>();
    QCOMPARE(v0.x(), 3);
    QCOMPARE(v0.y(), 4);

    variant = QVariant::fromValue(pointF);
    v0 = variant.value<qivec2>();
    QCOMPARE(v0.x(), 3);
    QCOMPARE(v0.y(), 4);

    variant = QVariant::fromValue(point);
    v0 = variant.value<qivec2>();
    QCOMPARE(v0.x(), 3);
    QCOMPARE(v0.y(), 4);

    variant = QVariant::fromValue(sizeF);
    v0 = variant.value<qivec2>();
    QCOMPARE(v0.x(), 3);
    QCOMPARE(v0.y(), 4);

    variant = QVariant::fromValue(size);
    v0 = variant.value<qivec2>();
    QCOMPARE(v0.x(), 3);
    QCOMPARE(v0.y(), 4);

}

void Vector2Test::testuvec2()
{

    quvec2 v0(1U, 2U);
    QCOMPARE(v0.x(), 1U);
    QCOMPARE(v0.y(), 2U);

    v0.setX(3U);
    v0.setY(4U);

    QCOMPARE(v0.x(), 3U);
    QCOMPARE(v0.y(), 4U);

    quvec2 v1;

    QCOMPARE(v1.x(), 0U);
    QCOMPARE(v1.y(), 0U);

    QCOMPARE(quvec2::staticMetaObject.property(0).name(), "x");
    QCOMPARE(quvec2::staticMetaObject.property(1).name(), "y");

    quvec2::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(3U));
    quvec2::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(4U));

    QCOMPARE(v0, v1);

    QCOMPARE(quvec2::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(3U));
    QCOMPARE(quvec2::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(4U));

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector2D>());
    QVERIFY(variant.canConvert<QPointF>());
    QVERIFY(variant.canConvert<QPoint>());
    QVERIFY(variant.canConvert<QSizeF>());
    QVERIFY(variant.canConvert<QSize>());

    QVector2D vector2d = variant.value<QVector2D>();
    QCOMPARE(vector2d.x(), 3.0f);
    QCOMPARE(vector2d.y(), 4.0f);

    QPointF pointF = variant.value<QPointF>();
    QCOMPARE(pointF.x(), 3.0);
    QCOMPARE(pointF.y(), 4.0);

    QPoint point = variant.value<QPoint>();
    QCOMPARE(point.x(), 3);
    QCOMPARE(point.y(), 4);

    QSizeF sizeF = variant.value<QSizeF>();
    QCOMPARE(sizeF.width(), 3.0);
    QCOMPARE(sizeF.height(), 4.0);

    QSize size = variant.value<QSize>();
    QCOMPARE(size.width(), 3);
    QCOMPARE(size.height(), 4);

    variant = QVariant::fromValue(vector2d);
    v0 = variant.value<quvec2>();
    QCOMPARE(v0.x(), 3U);
    QCOMPARE(v0.y(), 4U);

    variant = QVariant::fromValue(pointF);
    v0 = variant.value<quvec2>();
    QCOMPARE(v0.x(), 3U);
    QCOMPARE(v0.y(), 4U);

    variant = QVariant::fromValue(point);
    v0 = variant.value<quvec2>();
    QCOMPARE(v0.x(), 3U);
    QCOMPARE(v0.y(), 4U);

    variant = QVariant::fromValue(sizeF);
    v0 = variant.value<quvec2>();
    QCOMPARE(v0.x(), 3U);
    QCOMPARE(v0.y(), 4U);

    variant = QVariant::fromValue(size);
    v0 = variant.value<quvec2>();
    QCOMPARE(v0.x(), 3U);
    QCOMPARE(v0.y(), 4U);

}


void Vector2Test::testdvec2()
{

    qdvec2 v0(1.0, 2.0);
    QCOMPARE(v0.x(), 1.0);
    QCOMPARE(v0.y(), 2.0);

    v0.setX(3.0);
    v0.setY(4.0);

    QCOMPARE(v0.x(), 3.0);
    QCOMPARE(v0.y(), 4.0);

    qdvec2 v1;

    QCOMPARE(v1.x(), 0.0);
    QCOMPARE(v1.y(), 0.0);

    QCOMPARE(qdvec2::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qdvec2::staticMetaObject.property(1).name(), "y");

    qdvec2::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(3.0));
    qdvec2::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(4.0));

    QCOMPARE(v0, v1);

    QCOMPARE(qdvec2::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(3.0));
    QCOMPARE(qdvec2::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(4.0));

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QGeoCoordinate>());
    QVERIFY(variant.canConvert<QVector2D>());
    QVERIFY(variant.canConvert<QPointF>());
    QVERIFY(variant.canConvert<QPoint>());
    QVERIFY(variant.canConvert<QSizeF>());
    QVERIFY(variant.canConvert<QSize>());

    QGeoCoordinate geoCoordinate = variant.value<QGeoCoordinate>();
    QCOMPARE(geoCoordinate.longitude(), 3.0);
    QCOMPARE(geoCoordinate.latitude(), 4.0);
    QCOMPARE(geoCoordinate.altitude(), 0.0);

    QVector2D vector2d = variant.value<QVector2D>();
    QCOMPARE(vector2d.x(), 3.0f);
    QCOMPARE(vector2d.y(), 4.0f);

    QPointF pointF = variant.value<QPointF>();
    QCOMPARE(pointF.x(), 3.0);
    QCOMPARE(pointF.y(), 4.0);

    QPoint point = variant.value<QPoint>();
    QCOMPARE(point.x(), 3);
    QCOMPARE(point.y(), 4);

    QSizeF sizeF = variant.value<QSizeF>();
    QCOMPARE(sizeF.width(), 3.0);
    QCOMPARE(sizeF.height(), 4.0);

    QSize size = variant.value<QSize>();
    QCOMPARE(size.width(), 3);
    QCOMPARE(size.height(), 4);

    variant = QVariant::fromValue(geoCoordinate);
    v0 = variant.value<qdvec2>();
    QCOMPARE(v0.x(), 3.0);
    QCOMPARE(v0.y(), 4.0);

    variant = QVariant::fromValue(vector2d);
    v0 = variant.value<qdvec2>();
    QCOMPARE(v0.x(), 3.0);
    QCOMPARE(v0.y(), 4.0);

    variant = QVariant::fromValue(pointF);
    v0 = variant.value<qdvec2>();
    QCOMPARE(v0.x(), 3.0);
    QCOMPARE(v0.y(), 4.0);

    variant = QVariant::fromValue(point);
    v0 = variant.value<qdvec2>();
    QCOMPARE(v0.x(), 3.0);
    QCOMPARE(v0.y(), 4.0);

    variant = QVariant::fromValue(sizeF);
    v0 = variant.value<qdvec2>();
    QCOMPARE(v0.x(), 3.0);
    QCOMPARE(v0.y(), 4.0);

    variant = QVariant::fromValue(size);
    v0 = variant.value<qdvec2>();
    QCOMPARE(v0.x(), 3.0);
    QCOMPARE(v0.y(), 4.0);

    qdvec2 a(1,2);
    qdvec2 b(3,4);
    QCOMPARE(a.dotProduct(b), 11.0);
    QCOMPARE(a.times(b), qdvec2(3,8));
    QCOMPARE(a.times(4.48), qdvec2(4.48, 8.96));
    QCOMPARE(a.plus(b), qdvec2(4,6));
    QCOMPARE(a.minus(b), qdvec2(-2,-2));
    QCOMPARE(b.normalized().x(), 0.6);
    QCOMPARE(b.normalized().y(), 0.8);
    QCOMPARE(b.length(), 5.0);
}

void Vector2Test::testbvec2()
{

    qbvec2 v0(true, false);
    QCOMPARE(v0.x(), true);
    QCOMPARE(v0.y(), false);

    v0.setX(false);
    v0.setY(true);

    QCOMPARE(v0.x(), false);
    QCOMPARE(v0.y(), true);

    qbvec2 v1;

    QCOMPARE(v1.x(), false);
    QCOMPARE(v1.y(), false);

    QCOMPARE(qbvec2::staticMetaObject.property(0).name(), "x");
    QCOMPARE(qbvec2::staticMetaObject.property(1).name(), "y");

    qbvec2::staticMetaObject.property(0).writeOnGadget(&v1, QVariant::fromValue(false));
    qbvec2::staticMetaObject.property(1).writeOnGadget(&v1, QVariant::fromValue(true));

    QCOMPARE(v0, v1);

    QCOMPARE(qbvec2::staticMetaObject.property(0).readOnGadget(&v1), QVariant::fromValue(false));
    QCOMPARE(qbvec2::staticMetaObject.property(1).readOnGadget(&v1), QVariant::fromValue(true));

    QVariant variant = QVariant::fromValue(v0);

    QVERIFY(variant.canConvert<QVector2D>());
    QVERIFY(variant.canConvert<QPoint>());

    QVector2D vector2d = variant.value<QVector2D>();
    QCOMPARE(vector2d.x(), 0.0f);
    QCOMPARE(vector2d.y(), 1.0f);

    QPoint point = variant.value<QPoint>();
    QCOMPARE(point.x(), 0);
    QCOMPARE(point.y(), 1);

    variant = QVariant::fromValue(vector2d);
    v0 = variant.value<qbvec2>();
    QCOMPARE(v0.x(), false);
    QCOMPARE(v0.y(), true);

    variant = QVariant::fromValue(point);
    v0 = variant.value<qbvec2>();
    QCOMPARE(v0.x(), false);
    QCOMPARE(v0.y(), true);

}

void Vector2Test::testSwizzle()
{
    qvec2 a(1,2);

    // swizzle is performed by base class, so only testing one implementation
    QCOMPARE(a.xx(), vector2<float>(1,1));
    QCOMPARE(a.xy(), vector2<float>(1,2));
    QCOMPARE(a.yx(), vector2<float>(2,1));
    QCOMPARE(a.yy(), vector2<float>(2,2));
}

QTEST_APPLESS_MAIN(Vector2Test)

#include "tst_vector2test.moc"

#include <QString>
#include <QtTest>

#include "quickmatrix4.h"
#include "quickopengl_plugin.h"

class Matrix4Test : public QObject
{
    Q_OBJECT

public:
    Matrix4Test();

private Q_SLOTS:

    void testmat4();
    void testdmat4();

    void testmat4x2();
    void testmat4x3();

    void testdmat4x2();
    void testdmat4x3();


};

Matrix4Test::Matrix4Test()
{
    // register types
    QuickOpenGLPlugin plugin;
    plugin.registerTypes("QuickOpenGL");

}

void Matrix4Test::testmat4()
{

    //
    // Default constructor initializes with identity matrix
    //
    qmat4 m0;
    QCOMPARE(m0(0,0), 1.0f);
    QCOMPARE(m0(0,1), 0.0f);
    QCOMPARE(m0(0,2), 0.0f);
    QCOMPARE(m0(0,3), 0.0f);
    QCOMPARE(m0(1,0), 0.0f);
    QCOMPARE(m0(1,1), 1.0f);
    QCOMPARE(m0(1,2), 0.0f);
    QCOMPARE(m0(1,3), 0.0f);
    QCOMPARE(m0(2,0), 0.0f);
    QCOMPARE(m0(2,1), 0.0f);
    QCOMPARE(m0(2,2), 1.0f);
    QCOMPARE(m0(2,3), 0.0f);
    QCOMPARE(m0(3,0), 0.0f);
    QCOMPARE(m0(3,1), 0.0f);
    QCOMPARE(m0(3,2), 0.0f);
    QCOMPARE(m0(3,3), 1.0f);

    //
    // Diagonal intializer - as per GLSL
    //
    m0 = qmat4(2.0f);
    QCOMPARE(m0(0,0), 2.0f);
    QCOMPARE(m0(0,1), 0.0f);
    QCOMPARE(m0(0,2), 0.0f);
    QCOMPARE(m0(0,3), 0.0f);
    QCOMPARE(m0(1,0), 0.0f);
    QCOMPARE(m0(1,1), 2.0f);
    QCOMPARE(m0(1,2), 0.0f);
    QCOMPARE(m0(1,3), 0.0f);
    QCOMPARE(m0(2,0), 0.0f);
    QCOMPARE(m0(2,1), 0.0f);
    QCOMPARE(m0(2,2), 2.0f);
    QCOMPARE(m0(2,3), 0.0f);
    QCOMPARE(m0(3,0), 0.0f);
    QCOMPARE(m0(3,1), 0.0f);
    QCOMPARE(m0(3,2), 0.0f);
    QCOMPARE(m0(3,3), 2.0f);

    //
    // Column vector constructor - as per GLSL
    //
    m0 = qmat4(qvec4(0.0f,1.0f,2.0f,3.0f), qvec4(4.0f,5.0f,6.0f,7.0f), qvec4(8.0f,9.0f,10.0f,11.0f), qvec4(12.0f,13.0f,14.0f,15.0f));
    QCOMPARE(m0(0,0), 0.0f);
    QCOMPARE(m0(1,0), 1.0f);
    QCOMPARE(m0(2,0), 2.0f);
    QCOMPARE(m0(3,0), 3.0f);
    QCOMPARE(m0(0,1), 4.0f);
    QCOMPARE(m0(1,1), 5.0f);
    QCOMPARE(m0(2,1), 6.0f);
    QCOMPARE(m0(3,1), 7.0f);
    QCOMPARE(m0(0,2), 8.0f);
    QCOMPARE(m0(1,2), 9.0f);
    QCOMPARE(m0(2,2), 10.0f);
    QCOMPARE(m0(3,2), 11.0f);
    QCOMPARE(m0(0,3), 12.0f);
    QCOMPARE(m0(1,3), 13.0f);
    QCOMPARE(m0(2,3), 14.0f);
    QCOMPARE(m0(3,3), 15.0f);


    //
    // Column wise value constructor - as per GLSL
    //
    m0 = qmat4(0.0f,  1.0f,  2.0f,  3.0f,
            4.0f,  5.0f,  6.0f,  7.0f,
            8.0f,  9.0f,  10.0f, 11.0f,
            12.0f, 13.0f, 14.0f, 15.0f);
    QCOMPARE(m0(0,0), 0.0f);
    QCOMPARE(m0(1,0), 1.0f);
    QCOMPARE(m0(2,0), 2.0f);
    QCOMPARE(m0(3,0), 3.0f);
    QCOMPARE(m0(0,1), 4.0f);
    QCOMPARE(m0(1,1), 5.0f);
    QCOMPARE(m0(2,1), 6.0f);
    QCOMPARE(m0(3,1), 7.0f);
    QCOMPARE(m0(0,2), 8.0f);
    QCOMPARE(m0(1,2), 9.0f);
    QCOMPARE(m0(2,2), 10.0f);
    QCOMPARE(m0(3,2), 11.0f);
    QCOMPARE(m0(0,3), 12.0f);
    QCOMPARE(m0(1,3), 13.0f);
    QCOMPARE(m0(2,3), 14.0f);
    QCOMPARE(m0(3,3), 15.0f);

    QCOMPARE(m0.m00(), 0.0f);
    QCOMPARE(m0.m10(), 1.0f);
    QCOMPARE(m0.m20(), 2.0f);
    QCOMPARE(m0.m30(), 3.0f);
    QCOMPARE(m0.m01(), 4.0f);
    QCOMPARE(m0.m11(), 5.0f);
    QCOMPARE(m0.m21(), 6.0f);
    QCOMPARE(m0.m31(), 7.0f);
    QCOMPARE(m0.m02(), 8.0f);
    QCOMPARE(m0.m12(), 9.0f);
    QCOMPARE(m0.m22(), 10.0f);
    QCOMPARE(m0.m32(), 11.0f);
    QCOMPARE(m0.m03(), 12.0f);
    QCOMPARE(m0.m13(), 13.0f);
    QCOMPARE(m0.m23(), 14.0f);
    QCOMPARE(m0.m33(), 15.0f);

    QCOMPARE(m0.c0(), qvec4(0.0f,1.0f,2.0f,3.0f));
    QCOMPARE(m0.c1(), qvec4(4.0f,5.0f,6.0f,7.0f));
    QCOMPARE(m0.c2(), qvec4(8.0f,9.0f,10.0f,11.0f));
    QCOMPARE(m0.c3(), qvec4(12.0f,13.0f,14.0f,15.0f));

    QCOMPARE(m0.r0(), qvec4(0.0f,4.0f,8.0f,12.0f));
    QCOMPARE(m0.r1(), qvec4(1.0f,5.0f,9.0f,13.0f));
    QCOMPARE(m0.r2(), qvec4(2.0f,6.0f,10.0f,14.0f));
    QCOMPARE(m0.r3(), qvec4(3.0f,7.0f,11.0f,15.0f));

    m0.setR0(qvec4(0.0f,1.0f,2.0f,3.0f));
    m0.setR1(qvec4(4.0f,5.0f,6.0f,7.0f));
    m0.setR2(qvec4(8.0f,9.0f,10.0f,11.0f));
    m0.setR3(qvec4(12.0f,13.0f,14.0f,15.0f));

    QCOMPARE(m0.m00(), 0.0f);
    QCOMPARE(m0.m01(), 1.0f);
    QCOMPARE(m0.m02(), 2.0f);
    QCOMPARE(m0.m03(), 3.0f);
    QCOMPARE(m0.m10(), 4.0f);
    QCOMPARE(m0.m11(), 5.0f);
    QCOMPARE(m0.m12(), 6.0f);
    QCOMPARE(m0.m13(), 7.0f);
    QCOMPARE(m0.m20(), 8.0f);
    QCOMPARE(m0.m21(), 9.0f);
    QCOMPARE(m0.m22(), 10.0f);
    QCOMPARE(m0.m23(), 11.0f);
    QCOMPARE(m0.m30(), 12.0f);
    QCOMPARE(m0.m31(), 13.0f);
    QCOMPARE(m0.m32(), 14.0f);
    QCOMPARE(m0.m33(), 15.0f);

    m0.setC0(qvec4(0.0f,1.0f,2.0f,3.0f));
    m0.setC1(qvec4(4.0f,5.0f,6.0f,7.0f));
    m0.setC2(qvec4(8.0f,9.0f,10.0f,11.0f));
    m0.setC3(qvec4(12.0f,13.0f,14.0f,15.0f));

    QCOMPARE(m0.m00(), 0.0f);
    QCOMPARE(m0.m10(), 1.0f);
    QCOMPARE(m0.m20(), 2.0f);
    QCOMPARE(m0.m30(), 3.0f);
    QCOMPARE(m0.m01(), 4.0f);
    QCOMPARE(m0.m11(), 5.0f);
    QCOMPARE(m0.m21(), 6.0f);
    QCOMPARE(m0.m31(), 7.0f);
    QCOMPARE(m0.m02(), 8.0f);
    QCOMPARE(m0.m12(), 9.0f);
    QCOMPARE(m0.m22(), 10.0f);
    QCOMPARE(m0.m32(), 11.0f);
    QCOMPARE(m0.m03(), 12.0f);
    QCOMPARE(m0.m13(), 13.0f);
    QCOMPARE(m0.m23(), 14.0f);
    QCOMPARE(m0.m33(), 15.0f);

    qmat4 m1;
    m1.setM00(0.0f);
    m1.setM10(1.0f);
    m1.setM20(2.0f);
    m1.setM30(3.0f);
    m1.setM01(4.0f);
    m1.setM11(5.0f);
    m1.setM21(6.0f);
    m1.setM31(7.0f);
    m1.setM02(8.0f);
    m1.setM12(9.0f);
    m1.setM22(10.0f);
    m1.setM32(11.0f);
    m1.setM03(12.0f);
    m1.setM13(13.0f);
    m1.setM23(14.0f);
    m1.setM33(15.0f);

    //
    // Comparison
    //
    QCOMPARE(m0,m1);


    //
    // QVariant
    //
    QVariant variant = m0;
    QVERIFY(variant.canConvert<QMatrix4x4>());

    //
    // QMatrix4x4
    //
    QMatrix4x4 matrix = variant.value<QMatrix4x4>();
    QCOMPARE(matrix(0,0), 0.0f);
    QCOMPARE(matrix(1,0), 1.0f);
    QCOMPARE(matrix(2,0), 2.0f);
    QCOMPARE(matrix(3,0), 3.0f);
    QCOMPARE(matrix(0,1), 4.0f);
    QCOMPARE(matrix(1,1), 5.0f);
    QCOMPARE(matrix(2,1), 6.0f);
    QCOMPARE(matrix(3,1), 7.0f);
    QCOMPARE(matrix(0,2), 8.0f);
    QCOMPARE(matrix(1,2), 9.0f);
    QCOMPARE(matrix(2,2), 10.0f);
    QCOMPARE(matrix(3,2), 11.0f);
    QCOMPARE(matrix(0,3), 12.0f);
    QCOMPARE(matrix(1,3), 13.0f);
    QCOMPARE(matrix(2,3), 14.0f);
    QCOMPARE(matrix(3,3), 15.0f);

    variant = matrix;

    QVERIFY(variant.canConvert<qmat4>());

    m0 = variant.value<qmat4>();

    QCOMPARE(m0.m00(), 0.0f);
    QCOMPARE(m0.m10(), 1.0f);
    QCOMPARE(m0.m20(), 2.0f);
    QCOMPARE(m0.m30(), 3.0f);
    QCOMPARE(m0.m01(), 4.0f);
    QCOMPARE(m0.m11(), 5.0f);
    QCOMPARE(m0.m21(), 6.0f);
    QCOMPARE(m0.m31(), 7.0f);
    QCOMPARE(m0.m02(), 8.0f);
    QCOMPARE(m0.m12(), 9.0f);
    QCOMPARE(m0.m22(), 10.0f);
    QCOMPARE(m0.m32(), 11.0f);
    QCOMPARE(m0.m03(), 12.0f);
    QCOMPARE(m0.m13(), 13.0f);
    QCOMPARE(m0.m23(), 14.0f);
    QCOMPARE(m0.m33(), 15.0f);


    qmat4 scale = qmat4().scale(qvec3(10.0f, 20.0f, 30.0f));
    QCOMPARE(scale(0,0), 10.0f);
    QCOMPARE(scale(0,1), 0.0f);
    QCOMPARE(scale(0,2), 0.0f);
    QCOMPARE(scale(0,3), 0.0f);
    QCOMPARE(scale(1,0), 0.0f);
    QCOMPARE(scale(1,1), 20.0f);
    QCOMPARE(scale(1,2), 0.0f);
    QCOMPARE(scale(1,3), 0.0f);
    QCOMPARE(scale(2,0), 0.0f);
    QCOMPARE(scale(2,1), 0.0f);
    QCOMPARE(scale(2,2), 30.0f);
    QCOMPARE(scale(2,3), 0.0f);
    QCOMPARE(scale(3,0), 0.0f);
    QCOMPARE(scale(3,1), 0.0f);
    QCOMPARE(scale(3,2), 0.0f);
    QCOMPARE(scale(3,3), 1.0f);


    qmat4 translate = qmat4().translate(qvec3(10.0f, 20.0f, 30.0f));
    QCOMPARE(translate(0,0), 1.0f);
    QCOMPARE(translate(0,1), 0.0f);
    QCOMPARE(translate(0,2), 0.0f);
    QCOMPARE(translate(0,3), 10.0f);
    QCOMPARE(translate(1,0), 0.0f);
    QCOMPARE(translate(1,1), 1.0f);
    QCOMPARE(translate(1,2), 0.0f);
    QCOMPARE(translate(1,3), 20.0f);
    QCOMPARE(translate(2,0), 0.0f);
    QCOMPARE(translate(2,1), 0.0f);
    QCOMPARE(translate(2,2), 1.0f);
    QCOMPARE(translate(2,3), 30.0f);
    QCOMPARE(translate(3,0), 0.0f);
    QCOMPARE(translate(3,1), 0.0f);
    QCOMPARE(translate(3,2), 0.0f);
    QCOMPARE(translate(3,3), 1.0f);

}

void Matrix4Test::testdmat4()
{

    //
    // Default constructor initializes with identity matrix
    //
    qdmat4 m0;
    QCOMPARE(m0(0,0), 1.0);
    QCOMPARE(m0(0,1), 0.0);
    QCOMPARE(m0(0,2), 0.0);
    QCOMPARE(m0(0,3), 0.0);
    QCOMPARE(m0(1,0), 0.0);
    QCOMPARE(m0(1,1), 1.0);
    QCOMPARE(m0(1,2), 0.0);
    QCOMPARE(m0(1,3), 0.0);
    QCOMPARE(m0(2,0), 0.0);
    QCOMPARE(m0(2,1), 0.0);
    QCOMPARE(m0(2,2), 1.0);
    QCOMPARE(m0(2,3), 0.0);
    QCOMPARE(m0(3,0), 0.0);
    QCOMPARE(m0(3,1), 0.0);
    QCOMPARE(m0(3,2), 0.0);
    QCOMPARE(m0(3,3), 1.0);

    //
    // Diagonal intializer - as per GLSL
    //
    m0 = qdmat4(2.0);
    QCOMPARE(m0(0,0), 2.0);
    QCOMPARE(m0(0,1), 0.0);
    QCOMPARE(m0(0,2), 0.0);
    QCOMPARE(m0(0,3), 0.0);
    QCOMPARE(m0(1,0), 0.0);
    QCOMPARE(m0(1,1), 2.0);
    QCOMPARE(m0(1,2), 0.0);
    QCOMPARE(m0(1,3), 0.0);
    QCOMPARE(m0(2,0), 0.0);
    QCOMPARE(m0(2,1), 0.0);
    QCOMPARE(m0(2,2), 2.0);
    QCOMPARE(m0(2,3), 0.0);
    QCOMPARE(m0(3,0), 0.0);
    QCOMPARE(m0(3,1), 0.0);
    QCOMPARE(m0(3,2), 0.0);
    QCOMPARE(m0(3,3), 2.0);

    //
    // Column vector constructor - as per GLSL
    //
    m0 = qdmat4(qdvec4(0.0,1.0,2.0,3.0), qdvec4(4.0,5.0,6.0,7.0), qdvec4(8.0,9.0,10.0,11.0), qdvec4(12.0,13.0,14.0,15.0));
    QCOMPARE(m0(0,0), 0.0);
    QCOMPARE(m0(1,0), 1.0);
    QCOMPARE(m0(2,0), 2.0);
    QCOMPARE(m0(3,0), 3.0);
    QCOMPARE(m0(0,1), 4.0);
    QCOMPARE(m0(1,1), 5.0);
    QCOMPARE(m0(2,1), 6.0);
    QCOMPARE(m0(3,1), 7.0);
    QCOMPARE(m0(0,2), 8.0);
    QCOMPARE(m0(1,2), 9.0);
    QCOMPARE(m0(2,2), 10.0);
    QCOMPARE(m0(3,2), 11.0);
    QCOMPARE(m0(0,3), 12.0);
    QCOMPARE(m0(1,3), 13.0);
    QCOMPARE(m0(2,3), 14.0);
    QCOMPARE(m0(3,3), 15.0);


    //
    // Column wise value constructor - as per GLSL
    //
    m0 = qdmat4(0.0,  1.0,  2.0,  3.0,
            4.0,  5.0,  6.0,  7.0,
            8.0,  9.0,  10.0, 11.0,
            12.0, 13.0, 14.0, 15.0);
    QCOMPARE(m0(0,0), 0.0);
    QCOMPARE(m0(1,0), 1.0);
    QCOMPARE(m0(2,0), 2.0);
    QCOMPARE(m0(3,0), 3.0);
    QCOMPARE(m0(0,1), 4.0);
    QCOMPARE(m0(1,1), 5.0);
    QCOMPARE(m0(2,1), 6.0);
    QCOMPARE(m0(3,1), 7.0);
    QCOMPARE(m0(0,2), 8.0);
    QCOMPARE(m0(1,2), 9.0);
    QCOMPARE(m0(2,2), 10.0);
    QCOMPARE(m0(3,2), 11.0);
    QCOMPARE(m0(0,3), 12.0);
    QCOMPARE(m0(1,3), 13.0);
    QCOMPARE(m0(2,3), 14.0);
    QCOMPARE(m0(3,3), 15.0);

    QCOMPARE(m0.m00(), 0.0);
    QCOMPARE(m0.m10(), 1.0);
    QCOMPARE(m0.m20(), 2.0);
    QCOMPARE(m0.m30(), 3.0);
    QCOMPARE(m0.m01(), 4.0);
    QCOMPARE(m0.m11(), 5.0);
    QCOMPARE(m0.m21(), 6.0);
    QCOMPARE(m0.m31(), 7.0);
    QCOMPARE(m0.m02(), 8.0);
    QCOMPARE(m0.m12(), 9.0);
    QCOMPARE(m0.m22(), 10.0);
    QCOMPARE(m0.m32(), 11.0);
    QCOMPARE(m0.m03(), 12.0);
    QCOMPARE(m0.m13(), 13.0);
    QCOMPARE(m0.m23(), 14.0);
    QCOMPARE(m0.m33(), 15.0);

    QCOMPARE(m0.c0(), qdvec4(0.0,1.0,2.0,3.0));
    QCOMPARE(m0.c1(), qdvec4(4.0,5.0,6.0,7.0));
    QCOMPARE(m0.c2(), qdvec4(8.0,9.0,10.0,11.0));
    QCOMPARE(m0.c3(), qdvec4(12.0,13.0,14.0,15.0));

    QCOMPARE(m0.r0(), qdvec4(0.0,4.0,8.0,12.0));
    QCOMPARE(m0.r1(), qdvec4(1.0,5.0,9.0,13.0));
    QCOMPARE(m0.r2(), qdvec4(2.0,6.0,10.0,14.0));
    QCOMPARE(m0.r3(), qdvec4(3.0,7.0,11.0,15.0));

    m0.setR0(qdvec4(0.0,1.0,2.0,3.0));
    m0.setR1(qdvec4(4.0,5.0,6.0,7.0));
    m0.setR2(qdvec4(8.0,9.0,10.0,11.0));
    m0.setR3(qdvec4(12.0,13.0,14.0,15.0));

    QCOMPARE(m0.m00(), 0.0);
    QCOMPARE(m0.m01(), 1.0);
    QCOMPARE(m0.m02(), 2.0);
    QCOMPARE(m0.m03(), 3.0);
    QCOMPARE(m0.m10(), 4.0);
    QCOMPARE(m0.m11(), 5.0);
    QCOMPARE(m0.m12(), 6.0);
    QCOMPARE(m0.m13(), 7.0);
    QCOMPARE(m0.m20(), 8.0);
    QCOMPARE(m0.m21(), 9.0);
    QCOMPARE(m0.m22(), 10.0);
    QCOMPARE(m0.m23(), 11.0);
    QCOMPARE(m0.m30(), 12.0);
    QCOMPARE(m0.m31(), 13.0);
    QCOMPARE(m0.m32(), 14.0);
    QCOMPARE(m0.m33(), 15.0);

    m0.setC0(qdvec4(0.0,1.0,2.0,3.0));
    m0.setC1(qdvec4(4.0,5.0,6.0,7.0));
    m0.setC2(qdvec4(8.0,9.0,10.0,11.0));
    m0.setC3(qdvec4(12.0,13.0,14.0,15.0));

    QCOMPARE(m0.m00(), 0.0);
    QCOMPARE(m0.m10(), 1.0);
    QCOMPARE(m0.m20(), 2.0);
    QCOMPARE(m0.m30(), 3.0);
    QCOMPARE(m0.m01(), 4.0);
    QCOMPARE(m0.m11(), 5.0);
    QCOMPARE(m0.m21(), 6.0);
    QCOMPARE(m0.m31(), 7.0);
    QCOMPARE(m0.m02(), 8.0);
    QCOMPARE(m0.m12(), 9.0);
    QCOMPARE(m0.m22(), 10.0);
    QCOMPARE(m0.m32(), 11.0);
    QCOMPARE(m0.m03(), 12.0);
    QCOMPARE(m0.m13(), 13.0);
    QCOMPARE(m0.m23(), 14.0);
    QCOMPARE(m0.m33(), 15.0);

    qdmat4 m1;
    m1.setM00(0.0);
    m1.setM10(1.0);
    m1.setM20(2.0);
    m1.setM30(3.0);
    m1.setM01(4.0);
    m1.setM11(5.0);
    m1.setM21(6.0);
    m1.setM31(7.0);
    m1.setM02(8.0);
    m1.setM12(9.0);
    m1.setM22(10.0);
    m1.setM32(11.0);
    m1.setM03(12.0);
    m1.setM13(13.0);
    m1.setM23(14.0);
    m1.setM33(15.0);

    //
    // Comparison
    //
    QCOMPARE(m0,m1);


    //
    // QVariant
    //
    QVariant variant = m0;
    QVERIFY(variant.canConvert<QMatrix4x4>());

    //
    // QMatrix4x4
    //
    QMatrix4x4 matrix = variant.value<QMatrix4x4>();
    QCOMPARE(matrix(0,0), 0.0);
    QCOMPARE(matrix(1,0), 1.0);
    QCOMPARE(matrix(2,0), 2.0);
    QCOMPARE(matrix(3,0), 3.0);
    QCOMPARE(matrix(0,1), 4.0);
    QCOMPARE(matrix(1,1), 5.0);
    QCOMPARE(matrix(2,1), 6.0);
    QCOMPARE(matrix(3,1), 7.0);
    QCOMPARE(matrix(0,2), 8.0);
    QCOMPARE(matrix(1,2), 9.0);
    QCOMPARE(matrix(2,2), 10.0);
    QCOMPARE(matrix(3,2), 11.0);
    QCOMPARE(matrix(0,3), 12.0);
    QCOMPARE(matrix(1,3), 13.0);
    QCOMPARE(matrix(2,3), 14.0);
    QCOMPARE(matrix(3,3), 15.0);

    variant = matrix;

    QVERIFY(variant.canConvert<qdmat4>());

    m0 = variant.value<qdmat4>();

    QCOMPARE(m0.m00(), 0.0);
    QCOMPARE(m0.m10(), 1.0);
    QCOMPARE(m0.m20(), 2.0);
    QCOMPARE(m0.m30(), 3.0);
    QCOMPARE(m0.m01(), 4.0);
    QCOMPARE(m0.m11(), 5.0);
    QCOMPARE(m0.m21(), 6.0);
    QCOMPARE(m0.m31(), 7.0);
    QCOMPARE(m0.m02(), 8.0);
    QCOMPARE(m0.m12(), 9.0);
    QCOMPARE(m0.m22(), 10.0);
    QCOMPARE(m0.m32(), 11.0);
    QCOMPARE(m0.m03(), 12.0);
    QCOMPARE(m0.m13(), 13.0);
    QCOMPARE(m0.m23(), 14.0);
    QCOMPARE(m0.m33(), 15.0);


    qdmat4 scale = qdmat4().scale(qdvec3(10.0, 20.0, 30.0));
    QCOMPARE(scale(0,0), 10.0);
    QCOMPARE(scale(0,1), 0.0);
    QCOMPARE(scale(0,2), 0.0);
    QCOMPARE(scale(0,3), 0.0);
    QCOMPARE(scale(1,0), 0.0);
    QCOMPARE(scale(1,1), 20.0);
    QCOMPARE(scale(1,2), 0.0);
    QCOMPARE(scale(1,3), 0.0);
    QCOMPARE(scale(2,0), 0.0);
    QCOMPARE(scale(2,1), 0.0);
    QCOMPARE(scale(2,2), 30.0);
    QCOMPARE(scale(2,3), 0.0);
    QCOMPARE(scale(3,0), 0.0);
    QCOMPARE(scale(3,1), 0.0);
    QCOMPARE(scale(3,2), 0.0);
    QCOMPARE(scale(3,3), 1.0);


    qdmat4 translate = qdmat4().translate(qdvec3(10.0, 20.0, 30.0));
    QCOMPARE(translate(0,0), 1.0);
    QCOMPARE(translate(0,1), 0.0);
    QCOMPARE(translate(0,2), 0.0);
    QCOMPARE(translate(0,3), 10.0);
    QCOMPARE(translate(1,0), 0.0);
    QCOMPARE(translate(1,1), 1.0);
    QCOMPARE(translate(1,2), 0.0);
    QCOMPARE(translate(1,3), 20.0);
    QCOMPARE(translate(2,0), 0.0);
    QCOMPARE(translate(2,1), 0.0);
    QCOMPARE(translate(2,2), 1.0);
    QCOMPARE(translate(2,3), 30.0);
    QCOMPARE(translate(3,0), 0.0);
    QCOMPARE(translate(3,1), 0.0);
    QCOMPARE(translate(3,2), 0.0);
    QCOMPARE(translate(3,3), 1.0);

}

void Matrix4Test::testmat4x2()
{
    QCOMPARE(sizeof(qmat4x2), 32U);

    qmat4x2 m;
    QCOMPARE(m(0,0), 1.0f);
    QCOMPARE(m(0,1), 0.0f);
    QCOMPARE(m(0,2), 0.0f);
    QCOMPARE(m(0,3), 0.0f);
    QCOMPARE(m(1,0), 0.0f);
    QCOMPARE(m(1,1), 1.0f);
    QCOMPARE(m(1,2), 0.0f);
    QCOMPARE(m(1,3), 0.0f);

    m = qmat4x2(0.0f,1.0f,2.0f,3.0f,4.0f,5.0f,6.0f,7.0f);
    QCOMPARE(m(0,0), 0.0f);
    QCOMPARE(m(0,1), 2.0f);
    QCOMPARE(m(0,2), 4.0f);
    QCOMPARE(m(0,3), 6.0f);
    QCOMPARE(m(1,0), 1.0f);
    QCOMPARE(m(1,1), 3.0f);
    QCOMPARE(m(1,2), 5.0f);
    QCOMPARE(m(1,3), 7.0f);

    QCOMPARE(m.c0(), qvec2(0,1));
    QCOMPARE(m.c1(), qvec2(2,3));
    QCOMPARE(m.c2(), qvec2(4,5));
    QCOMPARE(m.c3(), qvec2(6,7));

    m = qmat4x2(qvec2(0,4), qvec2(1,5), qvec2(2,6), qvec2(3,7));

    QCOMPARE(m(0,0), 0.0f);
    QCOMPARE(m(0,1), 1.0f);
    QCOMPARE(m(0,2), 2.0f);
    QCOMPARE(m(0,3), 3.0f);
    QCOMPARE(m(1,0), 4.0f);
    QCOMPARE(m(1,1), 5.0f);
    QCOMPARE(m(1,2), 6.0f);
    QCOMPARE(m(1,3), 7.0f);

    QCOMPARE(m.r0(), qvec4(0,1,2,3));
    QCOMPARE(m.r1(), qvec4(4,5,6,7));

    QCOMPARE(m.m00(), 0.0f);
    QCOMPARE(m.m01(), 1.0f);
    QCOMPARE(m.m02(), 2.0f);
    QCOMPARE(m.m03(), 3.0f);
    QCOMPARE(m.m10(), 4.0f);
    QCOMPARE(m.m11(), 5.0f);
    QCOMPARE(m.m12(), 6.0f);
    QCOMPARE(m.m13(), 7.0f);

    m.setR0(qvec4(0,2,4,6));
    m.setR1(qvec4(1,3,5,7));

    QCOMPARE(m(0,0), 0.0f);
    QCOMPARE(m(0,1), 2.0f);
    QCOMPARE(m(0,2), 4.0f);
    QCOMPARE(m(0,3), 6.0f);
    QCOMPARE(m(1,0), 1.0f);
    QCOMPARE(m(1,1), 3.0f);
    QCOMPARE(m(1,2), 5.0f);
    QCOMPARE(m(1,3), 7.0f);

    m.setC0(qvec2(0,4));
    m.setC1(qvec2(1,5));
    m.setC2(qvec2(2,6));
    m.setC3(qvec2(3,7));

    QCOMPARE(m.m00(), 0.0f);
    QCOMPARE(m.m01(), 1.0f);
    QCOMPARE(m.m02(), 2.0f);
    QCOMPARE(m.m03(), 3.0f);
    QCOMPARE(m.m10(), 4.0f);
    QCOMPARE(m.m11(), 5.0f);
    QCOMPARE(m.m12(), 6.0f);
    QCOMPARE(m.m13(), 7.0f);


    m.setM00(10);
    m.setM01(20);
    m.setM02(30);
    m.setM03(40);
    m.setM10(50);
    m.setM11(60);
    m.setM12(70);
    m.setM13(80);

    QCOMPARE(m(0,0), 10.0f);
    QCOMPARE(m(0,1), 20.0f);
    QCOMPARE(m(0,2), 30.0f);
    QCOMPARE(m(0,3), 40.0f);
    QCOMPARE(m(1,0), 50.0f);
    QCOMPARE(m(1,1), 60.0f);
    QCOMPARE(m(1,2), 70.0f);
    QCOMPARE(m(1,3), 80.0f);

}

void Matrix4Test::testmat4x3()
{

    QCOMPARE(sizeof(qmat4x3), 48U);

    qmat4x3 m;
    QCOMPARE(m(0,0), 1.0f);
    QCOMPARE(m(0,1), 0.0f);
    QCOMPARE(m(0,2), 0.0f);
    QCOMPARE(m(0,3), 0.0f);
    QCOMPARE(m(1,0), 0.0f);
    QCOMPARE(m(1,1), 1.0f);
    QCOMPARE(m(1,2), 0.0f);
    QCOMPARE(m(1,3), 0.0f);
    QCOMPARE(m(2,0), 0.0f);
    QCOMPARE(m(2,1), 0.0f);
    QCOMPARE(m(2,2), 1.0f);
    QCOMPARE(m(2,3), 0.0f);

    m = qmat4x3(0,1,2,3,4,5,6,7,8,9,10,11);
    QCOMPARE(m(0,0), 0.0f);
    QCOMPARE(m(0,1), 3.0f);
    QCOMPARE(m(0,2), 6.0f);
    QCOMPARE(m(0,3), 9.0f);
    QCOMPARE(m(1,0), 1.0f);
    QCOMPARE(m(1,1), 4.0f);
    QCOMPARE(m(1,2), 7.0f);
    QCOMPARE(m(1,3), 10.0f);
    QCOMPARE(m(2,0), 2.0f);
    QCOMPARE(m(2,1), 5.0f);
    QCOMPARE(m(2,2), 8.0f);
    QCOMPARE(m(2,3), 11.0f);

    QCOMPARE(m.c0(), qvec3(0,1,2));
    QCOMPARE(m.c1(), qvec3(3,4,5));
    QCOMPARE(m.c2(), qvec3(6,7,8));
    QCOMPARE(m.c3(), qvec3(9,10,11));

    m = qmat4x3(qvec3(0,4,8), qvec3(1,5,9), qvec3(2,6,10), qvec3(3,7,11));

    QCOMPARE(m(0,0), 0.0f);
    QCOMPARE(m(0,1), 1.0f);
    QCOMPARE(m(0,2), 2.0f);
    QCOMPARE(m(0,3), 3.0f);
    QCOMPARE(m(1,0), 4.0f);
    QCOMPARE(m(1,1), 5.0f);
    QCOMPARE(m(1,2), 6.0f);
    QCOMPARE(m(1,3), 7.0f);
    QCOMPARE(m(2,0), 8.0f);
    QCOMPARE(m(2,1), 9.0f);
    QCOMPARE(m(2,2), 10.0f);
    QCOMPARE(m(2,3), 11.0f);

    QCOMPARE(m.r0(), qvec4(0,1,2,3));
    QCOMPARE(m.r1(), qvec4(4,5,6,7));
    QCOMPARE(m.r2(), qvec4(8,9,10,11));

    QCOMPARE(m.m00(), 0.0f);
    QCOMPARE(m.m01(), 1.0f);
    QCOMPARE(m.m02(), 2.0f);
    QCOMPARE(m.m03(), 3.0f);
    QCOMPARE(m.m10(), 4.0f);
    QCOMPARE(m.m11(), 5.0f);
    QCOMPARE(m.m12(), 6.0f);
    QCOMPARE(m.m13(), 7.0f);
    QCOMPARE(m.m20(), 8.0f);
    QCOMPARE(m.m21(), 9.0f);
    QCOMPARE(m.m22(), 10.0f);
    QCOMPARE(m.m23(), 11.0f);

    m.setR0(qvec4(0,3,6,9));
    m.setR1(qvec4(1,4,7,10));
    m.setR2(qvec4(2,5,8,11));

    QCOMPARE(m(0,0), 0.0f);
    QCOMPARE(m(0,1), 3.0f);
    QCOMPARE(m(0,2), 6.0f);
    QCOMPARE(m(0,3), 9.0f);
    QCOMPARE(m(1,0), 1.0f);
    QCOMPARE(m(1,1), 4.0f);
    QCOMPARE(m(1,2), 7.0f);
    QCOMPARE(m(1,3), 10.0f);
    QCOMPARE(m(2,0), 2.0f);
    QCOMPARE(m(2,1), 5.0f);
    QCOMPARE(m(2,2), 8.0f);
    QCOMPARE(m(2,3), 11.0f);

    m.setC0(qvec3(0,4,8));
    m.setC1(qvec3(1,5,9));
    m.setC2(qvec3(2,6,10));
    m.setC3(qvec3(3,7,11));

    QCOMPARE(m.m00(), 0.0f);
    QCOMPARE(m.m01(), 1.0f);
    QCOMPARE(m.m02(), 2.0f);
    QCOMPARE(m.m03(), 3.0f);
    QCOMPARE(m.m10(), 4.0f);
    QCOMPARE(m.m11(), 5.0f);
    QCOMPARE(m.m12(), 6.0f);
    QCOMPARE(m.m13(), 7.0f);
    QCOMPARE(m.m20(), 8.0f);
    QCOMPARE(m.m21(), 9.0f);
    QCOMPARE(m.m22(), 10.0f);
    QCOMPARE(m.m23(), 11.0f);


    m.setM00(10);
    m.setM01(20);
    m.setM02(30);
    m.setM03(40);
    m.setM10(50);
    m.setM11(60);
    m.setM12(70);
    m.setM13(80);
    m.setM20(90);
    m.setM21(100);
    m.setM22(110);
    m.setM23(120);

    QCOMPARE(m(0,0), 10.0f);
    QCOMPARE(m(0,1), 20.0f);
    QCOMPARE(m(0,2), 30.0f);
    QCOMPARE(m(0,3), 40.0f);
    QCOMPARE(m(1,0), 50.0f);
    QCOMPARE(m(1,1), 60.0f);
    QCOMPARE(m(1,2), 70.0f);
    QCOMPARE(m(1,3), 80.0f);
    QCOMPARE(m(2,0), 90.0f);
    QCOMPARE(m(2,1), 100.0f);
    QCOMPARE(m(2,2), 110.0f);
    QCOMPARE(m(2,3), 120.0f);

}

void Matrix4Test::testdmat4x2()
{
    QCOMPARE(sizeof(qdmat4x2), 64U);

    qdmat4x2 m;
    QCOMPARE(m(0,0), 1.0);
    QCOMPARE(m(0,1), 0.0);
    QCOMPARE(m(0,2), 0.0);
    QCOMPARE(m(0,3), 0.0);
    QCOMPARE(m(1,0), 0.0);
    QCOMPARE(m(1,1), 1.0);
    QCOMPARE(m(1,2), 0.0);
    QCOMPARE(m(1,3), 0.0);

    m = qdmat4x2(0,1,2,3,4,5,6,7);
    QCOMPARE(m(0,0), 0.0);
    QCOMPARE(m(0,1), 2.0);
    QCOMPARE(m(0,2), 4.0);
    QCOMPARE(m(0,3), 6.0);
    QCOMPARE(m(1,0), 1.0);
    QCOMPARE(m(1,1), 3.0);
    QCOMPARE(m(1,2), 5.0);
    QCOMPARE(m(1,3), 7.0);

    QCOMPARE(m.c0(), qdvec2(0,1));
    QCOMPARE(m.c1(), qdvec2(2,3));
    QCOMPARE(m.c2(), qdvec2(4,5));
    QCOMPARE(m.c3(), qdvec2(6,7));

    m = qdmat4x2(qdvec2(0,4), qdvec2(1,5), qdvec2(2,6), qdvec2(3,7));

    QCOMPARE(m(0,0), 0.0);
    QCOMPARE(m(0,1), 1.0);
    QCOMPARE(m(0,2), 2.0);
    QCOMPARE(m(0,3), 3.0);
    QCOMPARE(m(1,0), 4.0);
    QCOMPARE(m(1,1), 5.0);
    QCOMPARE(m(1,2), 6.0);
    QCOMPARE(m(1,3), 7.0);

    QCOMPARE(m.r0(), qdvec4(0,1,2,3));
    QCOMPARE(m.r1(), qdvec4(4,5,6,7));

    QCOMPARE(m.m00(), 0.0);
    QCOMPARE(m.m01(), 1.0);
    QCOMPARE(m.m02(), 2.0);
    QCOMPARE(m.m03(), 3.0);
    QCOMPARE(m.m10(), 4.0);
    QCOMPARE(m.m11(), 5.0);
    QCOMPARE(m.m12(), 6.0);
    QCOMPARE(m.m13(), 7.0);

    m.setR0(qdvec4(0,2,4,6));
    m.setR1(qdvec4(1,3,5,7));

    QCOMPARE(m(0,0), 0.0);
    QCOMPARE(m(0,1), 2.0);
    QCOMPARE(m(0,2), 4.0);
    QCOMPARE(m(0,3), 6.0);
    QCOMPARE(m(1,0), 1.0);
    QCOMPARE(m(1,1), 3.0);
    QCOMPARE(m(1,2), 5.0);
    QCOMPARE(m(1,3), 7.0);

    m.setC0(qdvec2(0,4));
    m.setC1(qdvec2(1,5));
    m.setC2(qdvec2(2,6));
    m.setC3(qdvec2(3,7));

    QCOMPARE(m.m00(), 0.0);
    QCOMPARE(m.m01(), 1.0);
    QCOMPARE(m.m02(), 2.0);
    QCOMPARE(m.m03(), 3.0);
    QCOMPARE(m.m10(), 4.0);
    QCOMPARE(m.m11(), 5.0);
    QCOMPARE(m.m12(), 6.0);
    QCOMPARE(m.m13(), 7.0);


    m.setM00(10);
    m.setM01(20);
    m.setM02(30);
    m.setM03(40);
    m.setM10(50);
    m.setM11(60);
    m.setM12(70);
    m.setM13(80);

    QCOMPARE(m(0,0), 10.0);
    QCOMPARE(m(0,1), 20.0);
    QCOMPARE(m(0,2), 30.0);
    QCOMPARE(m(0,3), 40.0);
    QCOMPARE(m(1,0), 50.0);
    QCOMPARE(m(1,1), 60.0);
    QCOMPARE(m(1,2), 70.0);
    QCOMPARE(m(1,3), 80.0);

}

void Matrix4Test::testdmat4x3()
{
    QCOMPARE(sizeof(qdmat4x3), 96U);

    qdmat4x3 m;
    QCOMPARE(m(0,0), 1.0);
    QCOMPARE(m(0,1), 0.0);
    QCOMPARE(m(0,2), 0.0);
    QCOMPARE(m(0,3), 0.0);
    QCOMPARE(m(1,0), 0.0);
    QCOMPARE(m(1,1), 1.0);
    QCOMPARE(m(1,2), 0.0);
    QCOMPARE(m(1,3), 0.0);
    QCOMPARE(m(2,0), 0.0);
    QCOMPARE(m(2,1), 0.0);
    QCOMPARE(m(2,2), 1.0);
    QCOMPARE(m(2,3), 0.0);

    m = qdmat4x3(0,1,2,3,4,5,6,7,8,9,10,11);
    QCOMPARE(m(0,0), 0.0);
    QCOMPARE(m(0,1), 3.0);
    QCOMPARE(m(0,2), 6.0);
    QCOMPARE(m(0,3), 9.0);
    QCOMPARE(m(1,0), 1.0);
    QCOMPARE(m(1,1), 4.0);
    QCOMPARE(m(1,2), 7.0);
    QCOMPARE(m(1,3), 10.0);
    QCOMPARE(m(2,0), 2.0);
    QCOMPARE(m(2,1), 5.0);
    QCOMPARE(m(2,2), 8.0);
    QCOMPARE(m(2,3), 11.0);

    QCOMPARE(m.c0(), qdvec3(0,1,2));
    QCOMPARE(m.c1(), qdvec3(3,4,5));
    QCOMPARE(m.c2(), qdvec3(6,7,8));
    QCOMPARE(m.c3(), qdvec3(9,10,11));

    m = qdmat4x3(qdvec3(0,4,8), qdvec3(1,5,9), qdvec3(2,6,10), qdvec3(3,7,11));

    QCOMPARE(m(0,0), 0.0);
    QCOMPARE(m(0,1), 1.0);
    QCOMPARE(m(0,2), 2.0);
    QCOMPARE(m(0,3), 3.0);
    QCOMPARE(m(1,0), 4.0);
    QCOMPARE(m(1,1), 5.0);
    QCOMPARE(m(1,2), 6.0);
    QCOMPARE(m(1,3), 7.0);
    QCOMPARE(m(2,0), 8.0);
    QCOMPARE(m(2,1), 9.0);
    QCOMPARE(m(2,2), 10.0);
    QCOMPARE(m(2,3), 11.0);

    QCOMPARE(m.r0(), qdvec4(0,1,2,3));
    QCOMPARE(m.r1(), qdvec4(4,5,6,7));
    QCOMPARE(m.r2(), qdvec4(8,9,10,11));

    QCOMPARE(m.m00(), 0.0);
    QCOMPARE(m.m01(), 1.0);
    QCOMPARE(m.m02(), 2.0);
    QCOMPARE(m.m03(), 3.0);
    QCOMPARE(m.m10(), 4.0);
    QCOMPARE(m.m11(), 5.0);
    QCOMPARE(m.m12(), 6.0);
    QCOMPARE(m.m13(), 7.0);
    QCOMPARE(m.m20(), 8.0);
    QCOMPARE(m.m21(), 9.0);
    QCOMPARE(m.m22(), 10.0);
    QCOMPARE(m.m23(), 11.0);

    m.setR0(qdvec4(0,3,6,9));
    m.setR1(qdvec4(1,4,7,10));
    m.setR2(qdvec4(2,5,8,11));

    QCOMPARE(m(0,0), 0.0);
    QCOMPARE(m(0,1), 3.0);
    QCOMPARE(m(0,2), 6.0);
    QCOMPARE(m(0,3), 9.0);
    QCOMPARE(m(1,0), 1.0);
    QCOMPARE(m(1,1), 4.0);
    QCOMPARE(m(1,2), 7.0);
    QCOMPARE(m(1,3), 10.0);
    QCOMPARE(m(2,0), 2.0);
    QCOMPARE(m(2,1), 5.0);
    QCOMPARE(m(2,2), 8.0);
    QCOMPARE(m(2,3), 11.0);

    m.setC0(qdvec3(0,4,8));
    m.setC1(qdvec3(1,5,9));
    m.setC2(qdvec3(2,6,10));
    m.setC3(qdvec3(3,7,11));

    QCOMPARE(m.m00(), 0.0);
    QCOMPARE(m.m01(), 1.0);
    QCOMPARE(m.m02(), 2.0);
    QCOMPARE(m.m03(), 3.0);
    QCOMPARE(m.m10(), 4.0);
    QCOMPARE(m.m11(), 5.0);
    QCOMPARE(m.m12(), 6.0);
    QCOMPARE(m.m13(), 7.0);
    QCOMPARE(m.m20(), 8.0);
    QCOMPARE(m.m21(), 9.0);
    QCOMPARE(m.m22(), 10.0);
    QCOMPARE(m.m23(), 11.0);


    m.setM00(10);
    m.setM01(20);
    m.setM02(30);
    m.setM03(40);
    m.setM10(50);
    m.setM11(60);
    m.setM12(70);
    m.setM13(80);
    m.setM20(90);
    m.setM21(100);
    m.setM22(110);
    m.setM23(120);

    QCOMPARE(m(0,0), 10.0);
    QCOMPARE(m(0,1), 20.0);
    QCOMPARE(m(0,2), 30.0);
    QCOMPARE(m(0,3), 40.0);
    QCOMPARE(m(1,0), 50.0);
    QCOMPARE(m(1,1), 60.0);
    QCOMPARE(m(1,2), 70.0);
    QCOMPARE(m(1,3), 80.0);
    QCOMPARE(m(2,0), 90.0);
    QCOMPARE(m(2,1), 100.0);
    QCOMPARE(m(2,2), 110.0);
    QCOMPARE(m(2,3), 120.0);

}

QTEST_APPLESS_MAIN(Matrix4Test)

#include "tst_matrix4test.moc"

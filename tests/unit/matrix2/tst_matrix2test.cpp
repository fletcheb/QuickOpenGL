#include <QString>
#include <QtTest>

#include "quickmatrix2.h"
#include "quickopengl_plugin.h"

class Matrix2Test : public QObject
{
    Q_OBJECT

public:
    Matrix2Test();

private Q_SLOTS:
    void testmat2();
};

Matrix2Test::Matrix2Test()
{
    // register types
    QuickOpenGLPlugin plugin;
    plugin.registerTypes("QuickOpenGL");

}

void Matrix2Test::testmat2()
{
    QVERIFY2(true, "Failure");
}

QTEST_APPLESS_MAIN(Matrix2Test)

#include "tst_matrix2test.moc"

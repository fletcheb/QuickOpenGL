import QtQuick 2.11
import QuickOpenGL 1.0

//
// Test OpenGL Shader Storage Buffer Object Unsized Arrays
//
Item
{

    ShaderStorageBuffer
    {
        id: ssboIn
        syncToOpenGL: ShaderStorageBuffer.SyncWriteOnce

        property var a : [GL.vec4(0,1,2,3), GL.vec4(4,5,6,7), GL.vec4(8,9,10,11), GL.vec4(12,13,14,15), GL.vec4(16,17,18,19), GL.vec4(20,21,22,23), GL.vec4(24,25,26,27), GL.vec4(28,29,30,31)]
    }

    ShaderStorageBuffer
    {
        id: ssboOut
        syncFromOpenGL: true

        property var b;
    }

    GraphicsProgram
    {
        id: program
        anchors.fill: parent

        property var ssboIn: ssboIn
        property var ssboOut: ssboOut

        drawingMode: GraphicsProgram.DrawingModePoints
        vertices: 1

        vertexShader: "
            #version 430

            buffer ssboIn
            {
                vec4 a[];
            };

            buffer ssboOut
            {
                vec4 b[8];
            };

            void main()
            {
                for(int i=0;i<8;++i)
                {
                    b[i] = a[i];
                }
                gl_Position = vec4(vec3(0), 1.0);
            }"


        fragmentShader: "
            #version 430

            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(1.0);
            }"
    }

}

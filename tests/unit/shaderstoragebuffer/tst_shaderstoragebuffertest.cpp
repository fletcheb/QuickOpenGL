#include <QString>
#include <QtTest>
#include <QQuickView>
#include <QQmlEngine>
#include <QQmlContext>
#include <QQuickItem>
#include <QOpenGLContext>
#include "introspection.h"

class ShaderStorageBufferTest : public QObject
{
    Q_OBJECT

public:
    ShaderStorageBufferTest();

private Q_SLOTS:

    void testBasic();
    void testUnsizedArrays();
    void testUnsizedStructures();


};

ShaderStorageBufferTest::ShaderStorageBufferTest()
{
    QOpenGLContext context;
    if(context.create())
    {
        int version = context.format().majorVersion()*100+context.format().minorVersion()*10;

        if(version<430)
        {
            qWarning() << Q_FUNC_INFO << "This test requires OpenGL 430 or newer.  This platform only supports OpenGL" << version;
            exit(-1);
        }
    }
    else
    {
        qWarning() << Q_FUNC_INFO << "This test requires OpenGL support";
        exit(-1);
    }
}

void ShaderStorageBufferTest::testBasic()
{
    QQuickView view;
    view.setX(0);
    view.setY(0);
    view.setMaximumWidth(0);
    view.setMaximumHeight(0);
    view.setFlag(Qt::FramelessWindowHint);
    view.engine()->addImportPath("../../../src/");
    view.setSource(QUrl("qrc:/basic.qml"));
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));
    QVERIFY(QTest::qWaitForWindowActive(&view));
    QTest::qWait(100);
    view.close();

    QVERIFY(view.engine()->rootContext());

    view.rootObject()->children().at(0);

    QVERIFY(view.rootObject()->children().at(0)->property("float1")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("float3")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("float4")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("int1")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("int2")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("int3")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("float2")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("int4")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("uint1")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("uint2")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("uint3")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("uint4")!=QVariant());
    QVERIFY(view.rootObject()->children().at(0)->property("float4x4")!=QVariant());

    QCOMPARE(view.rootObject()->children().at(0)->property("float1"), view.rootObject()->children().at(1)->property("float1"));
    QCOMPARE(view.rootObject()->children().at(0)->property("float2"), view.rootObject()->children().at(1)->property("float2"));
    QCOMPARE(view.rootObject()->children().at(0)->property("float3"), view.rootObject()->children().at(1)->property("float3"));
    QCOMPARE(view.rootObject()->children().at(0)->property("float4"), view.rootObject()->children().at(1)->property("float4"));
    QCOMPARE(view.rootObject()->children().at(0)->property("int1"), view.rootObject()->children().at(1)->property("int1"));
    QCOMPARE(view.rootObject()->children().at(0)->property("int2"), view.rootObject()->children().at(1)->property("int2"));
    QCOMPARE(view.rootObject()->children().at(0)->property("int3"), view.rootObject()->children().at(1)->property("int3"));
    QCOMPARE(view.rootObject()->children().at(0)->property("int4"), view.rootObject()->children().at(1)->property("int4"));
    QCOMPARE(view.rootObject()->children().at(0)->property("uint1"), view.rootObject()->children().at(1)->property("uint1"));
    QCOMPARE(view.rootObject()->children().at(0)->property("uint2"), view.rootObject()->children().at(1)->property("uint2"));
    QCOMPARE(view.rootObject()->children().at(0)->property("uint3"), view.rootObject()->children().at(1)->property("uint3"));
    QCOMPARE(view.rootObject()->children().at(0)->property("uint4"), view.rootObject()->children().at(1)->property("uint4"));
    QCOMPARE(view.rootObject()->children().at(0)->property("float4x4"), view.rootObject()->children().at(1)->property("float4x4"));

}

void ShaderStorageBufferTest::testUnsizedArrays()
{
    QQuickView view;
    view.setX(0);
    view.setY(0);
    view.setMaximumWidth(0);
    view.setMaximumHeight(0);
    view.setFlag(Qt::FramelessWindowHint);
    view.engine()->addImportPath("../../../src/");
    view.setSource(QUrl("qrc:/unsizedarrays.qml"));
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));
    QVERIFY(QTest::qWaitForWindowActive(&view));
    QTest::qWait(100);
    view.close();
    QVERIFY(view.engine()->rootContext());

    int sizeA = view.rootObject()->children().at(0)->property("size").toInt();
    int sizeB = view.rootObject()->children().at(1)->property("size").toInt();

    QVERIFY(sizeA>0);
    QVERIFY(sizeB>0);

    QCOMPARE(sizeA, sizeB);

    QVariantList a = view.rootObject()->children().at(0)->property("a").toList();
    QVariantList b = view.rootObject()->children().at(1)->property("b").toList();

    QVERIFY(a.count()>0);
    QVERIFY(b.count()>0);
    QCOMPARE(a,b);

}

void ShaderStorageBufferTest::testUnsizedStructures()
{
    QQuickView view;
    view.setX(0);
    view.setY(0);
    view.setMaximumWidth(0);
    view.setMaximumHeight(0);
    view.setFlag(Qt::FramelessWindowHint);
    view.engine()->addImportPath("../../../src/");
    view.setSource(QUrl("qrc:/unsizedstructures.qml"));
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));
    QVERIFY(QTest::qWaitForWindowActive(&view));
    QTest::qWait(100);
    view.close();
    QVERIFY(view.engine()->rootContext());

    int sizeA = view.rootObject()->children().at(0)->property("size").toInt();
    int sizeB = view.rootObject()->children().at(1)->property("size").toInt();

    QVERIFY(sizeA>0);
    QVERIFY(sizeB>0);

    QCOMPARE(sizeA, sizeB);

    QVariant a = Introspection::conditionVariant(view.rootObject()->children().at(0)->property("a"));
    QVariant b = Introspection::conditionVariant(view.rootObject()->children().at(1)->property("b"));

    QVERIFY(a!=QVariant());
    QVERIFY(b!=QVariant());

    QCOMPARE(a,b);

}

QTEST_MAIN(ShaderStorageBufferTest)

#include "tst_shaderstoragebuffertest.moc"

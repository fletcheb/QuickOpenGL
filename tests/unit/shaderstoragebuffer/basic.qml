import QtQuick 2.11
import QuickOpenGL 1.0

Item
{

    ShaderStorageBuffer
    {
        id: ssboIn
        syncToOpenGL: ShaderStorageBuffer.SyncWriteOnce
        property real float1: 1.0
        property var float2: GL.vec2(2.0, 3.0)
        property var float3: GL.vec3(4.0, 5.0, 6.0)
        property var float4: GL.vec4(7.0,8.0,9.0,10.0)
        property int int1: 11
        property var int2: GL.ivec2(12,13)
        property var int3: GL.ivec3(14,15,16)
        property var int4: GL.ivec4(17,18,19,20)
        property int uint1: 21
        property var uint2: GL.uvec2(22,23)
        property var uint3: GL.uvec3(24,25,26)
        property var uint4: GL.uvec4(27,28,29,30)
        property real double1: 31.0
        property var double2: GL.dvec2(32.0, 33.0)
        property var double3: GL.dvec3(34.0, 35.0, 36.0)
        property var double4: GL.dvec4(37.0, 38.0, 39.0, 40.0)
        property var float4x4: GL.mat4(41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0, 50.0,51.0, 52.0, 53.0, 54.0, 55.0, 56.0)
        property var double4x4: GL.dmat4(57.0,58.0,59.0,60.0,61.0,62.0,63.0,64.0,65.0,66.0,67.0,68.0,69.0,70.0,71.0,72.0)
    }

    ShaderStorageBuffer
    {
        id: ssboOut
        syncFromOpenGL: true
        property real float1
        property var float2
        property var float3
        property var float4
        property int int1
        property var int2
        property var int3
        property var int4
        property int uint1
        property var uint2
        property var uint3
        property var uint4
        property real double1
        property var double2
        property var double3
        property var double4
        property var float4x4
        property var double4x4
    }

    GraphicsProgram
    {
        //onShaderStorageBlocksChanged: console.log(JSON.stringify(shaderStorageBlocks))
        id: program
        anchors.fill: parent

        property var ssboIn: ssboIn
        property var ssboOut: ssboOut

        drawingMode: GraphicsProgram.DrawingModePoints
        vertices: 1

        vertexShader: "
            #version 430
            buffer ssboIn
            {
                float float1;
                vec2 float2;
                vec3 float3;
                vec4 float4;

                int int1;
                ivec2 int2;
                ivec3 int3;
                ivec4 int4;

                uint uint1;
                uvec2 uint2;
                uvec3 uint3;
                uvec4 uint4;

                double double1;
                dvec2 double2;
                dvec3 double3;
                dvec4 double4;

                mat4 float4x4;
                dmat4 double4x4;
            } i;

            buffer ssboOut
            {
                float float1;
                vec2 float2;
                vec3 float3;
                vec4 float4;

                int int1;
                ivec2 int2;
                ivec3 int3;
                ivec4 int4;

                uint uint1;
                uvec2 uint2;
                uvec3 uint3;
                uvec4 uint4;

                double double1;
                dvec2 double2;
                dvec3 double3;
                dvec4 double4;

                mat4 float4x4;
                dmat4 double4x4;

            } o;

            void main()
            {
                o.float1 = i.float1;
                o.float2 = i.float2;
                o.float3 = i.float3;
                o.float4 = i.float4;
                o.int1 = i.int1;
                o.int2 = i.int2;
                o.int3 = i.int3;
                o.int4 = i.int4;
                o.uint1 = i.uint1;
                o.uint2 = i.uint2;
                o.uint3 = i.uint3;
                o.uint4 = i.uint4;
                o.double1 = i.double1;
                o.double2 = i.double2;
                o.double3 = i.double3;
                o.double4 = i.double4;
                o.float4x4 = i.float4x4;
                o.double4x4 = i.double4x4;

                gl_Position = vec4(vec3(0), 1.0);
            }"


        fragmentShader: "
            #version 430

            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(1.0);
            }"
    }

}

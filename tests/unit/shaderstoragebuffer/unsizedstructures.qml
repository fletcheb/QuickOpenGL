import QtQuick 2.11
import QuickOpenGL 1.0

//
// Test OpenGL Shader Storage Buffer Object Unsized Arrays
//
Item
{

    //
    // Convenience function to enumerate object names and values
    //
    function enumerateObjectTree(object, name)
    {
        var result = ''
        if(typeof object === 'object')
        {
            for(var key in object)
            {
                result += enumerateObjectTree(object[key], (name===undefined?'':(name+'.'))+key)
            }
        }
        else
        {
            result = name + ': ' + object + '\n'
        }
        return result
    }


    ShaderStorageBuffer
    {
        id: ssboIn
        syncToOpenGL: ShaderStorageBuffer.SyncWriteOnce

        property var a : [{aa0: 0, aa1: [1,2,3,4] },{aa0: 5, aa1: [6,7,8,9]},{aa0: 10, aa1: [11, 12, 13, 14]}]
    }

    ShaderStorageBuffer
    {
        id: ssboOut
        syncFromOpenGL: true

        property var b;
    }

    GraphicsProgram
    {
        id: program
        anchors.fill: parent

        property var ssboIn: ssboIn
        property var ssboOut: ssboOut

        drawingMode: GraphicsProgram.DrawingModePoints
        vertices: 1

        vertexShader: "
            #version 430

            struct aa
            {
                int aa0;
                int aa1[4];
            };

            buffer ssboIn
            {
                aa a[];
            };

            buffer ssboOut
            {
                aa b[3];
            };

            void main()
            {
                for(int i=0;i<3;++i)
                {
                    b[i].aa0 = a[i].aa0;
                    b[i].aa1[0] = a[i].aa1[0];
                    b[i].aa1[1] = a[i].aa1[1];
                    b[i].aa1[2] = a[i].aa1[2];
                    b[i].aa1[3] = a[i].aa1[3];
                }
                gl_Position = vec4(vec3(0), 1.0);
            }"


        fragmentShader: "
            #version 430

            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(1.0);
            }"
    }

}

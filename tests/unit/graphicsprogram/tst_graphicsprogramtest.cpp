#include <QString>
#include <QtTest>
#include <QQuickView>
#include <QQmlEngine>
#include <QQmlContext>
#include <QQuickItem>
#include <QOpenGLContext>

class GraphicsProgramTest : public QObject
{
    Q_OBJECT

public:
    GraphicsProgramTest();

private Q_SLOTS:
    void testVertexPassing();
    void testUniformPassing();
};

GraphicsProgramTest::GraphicsProgramTest()
{
    QOpenGLContext context;
    if(context.create())
    {

        int version = context.format().majorVersion()*100+context.format().minorVersion()*10;

        if(version<430)
        {
            qWarning() << Q_FUNC_INFO << "This test requires OpenGL 430 or newer.  This platform only supports OpenGL" << version;
            exit(-1);
        }
    }
    else
    {
        qWarning() << Q_FUNC_INFO << "This test requires OpenGL support";
        exit(-1);
    }
}

void GraphicsProgramTest::testVertexPassing()
{
    QQuickView view;
    view.setX(0);
    view.setY(0);
    view.setMaximumWidth(0);
    view.setMaximumHeight(0);
    view.setFlag(Qt::FramelessWindowHint);
    view.engine()->addImportPath("../../../src/");
    view.setSource(QUrl("qrc:/vertexpassing.qml"));
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));
    QVERIFY(QTest::qWaitForWindowActive(&view));
    view.close();
    QTest::qSleep(100);
    QCoreApplication::processEvents();
    QVERIFY(view.engine()->rootContext());
    QCOMPARE(view.rootObject()->property("inFloat1") ,view.rootObject()->property("outFloat1"));
    QCOMPARE(view.rootObject()->property("inFloat2") ,view.rootObject()->property("outFloat2"));
    QCOMPARE(view.rootObject()->property("inFloat3") ,view.rootObject()->property("outFloat3"));
    QCOMPARE(view.rootObject()->property("inFloat4") ,view.rootObject()->property("outFloat4"));
    QCOMPARE(view.rootObject()->property("inInt1") ,view.rootObject()->property("outInt1"));
    QCOMPARE(view.rootObject()->property("inInt2") ,view.rootObject()->property("outInt2"));
    QCOMPARE(view.rootObject()->property("inInt3") ,view.rootObject()->property("outInt3"));
    QCOMPARE(view.rootObject()->property("inInt4") ,view.rootObject()->property("outInt4"));
    QCOMPARE(view.rootObject()->property("inUint1") ,view.rootObject()->property("outUint1"));
    QCOMPARE(view.rootObject()->property("inUint2") ,view.rootObject()->property("outUint2"));
    QCOMPARE(view.rootObject()->property("inUint3") ,view.rootObject()->property("outUint3"));
    QCOMPARE(view.rootObject()->property("inUint4") ,view.rootObject()->property("outUint4"));
    QCOMPARE(view.rootObject()->property("inFloat4x4") ,view.rootObject()->property("outFloat4x4"));

    //
    // Note: As at Qt5.12, we expect passing doubles as vertex attributes to fail.  This is because
    // glVertexAttribPointer is called for all values in qsgbatchrenderer.cpp.  For 64bit primitive
    // values to be passed successfully, glVertexAttribLPointer should be called.
    //
    QEXPECT_FAIL("", "QtQuick scene graph cannot pass 64bit primitives as vertex attributes", Continue);
    QCOMPARE(view.rootObject()->property("inDouble1") ,view.rootObject()->property("outDouble1"));
    QEXPECT_FAIL("", "QtQuick scene graph cannot pass 64bit primitives as vertex attributes", Continue);
    QCOMPARE(view.rootObject()->property("inDouble2") ,view.rootObject()->property("outDouble2"));
    QEXPECT_FAIL("", "QtQuick scene graph cannot pass 64bit primitives as vertex attributes", Continue);
    QCOMPARE(view.rootObject()->property("inDouble3") ,view.rootObject()->property("outDouble3"));
    QEXPECT_FAIL("", "QtQuick scene graph cannot pass 64bit primitives as vertex attributes", Continue);
    QCOMPARE(view.rootObject()->property("inDouble4") ,view.rootObject()->property("outDouble4"));
    QEXPECT_FAIL("", "QtQuick scene graph cannot pass 64bit primitives as vertex attributes", Continue);
    QCOMPARE(view.rootObject()->property("inDouble4x4"),view.rootObject()->property("outDouble4x4"));


}

void GraphicsProgramTest::testUniformPassing()
{
    QQuickView view;
    view.setX(0);
    view.setY(0);
    view.setMaximumWidth(0);
    view.setMaximumHeight(0);
    view.setFlag(Qt::FramelessWindowHint);
    view.engine()->addImportPath("../../../src/");
    view.setSource(QUrl("qrc:/uniformpassing.qml"));
    view.show();

    QVERIFY(QTest::qWaitForWindowExposed(&view));
    QVERIFY(QTest::qWaitForWindowActive(&view));
    view.close();
    QTest::qSleep(100);
    QCoreApplication::processEvents();
    QVERIFY(view.engine()->rootContext());

    QCOMPARE(view.rootObject()->property("inFloat1") ,view.rootObject()->property("outFloat1"));
    QCOMPARE(view.rootObject()->property("inFloat2") ,view.rootObject()->property("outFloat2"));
    QCOMPARE(view.rootObject()->property("inFloat3") ,view.rootObject()->property("outFloat3"));
    QCOMPARE(view.rootObject()->property("inFloat4") ,view.rootObject()->property("outFloat4"));
    QCOMPARE(view.rootObject()->property("inInt1") ,view.rootObject()->property("outInt1"));
    QCOMPARE(view.rootObject()->property("inInt2") ,view.rootObject()->property("outInt2"));
    QCOMPARE(view.rootObject()->property("inInt3") ,view.rootObject()->property("outInt3"));
    QCOMPARE(view.rootObject()->property("inInt4") ,view.rootObject()->property("outInt4"));
    QCOMPARE(view.rootObject()->property("inUint1") ,view.rootObject()->property("outUint1"));
    QCOMPARE(view.rootObject()->property("inUint2") ,view.rootObject()->property("outUint2"));
    QCOMPARE(view.rootObject()->property("inUint3") ,view.rootObject()->property("outUint3"));
    QCOMPARE(view.rootObject()->property("inUint4") ,view.rootObject()->property("outUint4"));
    QCOMPARE(view.rootObject()->property("inFloat4x4") ,view.rootObject()->property("outFloat4x4"));
    QCOMPARE(view.rootObject()->property("inDouble1") ,view.rootObject()->property("outDouble1"));
    QCOMPARE(view.rootObject()->property("inDouble2") ,view.rootObject()->property("outDouble2"));
    QCOMPARE(view.rootObject()->property("inDouble3") ,view.rootObject()->property("outDouble3"));
    QCOMPARE(view.rootObject()->property("inDouble4") ,view.rootObject()->property("outDouble4"));
    QCOMPARE(view.rootObject()->property("inDouble4x4"),view.rootObject()->property("outDouble4x4"));

}

QTEST_MAIN(GraphicsProgramTest)

#include "tst_graphicsprogramtest.moc"

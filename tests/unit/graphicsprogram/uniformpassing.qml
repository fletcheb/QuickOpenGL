import QtQuick 2.11
import QuickOpenGL 1.0

Item {

    property real inFloat1 :program.float1
    property var inFloat2 :program.float2
    property var inFloat3 :program.float3
    property var inFloat4 :program.float4
    property int inInt1 :program.int1
    property var inInt2 :program.int2
    property var inInt3 :program.int3
    property var inInt4 :program.int4
    property int inUint1 :program.uint1
    property var inUint2 :program.uint2
    property var inUint3 :program.uint3
    property var inUint4 :program.uint4
    property real inDouble1 :program.double1
    property var inDouble2 :program.double2
    property var inDouble3 :program.double3
    property var inDouble4 :program.double4
    property var inFloat4x4 :program.float4x4
    property var inDouble4x4 :program.double4x4


    property var outFloat1 :resultSsbo.float1
    property var outFloat2 :resultSsbo.float2
    property var outFloat3 :resultSsbo.float3
    property var outFloat4 :resultSsbo.float4
    property var outInt1 :resultSsbo.int1
    property var outInt2 :resultSsbo.int2
    property var outInt3 :resultSsbo.int3
    property var outInt4 :resultSsbo.int4
    property var outUint1 :resultSsbo.uint1
    property var outUint2 :resultSsbo.uint2
    property var outUint3 :resultSsbo.uint3
    property var outUint4 :resultSsbo.uint4
    property var outDouble1 :resultSsbo.double1
    property var outDouble2 :resultSsbo.double2
    property var outDouble3 :resultSsbo.double3
    property var outDouble4 :resultSsbo.double4
    property var outFloat4x4 :resultSsbo.float4x4
    property var outDouble4x4 :resultSsbo.double4x4

    ShaderStorageBuffer
    {
        id: resultSsbo
        syncFromOpenGL: true
        property var float1
        property var float2
        property var float3
        property var float4
        property var int1
        property var int2
        property var int3
        property var int4
        property var uint1
        property var uint2
        property var uint3
        property var uint4
        property var double1
        property var double2
        property var double3
        property var double4
        property var float4x4
        property var double4x4
    }


    //
    // Test passing uniforms
    //
    GraphicsProgram
    {
        id: program
        anchors.fill: parent

        property var result: resultSsbo

        property real float1: 1.0
        property var float2: GL.vec2(2.0, 3.0)
        property var float3: GL.vec3(4.0, 5.0, 6.0)
        property var float4: GL.vec4(7.0,8.0,9.0,10.0)
        property int int1: 11
        property var int2: GL.ivec2(12,13)
        property var int3: GL.ivec3(14,15,16)
        property var int4: GL.ivec4(17,18,19,20)
        property int uint1: 21
        property var uint2: GL.uvec2(22,23)
        property var uint3: GL.uvec3(24,25,26)
        property var uint4: GL.uvec4(27,28,29,30)
        property real double1: 31.0
        property var double2: GL.dvec2(32.0, 33.0)
        property var double3: GL.dvec3(34.0, 35.0, 36.0)
        property var double4: GL.dvec4(37.0, 38.0, 39.0, 40.0)
        property var float4x4: GL.mat4(41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0, 50.0,51.0, 52.0, 53.0, 54.0, 55.0, 56.0)
        property var double4x4: GL.dmat4(57.0,58.0,59.0,60.0,61.0,62.0,63.0,64.0,65.0,66.0,67.0,68.0,69.0,70.0,71.0,72.0)


        drawingMode: GraphicsProgram.DrawingModePoints

        vertexShader: "
            #version 430

            uniform float float1;
            uniform int int1;
            uniform uint uint1;
            uniform double double1;

            uniform vec2 float2;
            uniform ivec2 int2;
            uniform uvec2 uint2;
            uniform dvec2 double2;

            uniform vec3 float3;
            uniform ivec3 int3;
            uniform uvec3 uint3;
            uniform dvec3 double3;

            uniform vec4 float4;
            uniform ivec4 int4;
            uniform uvec4 uint4;
            uniform dvec4 double4;

            uniform mat4 float4x4;
            uniform dmat4 double4x4;

            buffer result
            {
                float float1;
                vec2 float2;
                vec3 float3;
                vec4 float4;

                int int1;
                ivec2 int2;
                ivec3 int3;
                ivec4 int4;

                uint uint1;
                uvec2 uint2;
                uvec3 uint3;
                uvec4 uint4;

                double double1;
                dvec2 double2;
                dvec3 double3;
                dvec4 double4;

                mat4 float4x4;
                dmat4 double4x4;

            } resultBuffer;


            void main()
            {
                resultBuffer.float1 = float1;
                resultBuffer.float2 = float2;
                resultBuffer.float3 = float3;
                resultBuffer.float4 = float4;
                resultBuffer.int1 = int1;
                resultBuffer.int2 = int2;
                resultBuffer.int3 = int3;
                resultBuffer.int4 = int4;
                resultBuffer.uint1 = uint1;
                resultBuffer.uint2 = uint2;
                resultBuffer.uint3 = uint3;
                resultBuffer.uint4 = uint4;
                resultBuffer.double1 = double1;
                resultBuffer.double2 = double2;
                resultBuffer.double3 = double3;
                resultBuffer.double4 = double4;
                resultBuffer.float4x4 = float4x4;
                resultBuffer.double4x4 = double4x4;

                gl_Position = vec4(vec3(0), 1.0);
            }"


        fragmentShader: "
            #version 430

            out vec4 fragColor;

            void main()
            {
                fragColor = vec4(1.0);
            }"
    }

}

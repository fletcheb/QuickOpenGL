TEMPLATE = aux

QT += qml quick opengl openglextensions positioning

CONFIG += qt qt_doc c++11

QMAKE_DOCS = $$PWD/quickopengl.qdocconf

OTHER_FILES += $$_PRO_FILE_PWD_/*.qdoc

CLANG_RESOURCE_DIR = $$system(clang -print-resource-dir)

INCLUDEPATH += $$CLANG_RESOURCE_DIR/include

load(qt)
INCLUDEPATH += $$QMAKE_INCDIR

DISTFILES += \
    QuickOpenGLDoc \
    src/quickopengl-overview.qdoc \
    src/quickopengl-index.qdoc \
    src/quickopengl-examples.qdoc \
    src/quickopengl-qmltypes.qdoc \
    src/quickopengl-qmlbasictypes.qdoc \
    src/quickopengl-qmlsingletontypes.qdoc \
    src/quickopengl-qmlmodule.qdoc \
    src/quickopengl-qmlattachedproperties.qdoc

documentation.files = $$OUT_PWD/quickopengl.qch
documentation.path = $$[QT_INSTALL_DOCS]

INSTALLS += documentation

OTHER_FILES += quickopengl.qdocconf
